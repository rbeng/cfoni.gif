<?php

class CGIOut{
	const VERSION = '0.0.5';
	
	public  $template = '';
	public  $cookies  = array();
	public  $vars     = array();
	public  $objs     = array();
	public  $errors   = array();
	public  $header   = array();
	
	public function addHeader($header){
		array_push($this->header, $header);
	}
	public function addCookie(){
		$cookie = array();
		
		for($i=0; $i < func_num_args(); $i++){
			$arg = func_get_arg($i);
			array_push($cookie, $arg);
		}

		array_push($this->cookies, $cookie);
	}
	
	/**
	* Setting variable for the template
	* 
	* $out->setVar('Name', 'Value');
	*
	* @return void
	* @param string $name
	* @param mixed $value
	*/
	public function setVar($name, $value){
		$this->vars[$name] = $value;
	}
	
	public function setObj($name, &$value){
		$this->objs[$name] = $value;
	}
	
	public function addError($error){
		array_push($this->errors, $error);
	}
	
	public function isError(){
		if(sizeof($this->errors) > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
		
	/**
	* Process the template and display
	* 
	* $out->flush()
	*
	* @return void
	*/
	public function flush(){
		//
		// Send utf-8 header
		//
    
		$header = header("Content-type: text/html; charset=utf-8");
		//
		// Flush out any available header
		//
		if(count($this->header) > 0){
			foreach($this->header as $header){
				header($header);
			}
		}
		//
		// Flush default header if not header is being assigned
		//
		else{
			//foreach(self::$default_header as $header){
				header($header);
			//}
		}
		
		//
		// Flush cookies to the client
		//
		$this->flushCookies();
		if (! empty($this->vars["content"])) {
		  //include (ROOT_PATH . '/src/function/gzip_start.php');
		  echo $this->vars["content"];
		  //include (ROOT_PATH . '/src/function/gzip_end.php');
		}
		die();
	}
	
	/**
	* Redirect user to the URL
	* 
	* $out->redirect('http://localhost')
	*
	* @return void
	* @param string $url
	*/
	public function redirect($url){
		$this->flushCookies();
		header("Location: $url");
    exit();
	}
  
  public function scriptRedirect($url, $msg="") {
    $this->flushCookies();
		echo "<html><body><script>
      " . $msg . "
      document.location.href='" . $url . "';
      </script></body></html>";
    exit();
  }
	
	/**
	* Generate the page navigation HTML
	* 
  * print $out->html_pagenav(array(
  *   'url'       => 'http://localhost/',
  *   'total'     => 50,
  *   'per_page'  => 10,
  *   'page_size' => 10,
  *   'page'      => 1,
  *   'param'     => array(
  *   	'm' => 'demo',
  *   ),
  * ));
	*
	* @return void
	* @param string $url
	*/
		
	/**
	* Send the cookies to the browser
	* 
	* $this->flushCookies();
	*
	* @return void
	*/
	public function flushCookies(){
		//
		// Setting cookies
		//
		foreach ($this->cookies as $cookie){
			list($name, $value, $expire, $path, $domain) = $cookie;
			setcookie($name, $value, $expire, $path, $domain);
		}
	}
}

?>