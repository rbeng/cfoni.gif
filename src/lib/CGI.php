<?php
class CGI{
	const VERSION = '0.1.6';
	
	public $req     = array();
	public $cookies = array();
	
	function __construct(){
		//
		// Strip slashes if magic_quotes_gpc is enable
		//
		if(get_magic_quotes_gpc()){
			foreach(array_keys($_REQUEST) as $field){
				//
				// Check for multiple value field
				//
				if(is_array($_REQUEST[$field])){
					$this->req[$field] = array();
					
					foreach($_REQUEST[$field] as $value){
						array_push($this->req[$field], stripslashes($value));
					}
				}
				else{
					$this->req[$field] = stripslashes($_REQUEST[$field]);
				}
			}
			
			foreach(array_keys($_COOKIE) as $field){
				$this->cookies[$field] = stripslashes($_COOKIE[$field]);
			}
		}
		else{
			$this->req     = $_REQUEST;
			$this->cookies = $_COOKIE;
		}
	}
}
?>