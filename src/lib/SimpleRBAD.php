<?php

class SimpleRBAD{
	const VERSION = '0.1.0';
	
	private $users      = array();
	private $roles      = array();
	private $perms      = array();
	private $user_roles = array();
	private $role_perms = array();
	private $user_perms = array();
	
	private $error = '';
	
	function __construct($file=null){
		//
		// Initialize from file
		//
		if(! is_null($file)){
			$this->load($file);
		}
	}
	
	function load($file){
		$dom = new DOMDocument;
		$dom->preserveWhiteSpace = false;
		
		$dom->load($file);
		
		if(! $dom->hasChildNodes()){
			return null;
		}
		
		$nodes = $dom->childNodes;
		
		if($nodes->length != 1){
			$this->error = 'Can only have 1 root.';
			return null;
		}
		
		$root = $nodes->item(0);
		
		if($root->nodeName !== 'RBAC'){
			$this->error = 'Root name must be "RBAC".';
			return null;
		}
		
		$nodes_lv1 = $root->childNodes;
		for($i = 0; $i < $nodes_lv1->length; $i++){
			$node_lv1 = $nodes_lv1->item($i);
			
			switch($node_lv1->nodeName){
				case 'users' : {
					$nodes_lv2 = $node_lv1->childNodes;
					
					for($j = 0; $j < $nodes_lv2->length; $j++){
						$node_lv2 = $nodes_lv2->item($j);
						
						switch($node_lv2->nodeName){
							case 'user' : {
								$attr = array();
								$this->_getDOMAttr($node_lv2, $attr);
								
								//
								// Generate cipher from password if necessary
								//
								if(! array_key_exists('cipher', $attr)){
									$attr['cipher'] = md5($attr['pass']);
								}
								
								//
								// Adding user
								//
								$this->users[strtolower($attr['id'])] = array(
									'id'     => $attr['id'],
									'name'   => $attr['name'],
									'cipher' => $attr['cipher'],
								);
								
								//
								// Adding role
								//
								if($node_lv2->hasChildNodes()){
									
									$user      = strtolower($attr['id']);
									$nodes_lv3 = $node_lv2->childNodes;
									
									for($k = 0; $k < $nodes_lv3->length; $k++){
										$node_lv3 = $nodes_lv3->item($k);
										
										switch($node_lv3->nodeName){
											case 'role' : {
												$attr = array();
												$this->_getDOMAttr($node_lv3, $attr);
												
												$role = strtolower($attr['id']);
												$this->user_roles[$user][$role] = 1;
												
												break;
											}
										}
									}
								}
								
								break;
							}
						}
					}

					break;
				}
				
				case 'roles' : {
					$nodes_lv2 = $node_lv1->childNodes;
					
					for($j = 0; $j < $nodes_lv2->length; $j++){
						$node_lv2 = $nodes_lv2->item($j);
						
						switch($node_lv2->nodeName){
							case 'role' : {
								$attr = array();
								$this->_getDOMAttr($node_lv2, $attr);
								
								//
								// Adding role
								//
								$this->roles[strtolower($attr['id'])] = array(
									'id'     => $attr['id'],
									'name'   => $attr['name'],
								);
								
								//
								// Grant permission
								//
								if($node_lv2->hasChildNodes()){
									
									$role      = strtolower($attr['id']);
									$nodes_lv3 = $node_lv2->childNodes;
									
									for($k = 0; $k < $nodes_lv3->length; $k++){
										$node_lv3 = $nodes_lv3->item($k);
										
										switch($node_lv3->nodeName){
											case 'perm' : {
												$attr = array();
												$this->_getDOMAttr($node_lv3, $attr);
												
												$perm = strtolower($attr['id']);
												$this->role_perms[$role][$perm] = 1;
												
												break;
											}
										}
									}
								}
								
								break;
							}
						}
					}

					break;
				}
				
				case 'perms' : {
					$nodes_lv2 = $node_lv1->childNodes;
					
					for($j = 0; $j < $nodes_lv2->length; $j++){
						$node_lv2 = $nodes_lv2->item($j);
						
						switch($node_lv2->nodeName){
							case 'perm' : {
								$attr = array();
								$this->_getDOMAttr($node_lv2, $attr);
								
								//
								// Adding permission
								//
								$this->perms[strtolower($attr['id'])] = array(
									'id'   => $attr['id'],
									'name' => $attr['name'],
								);
								
								break;
							}
						}
					}

					break;
				}
			}
		}
		
		return 1;
	}
	
	function asXML(){
		$dom = new DOMDocument();
		$dom->formatOutput = true;
		
		$root = $dom->appendChild($dom->createElement('RBAC'));
		
		//
		// Adding users
		//
		$users = $root->appendChild($dom->createElement('users'));
		foreach($this->users as $x){
			$user = $dom->createElement('user');
			
			$user->setAttribute('id'    , $x['id']);
			$user->setAttribute('name'  , $x['name']);
			$user->setAttribute('cipher', $x['cipher']);
			
			//
			// Assign user to role
			//
			$id = strtolower($x['id']);
			if(array_key_exists($id, $this->user_roles)){
				foreach($this->user_roles[$id] as $k => $v){
					$role = $dom->createElement('role');
					
					$role->setAttribute('id', $this->roles[$k]['id']);
					$user->appendChild($role);
				}
			}
			
			$users->appendChild($user);
		}
		
		//
		// Adding roles
		//
		$roles = $root->appendChild($dom->createElement('roles'));
		foreach($this->roles as $x){
			$role = $dom->createElement('role');
			
			$role->setAttribute('id'  , $x['id']);
			$role->setAttribute('name', $x['name']);
			
			//
			// Grant permission to role
			//
			$id = strtolower($x['id']);
			if(array_key_exists($id, $this->role_perms)){
				foreach($this->role_perms[$id] as $k => $v){
					$perm = $dom->createElement('perm');
					
					$perm->setAttribute('id', $this->perms[$k]['id']);
					$role->appendChild($perm);
				}
			}
			
			$roles->appendChild($role);
		}
		
		//
		// Adding permission
		//
		$perms = $root->appendChild($dom->createElement('perms'));
		foreach($this->perms as $x){
			$perm = $dom->createElement('perm');
			
			$perm->setAttribute('id'  , $x['id']);
			$perm->setAttribute('name', $x['name']);
			
			$perms->appendChild($perm);
		}
		
		return $dom->saveXML();
	}
	
	function save($file){
		$fh = fopen($file, 'wb')     or die("Error: Fail to open '$filename'");
		flock($fh, LOCK_EX)          or die('Error: Fail to lock file');
		
		// Write $content to opened file
		fwrite($fh, $this->asXML())  or die('Error: Fail to write content');
		fflush($fh)                  or die('Error: Fail to flush file');
		
		flock($fh, LOCK_UN)          or die('Error: Fail to unlock file');
		
		//close the file
		fclose($fh);
	}
	
	public function clearCache(){
		$this->user_perms = array();
		
		return 1;
	}
	
	public function cleanUp(){
		#
		# todo
		#
		
		$this->_clearCache();
	}
	
	public function importUsers($file){
	}
	
	// goto -- Administrative --
	
	public function addUser($user, $name, $pass=null){
		//
		// Try to preserve the cipher
		//
		$cipher = '';
		if(is_null($pass)){
			$cipher = $this->users[strtolower($user)]['cipher'];
		}
		
		$this->users[strtolower($user)] = array(
			'id'     => $user,
			'name'   => $name,
		);
		
		if(is_null($pass)){
			$this->users[strtolower($user)]['cipher'] = $cipher;
		}
		else{
			$this->users[strtolower($user)]['cipher'] = md5($pass);
		}
		
		return 1;
	}
	
	public function delUser($user){
		unset($this->users[strtolower($user)]);
		
		return 1;
	}
	
	public function addRole($role, $name){
		$this->roles[strtolower($role)] = array(
			'id'   => $role,
			'name' => $name,
		);
		
		return 1;
	}
	
	public function delRole($role){
		unset($this->roles[strtolower($role)]);
		
		return 1;
	}
	
	public function addPerm($perm, $name){
		$this->perms[strtolower($perm)] = array(
			'id'   => $perm,
			'name' => $name,
		);
		
		return 1;
	}
	
	public function delPerm($perm){
		unset($this->perms[strtolower($perm)]);
		
		return 1;
	}
	
	public function assignUser($user, $role){
		$user = strtolower($user);
		$role = strtolower($role);
		
		$this->user_roles[$user][$role] = 1;
		
		return 1;
	}
	
	public function deassignUser($user, $role){
		$user = strtolower($user);
		$role = strtolower($role);
		
		unset($this->user_roles[$user][$role]);
		
		return 1;
	}
	
	public function grantPerm($perm, $role){
		$perm = strtolower($perm);
		$role = strtolower($role);
		
		$this->role_perms[$role][$perm] = 1;
		
		return 1;
	}
	
	public function revokePerm($perm, $role){
		$perm = strtolower($perm);
		$role = strtolower($role);
		
		unset($this->role_perms[$role][$perm]);
		
		return 1;
	}
	
	// goto -- Authentication --
	
	public function isAuth($user, $pass){
		$user = strtolower($user);
		
		if(array_key_exists($user, $this->users)
			&& strtolower($this->users[$user]['cipher']) === strtolower(md5($pass))){
			
			return 1;
		}
		else{
			return 0;
		}
		
	}
	
	public function isUser($user, $list){
		$user = strtolower($user);
		
		//
		// $list can be either a scalar or array
		//
		$users = array();
		if(is_array($list)){
			foreach($list as $x){
				$users[strtolower($x)] = 1;
			}
		}
		else{
			$users[strtolower($list)] = 1;
		}
		
		if(array_key_exists($user, $users)){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	public function hasRole($user, $role){
		$user = strtolower($user);
		
		//
		// $role can be either a scalar or array
		//
		$roles = array();
		if(is_array($role)){
			foreach($role as $x){
				$roles[] = strtolower($x);
			}
		}
		else{
			$roles[] = strtolower($role);
		}
		
		//
		// Checking to see if user has the role
		//
		if(! array_key_exists($user, $this->user_roles)){
			return 0;
		}
		
		//
		// Checking to see if user has the role
		//
		$found = 0;
		foreach($roles as $role){
			if(array_key_exists($role, $this->user_roles[$user])){
				$found = 1;
				break;
			}
		}
		
		return $found;
	}
	
	public function hasPerm($user, $perm){
		$user = strtolower($user);
		
		//
		// $perm can be either a scalar or array
		//
		$perms = array();
		if(is_array($perm)){
			foreach($perm as $x){
				$perms[] = strtolower($x);
			}
		}
		else{
			$perms[] = strtolower($perm);
		}
		
		//
		// Cache user's permission if necessary
		//
		if(! array_key_exists($user, $this->user_perms)){
			$this->_cacheUserPerms($user);
		}
		
		//
		// Checking to see if user has permission
		//
		$found = 0;
		foreach($perms as $perm){
			if(array_key_exists($perm, $this->user_perms[$user])){
				$found = 1;
				break;
			}
		}
		
		return $found;
	}
	
	// goto -- Review --
	
	public function getUsers(){
		$result = array();
		
		foreach($this->users as $user){
			$result[] = $user['id'];
		}
		
		return $result;
	}
	
	public function getRoles(){
		$result = array();
		
		foreach($this->roles as $role){
			$result[] = $role['id'];
		}
		
		return $result;
	}
	
	public function getPerms(){
		$result = array();
		
		foreach($this->perms as $perm){
			$result[] = $perm['id'];
		}
		
		return $result;
	}
	
	public function getUserRole($user){
		$user = strtolower($user);
		
		$result = array();
		if(array_key_exists($user, $this->user_roles)){
			foreach($this->user_roles[$user] as $role => $x){
				$result[] = $this->roles[$role]['id'];
			}
		}
		
		return $result;
	}
	
	public function getUserPerm($user){
		$user = strtolower($user);
		
		//
		// Cache user's permission if necessary
		//
		if(! array_key_exists($user, $this->user_perms)){
			$this->_cacheUserPerms($user);
		}
		
		$result = array();
		if(array_key_exists($user, $this->user_perms)){
			foreach($this->user_perms[$user] as $perm => $x){
				$result[] = $this->perms[$perm]['id'];
			}
		}
		
		return $result;
	
	}
	
	public function getRolePerm($role){
		$role = strtolower($role);
		
		$result = array();
		if(array_key_exists($role, $this->role_perms)){
			foreach($this->role_perms[$role] as $perm => $x){
				$result[] = $this->perms[$perm]['id'];
			}
		}
		
		return $result;
	}
	
	public function getUser($user){
		$user = strtolower($user);
		
		if(array_key_exists($user, $this->users)){
			return $this->users[$user]['name'];
		}
		else{
			return null;
		}
	}
	
	public function getUserCipher($user){
		$user = strtolower($user);

		if(array_key_exists($user, $this->users)){
			return $this->users[$user]['cipher'];
		}
		else{
			return null;
		}
	}
	
	public function setUserCipher($user, $cipher){
		$user = strtolower($user);
		
		if(array_key_exists($user, $this->users)){
			$this->users[$user]['cipher'] = $cipher;
			
			return 1;
		}
		else{
			return null;
		}
	}
	
	public function getRole($role){
		$role = strtolower($role);
		
		if(array_key_exists($role, $this->roles)){
			return $this->roles[$role]['name'];
		}
		else{
			return null;
		}
	}
	
	public function getPerm($perm){
		$perm = strtolower($perm);
		
		if(array_key_exists($perm, $this->perms)){
			return $this->perms[$perm]['name'];
		}
		else{
			return null;
		}
	}
	
	// goto -- Private --
	private function _cacheUserPerms($user){
		if(! array_key_exists($user, $this->user_perms)){
			
			$this->user_perms[$user] = array();
			
			//
			// Scan user's roles
			//
			if(array_key_exists($user, $this->user_roles)){
				foreach(array_keys($this->user_roles[$user]) as $role){
					
					//
					// Scan role's permissions
					//
					if(array_key_exists($role, $this->role_perms)){
						foreach(array_keys($this->role_perms[$role]) as $perm){
							$this->user_perms[$user][$perm] = 1;
						}
					}
				}
			}
		}
	}
	
	private function _getDOMAttr($dom, &$attr){
		$attr = array();
		
		foreach($dom->attributes as $obj){
			$attr[$obj->name] = $obj->value;
		}
	}
}

?>
