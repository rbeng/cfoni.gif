<?php
class Lang{
	const VERSION = '0.1.0';
	
	static public $lang_dir = 'lang';
	
	public $group = '';
	public $lookup = array();
	
	public function load($group){
		//
		// Setting current group and load up the lauguage file
		//
		$this->group = $group;
		$this->_load($group);
	}
	
	private function _load($group){
		//
		// Loading the language file
		//
		{
			//require self::$lang_dir. "$group.php";
			require DIR_LANG. "/$group.php";
					
			foreach ($lang as $k => $v){
				$this->lookup[$group][$k] = $v;
			}
		}
		
		//
		// Cleaning up the language file
		//
		unset($lang);
	}
	
	public function gq($group, $code, $vars=null){
		//
		// Load the language file if not loaded
		//
		if(! array_key_exists($group, $this->lookup)){
			$this->_load($group);
		}
				
		//
		// Lookup the table
		//
		if(array_key_exists($code, $this->lookup[$group])){
			$value = $this->lookup[$group][$code];
			
			//
			// If it is a symbolic link
			//
			if(is_array($value)){
				list($link_group, $link_code) = $value;
				
				//
				// Use the original code If the code not specify
				//
				if(! isset($link_code)){
					$link_code = $code;
				}
				
				//
				// Lookup the code recursively
				//
				return $this->gq($link_group, $link_code, $vars);
			}
			//
			// Found string
			//
			else{
				//
				// Convert template if found vars
				//
				if(isset($vars)){
					$value = $this->_tmpl_fillin($value, $vars);
				}
				
				return $value;
			}
		}
		//
		// Code not found
		//
		else{
			return "Lang([$code] not found in '$group')";
		}
	}
	
	public function q($code, $vars=null){
		return $this->gq($this->group, $code, $vars);
	}
	
	
	
	
	private function _tmpl_fillin($content, $vars){

		$content = preg_replace(
			'/\{\{([\w\.]+)\}\}/e',
			'array_key_exists(\'\1\', $vars) ? $vars[\'\1\'] : \'{{\'. \'\1\' . \'}}\'',
			$content
		);

		return $content;
	}
	
	
	
	/**
  * Fill in the template which is indicated by {{var}}.
  * 
  * @return string $content
  * @param string $template
  * @param array $vars
  */
	
}
?>