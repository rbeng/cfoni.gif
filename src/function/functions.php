<?php
$version = '0.0.5';

// goto -- FILE --

/**
 * Read the content of a file and return its value
 *
 * @return string $content
 * @param string $filename
 */
function file_read($filename){
	$fh = fopen($filename, 'rb') or die("Error: Fail to open '$filename'");
	flock($fh, LOCK_SH)          or die('Error: Fail to lock file');

	// Read content from the file
	$content = '';
	while(!feof($fh)){
		$content .= fgets($fh);
	}

	flock($fh, LOCK_UN)          or die('Error: Fail to unlock file');

	//close the file
	fclose($fh);

	return $content;
}

/**
 * Write the content into a file
 *
 * @return
 * @param string $filename $content
 */
function file_write($filename, $content=''){
	$fh = fopen($filename, 'wb') or die("Error: Fail to open '$filename'");
	flock($fh, LOCK_EX)          or die('Error: Fail to lock file');

	// Write $content to opened file
	fwrite($fh, $content);
	fflush($fh)                  or die('Error: Fail to flush file');

	flock($fh, LOCK_UN)          or die('Error: Fail to unlock file');

	//close the file
	fclose($fh);
}

/**
 * Append the content to the end of a file
 *
 * @return
 * @param string $filename $content
 */
function file_append($filename, $content=''){
	$fh = fopen($filename, 'ab') or die("Error: Fail to open '$filename'");
	flock($fh, LOCK_EX)          or die('Error: Fail to lock file');

	// Write $content to opened file
	fwrite($fh, $content)        or die('Error: Fail to write content');
	fflush($fh)                  or die('Error: Fail to flush file');

	flock($fh, LOCK_UN)          or die('Error: Fail to unlock file');

	//close the file
	fclose($fh);
}

// goto -- SAVE/LOAD --

/**
 * Encode the tab and newline properly
 *
 * @return string $encoded
 * @param string $str
 */
function tabencode($str){
	return addcslashes($str, "\\\t\n\r");
}

/**
 * Reverse of tabencode()
 *
 * @return string $decoded
 * @param string $str
 */
function tabdecode($str){
	return stripcslashes($str);
}

/**
 * Save data from text file with the first field as key
 *
 * @param string $filename
 * @param array $data
 * @return
 */
function data_save($filename, &$data){
	$fh = fopen($filename, 'wb') or die("Error: Fail to open '$filename'");
	flock($fh, LOCK_EX)          or die('Error: Fail to lock file');

	//
	// Saving data
	//
	foreach($data as $key => $value){
		fwrite($fh, "$key\t".tabencode($value)."\n")
			or die('Error: Fail to write content');
	}
	fflush($fh)                  or die('Error: Fail to flush file');

	flock($fh, LOCK_UN)          or die('Error: Fail to unlock file');
	fclose($fh);
}

/**
 * Load data from text file with the first field as key
 *
 * @param string $filename
 * @param array $data
 * @return
 */
function data_load($filename, &$data){
	$data = array();

	//
	// Processing data
	//
	{
		$fh = fopen($filename, 'rb') or die;
		flock($fh, LOCK_SH) or die('Error: Fail to lock file');

		while(!feof($fh)){
			if($line = fgets($fh, 1000000)){
				$line = rtrim($line, "\r\n");

				list($key, $value) = split("\t", $line, 2);
				$data[$key] = &tabdecode($value);
			}
		}

		flock($fh, LOCK_UN) or die('Error: Fail to unlock file');
		fclose($fh);
	}

	//
	// Delete empty key
	//
	unset($data['']);
}

// goto -- FILTER --

/**
 * Load the rules from a file for filtering purpose
 *
 * filter_load("rules.txt", $rules)
 *
 * @param string $file
 * @param array $rules
 * @return
 */
function filter_load($file, &$rules){

	$rules = array();

	$fh = fopen($file, 'rb') or die;
	flock($fh, LOCK_SH) or die('Error: Fail to lock file');

	while(!feof($fh)){
		if($line = fgets($fh, 1000000)){
			$line = rtrim($line, "\r\n");

			//
			// Skip empty line and comment
			//
			if(preg_match('/(?:^\s*$)|(?:^\s*#)/', $line)){
				continue;
			}

			array_push($rules, $line);
		}
	}

	flock($fh, LOCK_UN) or die('Error: Fail to unlock file');
	fclose($fh);
};

/**
 * Check if a string match any of the rules
 *
 * filter_match($rules, $str)
 *
 * @param string $file
 * @param array $rules
 * @return int $matched_num
 */
function filter_match($rules, $str){

	$found = 0;

	//
	// Checking rules
	//
	{
		foreach($rules as $rule){
			if(preg_match("/$rule/i", $str)){
				$found++;
				break;
			}
		}
	}

	return $found;
}

/**
 * Load the rules from a file for filtering purpose.
 *
 * Return 1 on suceess, else 0
 *
 * filter_load($content)
 *
 * @param string $file
 * @param array $rules
 * @return scalar $success
 */
function filter_compile($content){

	$lines = preg_split('/[\n\r]+/', $content);

	//
	// Skip empty line and comment
	//
	foreach($lines as $line){
		if(preg_match('/(?:^\s*$)|(?:^\s*#)/', $line)){
			continue;
		}

		if(preg_match("/$line/i",'') === false){
			return 0;
		}
	}

	return 1;
}

// goto -- OTHERS --

/**
 * Fill in the template which is indicated by {{var}}.
 *
 * @return string $content
 * @param string $template
 * @param array $vars
 */
function tmpl_fillin($content, $vars){

	$content = preg_replace(
		'/\{\{([\w\.]+)\}\}/e',
		'array_key_exists(\'\1\', $vars) ? $vars[\'\1\'] : \'{{\'. \'\1\' . \'}}\'',
		$content
	);

	return $content;
}

/**
 * Return time in YYYY-MM-DD hh:mm:ss format
 *
 * @return string $timestr
 * @param int $time
 * @param int $offset
 */
function timestr($time, $offset=0){
	$gmt_offset = date('Z');
	$parts = getdate($time-$gmt_offset+$offset);

	$date_part = join('-', array(
		sprintf('%04d', $parts['year']),
		sprintf('%02d', $parts['mon']),
		sprintf('%02d', $parts['mday']),
	));

	$time_part = join(':', array(
		sprintf('%02d', $parts['hours']),
		sprintf('%02d', $parts['minutes']),
		sprintf('%02d', $parts['seconds']),
	));

	return "$date_part $time_part";
}


function timestrToday($time, $offset=0){
	$gmt_offset = date('Z');
	$parts = getdate($time-$gmt_offset+$offset);

	$date_part = join('-', array(
		sprintf('%04d', $parts['year']),
		sprintf('%02d', $parts['mon']),
		sprintf('%02d', $parts['mday']),
	));

	$time_part = join(':', array(
		sprintf('%02d', $parts['hours']),
		sprintf('%02d', $parts['minutes']),
		sprintf('%02d', $parts['seconds']),
	));

	return "$date_part";
	
	
	
	}




function timeCheck($time, $offset=0){
	$gmt_offset = date('Z');
	$parts = getdate($time-$gmt_offset+$offset);
	
	
	$time_part = join(':', array(
		sprintf('%02d', $parts['hours']),
		//sprintf('%02d', $parts['minutes']),
		//sprintf('%02d', $parts['seconds']),
	));

	return "$time_part";
	}




/**
 * Return a 12 random chars string
 *
 * @return string $session
 */
function session_get(){
	$size = 12;
	$chars = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
	$chars_size = sizeof($chars);

	$session = '';
	for($i=0; $i<$size; $i++){
		$session .= $chars[rand(0, $chars_size-1)];
	}

	return $session;
}

/**
 * Get the item by weight
 *
 * $item = weight_get(array(
 *   array('item1', 1),
 *   array('item2', 10),
 * ));
 *
 * @param array $items
 * @return string $item
 */
function weight_get(&$items){
	//
	// Calculate total weight
	//
	$total = 0;
	foreach($items as $item){
		$total += $item[1];
	}

	if($total == 0){
		return null;
	}

	//
	// Get random
	//
	$rnd = rand(1, $total);

	$selected = -1;
	while($rnd > 0){
		$selected++;

		//
		// Skip zero weight
		//
		while($items[$selected][1] == 0){
			$selected++;
		}

		$rnd -= $items[$selected][1];

		if($rnd <= 0){
			break;
		}
	}

	return $items[$selected][0];
}


function in_multi_array($needle, $haystack) {
  $in_multi_array = false;
  if(in_array($needle, $haystack)) {
      $in_multi_array = true;
  } else {
      foreach($haystack as $key => $val) {
          if(is_array($val)) {
              if(in_multi_array($needle, $val)) {
                  $in_multi_array = true;
                  break;
              }
          }
      }
  }
  return $in_multi_array;
}

//
// Display flash file
//

 
    
function flash($dir,$w,$h){

	/*$banner = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="'.$w.'" height="'.$h.'">
	<param name="movie" value="'.$dir.'">
	<param name="quality" value="high">
	<param name="wmode" value="transparent"> 	
	<embed src="'.$dir.'" quality="high"  pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="'.$w.'" height="'.$h.'" wmode="transparent">
	</embed>
	</object>';*/
	
	$banner = '<script type="text/javascript">
	AC_FL_RunContent( "codebase","http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0","width","550","height","550","src","'.$dir.'","quality","high","pluginspage","http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash","wmode","transparent","movie","'.$dir.'" ); //end AC code
	</script>
	<noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="550" height="550">
	<param name="movie" value="'.$dir.'" />
	<param name="quality" value="high" />
	<param name="wmode" value="transparent" />
	<embed src="'.$dir.'" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="550" height="550" wmode="transparent"></embed>
	</object></noscript>';
	
	
	
	
	
	
	
	
	return $banner;

}
//
// Show FLASH url
//
function flashurl($dir,$w,$h,$url,$show){
	
	
	if($show=='Y'){
	
	
	$flash ='<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="'.$w.'" height="'.$h.'" id="howtosms" align="middle">
<param name="allowScriptAccess" value="sameDomain" />
<param name="movie" value="'.$dir.'" />
<param name="quality" value="high" />
<param name="flashvars" value="more1=linktext/link1.txt&more2=linktext/link1.txt&more3=linktext/link1.txt&more4=linktext/link1.txt" />
<param name="wmode" value="transparent" />
<param name="bgcolor" value="#ff0000" />
<embed src="'.$dir.'" quality="high" wmode="transparent" flashvars="more1=linktext/link1.txt&more2=linktext/link1.txt&more3=linktext/link1.txt&more4=linktext/link1.txt" bgcolor="#ff0000" width="'.$w.'" height="'.$h.'" name="howtosms" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
</object>';
	
	
	
	
	
	
	
	
	
	
	
	
	
	return $flash;
	exit;
	}
	else{
		$flash ='<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="'.$w.'" height="'.$h.'" id="howtosms" align="middle">
<param name="allowScriptAccess" value="sameDomain" />
<param name="movie" value="howtosms.swf" />
<param name="quality" value="high" />
<param name="wmode" value="transparent" />
<param name="bgcolor" value="#ff0000" />
<embed src="'.$dir.'" quality="high" wmode="transparent" bgcolor="#ff0000" width="'.$w.'" height="'.$h.'" name="howtosms" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
</object>';
		
			return $flash;
		exit;
		
		}
}



//
// Truncate
//
// a cutoff of $max characters, and add an $append at the end
	function truncate_str($str, $max = 10, $append = ' &hellip;') {

		if (strlen($str) > $max) {
			$str = wordwrap($str, $max, $append, 1);
			return substr($str, 0, strpos($str, $append, 0) + strlen($append));
		}
		else {
			return $str;
		}
	}




//
// Cache File
//
	function get_cache($filename, $ttl = '10s') {
		if(USE_CACHE != true) {
			return false;
		}
		require_once DIR_CLASSES . '/Cache.php';
		$objCache = new Cache(get_cache_dir(), get_cache_filename($filename) , $ttl); 
		$content = $objCache->get();
		unset($objCache);
		return $content;
	}
	
	function get_cache_filename($filename = null) {
		if(trim($filename) == '') {
			$filename = PAGE_ID.'_noname.php';
		} 
		return md5($filename . '/' . $_SERVER['QUERY_STRING']) . '.php';
	}
	
	function get_cache_dir() {
		return DIR_CACHE;
	}
	
	function get_cache_time($section = null) {
		return "\n<!-- " . $section . ' cache created at ' . date('Y-m-d H:i:s') . " -->\n";
	}


	function generateCode() {
		/* list all possible characters, similar looking characters and vowels have been removed */
		$possible = '23456789bcdfghjkmnpqrstvwxyz';
		$code = '';
		$i = 0;
		while ($i < 6) { 
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		
		return $code;
		
	}
	

	function generateReloadCode($combination){
		//$size = 12;
		$chars = array_merge(range('a', 'z'));
		$ints = array_merge(range(0, 9));
		$chars_size = sizeof($chars);
		$ints_size = sizeof($ints);
	
		$total_size=0;
		$code = '';
		
		foreach($combination as $key => $values){
			$total_size = $total_size+$values['size'];
	
			for($i=0; $i< $values['size']; $i++){
				if($values['type']=='ints'){
					$code .= $ints[rand(0, $ints_size-1)];
				}else{
					$code .= $chars[rand(0, $chars_size-1)];
				}
			}
			//$code= $code;
			
			
		}

		return $code;
	}
	
	
	function format_date($date, $format = 'Y-m-d H:i:s', $lang = 'en') {

		$strDate = '';
		
		if($date!='') {
		
			for ($i = 0; $i < strlen($format); $i++) {
	
				if (preg_match("/^[a-zA-Z]/", $format{$i})) {
	
					switch ($format{$i}) {
	
						case 'a':
						case 'A':
							$strDate .= format_date_lang($date, '%p', $lang);
							break;
	
						case 'D':
							$strDate .= format_date_lang($date, '%a', $lang);
							break;
	
						case 'l':
							$strDate .= format_date_lang($date, '%A', $lang);
							break;
	
						case 'M':
							$strDate .= format_date_lang($date, '%b', $lang);
							break;
	
						case 'F':
							$strDate .= format_date_lang($date, '%B', $lang);
							break;
	
						default:
							$strDate .= date($format{$i}, strtotime($date));
							break;
					}
				}
				else {
					$strDate .= $format{$i};
				}
			}
		}

		return $strDate;
	}

  //
  // Check Box return
  //
  
  //
  // $split:1,2
  // $codeCheck : $CODE['selling_trade']
  //return array
  
  
  function checkBoxEdit($split,$codeCheck){
  	
 		$BuyCat = split(',',$split); 
           
          $arrdata    = array();
          $arrRe      = array();
          $newFilterBuy = array();
          $i = 0;
					
					foreach($BuyCat as $keydata => $datakey){
						$newFilterBuy[$datakey] = $datakey;
		  			}
			    foreach($BuyCat as $keydata1 => $datakey1){
			    	if($datakey1!=""){
			    	$arrDoneBuy[$datakey1] = 'checked';
			    	}
			    	}
			    
          foreach($codeCheck as $datakey => $value){
           	$arrRe[$datakey]= $datakey;
           	}
        
          $nocheckBuy = array_diff_assoc ($arrRe,$newFilterBuy);
         
          foreach($nocheckBuy as $keyno => $datano){
          	$arrDoneBuy[$keyno] = '';
          } 	
  	
  	   return $arrDoneBuy;
 	}
 	
 function Taxonomizer($intAdsId,$strDescription,$category,$objects) {
  
  if(strstr(strtolower($strDescription),trim($objects))!=FALSE) {
    
    return $category;
  }
  return false;
}
 
 function replace_space($str){
		
		$value = ereg_replace('[[:space:]]+','%20',$str);
		return $value;
		}
		
function encode_img($pathimg)               
		{                                       
		    $fd = fopen ($pathimg, 'rb');           
		    $size= filesize($pathimg);              
		    $cont = fread ($fd, $size);         
		    fclose ($fd);                       
		    $encimg = base64_encode($cont);    
		    return $encimg;                    
		 }                                      
		
	function _getPhotoScaleSize($x, $y, $max_x, $max_y){
		//
		// If both x and y are less than max, no scale
		//
		if($x <= $max_x && $y <= $max_y){
			return array($x, $y);
		}

		$x_ratio = $x / $max_x;
		$y_ratio = $y / $max_y;
		$ratio   = $x / $y;
	
		
		//
		// Scale width
		//
		if($x_ratio > $y_ratio){
			$scaled_x = $x / $x_ratio;
			$scaled_y = $scaled_x / $ratio;
		}
		//
		// Scale Height
		//
		else{
			$scaled_y = $y / $y_ratio;
			$scaled_x = $scaled_y * $ratio;
		}

		$scaled_x = (int)$scaled_x;
		$scaled_y = (int)$scaled_y;

		return array($scaled_x, $scaled_y);
	}	
		
function ajaxLink($link,$width,$height,$name){
	$ajaxUrl = '<a href='.$link.'&KeepThis=true&TB_iframe=true&height='.$width.'&width='.$height.' class="thickbox">'.$name.' </a>';

return $ajaxUrl ;
}

//
// Display video.mov file
//
function video_mov($dir, $w, $h, $autoplay, $controller){
	$video = '<object width="'.$w.'" height="'.$h.'" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" 			
				codebase="http://www.apple.com/qtactivex/qtplugin.cab">
				<param name="src" value="'.$dir.'">
				<param name="autoplay" value="'.$autoplay.'">
				<param name="controller" value="'.$controller.'">
				<embed src="'.$dir.'" width="'.$w.'" height="'.$h.'" autoplay="'.$autoplay.'" controller="'.$controller.'" pluginspage="http://www.apple.com/quicktime/download/">
				</embed>
			  </object>';
	return $video;

}	

//
// Encrypt & decrypt
//
function encrypt($string, $key) {
  $result = '';
  for($i=0; $i<strlen($string); $i++) {
	$char = substr($string, $i, 1);
	$keychar = substr($key, ($i % strlen($key))-1, 1);
	$char = chr(ord($char)+ord($keychar));
	$result.=$char;
  }

  return base64_encode($result);
}

function decrypt($string, $key) {
  $result = '';
  $string = base64_decode($string);

  for($i=0; $i<strlen($string); $i++) {
	$char = substr($string, $i, 1);
	$keychar = substr($key, ($i % strlen($key))-1, 1);
	$char = chr(ord($char)-ord($keychar));
	$result.=$char;
  }

  return $result;
}


function self_fb_post($to_uid,$acToken,$apiid,$apisecretid,$message_str,$message_more,$photo) {
    global $fb; //this is the fb object
   require"c:/wamp/www/facebook/facebook.php";
   if($message_more==""){
   	 $message_more = $message_str;
   	}
   
    $fb = new Facebook(array( 'appId' => $apiid, 'secret' => $apisecretid, 'cookie' => true ));
    $result = false;
    $feed_dir = '/'.$to_uid.'/feed/';  //to the UID you want to send to
    //$message_str =  'Reeve';
    $msg_body = array('access_token' => $acToken,   
                  'name' => $message_str,
                  'message' => '',
                  'caption' => "www.sms2board.com",
                  'link' => 'http://www.sms2board.com',
                  'description' => $message_more, 
                  'picture' => $photo,
                  'actions' => array(array('name' => 'Notices',
                              'link' => 'http://www.sms2board.com'))
                  );

try {
            //this is the API call that does it all
    $result = $fb->api($feed_dir, 'post', $msg_body);
   
} 
catch (Exception $e) {       
    $result = $e->getMessage();
   
}

     return $result;
}

function pogenerate($poid){

$standard = 9;
$numberofValue = strlen($poid);
$numberZero = $standard - $numberofValue  ;
for($i=0; $i< $numberZero;$i++){
   $a = 0;
   $w .=$a;
}
$poid = "#".$w.$poid;
return $poid;
}


function getShippingCost($intCompanyId,$intItemId,$strZone,$strFrom ,$intState,$Weight,$Quantity){
	
	$c['strState'] = $strFrom ;
	if($c['strState']==$intState){
						$strZone = 1;
						$arr5[1]['str500Price'] = '3.50';
						$arr5[1]['str500FolowingPrice'] = '0.80';
						$arr5[1]['str2kPrice'] = '9.50';
						$arr5[1]['str2kFolowingPrice']='0.50';
					}
					else{
						if($intState=="Sabah" && $c['strState']=="Sabah"){
							$strZone = 2;
						$arr5[2]['str500Price'] = '4.50';
						$arr5[2]['str500FolowingPrice'] = 1;
						
						$arr5[2]['str2kPrice'] = 16;
						$arr5[2]['str2kFolowingPrice']=2;
						}
						elseif($intState=="Sarawak" && $c['strState']=="Sarawak"){
							$strZone = 2;
					  $arr5[2]['str500Price'] = '4.50';
						$arr5[2]['str500FolowingPrice'] = 1;
						
						$arr5[2]['str2kPrice'] = 16;
						$arr5[2]['str500FolowingPrice']=2;
						}
						elseif($intState=="Sarawak" && $c['strState']=="Sabah"){
							$strZone = 3;
						// <= 2 kg
						//$str500Price = '6.5';
						//$str500FolowingPrice = '1.5';
						$arr5[3]['str500Price'] = 6;
						$arr5[3]['str500FolowingPrice'] = '1.50';
						// > 2kg
						$arr5[3]['str2kPrice'] = 21;
						$arr5[3]['str2kFolowingPrice'] = 3;
						}	
						elseif($intState=="Sabah" && $c['strState']=="Sarawak"){
							$strZone = 3;
						$arr5[3]['str500Price'] = 6;
						$arr5[3]['str500FolowingPrice'] = '1.50';
						$arr5[3]['str2kPrice'] = 21;
						$arr5[3]['str2kFolowingPrice'] = 3;
						}		
					elseif(($intState!="Sabah" || $intState!="Sarawak") && $c['strState']=="Sabah"){
							$strZone = 5;
						$arr5[5]['str500Price'] = 7;
						$arr5[5]['str500FolowingPrice'] = 2;
						$arr5[5]['str2kPrice'] = 31;
						$arr5[5]['str2kFolowingPrice']=4;
						}	
						elseif(($intState!="Sabah" || $intState!="Sarawak")  && $c['strState']=="Sarawak"){
							$strZone = 4;
						$arr5[4]['str500Price'] = '6.50';
						$arr5[4]['str500FolowingPrice'] = '1.50';
						// > 2kg
						$arr5[4]['str2kPrice'] = 26;
						$arr5[4]['str2kFolowingPrice'] = '3.50';
						}				
					elseif(($c['strState']!="Sabah" || $c['strState']!="Sarawak") && $intState=="Sabah"){
							$strZone = 5;
												
						$arr5[5]['str500Price'] = 7;
						$arr5[5]['str500FolowingPrice'] = 2;
						
						$arr5[5]['str2kPrice'] = 31;
						$arr5[5]['str2kFolowingPrice']=4;
						}	
						elseif(($c['strState']!="Sabah" || $c['strState']!="Sarawak")  && $intState=="Sarawak"){
							$strZone = 4;
								// <= 2 kg
						//$str500Price = '6.5';
						//$str500FolowingPrice = '1.5';
						$arr5[4]['str500Price'] = '6.50';
						$arr5[4]['str500FolowingPrice'] = '1.50';
						// > 2kg
						$arr5[4]['str2kPrice'] = 26;
						$arr5[4]['str2kFolowingPrice'] = '3.50';
							}	
						elseif(($c['strState']!="Sabah" && $c['strState']!="Sarawak")  && $intState!="Sarawak" && $intState!="Sabah"  ){
							$strZone = 2;
					$arr5[2]['str500Price'] = '4.50';
						$arr5[2]['str500FolowingPrice'] = 1;
						
						$arr5[2]['str2kPrice'] = 16;
						$arr5[2]['str2kFolowingPrice']=2;
							}	
						}
		
					// Fomula
					$intTotal = $Weight*$Quantity/1000;
					
					if($intTotal > 2.001){
						$intTotalRemaing = $intTotal - 2;
						$intFirstCost = $arr5[$strZone]['str2kPrice'];
						
						$intBahagian   = (int)(($str2kFolowingPrice/0.5)*$arr5[$strZone]['str2kFolowingPrice']);
						$finalShippingCost = $intFirstCost + $intBahagian;
						}
					else{
						$intTotalRemaing = $intTotal - 0.5 ;
						$intFirstCost = 	$arr5[$strZone]['str500Price'];
						$intBahagian   = ($intTotalRemaing/0.25)* $arr5[$strZone]['str500FolowingPrice'];
						$finalShippingCost = $intFirstCost + $intBahagian;
						}
				  return $finalShippingCost;
		
	}




/** 
* Perform a simple text replace 
* This should be used when the string does not contain HTML 
* (off by default) 
*/ 
define('STR_HIGHLIGHT_SIMPLE', 1); 

/** 
* Only match whole words in the string 
* (off by default) 
*/ 
define('STR_HIGHLIGHT_WHOLEWD', 2); 

/** 
* Case sensitive matching 
* (off by default) 
*/ 
define('STR_HIGHLIGHT_CASESENS', 4); 

/** 
* Overwrite links if matched 
* This should be used when the replacement string is a link 
* (off by default) 
*/ 
define('STR_HIGHLIGHT_STRIPLINKS', 8); 

/** 
* Highlight a string in text without corrupting HTML tags 
* 
* @author Aidan Lister 
* @version 3.1.1 
* @link http://aidanlister.com/repos/v/function.str_highlight.php 
* @param string $text Haystack - The text to search 
* @param array|string $needle Needle - The string to highlight 
* @param bool $options Bitwise set of options 
* @param array $highlight Replacement string 
* @return Text with needle highlighted 
*/ 
function str_highlight($text, $needle, $options = null, $highlight = null) 
{ 
// Default highlighting 
if ($highlight === null) { 
$highlight = '<font color=blue>'.$needle.'</font>'; 
} 

// Select pattern to use 
if ($options & STR_HIGHLIGHT_SIMPLE) { 
$pattern = '#(%s)#'; 
$sl_pattern = '#(%s)#'; 
} else { 
$pattern = '#(?!<.*?)(%s)(?![^<>]*?>)#'; 
$sl_pattern = '#(%s)#'; 
} 

// Case sensitivity 
if (!($options & STR_HIGHLIGHT_CASESENS)) { 
$pattern .= 'i'; 
$sl_pattern .= 'i'; 
} 

$needle = (array) $needle; 
foreach ($needle as $needle_s) { 
$needle_s = preg_quote($needle_s); 

// Escape needle with optional whole word check 
if ($options & STR_HIGHLIGHT_WHOLEWD) { 
$needle_s = 'b' . $needle_s . 'b'; 
} 

// Strip links 
if ($options & STR_HIGHLIGHT_STRIPLINKS) { 
$sl_regex = sprintf($sl_pattern, $needle_s); 
$text = preg_replace($sl_regex, '1', $text); 
} 

$regex = sprintf($pattern, $needle_s); 
$text = preg_replace($regex, $highlight, $text); 
} 

return $text; 
} 



function thumbnail_generator($srcfile, $params)
{
    // getting source image size
    @list($w, $h) = getimagesize($srcfile);
    if ($w == false)
        return false;

    // checking params array 
    if (!(is_array($params)&&is_array($params[0])))
        return false;

    $src = ImageCreateFromJpeg($srcfile);
    list($s1_w, $s1_h) = thumbnail_calcsize($w, $h, $params[0]['size']);

    // Create first thumbnail
    // Remember, first thumbnail should be largest thumbnail
    $img_s1 = imagecreatetruecolor($s1_w, $s1_h);
    imagecopyresampled($img_s1, $src, 0, 0, 0, 0, $s1_w, $s1_h, $w, $h);
    imagedestroy($src); // Destroy source image 

    $intS = sizeof($params);
    // Other thumbnails are just downscaled copies of the first one
    for($i=1; $i<$intS; $i++)
    {
        list($cur_w, $cur_h) = thumbnail_calcsize($w, $h, $params[$i]['size']);
        $img_cur = imagecreatetruecolor($cur_w, $cur_h);
        imagecopyresampled($img_cur, $img_s1, 0, 0, 0, 0, $cur_w, $cur_h, $s1_w, $s1_h);
        imagejpeg($img_cur, $params[$i]['file'], 90);
        imagedestroy($img_cur);
    }

    // Saving first thumbnail 
    imagejpeg($img_s1, $params[0]['file'], 90);
    imagedestroy($img_s1);
    return true;
}
function thumbnail_calcsize($w, $h, $square)
{
    $k = $square / max($w, $h);
    return array($w*$k, $h*$k);
}


function safe($value){ 
   return mysql_real_escape_string(strip_tags($value)); 
} 

function getSalesmanCode($id,$strType){
	

date_default_timezone_set('Asia/Kuala_Lumpur');
$id =  8800 + $id;

$standard = 4;
$numberofValue = strlen($id);
$numberZero = $standard - $numberofValue  ;
for($i=0; $i< $numberZero;$i++){
   $a = 0;
   $w .=$a;
}
$poid = $w.$id;
$n = strlen($id);
$r =  (int)($n%4)+($n/4) ;
$id = Date('dmy').$w.$id;               

return $id;
	
}

function getResellerCode($id){
	

date_default_timezone_set('Asia/Kuala_Lumpur');
$id =  1000 + $id;

$standard = 4;
$numberofValue = strlen($id);
$numberZero = $standard - $numberofValue  ;
for($i=0; $i< $numberZero;$i++){
   $a = 0;
   $w .=$a;
}
$poid = $w.$id;
$n = strlen($id);
$r =  (int)($n%4)+($n/4) ;
$id = $w.$id.Date('dmy');               

return $id;
	
}
?>