<?php

class DB {

	/* public: connection parameters */
	public $Host     = "";
	public $Database = "";
	public $User     = "";
	public $Password = "";

	/* public: configuration parameters */
	public $Auto_Free     = 0;     ## Set to 1 for automatic mysql_free_result()
	public $Debug         = 0;     ## Set to 1 for debugging messages.
	public $Halt_On_Error = "handler"; ## "yes" (halt with message), "no" (ignore errors quietly), "report" (ignore errror, but spit a warning), "handler" (pass to error handler defined)
	public $PConnect      = 0;     ## Set to 1 to use persistent database connections
	public $Seq_Table     = "db_sequence";

	/* public: result array and current row number */
	public $Record   = array();
	public $Row;
  
	/* public: current error number and error text */
	public $Errno    = 0;
	public $Error    = "";

	/* public: this is an api revision, not a CVS revision. */
	public $type     = "mysql";
	public $revision = "1.2";

	/* private: link and query handles */
	public $Link_ID  = 0;
	public $Query_ID = 0;
	public $Query_ID_arr = 0;
	public $locked   = false;      ## set to true while we have a lock


	/* public: constructor */
	public function DB($query = "") {

		$this->query($query);
	}

	/* public: connection management */
	public function connect($Database = DB_DATABASE, $Host = DB_SERVER, $User = DB_SERVER_USERNAME, $Password = DB_SERVER_PASSWORD) {

		/* establish connection, select database */
		if ($this->Link_ID == 0) {
			if (!$this->PConnect) {
				$this->Link_ID = mysql_connect($Host, $User, $Password);
			} else {
				$this->Link_ID = mysql_pconnect($Host, $User, $Password); 
			}

			if (!$this->Link_ID) {
				$this->halt("connect($Host, $User, \$Password) failed.");
				return 0;
			}

			if (!@mysql_select_db($Database, $this->Link_ID)) {
				$this->halt("cannot use database " . $Database);
				return 0;
			}
		}

		return $this->Link_ID;
	}
	/* The empty query string is passed on from the constructor,
	* when calling the class without a query, e.g. in situations
	* like these: '$db = new DB_Sql_Subclass;'   */
	public function query($Query_String) {
		//echo $Query_String;
		if ($Query_String == "")
			return 0;

		$Query_String = trim($Query_String); 

		if (!$this->connect())
			return 0;

		if ($this->Debug)
			printf("Debug: query = %s<br>\n", $Query_String);

		$this->Query_ID = @mysql_query($Query_String, $this->Link_ID);
		$this->Row   = 0;
		$this->Errno = mysql_errno();
		$this->Error = mysql_error();

		if (!$this->Query_ID)
			$this->halt("Invalid SQL: " . $Query_String);

		if ($this->Query_ID)
			return $this->Query_ID;
		else
			return 0;
	}

	/* public: some trivial reporting */
	public function link_id() {

		return $this->Link_ID;
	}

	function query_id() {

		return $this->Query_ID;
	}

	/* public: discard the query result */
	public function free($query_id = '') {

		if ($query_id != '') {
			mysql_free_result($query_id);
		}
		else if ($this->Query_ID != '') {
			mysql_free_result($this->Query_ID);
			$this->Query_ID = 0;
		}
	}

	/* public: walk result set */
	public function next_record($query_id = '') {

		if (!$this->Query_ID && $query_id == '') {
			$this->halt("next_record called with no query pending.");
			return 0;
		}

		if ($query_id != '')
			$this->Record = @mysql_fetch_array($query_id, MYSQL_ASSOC);
		else
			$this->Record = @mysql_fetch_array($this->Query_ID, MYSQL_ASSOC);

		$this->Row   += 1;
		$this->Errno  = mysql_errno();
		$this->Error  = mysql_error();

		$stat = is_array($this->Record);
		if (!$stat && $this->Auto_Free) {
			$this->free();
		}

		return $stat;
	}

	/* public: position in result set */
	public function seek($pos = 0, $query_id = '') {

		if ($query_id != '')
			$status = @mysql_data_seek($query_id, $pos);
		else
			$status = @mysql_data_seek($this->Query_ID, $pos);

		if ($status)
			$this->Row = $pos;
		else {
			$this->halt("seek($pos) failed: result has " . $this->num_rows() . " rows.");

			if ($query_id != '')
				@mysql_data_seek($query_id, $this->num_rows());
			else
				@mysql_data_seek($this->Query_ID, $this->num_rows());

			$this->Row = $this->num_rows($query_id);
			return 0;
		}

		return 1;
	}

	/* public: table locking */
	public function lock($table, $mode = "write") {

		$query = "lock tables ";
		if (is_array($table)) {
			while (list($key,$value) = each($table)) {
				// text keys are "read", "read local", "write", "low priority write"
				if (is_int($key))
					$key = $mode;

				if (strpos($value, ","))
					$query .= str_replace(",", " $key, ", $value) . " $key, ";
				else
					$query .= "$value $key, ";
			}
			$query = substr($query, 0, -2);
		}
		else if (strpos($table, ","))
			$query .= str_replace(",", " $mode, ", $table) . " $mode";
		else
			$query .= "$table $mode";

		if (!$this->query($query)) {
			$this->halt("lock() failed.");
			return false;
		}

		$this->locked = true;
		return true;
	}

	public function unlock() {

		// set before unlock to avoid potential loop
		$this->locked = false;

		if (!$this->query("unlock tables")) {
			$this->halt("unlock() failed.");
			return false;
		}

		return true;
	}

	/* public: evaluate the result (size, width) */
	public function affected_rows() {

		return mysql_affected_rows($this->Link_ID);
	}

	public function num_rows($query_id = '') {

		if ($query_id != '')
			return mysql_num_rows($query_id);

		if ($this->Query_ID != '')
			return mysql_num_rows($this->Query_ID);
	}

	public function num_fields() {

		return @mysql_num_fields($this->Query_ID);
	}

	public function insert_id(){

		return @mysql_insert_id($this->Link_ID);
	}
	
	/* public: sequence numbers */
	public function nextid($seq_name) {

		/* if no current lock, lock sequence table */
		if (!$this->locked) {
			if ($this->lock($this->Seq_Table))
				$locked = true;
			else
				$this->halt("cannot lock " . $this->Seq_Table . " - has it been created?");
			return 0;
		}

		/* get sequence number and increment */
		$q = sprintf("select nextid from %s where seq_name = '%s'", $this->Seq_Table, $seq_name);

		if (!$this->query($q)) {
			$this->halt('query failed in nextid: ' . $q);
			return 0;
		}

		/* No current value, make one */
		if (!$this->next_record()) {
			$currentid = 0;
			$q = sprintf("insert into %s values('%s', %s)", $this->Seq_Table, $seq_name, $currentid);

			if (!$this->query($q)) {
				$this->halt('query failed in nextid: ' . $q);
				return 0;
			}
		}
		else {
			$currentid = $this->f("nextid");
		}

		$nextid = $currentid + 1;
		$q = sprintf("update %s set nextid = '%s' where seq_name = '%s'", $this->Seq_Table, $nextid, $seq_name);

		if (!$this->query($q)) {
			$this->halt('query failed in nextid: ' . $q);
			return 0;
		}

		/* if nextid() locked the sequence table, unlock it */
		if ($locked)
			$this->unlock();

		return $nextid;
	}

	/* public: return table metadata */
	public function metadata_deprecated($table = "", $full = false) {

		$count = 0;
		$id    = 0;
		$res   = array();

		// if no $table specified, assume that we are working with a query
		// result
		if ($table) {
			$this->connect();
			$id = @mysql_list_fields($this->Database, $table);

			if (!$id) {
				$this->halt("Metadata query failed.");
				return false;
			}
		}
		else {
			$id = $this->Query_ID; 

			if (!$id) {
				$this->halt("No query specified.");
				return false;
			}
		}

		$count = @mysql_num_fields($id);

		// made this IF due to performance (one if is faster than $count if's)
		if (!$full) {
			for ($i = 0; $i < $count; $i++) {
				$res[$i]["table"] = @mysql_field_table ($id, $i);
				$res[$i]["name"]  = @mysql_field_name  ($id, $i);
				$res[$i]["type"]  = @mysql_field_type  ($id, $i);
				$res[$i]["len"]   = @mysql_field_len   ($id, $i);
				$res[$i]["flags"] = @mysql_field_flags ($id, $i);
			}
		}
		else { // full
			$res["num_fields"]= $count;

			for ($i = 0; $i < $count; $i++) {
				$res[$i]["table"] = @mysql_field_table ($id, $i);
				$res[$i]["name"]  = @mysql_field_name  ($id, $i);
				$res[$i]["type"]  = @mysql_field_type  ($id, $i);
				$res[$i]["len"]   = @mysql_field_len   ($id, $i);
				$res[$i]["flags"] = @mysql_field_flags ($id, $i);
				$res["meta"][$res[$i]["name"]] = $i;
			}
		}

		// free the result only if we were called on a table
		if ($table)
			@mysql_free_result($id);

		return $res;
	}

	public function metadata($table = "", $full = false) {
		$attField =& $attribute[$table] ;
		$sql = 'SELECT * FROM ' . $table . ' LIMIT 0';
		if($query = $this -> query($sql)) {
			$fields = mysql_num_fields($query);
			for ($i=0; $i < $fields; $i++) {
				$name = mysql_field_name($query, $i);
				$attField[$name]['name']  = $name;
				$attField[$name]['type']  = mysql_field_type($query, $i);
				$attField[$name]['len']   = mysql_field_len($query, $i);
				$attField[$name]['flags'] = mysql_field_flags($query, $i);
			}
		}
		return $attribute;
	}

	/* public: find available table names */
	public function table_names() {

		$this->connect();
		$h = @mysql_query("show tables", $this->Link_ID);
		$i = 0;

		while ($info = @mysql_fetch_row($h)) {
			$return[$i]["table_name"]      = $info[0];
			$return[$i]["tablespace_name"] = $this->Database;
			$return[$i]["database"]        = $this->Database;
			$i++;
		}

		@mysql_free_result($h);
		return $return;
	}

	/* private: error handling */
	public function halt($msg) {

		$this->Error = @mysql_error($this->Link_ID);
		$this->Errno = @mysql_errno($this->Link_ID);

		if ($this->locked)
			$this->unlock();

		if ($this->Halt_On_Error == "no")
			return;

		if ($this->Halt_On_Error == "yes") {
			$this->haltmsg($msg);
			die("Session halted.");
		}

		if ($this->Halt_On_Error == "report")
			$this->haltmsg($msg);
		else if ($this->Halt_On_Error == "handler") {
			$msg .= ' MySQL Error :' . $this->Errno . ' ' . $this->Error;
			trigger_error($msg, E_USER_ERROR);
			// die("Session halted.");
		}
	}

	public function haltmsg($msg) {

		printf("</td></tr></table><b>Database error:</b> %s<br>\n", $msg);
		printf("<b>MySQL Error</b>: %s (%s)<br>\n", $this->Errno, $this->Error);
	}

	public function close_db() {

		if ($this->Link_ID != '')
			return mysql_close($this->Link_ID);
		else
			return 0;
	}

	public function begin($link_id = '') {

		if (DB_TRANSACTION == true) { 
			if ($link_id == '')
				$link_id=$this->Link_ID;

			mysql_query("BEGIN", $link_id); 
		}
	} 

	public function rollback($link_id = '') {

		if (DB_TRANSACTION == true) { 
			if ($link_id == '')
				$link_id=$this->Link_ID;

			mysql_query("ROLLBACK", $link_id); 
		} 
	}

	public function commit($link_id = '') {

		if (DB_TRANSACTION == true) { 
			if ($link_id == '')
				$link_id=$this->Link_ID;

			mysql_query("COMMIT", $link_id); 
		}
	} 
}
?>