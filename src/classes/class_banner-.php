<?php

//
// For debuging
//
if(! defined('ROOT_PATH')){
	//define('ROOT_PATH', 'C:/Apache/htdocs/Match3/');
	define('ROOT_PATH', dirname(__FILE__).'/');
	require_once(ROOT_PATH . 'src/config.php');
	require_once(ROOT_PATH . 'src/functions.php');
};

class class_banner{
	public $version = '0.0.1';
	
	public  $isLoaded = false;
	private $banner = array();
	
	function __construct(){
	}
	
	public function _loadBanner(){
		global $CONF;
		
		$fh = fopen($CONF['dir_data'].'banner.txt', 'rb') or die;
		flock($fh, LOCK_SH) or die('Error: Fail to lock file');

		while(!feof($fh)){
			if($line = fgets($fh, 1000000)){
				$line = rtrim($line, "\r\n");
				
				list($set, $id, $weight, $code, $comment) = split("\t", $line);
				$code = tabdecode($code);
				
				$this->banner[$id] = array($set, $id, $weight, $code, $comment);
			}
		}
		
		flock($fh, LOCK_UN) or die('Error: Fail to unlock file');
		fclose($fh);
		
		$this->isLoaded = true;
	}
	
	public function show($set){
		if(! $this->isLoaded){
			$this->_loadBanner();
		}
		
		$filtered = array();
		foreach($this->banner as $banner){
			$id    = $banner[1];
			$weight= $banner[2];
			
			if($banner[0] == $set){
				array_push($filtered, array($id, $weight));
			}
		}
		
		$selected = weight_get($filtered);
		
		if(isset($selected)){
			return $this->banner[$selected][3];
		}
		else{
			return '';
		}
	}
}

?>