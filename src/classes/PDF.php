<?php
//require('fpdf.php');


require_once(ROOT_PATH . 'fpdf.php');
class PDF extends FPDF
{
	var $B;
	var $I;
	var $U;
	var $HREF;

	function PDF($orientation='P',$unit='mm',$format='A4')
	{
	    //Call parent constructor
	    $this->FPDF($orientation,$unit,$format);
	    //Initialization
	    $this->B=0;
	    $this->I=0;
	    $this->U=0;
	    $this->HREF='';
	}
	
	function WriteHTML($html)
	{
	    //HTML parser
	    $html=str_replace("\n",' ',$html);
	    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	    foreach($a as $i=>$e)
	    {
	        if($i%2==0)
	        {
	            //Text
	            if($this->HREF)
	                $this->PutLink($this->HREF,$e);
	            else
	                $this->Write(5,$e);
	        }
	        else
	        {
	            //Tag
	            if($e{0}=='/')
	                $this->CloseTag(strtoupper(substr($e,1)));
	            else
	            {
	                //Extract attributes
	                $a2=explode(' ',$e);
	                $tag=strtoupper(array_shift($a2));
	                $attr=array();
	                foreach($a2 as $v)
	                {
	                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
	                        $attr[strtoupper($a3[1])]=$a3[2];
	                }
	                $this->OpenTag($tag,$attr);
	            }
	        }
	    }
	}
	
	function OpenTag($tag,$attr)
	{
	    //Opening tag
	    if($tag=='B' or $tag=='I' or $tag=='U')
	        $this->SetStyle($tag,true);
	    if($tag=='A')
	        $this->HREF=$attr['HREF'];
	    if($tag=='BR')
	        $this->Ln(5);
	}
	
	function CloseTag($tag)
	{
	    //Closing tag
	    if($tag=='B' or $tag=='I' or $tag=='U')
	        $this->SetStyle($tag,false);
	    if($tag=='A')
	        $this->HREF='';
	}
	
	function SetStyle($tag,$enable)
	{
	    //Modify style and select corresponding font
	    $this->$tag+=($enable ? 1 : -1);
	    $style='';
	    foreach(array('B','I','U') as $s)
	        if($this->$s>0)
	            $style.=$s;
	    $this->SetFont('',$style);
	}
	
	function PutLink($URL,$txt)
	{
	    //Put a hyperlink
	    $this->SetTextColor(0,0,255);
	    $this->SetStyle('U',true);
	    $this->Write(5,$txt,$URL);
	    $this->SetStyle('U',false);
	    $this->SetTextColor(0);
	}
	
	//--- MAKE TABLES EASILY
	//Load data
	function LoadData($file)
	{
	    //Read file lines
	    $lines=explode("\r\n",$file);
	   //  print_R($lines);
	    $data=array();
	    foreach($lines as $line)
	        $data[]=explode(';',chop($line));
	     //   print_R($data);
	    return $data;
	}
	
	/*Simple table
	function BasicTable($header,$data)
	{
	    //Header
	    foreach($header as $col)
	        $this->Cell(40,7,$col,1);
	    $this->Ln();
	    //Data
	    foreach($data as $row)
	    {
	        foreach($row as $col)
	            $this->Cell(40,6,$col,1);
	        $this->Ln();
	    }
	}*/
	
	/*Better table
	function ImprovedTable($header,$data)
	{
	    //Column widths
	    $w=array(10,70,20,20);
	    //Header
	    for($i=0;$i<count($header);$i++)
	        $this->Cell($w[$i],7,$header[$i],1,0,'C');
	    $this->Ln();
	    //Data
	    foreach($data as $row)
	    {
	        $this->Cell($w[0],6,$row[0],'LR');
	        $this->Cell($w[1],6,$row[1],'LR');
	        $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R');
	        $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R');
	        $this->Ln();
	    }
	    //Closure line
	    $this->Cell(array_sum($w),0,'','T');
	}*/
	
	//Colored table
	function FancyTable($header,$data)
	{
	    //Colors, line width and bold font
	    $this->SetFillColor(112,128,144);
	    $this->SetTextColor(255);
	    $this->SetDrawColor(105,105,105);
	    $this->SetLineWidth(.3);
	    $this->SetFont('','B');
	    //Header
	    $w=array(10,50,25);
	    for($i=0;$i<count($header);$i++)
	        $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
	    $this->Ln();
	    //Color and font restoration
	    $this->SetFillColor(224,235,255);
	    $this->SetTextColor(0);
	    $this->SetFont('');
	    //Data
	    $fill=false;
	    foreach($data as $row)
	    {
	        $this->Cell($w[0],6,$row[0],'LR',0,'C',$fill);
	        $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
	        $this->Cell($w[2],6,number_format($row[2], 2, '.', ','),'LR',0,'C',$fill);
	       // $this->Cell($w[3],6,number_format($row[3], 2, '.', ','),'LR',0,'C',$fill);
	        $this->Ln();
	        $fill=!$fill;
	    }
	    //print_R($w);
	    $this->Cell(array_sum($w),0,'','T');
	}

	function Footer(){
		$this->SetY(-30);
		$this->SetTextColor(0);
    $this->SetFont('Arial','B',8);
    $this->Cell(0,3,'MBoard Media SDN BHD (793604-D)',0,1);
    $this->Image('footer.png',160,275,40,10);   
    
   // $this->SetFont('Arial','B',6);
    //$this->Cell(0,3,'GET NOTICED',0,1);
    
    $this->SetFont('Arial','',6);
    $this->Cell(0,3,'#16.01, 16th Floor Campbell Complex .	98, Jalan Dang Wangi 50100 Kuala Lumpur',0,1);
    
    $this->SetFont('Arial','',6);
    $this->Cell(0,3,'Tel: (603) 2698 9399   Fax: (603) 2693 1399 ',0,1);
    
    $this->SetFont('Arial','',6);
    $this->Cell(0,3,'Email: blufroge@gmail.com       Website: www.blufroge.com ',0,1);
    
    $this->SetFont('Arial','I',8);  
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}

}
