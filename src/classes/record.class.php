<?php


include_once(DIR_CLASSES.'/pagination.class.php');

//###################################################
//  Limitations/Constraints 
//###################################################
/*
1. The first field in a table is assumed as the PK or a unique ID. (e.g. nwsId)
2. Format for sorting for constructor looks like "<fieldname><space><ASC/DESC>,..." 
   (e.g. "spoDate DESC, spoTitle")
3. INNER JOINs are not supported by this class. Use a customized class.
4. Conditions for selective query can be added as needed or use a customized class.
*/

//###################################################
//  Reminders / Notes
//###################################################
// The class inheritance relationships is represented below:
// DB_Sql <--- Record <--- Agenda(optional) <--- Customized Class (e.g. News)

//###################################################
//  Prototype Functions
//###################################################
/*
BO FUNCTIONS:
===============
null DefineRecord();
int GetPage(int page, string sortby)
int SetTotalRecord();
int GetRecordList(string sortby);
int GetRecordById(string id);
int InsertRecord();
int UpdateRecord(string id);
int DeleteRecord(string id);

FO FUNCTIONS:
===============
int GetLatestRecord(string sortby);
int GetRecords(string sortby);

BO/FO FUNCTIONS:
===================
int GetNextRecord();
null SetRecord();
null Set();
data Get();
*/

class Record {

	//###################################################
	// Mandatory system properties
	//###################################################
	public $Host      = '';
	public $Database  = '';
	public $User      = '';
	public $Password  = '';
	public $dirpic    = '';
	public $diraudio  = '';
	public $dirvideo  = '';
	public $dirflash  = '';
	public $dblink    = '';
	public $language  = '';	// language type
	public $tablename = '';
	public $bosslang  = '';	// boss language for the site

	//###################################################
	//  Properties for paging system
	//###################################################
	public $currentpage  = 0;
	public $displaynext  = 0;
	public $displayprev  = 0;
	public $totalrecord  = 0;
	public $totallist    = 0;
	public $numperpage   = 15;	// Default to 20
	public $numfirstpage = 10;	// Default to 6
	public $pagelink = '';
	
	public $Field    = array();
	public $Property = array();

	// default sorting criteria initialized by constructor
	public $default_sort = "";
	
	public $data	= array();


	//###################################################
	//  Define and set data properties
	//###################################################
	public function DefineRecord() {
		if(trim($this -> tablename) == '') {
			trigger_error('Define Record only can be called by parent class', E_USER_ERROR);
		}

		$query = 'SELECT * FROM ' . $this->tablename . ' LIMIT 0';

		if ($this->query_id = $this->db->query($query)) {
			for ($x = 0; $x < mysql_num_fields($this->query_id); $x++) {
				// populate array with fieldnames
				$field_name = mysql_field_name($this->query_id,$x);
				array_push($this->Field, $field_name);

				$property_type = mysql_field_type($this->query_id, $x);
				switch($property_type) {

					case 'int':
						$this->data[$field_name] = 0;
						break;

					case 'datetime':
					case 'date':
					case 'time':
						$this->data[$field_name] = '';
						break;

					case 'real':
						$this->data[$field_name] = '0.0';
						break;	

					default:
						$this->data[$field_name] = '';
				}
			}
		}
	}

	//###################################################
	// To get the latest record
	//###################################################
	public function GetLatestRecord($sortby = '') {
		if(trim($this -> tablename) == '') {
			trigger_error('Get Latest Record only can be called by parent class', E_USER_ERROR);
		}

		$query = 'SELECT * FROM ' . $this->tablename;

		// sorting
		if ($sortby != '') {
			$query .= ' ORDER BY ' . $sortby;
		}
		else if ($this->default_sort != '') {
			$query .= ' ORDER BY ' . $this->default_sort;
		}

		$query .= ' LIMIT 1';

		if ($this->query_id = $this->db->query($query)) {
			if ($this->GetNextRecord()) {
				return 1;
			}
			else {
				return 0;
			}
		}
		else {
			return 0;
		}
	}

	//###################################################
	//      Skip to next record in results returned
	//###################################################
	public function GetNextRecord() {
		if ($this->db->next_record($this->query_id)) { 
			$this->data = $this->db->Record;
			return 1;
		}
		else {
			return 0;
		}
	}

	//###################################################
	//      Return Record by Id
	//###################################################
	public function GetRecordById($id = '') {
		if(trim($this -> tablename) == '') {
			trigger_error('Get Record By ID only can be called by parent class', E_USER_ERROR);
		}

		if ($id != '') {
			$query = "SELECT * FROM $this->tablename WHERE " . $this->Field[0] . " = '" . $id . "'";
			
			if ($this->query_id = $this->db->query($query)) {
				if ($this->GetNextRecord()) { 
					return 1;
				}
				else {
					return 0;
				}
			}
			else {
				return 0;
			}
		}
		else {
			return 0;
		}
	}

	//###################################################
	// To insert the data into the table
	//###################################################
	public function InsertRecord($start_index=1, $debug=false) {
		if(trim($this -> tablename) == '') {
			trigger_error('Insert Record only can be called by parent class', E_USER_ERROR);
		}

		$startIndex = $start_index;
		$total_fields = count($this->Field);

		// construct a string of field names
		if ($total_fields > 0) {
			$field_list = "";

			for ($i = $startIndex; $i < $total_fields; $i++) {
				$field_list .= '`' . $this->Field[$i] . '`';
				if ($i != $total_fields - 1)
					$field_list .= ', ';
			}

			// construct a string of field values
			$field_values = "";
			for ($i = $startIndex; $i < $total_fields; $i++) {
				if (isset($this->date) && $this->date == '')
					$this->date = date("Ymd H:i:s");
	
				$field_values .= '"' . $this->data[$this->Field[$i]] . '"';
				if ($i != $total_fields - 1)
					$field_values .= ', ';
			}
		}
		
		$query = 'INSERT INTO ' . $this->tablename . '(' . $field_list . ') VALUES (' . $field_values . ')';

        
		if($debug) { echo "<hr/>$query<hr/>"; }
		
		if ($this->query_id = $this->db->query($query)) {
			$this->data[$this->Field[0]] = $this->db->insert_id();
			return 1;
		}
		else {
			return 0;
		}
	}

	//###################################################
	// To update record
	//###################################################
	public function UpdateRecord($id, $debug=false) {
		if(trim($this -> tablename) == '') {
			trigger_error('Update Record only can be called by parent class', E_USER_ERROR);
		}

		if ($id != '') {

			$total_fields = count($this->Field);
			if ($total_fields > 0) {
				// construct a string of paired fieldname-values
				$str = '';
				for($i = 1; $i < count($this->Field); $i++) {
					$str .= '`' . $this->Field[$i] . '` = ' . '"' . $this->data[$this->Field[$i]] . '"';
					if($i != $total_fields-1) {
						$str .= ', ';
					}
				}
			}

			$query = "UPDATE $this->tablename SET $str WHERE " . $this->Field[0] . " = '$id'";
			
			if($debug) { echo "<hr/>$query<hr/>"; }

			if ($this->query_id = $this->db->query($query))  {
				return 1;
			}
			else {
				return 0;
			}
		}
		else {
			return 0;
		}
	}

	//###################################################
	// To delete record
	//###################################################
	public function DeleteRecord($id = '') {
		if(trim($this -> tablename) == '') {
			trigger_error('Delete Record only can be called by parent class', E_USER_ERROR);
		}

		if ($id != '') {
			$query = "DELETE FROM $this->tablename WHERE " . $this->Field[0] . " = '$id'";
			
			if ($this->query_id = $this->db->query($query)) {
				return 1;
			}
			else {
				return 0;
			}
		}
		else {
			return 0;
		}
	}
	
//--Added by Chee Leong 04/05/2005 - for use of Pagination class
	//###################################################
	// To get all records for FO display
	//###################################################
	public function GetRecordOriginal($page_no=0,$sortby = '') {

		$query = 'SELECT * FROM ' . $this->tablename;

		// sorting
		if ($sortby != '') {
			$query .= ' ORDER BY ' . $sortby;
		}
		else if ($this->default_sort != '') {
			$query .= ' ORDER BY ' . $this->default_sort;
		}

		if($page_no!=0) {				
			$this->GeneratePage($page_no, $query);
			$this->Set('num_row',$this->page->total_list);	
		} else {	//bypass pagination
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return $this->db->num_rows();
			}
			return 0 ;
		}
		
	}
//-- Modified by chua
	//###################################################
	// To get all records for FO display
	//###################################################

	public function GetRecord($page_no=0,$sortby = '', $condition = null) {
		if(trim($this -> tablename) == '') {
			trigger_error('Get Record only can be called by parent class', E_USER_ERROR);
		}

		$query = 'SELECT * FROM ' . $this->tablename;
		
		if($condition != '') {
			$query .= ' ' . $condition;
		}
		
		// sorting
		if ($sortby != '') {
			$full_query = $query . ' ORDER BY ' . $sortby;
		}
		else if ($this->default_sort != '') {
			$full_query = $query . ' ORDER BY ' . $this->default_sort;
		} else {
			$full_query = $query;
		}
		if($page_no!=0) {				
			$this->GeneratePage($page_no, $full_query, $query);
			$this->Set('num_row',$this->page->total_list);	
		} else {	//bypass pagination
			if($this->query_id = $this->db->query($full_query)){
				$this->Set('num_row',$this->db->num_rows());
				return $this->db->num_rows();
			}
			return 0 ;
		}
		
	}
	//==============================================================================================
	//  PAGING - Description:
	//  To initialize a paging object to handle to pagination for record retrieval
	//  $sql parameter should contain the full SELECT statement including ORDER BY if required, without LIMIT options
	//==============================================================================================
	public function GeneratePage($page_no, $sql, $sql_total = null) {

		if (isset($this->page)) {
			unset($this->page);
		}

		$this->page = new Pagination($this->db, $sql, $this->pagelink);
		$this->page->num_per_page = $this->numperpage;
		$this->page->num_first_page = $this->numfirstpage;
		$this->page->sql_total = $sql_total;
		$this->query_id = $this->page->GetPage($page_no);
	}

	//==============================================================================================
	//  GET DATA - Description:
	//  Getter method for getting data array values
	//==============================================================================================
	public function Get($field_name) {
		if(isset($this->data[$field_name])) {
			return $this->data[$field_name];
		} else {
			return "";
		}		
	} // end function
	
	//==============================================================================================
	//  SET DATA - Description:
	//  Setter method for setting data array values
	//==============================================================================================
	public function Set($field_name, $field_value) {
		$this->data[$field_name] = $field_value;
	} // end function
	
	/**
	*
	* set data using array
	*/
	public function SetArray($arrData) {
		if(is_array($arrData) && !empty($arrData)) {
			foreach($arrData as $field => $val) {
				$this -> data[$field] = $val;
			}
		}
	}
	
//--End: Addition	
	
}
?>