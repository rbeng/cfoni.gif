<?php
class Cache {
	var $path;
	var $filename;
	var $tempfilename;
	var $expiration;
	var $fp;
	
	function Cache($path, $file, $ttl=false) {
		$this->path = $path;
		$this->filename = $path .'/'. $file;
		$this->tempfilename = "$this->filename.".getmypid();
		
		$quantum = substr($ttl, -1);
		switch ($quantum) {
		case "s":
			$this->expiration = (int) substr($ttl,0,-1);
			break;
		case "m":
			$this->expiration = (int) substr($ttl,0,-1) * 60; // 60 secs in 1 minute
			break;
		case "h":
			$this->expiration = (int) substr($ttl,0,-1) * 3600; // 60 mins x 60 secs in 1 hour
			break;
		case "d":
			$this->expiration = (int) substr($ttl,0,-1) * 86400; // 24 hours x 60 mins x 60 secs in 1 day
			break;
						
		default:
			$this->expiration = false;
		}
	}
	
	// Prepare Cache directory
	// if not exists create one
	function prepare_dir() {
		if(trim($this->path) == '') 
			return false;
			
		// original path
		$arrPath = explode('/', $this->path);
		
		$spath = null;
		$no = count($arrPath);
		$check = false;
		
		// check each directory one by one
		for($i=1; $i<$no; $i++) {
			$spath .= '/' . $arrPath[$i];
			
			// b4 doc root, we do not have access to read
			// so we must start after cache
			if($spath == DIR_CACHE)
				$check = true;
			
			if($check === true) {
				if(!file_exists($spath)) {
					mkdir($spath);
					chmod($spath, 0755);
				}
			}
		}
	}
	
	function put($buffer) {
		if (($this->fp = fopen($this->tempfilename, "w")) == false) {
			return false;
		}
		fwrite($this->fp, $buffer);
		fclose($this->fp);
		rename($this->tempfilename, $this->filename);
		return true;
	}
	
	function get() {
		if ($this->expiration) {
			if (file_exists($this->filename)) {
				$stat = @stat($this->filename);
				if ($stat[9]) {
					if (time() > $stat[9] + $this->expiration) {
						if (file_exists($this->filename)) {
							//@unlink($this->filename);
							return false;
						}
					}
				}
			}
			else
			 return false;
		}
		if (file_exists($this->filename)) {
			return @file_get_contents($this->filename);
		}
		else {
			return false;
		}
	}
	
	function remove() {
		@unlink($this->filename);
	}
	
	function begin_org() {
		$this -> prepare_dir();
		/*
		if (!file_exists($this->path)) {
			mkdir($this->path);
			chmod($this->path, 0755);
		}
		*/
		if (($this->fp = fopen($this->tempfilename, "w")) == false) {
			return false;
		}
		ob_start();
	}
	
	function begin() {
		$this -> prepare_dir();
		ob_start();
	}
	
	function finish($mode = null) {
		$buffer = ob_get_contents();
		ob_end_clean();
		
		// by default we will echo the cache content
		// if mode = var, we will return the cache content as variable
		if($mode != 'var') {
			echo $buffer;
		}
		
		// If cache content is not empty
		if (strlen($buffer)) {
			
			if(USE_CACHE == true && ($this -> expiration != false || $this -> expiration != '' ) ) {
				if($this->fp = fopen($this->tempfilename, "w")) {
					fwrite($this->fp, $buffer);
					fclose($this->fp);
					rename($this->tempfilename, $this->filename);
				}
			}
			
			//echo "rename=".$ok;
			if($mode == 'var')
				return $buffer;
			else {
				return true;
			}
		} else {
			//fclose($this->fp);
			if(file_exists($this->tempfilename)) {
				unlink($this->tempfilename);
			}
			return false;
		}
	}

	
/*
	function finish($mode = null) {
		$buffer = ob_get_contents();
		ob_end_clean();
		
		if($mode != 'var')
			echo $buffer;
		
		if (strlen($buffer)) {
			
			if($this->fp = fopen($this->tempfilename, "w")) {
				fwrite($this->fp, $buffer);
				fclose($this->fp);
				rename($this->tempfilename, $this->filename);
			}
			
			//echo "rename=".$ok;
			if($mode == 'var')
				return $buffer;
			else {
				return true;
			}
		} else {
			fclose($this->fp);
			unlink($this->tempfilename);
			return false;
		}
	}
*/
	function finish_org($mode = null) {
		$buffer = ob_get_contents();
		ob_end_clean();
		
		if($mode != 'var')
			echo $buffer;
		
		if (strlen($buffer)) {
			fwrite($this->fp, $buffer);
			fclose($this->fp);
			rename($this->tempfilename, $this->filename);
			//echo "rename=".$ok;
			if($mode == 'var')
				return $buffer;
			else {
				return true;
			}
		}
		else {
			fclose($this->fp);
			unlink($this->tempfilename);
			return false;
		}
	}
}
?>