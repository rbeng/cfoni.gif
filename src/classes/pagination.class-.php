<?php

class Pagination {

	public $db;
	public $page_query;
	public $total_record;
	public $current_page;
	public $num_per_page;
	public $num_first_page;
	public $total_list;
	public $query_id;
	public $page_link;
	public $prev_link;
	public $next_link;
	public $link_array = array();
	
	// added by chua, query to check total no. of record without order by
	public $sql_total;
	
	// Constructor 
	// Note that $link is used to form the URL of the next & prev buttons as well as all the page links.
	// If no link is passed in, it will use the URL of the current page automatically
	function Pagination(&$db, $query, $link='') {
	
		$this->db = &$db;
		$this->page_query = $query;
		if ($link == '')
			$this->page_link = $_SERVER['REQUEST_URI'];
		else
			$this->page_link = $link;
		//echo $this->page_link;
	}

	// Used to get the total records without setting LIMIT
	public function SetTotalRecord_original() {
		if ($this->query_id = $this->db->query($this->page_query)) {
			 $this->total_record = $this->db->num_rows();
			 return 1;
		}
		return 0;		
	}
	// Used to get the total records without setting LIMIT
	public function SetTotalRecord() {
		if($this -> sql_total != '')
			$sql = $this -> sql_total;
		else
			$sql = $this -> page_query;
			
		if ($this->query_id = $this->db->query($sql)) {
			 $this->total_record = $this->db->num_rows();
			 return 1;
		}
		return 0;		
	}

	// Used to get the recordset for the particular page
	public function GetRecordList() {
	
		if ($this->current_page == 1)
			$offset = 0;
		else
			$offset = ($this->current_page * $this->num_per_page) - ($this->num_per_page +  ($this->num_per_page - $this->num_first_page));
		 
		if ($this->current_page == 1)
			$this->page_query .= " LIMIT $offset, $this->num_first_page";
		else
			$this->page_query .= " LIMIT $offset, $this->num_per_page";
		
		//echo "<hr>$this->page_query<hr>"; // testing

		if ($this->query_id = $this->db->query($this->page_query)) {
			$this->total_list = $this->db->num_rows();
			return 1;
		}
	
		return 0;
	
	} // end function
	
	// Used to calculate the total number of pages available
	public function CalcTotalPages() {
	
		$this->total_pages = 1;
		if ($this->total_record > $this->num_first_page) {
			$tmp = $this->total_record - $this->num_first_page;
			$this->total_pages += (int) ($tmp / $this->num_per_page);
			if ($tmp % $this->num_per_page > 0)
				$this->total_pages++;		
		}
		return;
	}
	
	//#####################################################################################
	// Get the next/prev page's record list
	// Must pass in 'next' or 'prev' parameter, also must call setTotalRecord() first.
	// Property numperpage must be set first
	//#####################################################################################
	
	public function GetPage($page=1) {
	
	  if ($page < 0)
		return 0;
		
	  // To strip the page URL off the page parameter if it exist in the URL.
	  $url_array = parse_url($this->page_link);
	  $url_params = isset($url_array['query']) ? explode("&", $url_array['query']) : array();
	  //print_r($url_params);
	  $url = '';
	  foreach ($url_params as $key => $value) {
	  //	echo "checking...".$value."<br>";
		$pos = strpos($value, 'page=');
	   	if ($pos === false) {
		//	echo "adding..".$value."<br>";
			$url .= $value . '&amp;';
		}
	  }
	//  echo 'url='.$url;
	  $this->page_link = $url_array['path'].'?'.$url;
	  
	  // To add the new page param back to the end of the URL
	  $link = $this->page_link . 'page=';
	
	  $this->current_page = $page;
	  $this->SetTotalRecord();
	  $this->GetRecordList();
	  $this->CalcTotalPages();
	  
	  //echo $this->total_record;
	  
	  // Working out the link for next button
	  if ($this->current_page == 1) {
		  if ($this->total_record - $this->num_first_page > 0)
			$this->next_link = $link . ($this->current_page + 1);
		  else
			$this->next_link = '';
	  }
	  else {
		  if (($this->total_record - ($this->num_per_page * $this->current_page - ($this->num_per_page - $this->num_first_page))) > 0)
			$this->next_link = $link . ($this->current_page + 1);	
		  else
			$this->next_link = '';
	  }
	  
	  // Working out the link for previous button
	  if (($this->current_page > 1) && ($this->total_list > 0))
		$this->prev_link = $link . ($this->current_page - 1);
	  else
		$this->prev_link = '';
		
	  // Working out the links for each of the pages
	  for ($i=1; $i<=$this->total_pages; $i++) {
	  	if ($i != $this->current_page)
		  	$this->link_array[$i] = $link . $i;
		else
			$this->link_array[$i] = '';
	  }
	
	  return $this->query_id; 
	} // end function
	
	// Use to render pagination list
	public function renderPageList($type = 'normal', $seperator = '', $class = '') {
		$strCode = null;
		
		if($type == 'normal')
			$strCode .= 'Page: ';
			
		if ($this->prev_link != '' && $type == 'normal') {
			$strCode .= '<a href="'. $this->prev_link . '" class="'.$class.'">Prev</a> ';
		}
		
		foreach ($this->link_array as $key=>$value) {
			if ($value == "") {
				$strCode .= '<span class="paginationTextSelected">'.$key . '</span> ' . $seperator . ' ';
			} else {
				$strCode .= '<a href="'.$value.'" class="'.$class.'">'.$key.'</a> ' . $seperator . ' ';
			}
		}
		if($key > 1)
			$strCode = substr($strCode, 0, -2);
		else
			$strCode = substr($strCode, 0, -2);
		
		if ($this->next_link != '' && $type == 'normal') {
			$strCode .= ' <a href="'. $this->next_link .'" class="'.$class.'">Next</a>';
		}
		return $strCode;
	}
	
	
	
	
	public function renderPageListAds($type = 'normal', $seperator = '', $class = '',$date='') {
		$strCode = null;
		
		if($type == 'normal')
			$strCode .= 'Page: ';
			
		if ($this->prev_link != '' && $type == 'normal') {
			$strCode .= '<a href="'. $this->prev_link .'&strDate='.$date.'" class="'.$class.'">Prev</a> ';
		}
		
		foreach ($this->link_array as $key=>$value) {
			if ($value == "") {
				$strCode .= '<span class="paginationTextSelected">'.$key . '</span> ' . $seperator . ' ';
			} else {
				$strCode .= '<a href="'.$value.'&strDate='.$date.'" class="'.$class.'">'.$key.'</a> ' . $seperator . ' ';
			}
		}
		if($key > 1)
			$strCode = substr($strCode, 0, -2);
		else
			$strCode = substr($strCode, 0, -2);
		
		if ($this->next_link != '' && $type == 'normal') {
			$strCode .= ' <a href="'. $this->next_link .'&strDate='.$date.'" class="'.$class.'">Next</a>';
		}
		return $strCode;
	}
	
	
	
	
	
	
}

?>