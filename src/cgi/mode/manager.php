<?php
require_once ROOT_PATH . "src/core/core_manager.php";
require_once ROOT_PATH . "/tcpdf/config/lang/eng.php";
require_once ROOT_PATH . "/tcpdf/tcpdf.php";

class manager {

  function __construct() {
    global $CONF, $Q, $OUT, $DB, $LANG, $MailClass, $MailHintClass, $MailClassSecond, $MailClassThird;

    // Language directoty	
    //$this->AccID = $Q->cookies['account:intAccId'];
    $LANG->load('admin');
	
  
    if (!array_key_exists('c', $Q->req)) {
      $Q->req['c'] = 'show_main';
    }
    //
    // Authentication	 //
		{
      //
      // Command that don't need authentication
      //
		$no_auth = array(
          'show_login'    => '',
		  'do_login'      => ''
		
      );

      if (!array_key_exists($Q->req['c'], $no_auth)) {
        if (!$this->doAuth()) {


          $param = array();
          foreach (array_keys($Q->req) as $key) {
            if (!preg_match('/manager:/i', $key)) {
              array_push($param, $key . '=' . $Q->req[$key]);
            }
          }
          $good_param = join("&", $param);
          //$intCheckout = $Q->req['intCheckout'];
          $return = $CONF['url_app'] . '?' . $good_param;
          $OUT->addCookie('account:Return', $return);
          $this->show_error('err_auth');
        }
      }
    }

  
  }

  public function _run() {
    global $CONF, $Q, $OUT, $LANG;
    $cmd = $Q->req['c'];
    $valid_cmd = array(
        'show_main'     => null,
		'show_login'    => null,
		'do_login'      => null,
		'do_logout'     => null,
		'show_salesman' => null,
		'show_salesman_form' => null,
		'do_add_salesman'    => null,
		'show_profile'       => null,
		'do_update_manager'  => null,
		'show_salesman_details'   => null,
		'do_update_salesmen'      => null,
		'do_delete_salesmen'      => null,
		
		
    );
    //
    // Setting default cmd if not found
    //
	 if (!array_key_exists($cmd, $valid_cmd)) {
      $cmd = 'show_login';
    }
    //
    // Executing the command
    //
		$this->$cmd();
  }

    //
	// Show main
	//
	function show_main(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_main());
		$OUT->flush();
	}
   
    //
	// Page main
	//
	function page_main(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		return $this->_temp('shw_main.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Login Page
	//
	function show_login(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_login());
		$OUT->flush();
	}
	//
	// Page Login 
	//
	function page_login(){
		global $CONF, $Q, $OUT, $DB,$LANG;
				
		$error = $OUT->errors;
  	    return $this->_output(DIR_TEMP_PATH_MANAGER . '/manager/login.php',$data,$error);
  		
	}	
	//
	// Do Login 
	//
	function do_login(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;
			
		
		if($Q->req['mustkeyin']!=""){
			data_load($CONF['dir_data']."blocklist.txt", $data);
			data_load($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			$cData = count($data);
			if($cData > 0){
				$blockid = $cData + 1;
			}
			else{
				$blockid = 1;
			}
			$data[$blockid] = $_SERVER['REMOTE_ADDR']." ".date('Y-m-d H:s');
			$dataip[$blockid] = $_SERVER['REMOTE_ADDR'];
			data_save($CONF['dir_data']."blocklist.txt", $data);
			data_save($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			//
			// Inform Server PPl some hacking is being try , please block this IP ASAP
			//
			$strTitle   = 'Caution [ Account Manager Page ]: Hacking Back End System';
			$strContent  = 'This IP ('.$_SERVER['REMOTE_ADDR'].') address  try to hack the Back End System , please take the action to block this IP ASAP.';
			include('email_tmpl/block.php');
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $CONF['administration_email'] );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;
 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			} 	
			exit;
		 }	
    	
	$user_id  = $Q->req['user_id'];
	$user_pwd = md5($Q->req['user_pwd']);
		
    $this->objMember = new core_manager();
    $this->objMember->getLoginMember($user_id);
	$this->objMember->GetNextRecord();
	$data['strPassword']         = $this->objMember->Get('strPass');
	$data['strUser']             = $this->objMember->Get('strUser');
	$data['intAccountManagerId'] = $this->objMember->Get('intAccountManagerId');

	if(!isset($data) || $user_pwd != $data['strPassword']){
		$OUT->addError('Access Denied!'); 
		$this->show_error('err_login');
		exit;
	}
	else{
	  $session = session_get();
    
	  $OUT->addCookie('manager:Session', $session);
	  $OUT->addCookie('manager:strPass', $data['strPassword']);
	  $OUT->addCookie('manager:strUser', $data['strUser']);
	  $OUT->addCookie('manager:intAccountManagerId', $data['intAccountManagerId']);

      $this->objSession = new core_manager();
	  $this->objSession->deleteRecord($data['strUser']);
	  $this->objSession = new core_manager();
	  $this->objSession->addRecord($data['strUser'],$session);
	  
	  //
	  //To get last login time
	  //
	  $intAccManagerId = $data['intAccountManagerId'];
	  $this->obj = new core_manager();
	  $this->obj->getLastlogin($intAccManagerId);
	  $this->obj->GetNextRecord();
	  $strNowLogin  = $this->obj->Get('strNowLogin');
	  $strLastLogin = $this->obj->Get('strLastlogin');
	  
	  if($strNowLogin=="" && $strLastLogin==""){
		 //
		 // Update Both First Time
		 //
		 $this->objUpdateBoth = new core_manager();
		 $this->objUpdateBoth->doUpdateBothLogin($intAccManagerId); 
	  } 
	  else{
	  //
	  // Update Now login
	  //
	 
	  $this->objlastlogin = new core_manager();
	  $this->objlastlogin->doUpdateLastlogin($intAccManagerId,$strNowLogin); 
		  
	  }
	  
	  $OUT->redirect($CONF['url_app'] . "?m=manager&c=show_main");
	 }
	}
	
	//
  	// Do Logout
  	//
	function do_logout() {
    	global $CONF, $Q, $OUT, $DB, $DBSH;
	
	    $strUser = $Q->cookies['manager:strUser'];
    	$this->objSession = new core_manager();
    	$this->objSession->deleteRecord($strUser);
    	$OUT->addCookie('manager:strPass', '', time() - 3600);
    	$OUT->addCookie('manager:strUser', '', time() - 3600);
    	$OUT->addCookie('manager:strEmail', '', time() - 3600);
    	$OUT->addCookie('manager:intAccountManagerId', '', time() - 3600);
    
        $OUT->redirect($CONF['url_app'] . "?m=manager");
  	}
	
	
  	//
  	// Do authentication
  	//
	function doAuth() {
    	global $CONF, $Q, $OUT, $DB, $LANG;

    	if (array_key_exists('manager:Session', $Q->cookies)) {
      	
			$Session = $Q->cookies['manager:Session'];
      		$strUser = $Q->cookies['manager:strUser'];
		    $data = array();
     	    // find user's session
      		$this->objSession = new core_manager();
      		$found = $this->objSession->retrieveSession('accountmanager_session', $strUser, $Session);
      		if (is_array($found)) {
        		return true;
      		} else {
        		return false;
      		}
    	}
  	}
	//
	// Show All Salesman By Account Manager
	//
	function show_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_salesman());
		$OUT->flush();
	}
	//
	// Page Salesman
	//
	function page_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;
		
		$intAccManagerId = $Q->cookies['manager:intAccountManagerId'];
		//
		// Get All Salesman
		//
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objSalesman = new core_manager();
		$this->objSalesman->numperpage   = 30;
		$this->objSalesman->numfirstpage = 30;
		$this->objSalesman->doGetAllSalesmanByAccManagerId($intPage,$intAccManagerId);
	
		while($this->objSalesman->GetNextRecord()){
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['intSalesId']   = $this->objSalesman->Get('intSalesId'); 
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strSalesCode'] = $this->objSalesman->Get('strSalesmanCode');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['intType']      = $CODE['prefix_salesman'][$this->objSalesman->Get('intType')];
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['CreateDate']   = $this->objSalesman->Get('CreateDate'); 
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['intStatus']    = $this->objSalesman->Get('intStatus'); 
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strEmail']      = $this->objSalesman->Get('strEmail');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strFullName']   = $this->objSalesman->Get('strName');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strUser']       = $this->objSalesman->Get('strUser');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strContact']    = $this->objSalesman->Get('strContact');
		}
			
		
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objSalesman->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		return $this->_temp('shw_salesman_lists.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Salesman Form
	//	
	function show_salesman_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_salesman_form());
		$OUT->flush();
		
	}
	//
	// Page Salesman Form
	//
	function page_salesman_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		return $this->_temp('shw_salesmen_form.php','NULL','NULL',$error,$data);	
		
	} 
	//
	// Do Add Salesman
	//
	function do_add_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE,$MailClass;
		
		
		$strFullName  = $Q->req['strFullName'];
		$strEmail     = $Q->req['strEmail'];
		$strContact   = $Q->req['strContact'];
		$strUsername  = $Q->req['strUsername'];
		$strPassword  = $Q->req['strPassword'];
		$intAutoEmail = $Q->req['intAutoEmail'];
		$strCommisionRate  = $Q->req['strCommisionRate'];
		$intType      = $Q->req['intType'];
		$intAccountManagerId   = $Q->cookies['manager:intAccountManagerId'];
		if($intAutoEmail==""){
			$intAutoEmail = 0;
		}
		$strType  =  $CODE['prefix_salesman'][$intType];
		
		
		//
		// Username Exists Or Not
		//
		if($strUsername!=""){
		$this->objcheck = new core_manager();
		$this->objcheck->doCheckUsername($strUsername);
		$this->objcheck->GetNextRecord();
		$strDBUsername = $this->objcheck->Get('strUser');
		 if($strDBUsername==$strUsername){
			$OUT->addError('Username already exists.');	
		  }
		}
		else{
			$OUT->addError('Username cannot be empty.');	
		}
		
		if($strEmail==""){
			$OUT->addError('Email Address cannot be empty.');	
		}
		else{
			
			if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
			}
			$this->objcheckemail = new core_manager();
			$this->objcheckemail->doCheckEmail($strEmail);
			$strDBEmail = $this->objcheckemail->Get('strEmail');
		   if($strDBEmail==$strEmail){
			 $OUT->addError('Email Address already exists.');	
		   }
	    }
		if($strPassword==""){
			$OUT->addError('Passwrod cannot be empty.');	
		}
		if($strContact==""){
			$OUT->addError('Contact No. cannot be empty.');	
		}
	
	   if($OUT->isError()){
			$this->show_error('err_add_salesmam');
			exit;
		}
		//
		// Do Insert Salesman
		//
		$this->obj = new core_manager();
		$id = $this->obj->doAddSalesman(array(
		 'strFullName' => $strFullName,
		 'strEmail'    => $strEmail,
		 'strContact'  => $strContact,
		 'strUsername' => $strUsername,
		 'strPassword' => $strPassword,
		 'intType'     => $intType ,
		 'strCommisionRate' => $strCommisionRate ,
		 'intAccountManagerId' => $intAccountManagerId
		 )
		);
		
		
			if($id > 0 ){
			$strCode = getSalesmanCode($id,$strType);
			$strCombineCode = $strType.$strCode;
			//
			// Update Salesman Code
			//
			$this->objsalesman = new core_manager();
			$this->objsalesman->doUpdateSalesman($strCode,$id);
	
			if($intAutoEmail==1){
			//
			// Inform New Salesman
			//
			$strTitle   = 'Welcome : New Salesman';
			$strContent  = 'Your account details as below <br> Salesman Code :'.$strCombineCode.' <br> Username :'.$strUsername.' <br> Password :'.$strPassword.'<br> URL : <a href='.$CONF['url_app'].'?m=salesman>'.$CONF['url_app'].'?m=salesman'.'<a>';
			
			include('email_tmpl/welcome_accountmanager.php');
			
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $strEmail  );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			 }
			}
			$OUT->redirect($CONF['url_app'].'?m=manager&c=show_salesman&msg=1');
		}
		else{
			echo "Error : Insert New Salesman Details...";	
			exit;	
		}
		
	}
	//
	// Show Profile
	//
	function show_profile(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_profile_form());
		$OUT->flush();			
	}
	//
	// Page Profile
	//
	function page_profile_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		$id    = $Q->cookies['manager:intAccountManagerId'];
		//
		// Get the Information
		//
		$this->obj = new core_manager();
		$this->obj->doGetProfile($id);
		$this->obj->GetNextRecord();
		$data['strFullName'] = $this->obj->Get('strFullName');
		$data['strEmail']    = $this->obj->Get('strEmail');
		$data['strContact']  = $this->obj->Get('strContact');
		$data['strLastLogin']  = $this->obj->Get('strLastlogin');
		
		
		return $this->_temp('shw_profile_form.php','NULL','NULL',$error,$data);	
		
	}
	
	//
	// Do Update Profile
	//
	function do_update_manager(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strFullName  = $Q->req['strFullName'];
		$strEmail     = $Q->req['strEmail'];
		$strContact   = $Q->req['strContact'];
		$strPassword1 = $Q->req['strPass1'];
		$strPassword2 = $Q->req['strPass2'];
		$id           = $Q->cookies['manager:intAccountManagerId'];
		
		//
		// Check Email
		//
		$this->objcheckemail =  new core_manager();
		$this->objcheckemail->doCheckEmailAdd($strEmail);
		$this->objcheckemail->GetNextRecord();
		$intDBId = $this->objcheckemail->Get('intAccountManagerId');
		
		//
		// Check Password
		//
		if($strPassword1 != $strPassword2){
			$OUT->addError('Password doesn\'t match.');	
		}
		if($strEmail==""){
			$OUT->addError('Email Address cannot be empty.');	
		}
		else{
			if($intDBId!=$id){
			$OUT->addError('Email Address already exists.');	
		     }
			
		}
		if($strContact==""){
			$OUT->addError('Contact No. cannot be empty.');	
		}
		
	   if($OUT->isError()){
			$this->show_error('err_update_acc_manager');
			exit;
		}
		//
		// Check Email Address
		//
		$this->obj = new core_manager();
		$this->obj->doUpdateAccManagerProfile(array(
		'strFullName'  => $strFullName,
		'strEmail'     => $strEmail,
		'strContact'  => $strContact,
		'strPassword1' => md5($strPassword1),
		'id'           => $id));
		
		$OUT->redirect($CONF['url_app'].'?m=manager&c=show_profile&msg=1');
	}
	//
	// Do Show Salesman Details
	//
	function show_salesman_details(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_salesman_details());
		$OUT->flush();		
	}
	//
	// Page show salesman details
	//
	function page_salesman_details(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$id               = $Q->req['id'];
		$intAccManagerId  = $Q->cookies['manager:intAccountManagerId'];
		//
		// Do Get Information From Account Manager 
		//
		$this->objAccManager = new core_manager();
		$this->objAccManager->doGetSalesmanDetailsById($id,$intAccManagerId);
		$this->objAccManager->GetNextRecord();
		$data['strFullName']  = $this->objAccManager->Get('strName');
        $data['strEmail']     = $this->objAccManager->Get('strEmail');
		$data['strContact']   = $this->objAccManager->Get('strContact');
		$data['intSalesId']   = $id;
		$data['intStatus']    = $this->objAccManager->Get('intStatus');
		$data['strUsername']  = $this->objAccManager->Get('strUser');
		$data['intType']  = $this->objAccManager->Get('intType');
		$data['strCommisionRate']  = $this->objAccManager->Get('strCommisionRate');
		
		return $this->_output(DIR_TEMP_PATH_MANAGER . '/manager/shw_salesman_editForm.php',$data,$error);	
		exit;
		
	}
	//
	// Do Edit Salesman Record
	//
	function do_update_salesmen(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$strFullName  = $Q->req['strFullName'];
		$strEmail     = $Q->req['strEmail'];
		$strContact   = $Q->req['strContact'];
		$strPassword  = $Q->req['strPassword'];
		$intStatus    = $Q->req['intStatus'];
		
		$strCommisionRate  = $Q->req['strCommisionRate'];
		$intType           = $Q->req['intType'];
		$id                = $Q->req['id'];
		
		$intAccountManagerId   = $Q->cookies['manager:intAccountManagerId'];
	
	
		
		if($strEmail==""){
			$OUT->addError('Email Address cannot be empty.');	
		}
		else{
			
			if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
			}
			$this->objcheckemail = new core_manager();
			$this->objcheckemail->doCheckEmail($strEmail);
			$strDBEmail = $this->objcheckemail->Get('strEmail');
		   if($strDBEmail==$strEmail){
			 $OUT->addError('Email Address already exists.');	
		   }
		  }
		
		if($strContact==""){
			$OUT->addError('Contact No. cannot be empty.');	
		}
	
	   if($OUT->isError()){
		 ?><font color="#FF0000"><?php foreach($OUT->errors as $k => $v){ echo $v."<br>";}?></font><?php							
		   //$OUT->redirect($CONF['url_app'].'?m=manager&c=show_salesman_details&id='.$id.'&error='.$error);
			//return $this->_output(DIR_TEMP_PATH_MANAGER . '/manager/shw_salesman_editForm.php',$data,$error);	
			exit;
		}
		//
		// Do Update Salesman Account
		//
		$this->objsalesman = new core_manager();
		$res = $this->objsalesman->doUpdateSalesmanRecord(array(
			'strFullName' => $strFullName,
			'strEmail'    => $strEmail,
			'strContact'  => $strContact,
			'strPassword' => $strPassword,
			'strCommisionRate' => $strCommisionRate,
			'intType'     => $intType,
			'intAccountManagerId' => $intAccountManagerId,
			'id'          => $id	,
			'intStatus'   => $intStatus
			
		
		));
		if($res==1){
			?><font color="#00CC33">You have sucessfully update the salesman record.</font><?php
		}
		else{
			
		 echo "Error : Update Salesman Record";
		 exit;	
		}
		
	}
	//
	// Do Delete Salesman
	//
	function do_delete_salesmen(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		 $id =  decrypt($Q->req['id'],'cfoni.8888');
		 $strAddedBy   = $Q->cookies['manager:intAccountManagerId'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_manager();
		 $res =  $this->obj->doDeleteSalesmanById($id,$strAddedBy);
		 
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=manager&c=show_salesman&msg=2'); 
		 }
		exit;
		
	}
	
	
	
	
	
	
	
	
	
	//	
	// Show Error and redirect
	//
	function show_error($err_code){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		if($err_code == 'err_auth'){
			$OUT->redirect($CONF['url_app']. "?m=manager&c=show_login");
			exit();
		}
		if($err_code=='err_add_salesmam'){
			$OUT->setVar('content', $this->page_salesman_form());
			$OUT->flush();
		}
		if($err_code=='err_update_acc_manager'){
			$OUT->setVar('content', $this->page_profile_form());
			$OUT->flush();
		}
		if($err_code == 'err_login'){
			$OUT->setVar('content', $this->page_login());
			$OUT->flush();
			
		}
	}
  
 	//
	// Output File
	//

   function _output($includefile,$data,$error) {
    ob_start();
    include ($includefile);
    $contents = ob_get_contents();
		ob_end_clean();
    
    
    return $contents;
  }
	
	//
	// Render Template
	//	
  function _temp($str, $mode,$c,$error,$data) {
    global $CONF, $Q, $OUT, $DB, $LANG;
    // To get the access

    ob_start();
   
    $include_file = array('main' => DIR_TEMP_PATH_MANAGER . '/manager/' . $str);
    include (ROOT_PATH . 'tpl/manager/tmp.php');
    $contents = ob_get_contents();
	ob_end_clean();
     
   
      return $contents;
   
  }
  
  

}
?>