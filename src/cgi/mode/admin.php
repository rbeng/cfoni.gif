<?php
require_once ROOT_PATH . "/src/lib/SimpleRBAD.php";
require_once ROOT_PATH . "/src/core/core_admin.php";
require_once ROOT_PATH . "/tcpdf/config/lang/eng.php";
require_once ROOT_PATH . "/tcpdf/tcpdf.php";

class admin{
	function __construct(){
		global $CONF,$Q,$OUT,$DB,$LANG,$MailClass,$PDF;
	
		// Language directoty
			
		$LANG->load('admin');
	
		$this->admin = new core_admin();
		
		//permission setting
		$this->rbac = new SimpleRBAD($CONF['dir_data']. '/user.xml');
		$role = $this->rbac->getUserRole($Q->cookies['admin:user_id']);
		$user_group = $role[0];
		//
		// Setting default command
		//
		if(! array_key_exists('c', $Q->req)){
			$Q->req['c'] = 'show_main';
		}
		//
		// Authentication	
		//
		{	
			//
			// Command that don't need authentication
			//
			$no_auth = array(
				'do_login'			=> '',
				'do_logout'			=> '',
				'show_login'		=> '',
      					
		
			);
			if(!array_key_exists($Q->req['c'],$no_auth)){	
				if(!$this->do_auth()){
					$this->show_error('err_auth');
				}
			}
		}
	}	
	
	public function _run(){
		global $Q;
		$cmd = $Q->req['c'];
		$valid_cmd	=	array(
			
			'show_main'				=> null,
			'do_edit'				=> null,
			'do_login'				=> null,
   		    'do_logout'				=> null,
			'show_login'			=> null,
			
			// Customer
			'show_customer'             => null,
			'do_change_status_customer' => null,
			
			// Account Manager
			'show_account_manager'      => null,
			'show_account_manager_form' => null,
			'do_add_account_manager'    => null,
			'show_account_manager_details' => null,
			'do_edit_accountmanager'    => null,
			'do_delete_acc_manager'     => null,
			
			// Salesman
			'show_salesman'             => null,
			'do_change_status_salesman' => null,
			
			
			// Reseller
			'show_reseller'             => null,
			'do_change_status_reseller' => null, 
			
			'do_point_salesman'         => null,
			
			
			
			
			
			
			            
			
			//Product
			'show_product'          => null,
			'show_product_form'     => null,
			'do_add_product'        => null,
			'show_product_details'  => null,
			'do_edit_product'       => null,
			'do_delete_product'     => null,
			// Package
			'show_package'          => null,
			'show_package_form'     => null,
			'do_add_package'        => null,
			'show_package_details'  => null,
			'do_edit_package'       => null,
			'do_delete_package'     => null,
			
			
			// Phone
			'show_phone'            => null,
			'show_phone_form'       => null,
			'show_phone_bulk_form'  => null,
			'do_upload_bulkphone'   => null,
			'do_delete_phone'       => null,
			
			
			// FAQ
			'show_faq'              => null,
			'show_faq_form'         => null,
			'do_add_faq'            => null,
			'show_faq_details'      => null,
			'do_edit_FAQ'           => null,
			'do_delete_FAQ'         => null,
			
			// Commision   
			'show_commission'       => null,   
			
			
			// Show Order
			'show_pending_order'    => null,
			'show_order'            => null,
			
			// Home
	        'show_messagelink'      => null,
	        'show_banner_lists'     => null,
	       
		    // About us
		    'show_messagephoto'     => null,
            'show_companyback'      => null,
	        'show_messagephotoma'   => null,
			
			//Product
	        'show_manageproduct'    => null,
			
			//Our Solution
	        'show_networkphoto'     => null,
			
			//Campaign
	        'show_campaign_listing' => null,
			'show_question_listings'=> null,
			'show_answerselected'   => null,
			'show_customereport'    => null,
			
			//Contact Us
	        'show_manageinformation'=> null,
			'show_feedback'         => null,
			'show_people'           => null,
			'do_add_ppl'            => null,
			'do_update_ppl'         => null,
			
			
			
			//Setting
	        'show_brand'            => null,
	        'show_mcategory'        => null,
			'show_subcategory'      => null,
			
			
			
			// Do Add Banner
			'do_add_banner'         => null,
			'do_update_banner'      => null,
			
			//Add and update
			
			
	        'do_add_messagelink'          => null,
	        'do_update_messagelink'       => null,
	        'do_add_messagephoto'         => null,
	        'do_update_messagephoto'      => null,
			
			'do_add_messagephoto_solutions'         => null,
	        'do_update_messagephoto_solutions'      => null,
	        'do_add_manageproduct'        => null,
	        'do_update_manageproduct'     => null,
	        'do_add_networkphoto'         => null,
	        'do_update_networkphoto'      => null,
	        'do_add_manageinformation'    => null,
	        'do_update_manageinformation' => null,
	        'do_add_campaign'             => null,
	        'do_update_campaign'          => null,
	        'do_add_brand'                => null,
	        'do_update_brand'             => null,
	        'do_add_category'             => null,
	        'do_update_category'          => null,
		    'do_add_subcategory'          => null,	
			'do_update_subcategory'       => null,
			'do_add_feedback'             => null,	
			'do_update_feedback'          => null,
			'do_add_questionlistings'     => null,	
			'do_update_questionlistings'  => null,
			'do_add_answerselected'       => null,	
			'do_update_answerselected'    => null,
			'do_add_customereport'        => null,	
			'do_update_customereport'     => null,
				
		);
		
	
		//
		// Setting default cmd if not found
		//
		if(!array_key_exists($cmd, $valid_cmd)){
			$cmd = 'show_main';
		}
		//
		// Executing the command
		//
		$this->$cmd();
	}
	//
	// Show main
	//
	function show_main(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_main());
		$OUT->flush();
	}
	//
	// Page main
	//
	function page_main(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		return $this->_temp('shw_main.php','NULL','NULL',$error,$data);	
	}
	
	function show_login(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_login());
		$OUT->flush();
	}
	function page_login(){
		global $CONF, $Q, $OUT, $DB,$LANG;
				
		$error = $OUT->errors;
  	    return $this->_output(DIR_TEMP_PATH_BO . '/admin/login.php',$data,$error);
  		
	}	
	//
	// Show Customers Lists
	//
	function show_customer(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_customers());
		$OUT->flush();
		
	}
	//
	// Page Customers
	//
	function page_customers(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;	
		$error = $OUT->errors;
		
		//
		// Get All Active Salesman Out Door
		//
		$this->objSalesman = new core_admin();
		$this->objSalesman->doGetSalesmanActive();
		while($this->objSalesman->GetNextRecord()){
			
			$data['salesman_selection'][$this->objSalesman->Get('strSalesmanCode')]['details'] = $CODE['prefix_salesman'][$this->objSalesman->Get('intType')].$this->objSalesman->Get('strSalesmanCode')."  /  ".$this->objSalesman->Get('strUser');
			$data['salesman_selection'][$this->objSalesman->Get('strSalesmanCode')]['type']    = $this->objSalesman->Get('intType');
		}
		
		
		
		
		//
		// Get Customer
		//
	    if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objCustomer = new core_admin();
		$this->objCustomer->numperpage   = 30;
		$this->objCustomer->numfirstpage = 30;
		$this->objCustomer->getAllCustomer($intPage);
		while($this->objCustomer->GetNextRecord()){
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intCustomerId']             = $this->objCustomer->Get('intCustomerId');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strUser']                   = $this->objCustomer->Get('strUser');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strEmail']                  = $this->objCustomer->Get('strEmail');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strContact']                 = $this->objCustomer->Get('strMobile');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strSalesmanCode']           = $this->objCustomer->Get('strSalesmanCode');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strResellerCode']           = $this->objCustomer->Get('strIntroduceResellerCode');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intStatus']                 = $this->objCustomer->Get('intStatus');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['CreateDate']                = $this->objCustomer->Get('CreateDate');
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objCustomer->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
		
		
		
		
		
		
		return $this->_temp('shw_customer_lists.php','NULL','NULL',$error,$data);	
	}
	//
	// Do Point Salesman
	//
	function do_point_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strSalesmanCode = explode(":",$Q->req['strSalesmanCode']);
		$intCustomerId   = $Q->req['intCustomerId'];
		
		$strSalesCode = $strSalesmanCode[1];
		$this->objinsalesman = new core_admin();
		$res = $this->objinsalesman->doUpdateCustomerSalesmanCode($strSalesCode,$intCustomerId);
		
		// Internal Salesman
		if($strSalesmanCode[0]==1){
			
			$msg=4;
		}
		// External Salesman
		if($strSalesmanCode[0]==2){
			
			$msg=5;
		}
		
		if($strSalesmanCode[0]==0){
			
			$msg=6;
		}
		if($res > 0){
			
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_customer&msg='.$msg);
		}
		
		exit;
	}
	
	
	
	
	//
	// Show Account Manager Lists
	//
	function show_account_manager(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_account_managers());
		$OUT->flush();
		
	}
	//
	// Page Account Manager
	//
	function page_account_managers(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$error = $OUT->errors;
		//
		// Get Account Manager
		//
	    if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objManager = new core_admin();
		$this->objManager->numperpage   = 30;
		$this->objManager->numfirstpage = 30;
		$this->objManager->doShowAllAccManager($intPage);
		
		
		while($this->objManager->GetNextRecord()){
			$data['acc_manager'][$this->objManager->Get('intAccountManagerId')]['intAccountManagerId'] = $this->objManager->Get('intAccountManagerId');
			$data['acc_manager'][$this->objManager->Get('intAccountManagerId')]['strFullName']         = $this->objManager->Get('strFullName');
			$data['acc_manager'][$this->objManager->Get('intAccountManagerId')]['strContact']          = $this->objManager->Get('strContact');
			$data['acc_manager'][$this->objManager->Get('intAccountManagerId')]['strEmail']            = $this->objManager->Get('strEmail');
			$data['acc_manager'][$this->objManager->Get('intAccountManagerId')]['CreateDate']            = $this->objManager->Get('CreateDate');
			$data['acc_manager'][$this->objManager->Get('intAccountManagerId')]['strUser']            = $this->objManager->Get('strUser');
			$data['acc_manager'][$this->objManager->Get('intAccountManagerId')]['intStatus']            = $this->objManager->Get('intStatus');
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objManager->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		return $this->_temp('shw_account_manager_lists.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Account Manager Form
	//
	function show_account_manager_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_account_managers_form());
		$OUT->flush();
	}
	//
	// Page Account Manager Form
	//
	function page_account_managers_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$error = $OUT->errors;
		return $this->_temp('shw_account_manager_form.php','NULL','NULL',$error,$data);	
	}
	//
	// Do Add Account Manager
	// 
	function do_add_account_manager(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;	
		
		$strFullName  = $Q->req['strFullName'];
		$strEmail     = $Q->req['strEmail'];
		$strContact   = $Q->req['strContact'];
		$strUsername  = $Q->req['strUsername'];
		$strPassword  = $Q->req['strPassword'];
		$intAutoEmail = $Q->req['intAutoEmail'];
		$strAddedBy   = $Q->cookies['admin:user_id'];
		if($intAutoEmail==""){
			$intAutoEmail = 0;
		}
		//
		// Username Exists Or Not
		//
		if($strUsername!=""){
		$this->objcheck = new core_admin();
		$this->objcheck->doCheckUsername($strUsername);
		$this->objcheck->GetNextRecord();
		$strDBUsername = $this->objcheck->Get('strUser');
		 if($strDBUsername==$strUsername){
			$OUT->addError('Username already exists.');	
		  }
		}
		else{
			$OUT->addError('Username cannot be empty.');	
		}
		
		if($strEmail==""){
			$OUT->addError('Email Address cannot be empty.');	
		}
		else{
			
			if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
			}
	    }
		if($strPassword==""){
			$OUT->addError('Passwrod cannot be empty.');	
		}
		if($strContact==""){
			$OUT->addError('Contact No. cannot be empty.');	
		}
	
	   if($OUT->isError()){
			$this->show_error('err_add_accountmanager');
			exit;
		}
		
		//
		// Do Insert Account Manager
		//
		$this->obj = new core_admin();
		$res = $this->obj->doAddNewAccManager(array(
		  'strFullName' => $strFullName,
		  'strEmail'    => $strEmail,
		  'strContact'  => $strContact,
		  'strUsername' => $strUsername,
		  'strPassword' => $strPassword,
		  'strAddedBy'  => $strAddedBy,
		  'intAutoEmail' => $intAutoEmail,
			)
		);
		
		if($res==1){
			
			if($intAutoEmail==1){
			//
			// Inform New Accout Manager
			//
			$strTitle   = 'Welcome : New Account Manager';
			$strContent  = 'Your account details as below <br> Username :'.$strUsername.' <br> Password :'.$strPassword.'<br> URL : <a href='.$CONF['url_app'].'?m=manager>'.$CONF['url_app'].'?m=manager'.'<a>';
			
			include('email_tmpl/welcome_accountmanager.php');
			
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $strEmail  );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			 }
			}
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_account_manager_form&msg=1');
		}
		else{
			echo "Error : Insert New Account Manager Details...";	
			exit;	
		}
	}
	//
	// Show Account Manager Page
	//
	function show_account_manager_details(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;	
		
		$OUT->setVar('content',	$this->page_account_managers_details_form());
		$OUT->flush();
		
	}
	//
	// Page Account Manager Edit Page
	//
	function page_account_managers_details_form(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;	
		
		$id = $Q->req['id'];
		//
		// Do Get Information From Account Manager 
		//
		$this->objAccManager = new core_admin();
		$this->objAccManager->doGetAccManagerDetailsById($id);
		$this->objAccManager->GetNextRecord();
		$data['strFullName'] = $this->objAccManager->Get('strFullName');
        $data['strEmail']    = $this->objAccManager->Get('strEmail');
		$data['strContact']    = $this->objAccManager->Get('strContact');
		$data['intAccountManagerId'] = $id;
		$data['intStatus']    = $this->objAccManager->Get('intStatus');
		$data['strUsername']  =  $this->objAccManager->Get('strUser');
		
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/shw_account_manager_editForm.php',$data,$error);	
		exit;
		
	}
	
	//
	// Do Edit Account Manager
	//	
	function do_edit_accountmanager(){
	 global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;	
	 
	    $strFullName  = $Q->req['strFullName'];
		$strEmail     = $Q->req['strEmail'];
		$strContact   = $Q->req['strContact'];
		$strUsername  = $Q->req['strUsername'];
		$strPassword  = $Q->req['strPassword'];
		$intAutoEmail = $Q->req['intAutoEmail'];
		$strAddedBy   = $Q->cookies['admin:user_id'];
		$intStatus  = $Q->req['intStatus'];
		$id           = $Q->req['id'];
	 	//
		// Username Exists Or Not
		//
		if($strUsername!=""){
		$this->objcheck = new core_admin();
		$this->objcheck->doCheckUsername($strUsername);
		$this->objcheck->GetNextRecord();
		$strDBUsername = $this->objcheck->Get('strUser');
		 if($strDBUsername==$strUsername){
			$OUT->addError('Username already exists.');	
		  }
		}
		if($strEmail==""){
			$OUT->addError('Email Address cannot be empty.');	
		}
		else{
			
			if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
			}
	    }
		
		if($strContact==""){
			$OUT->addError('Contact No. cannot be empty.');	
		}
	
	   if($OUT->isError()){
			
			 ?><font color="#FF0000"><?php foreach($OUT->errors as $k => $v){ echo $v."<br>";}?></font><?php	exit;
		}
	  else{
		  
		  //
		  // Do Update Account Manager
		  //
		  $this->objManagerUpdate = new core_admin();
		  $this->objManagerUpdate->doEditAccManager(array(
		  	 'strFullName'    => $strFullName ,
		  	 'strEmail'       => $strEmail,
		     'strContact'     => $strContact,
		     'strPassword'    => $strPassword,
		     'strAddedBy'     => $strAddedBy,
			 'id'             => $id,
			 'intStatus'      => $intStatus
		 
		  ));
		
		  echo "<font color='#009900'> Sucessfully to update. </font>";
		  exit;
		  }
	 exit;
	}
	//
	// Do Delete Account Account
	//
	function do_delete_acc_manager(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;	
		 
		 $id =  decrypt($Q->req['id'],'cfoni.8888');
		 $strAddedBy   = $Q->cookies['admin:user_id'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_admin();
		 $res =  $this->obj->doDeleteAccManagerById($id,$strAddedBy);
		 
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_account_manager&msg=2'); 
		 }
		exit;
	}
	
	
	
	
	
	//
	// Show Salesman Lists
	//
	function show_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_salesman());
		$OUT->flush();
		
	}
	//
	// Page Salesman
	//
	function page_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;	
		$error = $OUT->errors;
		
		//
		// Get Acc Manager Details
		//
		$this->objAccManager = new core_admin();
		$this->objAccManager->doGetAllListAccManager();
		while($this->objAccManager->GetNextRecord()){
			$data['acc_manager'][$this->objAccManager->Get('intAccountManagerId')]['FullName'] = $this->objAccManager->Get('strFullName');
		}
		//
		// Get Salesman
		//
	    if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objSalesman = new core_admin();
		$this->objSalesman->numperpage   = 30;
		$this->objSalesman->numfirstpage = 30;
		$this->objSalesman->doShowAllSalesman($intPage);

		while($this->objSalesman->GetNextRecord()){
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['intSalesId']          = $this->objSalesman->Get('intSalesId');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strSalesCode']        = $this->objSalesman->Get('strSalesmanCode');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['intType']             = $CODE['prefix_salesman'][$this->objSalesman->Get('intType')];
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strName']             = $this->objSalesman->Get('strName');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strContact']          = $this->objSalesman->Get('strContact');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['strEmail']            = $this->objSalesman->Get('strEmail');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['CreateDate']          = $this->objSalesman->Get('CreateDate');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['intAccountManagerId'] = $this->objSalesman->Get('intAccountManagerId');
			$data['salesman'][$this->objSalesman->Get('intSalesId')]['intStatus']           = $this->objSalesman->Get('intStatus');
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objSalesman->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
		return $this->_temp('shw_salesman_lists.php','NULL','NULL',$error,$data);	
	}
	//
	// Do Change Status
	//
	function do_change_status_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		 $id =  $Q->req['id'];
		 $intStatus =  $Q->req['intStatus'];
		 //$strAddedBy   = $Q->cookies['admin:user_id'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_admin();
		 $res =  $this->obj->doSalesmanStatusById($id,$intStatus);
		 
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_salesman&msg=3'); 
		 }
		exit;	
	}
	
	//
	// Show Reseller Lists
	//
	function show_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_reseller());
		$OUT->flush();
		
	}
	//
	// Page Reseller
	//
	function page_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		$error = $OUT->errors;
		
		//
		// Get Reseller
		//
	    if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objReseller = new core_admin();
		$this->objReseller->numperpage   = 30;
		$this->objReseller->numfirstpage = 30;
		$this->objReseller->getAllReseller($intPage);
		while($this->objReseller->GetNextRecord()){
			$data['reseller'][$this->objReseller->Get('intResellerId')]['intResellerId']          = $this->objReseller->Get('intResellerId');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['strResellerCode']        = $this->objReseller->Get('strResellerCode');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['intCommisionRate']       = $this->objReseller->Get('intCommisionRate');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['strSalesmanCode']        = $this->objReseller->Get('strSalesmanCode');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['intCustomerId']        = $this->objReseller->Get('intCustomerId');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['intStatus']                 = $this->objReseller->Get('intStatus');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['CreateDate']                 = $this->objReseller->Get('CreateDate');
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objReseller->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
		//
		// Get the Customer Details
		//
		$this->objCus = new core_admin();
		$this->objCus->doGetAllCustomers();
		while($this->objCus->GetNextRecord()){
			$data['customer'][$this->objCus->Get('intCustomerId')]['intCustomerId'] = $this->objCus->Get('intCustomerId');	
			$data['customer'][$this->objCus->Get('intCustomerId')]['strMobile'] = $this->objCus->Get('strMobile');
			$data['customer'][$this->objCus->Get('intCustomerId')]['strEmail'] = $this->objCus->Get('strEmail');
		}
		
		
		
		return $this->_temp('shw_reseller_lists.php','NULL','NULL',$error,$data);	
	}
	//
	// Do Change Status
	//
	function do_change_status_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		 
		 $id =  $Q->req['id'];
		 $intStatus =  $Q->req['intStatus'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_admin();
		 $res =  $this->obj->doResellerStatusById($id,$intStatus);
		 if($res==1){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_reseller&msg=3'); 
		 }
		exit;	
	}
	//
	// Do Change Customer Status
	//
	function do_change_status_customer(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		 
		 $id =  $Q->req['id'];
		 $intStatus =  $Q->req['intStatus'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_admin();
		 $res =  $this->obj->doCustomerStatusById($id,$intStatus);
		 if($res==1){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_customer&msg=3'); 
		 }
		exit;	
		
	}
	
	
	
	
	
	//
	// Show Product Lists
	//
	function show_product(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_product());
		$OUT->flush();		
	}
	//
	// Page Product
	//
	function page_product(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		$error = $OUT->errors;
		
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objProduct = new core_admin();
		$this->objProduct->numperpage   = 30;
		$this->objProduct->numfirstpage = 30;
		$this->objProduct->doShowAllProduct($intPage);
		
		
		while($this->objProduct->GetNextRecord()){
			$data['product'][$this->objProduct->Get('intProductId')]['intProductId'] = $this->objProduct->Get('intProductId');
			$data['product'][$this->objProduct->Get('intProductId')]['strFullName']         = $this->objProduct->Get('strProductName');
			$data['product'][$this->objProduct->Get('intProductId')]['intOrder']            = $this->objProduct->Get('intOrder');
			$data['product'][$this->objProduct->Get('intProductId')]['CreateDate']            = $this->objProduct->Get('CreateDate');
			$data['product'][$this->objProduct->Get('intProductId')]['strAddedBy']            = $this->objProduct->Get('strAddedBy');
			$data['product'][$this->objProduct->Get('intProductId')]['intStatus']            = $this->objProduct->Get('intStatus');
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objProduct->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
		return $this->_temp('shw_product_lists.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Product From
	//
	function show_product_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_product_form());
		$OUT->flush();
	}
	//
	// Page Product Form
	//
	function page_product_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		return $this->_temp('shw_product_form.php','NULL','NULL',$error,$data);	
		
	} 
	//
	// Do Add Product
	//
	function do_add_product(){
	 global $CONF, $Q, $OUT, $DB,$LANG;	
	 
	 $strProductName = trim($Q->req['strProductName']);
	 $strAddedBy     = $Q->cookies['admin:user_id'];
	 $intOrder       = $Q->req['intOrder'];
	 $intStatus      = $Q->req['intStatus'];
	 //
	 // Check the Product Name
	 //
	 $this->objCheckProduct = new core_admin();
	 $this->objCheckProduct->doCheckProductName($strProductName);
	 $this->objCheckProduct->GetNextRecord();
	 $strDBProductName = $this->objCheckProduct->Get('strProductName'); 
	 
	
	 
	  if($strProductName==""){
			$OUT->addError('Product Name cannot be empty.');	
		}
	 if($strProductName==$strDBProductName){
			$OUT->addError('Product Name already exists.');	
		}
	  if($strProductName==$strDBProductName){
			$OUT->addError('Order No. cannot be empty.');	
		}
	   if($OUT->isError()){
			$this->show_error('err_add_product');
			exit;
		}
	 
	 $postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
		//
		// Do Add Product
		//
		$this->objAdd = new core_admin();
		$res = $this->objAdd->doAddProduct(array(
		'strProductName' => $strProductName,
		'strDescription' => $strDescription,
		'intOrder'       => $intOrder,
		'intStatus'      => $intStatus,
		'strAddedBy'     => $strAddedBy
		
			)
		);
		
		if($res==1){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_product_form&msg=1');
		}
		else{
			
			echo "Error : Insert New Product";
			exit;
		}
	}
	//
	// Show Product Details
	//
	function show_product_details(){
		 global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_product_details());
		$OUT->flush();
	}
	//
	// Page product details
	//
	function page_product_details(){
		 global $CONF, $Q, $OUT, $DB,$LANG;
		 
		$id = $Q->req['id'];
		//
		// Do Get Information From Product
		//
		$this->objProduct = new core_admin();
		$this->objProduct->doGetProductDetailsById($id);
		$this->objProduct->GetNextRecord();
		$data['strProductName'] = $this->objProduct->Get('strProductName');
        $data['intOrder']    = $this->objProduct->Get('intOrder');
		$data['strDescription']    = $this->objProduct->Get('strDescription');
		$data['intProductId'] = $id;
		$data['intStatus']    = $this->objProduct->Get('intStatus');
		
		
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/shw_product_editForm.php',$data,$error);	
		exit;	
		
	}
	//
	// Do Edit Product
	//
	function do_edit_product(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
	 
	 $strProductName = trim($Q->req['strProductName']);
	 $strAddedBy     = $Q->cookies['admin:user_id'];
	 $intOrder       = $Q->req['intOrder'];
	 $intStatus      = $Q->req['intStatus'];
	 $id             = $Q->req['id'];
	 //
	 // Check the Product Name
	 //
	 $this->objCheckProduct = new core_admin();
	 $this->objCheckProduct->doCheckProductName($strProductName);
	 $this->objCheckProduct->GetNextRecord();
	 $strDBProductName = $this->objCheckProduct->Get('strProductName'); 
	 $strDBProductId = $this->objCheckProduct->Get('intProductId'); 
	
	 
	  if($strProductName==""){
			$OUT->addError('Product Name cannot be empty.');	
		}
	 if($strProductName==$strDBProductName && $id!=$strDBProductId){
			$OUT->addError('Product Name already exists.');	
		}
	  if($intOrder==""){
			$OUT->addError('Order No. cannot be empty.');	
		}
	   if($OUT->isError()){
			
		$error = json_encode($OUT->errors);
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_product_details&id='.$id.'&error='.$error);
		exit;
		}
	 
	  $postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
		//
		// Do Add Product
		//
		$this->objAdd = new core_admin();
		$res = $this->objAdd->doUpdateProduct(array(
		'strProductName' => $strProductName,
		'strDescription' => $strDescription,
		'intOrder'       => $intOrder,
		'intStatus'      => $intStatus,
		'strAddedBy'     => $strAddedBy,
		'id'             => $id
		
			)
		);
		
		if($res==1){
		  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_product_details&id='.$id.'&success=1');
		exit;
		}
		else{
			echo "Error : Update Product";
			exit;
		}
		
	}
	//
	// Do Delete Product
	//
	function do_delete_product(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;	
		 
		 $id =  decrypt($Q->req['id'],'cfoni.8888');
		 $strAddedBy   = $Q->cookies['admin:user_id'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_admin();
		 $res =  $this->obj->doDeleteProductById($id,$strAddedBy);
		 
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_product&msg=2'); 
		 }
		 
		 
		exit;
		
	}
	
	
	
	
	/*********************************/
    //
	// Show Package Lists
	//
	function show_package(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_package());
		$OUT->flush();		
	}
	//
	// Page Product
	//
	function page_package(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$error = $OUT->errors;
		
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objPackage = new core_admin();
		$this->objPackage->numperpage   = 30;
		$this->objPackage->numfirstpage = 30;
		$this->objPackage->doShowAllPackage($intPage);
		
		
		while($this->objPackage->GetNextRecord()){
			$data['package'][$this->objPackage->Get('intPackageId')]['intPackageId']        = $this->objPackage->Get('intPackageId');
			$data['package'][$this->objPackage->Get('intPackageId')]['strPackageName']         = $this->objPackage->Get('strPackageName');
			$data['package'][$this->objPackage->Get('intPackageId')]['intOrder']            = $this->objPackage->Get('intOrder');
			$data['package'][$this->objPackage->Get('intPackageId')]['CreateDate']          = $this->objPackage->Get('CreateDate');
			$data['package'][$this->objPackage->Get('intPackageId')]['strAddedBy']          = $this->objPackage->Get('strAddedBy');
			$data['package'][$this->objPackage->Get('intPackageId')]['intStatus']           = $this->objPackage->Get('intStatus');
			$data['package'][$this->objPackage->Get('intPackageId')]['strImage']           = $this->objPackage->Get('strImage');
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objPackage->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
		return $this->_temp('shw_package_lists.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Product From
	//
	function show_package_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_package_form());
		$OUT->flush();
	}
	//
	// Page Package Form
	//
	function page_package_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		return $this->_temp('shw_package_form.php','NULL','NULL',$error,$data);	
		
	} 
	//
	// Do Add Product
	//
	function do_add_package(){
	 global $CONF, $Q, $OUT, $DB,$LANG;	
	 
	 $strPackageName = trim($Q->req['strPackageName']);
	 $strAddedBy     = $Q->cookies['admin:user_id'];
	 $intOrder       = $Q->req['intOrder'];
	 $intStatus      = $Q->req['intStatus'];
	 $strImage       = $_FILES['strImage']['tmp_name'];
	 $strPrice       = $Q->req['strPrice'];
	 //
	 // Check the Product Name
	 //
	 $this->objCheckPackage = new core_admin();
	 $this->objCheckPackage->doCheckPackageName($strPackageName);
	 $this->objCheckPackage->GetNextRecord();
	 $strDBPackageName = $this->objCheckPackage->Get('strPackageName'); 
	 
	
	 
	  if($strPackageName==""){
			$OUT->addError('Package Name cannot be empty.');	
		}
	 if($strPackageName==$strDBPackageName){
			$OUT->addError('Package Name already exists.');	
		}
	  if($intOrder==""){
			$OUT->addError('Order No. cannot be empty.');	
		}
	   if($OUT->isError()){
			$this->show_error('err_add_package');
			exit;
		}
	 
	 $postArray = &$_POST['strDesc'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
	
	 $postArray1 = &$_POST['strFeatures'] ;
		if(get_magic_quotes_gpc() )
				$strFeatures = htmlspecialchars( stripslashes( $postArray1 ) ) ;
			else
				$strFeatures = htmlspecialchars( $postArray1 ) ;
		
	 
		//
		// Do Add Product
		//
		$this->objAdd = new core_admin();
		$id = $this->objAdd->doAddPackage(array(
		'strPackageName' => $strPackageName,
		'strDescription' => $strDescription,
		'strFeatures'   => $strFeatures,
		'intOrder'       => $intOrder,
		'intStatus'      => $intStatus,
		'strAddedBy'     => $strAddedBy,
		'strPrice'       => $strPrice
		
			)
		);
	
		$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        if($strImage!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/package/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/package/'.$folder_id.'/',0777);
							}
		  		 if(!file_exists($CONF['dir_photo'  ].'/package/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/package/'.$folder_id.'/'.$id.'/',0777);
							}
			    $upload_path   = $CONF['dir_photo'  ].'/package/'.$folder_id.'/'.$id.'/'; 
   		    	$arrfilename   = $_FILES['strImage']['name'];
   				$arrTemp       = $_FILES['strImage']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				  die('The file you attempted to upload is not allowed.');}
	 			   if(filesize($arrTemp) > $max_filesize){
      			     die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			     }
   				  if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			}
	      			else{
    	     			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             		}
				  }
		 		 //
		 		 // Update Image
		 		 //
				 if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdatePackageImage($id,$arrfilename);
	 				 }
		
		if($id > 0){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_package_form&msg=1');
		}
		else{
			
			echo "Error : Insert New Package";
			exit;
		}
	}
	//
	// Show Product Details
	//
	function show_package_details(){
		 global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_package_details());
		$OUT->flush();
	}
	//
	// Page pakage details
	//
	function page_package_details(){
		 global $CONF, $Q, $OUT, $DB,$LANG;
		 
		$id = $Q->req['id'];
		//
		// Do Get Information From Package
		//
		$this->objPackage = new core_admin();
		$this->objPackage->doGetPackageDetailsById($id);
		$this->objPackage->GetNextRecord();
		$data['strPackageName']    = $this->objPackage->Get('strPackageName');
        $data['intOrder']          = $this->objPackage->Get('intOrder');
		$data['strDescription']    = $this->objPackage->Get('strDescription');
		$data['strFeature']        = $this->objPackage->Get('strFeature');
		$data['strImage']          = $this->objPackage->Get('strImage');
		$data['intPackageId']      = $id;
		$data['intStatus']         = $this->objPackage->Get('intStatus');
		$data['strPrice']         = $this->objPackage->Get('strPrice');
		
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/shw_package_editForm.php',$data,$error);	
		exit;	
		
	}
	//
	// Do Edit Package
	//
	function do_edit_package(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
	 
	 $strPackageName = trim($Q->req['strPackageName']);
	 $strAddedBy     = $Q->cookies['admin:user_id'];
	 $intOrder       = $Q->req['intOrder'];
	 $intStatus      = $Q->req['intStatus'];
	 $strImage       = $_FILES['strImage']['tmp_name'];
	 $strPrice       = $Q->req['strPrice'];
	 $id             = $Q->req['id'];
	 $strFeature     = $Q->req['strFeature'];
	 //
	 // Check the Product Name
	 //
	 $this->objCheckPackage = new core_admin();
	 $this->objCheckPackage->doCheckPackageName($strPackageName);
	 $this->objCheckPackage->GetNextRecord();
	 $strDBPackageName = $this->objCheckPackage->Get('strPackageName'); 
	 $intDBPackageId = $this->objCheckPackage->Get('intPackageId'); 
	
	 
	  if($strPackageName==""){
			$OUT->addError('Package Name cannot be empty.');	
		}
	  else{
	 	if($strPackageName==$strDBPackageName && $intDBPackageId!=$id){
			$OUT->addError('Package Name already exists.');	
		}
	  }
	  if($intOrder==""){
			$OUT->addError('Order No. cannot be empty.');	
		}
	   if($OUT->isError()){
		$error = json_encode($OUT->errors);
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_package_details&id='.$id.'&error='.$error);
		exit;
		}
	 
	 $postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
	
	 $postArray1 = &$_POST['strFeatures'] ;
		if(get_magic_quotes_gpc() )
				$strFeatures = htmlspecialchars( stripslashes( $postArray1 ) ) ;
			else
				$strFeatures = htmlspecialchars( $postArray1 ) ;
		//
		// Do Add Product
		//
		$this->objAdd = new core_admin();
		$id = $this->objAdd->doUpdatePackage(array(
		'strPackageName' => $strPackageName,
		'strDescription' => $strDescription,
		'strFeatures'   => $strFeature,
		'intOrder'       => $intOrder,
		'intStatus'      => $intStatus,
		'strAddedBy'     => $strAddedBy,
		'strPrice'       => $strPrice,
		'id'             => $id
		
			)
		);
	
		$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        if($strImage!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/package/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/package/'.$folder_id.'/',0777);
							}
		  		 if(!file_exists($CONF['dir_photo'  ].'/package/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/package/'.$folder_id.'/'.$id.'/',0777);
							}
			    $upload_path   = $CONF['dir_photo'  ].'/package/'.$folder_id.'/'.$id.'/'; 
   		    	$arrfilename   = $_FILES['strImage']['name'];
   				$arrTemp       = $_FILES['strImage']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				  die('The file you attempted to upload is not allowed.');}
	 			   if(filesize($arrTemp) > $max_filesize){
      			     die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			     }
   				  if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			}
	      			else{
    	     			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             		}
				  }
		 		 //
		 		 // Update Image
		 		 //
				 if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdatePackageImage($id,$arrfilename);
	 				 }
	
		if($id > 0){
		  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_package_details&id='.$id.'&success=1');
		exit;
		}
		else{
			echo "Error : Update Product";
			exit;
		}
		
	}
	//
	// Do Delete Package
	//
	function do_delete_package(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;	
		 
		 $id =  decrypt($Q->req['id'],'cfoni.8888');
		 $strAddedBy   = $Q->cookies['admin:user_id'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_admin();
		 $res =  $this->obj->doDeletePackageById($id,$strAddedBy);
		 
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_package&msg=2'); 
		 }
		exit;
	}
	
	//
	// Show Phone Lists
	//
	function show_phone(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_phonelists());
		$OUT->flush();	
		
	}
	//
	// Page Phone Lists
	//
	function page_phonelists(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;
		//
		// To Get Phone Lists
		//
		$error = $OUT->errors;
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objPhone = new core_admin();
		$this->objPhone->numperpage   = 30;
		$this->objPhone->numfirstpage = 30;
		$this->objPhone->doShowAllPhone($intPage);
		
		
		while($this->objPhone->GetNextRecord()){
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['strBatch']          = $this->objPhone->Get('strBatch');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['intDidFaxId']          = $this->objPhone->Get('intDidFaxId');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['strNumber']       = $this->objPhone->Get('strNumber');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['strPrice']             = $this->objPhone->Get('strPrice');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['CreateDate']           = $this->objPhone->Get('CreateDate');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['strAddedBy']           = $this->objPhone->Get('strAddedBy');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['intStatus']            = $this->objPhone->Get('intStatus');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['intType']             =$CODE['phone_type'][$this->objPhone->Get('intType')];
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objPhone->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
		
		
		
		return $this->_temp('shw_phone_lists.php','NULL','NULL',$error,$data);	
		
	} 
	
	
	
	//
	// Show Pending Order 
	//
	function show_pending_order(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_pending_order());
		$OUT->flush();	
	}
	//
	// Page pending order
	//
	function page_pending_order(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		//
		// To Get Phone Lists
		//
		$error = $OUT->errors;
		/* if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objPhone = new core_admin();
		$this->objPhone->numperpage   = 30;
		$this->objPhone->numfirstpage = 30;
		$this->objPhone->doShowAllPhone($intPage);
		
		
		while($this->objPhone->GetNextRecord()){
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['intDidFaxId']          = $this->objPhone->Get('intDidFaxId');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['strNumber']       = $this->objPhone->Get('strNumber');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['strPrice']             = $this->objPhone->Get('strPrice');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['CreateDate']           = $this->objPhone->Get('CreateDate');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['strAddedBy']           = $this->objPhone->Get('strAddedBy');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['intStatus']            = $this->objPhone->Get('intStatus');
			$data['phone'][$this->objPhone->Get('intDidFaxId')]['intType']             =$CODE['phone_type'][$this->objPhone->Get('intType')];
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objPhone->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
		*/
		
		
		return $this->_temp('shw_pending_order_lists.php','NULL','NULL',$error,$data);	
		
	} 
	
	//
	// Show Phone Form
	//
	function show_phone_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_phone_form());
		$OUT->flush();	
		
	}
	//
	// Page show form
	//
	function page_phone_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		return $this->_temp('shw_phone_form.php','NULL','NULL',$error,$data);	
		
	}
	
	//
	// Popup Broswe Excel
	//
	function show_phone_bulk_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_phone_bulk_form());
		$OUT->flush();	
	}
	//
	// Page Popup Bulk Form
	//
	function page_phone_bulk_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
  	    return $this->_output(DIR_TEMP_PATH_BO . '/admin/shw_phone_bulk_form.php',$data,$error);
		
		
	}
	
	//
	// Do upload bulk phone lists
	//
	function do_upload_bulkphone(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strBatchNo    = $Q->req['strBatch'];
		 $sFile        = $_FILES['strFiles']['tmp_name'];
		 $sName        = $_FILES["strFiles"]["name"] ;
		 $intPhoneType = $Q->req['intPhoneType'];
		 $strAddedBy   = $Q->cookies['admin:user_id'];
	
	$oFilename = explode(".",$sFile);
    $extension = end($oFilename);
	
	$cFilenemE = explode(".",$sName);
	$cExt      = end($cFilenemE);
	
	if($intPhoneType==2){
		
	  $strPrice  =  $CONF['add_special_cost'];	
	}
	else{
		 $strPrice  = 0;
		
	}
	
	if($strBatchNo==""){
	  $OUT->addError('Batch No. cannot be empty.');	
	}
	
	
	if($sFile==""){
		$OUT->addError('File upload cannot be empty.');	
	 }
	 else{
		 
		
	 $arrExtension = array('xls'=>1,'xlsx'=>2);
	  if(!array_key_exists($cExt,$arrExtension)){
		
		$OUT->addError('Invalid file format , should be .xls / .xlsx.');	
	   }
	 }
	
	
	
if($sFile!=""){
$format = $extension=="xls" ? "Excel5" : "Excel2007";
$objReader = PHPExcel_IOFactory::createReader($format);
$objReader->setReadDataOnly(true);

$objPHPExcel = $objReader->load($sFile);
$objWorksheet = $objPHPExcel->getActiveSheet();
$index = 1;
$arrData  = array();

foreach ($objWorksheet->getRowIterator() as $row) {
      $iRow = $row->getRowIndex();
      
      $iCol = 0;
     //column number, row number
      $oCell = $objWorksheet->getCellByColumnAndRow($iCol, $iRow);
      $value = $oCell->getValue();
      //for formula based cell:
      $value = $oCell->getCalculatedValue();
	 
	  if($value!=""){
	   $arrData[$index] = $value;
	   
	   $index++;
	  }
	 /*
	  $iCol = 1;
     //column number, row number
      $oCell = $objWorksheet->getCellByColumnAndRow($iCol, $iRow);
      $value = $oCell->getValue();
      //for formula based cell:
      $value = $oCell->getCalculatedValue();
	  $arrData[$iRow]['intTitle2'] = $value;
      */
	 
  }
}
   $cPhone = count($arrData);
   
   if($cPhone <= 0 ){
		$OUT->addError('Phone Data cannot be empty.');	
	 }
  
    if($OUT->isError()){
		$error = json_encode($OUT->errors);
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_phone_bulk_form&error='.$error);
		exit;
	
	 }
		
  
  
     //
	 // Do Insert Phone Lists
	 //
	  $this->objPhone = new core_admin();
	  $res = $this->objPhone->doAddBulkPhone($arrData,$intPhoneType,$strPrice,$strBatchNo ,$strAddedBy ); 
  	
	  if($res==1){
		  
		  
		  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_phone_bulk_form&msg=1&total='.$cPhone);
		exit;
	  }
	  else{
	  	  
	   echo "Error : Insert Phone No.";	  
	 }
	
	exit;	
	}
	
	//
	// Do Delete Phone No.
	//
	function do_delete_phone(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		 $id =  decrypt($Q->req['id'],'cfoni.8888');
		 $strAddedBy   = $Q->cookies['admin:user_id'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_admin();
		 $res =  $this->obj->doDeletePhoneById($id,$strAddedBy);
		 
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_phone&msg=2'); 
		 }
		exit;	
		
	}
	
	
	
	
	
	//
	// FAQ
	//
	function show_faq(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_faq());
		$OUT->flush();	
	}
	//
	// FAQ Listings
	//
	function page_faq(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;
		
		$error = $OUT->errors;
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objFAQ = new core_admin();
		$this->objFAQ->numperpage   = 30;
		$this->objFAQ->numfirstpage = 30;
		$this->objFAQ->doShowAllFAQ($intPage);
		
		
		while($this->objFAQ->GetNextRecord()){
			$data['faq'][$this->objFAQ->Get('intFaqId')]['intFaqId']            = $this->objFAQ->Get('intFaqId');
			$data['faq'][$this->objFAQ->Get('intFaqId')]['strQ']                = $this->objFAQ->Get('strQ');
			$data['faq'][$this->objFAQ->Get('intFaqId')]['strA']                 = $this->objFAQ->Get('strA');
			$data['faq'][$this->objFAQ->Get('intFaqId')]['CreateDate']           = $this->objFAQ->Get('CreateDate');
			$data['faq'][$this->objFAQ->Get('intFaqId')]['strAddedBy']           = $this->objFAQ->Get('strAddedBy');
			$data['faq'][$this->objFAQ->Get('intFaqId')]['intStatus']            = $this->objFAQ->Get('intStatus');
			$data['faq'][$this->objFAQ->Get('intFaqId')]['intOrder']            = $this->objFAQ->Get('intOrder');
			$data['faq'][$this->objFAQ->Get('intFaqId')]['intType']             = $CODE['faq_type'][$this->objFAQ->Get('intType')];
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objFAQ->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
  	    return $this->_temp('shw_faq.php','NULL','NULL',$error,$data);
		
		
	}
	
	//
	// Show FAQ Form
	//
	function show_faq_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_faq_form());
		$OUT->flush();		
		
	}
	//
	// FAQ Form
	//
	function page_faq_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
  	    return $this->_temp('shw_faq_form.php','NULL','NULL',$error,$data);
	} 
	
	//
	// Do Add FAQ
	//
	function do_add_faq(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intType = $Q->req['intType'];
		$intOrder  = $Q->req['intOrder'];
		$intStatus = $Q->req['intStatus'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		
		$postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strQ = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strQ = htmlspecialchars( $postArray ) ;
	     

		 $postArray2 = &$_POST['editor2'] ;
		if(get_magic_quotes_gpc() )
				$strA = htmlspecialchars( stripslashes( $postArray2 ) ) ;
			else
				$strA = htmlspecialchars( $postArray2 ) ;
	      
		//
		// Do Add FAQ
		//
		$this->objAddFAQ = new core_admin();
		$res = $this->objAddFAQ->doAddFAQ(array(
			'intType'   => $intType,
			'intOrder'  => $intOrder,
			'intStatus' => $intStatus,
			'strAddedBy' => $strAddedBy,
			'strQ'       => $strQ,
			'strA'       => $strA
		
		));
		
		if($res > 0){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_faq_form&msg=1');
		}
		else{
		 
		 echo "Error : Insert Error FAQ";
		 exit;	
		}
	}
	//
	// Show FAQ Details
	//
	function show_faq_details(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_FAQ_deatails());
		$OUT->flush();
		
	}
	//
	// Page FAQ Details
	//
	function page_FAQ_deatails(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$id = $Q->req['id'];
		//
		// Do Get Information From Package
		//
		$this->objFAQ = new core_admin();
		$this->objFAQ->doGetFAQDetailsById($id);
		$this->objFAQ->GetNextRecord();
		$data['intFaqId']    = $this->objFAQ->Get('intFaqId');
        $data['intOrder']    = $this->objFAQ->Get('intOrder');
		$data['intType']     = $this->objFAQ->Get('intType');
		$data['strQ']        = $this->objFAQ->Get('strQ');
		$data['strA']        = $this->objFAQ->Get('strA');
		$data['intFaqId']    = $id;
		$data['intStatus']   = $this->objFAQ->Get('intStatus');
		
		
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/shw_FAQ_editForm.php',$data,$error);	
		exit;		
		
	}
	//
	// Do Edit FAQ
	//
	function do_edit_FAQ(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intType = $Q->req['intType'];
		
		$intOrder  = $Q->req['intOrder'];
		$intStatus = $Q->req['intStatus'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$id        = $Q->req['id'];
		
		$postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strQ = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strQ = htmlspecialchars( $postArray ) ;
	     

		 $postArray2 = &$_POST['editor2'] ;
		if(get_magic_quotes_gpc() )
				$strA = htmlspecialchars( stripslashes( $postArray2 ) ) ;
			else
				$strA = htmlspecialchars( $postArray2 ) ;
					
		//
		// Do Update FAQ
		//
		$this->obhUpdateFAQ = new core_admin();
		$this->obhUpdateFAQ->doUpdateFAQ(array(
		 'intType'  => $intType,
		 'intOrder' => $intOrder,
		 'intStatus' => $intStatus,
		 'strAddedBy' => $strAddedBy,
		 'strQ'       => $strQ,
		 'strA'       => $strA,
		 'id'         => $id));
		 
		
		 $OUT->redirect($CONF['url_app'].'?m=admin&c=show_faq_details&id='.$id.'&success=1');
	}
	
	
	//
	// Do Delete FAQ
	//
	function do_delete_FAQ(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;	
		 
		 $id =  decrypt($Q->req['id'],'cfoni.8888');
		 $strAddedBy   = $Q->cookies['admin:user_id'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_admin();
		 $res =  $this->obj->doDeleteFAQById($id,$strAddedBy);
		 
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_faq&msg=2'); 
		 }
		exit;
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	//
	// Show Message Link
	//
	function show_messagelink(){
	 global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_messagelink());
		$OUT->flush();
	}
	//
	// Page  Message Link
	//
	function page_messagelink(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetMessageLinkById($intId);
			$this->objedit->GetNextRecord();
			$data['msglinkedit']['intMsgId'] = $this->objedit->Get('intMsgId');
			$data['msglinkedit']['strMessage'] = $this->objedit->Get('strMessage');
			$data['msglinkedit']['strLink'] = $this->objedit->Get('strLink');
			$data['msglinkedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['msglinkedit']['CreateDate'] = $this->objedit->Get('CreateDate');
			
			}
		//
		// List Message
		//
		$this->obj = new core_admin();
		$this->obj->getAllMessage();
		while($this->obj->GetNextRecord()){
			$data['msglink'][$this->obj->Get('intMsgId')]['intMsgId']  =$this->obj->Get('intMsgId');
			$data['msglink'][$this->obj->Get('intMsgId')]['strMessage']    =$this->obj->Get('strMessage');
			$data['msglink'][$this->obj->Get('intMsgId')]['strLink']      =$this->obj->Get('strLink');
			$data['msglink'][$this->obj->Get('intMsgId')]['intStatus']=$this->obj->Get('intStatus');
			$data['msglink'][$this->obj->Get('intMsgId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		
		return $this->_temp('shw_msglink.php','NULL','NULL',$error,$data);	
	}
	
	
	
	
	//
	// Show Banner Lists
	//
	function show_banner_lists(){
			global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$OUT->setVar('content',	$this->page_main_banners());
		$OUT->flush();	
	}
	//
	// Page Main Banners
	//
	function page_main_banners(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		
	$error = $OUT->errors;
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetBannerById($intId);
			$this->objedit->GetNextRecord();
			$data['banneredit']['intBannerId'] = $this->objedit->Get('intBannerId');
			$data['banneredit']['intTypeBanner'] = $this->objedit->Get('intTypeBanner');
			$data['banneredit']['strBanner'] = $this->objedit->Get('strBanner');
			$data['banneredit']['strLink'] = $this->objedit->Get('strLink');
			$data['banneredit']['strCaption'] = $this->objedit->Get('strCaption');
			$data['banneredit']['intTypeBanner'] = $this->objedit->Get('intTypeBanner');
			$data['banneredit']['intOrder'] = $this->objedit->Get('intOrder');
			$data['banneredit']['intStatus'] = $this->objedit->Get('intStatus');
			}
		//
		// List Main Banner Images
		//
		$this->obj = new core_admin();
		$this->obj->getAllBanner();
		while($this->obj->GetNextRecord()){
			$data['banner'][$this->obj->Get('intBannerId')]['intBannerId']  =$this->obj->Get('intBannerId');
			$data['banner'][$this->obj->Get('intBannerId')]['strBanner']    =$this->obj->Get('strBanner');
			$data['banner'][$this->obj->Get('intBannerId')]['strLink']      =$this->obj->Get('strLink');
			$data['banner'][$this->obj->Get('intBannerId')]['strCaption']   =$this->obj->Get('strCaption');
			$data['banner'][$this->obj->Get('intBannerId')]['intTypeBanner']=$this->obj->Get('intTypeBanner');
			$data['banner'][$this->obj->Get('intBannerId')]['intOrder']=$this->obj->Get('intOrder');
			$data['banner'][$this->obj->Get('intBannerId')]['intStatus']=$this->obj->Get('intStatus');
			$data['banner'][$this->obj->Get('intBannerId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		return $this->_temp('show_mainbanner_pge.php',$mode,$c,$error,$data);
		
	}
	//
	// DO Add Banners
	//
	function do_add_banner(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$intBannerType     = $Q->req['intBannerType'];
		$strBannerLink     = $Q->req['strBannerlink'];
		$strBannerCaption  = $Q->req['strBannerCaption'];
		$intBannerOrder    = $Q->req['intBannerOrder'];
		$strBannerImage    = $_FILES['strBannerImage']['tmp_name'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		if($intBannerLink==""){
			$intBannerLink ="-";
			}
		if($intBannerCaption==""){
			$intBannerCaption="-";
			}
		if($intBannerOrder==""){
			$intBannerOrder="1";
			}
		//
		// Do Add Banners
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddBanner(array(
		     'intBannerType' => $intBannerType ,
		     'strBannerLink' => urldecode($strBannerLink) ,
		     'strBannerCaption' => $strBannerCaption ,
		     'intBannerOrder' => $intBannerOrder,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus
		     ));
		     
		     
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strBannerImage!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/mainbanner/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/mainbanner/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/mainbanner/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/mainbanner/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/mainbanner/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strBannerImage']['name'];
   				$arrTemp       = $_FILES['strBannerImage']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateBanner($id,$arrfilename);
	 				 }
		
		
		
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_banner_lists&msg=1');
		
				
	}
	//
	// Do Update Banner
	//
	function do_update_banner(){
			global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$intBannerType     = $Q->req['intBannerType'];
		$strBannerLink     = $Q->req['strBannerlink'];
		$strBannerCaption  = $Q->req['strBannerCaption'];
		$intBannerOrder    = $Q->req['intBannerOrder'];
		$strBannerImage    = $_FILES['strBannerImage']['tmp_name'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$id                = $Q->req['id'];
		if($intBannerLink==""){
			$intBannerLink ="-";
			}
		if($intBannerCaption==""){
			$intBannerCaption="-";
			}
		if($intBannerOrder==""){
			$intBannerOrder="1";
			}
		//
		// Do Add Banners
		//
		$this->obj = new core_admin();
		$result = $this->obj->doUpdateBanner(array(
		     'intBannerType' => $intBannerType ,
		     'strBannerLink' => urldecode($strBannerLink) ,
		     'strBannerCaption' => $strBannerCaption ,
		     'intBannerOrder' => $intBannerOrder,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
		     'intBannerId'    => $id
		     ));
		     
		   
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strBannerImage!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/mainbanner/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/mainbanner/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/mainbanner/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/mainbanner/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/mainbanner/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strBannerImage']['name'];
   				$arrTemp       = $_FILES['strBannerImage']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			}
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateBanner($id,$arrfilename);
	 				 }
		
		
		
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_banner_lists&msg=2');
		
		
		
	}
	//
	//Message Link
	//
	function do_add_messagelink(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strMessage        = $Q->req['strMessage'];
		$strLink           = $Q->req['strlink'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		//
		// Do Add Message Link
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddMessageLink(array(
		     'strMessage' => $strMessage ,
		     'strLink' => urldecode($strLink) ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus
		     ));
		     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_messagelink&msg=1');
		
				
	}
	
	//
	// Do update message link
	//
	function do_update_messagelink(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strMessage        = $Q->req['strMessage'];
		$strLink        = $Q->req['strlink'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		
		
	
	
		//
		// Do Add Message Link
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateMessageLink(array(
		     'strMessage'     => $strMessage ,
			 'strLink'        => urldecode($strLink) ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
			 'id'             => $Q->req['id']
		     ));
			 
			 
			 
			 //exit;
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_messagelink&msg=2');
	
	
	}
	//
	//Message Photo
	//
	function do_add_messagephoto(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$intSection        = $Q->req['intSection'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		$postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
	
		//
		// Do Add Message Photo
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddMessagePhoto(array(
		     'intSection' => $intSection ,
		     'strAddedBy'     => $strAddedBy,
			 'strDescription' => $strDescription,
		     'intStatus'      => $intStatus
		     ));
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_messagephoto&msg=1');
	}
	
	//
	// Do update message photo
	//
	function do_update_messagephoto(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$intSection        = $Q->req['intSection'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		$postArray = &$_POST['strMessagePhoto'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
	
	
	//echo $strDescription;
	
	//exit;
	
	
		//
		// Do Add Message Photo
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateMessagePhoto(array(
		     'intSection' => $intSection ,
		     'strAddedBy'     => $strAddedBy,
			 'strMessage'     => $strDescription,
		     'intStatus'      => $intStatus,
			 'id'             => $Q->req['id']
		     ));
			 
			 
			 
			 //exit;
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_messagephoto&msg=2');
	
	
	}
	
	
	
	
	
	
	
	
	
	//
	//Message Photo Solution
	//
	function do_add_messagephoto_solutions(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$intSection        = $Q->req['intSection'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		$postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
	
		//
		// Do Add Message Photo
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddMessagePhotoSolutions(array(
		     'intSection' => $intSection ,
		     'strAddedBy'     => $strAddedBy,
			 'strDescription' => $strDescription,
		     'intStatus'      => $intStatus
		     ));
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_networkphoto&msg=1');
	}
	
	//
	// Do update message photo Solutions
	//
	function do_update_messagephoto_solutions(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$intSection        = $Q->req['intSection'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		$postArray = &$_POST['strMessagePhoto'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
	
	
	//echo $strDescription;
	
	//exit;
	
	
		//
		// Do Add Message Photo
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateMessagePhotoSolutions(array(
		     'intSection' => $intSection ,
		     'strAddedBy'     => $strAddedBy,
			 'strMessage'     => $strDescription,
		     'intStatus'      => $intStatus,
			 'id'             => $Q->req['id']
		     ));
			 
			 
			 
			 //exit;
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_networkphoto&msg=2');
	
	
	}
	
	
	
	
	
	
	
	
	
	
	
	//
	//Manage Product
	//
	function do_add_manageproduct(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strProductName       = $Q->req['strProductName'];
		$strPhoto             = $_FILES['strPhoto']['tmp_name'];
		$intBrandId           = $Q->req['intBrandId'];
		$intCategoryId        = $Q->req['intCategoryId'];
		$intSubcategoryId     = $Q->req['intSubcategoryId'];
		$strSmallDescription       = $Q->req['strSmallDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$strURLStatus         = $Q->req['strURLStatus'];
		//$strURL             = $Q->req['strURL'];
		
		
		
	
	
		
		if($intURLStatus==1){
			$strURL = '-';
		}
else{
	
	$strURL             = $Q->req['strURL'];
}
		
		
		$postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckProductName($strProductName);
		$this->objvalidation->GetNextRecord();
		$strDBProductName = $this->objvalidation->Get('strProductName');
		
		
        if(strtolower($strProductName)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_manageproduct&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strProductName)==strtolower($strDBProductName)){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_manageproduct&errMsg=2');
			  
			  exit;
		
		}
		
		
		
		//
		// Do Add Manage Product
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddManageProduct(array(
		     'strProductName' => $strProductName ,
			 'intBrandId' => $intBrandId ,
			 'intCategoryId' => $intCategoryId ,
			 'intSubcategoryId' => $intSubcategoryId ,
			 'strSmallDescription' => $strSmallDescription ,
			 'strDescription' => $strDescription ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
		     'strURLStatus'   => $strURLStatus,
		     'strURL'         => $strURL 
		     ));
		
	
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/manageproductphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/manageproductphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/manageproductphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/manageproductphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/manageproductphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strPhoto']['name'];
   				$arrTemp       = $_FILES['strPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateManageProduct($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_manageproduct&msg=1');
		
				
	}
	
	//
	//Update Manage Product
	//
	function do_update_manageproduct(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strProductName       = $Q->req['strProductName'];
		$strPhoto             = $_FILES['strPhoto']['tmp_name'];
		$intBrandId           = $Q->req['intBrandId'];
		$intCategoryId        = $Q->req['intCategoryId'];
		$intSubcategoryId     = $Q->req['intSubcategoryId'];
		$strSmallDescription  = $Q->req['strSmallDescription'];
		$strAddedBy           = $Q->cookies['admin:user_id'];
		$intStatus            = $Q->req['intStatus'];
		$id                   = $Q->req['id'];
		
		$postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$strDescription = htmlspecialchars( $postArray ) ;
				
		$strURLStatus         = $Q->req['strURLStatus'];
	
		
		if($intURLStatus==1){
			$strURL = '-';
		}
     else{
		 $strURL             = $Q->req['strURL'];
       }	
		if(empty($Q->req['strURL'])){
			$strURL   = '-';
		}		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckProductName($strProductName);
		$this->objvalidation->GetNextRecord();
		$strDBProductName = $this->objvalidation->Get('strProductName');
		$strDBProductId = $this->objvalidation->Get('intProductId');
		
		
        if(strtolower($strProductName)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_manageproduct&type=1&id='.$id.'&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strProductName)==strtolower($strDBProductName)&& $id != $strDBProductId){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_manageproduct&id='.$id.'&errMsg=2');
			  
			  exit;
		
		}
		
		//
		// Do Update Manage Product
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateManageProduct(array(
		     'strProductName' => $strProductName ,
			 'intBrandId' => $intBrandId ,
			 'intCategoryId' => $intCategoryId ,
			 'intSubcategoryId' => $intSubcategoryId ,
			 'strDescription' => $strDescription ,
			 'strSmallDescription' => $strSmallDescription ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
		     'strURLStatus'   => $strURLStatus,
		     'strURL'         => $strURL,	     
			 'id' => $id
		     ));
		
		
	
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/manageproductphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/manageproductphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/manageproductphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/manageproductphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/manageproductphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strPhoto']['name'];
   				$arrTemp       = $_FILES['strPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateManageProduct($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_manageproduct&msg=2');
		
				
	}
	//
	//Network Photo
	//
	function do_add_networkphoto(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strTitle       = $Q->req['strTitle'];
		$strNetworkPhoto           = $_FILES['strNetworkPhoto']['tmp_name'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		
		//
		// Validation Network Photo
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckNetworkPhotoName($strTitle);
		$this->objvalidation->GetNextRecord();
		$strDBNetworkPhotoName = $this->objvalidation->Get('strTitle');
		
		
        if(strtolower($strTitle)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_networkphoto&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strTitle)==strtolower($strDBNetworkPhotoName)){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_networkphoto&errMsg=2');
			  
			  exit;
		
		}
		
		//
		// Do Add Network Photo
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddNetworkPhoto(array(
		     'strTitle' => $strTitle ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strNetworkPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/networkphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/networkphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/networkphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/networkphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/networkphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strNetworkPhoto']['name'];
   				$arrTemp       = $_FILES['strNetworkPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateNetworkPhoto($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_networkphoto&msg=1');
		
				
	}
	
	//
	//Do Update Network Photo
	//
	function do_update_networkphoto(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strTitle       = $Q->req['strTitle'];
		$strNetworkPhoto           = $_FILES['strNetworkPhoto']['tmp_name'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$id                 = $Q->req['id'];
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckNetworkPhotoName($strTitle);
		$this->objvalidation->GetNextRecord();
		$strDBNetworkPhotoName = $this->objvalidation->Get('strTitle');
		$strDBNetworkPhotoId = $this->objvalidation->Get('intNetworkId');
		
		
        if(strtolower($strTitle)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_networkphoto&type=1&id='.$id.'&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strTitle)==strtolower($strDBNetworkPhotoName)&& $id != $strDBNetworkPhotoId){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_networkphoto&id='.$id.'&errMsg=2');
			  
			  exit;
		
		}
		
		
	
		//
		// Do Add Network Photo
		//
		$this->obj = new core_admin();
		 $this->obj->doInsertUpdateNetworkPhoto(array(
		     'strTitle' => $strTitle ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
			 'id'      => $id
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strNetworkPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/networkphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/networkphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/networkphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/networkphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/networkphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strNetworkPhoto']['name'];
   				$arrTemp       = $_FILES['strNetworkPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateNetworkImage($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_networkphoto&msg=2');
		
				
	}
	//
	//Manage Information
	//
	function do_add_manageinformation(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strTitleinform       = $Q->req['strTitleinform'];
		$strMapPhoto           = $_FILES['strMapPhoto']['tmp_name'];
		$strTelefon       = $Q->req['strTelefon'];
		$strFax       = $Q->req['strFax'];
		$strEmail       = $Q->req['strEmail'];
		$strGoogleLink 				= htmlspecialchars($Q->req['strGoogleLink']);
		$strDescription       = $Q->req['strDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		//
		// Do Add Manage Information
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddManageinformation(array(
		     'strTitleinform' => $strTitleinform ,
			 'strTelefon' => $strTelefon ,
			 'strFax' => $strFax ,
			 'strEmail' => $strEmail ,
			 'strDescription' => $strDescription ,
			 'strGoogleLink'  => $strGoogleLink,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strMapPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/manageinformation/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/manageinformation/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/manageinformation/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/manageinformation/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/manageinformation/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strMapPhoto']['name'];
   				$arrTemp       = $_FILES['strMapPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateManageInformation($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_manageinformation&msg=1');
		
				
	}
	
	//
	//Update Manage Information
	//
	function do_update_manageinformation(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strTitleinform       = $Q->req['strTitleinform'];
		$strMapPhoto           = $_FILES['strMapPhoto']['tmp_name'];
		$strTelefon       = $Q->req['strTelefon'];
		$strFax       = $Q->req['strFax'];
		$strEmail       = $Q->req['strEmail'];
		$strDescription       = $Q->req['strDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$strGoogleLink 				= htmlspecialchars($Q->req['strGoogleLink']);
		$id                 = $Q->req['id'];
		
		
		
	
		//
		// Do Update Manage Information
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateManageinformation(array(
		     'strTitleinform' => $strTitleinform ,
			 'strTelefon' => $strTelefon ,
			 'strFax' => $strFax ,
			 'strEmail' => $strEmail ,
			 'strDescription' => $strDescription ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
		     'strGoogleLink' => $strGoogleLink, 
			 'id' => $id
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strMapPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/manageinformation/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/manageinformation/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/manageinformation/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/manageinformation/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/manageinformation/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strMapPhoto']['name'];
   				$arrTemp       = $_FILES['strMapPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateManageInformation($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_manageinformation&msg=2');
		
				
	}
	
	
	//
	//Campaign
	//
	function do_add_campaign(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strTitleCampaign       = $Q->req['strTitleCampaign'];
		$strCampaignPhoto           = $_FILES['strCampaignPhoto']['tmp_name'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckCampaignName($strTitleCampaign);
		$this->objvalidation->GetNextRecord();
		$strDBTitleCampaign = $this->objvalidation->Get('strTitle');
		
		
        if(strtolower($strTitleCampaign)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_campaign_listing&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strTitleCampaign)==strtolower($strDBTitleCampaign)){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_campaign_listing&errMsg=2');
			  
			  exit;
		
		}
		
		//
		// Do Add Campaign
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddCampaign(array(
		     'strTitleCampaign' => $strTitleCampaign ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strCampaignPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/campaignphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/campaignphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/campaignphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/campaignphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/campaignphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strCampaignPhoto']['name'];
   				$arrTemp       = $_FILES['strCampaignPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateCampaign($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_campaign_listing&msg=1');
		
				
	}
	
	//
	//Update Campaign
	//
	function do_update_campaign(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strTitleCampaign       = $Q->req['strTitleCampaign'];
		$strCampaignPhoto           = $_FILES['strCampaignPhoto']['tmp_name'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$id                 = $Q->req['id'];
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckCampaignName($strTitleCampaign);
		$this->objvalidation->GetNextRecord();
		$strDBTitleCampaign = $this->objvalidation->Get('strTitle');
		$strDBCampaignId = $this->objvalidation->Get('intCampaignId');
		
		
        if(strtolower($strTitleCampaign)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_campaign_listing&type=1&id='.$id.'&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strTitleCampaign)==strtolower($strDBTitleCampaign)&& $id != $strDBCampaignId){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_campaign_listing&id='.$id.'&errMsg=2');
			  
			  exit;
		
		}
		
		//
		// Do Update Campaign
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateCampaign(array(
		     'strTitleCampaign' => $strTitleCampaign ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
			 'id' => $id
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strCampaignPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/campaignphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/campaignphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/campaignphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/campaignphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/campaignphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strCampaignPhoto']['name'];
   				$arrTemp       = $_FILES['strCampaignPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateCampaign($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_campaign_listing&msg=2');
		
				
	}
	
	
	
	
	//
	//Question
	//
	function do_add_questionlistings(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strQuestion       = $Q->req['strQuestion'];
		$strPic           = $_FILES['strPic']['tmp_name'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$intCampaignId     = $Q->req['intCampaignId'];
		
	
		
		//
		// Do Add Question
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddQuestion(array(
		     'strQuestion' => $strQuestion ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
			 'intCampaignId'  => $intCampaignId
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strPic!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/questionphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/questionphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/questionphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/questionphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/questionphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strPic']['name'];
   				$arrTemp       = $_FILES['strPic']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateQuestion($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_question_listings&intCampaignId='.$intCampaignId.'&msg=1');
		
				
	}
	
	//
	//Update Question
	//
	function do_update_questionlistings(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strQuestion       = $Q->req['strQuestion'];
		$strPic           = $_FILES['strPic']['tmp_name'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intAnswer         = $Q->req['intAnswer'];
		$intStatus         = $Q->req['intStatus'];
		$id                 = $Q->req['id'];
		$intCampaignId      = $Q->req['intCampaignId'];
		//
		// Do Update Question
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateQuestion(array(
		     'strQuestion' => $strQuestion ,
		     'strAddedBy'     => $strAddedBy,
			 'intAnswer'      => $intAnswer,
		     'intStatus'      => $intStatus,
			 'id' => $id
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strPic!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/questionphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/questionphoto/'.$folder_id.'/',0777);

							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/questionphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/questionphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/questionphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strPic']['name'];
   				$arrTemp       = $_FILES['strPic']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateQuestion($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_question_listings&msg=2&intCampaignId='.$intCampaignId);
		
				
	}
	
	
	//
	//Answer Selected
	//
	function do_add_answerselected(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intOrder          = $Q->req['intOrder'];     
		$strAnswer         = $Q->req['strAnswer'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$intCampaignId     = $Q->req['intCampaignId'];
		$intQuestionId     = $Q->req['intQuestionId'];
		//
		// Do Add Answer Selected
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddAnswerSelected(array(
		     'intOrder'      => $intOrder,
		     'strAnswer'     => $strAnswer,
		     'strAddedBy'     => $strAddedBy,
			 'intCampaignId'   => $intCampaignId,
			 'intQuestionId' => $intQuestionId,
		     'intStatus'      => $intStatus
		     ));
	     
		
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_answerselected&msg=1&intQuestionId='.$intQuestionId.'&intCampaignId='.$intCampaignId);
		
				
	}
	
	//
	//Update Answer Selected
	//
	function do_update_answerselected(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$intOrder       = $Q->req['intOrder'];
		$strAnswer       = $Q->req['strAnswer'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$intQuestionId                 = $Q->req['intQuestionId'];
		$intCampaignId                 = $Q->req['intCampaignId'];
		$id                 = $Q->req['id'];
		
		
		
		
		
		//
		// Do Update Answer Selected
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateAnswerSelected(array(
		     'intOrder'       => $intOrder ,
			 'strAnswer'     => $strAnswer,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
			 'id' => $id
		     ));


	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_answerselected&msg=2&intQuestionId='.$intQuestionId.'&intCampaignId='.$intCampaignId);
		
				
	}
	//
	//New Brand
	//
	function do_add_brand(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strBrandName       = $Q->req['strBrandName'];
		$strBrandImage           = $_FILES['strBrandImage']['tmp_name'];
		$strDescription       = $Q->req['strDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		$intBrandOrder         = $Q->req['intBrandOrder'];
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckBrandName($strBrandName);
		$this->objvalidation->GetNextRecord();
		$strDBBrandName = $this->objvalidation->Get('strBrandName');
		
		
        if(strtolower($strBrandName)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_brand&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strBrandName)==strtolower($strDBBrandName)){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_brand&errMsg=2');
			  
			  exit;
		
		}
		
		
		$postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$filterstrDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$filterstrDescription = htmlspecialchars( $postArray ) ;
		
		
		
		
		
		//
		// Do Add Brand
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddBrand(array(
			 'intBrandOrder' => $intBrandOrder ,							   
		     'strBrandName' => $strBrandName ,
			 'strDescription' => $strDescription ,
			 'LongDescription' => $filterstrDescription,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strBrandImage!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/brandphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/brandphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/brandphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/brandphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/brandphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strBrandImage']['name'];
   				$arrTemp       = $_FILES['strBrandImage']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateBrand($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_brand&msg=1');
		
				
	}
	
	
	//
	//Update Brand
	//
	function do_update_brand(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strBrandName       = $Q->req['strBrandName'];
		$strBrandImage           = $_FILES['strBrandImage']['tmp_name'];
		$strDescription       = $Q->req['strDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$id                 = $Q->req['id'];
		$intOrder           = $Q->req['intBrandOrder'];
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckBrandName($strBrandName);
		$this->objvalidation->GetNextRecord();
		$strDBBrandName = $this->objvalidation->Get('strBrandName');
		$strDBBrandId = $this->objvalidation->Get('intBrandId');
		
		
        if(strtolower($strBrandName)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_brand&type=1&id='.$id.'&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strBrandName)==strtolower($strDBBrandName)&& $id != $strDBBrandId){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_brand&id='.$id.'&errMsg=2');
			  
			  exit;
		
		}
		$postArray = &$_POST['editor1'] ;
		if(get_magic_quotes_gpc() )
				$filterstrDescription = htmlspecialchars( stripslashes( $postArray ) ) ;
			else
				$filterstrDescription = htmlspecialchars( $postArray ) ;
		//
		// Do Update Brand
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateBrand(array(
			 'intOrder'    => $intOrder ,							 
		     'strBrandName' => $strBrandName ,
			 'strDescription' => $strDescription ,
			 'LongDesc' => $filterstrDescription ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
			 'id' => $id
		     ));
			 
			 
		
	
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strBrandImage!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/brandphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/brandphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/brandphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/brandphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/brandphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strBrandImage']['name'];
   				$arrTemp       = $_FILES['strBrandImage']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateBrand($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_brand&msg=2');
		
				
	}
	//
	//Manage Category
	//
	function do_add_category(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strCategoryName       = $Q->req['strCategoryName'];
		$strCategoryPhoto           = $_FILES['strCategoryPhoto']['tmp_name'];
		$strDescription       = $Q->req['strDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intOrder         = $Q->req['intOrder'];
		$intStatus         = $Q->req['intStatus'];
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckCategoryName($strCategoryName);
		$this->objvalidation->GetNextRecord();
		$strDBCategoryName = $this->objvalidation->Get('strCategoryName');
		
		
        if(strtolower($strCategoryName)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_mcategory&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strCategoryName)==strtolower($strDBCategoryName)){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_mcategory&errMsg=2');
			  
			  exit;
		
		}
		
		//
		// Do Add Manage Category
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddCategory(array(
		     'strCategoryName' => $strCategoryName ,
			 'strDescription' => $strDescription ,
			 'intOrder'      => $intOrder,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus
		     ));
			 
			 
			 
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strCategoryPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/categoryphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/categoryphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/categoryphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/categoryphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/categoryphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strCategoryPhoto']['name'];
   				$arrTemp       = $_FILES['strCategoryPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
				
				//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateCategory($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_mcategory&msg=1');
	}			
	//
	//Update Manage Category
	//
	function do_update_category(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strCategoryName       = $Q->req['strCategoryName'];
		$strCategoryPhoto           = $_FILES['strCategoryPhoto']['tmp_name'];
		$strDescription       = $Q->req['strDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$id                 = $Q->req['id'];
		$intOrder         = $Q->req['intOrder'];
		
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckCategoryName($strCategoryName);
		$this->objvalidation->GetNextRecord();
		$strDBCategoryName = $this->objvalidation->Get('strCategoryName');
		$intDBCategoryId = $this->objvalidation->Get('intCategoryId');
		
		
        if(strtolower($strCategoryName)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_mcategory&type=1&id='.$id.'&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strCategoryName)==strtolower($strDBCategoryName) && $id!=$intDBCategoryId){
		
			   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_mcategory&type=1&id='.$id.'&errMsg=2');
			  
			  exit;
		
		}
		
				
		
		
		//
		// Do Update Manage Category
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateManageCategory(array(
		     'strCategoryName' => $strCategoryName ,
			 'intOrder'       => $intOrder ,
			 'strDescription' => $strDescription ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
			 'id' => $id
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strCategoryPhoto!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/categoryphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/categoryphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/categoryphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/categoryphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/categoryphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strCategoryPhoto']['name'];
   				$arrTemp       = $_FILES['strCategoryPhoto']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateCategory($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_mcategory&msg=2');
		
				
	}
	
	
	//
	//Manage Subcategory
	//
	function do_add_subcategory(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strSubCategoryName       = $Q->req['strSubCategoryName'];
		$strPic           = $_FILES['strPic']['tmp_name'];
		$strDescription       = $Q->req['strDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$intCateId          = $Q->req['intCategoryId'];
		$intOrder          = $Q->req['intOrder'];
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckSubcategoryName($strSubCategoryName);
		$this->objvalidation->GetNextRecord();
		$strDBSubCategoryName = $this->objvalidation->Get('strSubCategoryName');
		
		
        if(strtolower($strSubCategoryName)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_subcategory&id=3&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strSubCategoryName)==strtolower($strDBSubCategoryName)){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_subcategory&id=3&errMsg=2');
			  
			  exit;
		
		}
		
		//
		// Do Add Manage Subcategory
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddSubcategory(array(
		     'strSubCategoryName' => $strSubCategoryName ,
			 'strDescription' => $strDescription ,
		     'strAddedBy'     => $strAddedBy,
			 'intCateId'      => $intCateId,
			 'intOrder'       => $intOrder,
		     'intStatus'      => $intStatus
		     ));
		
	
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strPic!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/subcategoryphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/subcategoryphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/subcategoryphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/subcategoryphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/subcategoryphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strPic']['name'];
   				$arrTemp       = $_FILES['strPic']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
				
				//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateSubcategory($id,$arrfilename);
	 				 }
				     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_subcategory&msg=1&id='.$intCateId);
	}			
	//
	//Update Subcategory
	//
	function do_update_subcategory(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strSubCategoryName       = $Q->req['strSubCategoryName'];
		$strPic           = $_FILES['strPic']['tmp_name'];
		$strDescription       = $Q->req['strDescription'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		$id                 = $Q->req['id'];
		$intCategoryId      = $Q->req['intCategoryId'];
		$intOrder          = $Q->req['intOrder'];
		
		
		
		
		
		
		
		//
		// Validation
		//
		$this->objvalidation = new core_admin();
		$this->objvalidation->doCheckSubcategoryName($strSubCategoryName);
		$this->objvalidation->GetNextRecord();
		$strDBSubCategoryName = $this->objvalidation->Get('strSubCategoryName');
		$intDBSubCategoryId = $this->objvalidation->Get('intSubCategoryId');
		
		
        if(strtolower($strSubCategoryName)==""){
		
			  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_subcategory&type=1&intSubCategorid='.$id.'&id='.$intCategoryId.'&errMsg=1');
			  
			  exit;
		}		
		
		
		if(strtolower($strSubCategoryName)==strtolower($strDBSubCategoryName) && $id!=$intDBSubCategoryId){
		
			   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_subcategory&type=1&intSubCategorid='.$id.'&id='.$intCategoryId.'&errMsg=2');
			  
			  exit;
		
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//
		// Do Update Subcategory
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateSubcategory(array(
		     'strSubCategoryName' => $strSubCategoryName ,
			 'strDescription' => $strDescription ,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus,
			 'intOrder'       => $intOrder,
			
			 'id' => $id
		     ));
		
		
		
		
			$folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
        	if($strPic!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/subcategoryphoto/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/subcategoryphoto/'.$folder_id.'/',0777);
							}
		
			  		 if(!file_exists($CONF['dir_photo'  ].'/subcategoryphoto/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/subcategoryphoto/'.$folder_id.'/'.$id.'/',0777);
							}
							
							
							
   			  $upload_path   = $CONF['dir_photo'  ].'/subcategoryphoto/'.$folder_id.'/'.$id.'/'; 
   			  
   			  
   		
 	 	 			$arrfilename	 = $_FILES['strPic']['name'];
   				$arrTemp       = $_FILES['strPic']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				die('The file you attempted to upload is not allowed.');}
				  if(filesize($arrTemp) > $max_filesize){
      			die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			}
   				if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			
      				}
      		else{
         			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             }
			  		
			
			    }
		 			//
		 			// Update Image
		 			//
					if($found==1){
		 				$this->objupdate = new core_admin();
		 				$this->objupdate->doInsertUpdateSubCategory($id,$arrfilename);
	 				 }
		  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_subcategory&type=1&intSubCategorid='.$id.'&id='.$intCategoryId.'&msg=2');		     
		     
	   //$OUT->redirect($CONF['url_app'].'?m=admin&c=show_subcategory&msg=2&id='.$id.'&intSubCategoryId=');
		
				
	}
	
	//
	//Feedback
	//
	function do_add_feedback(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strName        = $Q->req['strName'];
		$strCompanyName           = $Q->req['strCompanyName'];
		$strEmail           = $Q->req['strEmail'];
		$strTel           = $Q->req['strTel'];
		$strFax           = $Q->req['strFax'];
		$strWebsite           = $Q->req['strWebsite'];
		$strMessage           = $Q->req['strMessage'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		//
		// Do Add Feedback
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddFeedback(array(
		     'strName' => $strName,
		    'strMessage' => $strMessage,
			'strCompanyName' => $strCompanyName,
			'strEmail' => $strEmail,
			'strTel' => $strTel,
			'strFax' => $strFax,
			'strWebsite' => $strWebsite,
			'strMessage' => $strMessage,
		     'strAddedBy'     => $strAddedBy,
		     'intStatus'      => $intStatus
		     ));
		     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_feedback&msg=1');
		
				
	}
	
	//
	// Do update feedback
	//
	function do_update_feedback(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strName        = $Q->req['strName'];
		$strCompanyName           = $Q->req['strCompanyName'];
		$strEmail           = $Q->req['strEmail'];
		$strTel           = $Q->req['strTel'];
		$strFax           = $Q->req['strFax'];
		$strWebsite           = $Q->req['strWebsite'];
		$strMessage           = $Q->req['strMessage'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		$intStatus         = $Q->req['intStatus'];
		
		
		//
		// Do Add Feedback
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateFeedback(array(
		     'strName' => $strName,
		    'strMessage' => $strMessage,
			'strCompanyName' => $strCompanyName,
			'strEmail' => $strEmail,
			'strTel' => $strTel,
			'strFax' => $strFax,
			'strWebsite' => $strWebsite,
			'strMessage' => $strMessage,
		    'strAddedBy'     => $strAddedBy,
		    'intStatus'      => $intStatus,
			'id'             => $Q->req['id']
		     ));
			 
			 
			 
			 //exit;
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_feedback&msg=2');
	
	
	}
	
	
	//
	//Customer Report
	//
	function do_add_customereport(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strName        = $Q->req['strName'];
		$strCompanyName           = $Q->req['strCompanyName'];
		$strEmail           = $Q->req['strEmail'];
		$strTel           = $Q->req['strTel'];

		
		//
		// Do Add Customer Report
		//
		$this->obj = new core_admin();
		$id = $this->obj->doAddCustomerReport(array(
		     'strName' => $strName,
		    'strMessage' => $strMessage,
			'strCompanyName' => $strCompanyName,
			'strEmail' => $strEmail,
			'strTel' => $strTel

		     ));
		     
		     
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_customereport&msg=1');
		
				
	}
	
	//
	// Do update Customer Report
	//
	function do_update_customereport(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		     
		$strName            = $Q->req['strName'];
		$strCompanyName     = $Q->req['strCompanyName'];
		$strEmail           = $Q->req['strEmail'];
		$strTel             = $Q->req['strTel'];
		//
		// Do Add Customer Report
		//
		$this->obj = new core_admin();
		 $this->obj->doUpdateCustomerReport(array(
		    'strName'        => $strName,
			'strCompanyName' => $strCompanyName,
			'strEmail'       => $strEmail,
			'strTel'         => $strTel,
			'id'             => $Q->req['id']
		     ));
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_customereport&msg=2');
	
	
	}
	
	
	//
	// Show Message Photo
	//
	function show_messagephoto(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_messagephoto());
		$OUT->flush();
	}
	//
	// Page  Message Photo
	//
	function page_messagephoto(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;
		
		$error = $OUT->errors;
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetMessagePhotoById($intId);
			$this->objedit->GetNextRecord();
			$data['messagephotoedit']['intMshCeoId'] = $this->objedit->Get('intMshCeoId');
			$data['messagephotoedit']['intSection'] = $this->objedit->Get('intSection');
			$data['messagephotoedit']['strMessage'] = $this->objedit->Get('strMessage');
			$data['messagephotoedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['messagephotoedit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
				
		//
		// List Message Photo
		//
		$this->obj = new core_admin();
		$this->obj->getAllMessagePhoto();
		while($this->obj->GetNextRecord()){
			$data['msgphoto'][$this->obj->Get('intMshCeoId')]['intMshCeoId']      =$this->obj->Get('intMshCeoId');
			$data['msgphoto'][$this->obj->Get('intMshCeoId')]['intSection']       = $CODE['type_section'][$this->obj->Get('intSection')];
			$data['msgphoto'][$this->obj->Get('intMshCeoId')]['strMessage']      =$this->obj->Get('strMessage');
			$data['msgphoto'][$this->obj->Get('intMshCeoId')]['intStatus']       =$this->obj->Get('intStatus');
			$data['msgphoto'][$this->obj->Get('intMshCeoId')]['CreateDate']      =$this->obj->Get('CreateDate');
			
			$data['section_used'][$this->obj->Get('intSection')] = $CODE['type_section'][$this->obj->Get('intSection')];
		}
		
		return $this->_temp('shw_msgphoto.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Company Back
	//
	function show_companyback(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_companyback());
		$OUT->flush();
	}
	//
	// Page  Company Back
	//
	function page_companyback(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		return $this->_temp('shw_companyback.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Message photo ma
	//
	function show_messagephotoma(){
	   global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_messagephotoma());
		$OUT->flush();
	}
	//
	// Page  Message photo ma
	//
	function page_messagephotoma(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		return $this->_temp('shw_msgphotoma.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Manage Product
	//
	function show_manageproduct(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_manageproduct());
		$OUT->flush();
	}
	//
	// Page  Manage Product
	//
	function page_manageproduct(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;
		
		$error = $OUT->errors;
		//
		// Get Brand
		//
		$this->objbrand = new core_admin();
		$this->objbrand->getAllBrand();
		while($this->objbrand->GetNextRecord()){
		
		 $data['brand'][$this->objbrand->Get('intBrandId')] = $this->objbrand->Get('strBrandName'); 
		
		}
		//
		// Get Category
		//
		$this->objcategory = new core_admin();
		$this->objcategory->getAllManageCategory();
		while($this->objcategory->GetNextRecord()){
		
		 $data['category'][$this->objcategory->Get('intCategoryId')] = $this->objcategory->Get('strCategoryName'); 
		
		}
		
		//
		// Get Subcategory
		//
		$this->objsubcategory = new core_admin();
		$this->objsubcategory->getAllSubCate();
		while($this->objsubcategory->GetNextRecord()){
		
		 $data['subcategory'][$this->objsubcategory->Get('intCategoryId')][$this->objsubcategory->Get('intSubCategoryId')] = $this->objsubcategory->Get('strSubCategoryName'); 
		
		}
		
		
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetManageProductById($intId);
			$this->objedit->GetNextRecord();
			$data['manageproductedit']['intProductId'] = $this->objedit->Get('intProductId');
			$data['manageproductedit']['strProductName'] = $this->objedit->Get('strProductName');
			$data['manageproductedit']['strPhoto'] = $this->objedit->Get('strPhoto');
			$data['manageproductedit']['strDescription'] = $this->objedit->Get('strDescription');
			$data['manageproductedit']['strSmallDescription'] = $this->objedit->Get('strSmallDescription');
			$data['manageproductedit']['intBrandId'] = $this->objedit->Get('intBrandId');
			$data['manageproductedit']['intCategoryId'] = $this->objedit->Get('intCategoryId');
			$data['manageproductedit']['intSubcategoryId'] = $this->objedit->Get('intSubcategoryId');
			$data['manageproductedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['manageproductedit']['CreateDate'] = $this->objedit->Get('CreateDate');
			$data['manageproductedit']['strURLStatus'] = $this->objedit->Get('strURLStatus');
			$data['manageproductedit']['strURL'] = $this->objedit->Get('strURL');
		}
		
		//
		// List Manage Product
		//
		$this->obj = new core_admin();
		$this->obj->getAllManageProduct();
		while($this->obj->GetNextRecord()){
			$data['product'][$this->obj->Get('intProductId')]['intProductId']  =$this->obj->Get('intProductId');
			$data['product'][$this->obj->Get('intProductId')]['strProductName']    =$this->obj->Get('strProductName');
			$data['product'][$this->obj->Get('intProductId')]['strPhoto']      =$this->obj->Get('strPhoto');
			$data['product'][$this->obj->Get('intProductId')]['strDescription']    =$this->obj->Get('strDescription');
			$data['product'][$this->obj->Get('intProductId')]['strSmallDescription']    =$this->obj->Get('strSmallDescription');
			$data['product'][$this->obj->Get('intProductId')]['intBrandId']    = $this->obj->Get('intBrandId');
			$data['product'][$this->obj->Get('intProductId')]['intCategoryId']    = $this->obj->Get('intCategoryId');
			$data['product'][$this->obj->Get('intProductId')]['intSubcategoryId']    = $this->obj->Get('intSubcategoryId');
			$data['product'][$this->obj->Get('intProductId')]['intStatus']=$this->obj->Get('intStatus');
			$data['product'][$this->obj->Get('intProductId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		
		return $this->_temp('shw_manageproduct.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Network Photo
	//
	function show_networkphoto(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_networkphoto());
		$OUT->flush();
	}
	//
	// Page  Network Photo
	//
	function page_networkphoto(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetMessagePhotoSolutionById($intId);
			$this->objedit->GetNextRecord();
			$data['messagephotoedit']['intMshCeoId'] = $this->objedit->Get('intSolutionId');
			$data['messagephotoedit']['intSection'] = $this->objedit->Get('intSection');
			$data['messagephotoedit']['strMessage'] = $this->objedit->Get('strMessage');
			$data['messagephotoedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['messagephotoedit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
				
		//
		// List Message Photo
		//
		$this->obj = new core_admin();
		$this->obj->getAllMessagePhotoSolution();
		while($this->obj->GetNextRecord()){
			$data['msgphoto'][$this->obj->Get('intSolutionId')]['intMshCeoId']      =$this->obj->Get('intSolutionId');
			$data['msgphoto'][$this->obj->Get('intSolutionId')]['intSection']       = $this->obj->Get('intSection');
			$data['msgphoto'][$this->obj->Get('intSolutionId')]['strMessage']      =$this->obj->Get('strMessage');
			$data['msgphoto'][$this->obj->Get('intSolutionId')]['intStatus']       =$this->obj->Get('intStatus');
			$data['msgphoto'][$this->obj->Get('intSolutionId')]['CreateDate']      =$this->obj->Get('CreateDate');
			
			$data['section_used'][$this->obj->Get('intSection')] = $CODE['type_section'][$this->obj->Get('intSection')];
		}
		
		return $this->_temp('shw_solutions.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Campaign
	//
	function show_campaign_listing(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_campaignlisting());
		$OUT->flush();
	}
	//
	// Page  Campaign
	//
	function page_campaignlisting(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetCampaignById($intId);
			$this->objedit->GetNextRecord();
			$data['campaignedit']['intCampaignId'] = $this->objedit->Get('intCampaignId');
			$data['campaignedit']['strTitleCampaign'] = $this->objedit->Get('strTitle');
			$data['campaignedit']['strCampaignPhoto'] = $this->objedit->Get('strPic');
			$data['campaignedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['campaignedit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
		
		
		//
		// List Campaign
		//
		$this->obj = new core_admin();
		$this->obj->getAllCampaign();
		while($this->obj->GetNextRecord()){
			$data['campaign'][$this->obj->Get('intCampaignId')]['intCampaignId']  =$this->obj->Get('intCampaignId');
			$data['campaign'][$this->obj->Get('intCampaignId')]['strTitleCampaign']    =$this->obj->Get('strTitle');
			$data['campaign'][$this->obj->Get('intCampaignId')]['strCampaignPhoto']      =$this->obj->Get('strPic');
			$data['campaign'][$this->obj->Get('intCampaignId')]['intStatus']=$this->obj->Get('intStatus');
			$data['campaign'][$this->obj->Get('intCampaignId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		return $this->_temp('shw_campaignlisting.php','NULL','NULL',$error,$data);	
	}
	
	//
	// Show Question
	//
	function show_question_listings(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_questionlistings());
		$OUT->flush();
	}
	//
	// Page  Question
	//
	function page_questionlistings(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;
		
		$error = $OUT->errors;
		$intCampaignId = $Q->req['intCampaignId'];
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetQuestionById($intId);
			$this->objedit->GetNextRecord();
			$data['questionedit']['intQuestionId'] = $this->objedit->Get('intQuestionId');
			$data['questionedit']['strQuestion'] = $this->objedit->Get('strQuestion');
			$data['questionedit']['strPic'] = $this->objedit->Get('strPic');
			$data['questionedit']['intAnswer'] = $this->objedit->Get('intAnswer');
			$data['questionedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['questionedit']['CreateDate'] = $this->objedit->Get('CreateDate');
			$intQuestionId  = $Q->req['id'];
				//
		// List Answer Selected
		//
		$this->objAnswerforQ = new core_admin();
		$this->objAnswerforQ->doGetAnswerSelect($intQuestionId,$intCampaignId);
		while($this->objAnswerforQ->GetNextRecord()){
		 $data['ans_select'][$this->objAnswerforQ->Get('intAnswerSelectionId')] =  $CODE['type_answer'][$this->objAnswerforQ->Get('intOrder')].":".$this->objAnswerforQ->Get('strAnswer');
	    	}
    	}
		
		//
		// Get All Campaign
		//
		$this->objcampaign = new core_admin();
		$this->objcampaign->getAllCampaign();
		while($this->objcampaign->GetNextRecord()){
			$data['campaign_name'][$this->objcampaign->Get('intCampaignId')]['campaign_name'] =  $this->objcampaign->Get('strTitle');
		
		}
		//
		// List Question
		//
		$this->obj = new core_admin();
		$this->obj->getAllQuestionByCampaignId($intCampaignId);
		while($this->obj->GetNextRecord()){
			$data['campaign_question'][$this->obj->Get('intQuestionId')]['intQuestionId']  =$this->obj->Get('intQuestionId');
			$data['campaign_question'][$this->obj->Get('intQuestionId')]['intCampaignId']  =$this->obj->Get('intCampaignId');
			$data['campaign_question'][$this->obj->Get('intQuestionId')]['strQuestion']    =$this->obj->Get('strQuestion');
			$data['campaign_question'][$this->obj->Get('intQuestionId')]['strPic']      =$this->obj->Get('strPic');
			$data['campaign_question'][$this->obj->Get('intQuestionId')]['intAnswer']=$this->obj->Get('intAnswer');
			$data['campaign_question'][$this->obj->Get('intQuestionId')]['intStatus']=$this->obj->Get('intStatus');
			$data['campaign_question'][$this->obj->Get('intQuestionId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		return $this->_temp('shw_question_listings.php','NULL','NULL',$error,$data);	
	}
	
	
	//
	// Show Answer Selected
	//
	function show_answerselected(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_answerselected());
		$OUT->flush();
	}
	//
	// Page  Answer Selected
	//
	function page_answerselected(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetAnswerSelectedById($intId);
			$this->objedit->GetNextRecord();
			$data['answerselectededit']['intAnswerSelectionId'] = $this->objedit->Get('intAnswerSelectionId');
			$data['answerselectededit']['intOrder'] = $this->objedit->Get('intOrder');
			$data['answerselectededit']['strAnswer'] = $this->objedit->Get('strAnswer');
			$data['answerselectededit']['intQuestionId'] = $this->objedit->Get('intQuestionId');
			$data['answerselectededit']['intCampaignId'] = $this->objedit->Get('intCampaignId');
			$data['answerselectededit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['answerselectededit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
		
		$intQuestionId = $Q->req['intQuestionId'];
		$intCampaignId = $Q->req['intCampaignId'];
		//
		// Get Question
		//
		$this->objquestion = new core_admin();
		$this->objquestion->doGetQuestionById($intQuestionId);
		$this->objquestion->GetNextRecord();

		$data['strQuestion'] = $this->objquestion->Get('strQuestion');
		
		$this->obj = new core_admin();
		$this->obj->getAllAnswerSelected($intQuestionId,$intCampaignId);
		while($this->obj->GetNextRecord()){
			$data['campaign_answer_selection'][$this->obj->Get('intAnswerSelectionId')]['intAnswerSelectionId']  =$this->obj->Get('intAnswerSelectionId');
			$data['campaign_answer_selection'][$this->obj->Get('intAnswerSelectionId')]['intOrder']=$this->obj->Get('intOrder');
			$data['campaign_answer_selection'][$this->obj->Get('intAnswerSelectionId')]['strAnswer']    =$this->obj->Get('strAnswer');
			$data['campaign_answer_selection'][$this->obj->Get('intAnswerSelectionId')]['intStatus']=$this->obj->Get('intStatus');
			$data['campaign_answer_selection'][$this->obj->Get('intAnswerSelectionId')]['CreateDate']=$this->obj->Get('CreateDate');
			$data['campaign_answer_selection'][$this->obj->Get('intAnswerSelectionId')]['intQuestionId'] = $this->obj->Get('intQuestionId');
			$data['campaign_answer_selection'][$this->obj->Get('intAnswerSelectionId')]['intCampaignId'] = $this->obj->Get('intCampaignId');
			
		}
		
		return $this->_temp('shw_answerselected.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Manage Inform
	//
	function show_manageinformation(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_manageinformation());
		$OUT->flush();
	}
	//
	// Page  Manage Inform
	//
	function page_manageinformation(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetManageInformById($intId);
			$this->objedit->GetNextRecord();
			$data['manageinformedit']['intContactUsId'] = $this->objedit->Get('intContactUsId');
			$data['manageinformedit']['strTitleinform'] = $this->objedit->Get('strTitle');
			$data['manageinformedit']['strMapPhoto'] = $this->objedit->Get('strMapPhoto');
			$data['manageinformedit']['strDescription'] = $this->objedit->Get('strAdd');
			$data['manageinformedit']['strGoogleLink'] = $this->objedit->Get('strGoogleLink');
			$data['manageinformedit']['strTelefon'] = $this->objedit->Get('strTel');
			$data['manageinformedit']['strFax'] = $this->objedit->Get('strFax');
			$data['manageinformedit']['strEmail'] = $this->objedit->Get('strEmail');
			$data['manageinformedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['manageinformedit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
		
		//
		// List Manage Inform
		//
		$this->obj = new core_admin();
		$this->obj->getAllManageInform();
		while($this->obj->GetNextRecord()){
			$data['contactus'][$this->obj->Get('intContactUsId')]['intContactUsId']  =$this->obj->Get('intContactUsId');
			$data['contactus'][$this->obj->Get('intContactUsId')]['strTitleinform']    =$this->obj->Get('strTitle');
			$data['contactus'][$this->obj->Get('intContactUsId')]['strMapPhoto']      =$this->obj->Get('strMapPhoto');
			$data['contactus'][$this->obj->Get('intContactUsId')]['strDescription']    =$this->obj->Get('strAdd');
			$data['contactus'][$this->obj->Get('intContactUsId')]['strTelefon']    =$this->obj->Get('strTel');
			$data['contactus'][$this->obj->Get('intContactUsId')]['strFax']    =$this->obj->Get('strFax');
			$data['contactus'][$this->obj->Get('intContactUsId')]['strEmail']    =$this->obj->Get('strEmail');
			$data['contactus'][$this->obj->Get('intContactUsId')]['intStatus']=$this->obj->Get('intStatus');
			$data['contactus'][$this->obj->Get('intContactUsId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		
		return $this->_temp('shw_manageinform.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Feedback
	//
	function show_feedback(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_feedback());
		$OUT->flush();
	}
	//
	// Page  Feedback
	//
	function page_feedback(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetFeedbackById($intId);
			$this->objedit->GetNextRecord();
			$data['feedbackedit']['intFeedbackId'] = $this->objedit->Get('intFeedbackId');
			$data['feedbackedit']['strName'] = $this->objedit->Get('strName');
			$data['feedbackedit']['strCompanyName'] = $this->objedit->Get('strCompanyName');
			$data['feedbackedit']['strEmail'] = $this->objedit->Get('strEmail');
			$data['feedbackedit']['strTel'] = $this->objedit->Get('strTel');
			
			$data['feedbackedit']['strFax'] = $this->objedit->Get('strFax');
			$data['feedbackedit']['strWebsite'] = $this->objedit->Get('strWebsite');
			$data['feedbackedit']['strMessage'] = $this->objedit->Get('strMessage');
			$data['feedbackedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['feedbackedit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
				
		//
		// List Feedback
		//
		$this->obj = new core_admin();
		$this->obj->getAllFeedback();
		while($this->obj->GetNextRecord()){
			$data['feedback'][$this->obj->Get('intFeedbackId')]['intFeedbackId']  =$this->obj->Get('intFeedbackId');
			$data['feedback'][$this->obj->Get('intFeedbackId')]['strName']      =$this->obj->Get('strName');
			$data['feedback'][$this->obj->Get('intFeedbackId')]['strCompanyName']      =$this->obj->Get('strCompanyName');
			$data['feedback'][$this->obj->Get('intFeedbackId')]['strEmail']      =$this->obj->Get('strEmail');
			$data['feedback'][$this->obj->Get('intFeedbackId')]['strTel']      =$this->obj->Get('strTel')." <br> ".$this->obj->Get('strMobile');;
			$data['feedback'][$this->obj->Get('intFeedbackId')]['strFax']      =$this->obj->Get('strFax');
			$data['feedback'][$this->obj->Get('intFeedbackId')]['strWebsite']      =$this->obj->Get('strWebsite');
			$data['feedback'][$this->obj->Get('intFeedbackId')]['strMessage']      =$this->obj->Get('strMessage');
			$data['feedback'][$this->obj->Get('intFeedbackId')]['intStatus']=$this->obj->Get('intStatus');
			$data['feedback'][$this->obj->Get('intFeedbackId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		return $this->_temp('shw_feedback.php','NULL','NULL',$error,$data);	
	}
	
	//
	// Show People
	//
	function show_people(){
	 global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_people());
		$OUT->flush();
	}
	//
	// People Listings
	//
	function page_people(){
	 global $CONF, $Q, $OUT, $DB,$LANG;
	   
	   //
	   // Get the Stuff
	   //
	   $this->ppl = new core_admin();
	   $this->ppl->getAllStuff();
	   while($this->ppl->GetNextRecord()){
	   	$data['stuff'][$this->ppl->Get('intPeopleId')]['intPeopleId'] = $this->ppl->Get('intPeopleId');
		$data['stuff'][$this->ppl->Get('intPeopleId')]['intBrandId']  = $this->ppl->Get('intBrandId');
		$data['stuff'][$this->ppl->Get('intPeopleId')]['strName']     = $this->ppl->Get('strName');
		$data['stuff'][$this->ppl->Get('intPeopleId')]['strEmail']    = $this->ppl->Get('strEmail');
		$data['stuff'][$this->ppl->Get('intPeopleId')]['intStatus']   = $this->ppl->Get('intStatus');
		
	   }
	   if($Q->req['type']==1){
	   	$id = $Q->req['id'];
		   
		   
	   $this->ppl = new core_admin();
	   $this->ppl->getAllStuffById($id);
	   while($this->ppl->GetNextRecord()){
	   	$data['stuff_edit']['intPeopleId'] = $this->ppl->Get('intPeopleId');
		$data['stuff_edit']['intBrandId']  = $this->ppl->Get('intBrandId');
		$data['stuff_edit']['strName']     = $this->ppl->Get('strName');
		$data['stuff_edit']['strEmail']    = $this->ppl->Get('strEmail');
		$data['stuff_edit']['intStatus']   = $this->ppl->Get('intStatus');
		
	   }
		   
		   
	   }
	   
	   
	   
	    //
		// List Brand
		//
		$this->obj = new core_admin();
		$this->obj->getAllBrand();
		while($this->obj->GetNextRecord()){
			$data['brand'][$this->obj->Get('intBrandId')]['intBrandId']        = $this->obj->Get('intBrandId');
			$data['brand'][$this->obj->Get('intBrandId')]['strBrandName']      = $this->obj->Get('strBrandName');
			$data['brand'][$this->obj->Get('intBrandId')]['strBrandImage']     = $this->obj->Get('strPhoto');
			$data['brand'][$this->obj->Get('intBrandId')]['strDescription']    = $this->obj->Get('strDescription');
			$data['brand'][$this->obj->Get('intBrandId')]['intStatus']         = $this->obj->Get('intStatus');
			$data['brand'][$this->obj->Get('intBrandId')]['CreateDate']        = $this->obj->Get('CreateDate');
		}
	 
	 
	 //
	 // People Listigs
	 //
	 return $this->_temp('shw_ppl.php','NULL','NULL',$error,$data);	
	 	
	}
	//
	// Do Add People
	//
	function do_add_ppl(){
	 global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intBrandId   = $Q->req['intBrandId'];
		$strStuffName = $Q->req['strStuffName'];
		$strEmail     = $Q->req['strEmail'];
		$intStatus    = $Q->req['intStatus'];
		$strAddedBy   = $Q->cookies['admin:user_id'];
		
		//
		// Do Add People
		//
		$this->objppl = new core_admin();
		$this->objppl->doAddPeople($intBrandId,$strStuffName,$strEmail,$intStatus ,$strAddedBy );
		
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_people&msg=1');
	}
	//
	// Do Edit People
	//
	function do_update_ppl(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		
		$intBrandId   = $Q->req['intBrandId'];
		$strStuffName = $Q->req['strStuffName'];
		$strEmail     = $Q->req['strEmail'];
		$intStatus    = $Q->req['intStatus'];
		$strAddedBy   = $Q->cookies['admin:user_id'];
		$id           = $Q->req['id'];
		
		//
		// Do Add People
		//
		$this->objppl = new core_admin();
		$this->objppl->doUpdatePeople($intBrandId,$strStuffName,$strEmail,$intStatus ,$strAddedBy,$id );
		
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_people&msg=2');
		
		
	}
	
	
	
	
	//
	// Show Customer Report
	//
	function show_customereport(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_customereport());
		$OUT->flush();
	}
	//
	// Page  Customer Report
	//
	function page_customereport(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetCustomerReportById($intId);
			$this->objedit->GetNextRecord();
			$data['customereportedit']['intCustomerId'] = $this->objedit->Get('intCustomerId');
			$data['customereportedit']['strName'] = $this->objedit->Get('strName');
			$data['customereportedit']['strCompanyName'] = $this->objedit->Get('strCompanyName');
			$data['customereportedit']['strEmail'] = $this->objedit->Get('strEmail');
			$data['customereportedit']['strTel'] = $this->objedit->Get('strTel');
			$data['customereportedit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
				
		//
		// List Customer Repotrt
		//
		$this->obj = new core_admin();
		$this->obj->getAllCustomerReport();
		while($this->obj->GetNextRecord()){
			$data['campaign_customer'][$this->obj->Get('intCustomerId')]['intCustomerId']  =$this->obj->Get('intCustomerId');
			$data['campaign_customer'][$this->obj->Get('intCustomerId')]['strName']      =$this->obj->Get('strName');
			$data['campaign_customer'][$this->obj->Get('intCustomerId')]['strCompanyName']      =$this->obj->Get('strCompanyName');
			$data['campaign_customer'][$this->obj->Get('intCustomerId')]['strJobTitle']=$this->obj->Get('strJobTitle');
			$data['campaign_customer'][$this->obj->Get('intCustomerId')]['strEmail']      =$this->obj->Get('strEmail');
			$data['campaign_customer'][$this->obj->Get('intCustomerId')]['strTel']      =$this->obj->Get('strTel');
			$data['campaign_customer'][$this->obj->Get('intCustomerId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		return $this->_temp('shw_customereport.php','NULL','NULL',$error,$data);	
	}
	
	
	
	//
	// Show Brand
	//
	function show_brand(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_brand());
		$OUT->flush();
	}
	//
	// Page  Brand
	//
	function page_brand(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetBrandById($intId);
			$this->objedit->GetNextRecord();
			$data['brandedit']['intBrandId'] = $this->objedit->Get('intBrandId');
			$data['brandedit']['intOrder'] = $this->objedit->Get('intOrder');
			$data['brandedit']['strBrandName'] = $this->objedit->Get('strBrandName');
			$data['brandedit']['strBrandImage'] = $this->objedit->Get('strPhoto');
			$data['brandedit']['strDescription'] = $this->objedit->Get('strDescription');
			$data['brandedit']['strLongDescription'] = $this->objedit->Get('strLongDesc');
			$data['brandedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['brandedit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
		
		
		
		
		//
		// List Brand
		//
		$this->obj = new core_admin();
		$this->obj->getAllBrand();
		while($this->obj->GetNextRecord()){
			$data['brand'][$this->obj->Get('intBrandId')]['intBrandId']  =$this->obj->Get('intBrandId');
			$data['brand'][$this->obj->Get('intBrandId')]['intOrder']    =$this->obj->Get('intOrder');
			$data['brand'][$this->obj->Get('intBrandId')]['strBrandName']    =$this->obj->Get('strBrandName');
			$data['brand'][$this->obj->Get('intBrandId')]['strBrandImage']      =$this->obj->Get('strPhoto');
			$data['brand'][$this->obj->Get('intBrandId')]['strDescription']    =$this->obj->Get('strDescription');
			$data['brand'][$this->obj->Get('intBrandId')]['intStatus']=$this->obj->Get('intStatus');
			$data['brand'][$this->obj->Get('intBrandId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		return $this->_temp('shw_brand.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Mc Category
	//
	function show_mcategory(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_mcategory());
		$OUT->flush();
	}
	//
	// Page  Mc Category
	//
	function page_mcategory(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetMcCategoryById($intId);
			$this->objedit->GetNextRecord();
			$data['categoryedit']['intCategoryId'] = $this->objedit->Get('intCategoryId');
			$data['categoryedit']['strCategoryName'] = $this->objedit->Get('strCategoryName');
			$data['categoryedit']['strCategoryPhoto'] = $this->objedit->Get('strPhoto');
			$data['categoryedit']['strDescription'] = $this->objedit->Get('strDescription');
			$data['categoryedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['categoryedit']['intOrder'] = $this->objedit->Get('intOrder');
			$data['categoryedit']['CreateDate'] = $this->objedit->Get('CreateDate');
		}
		
		
		//
		// List Mc Category
		//
		$this->obj = new core_admin();
		$this->obj->getAllManageCategory();
		while($this->obj->GetNextRecord()){
			$data['category'][$this->obj->Get('intCategoryId')]['intCategoryId']  =$this->obj->Get('intCategoryId');
			$data['category'][$this->obj->Get('intCategoryId')]['strCategoryName']    =$this->obj->Get('strCategoryName');
			$data['category'][$this->obj->Get('intCategoryId')]['strCategoryPhoto']      =$this->obj->Get('strPhoto');
			$data['category'][$this->obj->Get('intCategoryId')]['strDescription']    =$this->obj->Get('strDescription');
			$data['category'][$this->obj->Get('intCategoryId')]['intOrder']=$this->obj->Get('intOrder');
			$data['category'][$this->obj->Get('intCategoryId')]['intStatus']=$this->obj->Get('intStatus');
			$data['category'][$this->obj->Get('intCategoryId')]['CreateDate']=$this->obj->Get('CreateDate');
		}
		
		return $this->_temp('shw_mccategory.php','NULL','NULL',$error,$data);	
	}
	
	
	//
	// Show Sub Category
	//
	function show_subcategory(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_subcategory());
		$OUT->flush();
	}
	//
	// Page  Sub Category
	//
	function page_subcategory(){
			global $CONF, $Q, $OUT, $DB,$LANG;
		$error = $OUT->errors;
		
		$intTypemode = $Q->req['type'];
		$intId = $Q->req['id'];
		
		if($intTypemode==1){
			$intSubCategoryId = $Q->req['intSubCategorid'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetSubcategoryById($intSubCategoryId);
			$this->objedit->GetNextRecord();
			$data['subcategoryedit']['intSubCategoryId'] = $this->objedit->Get('intSubCategoryId');
			$data['subcategoryedit']['intCategoryId'] = $this->objedit->Get('intCategoryId');
			$data['subcategoryedit']['strSubCategoryName'] = $this->objedit->Get('strSubCategoryName');
			$data['subcategoryedit']['strPic'] = $this->objedit->Get('strPic');
			$data['subcategoryedit']['strDescription'] = $this->objedit->Get('strDescription');
			$data['subcategoryedit']['intStatus'] = $this->objedit->Get('intStatus');
			$data['subcategoryedit']['intOrder'] = $this->objedit->Get('intOrder');
			$data['subcategoryedit']['CreateDate'] = $this->objedit->Get('CreateDate');
			
		
			//echo "123";
		}
		
		
		
		//
		// List All Categories
		//
		$this->objCate = new core_admin();
		$this->objCate->getAllManageCategory();
		while($this->objCate->GetNextRecord()){
		 $data['catename'][$this->objCate->Get('intCategoryId')] = $this->objCate->Get('strCategoryName');
		}
		
		//
		// List Sub Category
		//
		$this->obj = new core_admin();
		$this->obj->getAllSubcategory($intId);
		while($this->obj->GetNextRecord()){
			$data['subcategory'][$this->obj->Get('intSubCategoryId')]['intSubCategoryId']  =$this->obj->Get('intSubCategoryId');
			$data['subcategory'][$this->obj->Get('intSubCategoryId')]['intCategoryId']  =$this->obj->Get('intCategoryId');
			$data['subcategory'][$this->obj->Get('intSubCategoryId')]['strSubCategoryName']    =$this->obj->Get('strSubCategoryName');
			$data['subcategory'][$this->obj->Get('intSubCategoryId')]['strPic']      =$this->obj->Get('strPic');
			$data['subcategory'][$this->obj->Get('intSubCategoryId')]['strDescription']    =$this->obj->Get('strDescription');
			$data['subcategory'][$this->obj->Get('intSubCategoryId')]['intStatus']=$this->obj->Get('intStatus');
			$data['subcategory'][$this->obj->Get('intSubCategoryId')]['CreateDate']=$this->obj->Get('CreateDate');
			$data['subcategory'][$this->obj->Get('intSubCategoryId')]['intOrder']=$this->obj->Get('intOrder');
		}
		
		return $this->_temp('shw_subcategory.php','NULL','NULL',$error,$data);	
	
	
	
	
	}
	
	
	
	
	
	
	
	
	
	
	
/***************************************************************/	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	//
	// Show Category
	//
	function show_category(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_category());
		$OUT->flush();
	
	}
	//
	// Page Category
	//
	function page_category(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		
		$intCategoryId = $Q->req['intCategoryId'];
		$intType = $Q->req['intType'];
		
		
		$error = $OUT->errors;
		if($intType==1){

			//
		// Get Category By Id
		//
		$this->obj = new core_admin();
		$this->obj->getCategoryById($intCategoryId);
		$this->obj->GetNextRecord();
		$data['content']['strCategoryName'] = $this->obj->Get('strCategoryName');
		$data['content']['strStatus']  = $this->obj->Get('strStatus');
		
			}
		if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objacc = new core_admin();
		$this->objacc->numperpage   = 30;
		$this->objacc->numfirstpage = 30;
		$this->objacc->getCategory($intPage);		
		while($this->objacc->GetNextRecord()){
			      $data['details'][$this->objacc->Get('intCategoryId')]['intCategoryId']        =  $this->objacc->Get('intCategoryId');
        	  $data['details'][$this->objacc->Get('intCategoryId')]['strCategoryName']      =  $this->objacc->Get('strCategoryName');
        	  $data['details'][$this->objacc->Get('intCategoryId')]['strStatus']           =  $this->objacc->Get('strStatus');
            $data['details'][$this->objacc->Get('intCategoryId')]['CreateDate']           =  $this->objacc->Get('CreateDate');
     }
     
    $this->pages = new Paginator();        
		$this->pages->items_total = $this->objacc->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');   
  	
  	return $this->_temp('shw_category.php','NULL','NULL',$error,$data);	
		 //return $this->_temp('shw_category.php', $error, $data, $param);
	}
	//
	// Category Form
	//
	function show_category_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_categoryform());
		$OUT->flush();	
		
	}
	//
	// Page Category Form
	//
	function page_categoryform(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$error = $OUT->errors;
		
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/show_categoryform.php',$data,$error);	
		//include (ROOT_PATH . '/src/function/gzip_start.php');
		//include (DIR_TEMP_PATH_BO . '/admin/show_categoryform.php');
		//include (ROOT_PATH . '/src/function/gzip_end.php');
	}
	//
	// Do Add Category
	//
	function do_addcategory(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$strCategory = trim($Q->req['strCategory']);
		$strStatus   = $Q->req['strStatus'];	
		
		$intMode = $Q->req['intMode'];
		
		if($intMode==1){
			
			
			if($strCategory==""){
			$OUT->addError("Tag Category cannot be empty !");	
		}
		//
		// Check Database
		//
		$this->obj = new core_admin();
		$data = $this->obj->doCheckCategory($strCategory);
		
	
		if($data['intCategoryId'] > 0){
			$OUT->addError('Tag Category name not avaible');	
		}
		if($OUT->isError()){
			$this->show_error('err_add_tag_category');
			exit;
		}
		
		$intType    = 2;
		$strAddedBy = $Q->cookies['admin:user_id'];
		//
		// Add Category
		//		
		$this->objcate = new core_admin();
		$resultCategory = $this->objcate->addCategory($strCategory,$strStatus,$strAddedBy,$intType);
		if($resultCategory==0){
		  $OUT->addError($LANG->q('err_cannotinsert_category'));	
			exit;	
		}
				
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_category_lists_tag&msg=1');
			
			}
		else{
			
			
		
		if($strCategory==""){
			$OUT->addError($LANG->q('err_category'));	
		}
		//
		// Check Database
		//
		$this->obj = new core_admin();
		$data = $this->obj->doCheckCategory($strCategory);
		
	
		if($data['intCategoryId'] > 0){
			$OUT->addError($LANG->q('err_category_unavailiable'));	
		}
		if($OUT->isError()){
			$this->show_error('err_add_category');
			exit;
		}
		
		
		$strAddedBy = $Q->cookies['admin:user_id'];
		//
		// Add Category
		//		
		$this->objcate = new core_admin();
		$resultCategory = $this->objcate->addCategory($strCategory,$strStatus,$strAddedBy);
		if($resultCategory==0){
		  $OUT->addError($LANG->q('err_cannotinsert_category'));	
			exit;	
		}
		
		
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_category&msg=1');
		
	 }
	}
	//
	// Show Edit Page
	//
	function show_edit_category(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_editcategoryform());
		$OUT->flush();	
	}
	//
	// Edit Page Category
	//
	function page_editcategoryform(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intCategoryId = $Q->req['intCategory'];
		
		$error = $OUT->errors;
		//
		// Get Category By Id
		//
		$this->obj = new core_admin();
		$this->obj->getCategoryById($intCategoryId);
		$this->obj->GetNextRecord();
		$data['content']['strCategoryName'] = $this->obj->Get('strCategoryName');
		$data['content']['strStatus']  = $this->obj->Get('strStatus');
		
		
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/show_editcategory.php',$data,$error);
		/*include (ROOT_PATH . '/src/function/gzip_start.php');
		include (DIR_TEMP_PATH_BO . '/admin/show_editcategory.php');
		include (ROOT_PATH . '/src/function/gzip_end.php');
		*/
		
	}
	//
	// Update Category
	//
	function do_updatecategory(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strCategory   = $Q->req['strCategory'];
		$strStatus     = $Q->req['strStatus'];
		$intCategoryId = $Q->req['intCategory'];
		$strAddedBy = $Q->cookies['admin:user_id'];
		
		$intMode       = $Q->req['intMode'];
		
		if($intMode==1){
			if($strCategory==""){
			//$OUT->addError($LANG->q('err_category'));	
			
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_category_lists_tag&intCategoryId='.$intCategoryId.'&intType=1&errMsg=1');
			exit;
		}
		//
		// Check Database
		//
		$this->obj = new core_admin();
		$data = $this->obj->doCheckCategory($strCategory);

		if( ($intCategoryId!= $data['intCategoryId'] ) && $data['intCategoryId'] > 0){
				$OUT->redirect($CONF['url_app'].'?m=admin&c=show_category_lists_tag&intCategoryId='.$intCategoryId.'&intType=1&errMsg=2');
			exit;
		}
		
		//
		// Update Category
		//
		$this->objupdate = new core_admin();
		$resultCategory = $this->objupdate->doUpdateCategory($strCategory,$strStatus,$intCategoryId,$strAddedBy);
		if($resultCategory==0){
		  $OUT->addError($LANG->q('err_cannotinsert_category'));	
			exit;	
		}
		
		
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_category_lists_tag&msg=2');
			
		}
		else{
		
		
		
		if($strCategory==""){
			//$OUT->addError($LANG->q('err_category'));	
			
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_category&intCategoryId='.$intCategoryId.'&intType=1&errMsg=1');
			exit;
		}
		//
		// Check Database
		//
		$this->obj = new core_admin();
		$data = $this->obj->doCheckCategory($strCategory);

		if( ($intCategoryId!= $data['intCategoryId'] ) && $data['intCategoryId'] > 0){
				$OUT->redirect($CONF['url_app'].'?m=admin&c=show_category&intCategoryId='.$intCategoryId.'&intType=1&errMsg=2');
			exit;
		}
		
		//
		// Update Category
		//
		$this->objupdate = new core_admin();
		$resultCategory = $this->objupdate->doUpdateCategory($strCategory,$strStatus,$intCategoryId,$strAddedBy);
		if($resultCategory==0){
		  $OUT->addError($LANG->q('err_cannotinsert_category'));	
			exit;	
		}
		
		
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_category&msg=2');
		
		
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	//
	// Banner List
	//
	function show_banner(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_list_banner());
		$OUT->flush();
	
	}
  //
	//Page list banner
	//
	function page_list_banner(){
		global $CONF,	$Q,	$OUT,	$DB, $DBSH ,$LANG;	
		
		if(!array_key_exists('banner_type',$Q->req)){
			$Q->req['banner_type'] = 'All';
		}
		{
			$fh = fopen($CONF['dir_data'].'banner.txt', 'rb') or die;
			flock($fh, LOCK_SH)          or die('Error: Fail to lock file');
			$data = array();
			while(!feof($fh)){
				if($line = fgets($fh)){
					$line = rtrim($line, "\r\n");
					$datatxt = explode("\t", $line);
					$totalweight = $totalweight + $datatxt[2];
					if($Q->req['banner_type'] != 'All'){
						if($datatxt[0] == $Q->req['banner_type']){
							array_push($data,array(
								'Set' 				=> $datatxt[0],
								'BannerName' 	=> $datatxt[1],		
								'Weight' 			=> $datatxt[2],
								'Extension' 	=> $datatxt[4],
								'Status' 	    => $datatxt[5],
								'Url' 	      => $datatxt[6],
						
							));
						}
					}
					else{
						array_push($data,array(
							'Set' 				=> $datatxt[0],
							'BannerName' 	=> $datatxt[1],		
							'Weight' 			=> $datatxt[2],
							'Extension'		=> $datatxt[4],
							'Status' 	    => $datatxt[5],
							'Url' 	      => $datatxt[6],
						));
					}
				}
			}
		}
			
			flock($fh, LOCK_UN)          or die('Error: Fail to unlock file');
			fclose($fh);
			for($a=0;$a<sizeof($data);$a++){
				$data[$a]['Percent'] = $data[$a]['Weight']/$totalweight * 100;
			}
		$this->_temp('show_banner.php',NULL,NULL,NULL,$data);
	}
	//
	// Do delete banner
	//
	function do_delete_banner(){
		global $CONF,	$Q,	$OUT,	$DB, $DBSH,$LANG;
		$banner = array();
		$content = preg_replace('/\r/i', '', $Q->req['Code']);
		$fh = fopen($CONF['dir_data'].'banner.txt', 'rb') or die;
		flock($fh, LOCK_SH)          or die('Error: Fail to lock file');
			while(!feof($fh)){
				$found = false;
				if($line = fgets($fh)){
					$line = rtrim($line, "\r\n");
					$data = explode("\t", $line);
					array_push($banner,array(
						'Set' 				=> $data[0],
						'BannerName' 	=> $data[1],		
						'Weight' 			=> $data[2],
						'Loadname' 	  => $data[3],
						'Extension' 	=> $data[4],
						'Status'      => $data[5],
					  'Url'         => $data[6],
						
					));
				}
			}
			
		for($a=0;$a<sizeof($banner);$a++){
			if($banner[$a]['BannerName'] == $banner[$Q->req['id']]['BannerName']){
				$banner[$a]['Set'] 			  = '';
				$banner[$a]['Weight'] 	  = '';
				$banner[$a]['BannerName']	= '';
				$banner[$a]['Loadname']	  = '';
				$banner[$a]['Extension']  = '';
				$banner[$a]['Status']     = '';
				$banner[$a]['Url']        = '';
				
			}
		}

	
		// Write banner.txt
		//
		{
			$fh = fopen($CONF['dir_data']. 'banner.txt', 'wb') or die;
			flock($fh, LOCK_EX)          or die('Error: Fail to lock file');
		//
		//Store as Array
		//
			foreach($banner as $ban){	
			 	
			 	if($ban['Set']!=""){
			 	$data = array(
					$ban['Set'],
					trim($ban['BannerName']),
					$ban['Weight'],
					trim($ban['Loadname']),
					$ban['Extension'],
					$ban['Status'],
					$ban['Url'],
				);
				$line = join("\t", $data);
    		fwrite($fh, $line . "\n");		
    	}
    }
    	flock($fh, LOCK_UN)          or die('Error: Fail to unlock file');
			fclose($fh);
		}	
	
		
		//exit;
		$OUT->redirect($CONF['url_app']. "?m=admin&c=show_banner");
	}
	//
	// Do Login
	//
	function do_login(){
		global $CONF, $Q, $OUT,$DB, $LANG,$MailClass;
		
		if($Q->req['mustkeyin']!=""){
			
			data_load($CONF['dir_data']."blocklist.txt", $data);
			data_load($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			$cData = count($data);
			if($cData > 0){
				$blockid = $cData + 1;
			}
			else{
				$blockid = 1;
			}
			$data[$blockid] = $_SERVER['REMOTE_ADDR']." ".date('Y-m-d H:s');
			$dataip[$blockid] = $_SERVER['REMOTE_ADDR'];
			data_save($CONF['dir_data']."blocklist.txt", $data);
			data_save($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			//
			// Inform Server PPl some hacking is being try , please block this IP ASAP
			//
			$strTitle   = 'Caution : Hacking Back End System';
			$strContent  = 'This IP ('.$_SERVER['REMOTE_ADDR'].') address  try to hack the Back End System , please take the action to block this IP ASAP.';
			
			include('email_tmpl/block.php');
			
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $CONF['administration_email'] );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			} 	
			exit;
		 }
		
		$user_id  = $Q->req['user_id'];
		$user_pwd = $Q->req['user_pwd'];
		$langfir  = $Q->req['lang'];
		if($langfir=="ch"){
				$langcode = 1;
		}
		else{
				$langcode = 2;
		}
		$auth = $this->rbac->isAuth($user_id, $user_pwd);
	
		if(empty($auth)){
		
			 $OUT->addError($LANG->q('err_login'));
			 $this->show_error('err_login');
			}
		else{
			// Get random session number
			// Store session textfile
			data_load($CONF['dir_data']."session.txt", $data);
			if(array_key_exists($user_id, $data)){
				$session = $data[$user_id];
			}else{
				$session = session_get();
			}
			$data[$user_id] = $session;
			data_save($CONF['dir_data']."session.txt", $data);
			$OUT->addCookie('admin:session', $session);
			$OUT->addCookie('admin:user_id', $user_id);
			$OUT->addCookie('admin:lang', $langfir);
			$OUT->addCookie('admin:langcode',$langcode);
			$OUT->addCookie('admin:Return', '-', time()-3600);   // Clear cookies

			$OUT->redirect($CONF['url_app']. "?m=admin&c=show_main");
		}
	}
	//
	// Do Logout
	//
	function do_logout(){
		global $CONF, $CODE, $Q, $OUT, $LANG;

		$userId = $Q->cookies['admin:user_id'];
		$data = array();
		data_load($CONF['dir_data']. 'session.txt', $data);
		unset($data[$Q->cookies['admin:user_id']]);
		data_save($CONF['dir_data']. 'session.txt', $data);
		$OUT->addCookie('admin:user_id', '', time()-3600);
		$OUT->addCookie('admin:session', '', time()-3600);
		$OUT->addCookie('admin:user_pwd', '', time()-3600);
		$OUT->addCookie('admin:user_group', '', time()-3600);
		$OUT->addCookie('admin:lang', '', time()-3600);
		$OUT->redirect($CONF['url_app']. "?m=admin&c=show_login");
	}
	//
	// Do Authorization
	//
	function do_auth(){
		global $CONF, $CODE, $Q, $OUT, $LANG;

		if(array_key_exists('admin:session', $Q->cookies)){
			$session = $Q->cookies['admin:session'];
			$user_id = $Q->cookies['admin:user_id'];

			$data = array();
			data_load($CONF['dir_data']. "session.txt", $data);
				
			if(array_key_exists($user_id, $data) && $data[$user_id] == $session && $this->rbac->hasPerm($user_id, $Q->req['c'])){
					return true;
			}
			else{
				return false;
			}
		}
	}
	//
	// Show Visitor
	//
	function show_visitor(){
		global $CONF, $CODE, $Q, $OUT, $LANG;
		
		
		$OUT->setVar('content',	$this->page_visitor());
		$OUT->flush();
	}
	//
	// Show Page Visitor
	//
	function page_visitor(){
		global $CONF, $CODE, $Q, $OUT, $LANG;
		
		//
		// text location
		//
		$fh = fopen($CONF['dir_log'].'main_log.txt', 'rb') or die;
		flock($fh, LOCK_EX) or die('Error: Fail to lock file');
		$i=0;
		$d = array();
		while(!feof($fh)){
			if($line = fgets($fh)){
				$line = rtrim($line, "\r\n");	
				$data = explode("\t", $line);

				$date   = $data[0];
				$time   = $data[1];
				$IP     = $data[2]; 
				$status = $data[3]; 
						
			/*if(!isset($d[$date])){
				$d[$date] = array(0, 0);
			}*/
			if($status == 'V'){
				//$d[$date][$IP][1] +=1;
				
				$d[$date]['Total'] +=1;
				
				$d[$date]['UniqieIP'][$IP] +=1; 
				$d[$date]['CountUniqueIP'] = count($d[$date]['UniqieIP']);
				//$d[$date][$IP] = $IP;
			}
			$i++;
			}
		}
		$data = $d;

	 return $this->_temp('show_visitor_page.php','NULL','NULL',$error,$data);
	  
	  
	}

	
	//
	// Newsletter Listing
	//
	function show_newsletter_listing(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_newsletter());
		$OUT->flush();
		
	}
	//
	// Page Newsletter
	//
	function page_newsletter(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		if($Q->req['page']){ $intPage=$Q->req['page']; }
		else{ $intPage=1; }
		
		// Pagination
		$this->objnewsletter = new core_admin();
		$this->objnewsletter->numperpage   = 50;
		$this->objnewsletter->numfirstpage = 50;
		
		$this->objnewsletter->getAllnewsletter($intPage);
		while($this->objnewsletter->GetNextRecord()){
			
			$data[$this->objnewsletter->Get('intNewsId')]['NewsId'] = $this->objnewsletter->Get('intNewsId');
			$data[$this->objnewsletter->Get('intNewsId')]['strTitle'] = $this->objnewsletter->Get('strTitle');
			//$data[$this->objnewsletter->Get('intNewsId')]['strContent'] = html_entity_decode($this->objnewsletter->Get('strContent'));
			$data[$this->objnewsletter->Get('intNewsId')]['CreateDate'] =$this->objnewsletter->Get('CreateDate');
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objnewsletter->page->total_pages;
		$this->pages->mid_range 	= 8;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');	  
			
		return $this->_temp('show_newsletter.php','NULL','NULL',$error,$data);
		
		
	}
	//
	// Newsletter Form
	//
	function show_add_newsletter(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_add_newsletter());
		$OUT->flush();
	}
	//
	// Page form newsletter
	//
	function page_add_newsletter(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		return $this->_temp('show_newsletter_form.php','NULL','NULL',$error,$data);
	}
	//
	// Do add Newsletter
	//
	function do_add_fckeditor(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strSubject = $Q->req['strSubject'];
	  $postArray = &$_POST ;
	   foreach ( $postArray as $sForm => $value ){
			if(get_magic_quotes_gpc() )
				$postedValue = htmlspecialchars( stripslashes( $value ) ) ;
			else
				$postedValue = htmlspecialchars( $value ) ;
		}
		//
		// Do save content
		//
		$this->obj = new core_admin();
		$this->obj->doAddNewsletter($strSubject,$postedValue);
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_newsletter_listing&msg=doneSaveDraft');	
		
	}
	
	//
	// Show Edit newsletter
	//
	function show_edit_newsletter(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_editnewsletter());
		$OUT->flush();
	}
	//
	// Page show Edit Newsletter
	//
	function page_editnewsletter(){
		global $CONF, $Q, $OUT, $DB,$LANG;
				
		$intNewsLetterId = $Q->req['id'];	
		
		//
		// Load the Newsletter with ID
		//
		$this->obj = new core_admin();
		$this->obj->getNewsletter($intNewsLetterId);
		$this->obj->GetNextRecord();
		$data['strTitle']   = $this->obj->Get('strTitle');
		$data['strContent'] = $this->obj->Get('strContent');
		$data['intNewsId']   = $this->obj->Get('intNewsId');
			
		
		
		return $this->_temp('show_newsletter_edit.php','NULL','NULL',$error,$data);
	}
	//
	// Do edit Newsletter
	//
	function do_update_fckeditor(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		
		$strSubject = $Q->req['strSubject'];
		$newsId = $Q->req['newsId'];
	  $postArray = &$_POST ;
	   foreach ( $postArray as $sForm => $value ){
			if(get_magic_quotes_gpc() )
				$postedValue = htmlspecialchars(stripslashes( $value ) ) ;
			else
				$postedValue = htmlspecialchars( $value ) ;
		}
		//
		// Do save content
		//
		$this->obj = new core_admin();
		$this->obj->doEditNewsletter($newsId,$strSubject,$postedValue);
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_newsletter_listing&msg=doneEditDraft');	
		
	}
	//
	// Do delte Newsletter
	//
	function do_deletenesletter(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$newId = $Q->req['intNewsId'];
		//
		// Do delete Newsletter
		//
		$this->obj = new core_admin();
		$result = $this->obj->doDeleteNewsletter($newId);
		
		if($result==1){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_newsletter_listing');	
		}
		exit;
		
	}
	//
	// Preview Newsletter
	//
	function show_preview_newsletter(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_previewnewsletter());
		$OUT->flush();
	}
	//
	// Page Preview Newsletter
	//
	function page_previewnewsletter(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intNewsLetterId = $Q->req['id'];	
		//
		// Load the Newsletter with ID
		//
		$this->obj = new core_admin();
		$this->obj->getNewsletter($intNewsLetterId);
		$this->obj->GetNextRecord();
		$data['strTitle']   = $this->obj->Get('strTitle');
		$data['strContent'] = $this->obj->Get('strContent');
		$data['intNewsId']  = $this->obj->Get('intNewsId');
	
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/show_preview_newsletter.php',$data,$error);
	}
	//
	// Show Select User and Email Newsletter
	//	
	function show_usedthisnews(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_usedthistamp());
		$OUT->flush();
	}
	//
	// Used this Newsletter
	//
	function page_usedthistamp(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intNewsLetterId = $Q->req['id'];	
		
		//
		// Load the Newsletter with ID
		//
		$this->obj = new core_admin();
		$this->obj->getNewsletter($intNewsLetterId);
		$this->obj->GetNextRecord();
		$error['strTitle']   = $this->obj->Get('strTitle');
		$error['strContent'] = $this->obj->Get('strContent');
		$error['intNewsId']  = $this->obj->Get('intNewsId');
		
		
		return $this->_temp('show_usedthisnewsletter.php','NULL','NULL',$error,$data);
		
		
	}
		//
		// Use this NewsLetter
		//
		function do_sendnewsletter(){
			global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;
			
			$intNewsLetterId = $Q->req['id'];
			$strEmail = $Q->req['strEmail'];	
		
		//
		// Load the Newsletter with ID
		//
		$this->obj = new core_admin();
		$this->obj->getNewsletter($intNewsLetterId);
		$this->obj->GetNextRecord();
		$strTitle   = $this->obj->Get('strTitle');
		$strContent = html_entity_decode($this->obj->Get('strContent'));
		$data['intNewsId']  = $this->obj->Get('intNewsId');
		
	
		
		
		$strMessage .= '</body></html>';

	
	$strMessage  = '<html><body style="	margin-left:0px; font-family: Trebuc, Arial, Verdana, sans-serif; font-size:12px; font-style:normal;">';
		$strMessage .= '
		<div style="width:660px; padding-right:50px ; background: url('.HTTP_SERVER.'/images/bg_rpt.jpg) repeat left top; "> 
		<table width="660" border="0" cellspacing="0" cellpadding="0" style="padding-left:50px;">
  	
  
 	<tr>
    <td style="width:20px"><img src="'.HTTP_SERVER.'/images/box_toplft.png" width="20" height="20" /></td>
    <td style="width:940px; background:#fafafa">&nbsp;</td>
    <td style="width:20px"><img src="'.HTTP_SERVER.'/images/box_toprlt.png" width="20" height="20" /></td>
  </tr>
  <tr>
    <td style="background:#fafafa"><img src="'.HTTP_SERVER.'/images/box_side.png" width="20" height="1" /></td>
    <td style="background:#fafafa; padding:15px">
	
	 <a href="'.HTTP_SERVER.'index.php?m=main"><img src="'.HTTP_SERVER.'/images/logo.png" border=0 /></a>
	  <br><br>
	 Dear {DynamicUserName},<br>
				
 			  '.$strContent.' 
  			
					   <br><br>
	                  Over & Out,<br>
					  The Closetmino Team.
	 
	
	</td>
   <td style="background:#fafafa"><img src=src="'.HTTP_SERVER.'/images/box_side.png" width="20" height="1" /></td>
  </tr>
  
   <tr>
    <td><img src="'.HTTP_SERVER.'/images/box_bottomlft.png" width="20" height="20" /></td> 
    <td style="background:#fafafa">&nbsp;</td>  
    <td><img src="'.HTTP_SERVER.'/images/box_bottomrlt.png" width="20" height="20" /></td>
  </tr>
</table>

</div>
<br>
<div><a href='.$CONF['url_app'] .'?m=admin&c=show_usedthisnews&id='.$intNewsLetterId.'>Back</a></div>

'

;
		$strMessage .= '</body></html>';	
 
	
	
		$MailClass->IsHTML(true);
		$MailClass->SMTPAuth = true;
		$MailClass->addAddress( $Q->req['strEmail'] );
		//$MailClass->addAddress("kanasaikuat@live.com");
		$MailClass->Subject = $strTitle ;
		$MailClass->Body = $strMessage;

  if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
			if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
			}
		} 
  	$OUT->redirect($CONF['url_app'].'?m=admin&c=show_usedthisnews&id='.$intNewsLetterId.'&msg=doneSendNews');
	}
	//
	// Do send bulk email
	//	
	function do_newslettermail(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;
		
		
		
		$intAccId = $Q->req['intAccId'];
		$intDate  = $Q->req['strStartDate'];
		$intAccID = "(".implode(',',$intAccId).")";

		
		
		$intNewsLetterId = $Q->req['Newsid'];	
		
		
		//
		// Load the Newsletter with ID
		//
		$this->obj = new core_admin();
		$this->obj->getNewsletter($intNewsLetterId);
		$this->obj->GetNextRecord();
		$strTitle   = $this->obj->Get('strTitle');
		 $strContent = html_entity_decode($this->obj->Get('strContent'));
		$intNewsId  = $this->obj->Get('intNewsId');
		
		//
		// Get User Details
		//
		$arrUser = array();
		$this->objdetails = new core_admin();
		$this->objdetails->getAccDetails($intAccID);
		while($this->objdetails->GetNextRecord()){
		 $arrUser[$this->objdetails->Get('intAccId')]['Email']    = $this->objdetails->Get('strEmail');	
		 $arrUser[$this->objdetails->Get('intAccId')]['UserName'] = $this->objdetails->Get('strUser');	
		 $arrUser[$this->objdetails->Get('intAccId')]['intAccId'] = $this->objdetails->Get('intAccId');
		}
	
		$numberuser = count($arrUser);
		$strComp = $CONF['newsletter_name'];

    $MailClass->IsHTML(true);
		$MailClass->SMTPAuth = true;
		$MailClass->Subject = $strTitle;
      
		if($numberuser > 0){
			foreach($arrUser as $key => $d){
	
		
	$strMessage  = '
 <html>
 <body style="	margin-left:0px; font-family: Trebuc, Arial, Verdana, sans-serif; font-size:12px; font-style:normal;">';
		$strMessage .= '
		<div style="width:660px; padding-right:1px ; repeat left top; "> 
		<table width="500" border="0" cellspacing="0" cellpadding="0" style="padding-left:1px;">

  <tr>
    <td style="background:#fafafa"><img src="'.HTTP_SERVER.'/images/box_side.png" width="20" height="1" /></td>
    <td style="background:#fafafa; padding:20px">
	
	 <a href="'.HTTP_SERVER.'index.php?m=main"><img width="150" src="'.HTTP_SERVER.'/images/c_logo1.png" border=0 /></a>
	  <br><br>
	 Dear '.$d['UserName'].',<br>	
 			  '.$strContent.' 
  		       Over & Out,<br>
					  The Closetmino Team.
		</td>
   <td style="background:#fafafa"><img src="'.HTTP_SERVER.'/images/box_side.png" width="20" height="1" /></td>
  </tr>
</table>
</div>';
$strMessage .= '</body></html><br>';	
		
	
	    $MailClass->addAddress($d['Email']);
	    $MailClass->Body = $strMessage;		
 
   if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
			if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
			}
		} 


				}
		
		}
	
	   $OUT->redirect($CONF['url_app'].'?m=admin&c=show_usedthisnews&id='.$intNewsLetterId.'&msg=doneSendNews');
	}
	//
	// Show Collection
	//
	function show_collection(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_collection_pge());
		$OUT->flush();
	}
	//
	// Page collection
	//
	function page_collection_pge(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetCollectionById($intId);
			$this->objedit->GetNextRecord();
			$data['collection']['strCollection'] = $this->objedit->Get('strCollectionName');
			$data['collection']['strStatus'] = $this->objedit->Get('strStatus');
			}

		if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objacc = new core_admin($intPage);
		$this->objacc->numperpage   = 30;
		$this->objacc->numfirstpage = 30;
		$this->objacc->getCollection($intPage);		
		while($this->objacc->GetNextRecord()){
			      $data['real'][$this->objacc->Get('intCollectionId')]['intCollectionId']        =  $this->objacc->Get('intCollectionId');
        	  $data['real'][$this->objacc->Get('intCollectionId')]['strCollectionName']      =  $this->objacc->Get('strCollectionName');
        	  $data['real'][$this->objacc->Get('intCollectionId')]['strStatus']           =  $this->objacc->Get('strStatus');
            $data['real'][$this->objacc->Get('intCollectionId')]['CreateDate']           =  $this->objacc->Get('CreateDate');
     }
     
    $this->pages = new Paginator();        
		$this->pages->items_total = $this->objacc->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');   
		
	return $this->_temp('show_collection_pge.php',$mode,$c,$error,$data);	
		
	} 
	
	
	
	
	//
	// Show Currency
	//
	function show_currency(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_currency_pge());
		$OUT->flush();
	}
	//
	// Page Currency
	//
	function page_currency_pge(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		$intTypemode = $Q->req['type'];
		
			//
			// Get Currency
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetCurrency();
			$this->objedit->GetNextRecord();
			$data['currency']['strMoney1'] = $this->objedit->Get('strMoney1');
			$data['currency']['strMoney2'] = $this->objedit->Get('strMoney2');
			
  
		
	return $this->_temp('show_curency_pge.php',$mode,$c,$error,$data);	
		
	} 
	//
	// Do Edit Currency
	//
	function do_edit_currency(){
	 global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$strCurrency1 = $Q->req['strCurrency1'];
		$strCurrency2 = $Q->req['strCurrency2'];
		
		if($strCurrency1==""){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_currency&errMsg=1');
			exit;
		}
		if(!is_numeric($strCurrency1)){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_currency&errMsg=2');
			exit;
			}
		//
		// Do Update Currency
		//
		$strAddedBy = $Q->cookies['admin:user_id'];
		$this->obj = new core_admin();
		$this->obj->doUpdateCurrency($strCurrency1,$strAddedBy);
		//&c=show_currency
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_currency&msg=2');
	}
	
	//
	// Show VIP Setting
	//
	function show_vip(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_vip_pge());
		$OUT->flush();
	}
	//
	// Page Currency
	//
	function page_vip_pge(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error       = $OUT->errors;
		$intTypemode = $Q->req['type'];
		
			//
			// Get Currency
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetVIP();
			$this->objedit->GetNextRecord();
			$data['vip']['strValue'] = $this->objedit->Get('strValue');
			
	   return $this->_temp('show_vip_pge.php',$mode,$c,$error,$data);	
		
	} 
	//
	// Do Edit Currency
	//
	function do_edit_vip(){
	 global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$strCurrency1 = $Q->req['strValue'];
		
		
		if($strCurrency1==""){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_vip&errMsg=1');
			exit;
		}
		if(!is_numeric($strCurrency1)){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_vip&errMsg=2');
			exit;
			}
		//
		// Do Update Currency
		//
		$strAddedBy = $Q->cookies['admin:user_id'];
		$this->obj = new core_admin();
		$this->obj->doUpdateVIP($strCurrency1,$strAddedBy);
		//&c=show_currency
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_vip&msg=2');
	}
	//
	// Show Shipping
	//
	function show_shipping(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_shipping());
		$OUT->flush();
	}
	//
	// Show Shipping Page
	//
	function page_shipping(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		
		$error = $OUT->errors;
		$intTypemode = $Q->req['type'];
		if($intTypemode==1){
			$intId = $Q->req['id'];
			//
			// Get the id
			//
			$this->objedit = new core_admin();
			$this->objedit->doGetShippingById($intId);
			$this->objedit->GetNextRecord();
			$data['shipedit']['intShippingId'] = $this->objedit->Get('intShippingId');
			$data['shipedit']['intShippingAgent'] = $this->objedit->Get('intShippingAgent');
			$data['shipedit']['intTo'] = $this->objedit->Get('intTo');
			$data['shipedit']['strAmount'] = $this->objedit->Get('strAmount');
			$data['shipedit']['strRate'] = $this->objedit->Get('strRate');
			$data['shipedit']['strAmount2'] = $this->objedit->Get('strAmount2');
			$data['shipedit']['strRate2'] = $this->objedit->Get('strRate2');
			}
		
		
		//
		// Get All Shipping Details
		//
		$this->obj = new core_admin();
		$this->obj->doGetAllShipping();
		while($this->obj->GetNextRecord()){
			$data['shipping'][$this->obj->Get('intShippingId')]['intShippingId'] = $this->obj->Get('intShippingId');
			$data['shipping'][$this->obj->Get('intShippingId')]['intShippingAgent'] = $this->obj->Get('intShippingAgent');
			$data['shipping'][$this->obj->Get('intShippingId')]['intTo'] = $this->obj->Get('intTo');
			$data['shipping'][$this->obj->Get('intShippingId')]['strAmount'] = $this->obj->Get('strAmount');
			$data['shipping'][$this->obj->Get('intShippingId')]['strRate'] = $this->obj->Get('strRate');
			$data['shipping'][$this->obj->Get('intShippingId')]['strAmount2'] = $this->obj->Get('strAmount2');
			$data['shipping'][$this->obj->Get('intShippingId')]['CreateDate'] = $this->obj->Get('CreateDate');
			$data['shipping'][$this->obj->Get('intShippingId')]['strRate2'] = $this->obj->Get('strRate2');
			
		}
		return $this->_temp('show_shipping_pge.php',$mode,$c,$error,$data);	
	} 
	//
	// Do Add Shipping
	//
	function do_add_shipping(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intShippingAgent = $Q->req['intAgentShipping'];
		$intTo = $Q->req['intTo'];
		$strAmount        = $Q->req['strAmount'];
		$strAmount2        = $Q->req['strAmount2'];
		$strRate          = $Q->req['strRate'];
		$strRate2          = $Q->req['strRate2'];
	
		$strAddedBy       = $Q->cookies['admin:user_id'];
		
		if($intAgentShipping==""){
			$intAgentShipping ="-";
			}
		if($strAmount==""){
			$strAmount =0;
		}
		if($strRate==""){
			$strRate =0;
		}
		if($strAmount==""){
			$strAmount =0;
		}
		if($strRate2==""){
			$strRate2 = 0;
			}
			
		//
		// Do Add Shipping
		//
		$this->obj = new core_admin();
		$result = $this->obj->doInsertShipping($intShippingAgent,$intTo ,$strAmount,$strRate,$strAmount2,$strRate2,$strAddedBy);
		if($result==0){
		 
		  echo "Error : Insert Shipping";	
		  exit;
		}
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_shipping&msg=1');
		
	}
	
	//
	// Do Edit Shipping
	//
	function do_edit_shipping(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intShippingAgent = $Q->req['intAgentShipping'];
		$intTo            = $Q->req['intTo'];
		$strAmount        = $Q->req['strAmount'];
		$strRate          = $Q->req['strRate'];
		$strAmount2       = $Q->req['strAmount2'];
		$strAddedBy       = $Q->cookies['admin:user_id'];
		$intShippingId    = $Q->req['intShippingId'];
		$strRate2         = $Q->req['strRate2'];
		
		if($strShippingAgent==""){
			$strShippingAgent ="-";
			}
		if($strAmount==""){
			$strAmount =0;
		}
		if($strAmount2==""){
			$strAmount2 =0;
		}
		if($strRate==""){
			$strRate =0;
		}
			if($strRate2==""){
			$strRate2 =0;
		}
		//
		// Do Edit Shipping
		//
		$this->obj = new core_admin();
		$result = $this->obj->doEditShipping($intShippingAgent,$intTo ,$strAmount,$strAmount2,$strRate,$strRate2,$strAddedBy,$intShippingId );
		if($result==0){
		 
		  echo "Error : Insert Shipping";	
		  exit;
		}
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_shipping&msg=2');	
		
		
	}
	
	//
	// Show All Categories Tagging
	//
	function show_category_lists_tag(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_category_tagging());
		$OUT->flush();
	}
	//
	// Page All Categories
	//
	function page_category_tagging(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intType = $Q->req['intType'];
		$intCategoryId = $Q->req['intCategoryId'];
		
		$error = $OUT->errors;
		if($intType==1){

			//
		// Get Category By Id
		//
		$this->obj = new core_admin();
		$this->obj->getCategoryById($intCategoryId);
		$this->obj->GetNextRecord();
		$data['content']['strCategoryName'] = $this->obj->Get('strCategoryName');
		$data['content']['strStatus']  = $this->obj->Get('strStatus');
		
			}
		
			
		$this->objcategory = new core_admin();
		$this->objcategory->getAllCategoriesTagging();	
		while($this->objcategory->GetNextRecord()){
			$data['real'][$this->objcategory->Get('intCategoryId')]['intCategoryId'] = $this->objcategory->Get('intCategoryId');
			$data['real'][$this->objcategory->Get('intCategoryId')]['strCategoryName'] = $this->objcategory->Get('strCategoryName');
			$data['real'][$this->objcategory->Get('intCategoryId')]['strStatus'] = $this->objcategory->Get('strStatus');
			$data['real'][$this->objcategory->Get('intCategoryId')]['CreateDate'] = $this->objcategory->Get('CreateDate');
			
			
		}
		
		return $this->_temp('show_category_tagging.php',$mode,$c,$error,$data);	
	} 
	
	
	
	//
	// Do edit
	// 
	function do_edit_customer(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strStatus = $Q->req['strStatus'];
		$intCustomerType = $Q->req['intCustomerType'];
		$strAddedBy = $Q->cookies['admin:user_id'];
		$intAccId   = $Q->req['id'];
		
		//
		// Do Change Customer Record
		//
		$this->obj = new core_admin();
		$result = $this->obj->doChangesStatusCustomer($strStatus,$intCustomerType,$strAddedBy,$intAccId);
		if($result==0){
		 echo "Error : Update customer table";
		 exit;	
		}
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_customers&msg=2');
		
		
		
		
	}
	//
	// Show Attribute
	//
	function show_attribute(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_attribute_pge());
		$OUT->flush();
	}
	//
	// Page Attribute
	//
	function page_attribute_pge(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		//
		// Get All Attribute
		//
		$this->objattribute = new core_admin();
		$this->objattribute->doGetAttribute();
		while($this->objattribute->GetNextRecord()){
			$data[$this->objattribute->Get('intAttId')]['intAttributeId'] = $this->objattribute->Get('intAttId');
			$data[$this->objattribute->Get('intAttId')]['strValue'] = $this->objattribute->Get('strValue');
				$data[$this->objattribute->Get('intAttId')]['CreateDate'] = $this->objattribute->Get('CreateDate');
		}
		
		return $this->_temp('show_attribute_page.php','NULL','NULL',$error,$data);
		
	}
	//
	// Show Attribute Page
	//
	function show_value_page(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_attributepge());
		$OUT->flush();
		
	}
	//
	// Page attribute
	//
	function page_attributepge() {
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		$intAttributeId = $Q->req['intAttributeId'];
		
		if($Q->req['intType']==1){
			//
			// Get the Values
			//	
			$intValueId = $Q->req['valueid'];
			$this->objedit = new core_admin();
			$this->objedit->doGetValueVariable($intAttributeId,$intValueId);
			$this->objedit->GetNextRecord();
			$data['content']['intValueId'] = $this->objedit->Get('intValueId');
			$data['content']['variable'] = $this->objedit->Get('variable');
			$data['content']['strStatus'] = $this->objedit->Get('strStatus');
		}
		//
		// What value
		//
		$this->objValue = new core_admin();
		$this->objValue->getValueHead($intAttributeId);
		$this->objValue->GetNextRecord();
		$c['HeadName'] = $this->objValue->Get('strValue');
		if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->obj = new core_admin();
		$this->obj->numperpage   = 30;
		$this->obj->numfirstpage = 30;
		$this->obj->getAllValuesByattId($intPage,$intAttributeId);
		while($this->obj->GetNextRecord()){
			$data['value'][$this->obj->Get('intValueId')]['intValueId'] = $this->obj->Get('intValueId');
			$data['value'][$this->obj->Get('intValueId')]['variable']   = $this->obj->Get('variable');
			$data['value'][$this->obj->Get('intValueId')]['strStatus']   = $this->obj->Get('strStatus');
			$data['value'][$this->obj->Get('intValueId')]['CreateDate']   = $this->obj->Get('CreateDate');
			
		}
		
     
    $this->pages = new Paginator();        
		$this->pages->items_total = $this->obj->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');   
		
	return $this->_temp('show_value_pge.php',$mode,$c,$error,$data);	

	}
	//
	// Do Add Values
	//
	function do_addValue(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strValue         = $Q->req['strValue'];
		$strStatus        = $Q->req['strStatus'];
		$strAddedBy       = $Q->cookies['admin:user_id'];
		$intAtt           = $Q->req['intAttributeId'];
		
		//
		// Check Database
		//
		$this->obj = new core_admin();
		$data = $this->obj->doCheckValue($strValue);
		
	
		if($data['intValueId'] > 0){
			$OUT->addError('Duplicate the Value.');	
		}
		if($OUT->isError()){
			$this->show_error('err_add_attribute');
			exit;
		}
		
		//
		// Insert values
		//
		$this->objinsert = new core_admin();
		$result = $this->objinsert->doInsertValueAtt($strValue,$intAtt,$strStatus,$strAddedBy);
		if($result==0){
			echo "Insert Value : Error...";
		 	exit;
		}
		//echo "ssss";
		//exit;
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_value_page&intAttributeId='.$intAtt.'&msg=1');
	}
	//
	//Do update values
	//
	function do_updateValue(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$valueid        = $Q->req['valueid'];
		$intAttributeId = $Q->req['intAttributeId'];
		$strValue       = $Q->req['variable'];
		$strStatus      = $Q->req['strStatus'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		
		//
		// Check Database
		//
		$this->obj = new core_admin();
	  $this->obj->doCheckValue($strValue);
		$this->obj->GetNextRecord();
		$strValueNameDB = $this->obj->Get('variable');
	  $intDB          = $this->obj->Get('intValueId');
	 	if(trim(strtolower($strValue))==""){
  		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_value_page&errMsg=1&valueid='.$valueid.'&intAttributeId='.$intAttributeId.'&intType=1');
  		exit;
  		}
	 	if(trim(strtolower($strValue))==trim(strtolower($strValueNameDB)) &&  $intDB!=$valueid){
  		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_value_page&errMsg=2&valueid='.$valueid.'&intAttributeId='.$intAttributeId.'&intType=1');
  		exit;
  		}
		 
	//
	// Do Update
	//
	$this->objup = new core_admin();
	$this->objup->doUpdateValue($valueid,$strValue ,	$strStatus , $strAddedBy);
		
	$OUT->redirect($CONF['url_app'].'?m=admin&c=show_value_page&msg=2&valueid='.$valueid.'&intAttributeId='.$intAttributeId);
  	
		
	}
	//
	// Show show_products_listings
	//
	function show_products_listings(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_product_lists());
		$OUT->flush();
	}
	//
	// Show page products
	//
	function page_product_lists(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->obj = new core_admin();
		$this->obj->numperpage   = 10000000000000;
		$this->obj->numfirstpage = 10000000000000;
		$this->obj->getAllProduct($intPage);
		while($this->obj->GetNextRecord()){
			$data['item'][$this->obj->Get('intItemId')]['intItemId']         = $this->obj->Get('intItemId');
			$data['item'][$this->obj->Get('intItemId')]['intTypeId']         = $this->obj->Get('intTypeId');
			$data['item'][$this->obj->Get('intItemId')]['intCollectionId']   = $this->obj->Get('intCollectionId');
			$data['item'][$this->obj->Get('intItemId')]['strItemName']       = $this->obj->Get('strItemName');
			$data['item'][$this->obj->Get('intItemId')]['strItemCode']       = $this->obj->Get('strItemCode');
			$data['item'][$this->obj->Get('intItemId')]['strQuantity']       = $this->obj->Get('strQuantity');
			$data['item'][$this->obj->Get('intItemId')]['intStatus']         = $this->obj->Get('intStatus');
			$data['item'][$this->obj->Get('intItemId')]['strImage']            = $this->obj->Get('strImage');
			$data['item'][$this->obj->Get('intItemId')]['CreateDate']        = $this->obj->Get('CreateDate');
		}
    $this->pages = new Paginator();        
		$this->pages->items_total = $this->obj->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
	 return $this->_temp('show_products_lists.php',$mode,$c,$error,$data);	
		
	}
	
	//
	// Show Product Form
	//
	function show_add_productform(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_productform());
		$OUT->flush();	
	}
	//
	// Page Product Form
	//
	function page_productform(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		//
		// Get Category 
		//
		$arrCate = array();
		$this->objCate = new core_admin();
		$this->objCate->getAllCategory();
		while($this->objCate->GetNextRecord()){
			$data['caterory'][$this->objCate->Get('intCategoryId')] = $this->objCate->Get('strCategoryName');
			$data['arrCate'][$this->objCate->Get('intCategoryId')] = $this->objCate->Get('strCategoryName');
		}
		//
		// Get Subcategory
		//
		$arrSubCate = array();
		$this->objSubCate = new core_admin();
		$this->objSubCate->getAllSubCategory();
		while($this->objSubCate->GetNextRecord()){
			$data['subcate'][$this->objSubCate->Get('intCategoryId')][$this->objSubCate->Get('intSubCategoryId')] = $this->objSubCate->Get('strSubCategoryName');
			$data['arrSubCate'][$this->objSubCate->Get('intCategoryId')][$this->objSubCate->Get('intSubCategoryId')] = $this->objSubCate->Get('strSubCategoryName');
			}
		//
		// Get Collection
		//
		$this->objCollection = new core_admin();
		$this->objCollection->getCollection();
		while($this->objCollection->GetNextRecord()){
			
			$data['collection'][$this->objCollection->Get('intCollectionId')]['intCollectionId'] = $this->objCollection->Get('intCollectionId');	
			$data['collection'][$this->objCollection->Get('intCollectionId')]['strCollectionName'] = $this->objCollection->Get('strCollectionName');	
			
		}
			
		 return $this->_temp('show_product_form.php',$mode,$c,$error,$data);	
	}  
	//
	// Do do product
	//
	function do_add_product1(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intTypeid       = $Q->req['intTypeid'];
		$intCollectionid = $Q->req['intCollectionid'];
		$intCate         = $Q->req['intCate'];
		$intSubCate      = $Q->req['intSubCate'];
		$strProductName  = $Q->req['strProductName'];
		$strProductCode  = $Q->req['strProductCode'];
		$strSKU          = $Q->req['strSKU'];
		$strWeight       = $Q->req['strWeight'];
		$strNPrice       = $Q->req['strNPrice'];
		$strSPrice       = $Q->req['strSPrice'];
		$strVIPPrice     = $Q->req['strVIPPrice'];
		$intQuantity     = $Q->req['intQuantity'];
		$strPic          = $_FILES['strPic']['tmp_name'];
		$intStatus       = $Q->req['intStatus'];
		$intDayEnd       = $Q->req['intDayEnd'];
		$strStartDateF   = $Q->req['strStartDate'];
		$strStartDateE   = $Q->req['strEndDate'];
		$intPointRedemption = $Q->req['intRedemption'];
		
		
		if($intPointRedemption=="Y"){
			 $intPoint           = $Q->req['intPoint'];
			}
		else{
			$intPoint = 0;
			
			}
		
		
		//
		// explode Date
		//
		$strexplodeDate  = explode("/",$strStartDateF);
		
		$strexplodeDateE  = explode("/",$strStartDateE);
		//$strStartDateE  = $strexplodeDateE[1]."-".$strexplodeDateE[0]."-".$strexplodeDateE[2];
		$todayExpiryMk = mktime(0,0,0,$strexplodeDateE[0],$strexplodeDateE[1] ,$strexplodeDateE[2]);

		$postArray = &$_POST ;
	   foreach ( $postArray as $sForm => $value ){
			if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $value ) ) ;
			else
				$strDescription = htmlspecialchars( $value ) ;
		}
		$strAddedBy  = $Q->cookies['admin:user_id'];
		
			
			if(strpos($_SERVER['HTTP_USER_AGENT'],"Safari")!=FALSE){
			$find	= array('%0D', '%0A');
			$replace= array('', '');
			$filterstrDescription = str_replace($find, $replace, rawurlencode($strDescription)); 
			$filterstrDescription = rawurldecode($filterstrDescription); 
			//
			// Video
			//
				$find3	= array('%0D', '%0A');                                                               
				$replace3= array('', '');                                                                    
				$strVideoUrl = str_replace($find3, $replace3, rawurlencode($strUrlVideo));  
				$strVideoUrl = rawurldecode($strVideoUrl);                               
			
		}
		else{
			$filterstrDescription  = str_replace("\n","", $strDescription); 
			$strVideoUrl = str_replace("\n","", $strUrlVideo); 
			
		}
		//
		// Validation
		//
		if($intSubCate==""){
			$intSubCate = 0;
			}
		if($strSKU==""){
			$strSKU = "-";
			}
		if($strWeight==""){
			$strWeight = "-";
			}
		
		$this->obj = new core_admin();
		$id = $this->obj->doAddProduct(array(
		   'intTypeid'      => $intTypeid,           
		   'intCollectionid'=> $intCollectionid,     
		   'intCate'        => $intCate,             
		   'intSubCate'     => $intSubCate,          
		   'strProductName' => $strProductName,      
		   'strProductCode' => $strProductCode,      
		   'strSKU'         => $strSKU,              
		   'strWeight'      => $strWeight,           
		   'strNPrice'      => $strNPrice ,          
		   'strSPrice'      => $strSPrice,           
		   'strVIPPrice'    => $strVIPPrice , 
		   'intQuantity'    => $intQuantity,       
		   'strPic'         => $strPic,              
		   'intStatus'      => $intStatus,  
		   'strDesc'        => $strDescription, 
		   'strStartDate'   => $strStartDateF, 
		   'strStartDateE'  => $strStartDateE,
		   'intMkDayEnd'    => $todayExpiryMk,
		   'intPointRedemption' => $intPointRedemption, 
		   'intPoint'       => $intPoint,
		   'strAddedBy'     => $strAddedBy
	    )); 
		   
		 
		 //
		 // Add Quantity Store
		 //  
		if($id > 0){
		 $this->obj = new core_admin();
		 $i =  $this->obj->doAddProductStore(array(
		  		'intItemId'      => $id,
		  		'strItemPrice'   => $strSPrice, 
		  		'intQuantity'    => $intQuantity,
		  		'strAddedBy'     => $strAddedBy,
		  		'strStatus'      => $strStatus,     
		  ));
		}
		  $folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
		  $rand = rand(1000000, 9999999);  
           
            if($strPic!=""){
             if(!file_exists( $CONF['dir_photo']."product_pic/".$folder_id."/")){
                        mkdir($CONF['dir_photo'].'/product_pic/'.$folder_id.'/',0777);
                }
                if(!file_exists( $CONF['dir_photo']."product_pic/".$folder_id."/".$id."/")){
                        mkdir($CONF['dir_photo'].'/product_pic/'.$folder_id.'/'.$id.'/',0777);
                } 
                $upload_dirBig     = $CONF['dir_photo'].'product_pic/'.$folder_id.'/'.$id.'/'; 
                
                $arrfilename = $rand.'_'.basename($_FILES['strPic']['name']);
          
            move_uploaded_file($_FILES['strPic']['tmp_name'], $upload_dirBig.'/'.$arrfilename);
           $params = array(array('size' => 320,
                          'file' => $upload_dirBig.'/s320-'.$arrfilename),
                          array('size' => 200,
                          'file' => $upload_dirBig.'/s200-'.$arrfilename),
                    array('size' => 156,
                          'file' => $upload_dirBig.'/s156-'.$arrfilename),
                    array('size' => 46,
                          'file' => $upload_dirBig.'/s46-'.$arrfilename),
                    array('size' => 40,
                          'file' => $upload_dirBig.'/s40-'.$arrfilename));
    
  

    if (thumbnail_generator($upload_dirBig.'/'.$arrfilename, $params) == false)
        die("Error processing uploaded file {$arrfilename}");
        
      $this->objupdate = new core_admin();
      $this->objupdate->doInsertImageAdPic($id,$arrfilename);
        } 
		
		//
		// Single Redirect
		//
		if($intTypeid==1){
			
		  $OUT->redirect($CONF['url_app']."?m=admin&c=show_add_productform&msgSingle=1");
		  exit;	
		}
		
		//
		// Semi Redirect
		//
			if($intTypeid==2){
			
		  $OUT->redirect($CONF['url_app']."?m=admin&c=show_addSemiform&id=".$id."&intStotrId=".$i);
		  exit;	
		}
		
		//
		// Multiple Redirect Hold
		//
	}
	//
	// Show Semi 
	//
	function show_addSemiform(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_semi_form());
		$OUT->flush();
	}
	//
	// Page Semi
	//
	function page_semi_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$data['id'] = $Q->req['id'];
		$intID      = $Q->req['id'];
		//
		// Select the arritube
		//
		$this->obj = new core_admin();
		$this->obj->doGetAllArr();
		while($this->obj->GetNextRecord()){
			$data['single_arr'][$this->obj->Get('intAttId')]['strValue'] = $this->obj->Get('strValue');
		}
		//
		// Show all avaible value
		//	
		$this->objall = new core_admin();
		$this->objall->doShowallvalue();
		while($this->objall->GetNextRecord()){
			$data['all_value'][$this->objall->Get('intAttId')][$this->objall->Get('intValueId')] = $this->objall->Get('variable');
		}
		
		//
		// Get Store Id
		//
		$this->objstore = new core_admin();
		$this->objstore->doGetStoreId($intID);
		$this->objstore->GetNextRecord();
		$data['storeId'] = $this->objstore->Get('intItemStoreId');
		
		return $this->_temp('show_semiform.php',$mode,$c,$error,$data);	
		
	}
	//
	// Show pop-up qty form
	//
	function show_addSemiform_pop(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_semiqtyform_popup());
		$OUT->flush();
	}
	//
	// Page Qty Semi form popup
	//
	function page_semiqtyform_popup(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$id = $Q->req['id'];
		$intStoreId = $Q->req['intStoreId'];
		$intAttId  = $Q->req['intAttId'];
		
	
		
		//
		// Select the arritube
		//
		$this->obj = new core_admin();
		$this->obj->doGetAllArr();
		while($this->obj->GetNextRecord()){
			$data['single_arr'][$this->obj->Get('intAttId')]['strValue'] = $this->obj->Get('strValue');
		}
		//
		// Show all avaible value
		//	
		$this->objall = new core_admin();
		$this->objall->doShowallvalue();
		while($this->objall->GetNextRecord()){
			$data['all_value'][$this->objall->Get('intAttId')][$this->objall->Get('intValueId')] = $this->objall->Get('variable');
		}
		
		//
		// Get Store Id
		//
		$this->objstore = new core_admin();
		$this->objstore->doGetStoreId($intID);
		$this->objstore->GetNextRecord();
		$data['storeId'] = $this->objstore->Get('intItemStoreId');
		
		$data['intStoreId'] = $intStoreId;
		//
		//  closetmino_item_qc
		//
		$this->objqcs = new core_admin();
		$this->objqcs->doGetQCSQuantity($id , $intStoreId );
		while($this->objqcs->GetNextRecord()){
			$data['all_qcs_check'][$this->objqcs->Get('intAttId')][$this->objqcs->Get('intAtt1')] = $this->objqcs->Get('intQuantity');	
			
		}
		$data['attId'] = $intAttId;
		$data['id']    = $Q->req['id'];
		//echo "<pre>";
		//print_R($data['all_qcs_check']);
		
		//exit;
		
		if($intAttId==""){
			return $this->_output(DIR_TEMP_PATH_BO . '/admin/show_qtycsqform.php',$data,$error);
			}
		else{
			
			
		$intOption    = $intAttId;
		$data['id']   = $id;
		$data['intStoreId'] = $intStoreId;
	
		//
		// Show all avaible value
		//	
		$this->objall = new core_admin();
		$this->objall->doShowallvalueByid($intOption);
		while($this->objall->GetNextRecord()){
			$data['all_value_pop'][$this->objall->Get('intAttId')][$this->objall->Get('intValueId')] = $this->objall->Get('variable');
		}

		return $this->_output(DIR_TEMP_PATH_BO . '/admin/show_qtyform_pop.php',$data,$error);
			
			
			}
		
		
	}
	//
	// Show Quantity Form
	//
	function show_addQ_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_pageQform());
		$OUT->flush();
		
	}
	//
	// Page show qty
	//
	function page_pageQform(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intItemId   = $Q->req['intItemId'];		
		$strSPrice = $Q->req['strSPrice'];
		$error = $OUT->errors;
		
		include (ROOT_PATH . '/src/function/gzip_start.php');
		include(DIR_TEMP_PATH_BO . '/admin/show_add_Q.php');
		//include (ROOT_PATH. 'tpl/fo/tmp.php');
		include (ROOT_PATH . '/src/function/gzip_end.php');
		exit();	
		
		
	}
	
		//
	// Do Add Quantity
	//
	function do_add_Q(){
		global $CONF, $Q, $OUT,$LANG,$CODE;	
		
		$intItemId      = $Q->req['intItemId'];
		$strAddedBy     = $Q->cookies['admin:user_id'];
		$strQuantity    = $Q->req['strQuantity'];
		$strSPrice        = $Q->req['strSPrice'];
		if($strQuantity==""){
			$strQuantity  = 0;
			}
		
		
		//
		// Do Add Quantity
		//	
		$this->obj = new core_admin();
		$result = $this->obj->doAddQuantity($intItemId,$strAddedBy,$strQuantity,$strSPrice);
	
		if($result > 0){
			
			//
			// Get total quatity
			//
			$this->objmore = new core_admin();
			$this->objmore->getitemdetail($intItemId); 
			while($this->objmore->GetNextRecord()){
				$intTotal += $this->objmore->Get('intQuantity');
			}
			//
			// Get all used quantity
			//
			$this->objmore = new core_admin();
			$this->objmore->getitemdetailtran($intItemId); 
			while($this->objmore->GetNextRecord()){
				$intTotalUsed += $this->objmore->Get('strQuantityBuy');
			}
			$finalQuantity = $intTotal - $intTotalUsed;
			
			//
			// Update main quantity
			//
			$this->objmainq = new core_admin();
			$r = $this->objmainq->doUpdateItemQ($finalQuantity,$intItemId);
			
			if($r > 0){
			 $OUT->redirect($CONF['url_app'].'?m=admin&c=show_addQ_form&intItemId='.$intItemId.'&strSPrice='.$strSPrice.'&msg=1');
		exit();
				}
			
		}	
	}
	
	
	// 
	// Show Semi Quantity
	//
	function show_semiqtyform(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_semiqtyform());
		$OUT->flush();
	}
	//
	// Page qty form
	//
	function page_semiqtyform(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intOption    = $Q->req['intOption'];
		$data['id']   = $Q->req['itemid'];
		$data['intStoreId'] = $Q->req['intStoreId'];
	
		//
		// Show all avaible value
		//	
		$this->objall = new core_admin();
		$this->objall->doShowallvalueByid($intOption);
		while($this->objall->GetNextRecord()){
			$data['all_value'][$this->objall->Get('intAttId')][$this->objall->Get('intValueId')] = $this->objall->Get('variable');
		}
		if($Q->req['popup']==1){
			return $this->_output(DIR_TEMP_PATH_BO . '/admin/show_qtyform.php',$data,$error);
			}
		else{
		return $this->_temp('show_qtyform.php',$mode,$c,$error,$data);	
		}
	}
	
	//
	// Show Popup Quantity
	//
	function show_semiqtyform_pop(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_semiqtyform_pop());
		$OUT->flush();
	}
	//
	// Page semiformpop
	//
	function page_semiqtyform_pop(){
			global $CONF, $Q, $OUT, $DB,$LANG;
			
		$intStoreId = $Q->req['intStoreId'];
		$intItemId = $Q->req['id'];
		$intOption = $Q->req['intAttId'];
		$data['id'] = $intItemId;
		$data['intStoreId'] = $intStoreId;
		$data['OptionId'] = $intOption;
		//
		// Show all avaible value
		//	
		$this->objall = new core_admin();
		$this->objall->doShowallvalueByid($intOption);
		while($this->objall->GetNextRecord()){
			$data['all_value_pop'][$this->objall->Get('intAttId')][$this->objall->Get('intValueId')] = $this->objall->Get('variable');
		}
		//
		// Query from  closetmino_item_qc
		//
		$this->obj = new core_admin();
		$this->obj->getAllDetailsSizeColor($intItemId,	$intStoreId );
		while($this->obj->GetNextRecord()){
			$data['all_value_set'][$this->obj->Get('intAttId')][$this->obj->Get('intAtt1')] = $this->obj->Get('intQuantity');
			
		}
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/show_qtyform_value.php',$data,$error);
		
	} 
	
	//
	// Do Edit Quantity & size
	//
	function do_edit_size_quantity(){
		global $CONF, $Q, $OUT, $DB,$LANG;
				
		$arrColor   = $Q->req['arrColor'];
		$intItemId  = $Q->req['intItemId'];
		$intStoreId = $Q->req['intStoreId'];
		$intOption  = $Q->req['intOption'];
		
	 //
		// Get the Attribute By intAttribute Id
		//
		$this->objall = new core_admin();
		$this->objall->doShowallvalueByid($intOption);
		while($this->objall->GetNextRecord()){
			$data['all_value'][$this->objall->Get('intValueId')] = $this->objall->Get('variable');
		}
		
	$arrFinalAdd = array();
	$fzero = 0;
		foreach($data['all_value'] as $k => $Size){
			$arrSize[$k]['size']     = $Q->req['size_'.$k];
			$arrSize[$k]['quantity'] =$Q->req['qs_'.$k];
			$i++;
		}
		$strAddedBy  = $Q->cookies['admin:user_id'];

		$c = array();
		$s = array();
		$q = array();
		$i=0;
		$j=0;
		//
		// Mashup array
		//
		$arrFinal = array();
		foreach($arrSize as $n => $t){
			array_push($c,$n);
			foreach($t as $m => $vv){
				if(count($vv) > 0){
				foreach($vv as $r => $rr){
					if($m=="size"){
						if($rr!=""){
							$s[$n][$i] = $rr;
							$i++;
						}
				}
			if($m=="quantity"){
				if($rr!=""){
					$q[$n][$j] = $rr;
					$j++;
							}
						}
					}
				}
			}
		}
	
		foreach($s as $h => $size){
			foreach($size as $k => $v){
					$arrFinal[$intStoreId][$h][$v] =$q[$h][$k];
					$arrFinalAdd[$h][$v] =$q[$h][$k];
			  }
		}
	

		

		
	
		
		$cDataFinal = count($arrFinal);
		
		

		//
		// Do delete all blufroge_item_qc
		//
		$this->objdelete = new core_admin();
		$this->objdelete->doDeleteCSQ($intItemId,$intStoreId);
		
		
		//
		// Do insert color blufroge_item_qc
		//
		$this->objinsert = new core_admin();
		$result = $this->objinsert->doAddQCSQtyEdit($arrFinal,$intOption,$intItemId,$intStoreId,$strAddedBy );
		//
		// Query the total qc size
		//
		$arrExistingData = array();
		$arrExistD = array();
		$this->objqcs = new core_admin();
		$this->objqcs->doGetExisting($intItemId);
		while($this->objqcs->GetNextRecord()){
			$arrExistingData[$this->objqcs->Get('intItemStoreId')][$this->objqcs->Get('intAtt1')][$this->objqcs->Get('intAtt2')] = $this->objqcs->Get('intQuantity');
			$arrUpdateMashup[$this->objqcs->Get('intAtt1')][$this->objqcs->Get('intAtt2')] += $this->objqcs->Get('intQuantity');
		}
	
	
			//
			// Delete first and update later
			//
			$this->objUpdateDQCS = new core_admin();
			$this->objUpdateDQCS->doDeleteQCSAdd($intItemId,$arrUpdateMashup);
			
			
			$this->objUpdateQCS = new core_admin();
			$this->objUpdateQCS->doAddTotalQCS($intItemId,$intOption ,$arrUpdateMashup,$strAddedBy);
		
	
		//
		// Get the total ac
		//
		$this->objGetTotal = new core_admin();
		$this->objGetTotal->doGetTotal($intStoreId);
		while($this->objGetTotal->GetNextRecord()){
			$intTotal +=$this->objGetTotal->Get('intQuantity');
		}
		
		//
		// Update the  closetmino_item_store
		//
		$this->objupdateQc = new core_admin();
		$this->objupdateQc->doUpdateitemStore($intTotal,$intStoreId,$strAddedBy);
		
		
		//
		// Update the   closetmino_item_store
		//
		$this->objupdateQc = new core_admin();
		$this->objupdateQc->doGetitemStoreItem($intItemId);
		while($this->objupdateQc->GetNextRecord()){
			$intAllOver += $this->objupdateQc->Get('intQuantity');
		}
		//
		// Update closetmino_item
		//
		$this->objupdateItemMain = new core_admin();
		$this->objupdateItemMain->doUpdateComitem($intAllOver,$intItemId,$strAddedBy);
		
	
				
		$OUT->redirect($CONF['url_app']. "?m=admin&c=show_semiqtyform_pop&id=".$intItemId."&intStoreId=".$intStoreId."&intAttId=".$intOption."&msg=done");
	
		
		
	}
	
	
	
	
	
	
	//
	// Show Edit Store
	//
	function show_edit_product(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$OUT->setVar('content',	$this->page_edit_pro());
		$OUT->flush();	
	}
	//
	// Page Edit Product
	//
	function page_edit_pro(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$id = $Q->req['id'];
		
		//
		// Get All Product Details
		//
		$this->objPro = new core_admin();
		$this->objPro->doGetProductById($id);
		$this->objPro->GetNextRecord();
		  
		$data['item']['intItemId']         = $this->objPro->Get('intItemId'); 
		$data['item']['intTypeId']         = $this->objPro->Get('intTypeId');
		$data['item']['intCollectionId']   = $this->objPro->Get('intCollectionId');
		$data['item']['intCategoryId']     = $this->objPro->Get('intCategoryId');
		$data['item']['intSubCateId']      = $this->objPro->Get('intSubCateId');
		$data['item']['strItemName']       = $this->objPro->Get('strItemName');
		$data['item']['strItemCode']       = $this->objPro->Get('strItemCode');
		$data['item']['strSKU']            = $this->objPro->Get('strSKU');
		$data['item']['strWeight']         = $this->objPro->Get('strWeight');
		$data['item']['strNormalItemPrice'] = $this->objPro->Get('strNormalItemPrice');
		$data['item']['strSPrice']          = $this->objPro->Get('strSPrice');        
		$data['item']['strVPrice']          = $this->objPro->Get('strVPrice');     
		$data['item']['strQuantity']        = $this->objPro->Get('strQuantity');
		$data['item']['strImage']           = $this->objPro->Get('strImage');
		$data['item']['strDescription']     = $this->objPro->Get('strDescription');
		$data['item']['intStatus']          = $this->objPro->Get('intStatus');
		$data['item']['strStartDate']       = $this->objPro->Get('strStartDate');
		$data['item']['strEndDate']         = $this->objPro->Get('strEndDate');
		$data['item']['strRedemption']         = $this->objPro->Get('strRedemption');
		$data['item']['strRedemptionPoint']         = $this->objPro->Get('strRedemptionPoint');
		
		//
		// Get Category 
		//
		$arrCate = array();
		$this->objCate = new core_admin();
		$this->objCate->getAllCategory();
		while($this->objCate->GetNextRecord()){
			$data['caterory'][$this->objCate->Get('intCategoryId')] = $this->objCate->Get('strCategoryName');
			$data['arrCate'][$this->objCate->Get('intCategoryId')]  = $this->objCate->Get('strCategoryName');
		}
		//
		// Get Subcategory
		//
		$arrSubCate = array();
		$this->objSubCate = new core_admin();
		$this->objSubCate->getAllSubCategory();
		while($this->objSubCate->GetNextRecord()){
			$data['subcate'][$this->objSubCate->Get('intCategoryId')][$this->objSubCate->Get('intSubCategoryId')] = $this->objSubCate->Get('strSubCategoryName');
			$data['arrSubCate'][$this->objSubCate->Get('intCategoryId')][$this->objSubCate->Get('intSubCategoryId')] = $this->objSubCate->Get('strSubCategoryName');
			}
		//
		// Get Collection
		//
		$this->objCollection = new core_admin();
		$this->objCollection->getCollection();
		while($this->objCollection->GetNextRecord()){
			
			$data['collection'][$this->objCollection->Get('intCollectionId')]['intCollectionId'] = $this->objCollection->Get('intCollectionId');	
			$data['collection'][$this->objCollection->Get('intCollectionId')]['strCollectionName'] = $this->objCollection->Get('strCollectionName');	
			
		}
			
		return $this->_temp('show_edit_product_form.php',$mode,$c,$error,$data);
	}
	
	//
	// Show Copy Content
	//
	function show_copyedit_product(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$OUT->setVar('content',	$this->page_editadd_pro());
		$OUT->flush();	
	}
	//
	//Page Copy Content
	//
	function page_editadd_pro(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$id = $Q->req['id'];
		
		//
		// Get All Product Details
		//
		$this->objPro = new core_admin();
		$this->objPro->doGetProductById($id);
		$this->objPro->GetNextRecord();
		  
		$data['item']['intItemId']         = $this->objPro->Get('intItemId'); 
		$data['item']['intTypeId']         = $this->objPro->Get('intTypeId');
		$data['item']['intCollectionId']   = $this->objPro->Get('intCollectionId');
		$data['item']['intCategoryId']     = $this->objPro->Get('intCategoryId');
		$data['item']['intSubCateId']      = $this->objPro->Get('intSubCateId');
		$data['item']['strItemName']       = $this->objPro->Get('strItemName');
		$data['item']['strItemCode']       = $this->objPro->Get('strItemCode');
		$data['item']['strSKU']            = $this->objPro->Get('strSKU');
		$data['item']['strWeight']         = $this->objPro->Get('strWeight');
		$data['item']['strNormalItemPrice'] = $this->objPro->Get('strNormalItemPrice');
		$data['item']['strSPrice']          = $this->objPro->Get('strSPrice');        
		$data['item']['strVPrice']          = $this->objPro->Get('strVPrice');     
		$data['item']['strQuantity']        = $this->objPro->Get('strQuantity');
		$data['item']['strImage']           = $this->objPro->Get('strImage');
		$data['item']['strDescription']     = $this->objPro->Get('strDescription');
		$data['item']['intStatus']          = $this->objPro->Get('intStatus');
		$data['item']['strStartDate']       = $this->objPro->Get('strStartDate');
		$data['item']['strEndDate']         = $this->objPro->Get('strEndDate');
		
		//
		// Get Category 
		//
		$arrCate = array();
		$this->objCate = new core_admin();
		$this->objCate->getAllCategory();
		while($this->objCate->GetNextRecord()){
			$data['caterory'][$this->objCate->Get('intCategoryId')] = $this->objCate->Get('strCategoryName');
			$data['arrCate'][$this->objCate->Get('intCategoryId')]  = $this->objCate->Get('strCategoryName');
		}
		//
		// Get Subcategory
		//
		$arrSubCate = array();
		$this->objSubCate = new core_admin();
		$this->objSubCate->getAllSubCategory();
		while($this->objSubCate->GetNextRecord()){
			$data['subcate'][$this->objSubCate->Get('intCategoryId')][$this->objSubCate->Get('intSubCategoryId')] = $this->objSubCate->Get('strSubCategoryName');
			$data['arrSubCate'][$this->objSubCate->Get('intCategoryId')][$this->objSubCate->Get('intSubCategoryId')] = $this->objSubCate->Get('strSubCategoryName');
			}
		//
		// Get Collection
		//
		$this->objCollection = new core_admin();
		$this->objCollection->getCollection();
		while($this->objCollection->GetNextRecord()){
			
			$data['collection'][$this->objCollection->Get('intCollectionId')]['intCollectionId'] = $this->objCollection->Get('intCollectionId');	
			$data['collection'][$this->objCollection->Get('intCollectionId')]['strCollectionName'] = $this->objCollection->Get('strCollectionName');	
			
		}
			
		return $this->_temp('show_copyedit_product_form.php',$mode,$c,$error,$data);
	}
	//
	// Show Return Form
	//
	function show_add_inventory(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$OUT->setVar('content',	$this->page_inventory_return_lists());
		$OUT->flush();		
	}
	//
	// Page Add Form Return Good
	//
	function page_inventory_return_lists(){
		global $CONF, $Q, $OUT,$LANG,$CODE;

		//
		// Get All Product Code
		//
		$this->objPro = new core_admin();
		$this->objPro->getAllProductCode();
		while($this->objPro->GetNextRecord()){
		  $data['productcode'][$this->objPro->Get('intItemId')] = $this->objPro->Get('strItemCode');	
		}
		
		if($Q->req['intSearch']==1){
			
			$id = trim($Q->req['intProductItem']);
			//
			// Get Item Details
			//
			$this->obj = new core_admin();
			$this->obj->doGetProductById($id);
			$this->obj->GetNextRecord();
			$data['itemdetails']['intItemId'] = $id;
			$data['itemdetails']['strItemName'] = $this->obj->Get('strItemName');
			$data['itemdetails']['strItemCode'] = $this->obj->Get('strItemCode');
			$data['itemdetails']['intTypeId']   = $this->obj->Get('intTypeId');
			$this->objatt = new core_admin();
			$this->objatt->doGetAttribute();
			while($this->objatt->GetNextRecord()){
				$data['itemdAtt'][$this->objatt->Get('intAttId')] = $this->objatt->Get('strValue');
			}
			$this->objvatt = new core_admin();
			$this->objvatt->doShowallvalue();
			while($this->objvatt->GetNextRecord()){
				$data['itemdAttV'][$this->objvatt->Get('intValueId')] = $this->objvatt->Get('variable');
			}
			if($this->obj->Get('intTypeId')!=1){
				//
				// Get the qc
				//
				$this->objvalue = new core_admin();
				$this->objvalue->doGetValue($id);
				while($this->objvalue->GetNextRecord()){
					$data['itemoption'][$this->objvalue->Get('intAttId')][$this->objvalue->Get('intAtt1')] = $this->objvalue->Get('intAtt1');
					}
				}
		}
		return $this->_temp('show_returnqty.php',$mode,$c,$error,$data);
	}
	//
	// Show Inventory
	//
	function show_inventory(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$OUT->setVar('content',	$this->page_inventoryform_lists());
		$OUT->flush();
	}
	//
	// Page inventory
	//
	function page_inventoryform_lists(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		//
		// Get All Product Code
		//
		$this->objPro = new core_admin();
		$this->objPro->getAllProductCode();
		while($this->objPro->GetNextRecord()){
		  $data['productcode'][$this->objPro->Get('intItemId')] = $this->objPro->Get('strItemCode');	
		}
		
		if($Q->req['intSearch']==1){
			
			$id = trim($Q->req['intProductItem']);
			$intItemId = trim($Q->req['intProductItem']);
			
			$arrCategory = array();
			$arrSubCate  = array();
			$arrCollection = array();
			//
			// Get All Category
			//
			$this->objcategory = new core_admin();
			$this->objcategory->getAllCategoryBoth();
			while($this->objcategory->GetNextRecord()){
				$arrCategory[$this->objcategory->Get('intCategoryId')]= $this->objcategory->Get('strCategoryName');
				
			}
			//
			// Get All SubCategory
			//
			$this->objSubcategory = new core_admin();
			$this->objSubcategory->getAllSubCategoryBoth();
			while($this->objSubcategory->GetNextRecord()){
				$arrSubCate[$this->objSubcategory->Get('intSubCategoryId')]= $this->objSubcategory->Get('strSubCategoryName');
				
			}
			
			
			//
			// Get All Collection
			//
			$this->objCollection= new core_admin();
			$this->objCollection->getAllColl();
			while($this->objCollection->GetNextRecord()){
				$arrCollection[$this->objCollection->Get('intCollectionId')]= $this->objCollection->Get('strCollectionName');
				
			}
			
			
			
      //
			// Get Item Details
			//
			$this->obj = new core_admin();
			$this->obj->doGetProductById($id);
			$this->obj->GetNextRecord();
			$data['itemdetails']['intItemId']        = $id;
			$data['itemdetails']['strItemName']      = $this->obj->Get('strItemName');
			$data['itemdetails']['strItemCode']      = $this->obj->Get('strItemCode');
			$data['itemdetails']['intCollectionId']  = $arrCollection[$this->obj->Get('intCollectionId')];
			$data['itemdetails']['intCategoryId']    = $arrCategory[$this->obj->Get('intCategoryId')];
			$data['itemdetails']['intSubCateId']     = $arrSubCate[$this->obj->Get('intSubCateId')];
		
			$data['itemdetails']['strSKU']           = $this->obj->Get('strSKU');
			$data['itemdetails']['strWeight']        = $this->obj->Get('strWeight');
			$data['itemdetails']['strNormalItemPrice']      = $this->obj->Get('strNormalItemPrice');
			$data['itemdetails']['strSPrice']        = $this->obj->Get('strSPrice');
			$data['itemdetails']['strVPrice']        = $this->obj->Get('strVPrice');
			$data['itemdetails']['strQuantity']      = $this->obj->Get('strQuantity');
			$data['itemdetails']['strImage']         = $this->obj->Get('strImage');
			$data['itemdetails']['strDescription']   = $this->obj->Get('strDescription');
			$data['itemdetails']['intTypeId']        = $this->obj->Get('intTypeId');
			
			
			
			$this->objatt = new core_admin();
			$this->objatt->doGetAttribute();
			while($this->objatt->GetNextRecord()){
				$data['itemdAtt'][$this->objatt->Get('intAttId')] = $this->objatt->Get('strValue');
			}
			$this->objvatt = new core_admin();
			$this->objvatt->doShowallvalue();
			while($this->objvatt->GetNextRecord()){
				$data['itemdAttV'][$this->objvatt->Get('intValueId')] = $this->objvatt->Get('variable');
			}
			if($this->obj->Get('intTypeId')!=1){
				//
				// Get the qc
				//
				$this->objvalue = new core_admin();
				$this->objvalue->doGetValue($id);
				while($this->objvalue->GetNextRecord()){
					$data['itemoption'][$this->objvalue->Get('intAttId')][$this->objvalue->Get('intAtt1')] = $this->objvalue->Get('intAtt1');
					}
				}
				
				
				
				
		
	
		if($Q->req['page']){ $intPage=$Q->req['page']; }
		else{ $intPage=1; }
			
			$this->objads = new core_admin();
			$this->objads->numperpage   = 50;
			$this->objads->numfirstpage = 50;
			$this->objads->getAllItemStore($intItemId,$intPage);;
		
			while($this->objads->GetNextRecord()){ 
				$data['item_total'] += $this->objads->Get('intQuantity');
				$data['item_qty'][$this->objads->Get('intItemStoreId')]['strQuantity']              = $this->objads->Get('intQuantity');
				$data['item_qty'][$this->objads->Get('intItemStoreId')]['CreateDate']               = $this->objads->Get('CreateDate');
			}
			
		
			// temp take data
			$this->pages 				= new Paginator();        
			$this->pages->items_total 	= $this->objads->page->total_pages;
			$this->pages->mid_range 	= 6;   
			$this->pages->default_ipp 	= 1;     
			$this->pages->paginate('|');
		
		//
		// Get More PHOTO
		//
		$this->objphoto = new core_admin();
		$this->objphoto->getMorePhoto($intItemId);
		while($this->objphoto->GetNextRecord()){
			$data['item_image'][$this->objphoto->Get('intItemStorePhotoId')]['strImage']  = $this->objphoto->Get('strImage');
			$data['item_image'][$this->objphoto->Get('intItemStorePhotoId')]['strStatus'] = $this->objphoto->Get('strStatus');
			$data['item_image'][$this->objphoto->Get('intItemStorePhotoId')]['strDate']   = $this->objphoto->Get('CreateDate');
			$data['item_image'][$this->objphoto->Get('intItemStorePhotoId')]['intItemId'] = $intItemId;
			
		}
		
		// Used Total Quantity
		$this->objused = new core_admin();
		$this->objused->getitemdetailtran($intItemId); 
		while($this->objused->GetNextRecord()){
			$data['item_used'] += $this->objused->Get('strQuantityBuy');
			}
		
		//
		// List All Attribute closetmino_attribute , closetmino_value
		//
		$arrAtt = array();
	  $this->objatt = new core_admin();
	  $this->objatt->doGetAttribute();
	  while($this->objatt->GetNextRecord()){
	  	$arrAtt[$this->objatt->Get('intAttId')] = $this->objatt->Get('strValue');
	  }	
		//
		// Closetmino_value
		//
		$arrAttV = array();
		$this->objattv = new core_admin();
	  $this->objattv->doShowallvalue();
	  while($this->objattv->GetNextRecord()){
	  	$arrAttV[$arrAtt[$this->objattv->Get('intAttId')]][$this->objattv->Get('intValueId')] = $this->objattv->Get('variable');
	  }	
		
		//
		// Get All blufroge_item_qc
		//
		$index = 1;
		$this->objGetAllQCS = new core_admin();
		$this->objGetAllQCS->getAllQCSSIZE($intItemId);
		while($this->objGetAllQCS->GetNextRecord()){
		
			$data['all_qcs_menu'][$this->objGetAllQCS->Get('intItemStoreId')][$arrAtt[$this->objGetAllQCS->Get('intAttId')]][$arrAttV[$arrAtt[$this->objGetAllQCS->Get('intAttId')]][$this->objGetAllQCS->Get('intAtt1')]]+= $this->objGetAllQCS->Get('intQuantity');
			$data['all_qcs'][$arrAtt[$this->objGetAllQCS->Get('intAttId')]][$arrAttV[$arrAtt[$this->objGetAllQCS->Get('intAttId')]][$this->objGetAllQCS->Get('intAtt1')]]+= $this->objGetAllQCS->Get('intQuantity');
			$data['attid'] = $this->objGetAllQCS->Get('intAttId');
			
			$index++;
		}
		
				
				
					//
			// Get total quatity
			//
			$this->objmore = new core_admin();
			$this->objmore->getitemdetail($intItemId); 
			while($this->objmore->GetNextRecord()){
				$intTotal += $this->objmore->Get('intQuantity');
			}
			//
			// Get all used quantity
			//
			$this->objmore = new core_admin();
			$this->objmore->getitemdetailtran($intItemId); 
			while($this->objmore->GetNextRecord()){
				$intTotalUsed += $this->objmore->Get('strQuantityBuy');
			}
			$data['TotalQuantity'] = $intTotal - $intTotalUsed;
			
			$data['TotalSold'] =	$intTotalUsed;
						
		}
		return $this->_temp('show_inventory_control.php',$mode,$c,$error,$data);
		
		
	}
	//
	// Do Delete Quantity
	//
	function do_deleteQuantity(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		
		
		$intItemId      = $Q->req['id'];
		$storeid        = $Q->req['storeid'];
		$intProductType = $Q->req['intProductType'];
		$strAddedBy     = $Q->cookies['admin:user_id'];
		$qSearch        = $Q->req['qSearch'];
		

		if($intProductType==1){
			//
			// find closetmino_item_store
			//
			$this->obj = new core_admin();
			$this->obj->doGetQuantity($intItemId ,$storeid);	
			$this->obj->GetNextRecord();
			$intStoreQuantity =  $this->obj->Get('intQuantity');
			//
			// Deduct blufroge_item first
			//
			$this->objmain = new core_admin();
			$this->objmain->doGetitemQ($intItemId);
			$this->objmain->GetNextRecord();
			$intTotalQuantity =  $this->objmain->Get('strQuantity');
			$intFinalTotal = $intTotalQuantity - $intStoreQuantity;
			//
			// Check Quantity Order / being used
			//
			$this->objquantityinorder = new core_admin();
			$this->objquantityinorder->doGetQuantityOrder($intItemId);
			while($this->objquantityinorder->GetNextRecord()){
				$intQuantityOrder +=	$this->objquantityinorder->Get('strQty');
			}
			
			if($intQuantityOrder > $intFinalTotal){
				
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_inventory&intProductItem='.$qSearch.'&intSearch=1&msgError=1');
		    
				exit;
			}
			//
			// Insert Table_Delete
			//
			$strProductDetails = "-";
			$this->objinsertDelete = new core_admin();
			$result = $this->objinsertDelete->doInsertDeleteAgent($intItemId ,$intStoreQuantity,$intProductType,$strProductDetails,$strAddedBy);
			if($result > 0){
				//
				// Update blufroge_item
				//
				$this->objupdateItem = new core_admin();
				$resultUpdate = $this->objupdateItem->doUpdateQuantity($strAddedBy ,$intItemId,$intFinalTotal);
				if($resultUpdate==0){
					echo "Error : Update Item Quantity...";
					exit;
					}
				//
				// Delete the item store
				//
				$this->objdatestore = new core_admin();
				$re = $this->objdatestore->doDeleteQuantityStore($storeid,$intItemId,$strAddedBy);
				if($re==0){
					 echo "Error : Delete unsucessful...";
					 exit;
				}
				
				
					$OUT->redirect($CONF['url_app'].'?m=admin&c=show_inventory&intProductItem='.$qSearch.'&intSearch=1&msgSucess=1');
		
			}
			exit;
		}
		if($intProductType==2){
			
			//
			// sms_company_item_store
			//
			$this->obj = new core_admin();
			$this->obj->doGetQuantity($intItemId ,$storeid);	
			$this->obj->GetNextRecord();
			$intStoreQuantity =  $this->obj->Get('intQuantity');
			
			
			
			
			
			//
			// Deduct blufroge_item first
			//
			$this->objmain = new core_admin();
			$this->objmain->doGetitemQ($intItemId);
			$this->objmain->GetNextRecord();
			$intTotalQuantity =  $this->objmain->Get('strQuantity');
			
			
			
			
			
			//
			// blufroge_item_qc
			//
			$this->objqc = new core_admin();
			$this->objqc->doGetQuantityQC($intItemId ,$storeid);	
		  $arrQSC = array();
		  $arrColor = array();
		  $arrSize = array();
		  $arrSerial = array();
		  while($this->objqc->GetNextRecord()){
		  	$arrQSC[$intItemId][$this->objqc->Get('intAtt1')][$this->objqc->Get('intAtt2')] = $this->objqc->Get('intQuantity');
		  	$arrColor[$this->objqc->Get('intAtt1')] = $this->objqc->Get('intAtt1');
		  	$arrSerial['itemId'][$intItemId]['intAtt1']['size'][$this->objqc->Get('intAtt2')]['qty'] = $this->objqc->Get('intQuantity');
		  }
	

		  $countColor = count($arrColor);
		  
		  
		  //echo "<pre>";
		  //print_R($arrColor);
		  if($countColor > 0){
		  	
		  //
			// closetmino_qc_total
			//
			$arrColorRecord = array();
		  $this->objtotal = new core_admin();
		  $this->objtotal->doGetTotalQC($intItemId, $arrColor);
		  while($this->objtotal->GetNextRecord()){
		  	$arrColorRecord[$intItemId][$this->objtotal->Get('intAtt1')][$this->objtotal->Get('intAtt2')] = $this->objtotal->Get('intQuantity');
		  	$intAttId = $this->objtotal->Get('intAttId');
		  }
		  
		  
		  
		  
	
		  
		  
		  //
		  // Used qc total
		  //
		  $arrUsedColor = array();
		  $this->objused = new core_admin();
		  $this->objused->doGetUsedQC($intItemId, $arrColor);
		  while($this->objused->GetNextRecord()){
		  	$arrUsedColor[$intItemId][$this->objused->Get('intAtt1')][$this->objused->Get('intAtt2')] = $this->objused->Get('intQuantity');
	  	}
		  
		  

		  
		  
		  
		  
		  $arrMashup = array();
		  foreach($arrColorRecord as $intTid => $k){
		  	foreach($k as $size => $v){
		  		foreach($v as $vv => $quantityDB){
		  			$intQMashup =  $quantityDB - $arrQSC[$intTid][$size][$vv];
		  			//if($intQMashup > 0){
			  	  $arrMashup[$intTid][$size][$vv] = $intQMashup;
			  			//}
		  			}
		  	}
		  }
		  
		  		  	  	
		  
		  
		  
	$strProductDetails = gzcompress(serialize($arrQSC), 9);	  
		
	
		  //print_R($arrMashup);
		  $cMashup = count($arrMashup);
		  //if($cMashup > 0){
		  	foreach($arrMashup as $u => $ru){
		  		foreach($ru as $rru => $rrru){
		  			foreach($rrru as $rrrrU => $ruQ){
		  				$intDeduct =  $ruQ - $arrUsedColor[$u][$rru][$rrrrU];
		  			
		  		
		  				
		  				if(	$intDeduct < 0){
		  				
		  				$OUT->redirect($CONF['url_app'].'?m=admin&c=show_inventory&intProductItem'.$intItemId.'&intSearch=1&msgError=1');
							exit;
		  					}
		  				
		  				}
		  			
		  			}
		  		
		  		}
	
		 
		 	//
		 	// Insert Delete Quantity
		 	//
		  $this->objinsertDelete = new core_admin();
			$result = $this->objinsertDelete->doInsertDeleteAgent($intItemId ,$intStoreQuantity,$intProductType,$strProductDetails,$strAddedBy);
		  
		 
		 
		 
		  //
			// Delete the item store
			//
			$this->objdatestore = new core_admin();
			$re = $this->objdatestore->doDeleteQuantityStore($storeid,$intItemId);
		  
		  
		  
				
		  //
		  // Delete blufroge_item_qc
		  //
		  $this->objdeleteitemqc = new core_admin();
		  $this->objdeleteitemqc->doDeleteitemqc($storeid,$intItemId);		
		  //
		  // Delete blufroge_qc_total
		  //
		  $this->objDeleteQCTotal = new core_admin();
		 	$this->objDeleteQCTotal->doDeleteQCTotal($intItemId,$arrColorRecord);	
		  
		// echo "<pre>";
		// print_R($arrMashup);
		  
		// echo "<pre>";
		// print_R($arrColorRecord); 
		  //
		  //Insert blufroge_qc_total
		  //		
		 	$this->objInsertitemqc = new core_admin();
		 	$this->objInsertitemqc->doAddQCSQtyTotal($arrMashup,$intOption,$intItemId,$intAttId ,$strAddedBy);
		  
		  
		  //
		  // Query Total blufroge_qc_total
		  //
		  $this->objTotalitem = new core_admin();
		  $this->objTotalitem->GetTheQctotal($intItemId);
		  while($this->objTotalitem->GetNextRecord()){
		  	$intTotalUp +=$this->objTotalitem->Get('intQuantity');
		  
		  }
		  //
		  // Update sms_company_item quantity
		  //
		  $this->objupdatecompnayitem = new core_admin();
		  $this->objupdatecompnayitem->doUpdatetheQuantityItem($intItemId,$intTotalUp,$strAddedBy);
			// exit;
		  $OUT->redirect($CONF['url_app'].'?m=admin&c=show_inventory&intProductItem='.$qSearch.'&intSearch=1&msgSucess=1');
		
			exit;
		  }
		  else{
		  //
			// Insert Table_Delete
			//
			$strProductDetails = "-";
			$intProductType = 2;
			$this->objinsertDelete = new core_admin();
			$result = $this->objinsertDelete->doInsertDeleteAgent($intItemId ,$intStoreQuantity,$intProductType,$strProductDetails,$strAddedBy);
		  if($result==0){
					echo "Error : Insert Delete Table";
					exit;		  	
		  	}
		  else{
		  $intFinalTotal = 	$intTotalQuantity - $intStoreQuantity;
		  //
				// Update blufroge_item
				//
				$this->objupdateItem = new core_admin();
				$resultUpdate = $this->objupdateItem->doUpdateQuantity($strAddedBy ,$intItemId,$intFinalTotal);
				if($resultUpdate==0){
					echo "Error : Update Item Quantity...";
					exit;
					}
				//
				// Delete the item store
				//
				$this->objdatestore = new core_admin();
				$re = $this->objdatestore->doDeleteQuantityStore($storeid,$intItemId,$strAddedBy);
				if($re==0){
					 echo "Error : Delete unsucessful...";
					 exit;
				}
			 	$OUT->redirect($CONF['url_app'].'?m=admin&c=show_inventory&intProductItem='.$qSearch.'&intSearch=1&msgSucess=1');
			exit;
		  	
		  }	
	  }
	}
	
		
		
	}
	//
	// Do Update Return Quantity
	//
	function show_editon_inventory(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$strPoId         = $Q->req['strPoId'];
		$strProductCode  = $Q->req['strProductCode'];
		$strProductName  = $Q->req['strProductName'];
		$intTypeId       = $Q->req['intTypeId'];
		$intValueId      = $Q->req['intValueId'];
		$intAttId        = $Q->req['intAttId'];
		$strAddedBy      = $Q->cookies['admin:user_id'];
		$strRemarks      = $Q->req['strRemarks'];
		$intQtyReturn    = $Q->req['intQtyReturn'];
		$intItemId       = $Q->req['intItemId'];
		$intId           = $Q->req['intId'];
		
		if($intTypeId==1){
			$intAttId  = 0;
			$intValueId = 0;
			}
		
		//
		// Do Insert return quantity
		//
		$this->obj = new core_admin();
		$res = $this->obj->doUpdateReturnQuantity(
			array(	
			'strPoId'        => $strPoId,        
			'strProductCode' => $strProductCode, 
			'strProductName' => $strProductName , 
			'intQtyReturn'   => $intQtyReturn  ,
			'intTypeId'      => $intTypeId,      
			'intValueId'     => $intValueId ,    
			'intAttId'       => $intAttId,       
			'strAddedBy'     => $strAddedBy,     
			'strRemarks'     => $strRemarks,  
			'intItemId'      => $intItemId,   
			'intQtyReturn'   => $intQtyReturn,
			'intId'          => $intId
			
			));
		
		if($res > 0){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_edit_returnform&id='.$intId.'&msg=1');
			}
		
		
	}
	//
	// Do Add Return Form
	//
	function show_addon_inventory(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$strPoId         = $Q->req['strPoId'];
		$strProductCode  = $Q->req['strProductCode'];
		$strProductName  = $Q->req['strProductName'];
		$intTypeId       = $Q->req['intTypeId'];
		$intValueId      = $Q->req['intValueId'];
		$intAttId        = $Q->req['intAttId'];
		$strAddedBy      = $Q->cookies['admin:user_id'];
		$strRemarks      = $Q->req['strRemarks'];
		$intQtyReturn    = $Q->req['intQtyReturn'];
		$intItemId       = $Q->req['intItemId'];
		
		if($intTypeId==1){
			$intAttId  = 0;
			$intValueId = 0;
			}
		
		//
		// Do Insert return quantity
		//
		$this->obj = new core_admin();
		$res = $this->obj->doAddReturnQuantity(
			array(	
			'strPoId'        => $strPoId,        
			'strProductCode' => $strProductCode, 
			'strProductName' => $strProductName , 
			'intQtyReturn'   => $intQtyReturn  ,
			'intTypeId'      => $intTypeId,      
			'intValueId'     => $intValueId ,    
			'intAttId'       => $intAttId,       
			'strAddedBy'     => $strAddedBy,     
			'strRemarks'     => $strRemarks,  
			'intItemId'      => $intItemId,   
			'intQtyReturn'   => $intQtyReturn));
		
		if($res > 0){
			$OUT->redirect($CONF['url_app'].'?m=admin&c=show_add_inventory&msg=1');
			}
		
		
	}
	//
	// Do Edit Product
	//
	function do_update_product(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intTypeid       = $Q->req['intTypeid'];
		$intCollectionid = $Q->req['intCollectionid'];
		$intCate         = $Q->req['intCate'];
		$intSubCate      = $Q->req['intSubCate'];
		$strProductName  = $Q->req['strProductName'];
		$strProductCode  = $Q->req['strProductCode'];
		$strSKU          = $Q->req['strSKU'];
		$strWeight       = $Q->req['strWeight'];
		$strNPrice       = $Q->req['strNPrice'];
		$strSPrice       = $Q->req['strSPrice'];
		$strVIPPrice     = $Q->req['strVIPPrice'];
		$intQuantity     = $Q->req['intQuantity'];
		$strPic          = $_FILES['strPic']['tmp_name'];
		$intStatus       = $Q->req['intStatus'];
		$intDayEnd       = $Q->req['intDayEnd'];
		$strStartDateF   = $Q->req['strStartDate'];
		$strStartDateE   = $Q->req['strEndDate'];
		$id              = $Q->req['id'];
		$arrPointType      = explode(":",$Q->req['intRedemption']);
		$intPointRedemption = $arrPointType[0] ;
		
		
		if($intPointRedemption=="Y"){
			 $intPoint           = $Q->req['intPoint'];
			}
		else{
			$intPoint = 0;
			
			}
		//
		// explode Date
		//
		$strexplodeDate  = explode("/",$strStartDateF);
		
		$strexplodeDateE  = explode("/",$strStartDateE);
		//$strStartDateE  = $strexplodeDateE[1]."-".$strexplodeDateE[0]."-".$strexplodeDateE[2];
		$todayExpiryMk = mktime(0,0,0,$strexplodeDateE[0],$strexplodeDateE[1] ,$strexplodeDateE[2]);

		$postArray = &$_POST ;
	   foreach ( $postArray as $sForm => $value ){
			if(get_magic_quotes_gpc() )
				$strDescription = htmlspecialchars( stripslashes( $value ) ) ;
			else
				$strDescription = htmlspecialchars( $value ) ;
		}
		$strAddedBy  = $Q->cookies['admin:user_id'];
		
		
			if(strpos($_SERVER['HTTP_USER_AGENT'],"Safari")!=FALSE){
			$find	= array('%0D', '%0A');
			$replace= array('', '');
			$filterstrDescription = str_replace($find, $replace, rawurlencode($strDescription)); 
			$filterstrDescription = rawurldecode($filterstrDescription); 
			//
			// Video
			//
				$find3	= array('%0D', '%0A');                                                               
				$replace3= array('', '');                                                                    
				$strVideoUrl = str_replace($find3, $replace3, rawurlencode($strUrlVideo));  
				$strVideoUrl = rawurldecode($strVideoUrl);                               
			
		}
		else{
			$filterstrDescription  = str_replace("\n","", $strDescription); 
			$strVideoUrl = str_replace("\n","", $strUrlVideo); 
			
		}
		//
		// Validation
		//
		if($intSubCate==""){
			$intSubCate = 0;
			}
		if($strSKU==""){
			$strSKU = "-";
			}
		if($strWeight==""){
			$strWeight = "-";
			}
		
		$this->obj = new core_admin();
		 $this->obj->doEditProduct(array(
		   'intTypeid'      => $intTypeid,           
		   'intCollectionid'=> $intCollectionid,     
		   'intCate'        => $intCate,             
		   'intSubCate'     => $intSubCate,          
		   'strProductName' => $strProductName,      
		   'strProductCode' => $strProductCode,      
		   'strSKU'         => $strSKU,              
		   'strWeight'      => $strWeight,           
		   'strNPrice'      => $strNPrice ,          
		   'strSPrice'      => $strSPrice,           
		   'strVIPPrice'    => $strVIPPrice , 
		   'intQuantity'    => $intQuantity,       
		   'strPic'         => $strPic,              
		   'intStatus'      => $intStatus,  
		   'strDesc'        => $strDescription, 
		   'strStartDate'   => $strStartDateF, 
		   'strStartDateE'  => $strStartDateE,
		   'intMkDayEnd'    => $todayExpiryMk, 
		   'strAddedBy'     => $strAddedBy,
		   'strPointRedemption' => $intPointRedemption,
		   'intPoint'       => $intPoint,
		   'id'             => $id
	    )); 
		   
		
		  $folder_id    = (int)($id / $CONF['const_photo_per_dir'    ]);  
		  $rand = rand(1000000, 9999999);  
           
            if($strPic!=""){
             if(!file_exists( $CONF['dir_photo']."product_pic/".$folder_id."/")){
                        mkdir($CONF['dir_photo'].'/product_pic/'.$folder_id.'/',0777);
                }
                if(!file_exists( $CONF['dir_photo']."product_pic/".$folder_id."/".$id."/")){
                        mkdir($CONF['dir_photo'].'/product_pic/'.$folder_id.'/'.$id.'/',0777);
                } 
                $upload_dirBig     = $CONF['dir_photo'].'product_pic/'.$folder_id.'/'.$id.'/'; 
                
                $arrfilename = $rand.'_'.basename($_FILES['strPic']['name']);
          
            move_uploaded_file($_FILES['strPic']['tmp_name'], $upload_dirBig.'/'.$arrfilename);
           $params = array(array('size' => 320,
                          'file' => $upload_dirBig.'/s320-'.$arrfilename),
                          array('size' => 200,
                          'file' => $upload_dirBig.'/s200-'.$arrfilename),
                    array('size' => 156,
                          'file' => $upload_dirBig.'/s156-'.$arrfilename),
                    array('size' => 46,
                          'file' => $upload_dirBig.'/s46-'.$arrfilename),
                    array('size' => 40,
                          'file' => $upload_dirBig.'/s40-'.$arrfilename));
    
  

    if (thumbnail_generator($upload_dirBig.'/'.$arrfilename, $params) == false)
        die("Error processing uploaded file {$arrfilename}");
        
      $this->objupdate = new core_admin();
      $this->objupdate->doInsertImageAdPic($id,$arrfilename);
        } 
		
		
		$OUT->redirect($CONF['url_app']. "?m=admin&c=show_edit_product&id=".$id."&msg=DoneUpdate");
		
	}
	//
	// Do Delete 
	//
	function do_deleteReturn(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$arrReturnId = $Q->req['intReturnId'];
		$strAddedBy  = $Q->cookies['admin:user_id'];
		//
		// Do Delete Quantity Note
		//
		$this->obj = new core_admin();
		$this->obj->doDeleteNote($arrReturnId,$strAddedBy);
		
		$OUT->redirect($CONF['url_app']."?m=admin&c=show_inventory_return&msg=2");
	}
	//
	// Show the Form
	//
	function show_edit_returnform(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$OUT->setVar('content',	$this->page_edit_qtyreturn());
		$OUT->flush();	
	}
	//
	// Page qty
	//	

	
	function page_edit_qtyreturn(){
		global $CONF, $Q, $OUT,$LANG,$CODE;

		//
		// Get All Product Code
		//
		$this->objPro = new core_admin();
		$this->objPro->getAllProductCode();
		while($this->objPro->GetNextRecord()){
		  $data['productcode'][$this->objPro->Get('intItemId')] = $this->objPro->Get('strItemCode');	
		}
	
			
			$id = trim($Q->req['id']);
			//
			// Get Item Details
			//
			$this->obj = new core_admin();
			$this->obj->doGetReturnById($id);
			$this->obj->GetNextRecord();
			$data['itemdetails']['intItemId'] = $this->obj->Get('intItemId');
			$data['itemdetails']['strItemName'] = $this->obj->Get('strItemName');
			$data['itemdetails']['strItemCode'] = $this->obj->Get('strItemCode');
			$data['itemdetails']['intTypeId']   = $this->obj->Get('intTypeId');
			$data['itemdetails']['intValueId']   = $this->obj->Get('intValueId');
			
			$data['itemdetails']['strPoId'] = $this->obj->Get('strPoId');
			$data['itemdetails']['intQtyReturn'] = $this->obj->Get('intQtyReturn');
			$data['itemdetails']['strRemarks']   = $this->obj->Get('strRemarks');
			$this->objatt = new core_admin();
			$this->objatt->doGetAttribute();
			while($this->objatt->GetNextRecord()){
				$data['itemdAtt'][$this->objatt->Get('intAttId')] = $this->objatt->Get('strValue');
			}
			$this->objvatt = new core_admin();
			$this->objvatt->doShowallvalue();
			while($this->objvatt->GetNextRecord()){
				$data['itemdAttV'][$this->objvatt->Get('intValueId')] = $this->objvatt->Get('variable');
			}
			if($this->obj->Get('intTypeId')!=1){
				$ids = 	$data['itemdetails']['intItemId'];
				//
				// Get the qc
				//
				$this->objvalue = new core_admin();
				$this->objvalue->doGetValue($ids);
				while($this->objvalue->GetNextRecord()){
					$data['itemoption'][$this->objvalue->Get('intAttId')][$this->objvalue->Get('intAtt1')] = $this->objvalue->Get('intAtt1');
					}
				}
		
		return $this->_temp('show_returnqty_edit.php',$mode,$c,$error,$data);
	}
	
	
	//
	//Show Edit Tag
	//
	function show_edit_tag(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$OUT->setVar('content',	$this->page_edit_tagging());
		$OUT->flush();	
	}
	//
	// Page Edit Tag
	//	
	function page_edit_tagging(){
		global $CONF, $Q, $OUT,$LANG,$CODE;
		
		$id = $Q->req['id'];
		//
		// Get Category 
		//
		$arrCate = array();
		$this->objCate = new core_admin();
		$this->objCate->getAllCategory();
		while($this->objCate->GetNextRecord()){
			$data['caterory'][$this->objCate->Get('intCategoryId')] = $this->objCate->Get('strCategoryName');
			$data['arrCate'][$this->objCate->Get('intCategoryId')]  = $this->objCate->Get('strCategoryName');
		}
		//
		// Get Subcategory
		//
		$arrSubCate = array();
		$this->objSubCate = new core_admin();
		$this->objSubCate->getAllSubCategory();
		while($this->objSubCate->GetNextRecord()){
			$data['subcate'][$this->objSubCate->Get('intCategoryId')][$this->objSubCate->Get('intSubCategoryId')] = $this->objSubCate->Get('strSubCategoryName');
			$data['arrSubCate'][$this->objSubCate->Get('intCategoryId')][$this->objSubCate->Get('intSubCategoryId')] = $this->objSubCate->Get('strSubCategoryName');
			}
			
		//
		// Purposely Tagging
		//	
		$arrTag = array();
		$this->objtag = new core_admin();
		$this->objtag->getAllCategoryTag();
		while($this->objtag->GetNextRecord()){
			$data['Tag'][$this->objtag->Get('intCategoryId')] = $this->objtag->Get('strCategoryName');
		}	
		//
		// Get The Existing Tag
		//
		$this->tagexists = new core_admin();
		$this->tagexists->doGetTagExisting($id);
		while($this->tagexists->GetNextRecord()){
			$data['existing'][ $this->tagexists->Get('intCateSubCateId')] = $this->tagexists->Get('intCateSubCateId');
			
		}
	  return $this->_temp('show_edit_tag_form.php',$mode,$c,$error,$data);		
		
	} 
	
	//
	// Do Tagging
	//
	function do_edit_tag(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$intItemId = $Q->req['intItemId'];
		$arrTag    = $Q->req['arrTag'];
		$strAddedBy  = $Q->cookies['admin:user_id'];
		//
		// Delete All Tag
		//
		$this->obj = new core_admin();
		$resut = $this->obj->doDeleteTag($intItemId); 
		
		if($resut > 0){
		//
		// Re-insert Tag
		//
		$this->objadd = new core_admin();
		$this->objadd->doInsertTag($intItemId,$arrTag,$strAddedBy);
			
			$OUT->redirect($CONF['url_app']."?m=admin&c=show_edit_tag&id=".$intItemId."&msg=1");
			
		}
		//exit;
	}
	//
	// Show Inventory
	//
	function show_inventory_return(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_inventory_return());
		$OUT->flush();
		
	}
	//
	// Page inventory Return
	//
	function page_inventory_return(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
			
		
			$this->objatt = new core_admin();
			$this->objatt->doGetAttribute();
			while($this->objatt->GetNextRecord()){
				$data['itemdAtt'][$this->objatt->Get('intAttId')] = $this->objatt->Get('strValue');
			}
			$this->objvatt = new core_admin();
			$this->objvatt->doShowallvalue();
			while($this->objvatt->GetNextRecord()){
				$data['itemdAttV'][$this->objvatt->Get('intValueId')] = $this->objvatt->Get('variable');
			}
	
		if($Q->req['page']){ $intPage=$Q->req['page']; }
		else{ $intPage=1; }
			
			$this->objads = new core_admin();
			$this->objads->numperpage   = 50;
			$this->objads->numfirstpage = 50;
			$this->objads->getAllReturnStore($intPage);
		
			while($this->objads->GetNextRecord()){ 
				$data['item_total'] += $this->objads->Get('intQtyReturn');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['intInvRId']              = $this->objads->Get('intInvRId');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['intItemId']              = $this->objads->Get('intItemId');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['intAttId']               = $this->objads->Get('intAttId');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['strPoId']               = $this->objads->Get('strPoId');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['intValueId']             = $this->objads->Get('intValueId');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['strItemCode']            = $this->objads->Get('strItemCode');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['strItemName']            = $this->objads->Get('strItemName');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['intQtyReturn']           = $this->objads->Get('intQtyReturn');
				$data['item_return_qty'][$this->objads->Get('intInvRId')]['strRemarks']             = $this->objads->Get('strRemarks');
			}
			
		
			// temp take data
			$this->pages 				= new Paginator();        
			$this->pages->items_total 	= $this->objads->page->total_pages;
			$this->pages->mid_range 	= 6;   
			$this->pages->default_ipp 	= 1;     
			$this->pages->paginate('|');
		
		
		
		
		
		 return $this->_temp('show_inventory_lists.php',$mode,$c,$error,$data);
		
	}
	
	
	//
	// Do Add Quantitry & Size
	//
	function do_add_csq(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intItemId  = $Q->req['intItemId'];
		$intStoreId = $Q->req['intStoreId'];
		$intOption  = $Q->req['intAttId'];
		//
		// Get the Attribute By intAttribute Id
		//
		$this->objall = new core_admin();
		$this->objall->doShowallvalueByid($intOption);
		while($this->objall->GetNextRecord()){
			$data['all_value'][$this->objall->Get('intValueId')] = $this->objall->Get('variable');
		}
		
	
	$fzero = 0;
		foreach($data['all_value'] as $k => $Size){
			$arrSize[$k]['size']     = $Q->req['size_'.$k];
			$arrSize[$k]['quantity'] =$Q->req['qs_'.$k];
			$i++;
		}
		$strAddedBy  = $Q->cookies['admin:user_id'];

		$c = array();
		$s = array();
		$q = array();
		$i=0;
		$j=0;
		//
		// Mashup array
		//
		$arrFinal = array();
		foreach($arrSize as $n => $t){
			array_push($c,$n);
			foreach($t as $m => $vv){
				if(count($vv) > 0){
				foreach($vv as $r => $rr){
					if($m=="size"){
						if($rr!=""){
							$s[$n][$i] = $rr;
							$i++;
						}
				}
			if($m=="quantity"){
				if($rr!=""){
					$q[$n][$j] = $rr;
					$j++;
							}
						}
					}
				}
			}
		}
	
		foreach($s as $h => $size){
			foreach($size as $k => $v){
					$arrFinal[$h][$v] =$q[$h][$k];
			  }
		}
		//
		// Query the total qc size
		//
		$arrExistingData = array();
		$this->objqcs = new core_admin();
		$this->objqcs->doGetExisting($intItemId);
		while($this->objqcs->GetNextRecord()){
			$arrExistingData[$this->objqcs->Get('intAtt1')][$this->objqcs->Get('intAtt2')] += $this->objqcs->Get('intQuantity');
			
		}
	
		//
		// Mashup the array
		//
		$arrUpdateMashup = array();
		$arrNewColor = array();
		$arrUpdateSize =  array();
		foreach($arrExistingData as $intColor => $postA){
			foreach($arrFinal as $intColorSelect => $postB){
				if($intColor==$intColorSelect){
				foreach($postA as $intSize => $qA){
					foreach($postB as $intSelectSize => $qB){
						if($intSize==$intSelectSize){
							$arrUpdateMashup[$intColor][$intSelectSize] = $qB + $qA;
							}
						else{
							$arrUpdateSize[$intColorSelect][$intSize] = $qA;  
							}
						}
					}
				}
			}
		}
		
		$arrNewFinalMashup =  array();
		
		foreach($arrFinal as $k => $c){
			foreach($c as $f => $rr){
				
			  if($arrMashup[$k][$f]!=""){
			 $arrNewFinalMashup[$k][$f] = $arrMashup[$k][$f];
			}
			else{
				 $arrNewFinalMashup[$k][$f] = $rr;
				}
				}
			}
			
	  
		$cNewMaship = count($arrNewFinalMashup);
		if($cNewMaship > 0){
		$this->objAddQCS = new core_admin();
	  $res = $this->objAddQCS->doAddTotalQCS($intItemId,$intOption ,$arrNewFinalMashup,$strAddedBy);
	  
		}
		
		
		
	
		
		$cUpdate = count($arrUpdateMashup);
		if($cUpdate > 0){
			//
			// Delete first and update later
			//
			$this->objUpdateDQCS = new core_admin();
			$this->objUpdateDQCS->doDeleteQCSAdd($intItemId,$arrUpdateMashup);
			
			
			$this->objUpdateQCS = new core_admin();
			$this->objUpdateQCS->doAddTotalQCS($intItemId,$intOption ,$arrUpdateMashup,$strAddedBy);
			
		}
		
		//
		// Do insert color
		//
		$this->objaqcs = new core_admin();
		$result = $this->objaqcs->doAddQCSQty($arrFinal,$intOption,$intItemId,$intStoreId,$strAddedBy);
		
		//
		// Get the total ac
		//
		$this->objGetTotal = new core_admin();
		$this->objGetTotal->doGetTotal($intStoreId);
		while($this->objGetTotal->GetNextRecord()){
			$intTotal +=$this->objGetTotal->Get('intQuantity');
		}
		
		//
		// Update the  closetmino_item_store
		//
		$this->objupdateQc = new core_admin();
		$this->objupdateQc->doUpdateitemStore($intTotal,$intStoreId,$strAddedBy);
		
		
		//
		// Update the   sms_company_item_store
		//
		$this->objupdateQc = new core_admin();
		$this->objupdateQc->doGetitemStoreItem($intItemId);
		while($this->objupdateQc->GetNextRecord()){
			$intAllOver += $this->objupdateQc->Get('intQuantity');
		}
		//
		// Update  sms_company_item
		//
		$this->objupdateItemMain = new core_admin();
		$this->objupdateItemMain->doUpdateComitem($intAllOver,$intItemId,$strAddedBy);
		
		
	  $strSizeBring = $Q->req['strSize'];
		
		if($Q->req['popup']==1){
			
				$OUT->redirect($CONF['url_app']. "?m=admin&c=show_semiqtyform_pop&id=".$intItemId."&intStoreId=".$intStoreId."&intAttId=".$intOption."&msg=DoneInsertPopup");
				
			}
		else{
			
			$OUT->redirect($CONF['url_app']. "?m=admin&c=show_inventory&intSearch=1&intProductItem=".$intItemId."&intPage=".$intPage."&msg=DoneInsert");
			}
	}
	//
	// Collection form
	//
	function show_add_collection(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_collection_form());
		$OUT->flush();
	}
	//
	// Page collection form
	//
	function page_collection_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
			
		return $this->_output(DIR_TEMP_PATH_BO . '/admin/show_collectionform.php',$data,$error);
	}
	//
	// Do Add Collection
	//
	function do_add_collection(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;
		
		$strCollectionName = trim($Q->req['strCollection']);
		$strStatus         = $Q->req['strStatus'];
		$strAddedBy        = $Q->cookies['admin:user_id'];
		
		
		if($strCollectionName==""){
			$OUT->addError($LANG->q('err_collection'));	
		}
		//
		// Check Database
		//
		$this->obj = new core_admin();
		$data = $this->obj->doCheckCollection($strCollectionName);
		
	
		if($data['intCollectionId'] > 0){
			$OUT->addError($LANG->q('err_collection_unavailiable'));	
		}
		if($OUT->isError()){
			$this->show_error('err_add_collection');
			exit;
		}
		
		
		//
		// Do Insert Collection
		//
		$this->obj = new core_admin();
		$result = $this->obj->doInsertCollection($strCollectionName,$strStatus , $strAddedBy);
		if($result == 0){
			echo "Error : Insert Collection";
		}
		
		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_collection&msg=1');
		
		
	}
	
	//
	// Do search User by date
	//
	function do_search_mail(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;
		
		//
		// Get Date
		//
		$strDateSelect = $Q->req['strStartDate'];
  	$selectexplodedte = explode('/',$strDateSelect);
	
		if($selectexplodedte[1]< 10){
		   $strDate = "0".$selectexplodedte[1];	
		}
		else{
			
			$strDate = $selectexplodedte[1];	
			
			}
		if($selectexplodedte[0]< 10){
		   $strMonth = "0".$selectexplodedte[0];	
		}
		else{
			
			$strMonth = $selectexplodedte[0];	
			
			}

		$todayMk = mktime(0,0,0,$strMonth,$strDate,$selectexplodedte[2]);

		//
		// Get Active User and Email Not NULL
		//
		$this->objUser = new core_admin();
		$this->objUser->getActiveUser();
		$dataUser = array();
		while($this->objUser->GetNextRecord()){
			$dataUser[$this->objUser->Get('intAccId')]['intAccId']  = $this->objUser->Get('intAccId');
			$dataUser[$this->objUser->Get('intAccId')]['strEmail']  = $this->objUser->Get('strEmail');
			$dataUser[$this->objUser->Get('intAccId')]['strMobile'] = $this->objUser->Get('strMobile');
			$dataUser[$this->objUser->Get('intAccId')]['strUser']   = $this->objUser->Get('strUser');
		  $dataUser[$this->objUser->Get('intAccId')]['CreateDate']= substr($this->objUser->Get('CreateDate'),0,10);
		  $explodedte = explode('-',$dataUser[$this->objUser->Get('intAccId')]['CreateDate']);
		  $datemktime = mktime(0,0,0,$explodedte[1],$explodedte[2],$explodedte[0]);
		 	$dataUser[$this->objUser->Get('intAccId')]['mktimedate'] = $datemktime;
		  
		  
		}
		//
		// Filter Date
		//
		$data = array();
		foreach($dataUser as $key => $fdetails){
		 		
		
			if($fdetails['mktimedate'] <= $todayMk){
				$data[$fdetails['intAccId']]['intAccId']  = $fdetails['intAccId'];
				$data[$fdetails['intAccId']]['strEmail']  = $fdetails['strEmail'];
				$data[$fdetails['intAccId']]['strMobile'] = $fdetails['strMobile'];
				$data[$fdetails['intAccId']]['strUser']   = $fdetails['strUser'];
				$data[$fdetails['intAccId']]['CreateDate']   = $fdetails['CreateDate'];
				}
		}
	
		if($Q->req['newsletter']==1){
	    $intNewsLetterId = $Q->req['id'];	
		//
		// Load the Newsletter with ID
		//
		$this->obj = new core_admin();
		$this->obj->getNewsletter($intNewsLetterId);
		$this->obj->GetNextRecord();
		$error['strTitle']   = $this->obj->Get('strTitle');
		$error['strContent'] = $this->obj->Get('strContent');
		$error['intNewsId']  = $this->obj->Get('intNewsId');
			
			
        $OUT->setVar('content', $this->_temp('show_usedthisnewsletter.php', $mode,$c,$error,$data));
        $OUT->flush();
			
		 //return $this->_temp("show_usedthisnewsletter.php", $mode,$c,$error,$data);
		}
   
	}
  //
  // Do edit Collection
  //
  function do_edit_collection(){
  	global $CONF, $Q, $OUT, $DB,$LANG;
  	
  	$intId = $Q->req['id'];
  	$strCollectionName = $Q->req['strCollection'];
  	$strStatus= $Q->req['strStatus'];
  	
  	if($strCollectionName==""){
  		//$OUT->addError($LANG->q('err_collection'));
  		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_collection&errMsg=1&type=1&id='.$intId);
  		
  		exit;	
  		}
  	
  	//
  	// Check Collection Name
  	//
  	$this->objCheck = new core_admin();
  	$this->objCheck->doGetCollectionAll();
  	$this->objCheck->GetNextRecord();
  	$intDB = $this->objCheck->Get('intCollectionId');
  	$strCollectionNameDB = $this->objCheck->Get('strCollectionName');
  	
  	if(trim(strtolower($strCollectionName))==trim(strtolower($strCollectionNameDB)) &&  $intDB!=$intId){
  		$OUT->redirect($CONF['url_app'].'?m=admin&c=show_collection&errMsg=2&type=1&id='.$intId);
  		
  		exit;	
  		
  		}
  	$strAdded = $Q->cookies['admin:user_id'];
  	$this->objCheckEdit = new core_admin();
  	$this->objCheckEdit->doGetCollectionEdit($intId,$strCollectionName,$strStatus,$strAdded);
  	
  	$OUT->redirect($CONF['url_app'].'?m=admin&c=show_collection&msg=2&type=1&id='.$intId);
  	
  }
  
 	
	//	
	// Show Error and redirect
	//
	function show_error($err_code){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		if($err_code == 'err_auth'){
			$OUT->redirect($CONF['url_app']. "?m=admin&c=show_login");
			exit();
		}
		
		if($err_code == 'err_login'){
			$OUT->setVar('content', $this->page_login());
			$OUT->flush();
			
		}
		if($err_code=='err_add_accountmanager'){
			$OUT->setVar('content', $this->page_account_managers_form());
			$OUT->flush();
		}
		
		if($err_code=='err_add_product'){
			$OUT->setVar('content', $this->page_product_form());
			$OUT->flush();
		}
		if($err_code=='err_add_package'){
			$OUT->setVar('content', $this->page_package_form());
			$OUT->flush();
		}
		 
		if($err_code=='err_upload_phone'){
			$OUT->setVar('content', $this->page_phone_bulk_form());
			$OUT->flush();
		}	
		
		if($err_code == 'err_trace'){
			$OUT->setVar('content', $this->page_main());
		}
		
		
		if($err_code=='err_add_collection'){
			$OUT->setVar('content', $this->page_collection_pge());
			$OUT->flush();
			}
			
		if($err_code=='err_add_attribute'){
				$OUT->setVar('content', $this->page_attributepge());
			$OUT->flush();
			
			
			}
		if($err_code=='err_add_category'){
			$OUT->setVar('content', $this->page_category());
			$OUT->flush();
			}
			
			if($err_code=='err_add_tag_category'){
			$OUT->setVar('content', $this->page_category_tagging());
			$OUT->flush();
			}
			
		if($err_code=='err_add_subcategory'){
			$OUT->setVar('content', $this->page_subcategory());
					$OUT->flush();
			}
		if($err_code=='err_edit_category'){
			$OUT->setVar('content', $this->page_category());
					$OUT->flush();
			}
		if($err_code=='err_edit_subcategory'){
		$OUT->redirect($CONF['url_app']. "?m=admin&c=show_edit_subcategory&intsubCategory=".$intSubCategoryId."&intCategoryId=".$intCategoryId."&msgerror=".$found."");
			exit();
			}
	}
	//
	// Template
	//
	/*function _temp($str,$mode,$c,$error,$data){
		global $CONF, $Q, $OUT, $DB,$LANG;
		// To get the access
		
		//$role = $this->rbac->getUserRole($Q->cookies['admin:user_id']);
		//$user_group = $role[0];
		//$username  = $this->rbac->getUser($Q->cookies['admin:user_id']);
		include (ROOT_PATH . '/src/function/gzip_start.php');
		$include_file = array('main'	=> DIR_TEMP_PATH_BO . '/admin/'.$str);
		include (ROOT_PATH. 'tpl/bo/tmp.php');
		include (ROOT_PATH . '/src/function/gzip_end.php');
	}
	*/
	
	
	
  function _output($includefile,$data,$error) {
    ob_start();
    include ($includefile);
    $contents = ob_get_contents();
		ob_end_clean();
    
    
    return $contents;
  }

  function _temp($str, $mode,$c,$error,$data) {
    global $CONF, $Q, $OUT, $DB, $LANG;
    // To get the access

    ob_start();
   
    $include_file = array('main' => DIR_TEMP_PATH_BO . '/admin/' . $str);
    include (ROOT_PATH . 'tpl/bo/tmp.php');
    $contents = ob_get_contents();
		ob_end_clean();
     
   
      return $contents;
   
  }
	
	
	
	
	
}
?>