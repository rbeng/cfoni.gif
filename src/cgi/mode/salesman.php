<?php
require_once ROOT_PATH . "src/core/core_salesman.php";
require_once ROOT_PATH . "/tcpdf/config/lang/eng.php";
require_once ROOT_PATH . "/tcpdf/tcpdf.php";

class salesman {

  function __construct() {
    global $CONF, $Q, $OUT, $DB, $LANG, $MailClass, $MailHintClass, $MailClassSecond, $MailClassThird;

    // Language directoty	
    //$this->AccID = $Q->cookies['account:intAccId'];
    $LANG->load('admin');
	
  
    if (!array_key_exists('c', $Q->req)) {
      $Q->req['c'] = 'show_main';
    }
    //
    // Authentication	 //
		{
      //
      // Command that don't need authentication
      //
		$no_auth = array(
          'show_login'    => '',
		  'do_login'      => ''
		
      );

      if (!array_key_exists($Q->req['c'], $no_auth)) {
        if (!$this->doAuth()) {


          $param = array();
          foreach (array_keys($Q->req) as $key) {
            if (!preg_match('/salesman:/i', $key)) {
              array_push($param, $key . '=' . $Q->req[$key]);
            }
          }
          $good_param = join("&", $param);
          //$intCheckout = $Q->req['intCheckout'];
          $return = $CONF['url_app'] . '?' . $good_param;
          $OUT->addCookie('salesman:Return', $return);
          $this->show_error('err_auth');
        }
      }
    }

  
  }

  public function _run() {
    global $CONF, $Q, $OUT, $LANG;
    $cmd = $Q->req['c'];
    $valid_cmd = array(
        'show_main'     => null,
		'show_login'    => null,
		'do_login'      => null,
		'do_logout'     => null,
		
		'show_salesman' => null,
		'show_salesman_form' => null,
		'do_add_salesman'    => null,
		'show_profile'       => null,
		'do_update_manager'  => null,
		'show_salesman_details'   => null,
		'do_update_salesman'      => null,
		'do_delete_salesmen'      => null,
		
		
		
		
		
		
		'show_customer'        => null,
		'show_reseller'        => null,
		'show_customer_reseller' => null,
		'shw_reseller_form'    => null,
		'show_member_id'       => null,
		'do_add_reseller'      => null,
		'do_change_status_reseller'  => null,
		'do_point_reseller'      => null
		
    );
    //
    // Setting default cmd if not found
    //
	 if (!array_key_exists($cmd, $valid_cmd)) {
      $cmd = 'show_login';
    }
    //
    // Executing the command
    //
		$this->$cmd();
  }

    //
	// Show main
	//
	function show_main(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_main());
		$OUT->flush();
	}
   
    //
	// Page main
	//
	function page_main(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		
		return $this->_temp('shw_main.php','NULL','NULL',$error,$data);	
	}
	//
	// Show Login Page
	//
	function show_login(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_login());
		$OUT->flush();
	}
	//
	// Page Login 
	//
	function page_login(){
		global $CONF, $Q, $OUT, $DB,$LANG;
				
		$error = $OUT->errors;
  	    return $this->_output(DIR_TEMP_PATH_SALESMAN . '/salesman/login.php',$data,$error);
  		
	}	
	//
	// Do Login 
	//
	function do_login(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;
			
		
		if($Q->req['mustkeyin']!=""){
			data_load($CONF['dir_data']."blocklist.txt", $data);
			data_load($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			$cData = count($data);
			if($cData > 0){
				$blockid = $cData + 1;
			}
			else{
				$blockid = 1;
			}
			$data[$blockid] = $_SERVER['REMOTE_ADDR']." ".date('Y-m-d H:s');
			$dataip[$blockid] = $_SERVER['REMOTE_ADDR'];
			data_save($CONF['dir_data']."blocklist.txt", $data);
			data_save($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			//
			// Inform Server PPl some hacking is being try , please block this IP ASAP
			//
			$strTitle   = 'Caution [ Salesman Page ]: Hacking Back End System';
			$strContent  = 'This IP ('.$_SERVER['REMOTE_ADDR'].') address  try to hack the Back End System , please take the action to block this IP ASAP.';
			include('email_tmpl/block.php');
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $CONF['administration_email'] );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;
 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){

				echo $MailClass ->ErrorInfo;
				}
			} 	
			exit;
		 }	
    	
	$user_id  = $Q->req['user_id'];
	$user_pwd = md5($Q->req['user_pwd']);
		
    $this->objMember = new core_salesman();
    $this->objMember->getLoginMember($user_id);
	$this->objMember->GetNextRecord();
	$data['strPassword']         = $this->objMember->Get('strPass');
	$data['strUser']             = $this->objMember->Get('strUser');
	$data['intSalesId']          = $this->objMember->Get('intSalesId');
	$data['intType']             = $this->objMember->Get('intType');
	$data['strSalesmanCode']     = $this->objMember->Get('strSalesmanCode');
 
	if(!isset($data) || $user_pwd != $data['strPassword']){
		$OUT->addError('Access Denied!'); 
		$this->show_error('err_login');
		exit;
	}
	else{
	  $session = session_get();
    
	  $OUT->addCookie('salesman:Session', $session);
	  $OUT->addCookie('salesman:strPass', $data['strPassword']);
	  $OUT->addCookie('salesman:strUser', $data['strUser']);
	  $OUT->addCookie('salesman:intSalesId', $data['intSalesId']);
	  $OUT->addCookie('salesman:intType', $data['intType']);
	  $OUT->addCookie('salesman:strSalesmanCode', $data['strSalesmanCode']);
	  
	  

      $this->objSession = new core_salesman();
	  $this->objSession->deleteRecord($data['strUser']);
	  $this->objSession = new core_salesman();
	  $this->objSession->addRecord($data['strUser'],$session);
	  
	  //
	  //To get last login time
	  //
	  $intSalesId = $data['intSalesId'];
	  $this->obj = new core_salesman();
	  $this->obj->getLastlogin($intSalesId);
	  $this->obj->GetNextRecord();
	  $strNowLogin  = $this->obj->Get('strNowLogin');
	  $strLastLogin = $this->obj->Get('strLastlogin');
	  
	  if($strNowLogin=="" && $strLastLogin==""){
		 //
		 // Update Both First Time
		 //
		 $this->objUpdateBoth = new core_salesman();
		 $this->objUpdateBoth->doUpdateBothLogin($intSalesId); 
	  } 
	  else{
	  //
	  // Update Now login
	  //
	 
	  $this->objlastlogin = new core_salesman();
	  $this->objlastlogin->doUpdateLastlogin($intSalesId,$strNowLogin); 
		  
	  }
	  
	  $OUT->redirect($CONF['url_app'] . "?m=salesman&c=show_main");
	 }
	}
	
	//
  	// Do Logout
  	//
	function do_logout() {
    	global $CONF, $Q, $OUT, $DB, $DBSH;
	
	    $strUser = $Q->cookies['salesman:strUser'];
    	$this->objSession = new core_salesman();
    	$this->objSession->deleteRecord($strUser);
    	$OUT->addCookie('salesman:strPass', '', time() - 3600);
    	$OUT->addCookie('salesman:strUser', '', time() - 3600);
    	
    	$OUT->addCookie('salesman:intSalesId', '', time() - 3600);
		$OUT->addCookie('salesman:intType', '', time() - 3600);
		$OUT->addCookie('salesman:strSalesmanCode', '', time() - 3600);
        
        $OUT->redirect($CONF['url_app'] . "?m=salesman");
  	}
	
	
  	//
  	// Do authentication
  	//
	function doAuth() {
    	global $CONF, $Q, $OUT, $DB, $LANG;

    	if (array_key_exists('salesman:Session', $Q->cookies)) {
      	
			$Session = $Q->cookies['salesman:Session'];
      		$strUser = $Q->cookies['salesman:strUser'];
		    $data = array();
     	    // find user's session
      		$this->objSession = new core_salesman();
      		$found = $this->objSession->retrieveSession('salesman_session', $strUser, $Session);
      		if (is_array($found)) {
        		return true;
      		} else {
        		return false;
      		}
    	}
  	}
	//
	// Show All Customer By Salesman Id
	//
	function show_customer(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_customer());
		$OUT->flush();
	}
	//
	// Page Customer
	//
	function page_customer(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;
		
		$intSalesId = $Q->cookies['salesman:intSalesId'];
		$intSalesCode = $Q->cookies['salesman:strSalesmanCode'];
		
	
		//
		// Get All Salesman
		//
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objCustomer = new core_salesman();
		$this->objCustomer->numperpage   = 30;
		$this->objCustomer->numfirstpage = 30;
		$this->objCustomer->doGetAllCustomerBySalesmanId($intPage,$intSalesCode);
	
		while($this->objCustomer->GetNextRecord()){
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intCustomerId']   = $this->objCustomer->Get('intCustomerId'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intAccCate']      = $this->objCustomer->Get('intAccCate');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strMobile']       = $this->objCustomer->Get('strMobile');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strEmail']        = $this->objCustomer->Get('strEmail');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['CreateDate']      = $this->objCustomer->Get('CreateDate'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intStatus']       = $this->objCustomer->Get('intStatus'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strCompanyName']   = $this->objCustomer->Get('strCompanyName');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strUser']       = $this->objCustomer->Get('strUser');

		}
			
		
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objCustomer->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		return $this->_temp('shw_customer_lists.php','NULL','NULL',$error,$data);	
	}
	
	
	
	
	
	//
	// Show All Customer By Salesman Id
	//
	function show_customer_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_customer_reseller());
		$OUT->flush();
	}
	//
	// Page Reseller Customer
	//
	function page_customer_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE;
		
		$intSalesId = $Q->cookies['salesman:intSalesId'];
		$intSalesCode = $Q->cookies['salesman:strSalesmanCode'];
		
	
		//
		// Get All Salesman
		//
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objCustomer = new core_salesman();
		$this->objCustomer->numperpage   = 30;
		$this->objCustomer->numfirstpage = 30;
		$this->objCustomer->doGetAllCustomerBySalesmanId($intPage,$intSalesCode);
	
		while($this->objCustomer->GetNextRecord()){
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intCustomerId']   = $this->objCustomer->Get('intCustomerId'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intAccCate']      = $this->objCustomer->Get('intAccCate');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strMobile']       = $this->objCustomer->Get('strMobile');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strEmail']        = $this->objCustomer->Get('strEmail');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['CreateDate']      = $this->objCustomer->Get('CreateDate'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intStatus']       = $this->objCustomer->Get('intStatus'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strCompanyName']  = $this->objCustomer->Get('strCompanyName');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strUser']         = $this->objCustomer->Get('strUser');
            $data['customer'][$this->objCustomer->Get('intCustomerId')]['strIntroduceResellerCode']         = $this->objCustomer->Get('strIntroduceResellerCode');
		}
		
		//
		// Get All Reseller
		//
		$this->objReseller = new core_salesman();
		$this->objReseller->doGetAllActiveResellerlists($intSalesCode);
		while($this->objReseller->GetNextRecord()){
			
			$data['reseller'][$this->objReseller->Get('strResellerCode')] = $this->objReseller->Get('strResellerCode')." &nbsp; / &nbsp; ".$this->objReseller->Get('strUser');
		}
		
		
		
			
		
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objCustomer->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		return $this->_temp('shw_reseller_customer_lists.php','NULL','NULL',$error,$data);	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//
	// Show All Reseller By Salesman Id
	//
	function show_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_reseller());
		$OUT->flush();
	}
	//
	// Page Reseller
	//
	function page_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$intSalesId = $Q->cookies['salesman:intSalesId'];
		$intSalesCode = $Q->cookies['salesman:strSalesmanCode'];
		
	
		//
		// Get All Salesman
		//
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objReseller = new core_salesman();
		$this->objReseller->numperpage   = 30;
		$this->objReseller->numfirstpage = 30;
		$this->objReseller->doGetAllResellerBySalesmanId($intPage,$intSalesCode);
	
		while($this->objReseller->GetNextRecord()){
			$data['reseller'][$this->objReseller->Get('intResellerId')]['intResellerId']   = $this->objReseller->Get('intResellerId'); 
			$data['reseller'][$this->objReseller->Get('intResellerId')]['strResellerCode']      = $this->objReseller->Get('strResellerCode');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['intCommisionRate']       = $this->objReseller->Get('intCommisionRate');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['intCustomerId']        = $this->objReseller->Get('intCustomerId');
			$data['reseller'][$this->objReseller->Get('intResellerId')]['CreateDate']      = $this->objReseller->Get('CreateDate'); 
		    $data['reseller'][$this->objReseller->Get('intResellerId')]['intStatus']       = $this->objReseller->Get('intStatus');
		    $data['reseller'][$this->objReseller->Get('intResellerId')]['strEmail']       = $this->objReseller->Get('strEmail');
		    $data['reseller'][$this->objReseller->Get('intResellerId')]['strContact']       = $this->objReseller->Get('strContact');
			
		   $data['reseller'][$this->objReseller->Get('intResellerId')]['intResellerType']       = $this->objReseller->Get('intResellerType');
			
			
			
			
		}
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->objReseller->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|'); 
		//
		// Get Customer Details
		//	
		$this->objCustomer = new core_salesman();
		$this->objCustomer->getAllCustomerDetailsBySalesmanCode($intSalesCode);
		while($this->objCustomer->GetNextRecord()){
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intCustomerId']   = $this->objCustomer->Get('intCustomerId'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intAccCate']      = $this->objCustomer->Get('intAccCate');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strMobile']       = $this->objCustomer->Get('strMobile');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strEmail']        = $this->objCustomer->Get('strEmail');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['CreateDate']      = $this->objCustomer->Get('CreateDate'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['intStatus']       = $this->objCustomer->Get('intStatus'); 
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strCompanyName']   = $this->objCustomer->Get('strCompanyName');
			$data['customer'][$this->objCustomer->Get('intCustomerId')]['strUser']       = $this->objCustomer->Get('strUser');

		}
		return $this->_temp('shw_reseller_lists.php','NULL','NULL',$error,$data);	
		
	}
	
	//
	// Show Reseller Form
	//
	function shw_reseller_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_reseller_form());
		$OUT->flush();
		
	}
	//
	// Page Reseller Form
	//
	function page_reseller_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		return $this->_temp('shw_reseller_form.php','NULL','NULL',$error,$data);	
	}
	
	
	//
	// Show Own Customer Id
	//
	function show_member_id(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_customer_form());
		$OUT->flush();
		
	}
	//
	// Page Customer Id Form
	//
	function page_customer_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		//
		// Get Own Customer to Select
		//
		$strSalesmanCode = $Q->cookies['salesman:strSalesmanCode'];
		
		//
		// Get All Salesman
		//
		 if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->obj = new core_salesman();
		$this->obj->numperpage   = 30;
		$this->obj->numfirstpage = 30;
		
		
		
		
		$this->obj->getAllCustomerBySalesman($strSalesmanCode,$intPage);
		while($this->obj->GetNextRecord()){
			
			$data['customer'][$this->obj->Get('intCustomerId')]['intCustomerId'] = $this->obj->Get('intCustomerId');
			$data['customer'][$this->obj->Get('intCustomerId')]['strName'] = $this->obj->Get('strFirstName')."".$this->obj->Get('strLastName');
			$data['customer'][$this->obj->Get('intCustomerId')]['strEmail'] = $this->obj->Get('strEmail');
			$data['customer'][$this->obj->Get('intCustomerId')]['strCompanyName'] = $this->obj->Get('strCompanyName');
			
			$data['customer'][$this->obj->Get('intCustomerId')]['strContact'] = $this->obj->Get('strMobile');
			$data['customer'][$this->obj->Get('intCustomerId')]['CreateDate'] = $this->obj->Get('CreateDate');
		}
		
		
		$this->pages = new Paginator();        
		$this->pages->items_total = $this->obj->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');  
		
		
		
		return $this->_output(DIR_TEMP_PATH_SALESMAN . '/salesman/show_customerid.php',$data,$error);
		
	}
	
	//
	// Do Add Reseller
	//
	function do_add_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG,$MailClass;
		
		//
		// Reseller 1= New , 2= Existing Customer
		//
		
		$intResellerType  = $Q->req['intResellerType'];
		$intCustomerId    = $Q->req['parent_id'];
		$strEmail         = $Q->req['parent_email'];
		$strContact       = $Q->req['parent_contact'];
		$strCommisionRate = $Q->req['strCommisionRate'];
		$strSalesmanCode  = $Q->cookies['salesman:strSalesmanCode'];
		$intAutoEmail     = $Q->req['intAutoEmail'];
		
		$strUsername      = $Q->req['parent_username'];
		$strPassword      = $Q->req['parent_password'];
		
		
		//
		// Check Username
		//
		if($strUsername!=""){
			$this->objCheckUsername = new core_salesman();
			$this->objCheckUsername->doCheckUsernameReseller($strUsername);
			$this->objCheckUsername->GetNextRecord();
			$strDBUsername = $this->objCheckUsername->Get('strUser'); 
			if($strDBUsername==$strUsername){
				 $OUT->addError('Username already exists.');
			}
			
			
		}
		
		if($strEmail==""){
			 $OUT->addError('Email Address cannot be empty.');
		}
		else{if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
		 }
		}
		
		if($intResellerType==1){
			if($strContact==""){
				 $OUT->addError('Contact No. cannot be empty.');
			}
			if($strUsername==""){
				 $OUT->addError('Username cannot be empty.');
			}
			if($strPassword==""){
				 $OUT->addError('Password cannot be empty.');
			}
		}
		if($strCommisionRate==""){
			$strCommisionRate = 0;
		}
		
		if($intResellerType ==2){
		  if($intCustomerId==""){
			 $OUT->addError('Customer Id. cannot be empty.');
			
		  }
		}
		if($intAutoEmail==1){
			if($strEmail==""){
				$OUT->addError('Customer Email Address. cannot be empty.');
				
			}
			
		}
		
	   if($OUT->isError()){
			$this->show_error('err_add_reseller');
			exit;
		}
		
		// Point Reseller
		if($intResellerType==2){
			//
			// Get the Username & Password
			//
			$this->objcustomerdetails = new core_salesman();
			$this->objcustomerdetails->doGetDetailsCustomer($intCustomerId);
			$this->objcustomerdetails->GetNextRecord();
			$strUsername = $this->objcustomerdetails->Get('strUser'); 
			$strPassword = $this->objcustomerdetails->Get('strPass'); 
			
		//
		// Point Reseller Record
		//
		$this->objreseller = new core_salesman();
		$id = $this->objreseller->doAddNewReseller($intResellerType,$intCustomerId,$strCommisionRate,$strSalesmanCode,$strUsername,$strPassword,$strEmail,$strContact );
			
			if($id > 0){
			//
			// Update Reseller Code
			//
			$strResellerCode = getResellerCode($id);
			$this->objResellerCode = new core_salesman();
			$resUpdate = $this->objResellerCode->doUpdateResellerCode($strResellerCode,$id,$strSalesmanCode);
		    //
			// Update Customer Record to Point As Reseller
			//
			if($resUpdate==1){
			$this->objCustomer = new core_salesman();
			$this->objCustomer->doUpdateCustomer($intCustomerId);
			//
			// Email to Customer inform as Reseller
			//
			if($intAutoEmail==1){
			//
			// Inform New Salesman
			//
			$strTitle   = 'Welcome : New Reseller';
			$strContent  = 'Your are  welcome to join our reseller team.  Reseller Code :'.$strResellerCode;
			$strFullName = $strUsername;
			include('email_tmpl/welcome_accountmanager.php');
			
			
			
			
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $strEmail  );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			 }
			}
			$OUT->redirect($CONF['url_app'].'?m=salesman&c=shw_reseller_form&msg=1');
			
			}
		}
		else{
		 
		  echo "Error : Insert Reseller Record";	
			
		 }	
			
	    }
		//
		// Adding New Reseller
		//
		if($intResellerType==1){
			
		//
		// Add Reseller Record
		//
		$this->objreseller = new core_salesman();
		$id = $this->objreseller->doAddNewResellerNew($intResellerType,$strCommisionRate,$strSalesmanCode,$strUsername,$strPassword,$strEmail,$strContact );
		if($id > 0){
			//
			// Update Reseller Code
			//
			$strResellerCode = getResellerCode($id);
			$this->objResellerCode = new core_salesman();
			$resUpdate = $this->objResellerCode->doUpdateResellerCode($strResellerCode,$id,$strSalesmanCode);
			
			
			
			//
			// Email to Customer inform as Reseller
			//
			if($intAutoEmail==1){
			//
			// Inform New Salesman
			//
			$strTitle   = 'Welcome : New Reseller';
			$strContent  = 'Your account details as below <br> Reseller Code :'.$strResellerCode.' <br> Username :'.$strUsername.' <br> Password :'.$strPassword.'<br> URL : <a href='.$CONF['url_app'].'?m=reseller>'.$CONF['url_app'].'?m=reseller'.'<a>';
			$strFullName = $strUsername;
			include('email_tmpl/welcome_accountmanager.php');
			
			
		
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $strEmail  );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			 }
			}
			$OUT->redirect($CONF['url_app'].'?m=salesman&c=shw_reseller_form&msg=1');
			
			
			
			}
			
		}
		
		
		exit;
	}
	//
	// Do Changer Reseller Status
	//
	function do_change_status_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		 $id         =  $Q->req['id'];
		 $intStatus  =  $Q->req['intStatus'];
		 //$strAddedBy   = $Q->cookies['admin:user_id'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_salesman();
		 $res =  $this->obj->doResellerStatusById($id,$intStatus);
		 
	
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=salesman&c=show_reseller&msg=3'); 
		 }
		exit;	
		
		
	}
	//
	// Do point Reseller Path
	//
	function do_point_reseller(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strResellerCode = $Q->req['strResellerCode'];
		$intCustomerId   = $Q->req['intCustomerId'];
		$strSalesmanCode  = $Q->cookies['salesman:strSalesmanCode'];
		
		//
		//Do Update Reseller's customer
		//
		$this->objdoUpdate = new core_salesman();
		$res = $this->objdoUpdate->doUpdateResellertoCustomer($strResellerCode,$intCustomerId,$strSalesmanCode);
		
		if($res==1){
			
			$OUT->redirect($CONF['url_app'].'?m=salesman&c=show_customer_reseller&msg=3');
		}
	}
	
	
	
	
	
	
	
	//
	// Show Salesman Form
	//	
	function show_salesman_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_salesman_form());
		$OUT->flush();
		
	}
	//
	// Page Salesman Form
	//
	function page_salesman_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		return $this->_temp('shw_salesmen_form.php','NULL','NULL',$error,$data);	
		
	} 
	//
	// Do Add Salesman
	//
	function do_add_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG,$CODE,$MailClass;
		
		
		$strFullName  = $Q->req['strFullName'];
		$strEmail     = $Q->req['strEmail'];
		$strContact   = $Q->req['strContact'];
		$strUsername  = $Q->req['strUsername'];
		$strPassword  = $Q->req['strPassword'];
		$intAutoEmail = $Q->req['intAutoEmail'];
		$strCommisionRate  = $Q->req['strCommisionRate'];
		$intType      = $Q->req['intType'];
		$intAccountManagerId   = $Q->cookies['manager:intAccountManagerId'];
		if($intAutoEmail==""){
			$intAutoEmail = 0;
		}
		$strType  =  $CODE['prefix_salesman'][$intType];
		
		
		//
		// Username Exists Or Not
		//
		if($strUsername!=""){
		$this->objcheck = new core_salesman();
		$this->objcheck->doCheckUsername($strUsername);
		$this->objcheck->GetNextRecord();
		$strDBUsername = $this->objcheck->Get('strUser');
		 if($strDBUsername==$strUsername){
			$OUT->addError('Username already exists.');	
		  }
		}
		else{
			$OUT->addError('Username cannot be empty.');	
		}
		
		if($strEmail==""){
			$OUT->addError('Email Address cannot be empty.');	
		}
		else{
			
			if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
			}
			$this->objcheckemail = new core_salesman();
			$this->objcheckemail->doCheckEmail($strEmail);
			$this->objcheckemail->GetNextRecord();
			$strDBEmail = $this->objcheckemail->Get('strEmail');
		   if($strDBEmail==$strEmail){
			 $OUT->addError('Email Address already exists.');	
		   }
	    }
		if($strPassword==""){
			$OUT->addError('Passwrod cannot be empty.');	
		}
		if($strContact==""){
			$OUT->addError('Contact No. cannot be empty.');	
		}
	
	   if($OUT->isError()){
			$this->show_error('err_add_salesmam');
			exit;
		}
		//
		// Do Insert Salesman
		//
		$this->obj = new core_salesman();
		$id = $this->obj->doAddSalesman(array(
		 'strFullName' => $strFullName,
		 'strEmail'    => $strEmail,
		 'strContact'  => $strContact,
		 'strUsername' => $strUsername,
		 'strPassword' => $strPassword,
		 'intType'     => $intType ,
		 'strCommisionRate' => $strCommisionRate ,
		 'intAccountManagerId' => $intAccountManagerId
		 )
		);
		
		
			if($id > 0 ){
			$strCode = getSalesmanCode($id,$strType);
			$strCombineCode = $strType.$strCode;
			//
			// Update Salesman Code
			//
			$this->objsalesman = new core_salesman();
			$this->objsalesman->doUpdateSalesman($strCode,$id);
	
			if($intAutoEmail==1){
			//
			// Inform New Salesman
			//
			$strTitle   = 'Welcome : New Salesman';
			$strContent  = 'Your account details as below <br> Salesman Code :'.$strCombineCode.' <br> Username :'.$strUsername.' <br> Password :'.$strPassword.'<br> URL : <a href='.$CONF['url_app'].'?m=salesman>'.$CONF['url_app'].'?m=salesman'.'<a>';
			
			include('email_tmpl/welcome_accountmanager.php');
			
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $strEmail  );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			 }
			}
			$OUT->redirect($CONF['url_app'].'?m=manager&c=show_salesman&msg=1');
		}
		else{
			echo "Error : Insert New Salesman Details...";	
			exit;	
		}
		
	}
	//
	// Show Profile
	//
	function show_profile(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_profile_form());
		$OUT->flush();			
	}
	//
	// Page Profile
	//
	function page_profile_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$error = $OUT->errors;
		$id    = $Q->cookies['salesman:intSalesId'];
		//
		// Get the Information
		//
		$this->obj = new core_salesman();
		$this->obj->doGetProfile($id);
		$this->obj->GetNextRecord();
		$data['strFullName']   = $this->obj->Get('strName');
		$data['strIC']         = $this->obj->Get('strIC');
		$data['strEmail']      = $this->obj->Get('strEmail');
		$data['strContact']    = $this->obj->Get('strContact');
		$data['strAdd']        = $this->obj->Get('strAdd');
		$data['strCity']       = $this->obj->Get('strCity');
		$data['strState']      = $this->obj->Get('strState');
		$data['intCountry']    = $this->obj->Get('intCountry');
		$data['strZip']        = $this->obj->Get('strZip');
		$data['strLastLogin']  = $this->obj->Get('strLastlogin');
		
		
		return $this->_temp('shw_profile_form.php','NULL','NULL',$error,$data);	
		
	}
	
	
	//
	// Do Show Salesman Details
	//
	function show_salesman_details(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$OUT->setVar('content',	$this->page_salesman_details());
		$OUT->flush();		
	}
	//
	// Page show salesman details
	//
	function page_salesman_details(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$id               = $Q->req['id'];
		$intAccManagerId  = $Q->cookies['manager:intAccountManagerId'];
		//
		// Do Get Information From Account Manager 
		//
		$this->objAccManager = new core_salesman();
		$this->objAccManager->doGetSalesmanDetailsById($id,$intAccManagerId);
		$this->objAccManager->GetNextRecord();
		$data['strFullName']  = $this->objAccManager->Get('strName');
        $data['strEmail']     = $this->objAccManager->Get('strEmail');
		$data['strContact']   = $this->objAccManager->Get('strContact');
		$data['intSalesId']   = $id;
		$data['intStatus']    = $this->objAccManager->Get('intStatus');
		$data['strUsername']  = $this->objAccManager->Get('strUser');
		
		return $this->_output(DIR_TEMP_PATH_MANAGER . '/manager/shw_salesman_editForm.php',$data,$error);	
		exit;
		
	}
	//
	// Do Edit Salesman Record Personal profile
	//
	function do_update_salesman(){
		global $CONF, $Q, $OUT, $DB,$LANG;	
		
		$strFullName  = $Q->req['strFullName'];
		$strEmail     = $Q->req['strEmail'];
		$strIc        = $Q->req['strIc'];
		$strAdd       = $Q->req['strAdd'];
		$strCity      = $Q->req['strCity'];
		$intCountry   = $Q->req['country'];
		$strZip       = $Q->req['strZip'];
		
		if($intCountry==1){
			$strState = $Q->req['malaysia_city'];
		}
		else{
			
		   $strState = $Q->req['non_malaysia_city'];
		}
		if($strAdd==""){
			$strAdd= "-";
		}
		
	
		if($strCity==""){
			$strCity="-";
		}
		if($strZip==""){
			$strZip = 0;
		}
		if($strIc==""){
			$strIc ="-";
		}
		$strContact   = $Q->req['strContact'];
		$strUsername  = $Q->req['strUsername'];
		$strPass1     = $Q->req['strPass1'];
		$strPass2     = $Q->req['strPass2'];
		
		$intSalesId   = $Q->cookies['salesman:intSalesId'];
			
		
		if($strEmail==""){
			$OUT->addError('Email Address cannot be empty.');	
		}
		else{
			
			if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
			}
			$this->objcheckemail = new core_salesman();
			$this->objcheckemail->doCheckEmail($strEmail);
			$this->objcheckemail->GetNextRecord();
			$strDBEmail = $this->objcheckemail->Get('strEmail');
			$intSalesIdDB = $this->objcheckemail->Get('intSalesId');
		   if($strDBEmail==$strEmail && $intSalesIdDB!=$intSalesId ){
			 $OUT->addError('Email Address already exists.');	
		   }
	    }
		if($strPass1!=$strPass2){
			$OUT->addError('Passwrod does\'t match.');	
		}
		if($strContact==""){
			$OUT->addError('Contact No. cannot be empty.');	
		}
	
	   if($OUT->isError()){
			$this->show_error('err_update_salesmam');
			exit;
		}
		//
		// Update Profile
		//
		$this->objupdatesales = new core_salesman();
		$res = $this->objupdatesales->doUpdateProfileSalesman(array(
			'strFullName' => $strFullName,
			'strEmail'    => $strEmail,
			'strIc'       => $strIc,
			'strAdd'     => $strAdd,
			'strCity'     => $strCity ,
			'intCountry'  => $intCountry ,
			'strState'    => $strState,
			'strZip'      => $strZip,
			'strContact'  => $strContact,
			'strPass'     => $strPass1,
			'intSalesId'  => $intSalesId 
		));
		
		if($res==1){
			$OUT->redirect($CONF['url_app'].'?m=salesman&c=show_profile&msg=1');
		}
		else{
		 echo "Error : Update Salesman Profile...";	
		}
		
	}
	//
	// Do Delete Salesman
	//
	function do_delete_salesmen(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		 $id =  decrypt($Q->req['id'],'cfoni.8888');
		 $strAddedBy   = $Q->cookies['manager:intAccountManagerId'];
		 //
		 // Do Delete
		 //
		 $this->obj = new core_salesman();
		 $res =  $this->obj->doDeleteSalesmanById($id,$strAddedBy);
		 
		 if($res==1){
			 
			$OUT->redirect($CONF['url_app'].'?m=manager&c=show_salesman&msg=2'); 
		 }
		exit;
		
	}
	
	
	
	
	
	
	
	
	//	
	// Show Error and redirect
	//
	function show_error($err_code){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		if($err_code == 'err_auth'){
			$OUT->redirect($CONF['url_app']. "?m=salesman&c=show_login");
			exit();
		}
		if($err_code=='err_add_salesmam'){
			$OUT->setVar('content', $this->page_salesman_form());
			$OUT->flush();
		}
		if($err_code=='err_update_salesmam'){
			$OUT->setVar('content', $this->page_profile_form());
			$OUT->flush();
		}
		
		  
		if($err_code=='err_add_reseller'){
			$OUT->setVar('content', $this->page_reseller_form());
			$OUT->flush();
		}
		
		
		
		if($err_code == 'err_login'){
			$OUT->setVar('content', $this->page_login());
			$OUT->flush();
			
		}
	}
  
 	//
	// Output File
	//

   function _output($includefile,$data,$error) {
    ob_start();
    include ($includefile);
    $contents = ob_get_contents();
	ob_end_clean();
    
    return $contents;
  }
	
	//
	// Render Template
	//	
  function _temp($str, $mode,$c,$error,$data) {
    global $CONF, $Q, $OUT, $DB, $LANG;
    // To get the access
    ob_start();
  
    $include_file = array('main' => DIR_TEMP_PATH_SALESMAN . '/salesman/' . $str);
    include (ROOT_PATH . 'tpl/salesman/tmp.php');
    $contents = ob_get_contents();
	ob_end_clean();
     return $contents;
   
  }
  
  

}
?>