﻿<?php
require_once ROOT_PATH . "src/core/core_account.php";
//require_once ROOT_PATH . "src/core/core_ads.php";
require_once ROOT_PATH . "src/core/core_main.php";

class account {

	// START function __construct()
	function __construct(){
		global $CONF,$Q,$OUT,$DB,$LANG,$MailClass,$MailHintClass;
		
		$this->AccID = $Q->cookies['account:intCustomerId'];
		
		// Language directoty
		$LANG->load('account');
		
		//
		// Setting default command
		//
		if(! array_key_exists('c', $Q->req)){
			$Q->req['c'] = 'shw_all_modules';
		}
		
		//
		// Authentication	
		//
		{	
		
		//
		// Command that don't need authentication
		//
		$no_auth = array(
		//'show_main_account'					=> '',
		'do_change_email'             =>'',
		'show_msg_changeEmail'        =>'',
		'show_my_listings'            => '',
			);
			
		if(! array_key_exists($Q->req['c'], $no_auth)){
			if(!$this->doAuth()){
				$param = array();
				foreach (array_keys($Q->req) as $key){
					if(!preg_match('/account:/i',$key)){
						array_push($param, $key.'='.$Q->req[$key]);
					}
				}
				$good_param = join("&",$param);
				$return = $CONF['url_app'].'?'.$good_param;
				$OUT->addCookie('account:Return', $return);
				$this->show_error('err_auth');
			}
		}
		}
		
	}
	// END function __construct()
	
	// START public function _run()
	public function _run(){
		global $CONF,$Q,$OUT,$LANG;
		$cmd = $Q->req['c'];
		$valid_cmd	=	array(
      		//
      		// Account Module
      		//
      		'do_updateacc'             => null,
			'show_time'      			=> null,
      		
			// Ads Listing
			'show_main_account'         => null,
      'show_my_listings'          => null,
      'show_my_listings_popup'    => null,
			
			// Edit Ads
      		'show_edit_ads'             => null,
			'do_update_ads'             => null,
			
			// Verify & Instruction
			'show_verify_steps'         => null,
			'show_instruction'          => null,
			
			// Update Account
			'shw_all_modules'           => null,
     		'show_edit_account'         => null,
      		'do_edit_account'           => null,
      		'do_update_account'         => null,
			
			// Edit Password
      		'show_edit_password'        => null, 	
      		'do_update_password'        => null,
      		
      		
      		//
      		// Show Manage Images
      		//
      		'show_manageimage' => null,
      		'do_uploadImage'   => null,
      		'do_delete_image'  => null,
      		
      		'show_edit_email'  => null,
      		'do_send_email_confirm' => null,
      		'do_change_email'     => null,
      		'show_msg_changeEmail' => null,
      		'show_thanks_email'    => null,
      		'do_download_pdf'      => null,
      		'do_sync_photo'        => null,
      		'do_sendRedemCash'     => null,
			);

		//
		// Setting default cmd if not found
		//
		if(!array_key_exists($cmd, $valid_cmd)){
			$cmd = 'shw_all_modules';
		}
		//
		// Executing the command
		//
		$this->$cmd();
	}
	// END public function _run()
	
	

	//
	// Authentication Area
	//
	// START function doAuth()
	function doAuth(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		if(array_key_exists('account:Session', $Q->cookies)){
			$Session  = $Q->cookies['account:Session'];
			$UsrName = $Q->cookies['account:strUser'];
			
			$data = array();
			// find user's session
			$this->objSession = new core_main();
			$found = $this->objSession->retrieveSession('customer_session',$UsrName,$Session);
			
			if(is_array($found)){
				return true;
			}
			else{
	
				return false;
			}
		}
	}
	// END function doAuth()
	   
	
	
	
/******************************************************************* Update Account ******************************************************************/	

  function shw_all_modules(){
  	global $CONF, $Q, $OUT, $DB,$LANG;	
  	$OUT->setVar('content', $this->page_account_form()); 
    $OUT->flush();
  	
  }
	   
	// START function page_account_form()
	function page_account_form(){
		global $CONF, $Q, $OUT, $DB,$LANG;
       
		$error = $OUT->errors;
		$intAccId =  $Q->cookies['account:intCustomerId'];
		
		$arrUserEmail = array();
		// Get Account Table By ID
		$this->objacc = new core_account();  
		$this->objacc->getAccById($intAccId);
		$this->objacc->GetNextRecord();
		$data['account']['intCustomerId'] =  $this->objacc->Get('intCustomerId');
		$data['account']['strFirstName']  =  $this->objacc->Get('strFirstName');
		$data['account']['strLastName']   =  $this->objacc->Get('strLastName');
		$data['account']['strEmail']      =  $this->objacc->Get('strEmail');
		$data['account']['strMobile']     =  $this->objacc->Get('strMobile');
		$data['account']['strEmail']      =  $this->objacc->Get('strEmail');
		$data['account']['strMobile']     =  $this->objacc->Get('strMobile');
		$data['account']['strCompanyAdd']   =  $this->objacc->Get('strCompanyAdd');
		$data['account']['strCompanyName']   = $this->objacc->Get('strCompanyName');
		$data['account']['strCompanyCity']  	    =  $this->objacc->Get('strCompanyCity');
		$data['account']['strCompanyZip']   =  $this->objacc->Get('strCompanyZip');
		$data['account']['intCompanyCountry']    =  $this->objacc->Get('intCompanyCountry');
		$data['account']['strCompanyState']      =  $this->objacc->Get('strCompanyState');
		$data['account']['strCompanyImage']      =  $this->objacc->Get('strCompanyImage');
		
    $data[$this->objacc->Get('intAccId')]['createdate']             = date('d F Y',strtotime($this->objacc->Get('CreateDate')));
 
 
	
	return $this->_temp('show_account_form.php',$error,$data,$param);
	
	

	}
	
	// UPDATE ACCOUNT
	function do_updateacc(){
		global $CONF, $Q, $OUT, $DB,$LANG;


		$strFirstName 		 = $Q->req['strFirstName'];     
		$strLastName		  = $Q->req['strLastName'] ;     
		$strEmail  			  = $Q->req['strEmail']    ;         
		
		
		$strMobile 		      = $Q->req['strMobile']    ;     
		    
		$strCompanyName       = $Q->req['strCompanyName'];
		$strAddress		      = $Q->req['strAddress'] ;     
	      
		$strCity			  = $Q->req['strCompanyCity'] ;       
		$intPostcode		  = $Q->req['strCompanyZip'] ;     
		$intCountry			  = $Q->req['country']  ;     
		
		if($intCountry==1){
			$strState             = $Q->req['malaysia_city'];
		}
		else{
			$strState  			  = $Q->req['non_malaysia_city']    ;
		}
		$strCompanyImage	  = $_FILES['strImage']['name'];      
		$strAttachment1       = $_FILES['strAttachment1']['name'];
		$strAttachment2       = $_FILES['strAttachment2']['name'];
		
		// validation checking
		if($strFirstName ==""){
			$OUT->addError('Please enter your first name.');
		}
			
		if($strLastName ==""){
			$OUT->addError('Please enter your last name.');
		}
	  if($strState ==""){
			$OUT->addError('Please enter your state.');
		}
		if($intPostcode ==""){
			$OUT->addError('Please enter your Postal.');
		}
		if($strCity ==""){
			$OUT->addError('Please enter your city.');
		}
		
		if($strCompanyName ==""){
			$OUT->addError('Please enter your Company Name.');
		}
		
	if($strAddress ==""){
			$OUT->addError('Please enter your address.');
		}
		
	
		if($OUT->isError()){ 
                  
			$this->show_error('err_edit_account');
			exit;
		}
	
		// Update Account
		$this->objacc = new core_account();
		$this->objacc->doUpdateAcc(array(
         				'strFirstName' => $strFirstName	,   
         				'strLastName'	 => $strLastName	,  
         				'strAddress'	 => $strAddress	,  
						'strEmail'	     => $strEmail		, 
						'strMobile'	     => $strMobile		, 
         				'strCity'	     => $strCity		,
						'strCompanyName' => $strCompanyName, 
         				'intPostcode'	 => $intPostcode	,  
         				'intCountry'	 => $intCountry	,		 
         				'strState'  	 => $strState  	,
         				'intAccId'     => $this->AccID	));
		
		$id = $this->AccID;
		//
		// Update Company Logo
		//
			$folder_id    = (int)($this->AccID/ $CONF['const_photo_per_dir'    ]);  
        if($strCompanyImage!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/company/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/company/'.$folder_id.'/',0777);
							}
		  		 if(!file_exists($CONF['dir_photo'  ].'/company/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/company/'.$folder_id.'/'.$id.'/',0777);
							}
			    $upload_path   = $CONF['dir_photo'  ].'/company/'.$folder_id.'/'.$id.'/'; 
   		    	$arrfilename   = $_FILES['strImage']['name'];
   				$arrTemp       = $_FILES['strImage']['tmp_name'];
			    $ext = substr($arrfilename, strpos($arrfilename,'.'), strlen($arrfilename)-1);
			    $explodeImagename = explode('.',$arrfilename);
					if(!in_array($ext,$allowed_filetypes)){
     				  die('The file you attempted to upload is not allowed.');}
	 			   if(filesize($arrTemp) > $max_filesize){
      			     die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			     }
   				  if(move_uploaded_file($arrTemp,$upload_path.$arrfilename))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =1;
        			}
	      			else{
    	     			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             		}
				  }
		 		 //
		 		 // Update Image
		 		 //
				 if($found==1){
		 				$this->objupdate = new core_account();
		 				$this->objupdate->doInsertCompanyImage($id,$arrfilename);
	 				 }
		
	
	
	//
	// Attachment 1
	//
	
	
	$folder_id    = (int)($this->AccID/ $CONF['const_photo_per_dir'    ]);  
        if($strAttachment1!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/att1/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/att1/'.$folder_id.'/',0777);
							}
		  		 if(!file_exists($CONF['dir_photo'  ].'/att1/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/att1/'.$folder_id.'/'.$id.'/',0777);
							}
			    $upload_path   = $CONF['dir_photo'  ].'/att1/'.$folder_id.'/'.$id.'/'; 
   		    	$strAttachment1   = $_FILES['strAttachment1']['name'];
   				$arrTemp       = $_FILES['strAttachment1']['tmp_name'];
			    $ext = substr($strAttachment1, strpos($strAttachment1,'.'), strlen($strAttachment1)-1);
			    $explodeImagename = explode('.',$strAttachment1);
				
	 			   if(filesize($arrTemp) > $max_filesize){
      			     die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			     }
   				  if(move_uploaded_file($arrTemp,$upload_path.$strAttachment1))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =2;
        			}
	      			else{
    	     			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             		}
				  }
		 		 //
		 		 // Update Image
		 		 //
				 if($found==2){
		 				$this->objupdate = new core_account();
		 				$this->objupdate->doInsertCompanyAtt($id,$strAttachment1);
	 				 }
	
	
	
	
	//
	// Att 2
	//
	
	$folder_id    = (int)($this->AccID/ $CONF['const_photo_per_dir'    ]);  
        if($strAttachment2!=""){
			    $allowed_filetypes = array('.jpg','.gif','.bmp','.png','.swf','.jpeg'); // These will be the types of file that will pass the validation.
    			//$max_filesize = 524288; // Maximum filesize in BYTES (currently 0.5MB).
    			$max_filesize = 1048576; // Maximum filesize in BYTES (currently 1.0MB).
   		  		 
   		  		 if(!file_exists($CONF['dir_photo'  ].'/att2/'.$folder_id.'/')){
								mkdir($CONF['dir_photo'].'/att2/'.$folder_id.'/',0777);
							}
		  		 if(!file_exists($CONF['dir_photo'  ].'/att2/'.$folder_id.'/'.$id.'/')){
								mkdir($CONF['dir_photo'].'/att2/'.$folder_id.'/'.$id.'/',0777);
							}
			    $upload_path   = $CONF['dir_photo'  ].'/att2/'.$folder_id.'/'.$id.'/'; 
				
				
				
   		    	$strAttachment2   = $_FILES['strAttachment2']['name'];
   				$arrTemp       = $_FILES['strAttachment2']['tmp_name'];
			    $ext = substr($strAttachment2, strpos($strAttachment2,'.'), strlen($strAttachment2)-1);
			    $explodeImagename = explode('.',$strAttachment2);
				
	 			   if(filesize($arrTemp) > $max_filesize){
      			     die('The file you attempted to upload is too large.');}
			 	  if(!is_writable($upload_path)){
     				die('You cannot upload to the specified directory, please CHMOD it to 777.');
     			     }
   				  if(move_uploaded_file($arrTemp,$upload_path.$strAttachment2))
					   {
        				//echo 'You have successfully uploaded the pictures '. $arrfilename.'<br>'; 
        				$found =3;
        			}
	      			else{
    	     			echo 'There was an error during the file upload.  Please try again.'; // It failed :(.
             		}
				  }
		 		 //
		 		 // Update Image
		 		 //
				 if($found==3){
		 				$this->objupdate = new core_account();
		 				$this->objupdate->doInsertCompanyAtt2($id,$strAttachment2);
	 				 }
	
	
	
	
		
		 $OUT->redirect($CONF['url_app'].'?m=account&c=show_all_modules&msg=done');
                
                
                
	}
	
	function do_update_password(){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		$strOld     = md5($Q->req['strOldPassword']);
		$strNew     = $Q->req['strNewPassword'];
		$strComfirm = $Q->req['strVerifyPassword'];
		
		if($strOld =="" || $strNew =="" || $strComfirm ==""){
			$OUT->redirect($CONF['url_app'].'?m=account&c=show_my_listings&msg=empty');
			exit;	
		}
		if($Q->cookies['account:strPass'] != $strOld ){
			$OUT->redirect($CONF['url_app'].'?m=account&c=show_my_listings&msg=oldmatch');
			exit;
		}
		if($strNew != $strComfirm){
			$OUT->redirect($CONF['url_app'].'?m=account&c=show_my_listings&msg=nomatch');
			exit;
		}
		
		//
		// Update Password
		//
		$this->objPass = new core_account();
		$found = $this->objPass->doUpdatepass($this->AccID,$strNew);
				
		if($found == 1){
			$OUT->addCookie('account:strPassword', '',time()-3600);
			$OUT->addCookie('account:strPass',md5($strNew));
			$OUT->redirect($CONF['url_app'].'?m=account&c=show_my_listings&msg=done3');
			exit;
		}
		else{
			echo "Database Error : Call Your Database Administrator";
		}
		
		exit;
	}
	
		
	// START function show_error()
	function show_error($err_code){
		global $CONF, $Q, $OUT, $DB,$LANG;
		
		if($err_code == 'err_auth'){
			$OUT->redirect($CONF['url_app']. "?m=main&c=show_login");
			exit();
		}

		if($err_code == 'err_edit_account'){
			$OUT->setVar('content', $this->page_account_form());
			$OUT->flush();	
		
		}
		
		if($err_code=='show_edit_ads_error'){
			$OUT->setVar('content', $this->page_ads_listings_edit());
			$OUT->flush();	
		
		}
		if($err_code=='err_upload'){
			$OUT->setVar('content', $this->page_premiumn_images());
			$OUT->flush();	
		
			}
		if($err_code=="err_edit_email"){
			$OUT->setVar('content', $this->page_editemail());
			$OUT->flush();	
		
			
			
			}
		
	}
	// END function show_error()
	
	// END function _temp()
		  
  function _output($includefile) {
    ob_start();
    include ($includefile);
    $contents = ob_get_contents();
		ob_end_clean();
    
    return $contents;
  }

  function _temp($str, $error, $data, $param) {
    global $CONF, $Q, $OUT, $DB, $LANG;
    // To get the access

    ob_start();
   
    $include_file = array('main' => DIR_TEMP_PATH_FO . '/account/' . $str);
     include (ROOT_PATH . 'design/'.$CONF['tpl_name'].'/tpl/fo/tmp.php');
    $contents = ob_get_contents();
	ob_end_clean();
      return $contents;
   
  }
}
?>