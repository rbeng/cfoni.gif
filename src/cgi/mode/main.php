<?php
require_once ROOT_PATH . "src/core/core_main.php";
require_once ROOT_PATH . "/tcpdf/config/lang/eng.php";
require_once ROOT_PATH . "/tcpdf/tcpdf.php";

class main {

  function __construct() {
    global $CONF, $Q, $OUT, $DB, $LANG, $MailClass, $MailHintClass, $MailClassSecond, $MailClassThird;

    // Language directoty	
    $this->AccID = $Q->cookies['account:intAccId'];
    $LANG->load('main');
		
		if($Q->req['intC']!=""){
			$intCookieCurrncy = $Q->req['intC'];
		   $OUT->addCookie('select:currency', '', time() - 3600);
			 $OUT->addCookie('select:currency', $intCookieCurrncy);
			
			}
		
  
    if (!array_key_exists('c', $Q->req)) {
      $Q->req['c'] = 'show_main';
    }
    //
    // Authentication	 //
		{
      //
      // Command that don't need authentication
      //
		$no_auth = array(
          'show_main'     => '',
		  'shw_register'  => '',
		  'do_register'   => '',
		  'shw_register_msg' => '',
		  'show_register_verify' => '',
		  'do_varify_acc'=>'',
		  'shw_login'    => '',
		  'do_login'     => '',
		  'do_logout'    => '',
		  'shw_all_modules'=>'',
		  'shw_contactus'  => '',
		  'do_addContactUs' => '',
		  'shw_forgot'      => '',
		  'do_send_forgot'  => '',
		  'shw_package'     => '',
		  'shw_table_price' => '',
		  'shw_forgot_thanks' => '',
		  'shw_change_passform' => '',
		  'do_change_password'  => '',
		  'shw_product_byid'    => '',
		  'shw_selection'      => '',
		  'do_check_email'     => '',
		  
		  'shw_cloudpbx'  => '',
		  'shw_emailzimbra'  => '',
		  'shw_hosted_crm'  =>'',
		  'shw_didnumbers'  =>'',
		  'shw_faxemail'  =>'',
		  'shw_customizedsolutions' =>'',
		  'shw_conference' =>'',
		  'shw_callforwarding' =>'',
		  'shw_pagefaq' =>'',
		  'shw_howto'  =>'',
		  'shw_download' =>'',
          'shw_about'     => '',
          'shw_privacypolicy'  => '',
          'shw_terms_and_services'  => '',
          'shw_service_level_agreement'  => '',
          
          'shw_solutions' => '',
          'shw_campaign'  => '',
          'shw_product'   => '',
		  'shw_subproducts'  => '',
          'do_addContact' => '',
		  'shw_googlemap' => '',
		  'show_product_moredetails' => '',
		  'do_addCampaign'   => '',
		  'show_notice'     => '',
		  'show_small_contact' => '',
      );

      if (!array_key_exists($Q->req['c'], $no_auth)) {
        if (!$this->doAuth()) {


          $param = array();
          foreach (array_keys($Q->req) as $key) {
            if (!preg_match('/main:/i', $key)) {
              array_push($param, $key . '=' . $Q->req[$key]);
            }
          }
          $good_param = join("&", $param);
          //$intCheckout = $Q->req['intCheckout'];
          $return = $CONF['url_app'] . '?' . $good_param;
          $OUT->addCookie('account:Return', $return);
          $this->show_error('err_auth');
        }
      }
    }

    if (!isset($_SESSION["spiral"])) {
      $_SESSION["spiral"] = array();
    }
  }

  public function _run() {
    global $CONF, $Q, $OUT, $LANG;
    $cmd = $Q->req['c'];
    $valid_cmd = array(
        'show_main'     => null,
		'shw_register'  => null,
		'do_register'   => null,
		'shw_register_msg'=> null,
		'show_register_verify' => null,
		'do_varify_acc'=>null,
		'shw_login'    => null,
		'do_login'     => null,
		'do_logout'    => null,
		'shw_all_modules' => null,
		'shw_contactus'   => null,
		'do_addContactUs' => null,
		'shw_forgot'      => null,
		'do_send_forgot'  => null,
		'shw_package'     => null,
		'shw_table_price' => null,   
		'shw_forgot_thanks' => null,
		'shw_change_passform' => null,
		'do_change_password'  => null,
		'shw_product_byid'    => null,
		'do_addCart'          => null,
		'shw_selection'       => null,
		'do_check_email'      => null,
		
		'shw_cloudpbx'  => null,
		'shw_emailzimbra'  => null,
		'shw_hosted_crm'  => null,
		'shw_didnumbers' => null,
		'shw_faxemail' => null,
		'shw_customizedsolutions' => null,
		'shw_conference' => null,
		'shw_callforwarding' => null,
		'shw_pagefaq' => null,
		'shw_howto' => null,
		'shw_download' => null,
        'shw_about'     => null,
        'shw_privacypolicy'   => null,
        'shw_terms_and_services'   => null,
        'shw_service_level_agreement'  => null,
        'shw_contactus'  => null,

        'shw_solutions' => null,
         'shw_campaign'  => null,
         'shw_product'   => null,
	    'shw_subproducts'  => null,
        'do_addContact' => null,
        'shw_googlemap' => null,
		'show_product_moredetails' => null,
		'do_addCampaign'  => null,
		'show_notice'     => null,
		'show_small_contact' => null,
    );
    //
    // Setting default cmd if not found
    //
		if (!array_key_exists($cmd, $valid_cmd)) {
      $cmd = 'show_login';
    }
    //
    // Executing the command
    //
		$this->$cmd();
  }

  function show_main() {
    global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_main()); {
      $fh = fopen($CONF['dir_log'] . 'main_log.txt', 'a') or die;
      flock($fh, LOCK_EX) or die('Error: Fail to lock file');
      fwrite($fh, join("\t", array(
                  date("Y-m-d\tH:i:s"),
                  $_SERVER['REMOTE_ADDR'],
                  V,
              )) . "\n");
      flock($fh, LOCK_UN) or die('Error: Fail to unlock file');
      fclose($fh);
    }
    $OUT->flush();
  }

  function page_main() {
    global $CONF, $Q, $OUT, $LANG;
    
   
		return $this->_temp('show_main.php', $error, $data, $param);
  }
  //
  // Show Register Page
  //
  function shw_register(){
	global $CONF, $Q, $OUT, $LANG;

    	$OUT->setVar('content', $this->page_register()); 
    	$OUT->flush();
	}
  //
  // Page Register
  //
  function page_register(){
	global $CONF, $Q, $OUT, $LANG;
	  
	  $error = $OUT->errors;
	  return $this->_temp('show_register.php', $error, $data, $param); 
	}
  //
  // Do Register
  //
  function do_register(){
	global $CONF, $Q, $OUT, $LANG;
	 
	 $strCustomerIp = $_SERVER['REMOTE_ADDR']; 
	 $strUsernamee  = $Q->req['strUsernamee'];
	  
	 $strUsername_Email = $Q->req['strUsername_Email'];
	 $strPassword = $Q->req['strPassword'];
	 $strCPassword   = $Q->req['strComfirmPassword'];
	 $strSalesmanCode = $Q->req['strSalesmanCode'];
	 
	 $intBusinessType  = $Q->req['intBusinessType'];
	 
	if($strUsernamee!=""){
			
			data_load($CONF['dir_data']."blocklist.txt", $data);
			data_load($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			$cData = count($data);
			if($cData > 0){
				$blockid = $cData + 1;
			}
			else{
				$blockid = 1;
			}
			$data[$blockid] = $_SERVER['REMOTE_ADDR']." ".date('Y-m-d H:s');
			$dataip[$blockid] = $_SERVER['REMOTE_ADDR'];
			data_save($CONF['dir_data']."blocklist.txt", $data);
			data_save($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			//
			// Inform Server PPl some hacking is being try , please block this IP ASAP
			//
			$strTitle   = 'Caution : Hacking for Registration Page';
			$strContent  = 'This IP ('.$_SERVER['REMOTE_ADDR'].') address  try to hack the Registration Page, please take the action to block this IP ASAP.';
			
			include('email_tmpl/block.php');
			
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $CONF['administration_email'] );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			} 	
			exit;
		 }
		 
		//
		// Username Exists Or Not
		//
		if($strUsername_Email!=""){
		$this->objcheck = new core_main();
		$this->objcheck->doCheckUsername($strUsername_Email);
		$this->objcheck->GetNextRecord();
		$strDBUsername = $this->objcheck->Get('strUser');
		 if(!preg_match($CONF['regex_email'],$strUsername_Email)){
			$OUT->addError('Invalid Email Format.');
			}
		 if($strDBUsername==$strUsername_Email){
			$OUT->addError('Username already exists.');	
		  }
		}
		else{
			$OUT->addError('Username cannot be empty.');
		}
		
		
		if($strPassword==""){
		
			$OUT->addError('Passwrod cannot be empty.');	
		}
		else{
			
			if($strPassword!=$strCPassword){
				
				$OUT->addError('Passwrod doesn\'t match.');	
			}
			
		}
	
	  if($strSalesmanCode!=""){
		 //
		 // Check the Salesman Code
		 //  
		 $this->objchecksalescode = new core_main();
		 $this->objchecksalescode->doChecksalescodebyid($strSalesmanCode);
		 $this->objchecksalescode->GetNextRecord();
		 $strDBSalemanCode = $this->objchecksalescode->Get('strSalesmanCode');
		  if($strDBSalemanCode!=$strSalesmanCode){
			  $OUT->addError('Invalid Introducer Id.');	
			 }
	   }
	   if($OUT->isError()){
			$this->show_error('err_add_customer');
		exit;
		}
		 if(empty($strSalesmanCode)){
		  $strSalesmanCode = "-";
		 } 
		//
		// Do Insert New Customer 
		//
		$this->objAddCustomer = new core_main();
		$intCustomerId = $this->objAddCustomer->doAddCustomer($strCustomerIp,$strUsername_Email,$strPassword,$strSalesmanCode,$intBusinessType);
	    if($intCustomerId > 0){
			//
			// Email to Customer
			//
			//$strTitle   = 'Welcome to CFONI';
			//$strContent  = '';
			
			include('email_tmpl/register_email.php');
			exit;
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $strUsername_Email );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			} 	
			
		$OUT->redirect($CONF['url_app'].'?m=main&c=shw_register_msg');	
		}
	  
  }
  //
  // Show register Message
  //
  function shw_register_msg(){
	 global $CONF, $Q, $OUT, $LANG;
	  
	 $OUT->setVar('content', $this->page_register_msg()); 
   	 $OUT->flush();
	}
  //
  //Page Message
  //
  function page_register_msg(){
	 global $CONF, $Q, $OUT, $LANG;
	   
	 return $this->_temp('show_thanks_register.php', $error, $data, $param); 
  }
  //
  // do varify
  //
  function do_varify_acc(){
	  global $CONF, $Q, $OUT, $LANG;
	  
	  $intAccId = decrypt($Q->req['intAccId'],'cfoni.2828');
	  $strPassword = decrypt($Q->req['strPassword'],'cfoni.999888');
	 // echo $intAccId.":".$strPassword;
	  //
	  // Do check valdation
	  //
	  $this->obj = new core_main();
	  $this->obj->doCheckActive($intAccId);
	  $this->obj->GetNextRecord();
	  $intVerify = $this->obj->Get('intVerify'); 
	 
	  if($intVerify==1){
		  //
		  // Update Status and Verify= 2
		  //
		  $this->objVarify = new core_main();
		  $res =  $this->objVarify->doUpdateStatsCustomer($intAccId);
		  if($res > 0){
			  
			  $OUT->redirect($CONF['url_app'].'?m=main&c=shw_login&msg='.encrypt('1','!@#$QWee'));
			  //decrypt($Q->req['msg'],'!@#$QWee')
			 }
		  
		 }
	  exit;
	}
  //
  // Show Contact Us
  //
  function shw_contactus(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_contactus()); 
    $OUT->flush();  
  }
  //
  // Page Contact Us
  //
  function page_contactus(){
	 global $CONF, $Q, $OUT, $LANG;
	 
	  $error = $OUT->errors;
	   return $this->_temp('show_contactus.php', $error, $data, $param); 
   }
  //
  // Do Add Contact Us
  //
  function do_addContactUs(){
	global $CONF, $Q, $OUT, $LANG,$MailClass;
	
	$strName    = $Q->req['name'];
	$strEmail   = $Q->req['email'];  
	$strSubject = $Q->req['subject']; 
	$strMsg     = $Q->req['message'];
	$csf        = $Q->req['csf'];
	$strIp      = $_SERVER['REMOTE_ADDR']; 
	
	if($Q->req['csf']!=""){
			data_load($CONF['dir_data']."blocklist.txt", $data);
			data_load($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			$cData = count($data);
			if($cData > 0){
				$blockid = $cData + 1;
			}
			else{
				$blockid = 1;
			}
			$data[$blockid] = $_SERVER['REMOTE_ADDR']." ".date('Y-m-d H:s');
			$dataip[$blockid] = $_SERVER['REMOTE_ADDR'];
			data_save($CONF['dir_data']."blocklist.txt", $data);
			data_save($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			//
			// Inform Server PPl some hacking is being try , please block this IP ASAP
			//
			$strTitle   = 'Caution [ Front-End Contact Us ]: Hacking Front End System';
			$strContent  = 'This IP ('.$_SERVER['REMOTE_ADDR'].') address  try to hack the Front End System , please take the action to block this IP ASAP.';
			include('email_tmpl/block.php');
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $CONF['administration_email'] );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;
 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			} 	
			exit;
		 }	
	if($strName==""){
		
		$OUT->addError('Name cannot be empty.');	
	}
	if($strEmail==""){
		$OUT->addError('Email Address cannot be empty.');
	}
    else{
			
		if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
		}
		
	}
	
	if($strSubject==""){
		
		$OUT->addError('Subject cannot be empty.');	
	}
	
	if($strMsg==""){
		$OUT->addError('Message cannot be empty.');	
	}
	 
	if($OUT->isError()){
			$this->show_error('err_add_contactus');
			exit;
		} 
	
	  $strMessage = "Customer Name : ". $strName." <br> Email :".$strEmail."<br> Message :".$strMsg;
	
	
	
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $CONF['info_email'] );
		    $MailClass->Subject = $strSubject ;
		    $MailClass->Body = $strMessage;
 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			}
		    
	   //
	   // Do Insert Feedback Table
	   //
	   $this->objinsert = new core_main();
	   $this->objinsert->doInsertFeedback($strName,$strEmail,$strSubject,$strMsg,$strIp );
	   
	   $OUT->redirect($CONF['url_app'].'?m=main&c=shw_contactus&msg=1');
	exit;	
 }
 
  //
  // Show Selection
  //
  function shw_selection(){
	   global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_freetrial()); 
    $OUT->flush();   
	  
	 }
  //
  // Page Free Trial
  //
  function page_freetrial(){
	   global $CONF, $Q, $OUT, $LANG;  
	
	  $error = $OUT->errors;
	  //
	  // Get the Free Number & Fax
	  //
	  $this->objfreenumber = new core_main();
	  $this->objfreenumber->doGetFreeNumber();
	  while($this->objfreenumber->GetNextRecord()){
		  
		  $data['number'][$this->objfreenumber->Get('intDidFaxId')] = $this->objfreenumber->Get('strNumber');
		}
	  
	  
	  
	  return $this->_temp('show_selection_page.php', $error, $data, $param);  
	  
   } 
   //
   // Do Check Avaibilty
   //
   function do_check_email(){
	 global $CONF, $Q, $OUT, $LANG;   
	  
	  $strEmail = $_POST['username'];
	  
	   if($strEmail=="abc"){
		  
		  echo 1; 
		  }  
	     else{
			 
			 echo 0;
			 }
			 
			 //exit();
	 }
 
 
 
 
  //
  // Show forgot password page
  //
  function shw_forgot(){
	 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_forgot()); 
    $OUT->flush();   
  }
  //
  // Page forgot
  //
  function page_forgot(){
	  global $CONF, $Q, $OUT, $LANG;  
	
	  $error = $OUT->errors;
	  return $this->_temp('show_forogt_page.php', $error, $data, $param);  
	  
	} 
  //
  // Do Send Forgot Password Page
  //
  function do_send_forgot(){
	 global $CONF, $Q, $OUT, $LANG,$MailClass;  
	
	  $strEmail = $Q->req['strUsername_Email'];	
	  
	  if($Q->req['strUsernamee']!=""){
		  
		  $OUR->redirect('http://www.hotmail.com');
		  exit;
		}
	  
	  if($strEmail==""){
		$OUT->addError('Username cannot be empty.');	
		}
		
	  else{
	
	   if(!preg_match($CONF['regex_email'],$strEmail)){
			$OUT->addError('Invalid Email Format.');
		}
		
	  //
	  // Check Valid Username
	  //
	  $this->doCheckUsername = new core_main();
	  $this->doCheckUsername->doCheckUsername($strEmail);
	  $this->doCheckUsername->GetNextRecord();
	  $strDBUser = $this->doCheckUsername->Get('strUser');
	  if($strDBUser==$strEmail){
		  //
		  // Email to this user
		  //
		
		$rcode = encrypt($strEmail, 'cfoni8888!23');
		$strMessage  = '
	
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<META http-equiv="Content-Type" CONTENT="text/html; charset=utf-8" />
<TITLE>email template</TITLE>
</HEAD>

<BODY>
<TABLE WIDTH="600" BORDER=0 CELLSPACING=0 CELLPADDING=0 STYLE="-MOZ-BORDER-RADIUS:8PX;-WEBKIT-BORDER-RADIUS:8PX;BORDER-RADIUS:8PX;PADDING-BOTTOM:15PX;BACKGROUND:URL('.HTTP_SERVER.'/email_templ/email_valid-template.png) #dedede NO-REPEAT TOP">
	<TR><TD>
        <TABLE BORDER=0 ALIGN=CENTER CELLPADDING=0 CELLSPACING=0 STYLE="COLOR:#000;WIDTH:570PX;PADDING:15PX;FONT-SIZE:11PX;FONT-FAMILY:Arial, Helvetica, sans-serif;MARGIN-TOP:150PX;-MOZ-BORDER-RADIUS:5PX;-WEBKIT-BORDER-RADIUS:5PX;BORDER-RADIUS:5PX;BACKGROUND:#fff">
          <TR><TD>Hi '.$strEmail.',</TD></TR>
          <TR><TD STYLE="PADDING:8PX 0">
          
         Cfoni received a request to reset the password for your account.<br><br>
					
		
					To reset your password, please click on the link below:<br>
			
					 <a href="' . HTTP_SERVER . HTTP_ROOT . 'index.php?m=main&c=shw_change_passform&rcode=' . $rcode . '&strEmail=' . $strEmail . '">' . HTTP_SERVER . HTTP_ROOT . 'index.php?m=main&c=shw_change_passform&rcode=' . $rcode . '&strEmail=' . $strEmail . '</a> <br>
			<br>
					If you don\'t want to reset your password, kindly ignore this message.
                                                                                              
                                                                                                                                                                               </TD></TR>
          <TR><TD>All the best<br /><b>The Cfoni Team</b></TD></TR>
        </TABLE>
	</TD></TR>
</TABLE>
</BODY></HTML>
	';



      $MailClass->IsHTML(true);
      $MailClass->SMTPAuth = true;
      $MailClass->addAddress($strEmail);
      $MailClass->Subject = 'Cfoni : Forgot your password';
      $MailClass->Body = $strMessage;


      //$MailClass->SMTPDebug = true;

      if ($_SERVER['SERVER_NAME'] != "127.0.0.1") {
        if (!$MailClass->Send()) {
          echo $MailClass->ErrorInfo;
          return;
        }
      }
		
		
		 }
		else{
			
			$OUT->addError('invalid Username.');	
			
	     }
	  }
		 if($OUT->isError()){
			$this->show_error('err_send_forgot');
			exit;
		} 
		$OUT->redirect($CONF['url_app'].'?m=main&c=shw_forgot_thanks');
	}
	
	//
	// Show Change Format
	//
	function shw_change_passform(){
	  global $CONF, $Q, $OUT, $LANG;

     $OUT->setVar('content', $this->page_forgot_form()); 
     $OUT->flush(); 
		
	}
	//
	// Page Forgot Form
	//
	function page_forgot_form(){
	 global $CONF, $Q, $OUT, $LANG;
		
		$strEmail = $Q->req['strEmail'];
		$rcodeR    = $Q->req['rcode'];
		 $error = $OUT->errors;
		$rcode = encrypt($strEmail, 'cfoni8888!23');
		
		if($rcode==$rcodeR){
			
		  return $this->_temp('show_change_password_form.php', $error, $data, $param);  
		exit;	
		}
		else{
			
			
		  $OUT->redirect($CONF['url_app']);	
		  exit;
		}
		
		
		
	}
	//
  // Do edit Password & Send Email Notification
  //
	function do_change_password() {
    global $CONF, $Q, $OUT, $LANG, $CODE, $MailClass;


    $strEmail1 = $Q->req['strEmail'];
    $strPass   = $Q->req['pass'];
    $strCPass  = $Q->req['pass2'];
	
	if($strPass==""){
		$OUT->addError('Password cannot be empty.');
		$arrMsg[0] = encrypt('Password cannot be empty.','cfoni8888!23');	
	 }
	if($strPass!=$strCPass){
		
		$OUT->addError('Password doesn\'t match.');
		$arrMsg[1] = encrypt('Password doesn\'t match.','cfoni8888!23');		
	}
	$rcode = encrypt($strEmail1, 'cfoni8888!23');
	if($OUT->isError()){
			//$this->show_error('err_send_forgot');
			
		
		$a = json_encode($arrMsg);
		
		$OUT->redirect($CONF['url_app'].'?m=main&c=shw_change_passform&rcode='.$rcode.'&strEmail='.$strEmail1.'&errorMsg='.$a);
		
			
			exit;
	}
	//
	// Do Change Password
	//
	$this->objchange = new core_main();
	$this->objchange->doChangePassword($strEmail1,$strPass);
     //return $this->_temp('show_changepass_thanks.php', $error, $data, $param);
	 $OUT->redirect($CONF['url_app'].'?m=main&c=shw_login&msg='.$rcode.'&strUser='.$strEmail1);
	 

	}
	
	
  //
  // Show Forgot Thanks Msg
  //
  function shw_forgot_thanks(){
	 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_forgot_msg()); 
    $OUT->flush(); 
	  
	}
  //
  // Page Forgot
  //
  function page_forgot_msg(){
	   global $CONF, $Q, $OUT, $LANG;
	  
	  
	  
	  return $this->_temp('show_forgot_thanks_msg.php', $error, $data, $param);     
	  
	 }
	
	
	
	
	
  //
  // Show Product
  //
  function shw_product_byid(){
	 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_productby()); 
    $OUT->flush(); 
	  
	}
  //
  // Page Product
  //
  function page_productby(){
	   global $CONF, $Q, $OUT, $LANG;
	  
	   $intProductId = decrypt($Q->req['id'],'cfoni.282828');
	   
	   //
	   // Get the Product By Id
	   //
	   $this->objpackagedetailsbyid = new core_main();
	   $this->objpackagedetailsbyid->doGetProductDetails($intProductId);
	   $this->objpackagedetailsbyid->GetNextRecord();
	   $data['strProductName'] = $this->objpackagedetailsbyid->Get('strProductName');
	   $data['strFeature']     = html_entity_decode($this->objpackagedetailsbyid->Get('strFeature'));
	   $data['strDescription'] = html_entity_decode($this->objpackagedetailsbyid->Get('strDescription'));
	   $data['intProductId']   = $this->objpackagedetailsbyid->Get('intProductId');
	   $data['strImage']       = $this->objpackagedetailsbyid->Get('strImage');
	   
	  return $this->_temp('show_product_details.php', $error, $data, $param);     
	 } 
	
	
	
	
	
	
	
	
	
  //
  // Show Package
  //
  function shw_package(){
	 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_package()); 
    $OUT->flush(); 
	  
	}
  //
  // Page Package
  //
  function page_package(){
	   global $CONF, $Q, $OUT, $LANG;
	  
	   $intPackageId = decrypt($Q->req['id'],'cfoni.282828');
	   
	   //
	   // Get the Package By Id
	   //
	   $this->objpackagedetailsbyid = new core_main();
	   $this->objpackagedetailsbyid->doGetPackageDetails($intPackageId);
	   $this->objpackagedetailsbyid->GetNextRecord();
	   $data['strPackageName'] = $this->objpackagedetailsbyid->Get('strPackageName');
	   $data['strFeature']     = html_entity_decode($this->objpackagedetailsbyid->Get('strFeature'));
	   $data['strDescription'] = html_entity_decode($this->objpackagedetailsbyid->Get('strDescription'));
	   $data['intPackageId']   = $this->objpackagedetailsbyid->Get('intPackageId');
	   $data['strImage']       = $this->objpackagedetailsbyid->Get('strImage');
	   
	  return $this->_temp('show_package_details.php', $error, $data, $param);     
	 } 
  	
	//
	// Show Table Lists
	//
	function shw_table_price(){
		global $CONF, $Q, $OUT, $LANG;

    	$OUT->setVar('content', $this->page_package_table()); 
    	$OUT->flush(); 
	}
	//
	// Page Table price
	//
	function page_package_table(){
		global $CONF, $Q, $OUT, $LANG;
		
		//
		// Get Active Package
		//
		$this->obj = new core_main();
		$this->obj->doGetPackageActive();
		while($this->obj->GetNextRecord()){
			$data['table'][$this->obj->Get('intPackageId')]['intPackageId'] = $this->obj->Get('intPackageId');
			$data['table'][$this->obj->Get('intPackageId')]['strPackageName'] = $this->obj->Get('strPackageName');
			$data['table'][$this->obj->Get('intPackageId')]['strFeature'] = html_entity_decode($this->obj->Get('strFeature'));
			$data['table'][$this->obj->Get('intPackageId')]['strPrice'] = $this->obj->Get('strPrice');
			$data['table'][$this->obj->Get('intPackageId')]['strImage'] = $this->obj->Get('strImage');
		}
		
		return $this->_temp('show_all_package_details.php', $error, $data, $param); 
	} 
	
	
	
  //
  // Show Login Page
  //
  function shw_login(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_login()); 
    $OUT->flush();  
  }
  //
  // Page Login
  //
  function page_login(){
	global $CONF, $Q, $OUT, $LANG;  
	  $error = $OUT->errors;
	return $this->_temp('show_login_page.php', $error, $data, $param);   
  } 
  
    //
	// Do Login
	//
	function do_login(){
		global $CONF, $Q, $OUT,$DB, $LANG,$MailClass;
		
		if($Q->req['mustkeyin']!=""){
			
			data_load($CONF['dir_data']."blocklist.txt", $data);
			data_load($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			$cData = count($data);
			if($cData > 0){
				$blockid = $cData + 1;
			}
			else{
				$blockid = 1;
			}
			$data[$blockid] = $_SERVER['REMOTE_ADDR']." ".date('Y-m-d H:s');
			$dataip[$blockid] = $_SERVER['REMOTE_ADDR'];
			data_save($CONF['dir_data']."blocklist.txt", $data);
			data_save($CONF['dir_data']."blocklist_iponly.txt", $dataip);
			//
			// Inform Server PPl some hacking is being try , please block this IP ASAP
			//
			$strTitle   = 'Caution : Hacking Front-End Login Page';
			$strContent  = 'This IP ('.$_SERVER['REMOTE_ADDR'].') address  try to hack Front-End Login Page , please take the action to block this IP ASAP.';
			
			include('email_tmpl/block.php');
			
			$MailClass->IsHTML(true);
		    $MailClass->SMTPAuth = true;
		    $MailClass->addAddress( $CONF['administration_email'] );
		    $MailClass->Subject = $strTitle ;
		    $MailClass->Body = $strMessage;

 			if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
				if ( ! $MailClass ->Send() ){
				echo $MailClass ->ErrorInfo;
				}
			} 	
			exit;
		 }
		
		$strUsername	= $Q->req['user_id'];
		$strPassword	= md5($Q->req['user_pwd']);
		$remember       = $Q->req['remember'];
		
		
		
		$this->objMember = new core_main();
		$this->objMember->getLoginMember($strUsername);
		$this->objMember->GetNextRecord();
		$data['strPass'] = $this->objMember->Get('strPass');
        $data['strUser'] = $this->objMember->Get('strUser');
		 $data['intCustomerId'] = $this->objMember->Get('intCustomerId');
		if($strPassword!= $data['strPass']){
				$OUT->addError('Incorrect login or password.'); 
				$this->show_error('err_login');
			
		
		}
		else{
			
			
			
	
            $session = session_get();
			$OUT->addCookie('account:Session', $session);
			
			$OUT->addCookie('account:strPass', $data['strPass']);
			$OUT->addCookie('account:strUser', $data['strUser']);
			$OUT->addCookie('account:intCustomerId', $data['intCustomerId']);
						
			$OUT->addCookie('account:Return' , '-', time()-3600);   // Clear cookies
			
			
			if(isset($remember)){
					$OUT->addCookie('account:strRemember', 1,time()+604800);
					$OUT->addCookie('account:strUser', $data['strUser'],time()+604800);
					$OUT->addCookie('account:strPass', $data['strPass'],time()+604800);
					session_start();
					$_SESSION['cookie_expire']=1;
				}else{
					$OUT->addCookie('account:strUser', $data['strUser']);
					$OUT->addCookie('account:strPass', $data['strPass']);	
				}
				
			//
			// Delete the old session 
			//
			
			$this->objSession = new core_main();
			$deletR = $this->objSession->deleteRecord($data['strUser']);
			
			
			$this->objSession = new core_main();
			$addR=$this->objSession->addRecord($data['strUser'],$session);
			
 			if(array_key_exists('account:Return', $Q->cookies)){
 				
 			
				$OUT->redirect($Q->cookies['account:Return']);
			}
			else{
				
				$OUT->redirect($CONF['url_app']. "?m=main&a=1");
			}
		}
	}
	//
	// Do Logout
	//
	function do_logout(){
		global $CONF, $CODE, $Q, $OUT, $LANG;

			
		
		$OUT->addCookie('account:Session', '', time()-3600);
		$OUT->addCookie('account:strPass', '', time()-3600);
		$OUT->addCookie('account:strUser', '', time()-3600);
		$OUT->addCookie('account:intCustomerId', '', time()-3600);
		
		
		$OUT->redirect($CONF['url_app']. "?m=main&c=shw_login");
	}
  	//
	// All Account Modules
	//
	function shw_all_modules(){
	 global $CONF, $Q, $OUT, $DB, $LANG;
		
		$OUT->setVar('content', $this->page_allmodules()); 
        $OUT->flush();
	}
	//
	// Page Account Modules
	//
	function page_allmodules(){
		global $CONF, $Q, $OUT, $DB, $LANG;
		
		$id = $Q->cookies['account:intCustomerId'];
		return $this->_temp('show_all_modules.php', $error, $data, $param); 
	} 
	
	
  
   //
  // Do authentication
  //
	function doAuth() {
    global $CONF, $Q, $OUT, $DB, $LANG;

    if (array_key_exists('account:Session', $Q->cookies)) {
      $Session = $Q->cookies['account:Session'];
      $strUser = $Q->cookies['account:strUser'];

      $data = array();
      // find user's session
      $this->objSession = new core_main();
      $found = $this->objSession->retrieveSession('customer_session', $strUser, $Session);
      if (is_array($found)) {
        return true;
      } else {
        return false;
      }
    }
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
   
  //
  // Show Cloud PBX
  //
  function shw_cloudpbx(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_cloudpbx()); 
    $OUT->flush();
	  
	}
   //
   // Page Clound PBX
   //
   function page_cloudpbx(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_cloudpbx.php', $error, $data, $param); 
	} 
  
  //
  // Show Email zimbra
  //
  function shw_emailzimbra(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_emailzimbra()); 
    $OUT->flush();
	  
	}
   //
   // Page Email zimbra
   //
   function page_emailzimbra(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_emailzimbra.php', $error, $data, $param); 
	} 

	//
  // Show customer relationship management
  //
  function shw_hosted_crm(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_hosted_crm()); 
    $OUT->flush();
	  
	}
   //
   // Page hosted crm
   //
   function page_hosted_crm(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_hosted_crm.php', $error, $data, $param); 
	} 
  //
  // Show did numbers
  //
  function shw_didnumbers(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_didnumbers()); 
    $OUT->flush();
	  
	}
   //
   // Page didnumbers
   //
   function page_didnumbers(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_didnumbers.php', $error, $data, $param); 
	} 
	//
  // Show faxemail
  //
  function shw_faxemail(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_faxemail()); 
    $OUT->flush();
	  
	}
   //
   // Page faxemail
   //
   function page_faxemail(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_faxemail.php', $error, $data, $param); 
	} 
	//
  // Show customized solutions
  //
  function shw_customizedsolutions(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_customizedsolutions()); 
    $OUT->flush();
	  
	}
   //
   // Page customized solutions
   //
   function page_customizedsolutions(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_customizedsolutions.php', $error, $data, $param); 
	} 
	//
  // Show conference
  //
  function shw_conference(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_conference()); 
    $OUT->flush();
	  
	}
   //
   // Page conference
   //
   function page_conference(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_conference.php', $error, $data, $param); 
	} 
  
  //
  // Show callforwarding
  //
  function shw_callforwarding(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_callforwarding()); 
    $OUT->flush();
	  
	}
   //
   // Page callforwarding
   //
   function page_callforwarding(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_callforwarding.php', $error, $data, $param); 
	} 
	//
  // Show pagefaq
  //
  function shw_pagefaq(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_pagefaq()); 
    $OUT->flush();
	  
	}
   //
   //  page Pagefaq
   //
   function page_pagefaq(){
	  global $CONF, $Q, $OUT, $LANG;  
	  //
	  // Get All the FAQ
	  //
	  $this->faq = new core_main();
	  $this->faq->doGetFAQ();
	  while($this->faq->GetNextRecord()){
		$data['faq'][$this->faq->Get('intType')][$this->faq->Get('intFaqId')]['strQ']  = $this->faq->Get('strQ');
		$data['faq'][$this->faq->Get('intType')][$this->faq->Get('intFaqId')]['strA']  = $this->faq->Get('strA');
	  }
	  
	  
	  return $this->_temp('show_pagefaq.php', $error, $data, $param); 
	} 
	//
  // Show howto
  //
  function shw_howto(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_howto()); 
    $OUT->flush();
	  
	}
   //
   //  page howto
   //
   function page_howto(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_howto.php', $error, $data, $param); 
	} 
	//
  // Show download
  //
  function shw_download(){
	global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_download()); 
    $OUT->flush();
	  
	}
   //
   //  page download
   //
   function page_download(){
	  global $CONF, $Q, $OUT, $LANG;  
	   
	  return $this->_temp('show_download.php', $error, $data, $param); 
	} 
	//
	// Show About
	//	
	function shw_about(){
		 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_about()); 
    $OUT->flush();
	}
	//
	// Page About Us
	//
	function page_about(){
		 global $CONF, $Q, $OUT, $LANG;
	
		return $this->_temp('show_about.php', $error, $data, $param);
	} 
	
	//
	// Show privacypolicy
	//	
	function shw_privacypolicy(){
		 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_privacypolicy()); 
    $OUT->flush();
	}
	//
	// Page privacypolicy
	//
	function page_privacypolicy(){
		 global $CONF, $Q, $OUT, $LANG;
	
		return $this->_temp('show_privacypolicy.php', $error, $data, $param);
	} 
	//
	// Show terms and services
	//	
	function shw_terms_and_services(){
		 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_terms_and_services()); 
    $OUT->flush();
	}
	//
	// Page terms and services
	//
	function page_terms_and_services(){
		 global $CONF, $Q, $OUT, $LANG;
	
		return $this->_temp('show_terms_and_services.php', $error, $data, $param);
	} 
	//
	// Show service level agreement
	//	
	function shw_service_level_agreement(){
		 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_service_level_agreement()); 
    $OUT->flush();
	}
	//
	// Page service level agreement
	//
	function page_service_level_agreement(){
		 global $CONF, $Q, $OUT, $LANG;
	
		return $this->_temp('show_service_level_agreement.php', $error, $data, $param);
	} 
	
		

	//
	// Show Solutions
	//
	function shw_solutions(){
		 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_solutions()); 
    $OUT->flush();	
	
	}
	//
	// Page Solutions
	//
	function page_solutions(){
	 global $CONF, $Q, $OUT, $LANG;
	
			
		//
		// Do Call DB Data
		//
		$this->obj = new core_main();
		$this->obj->doGetContentSolution();
		while($this->obj->GetNextRecord()){
			$data['content'][$this->obj->Get('intSection')] = $this->obj->Get('strMessage');
			}	
			
			
			
			
		return $this->_temp('show_solutions.php', $error, $data, $param);	
		
		
	}
	//
	// Show Campaign
	//
	function shw_campaign(){
		 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_compaign()); 
    $OUT->flush();	
	}
	//
	// Page Compaign
	//
	function page_compaign(){
		global $CONF, $Q, $OUT, $LANG;
		
		//
		// Get the Active Compaign 
		//
		$this->obj = new core_main();
		$this->obj->doGetActiveOneCampaing();
		$this->obj->GetNextRecord();
		$data['campaign']['intCampaignId'] = $this->obj->Get('intCampaignId');
		$data['campaign']['strTitle']      = $this->obj->Get('strTitle');
		$data['campaign']['strPic']        = $this->obj->Get('strPic');
		//
		// Get All the Questions
		//
		$arrAnswerSelect = array();
		$intCampaignId = $this->obj->Get('intCampaignId');
		$this->objq = new core_main();
		$this->objq->GetAllQ($intCampaignId);
		while($this->objq->GetNextRecord()){
		 $data['campaign_q'][$this->objq->Get('intQuestionId')] = $this->objq->Get('strQuestion');
		  array_push($arrAnswerSelect,$this->objq->Get('intQuestionId'));
		
		}
		//
		// Count Answer Select 
		//
		$cQuestion =  count($arrAnswerSelect);
		if($cQuestion > 0){
		  //
		  // Query Answer Select
		  //
		  $this->ansewer = new core_main();
		  $this->ansewer->doGetAnswerSelect($arrAnswerSelect,$intCampaignId );
		  while($this->ansewer->GetNextRecord()){
		   $data['answer_select'][$this->ansewer->Get('intQuestionId')][$this->ansewer->Get('intAnswerSelectionId')] = $this->ansewer->Get('strAnswer');
		  }
		  
		}
		
		
		
		return $this->_temp('show_compaign.php', $error, $data, $param);	
	}
	
	//
	// Show Product
	//
	function shw_product(){
		global $CONF, $Q, $OUT, $LANG;
		
		$OUT->setVar('content', $this->page_product()); 
    $OUT->flush();		
	}
	//
	// Page Product
	//
	function page_product(){
	 global $CONF, $Q, $OUT, $LANG;
		
        $this->objship = new core_main();
        $this->objship->doGetAllBrand();
        while($this->objship->GetNextRecord()){
	     $data['brand_flip'][$this->objship->Get('intBrandId')]['intBrandId'] = $this->objship->Get('intBrandId');
		 $data['brand_flip'][$this->objship->Get('intBrandId')]['strPhoto'] = $this->objship->Get('strPhoto');
		 $data['brand_flip'][$this->objship->Get('intBrandId')]['strBrandName'] = $this->objship->Get('strBrandName');
		 $data['brand_flip'][$this->objship->Get('intBrandId')]['strDescription'] = $this->objship->Get('strDescription');
		 
         }
	
		return $this->_temp('show_products.php', $error, $data, $param);	
	}
	
	//
	// Show Subproducts
	//
	function shw_subproducts(){
	 global $CONF, $Q, $OUT, $LANG;

    $OUT->setVar('content', $this->page_subproduct()); 
    $OUT->flush();	
	}
	//
	// Page Subproducts
	//
	function page_subproduct(){
		global $CONF, $Q, $OUT, $LANG;
		 
		 $intBrandId = decrypt($Q->req['BrandId'],'techdata.8888');
		 $intCategoryId = decrypt($Q->req['CateId'],'techdata.8888');
		 if(empty($intBrandId)){
		   $OUT->redirect($CONF['url_app'].'?m=main&c=shw_product');
		  exit;
		 }
		 //
		 // Get Brand Pic
		 //
		 $this->objBrand = new core_main();
		 $this->objBrand->doGetBrandDetailsById($intBrandId);
		 $this->objBrand->GetNextRecord();
		 $data['brand_details']['strBrandName'] =  $this->objBrand->Get('strBrandName');
		 $data['brand_details']['strPhoto'] =  $this->objBrand->Get('strPhoto');
		 $data['brand_details']['intBrandId'] =  $this->objBrand->Get('intBrandId');
		 
		 
		 //
		 // Get All Category By Brand Id 
		 //
		 $arrCategoryId = array();
		 $arrSubCate    = array();
		 $this->objCategory = new core_main();
		 $this->objCategory->doGetAllCategories($intBrandId);
		 while($this->objCategory->GetNextRecord()){
		  array_push($arrCategoryId,$this->objCategory->Get('intCategoryId'));
		  array_push($arrSubCate,$this->objCategory->Get('intSubcategoryId'));
		 }
		 //
		 // Get the SubCategory
		 //
		 $cSub = count($arrSubCate);
		 if($cSub > 0){
		 	$this->objsubcate = new core_main();
		 	$this->objsubcate->doGetSubCate($arrSubCate);
			while($this->objsubcate->GetNextRecord()){
				$data['sub_cate'][$this->objsubcate->Get('intSubCategoryId')] = $this->objsubcate->Get('strSubCategoryName');
				$data['sub_cate_bar'][$this->objsubcate->Get('intCategoryId')][$this->objsubcate->Get('intSubCategoryId')] = $this->objsubcate->Get('strSubCategoryName'); 
			}
		  }
		
		//
		// Get Subcategory Link
		//
		$arrLeftnavlink = array();
		$this->objsublink = new core_main();
		$this->objsublink->doGetSubCateProduct($arrSubCate,$intBrandId);
		while($this->objsublink->GetNextRecord()){
			$data['arrLeftnavlink'][$this->objsublink->Get('intSubcategoryId')]['intProductId'] = $this->objsublink->Get('intProductId');
			$data['arrLeftnavlink'][$this->objsublink->Get('intSubcategoryId')]['strURLStatus'] = $this->objsublink->Get('strURLStatus');
			$data['arrLeftnavlink'][$this->objsublink->Get('intSubcategoryId')]['strURL'] = $this->objsublink->Get('strURL');
			$data['arrLeftnavlink'][$this->objsublink->Get('intSubcategoryId')]['intSubcategoryId'] = $this->objsublink->Get('intSubcategoryId');
		}
		
		
		
		 //
		 // Get Ready Category
		 //
		 $cCategories = count($arrCategoryId);
		 if($cCategories > 0){
		   //
		   // Get Categories By Brand Id
		   //
		   $this->objcat = new core_main();
		   $this->objcat->getCategoryDetails($arrCategoryId);
		   while($this->objcat->GetNextRecord()){
		    $data['leftnav_cat'][$this->objcat->Get('intCategoryId')] = $this->objcat->Get('strCategoryName');
		   
		   }
		 }
		 
		
		if($Q->req['page']){
				$intPage=$Q->req['page'];
				}
		else{
			$intPage=1;
			}
		// Pagination
		$this->objacc = new core_main();
		$this->objacc->numperpage   = 50;
		$this->objacc->numfirstpage = 50;
		
		if( $intBrandId !="" && $intCategoryId!=""){
		 $this->objacc->doGetAllBrandProductCateID($intPage,$intBrandId,$intCategoryId);		
		}
		else{
		$this->objacc1 = new core_main();
		$this->objacc1->doGetBrandLongDesc($intBrandId);	
		$this->objacc1->GetNextRecord();
		$data['strLongDesc'] = $this->objacc1->Get('strLongDesc');
		
		return $this->_temp('show_subproducts_brandonly.php', $error, $data, $param);	
		
		exit;
		}
		
		while($this->objacc->GetNextRecord()){
		 $data['details'][$this->objacc->Get('intProductId')]['intProductId']           =  $this->objacc->Get('intProductId');
         $data['details'][$this->objacc->Get('intProductId')]['strProductName']         =  $this->objacc->Get('strProductName');
         $data['details'][$this->objacc->Get('intProductId')]['strPhoto']               =  $this->objacc->Get('strPhoto');
         $data['details'][$this->objacc->Get('intProductId')]['strSmallDescription']    =  $this->objacc->Get('strSmallDescription');
		 $data['details'][$this->objacc->Get('intProductId')]['strURLStatus']           =  $this->objacc->Get('strURLStatus');
		 $data['details'][$this->objacc->Get('intProductId')]['strURL']                 =  $this->objacc->Get('strURL');
		 $data['details'][$this->objacc->Get('intProductId')]['intSubcategoryId']     	=  $data['sub_cate'][$this->objacc->Get('intSubcategoryId')];
		 
		 		
     }
     
    $this->pages = new Paginator();        
		$this->pages->items_total = $this->objacc->page->total_pages;
		$this->pages->mid_range 	= 6;   
		$this->pages->default_ipp = 1;     
		$this->pages->paginate('|');   
  			 
		 
		 
		 
		return $this->_temp('show_subproducts.php', $error, $data, $param);	
	}
	
	//
	// Do Add Contact Us
	//
	function do_addContact(){
		global $CONF, $Q, $OUT, $LANG,$MailClass;
		
		$strCompanyName =$Q->req['strCompany'];
		$strName =$Q->req['firmname'];
		$strEmail = $Q->req['mail'];
		$strTel = $Q->req['phone'];
		$strMobile = $Q->req['mobile'];
		$strFax = $Q->req['fax'];
		$strWebsite = $Q->req['website'];
		$strMsg = $Q->req['message'];
		$security       = strtolower($Q->req['security']);
		$securityhidden   = strtolower($Q->req['securityhidden']);
        $strSecureBot   = $Q->req['strSecureBot'];
      
	  if(empty($strName)){
		  $respone[0] = 'Name cannot be empty';
		 }
	  
	  if(!preg_match($CONF['regex_email'],$strEmail)){
		  $respone[1] = "Invalid Email Format.";
		  }
	  if($securityhidden!=$security){
		    $respone[2] = "Security Code does not Match.";
		  }
	  $CError = count($respone);
	  if($CError > 0 ){
		  foreach($respone as $error){
			  echo "<font color='red' size='2'>".$error."</font>";
			  echo "<br>";
			  
			  }
		  
		  exit;
		  }
	  if( $strSecureBot!=""){
		   echo "done";
		   exit;
	   }
	  //
	  // Add to Database
	  //
	  $this->objadd = new core_main();
	  $resId = $this->objadd->doAddContact(array(
	  	'strCompanyName' => $strCompanyName,
	  	'strName'        => $strName,
	  	'strEmail'       => $strEmail,
	  	'strTel'         => $strTel,
		'strMobile'      => $strMobile ,
	  	'strFax'         => $strFax,
	  	'strWebsite'     => $strWebsite,
	  	'strMsg'         => $strMsg,
	  ));
    
	
	if($resId > 0){
		
	echo "done";
		
	$strMessage ='
 	  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<META http-equiv="Content-Type" CONTENT="text/html; charset=utf-8" />
<TITLE>Contact Us Email</TITLE>
</HEAD>

<BODY>
<TABLE WIDTH="600" BORDER=0 CELLSPACING=0 CELLPADDING=0 STYLE="-MOZ-BORDER-RADIUS:8PX;-WEBKIT-BORDER-RADIUS:8PX;BORDER-RADIUS:8PX;PADDING-BOTTOM:15PX;BACKGROUND:URL('.HTTP_SERVER.'email_templ/email_valid-template.png) #FFF NO-REPEAT TOP">
	<TR><TD>
        <TABLE BORDER=0 ALIGN=CENTER CELLPADDING=0 CELLSPACING=0 STYLE="COLOR:#000;WIDTH:570PX;PADDING:15PX;FONT-SIZE:11PX;FONT-FAMILY:Arial, Helvetica, sans-serif;MARGIN-TOP:150PX;-MOZ-BORDER-RADIUS:5PX;-WEBKIT-BORDER-RADIUS:5PX;BORDER-RADIUS:5PX;BACKGROUND:#FFF">
          <TR><TD>Hi <strong> Support</strong> ,	</TD></TR>
          <TR><TD STYLE="PADDING:8PX 0">
            
			Customer Details : <br>
			<hr>
			
			
		    Name  : '.$strName.' <br>     
		 Company Name : '.$strCompanyName.'<br>
		  Email Address : '.$strEmail.' <br>
		  Contact No.  : '.$strTel.' <br> 
		   Mobile No.  : '.$strMobile.' <br>
		
		 Message : '.$strMsg.'  
	      
            </TD></TR>
          <TR><TD>Over &amp; Out,<br />The Techdata Team</TD></TR>
        </TABLE>
	</TD></TR>
</TABLE>
</BODY></HTML>'; 	



$CCMail = $CONF['strCCEmail'];
	 	                                $MailClass->IsHTML(true);
										$MailClass->SMTPAuth = true;
										$MailClass->ClearAllRecipients();
										$MailClass->ClearAttachments();
										$MailClass->addAddress($CONF['strToEmail']);
   								        $MailClass->AddCustomHeader("Cc: $CCMail"); 
									
									
										
									
										
										$MailClass->Subject = 'New Message FROM ' .$strName;
										$MailClass->Body = $strMessage;
 										if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
											if ( ! $MailClass ->Send() ){
												echo $MailClass ->ErrorInfo;
												}
										} 
		
		}
	

	exit;
	
	}
	//
	// Do Add Join Campaign
	//
	function do_addCampaign(){
	global $CONF, $Q, $OUT, $LANG,$MailClass;
	
		$strCompanyName =$Q->req['strCompany'];
		$strName =$Q->req['firmname'];
		$strEmail = $Q->req['mail'];
		$strTel = $Q->req['phone'];
		$intCampaignId = $Q->req['intCampaignId'];
		$answer     = $Q->req['answer'];
		
		$intTotalq  = $Q->req['intTotalq'];
		$intRA       = count($answer);
	
		
		$security       = strtolower($Q->req['security']);
		$securityhidden   = strtolower($Q->req['securityhidden']);
        $strSecureBot   = $Q->req['strSecureBotA'];
		$strJobTitle    = $Q->req['strJob'];
      
	 
	  if(empty($strName)){
		  $respone[0] = 'Name cannot be empty';
		 }
	  
	  if(!preg_match($CONF['regex_email'],$strEmail)){
		  $respone[1] = "Invalid Email Format.";
		  }
	  /*if($securityhidden!=$security){
		    $respone[2] = "Security Code does not Match.";
		  }*/
		  if($intRA!=$intTotalq){
	  		    $respone[3] = "Some of the question still not yet to answer.";

	  }
	  $CError = count($respone);
	  if($CError > 0 ){
		  foreach($respone as $error){
			  echo "<font color='red' size='2'>".$error."</font>";
			  echo "<br>";
			  
			  }
		  
		  exit;
		  }
	  if( $strSecureBot!=""){
		   echo "done";
		   exit;
	   }
	   //
	   // Insert Customer Details
	   //
	   $this->objcustomer = new core_main();
	   $intCustomerId = $this->objcustomer->doAddCustomer(array(
	   	'strCompanyName' => $strCompanyName,
		'strName'        => $strName,
		'strEmail'       => $strEmail,
		'strTel'         => $strTel,
		'strJobTitle'    => $strJobTitle
	   ));
	   
	   
	   //
	   // Insert Customer Answer
	   //
	   $this->objAnswer = new core_main();
	   $resId = $this->objAnswer->doAddAnswerCustomer($intCampaignId,$intCustomerId,$answer);
	   
	  
	  
	  
	  if($resId > 0){
		
	echo "done";
		
	$strMessage ='
 	  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<META http-equiv="Content-Type" CONTENT="text/html; charset=utf-8" />
<TITLE>Contact Us Email</TITLE>
</HEAD>

<BODY>
<TABLE WIDTH="600" BORDER=0 CELLSPACING=0 CELLPADDING=0 STYLE="-MOZ-BORDER-RADIUS:8PX;-WEBKIT-BORDER-RADIUS:8PX;BORDER-RADIUS:8PX;PADDING-BOTTOM:15PX;BACKGROUND:URL('.HTTP_SERVER.'email_templ/email_valid-template.png) #FFF NO-REPEAT TOP">
	<TR><TD>
        <TABLE BORDER=0 ALIGN=CENTER CELLPADDING=0 CELLSPACING=0 STYLE="COLOR:#000;WIDTH:570PX;PADDING:15PX;FONT-SIZE:11PX;FONT-FAMILY:Arial, Helvetica, sans-serif;MARGIN-TOP:150PX;-MOZ-BORDER-RADIUS:5PX;-WEBKIT-BORDER-RADIUS:5PX;BORDER-RADIUS:5PX;BACKGROUND:#FFF">
          <TR><TD>Hi <strong> Support</strong> ,	</TD></TR>
          <TR><TD STYLE="PADDING:8PX 0">
            
			Customer Details : <br>
			<hr>
			
			
		    Name  : '.$strName.' <br>     
		 Company Name : '.$strCompanyName.'<br>
		  Job title : '.$strJobTitle.'  <br>
		  Email Address : '.$strEmail.' <br>
		  Contact No.  : '.$strTel.' <br> 
		   Mobile No.  : '.$strMobile.' 
		
		
	      
            </TD></TR>
          <TR><TD>Over &amp; Out,<br />The Techdata Team</TD></TR>
        </TABLE>
	</TD></TR>
</TABLE>
</BODY></HTML>'; 	



$CCMail = $CONF['strCompCCEmail'];
	 	                                $MailClass->IsHTML(true);
										$MailClass->SMTPAuth = true;
										$MailClass->ClearAllRecipients();
										$MailClass->ClearAttachments();
										$MailClass->addAddress($CONF['strComToEmail']);
   								        $MailClass->AddCustomHeader("Cc: $CCMail"); 
									
									
										
									
										
										$MailClass->Subject = '1 New customer joined the campaign from ' .$strCompanyName;
										$MailClass->Body = $strMessage;
 										if($_SERVER['SERVER_NAME']!="127.0.0.1"){	
											if ( ! $MailClass ->Send() ){
												echo $MailClass ->ErrorInfo;
												}
										} 
		exit;
		}
	

	
	  
	  

	}
	
	//
	// Show Notice
	//
	function show_notice(){
		global $CONF, $Q, $OUT, $LANG;
		
		$OUT->setVar('content', $this->page_notice()); 
       	$OUT->flush();	
		
	}
	//
	// Page Notice
	//
	function page_notice(){
	   global $CONF, $Q, $OUT, $LANG;
		
		//
        // Get Msg Link
        //
        $this->objmsg = new core_main();
        $this->objmsg->doGetMsg();
        while($this->objmsg->GetNextRecord()){
		  $data['details'][$this->objmsg->Get('intMsgId')]['strLink'] = $this->objmsg->Get('strLink');
		   $data['details'][$this->objmsg->Get('intMsgId')]['strMessage'] = $this->objmsg->Get('strMessage');
		   
		 }
		 return $this->_output(DIR_TEMP_PATH_FO . '/main/show_notice.php',$data,$error);
	}
	
	//
	// Show Google Map
	//
	function shw_googlemap(){
  	 global $CONF, $Q, $OUT, $LANG;
		
		$OUT->setVar('content', $this->page_googlemap()); 
       	$OUT->flush();		
    }
	//
	// Page google Map
	//
	function page_googlemap(){
  	 global $CONF, $Q, $OUT, $LANG;
	 
	 $iCode = decrypt($Q->req['iCode'],'techdata.8888');
	 
	
	  $this->objcontact =  new core_main();
	  $this->objcontact->getMapAdd();
	  while($this->objcontact->GetNextRecord()){
		 $data['contact'][$this->objcontact->Get('intContactUsId')]['strTitle']  = $this->objcontact->Get('strTitle');
		 $data['contact'][$this->objcontact->Get('intContactUsId')]['strMapPhoto']  = $this->objcontact->Get('strMapPhoto');
		 $data['contact'][$this->objcontact->Get('intContactUsId')]['strAdd']  = $this->objcontact->Get('strAdd');
		 $data['contact'][$this->objcontact->Get('intContactUsId')]['strTel']  = $this->objcontact->Get('strTel');
		 $data['contact'][$this->objcontact->Get('intContactUsId')]['strFax']  = $this->objcontact->Get('strFax');
		 $data['contact'][$this->objcontact->Get('intContactUsId')]['strEmail']  = $this->objcontact->Get('strEmail');
		 $data['contact'][$this->objcontact->Get('intContactUsId')]['strGoogleLink']  = $this->objcontact->Get('strGoogleLink');
		}
	 
	 echo html_entity_decode($data['contact'][$iCode]['strGoogleLink']);
	 
	 
	 
	 exit;
	
	
	}
	//
	// Show small contact Form
	//
	function show_small_contact(){
		global $CONF, $Q, $OUT, $LANG;
		
		$OUT->setVar('content', $this->page_smallcontact()); 
       	$OUT->flush();	
	}
	//
	// Page small contact
	//
	function page_smallcontact(){
		global $CONF, $Q, $OUT, $LANG;
		
		$intProductId = decrypt($Q->req['idCode'],'techdata.8888');
		//
		// Get Product More Details
		//
		$this->obj = new core_main();
		$this->obj->doGetMoreDetails($intProductId);
		$this->obj->GetNextRecord();
		$data['details']['intProductId']    = $this->obj->Get('intProductId');
		$data['details']['strProductName']  = $this->obj->Get('strProductName');
		$data['details']['intBrandId']      = $this->obj->Get('intBrandId');
		$data['details']['strDescription']   =$this->obj->Get('strDescription');
		$data['details']['strPhoto']          =$this->obj->Get('strPhoto');
		$data['details']['intCategoryId']             = $this->obj->Get('intCategoryId');
		
		return $this->_output(DIR_TEMP_PATH_FO . '/main/show_small_contactus.php',$data,$error);
	}
	
	
	//
	// Show More Details
	//
	function show_product_moredetails(){
		global $CONF, $Q, $OUT, $LANG;
		
		$OUT->setVar('content', $this->page_moredetails()); 
       	$OUT->flush();		
	
	}
	//
	// Page More Details
	//
	function page_moredetails(){
		global $CONF, $Q, $OUT, $LANG;
	
		$intProductId = decrypt($Q->req['idCode'],'techdata.8888');
	   $this->brand = new core_main();
	   $this->brand->getAllBrand();
	  while($this->brand->GetNextRecord()){
	    $brand[$this->brand->Get('intBrandId')] = $this->brand->Get('strBrandName');
	  }
	   
	   
	   $arrCate = array();
	   $arrSubCate = array();
	    //
		// Get Product More Details
		//
		$this->obj = new core_main();
		$this->obj->doGetMoreDetails($intProductId);
		$this->obj->GetNextRecord();
		$data['details']['intProductId']    = $this->obj->Get('intProductId');
		$data['details']['strProductName']  = $this->obj->Get('strProductName');
		$data['details']['intBrandId']      = $brand[$this->obj->Get('intBrandId')];
		$data['details']['strDescription']   =$this->obj->Get('strDescription');
		$data['details']['strPhoto']          =$this->obj->Get('strPhoto');
		$strUrl                               = $this->obj->Get('strURL');
		
		array_push($arrCate,$this->obj->Get('intCategoryId'));
        array_push($arrSubCate,$this->obj->Get('intSubcategoryId'));
	    //
		 // Get the SubCategory
		 //
		 $cSub = count($arrSubCate);
		 if($cSub > 0){
		 	$this->objsubcate = new core_main();
		 	$this->objsubcate->doGetSubCate($arrSubCate);
			while($this->objsubcate->GetNextRecord()){
				$data['sub_cate'][$this->objsubcate->Get('intSubCategoryId')] = $this->objsubcate->Get('strSubCategoryName'); 
			}
		  }
		
		 //
		 // Get Ready Category
		 //
		 $cCategories = count($arrCate);
		 if($cCategories > 0){
		   //
		   // Get Categories By Brand Id
		   //
		   $this->objcat = new core_main();
		   $this->objcat->getCategoryDetails($arrCate);
		   while($this->objcat->GetNextRecord()){
		    $data['cate'][$this->objcat->Get('intCategoryId')] = $this->objcat->Get('strCategoryName');
		   
		   }
		 }
		
		
		if($Q->req['link']==1){
			
			//include_once(ROOT_PATH.'outredirect.php');
			
		?>	<iframe src="<?php echo  $strUrl;?>" width="100%" height="100%" allowtransparency="true" frameborder="0"></iframe><?php
		exit;
		} 
		
		$data['details']['intCategoryId']   = $data['cate'][$this->obj->Get('intCategoryId')];
		$data['details']['intSubcategoryId'] =$data['sub_cate'][$this->obj->Get('intSubcategoryId')];
		
		
		
		
		
		return $this->_output(DIR_TEMP_PATH_FO . '/main/show_morepDetails.php',$data,$error);
		
	}
	
  //	
  // Show Error and redirect
  //
	function show_error($err_code) {
    global $CONF, $Q, $OUT, $DB, $LANG;

    if ($err_code == 'err_add_customer') {
      $OUT->setVar('content', $this->page_register());
	  $OUT->flush();
    }
	
	  if ($err_code == 'err_send_forgot') {
      $OUT->setVar('content', $this->page_forgot());
	  $OUT->flush();
    }
	
    if ($err_code == 'err_login') {
      $OUT->setVar('content', $this->page_login());
	  $OUT->flush();
    }
	if ($err_code == 'err_add_contactus') {
      $OUT->setVar('content', $this->page_contactus());
	  $OUT->flush();
    }
	 
  }
  
  function _output($includefile,$data,$error) {
    ob_start();
    include ($includefile);
    $contents = ob_get_contents();
		ob_end_clean();
    
    return $contents;
  }

  function _temp($str, $error, $data, $param) {
    global $CONF, $Q, $OUT, $DB, $LANG;
    // To get the access
   
    ob_start();
    $include_file = array('main' => DIR_TEMP_PATH_FO . '/main/' . $str);
    include (ROOT_PATH . 'design/'.$CONF['tpl_name'].'/tpl/fo/tmp.php');
    $contents = ob_get_contents();
		ob_end_clean();
      return $contents;
  }
    function _tempMenu($str, $error, $data, $param) {
    global $CONF, $Q, $OUT, $DB, $LANG;
    // To get the access
   
    ob_start();
    $include_file = array('main' => DIR_TEMP_PATH_FO . '/main/' . $str);
    include (ROOT_PATH . 'design/'.$CONF['tpl_name'].'/tpl/fo/tmp_menu.php');
    $contents = ob_get_contents();
		ob_end_clean();
      return $contents;
  }
  
  
  

}
?>