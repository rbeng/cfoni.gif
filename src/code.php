<?php
global $CONF,$CODE;


//
// Day Month Year
//
$CODE['dob_day'] = array();
{
	//
	// From day 1 to day 31
	//
	foreach(range(1, 31) as $d){
		$CODE['dob_day'][$d] = $d;
	}
}
									
$CODE['dob_month'] = array(
	'1' => 'January',
	'2' => 'February',
	'3' => 'Mach',
	'4' => 'April',
	'5' => 'May',
	'6' => 'June',
	'7' => 'July',
	'8' => 'August',
	'9' => 'September',
	'10' => 'October',
	'11' => 'November',
	'12' => 'December',
);


$CODE['deal_month'] = array(
	'1' => 'January',
	'2' => 'February',
	'3' => 'macch',
	'4' => 'April',
	'5' => 'May',
	'6' => 'June',
	'7' => 'July',
	'8' => 'August',
	'9' => 'September',
	'10' => 'October',
	'11' => 'November',
	'12' => 'December',
);



$CODE['mobile'] = array(
	'010' => '010',
	'012' => '012',
	'013' => '013',
	'014' => '014',
	'016' => '016',
	'017' => '017',
	'018' => '018',
	'019' => '019',
);

$CONF['c_code'] = '6';

							
$CODE['dob_year'] = array();
{
	//
	// From day 1904 to day 2005
	//
	foreach(range(2011, 2100) as $y){
		$CODE['dob_year'][$y] = $y;
	}
}
//
// Malaysia State
//
$CODE['state'] = array(

'1'            => 'Penang',
'2'             => 'Johor',
'3'             => 'Kedah',
'4'          => 'Kelantan',
'5'       => 'Kuala Lumpur',
'6'       => 'Putra Jaya',
'7'            => 'Melaka',
'8'   => 'Negeri Sembilan',
'9'            => 'Pahang',
'10'             => 'Perak',
'11'            => 'Perlis',
'12'             => 'Sabah',
'13'           => 'Sarawak',
'14'          => 'Selangor',
'15'        => 'Terengganu',
'16'          => 'Labuan'
);


$CODE['state_real'] = array(

'Penang'            => 'Penang',
'Johor'             => 'Johor',
'Kedah'             => 'Kedah',
'Kelantan'          => 'Kelantan',
'Kuala Lumpur'       => 'Kuala Lumpur',
'Putra Jaya'        => 'Putra Jaya',
'Melaka'            => 'Melaka',
'Negeri Sembilan'   => 'Negeri Sembilan',
'Pahang'            => 'Pahang',
'Perak'             => 'Perak',
'Perlis'            => 'Perlis',
'Sabah'             => 'Sabah',
'Sarawak'           => 'Sarawak',
'Selangor'          => 'Selangor',
'Terengganu'        => 'Terengganu',
'Labuan'            => 'Labuan'
);

$CODE['shipping_agent'] = array('1'=>'Poslaju');



$CODE['country'] = array(


'2' =>'Afghanistan ',
'3' =>'Algeria ',
'4' =>'American Samoa ',
'5' =>'Andorra ',
'6' =>'Angola ',
'7' =>'Anguilla ',
'8' =>'Antarctica ',
'9' =>'Antigua and Barbuda ',
'10' =>'Argentina ',
'11' =>'Armenia ',
'12' =>'Aruba ',
'13' =>'Ascension Island ',
'14' =>'Australia ',
'15' =>'Austria ',
'16' =>'Azerbaijan ',
'17' =>'Bahamas ',
'18' =>'Bahrain ',
'19' =>'Bangladesh ',
'20' =>'Barbados ',
'21' =>'Belarus ',
'22' =>'Belgium ',
'23' =>'Belize ',
'24' =>'Benin ',
'25' =>'Bermuda ',
'26' =>'Bhutan ',
'27' =>'Bolivia ',
'28' =>'Bosnia ',
'29' =>'Botswana ',
'30' =>'Bouvet Island ',
'31' =>'Brazil ',
'32' =>'British Indian Ocean Territory ',
'33' =>'Brunei / Brunei Darussalam ',
'34' =>'Bulgaria ',
'35' =>'Burkina Faso ',
'36' =>'Burundi ',
'37' =>'Cambodia ',
'38' =>'Cameroon ',
'39' =>'Canada ',
'40' =>'Cape Verde ',
'41' =>'Cayman Islands ',
'42' =>'Central African Republic ',
'43' =>'Chad ',
'44' =>'Chile ',
'45' =>'China ',
'46' =>'Christmas Island ',
'47' =>'Cocos (Keeling) Islands ',
'48' =>'Colombia ',
'49' =>'Comoros ',
'50' =>'Congo ',
'51' =>'Cook Islands ',
'52' =>'Costa Rica ',
'53' =>'Cote D\'Ivoire (Ivory Coast) ',
'54' =>'Croatia(Hrvatska) ',
'55' =>'Cuba ',
'56' =>'Cyprus ',
'57' =>'Czech Republic ',
'58' =>'Czechoslovakia (former) ',
'59' =>'Denmack ',
'60' =>'Djibouti ',
'61' =>'Dominica ',
'62' =>'Dominican Republic ',
'63' =>'East Africa ',
'64' =>'East Timor ',
'65' =>'Ecuador ',
'66' =>'Egypt ',
'67' =>'El Salvador ',
'68' =>'Equatorial Guinea ',
'69' =>'Eritrea ',
'70' =>'Eslovaquia ',
'71' =>'Estonia ',
'72' =>'Ethiopia ',
'73' =>'Falkland Islands (Malvinas) ',
'74' =>'Faroe Islands ',
'75' =>'Fiji ',
'76' =>'Finland ',
'77' =>'France ',
'78' =>'France, Metropolitan (European Territory) ',
'79' =>'French Guiana ',
'80' =>'French Polynesia ',
'81' =>'French Southern Territories ',
'82' =>'Gabon ',
'83' =>'Gambia ',
'84' =>'Georgia ',
'85' =>'Germany ',
'86' =>'Ghana ',
'87' =>'Gibraltar ',
'88' =>'Great Britain ',
'89' =>'Greece ',
'90' =>'Greenland ',
'91' =>'Grenada ',
'92' =>'Guadeloupe ',
'93' =>'Guam ',
'94' =>'Guatemala ',
'95' =>'Guernsey Islands ',
'96' =>'Guinea ',
'97' =>'Guinea Bissau ',
'98' =>'Guyana ',
'99' =>'Haiti ',
'100' =>'Heard and McDonald Islands ',
'101' =>'Hong Kong ',
'102' =>'Honduras ',
'103' =>'Hungary ',
'104' =>'Iceland ',
'105' =>'India ',
'106' =>'Indonesia ',
'107' =>'Iran ',
'108' =>'Iraq ',
'109' =>'Ireland ',
'110' =>'Israel ',
'111' =>'Italy ',
'112' =>'Jamaica ',
'113' =>'Japan ',
'114' =>'Jersey Island ',
'115' =>'Jordan ',
'116' =>'Kazakhstan ',
'117' =>'Kenya ',
'118' =>'Kiribati ',
'119' =>'Korea ',
'120' =>'Kuwait ',
'121' =>'Kyrgyzstan ',
'122' =>'Laos ',
'123' =>'Latvia ',
'124' =>'Lebanon ',
'125' =>'Lesotho ',
'126' =>'Letonia ',
'127' =>'Liberia ',
'128' =>'Libya ',
'129' =>'Liechtenstein ',
'130' =>'Lithuania ',
'131' =>'Luxembourg ',
'132' =>'Macau ',
'133' =>'Macedonia ',
'134' =>'Madagascar ',
'135' =>'Malawi ',
'1'   =>'Malaysia',
'137' =>'Maldives ',
'138' =>'Mali ',
'139' =>'Malta ',
'140' =>'macshall Islands ',
'141' =>'mactinique ',
'142' =>'Mauritania ',
'143' =>'Mauritius ',
'144' =>'Mexico ',
'145' =>'Micronesia ',
'146' =>'Moldova ',
'147' =>'Monaco ',
'148' =>'Mongolia ',
'149' =>'Montserrat ',
'150' =>'Morocco ',
'151' =>'Mozambique ',
'152' =>'Myanmac ',
'153' =>'Namibia ',
'154' =>'Nauru ',
'155' =>'Nepal ',
'156' =>'Netherlands ',
'157' =>'Netherlands Antilles ',
'158' =>'Neutral Zone ',
'159' =>'New Caledonia ',
'160' =>'New Zealand ',
'161' =>'Nicaragua ',
'162' =>'Niger ',
'163' =>'Nigeria ',
'164' =>'Niue ',
'165' =>'Norfolk Island ',
'166' =>'North Africa ',
'167' =>'North Korea ',
'168' =>'Northern maciana Islands ',
'169' =>'Norway ',
'170' =>'Oman ',
'171' =>'Pakistan ',
'172' =>'Palau ',
'173' =>'Panama ',
'174' =>'Papua New Guinea ',
'175' =>'Paraguay ',
'176' =>'Peru ',
'177' =>'Philippines ',
'178' =>'Pitcairn ',
'179' =>'Poland ',
'180' =>'Portugal ',
'181' =>'Puerto Rico ',
'182' =>'Qatar ',
'183' =>'Reunion ',
'184' =>'Romania ',
'185' =>'Russian Federation ',
'186' =>'Rwanda ',
'187' =>'S. Georgia & S. Sandwich Isls. ',
'188' =>'Saint Helena ',
'189' =>'Saint Kitts and Nevis ',
'190' =>'Saint Lucia ',
'191' =>'Saint Pierre and Miquelon ',
'192' =>'Saint Vincent and the Grenadines ',
'193' =>'Samoa ',
'194' =>'San macino ',
'195' =>'Sao Tome and Principe ',
'196' =>'Saudi Arabia ',
'197' =>'Senegal ',
'198' =>'Serbia Montenegro ',
'199' =>'Seychelles ',
'200' =>'Sierra Leone ',
'201' =>'Singapore ',
'202' =>'Slovakia ',
'203' =>'Slovenia ',
'204' =>'Solomon Islands ',
'205' =>'Somalia ',
'206' =>'South Africa ',
'207' =>'Spain ',
'208' =>'Sri Lanka ',
'209' =>'Sudan ',
'210' =>'Suriname ',
'211' =>'Svalbard and Jan Mayen Islands ',
'212' =>'Swaziland ',
'213' =>'Sweden ',
'214' =>'Switzerland ',
'215' =>'Syria ',
'216' =>'Taiwan ',
'217' =>'Tajikistan ',
'218' =>'Tanzania ',
'219' =>'Thailand ',
'220' =>'The Isle of Man ',
'221' =>'Togo ',
'222' =>'Tokelau ',
'223' =>'Tonga ',
'224' =>'Trinidad and Tobago ',
'225' =>'Tunisia ',
'226' =>'Turkey ',
'227' =>'Turkmenistan ',
'228' =>'Turks and Caicos Islands ',
'229' =>'Tuvalu ',
'230' =>'U.S.S.R. / Soviet Union (former) ',
'231' =>'Uganda ',
'232' =>'Ukraine ',
'233' =>'United Arab Emirates ',
'234' =>'United Kingdom ',
'235' =>'United States Minor Outlying Islands ',
'236' =>'United States of America ',
'237' =>'Uruguay ',
'238' =>'Uzbekistan ',
'239' =>'Vanuatu ',
'240' =>'Vatican City State(Holy See) ',
'241' =>'Venezuela ',
'242' =>'Vietnam ',
'243' =>'Virgin Islands (UK) ',
'244' =>'Virgin Islands (US) ',
'245' =>'Wallis and Futuna Islands ',
'246' =>'Western Sahara ',
'247' =>'Yemen ',
'248' =>'Yugoslavia ',
'249' =>'Zaire ',
'250' =>'Zambia ',
'251' =>'Zimbabwe '


);

//
// Cfoni Admin Setting Mail - > do forworded later
//
$CONF['administration_email'] = 'serverreport@cfoni.com';
$CONF['url_redirect_blocked'] = 'http://www.yahoo.com'; // URL After blocked
$CONF['url_download_pbx']     = 'http://counterpath.s3.amazonaws.com/downloads/X-Lite_Win32_4.5.5._71236.exe';
$CONF['url_check_domain']     = 'https://my.webnic.cc/jsp/pn_qry.jsp?source=webcc-0327255610&domain=';
//
// Linking URL
//
$CONF['url_facebook'] = 'https://www.facebook.com/pages/Cfoni-Technologies-Sdn-Bhd/435982113177783';
$CONF['url_twitter'] ="https://twitter.com/cfoni";

$CODE['status']       = array('1'=>'Active','2'=>'Pending','3' => 'Suspend','4'=>'Inactive','5'=>'Delete');
$CODE['status_add']   = array('1'=>'Active','4'=>'Inactive');

$CODE['salesman_type'] = array('1' => 'Indoor','2'=>'Outdoor'); 
$CODE['phone_type']    = array('1' => 'Normal','2'=>'Special'); 
$CODE['vip_status']    = array('1' => 'No','2'=>'Yes'); 
$CODE['prefix_salesman']  = array('1' => 'IN','2'=>'EX');  // 1.Internal 2.External

$CONF['add_special_cost'] = "38.00";
$CODE['customer_type'] = array('1'=>'Personal','2'=>'Company');


$CODE['reseller_type'] = array('1'=>'New','2'=>'Existing Customer');
$CODE['faq_type']      = array('1'=>'General','2'=>'Account','3'=>'Payment');
//
// Image Link
//
$CONF['url_pic_direct'] = HTTP_SERVER.'design/'.$CONF['tpl_name'].'/';



		    
$CONF['strToEmail'] = "reeve@webapplicationz.com";
$CONF['strCCEmail'] = "reeve@xbugs.net";

$CONF['strComToEmail'] = "reeve@webapplicationz.com";
$CONF['strCompCCEmail'] = "reeve@xbugs.net";

$CODE['type_answer'] = array('1'=>'A','2'=>'B','3'=>'C','4'=>'D');

$CODE['type_banner'] = array('1'=>'Big - 990(W) x 410 (H)','2'=>'Small - 276(W) x 245(H)');
$CODE['type_banner_status_nodelete'] = array('1'=>'Show','2'=>'Hide');
$CODE['type_banner_status'] = array('1'=>'Show','2'=>'Hide','3' => 'Delete');
$CODE['type_promotion'] = array('N'=>'No','Y'=>'Yes');
$CODE['type_section'] = array('1'=>'CEO MESSAGE','2'=>'VISION , MISSION & VALUE PROPOSITION','3'=>'COMPANY BACKGROUND','4'=> 'MILESTONES','5'=>'AWARDS & RECOGNITIONS');
$CODE['type_solution_section'] = array('1'=>'WHAT WE OFFERS','2'=>'WHAT OUR CUSTOMERS RECEIVE','3'=>'OUR SERVICES','4'=>'NETWORKING SOLUTION');
$CODE['url_status'] = array('N'=>'No','Y'=>'Yes');
$CODE['type_ppl_status']  = array('1'=>'Show','2'=>'Hide','3'=>'Delete');

$CODE['type_solutions'] = array('1'=>'WHAT WE OFFERS','2'=>'WHAT OUR CUSTOMERS RECEIVE','3'=>'OUR SERVICES','4'=>'NETWORKING SOLUTION');

?>