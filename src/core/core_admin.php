<?php

class core_admin extends Record{
	function __construct(){
		$this->db = new DB();
		}
	
	//
	// Do Delete FAQ
	//
	function doDeleteFAQById($id,$strAddedBy){
		global $CONF;
		
		global $CONF;
		// 5 = Delete
		$intStatus = 5;
		 $query ='UPDATE faq                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 strAddedBy       ="'.$strAddedBy.	'" ,      
		     intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intFaqId='.$id; 
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
	
	//
	// Do Update FAQ
	//
	function doUpdateFAQ($args){
		global $CONF;
		
		 $query ='UPDATE faq                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strQ	      ="'.$args['strQ'].'",   
			  strA	      ="'.$args['strA'].'",
			  intStatus	      ='.$args['intStatus'].',
			  intType	      ='.$args['intType'].',
		 	 intOrder         ='.$args['intOrder'].	' ,
			 strAddedBy    ="'.$args['strAddedBy'].'" 
       WHERE 
      			intFaqId='.$args['id']; 

	if ($this->query_id = $this->db->query($query))
		return 1;
	  else
		return 0;
		
		
	}
	
	
	//
	// Do Get FAQ Details By Id
	//
	function doGetFAQDetailsById($id){
		$query = "SELECT * FROM faq WHERE intFaqId=".$id;
	
		if($this->query_id = $this->db->query($query))
		 return 1;
		else
	 	 return 0;	
		
	}
	
	
	//
	// Do Show All Active FAQ
	//
	function doShowAllFAQ($intPage=0){
		$query = "SELECT * FROM faq WHERE intStatus!=5 Order By CreateDate DESC ";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
		
	}
	//
	// Do Add FAQ
	//
	function doAddFAQ($args){
		global $CONF;
		
		$query = "INSERT INTO faq(intFaqId,intType,strQ,strA,intStatus,strAddedBy,CreateDate,UpdateDate) 
		VALUES(0,".$args['intType'].",'".$args['strQ']."','".$args['strA']."',".$args['intStatus'].",'".$args['strAddedBy']."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
	
	  if ($this->query_id = $this->db->query($query))
		return 1;
	  else
		return 0;
			
		
	}
	
	
	//
	// Do Update SalesmanCode
	//
	function doUpdateCustomerSalesmanCode($strSalesCode,$intCustomerId){
		global $CONF;
		
		 $query ='UPDATE customer                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strIntroduceResellerCode	      =0,    
		 	 strSalesmanCode         ="'.$strSalesCode.	'" 
       WHERE 
      			intCustomerId='.$intCustomerId; 

	if ($this->query_id = $this->db->query($query))
		return 1;
	  else
		return 0;
	}
	//
	// Do Get All Active Salesman
	//
	function doGetSalesmanActive(){
		
		$query = "SELECT strSalesmanCode,intSalesId,strUser,intType FROM salesman WHERE intStatus=1 Order By CreateDate DESC ";
		
			
	  if ($this->query_id = $this->db->query($query))
		return 1;
	  else
		return 0;
	}
	
	
	//
	// Do Add Account Manager
	//
	function doAddNewAccManager($args){
		global $CONF;
		
		$query = "INSERT INTO accountmanager(intAccountManagerId,strFullName,strContact,strEmail,strUser,strPass,intStatus,intAutoEmailInfor,strAddedBy,CreateDate,UpdateDate) 
		VALUES(0,'".$args['strFullName']."','".$args['strContact']."','".$args['strEmail']."','".$args['strUsername']."','".md5($args['strPassword'])."',1,".$args['intAutoEmail'].",'".$args['strAddedBy']."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 	
	  if ($this->query_id = $this->db->query($query))
		return 1;
	  else
		return 0;
		
	}
	
	//
	// Do validation User Name
	//
	function doCheckUsername($strUsername){
		$query = "SELECT * FROM accountmanager WHERE strUser='".$strUsername."' ";
	
		if($this->query_id = $this->db->query($query))
		 return 1;
		else
	 	 return 0;
	}
	//
	// Do Show All Account Manager
	//
	function doShowAllAccManager($intPage=0){
		
		$query = "SELECT * FROM accountmanager WHERE intStatus!=5 Order By CreateDate DESC ";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
		
	}
	//
	// Get Account Manager By Id
	//
	function doGetAccManagerDetailsById($id){
		$query = "SELECT * FROM accountmanager WHERE intAccountManagerId=".$id;
		
		if($this->query_id = $this->db->query($query))
		 return 1;
		else
	 	 return 0;
		
	}
	
	//
	// Do Edit Account Manager
	//
	function doEditAccManager($args){
	 global $CONF;
	
	if($args['strPassword']==""){	
    $query ='UPDATE accountmanager                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strFullName      ="'.$args['strFullName'].	'",    
		 	 strEmail         ="'.$args['strEmail'].	'",                                
		 	 strContact       ="'.$args['strContact'].	'" ,                                  
 		  	 strAddedBy       ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus        ='.$args['intStatus'].	'                                      
       WHERE 
      		intAccountManagerId='.$args['id']; 
	}
	else{
		 $query ='UPDATE accountmanager                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strFullName      ="'.$args['strFullName'].	'",    
		 	 strEmail         ="'.$args['strEmail'].	'",                                
		 	 strContact       ="'.$args['strContact'].	'" ,                                  
 		  	 strAddedBy       ="'.$args['strAddedBy'].	'" ,      
			  strPass      ="'.md5($args['strPassword']).	'" ,                             
		     intStatus        ='.$args['intStatus'].	'                                      
       WHERE 
      		intAccountManagerId='.$args['id']; 
		
		
	}
	
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
	
	//
	// Do Delete Account Manager
	//
	function doDeleteAccManagerById($id,$strAddedBy){
		global $CONF;
		// 5 = Delete
		$intStatus = 5;
		 $query ='UPDATE accountmanager                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 strAddedBy       ="'.$strAddedBy.	'" ,      
		     intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intAccountManagerId='.$id; 
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Do Check Product Name
	//
	function doCheckProductName($strProductName){
		$query = "SELECT * FROM product WHERE strProductName='".$strProductName."' ";
	
		if($this->query_id = $this->db->query($query))
		 return 1;
		else
	 	 return 0;
		
		
	}
	//
	// Do Add Product
	//
	function doAddProduct($args){
		global $CONF;
		
		if(trim($args['strDescription'])==""){
			$args['strDescription']= '-';
		}
		$query = "INSERT INTO product(intProductId,strProductName,strDescription,intOrder,strAddedBy,intStatus,CreateDate,UpdateDate) 
		VALUES(0,'".$args['strProductName']."','".$args['strDescription']."',".$args['intOrder'].",'".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
		
	  if ($this->query_id = $this->db->query($query))
		return 1;
	  else
		return 0;
	}
	//
	// Show all Product
	//
	function doShowAllProduct($intPage=0){
		
		$query = "SELECT * FROM product WHERE intStatus!=5 Order By CreateDate DESC ";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
		
		
	}
	//
	// Do Get the product datails by Id
	//
	function doGetProductDetailsById($id){
		
		$query = "SELECT * FROM product WHERE intProductId=".$id;
		
		if($this->query_id = $this->db->query($query))
		 return 1;
		else
	 	 return 0;
		
	}
	
	//
	// Update Product
	//
	function doUpdateProduct($args){
		global $CONF;
		// 5 = Delete
		$intStatus = 5;
		 $query ='UPDATE product                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 strProductName   = "'.$args['strProductName'].'",
			 strDescription   = "'.$args['strDescription'].'",
			 intOrder         = '.$args['intOrder'].',
			 strAddedBy       ="'.$args['strAddedBy'].	'" ,      
		     intStatus        ='.$args['intStatus'].	'                                      
       WHERE 
      		intProductId='.$args['id']; 
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Do Delete Product
	//
	function doDeleteProductById($id,$strAddedBy){
		global $CONF;
		// 5 = Delete
		$intStatus = 5;
		 $query ='UPDATE product                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
			 strAddedBy       ="'.$strAddedBy.	'" ,      
		     intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intProductId='.$id; 
			
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Show All Package
	//
	function doShowAllPackage($intPage=0){
		$query = "SELECT * FROM package WHERE intStatus!=5 Order By CreateDate DESC ";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
		
	}
	
	//
	// Check Package Name
	//
	function doCheckPackageName($strPackageName){
		$query = "SELECT * FROM package WHERE strPackageName='".$strPackageName."' ";
	
		if($this->query_id = $this->db->query($query))
		 return 1;
		else
	 	 return 0;
		
	}
	//
	// Do Add Package 
	//
	function doAddPackage($args){
		global $CONF;
		
		if(trim($args['strDescription'])==""){
			$args['strDescription']= '-';
		}
		if(trim($args['strFeatures'])==""){
		  $args['strFeatures'] = '-';	
		}
		$strImage = "-";
		$query = "INSERT INTO package(intPackageId,strPackageName,strImage,strFeature,strDescription,strPrice,intOrder,intStatus,strAddedBy,CreateDate,UpdateDate) 
		VALUES(0,'".$args['strPackageName']."','".$strImage."','".$args['strFeatures']."','".$args['strDescription']."','".$args['strPrice']."',".$args['intOrder'].",".$args['intStatus'].",'".$args['strAddedBy']."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
		
	
	  if ($this->query_id = $this->db->query($query)){
	    $id = mysql_insert_id();
		return $id;
	  }
	  else{
		return 0;
	  }
		
	}
	//
	// Update Package Image
	//
	function doInsertUpdatePackageImage($id,$arrfilename){
		global $CONF;
			
		 $query ='UPDATE package                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
			 strImage       ="'.$arrfilename.	'" 	                               
      	 WHERE 
      		intPackageId='.$id; 
			
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	
	//
	// Do Get the package datails by Id
	//
	function doGetPackageDetailsById($id){
		
		$query = "SELECT * FROM package WHERE intPackageId=".$id;
		
		if($this->query_id = $this->db->query($query))
		 return 1;
		else
	 	 return 0;
		
	}
	//
	// Update Package
	//
	function doUpdatePackage($args){
		global $CONF;
		
		 $query ='UPDATE package                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 strPackageName   = "'.$args['strPackageName'].'",
			 strFeature       = "'.$args['strFeatures'].'",
			 strDescription   = "'.$args['strDescription'].'",
			 intOrder         = '.$args['intOrder'].',
			 strPrice         = "'.$args['strPrice'].'",
			 strAddedBy       ="'.$args['strAddedBy'].	'" ,      
		     intStatus        ='.$args['intStatus'].	'                                      
       WHERE 
      		intPackageId='.$args['id']; 
			
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	
	//
	// Do Delete Package
	//
	function doDeletePackageById($id,$strAddedBy){
		global $CONF;
		// 5 = Delete
		$intStatus = 5;
		 $query ='UPDATE package                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
			 strAddedBy       ="'.$strAddedBy.	'" ,      
		     intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intPackageId='.$id; 
			
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	
	//
	// Do Get All Salesman
	//
	function doShowAllSalesman($intPage=0){
		
		$query = "SELECT * FROM salesman WHERE intStatus!=5 Order By CreateDate DESC ";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
	//
	// Do Add Phone No.
	//
	function doAddBulkPhone($arrData,$intPhoneType,$strPrice,$strBatchNo ,$strAddedBy ){
	  global $CONF;
	  $intStatus  = 1;
	  	
		foreach($arrData as $key => $PhoneNo){
		
			$query = "INSERT INTO number_did_fax(intDidFaxId,strBatch,strNumber,intType,strPrice,intStatus,strAddedBy,CreateDate,UpdateDate) 
		      VALUES(0,'".$strBatchNo."','".$PhoneNo."',".$intPhoneType.",'".$strPrice."',".$intStatus.",'".$strAddedBy."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
			if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			}
		 	return 1;	
	}
	//
	// Show All Phone No.
	//
	function doShowAllPhone($intPage=0){
		
		$query = "SELECT * FROM number_did_fax WHERE intStatus!=5 Order By CreateDate DESC ";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
	//
	// Do Delete Phone No.
	//
	function doDeletePhoneById($id,$strAddedBy){
			global $CONF;
		// 5 = Delete
		$intStatus = 5;
		 $query ='UPDATE number_did_fax                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 strAddedBy       ="'.$strAddedBy.	'" ,      
		     intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intDidFaxId='.$id; 
			
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Do Change Status Salesman
	//
	function doSalesmanStatusById($id,$intStatus){
		global $CONF;
		
		 $query ='UPDATE salesman                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	   intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intSalesId='.$id; 
	
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Get All Details From Account Manager
	//
	function doGetAllListAccManager(){
		
		$query = "SELECT * FROM accountmanager";
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
	
	//
	// Do Change Status Reseller
	//
	function doResellerStatusById($id,$intStatus){
		global $CONF;
		$query ='UPDATE reseller                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	   intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intResellerId='.$id; 
	
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
	//
	// Update Customer Status
	//
	function doCustomerStatusById($id,$intStatus){
		global $CONF;
		
		$query ='UPDATE customer                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	   intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intCustomerId='.$id; 
	
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
	//
	// Get All Reseller
	//
	function getAllReseller($intPage=0){
		$query ="SELECT * FROM reseller  Group By strSalesmanCode Order By CreateDate DESC ";
	
	if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
	//
	//Get Customer Details
	//
	function getAllCustomer($intPage=0){
		$query ="SELECT * FROM customer";
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
	
	//
	//Get Customer Details
	//
	function doGetAllCustomers(){
		$query ="SELECT * FROM customer";
		
	if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
	
	
	
	
	/******************************************** End Cfoni **********************************************************/
	
	
	//
	// Update Ppl information
	//
	function doUpdatePeople($intBrandId,$strStuffName,$strEmail,$intStatus ,$strAddedBy,$id ){
		global $CONF;
		
    $query ='UPDATE people                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strName          ="'.$strStuffName.	'",    
		 	  strEmail        ="'.$strEmail.	'",                                
		 	 intBrandId       ='.$intBrandId.	' ,                                  
 		  	 strAddedBy        ="'.$strAddedBy.	'" ,                                  
		     intStatus         ='.$intStatus.	'                                      
       WHERE 
      		intPeopleId='.$id; 
	 
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	
	//
	// Get Stuff By Id
	//
	function getAllStuffById($id){
		
		   $query = 'SELECT * FROM people WHERE intPeopleId='.$id;	
		
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
		
		
	}
	//
	// Get aLL pEOPLE
	//
	function getAllStuff(){
	  
	   $query = 'SELECT * FROM people WHERE intStatus!=3 order by CreateDate DESC';	
		
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
		
	}
	
		
	//
	// Do Add people
	//
	function doAddPeople($intBrandId,$strStuffName,$strEmail,$intStatus ,$strAddedBy ){
		global $CONF;
	 	 	
	 	$query = "INSERT INTO people(intPeopleId,intBrandId,strName,strEmail,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,".$intBrandId.",'".$strStuffName."','".$strEmail."','".$strAddedBy ."',".$intStatus.",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 	
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
		
		
	}
	
	
	//
	// Do validation SubCategory Name
	//
	function doCheckSubcategoryName($strSubCategoryName){
		  $query = "SELECT * FROM subcategory WHERE strSubCategoryName='".$strSubCategoryName."' ";
	 	
				
		if($this->query_id = $this->db->query($query)){
						
				return 1;
			}
			else{
				return 0;
			}
	
	
	}
	
	//
	// Do validation Category Name
	//
	function doCheckCategoryName($strCategoryName){
		  $query = "SELECT * FROM category WHERE strCategoryName='".$strCategoryName."' ";
	 	
				
		if($this->query_id = $this->db->query($query)){
						
				return 1;
			}
			else{
				return 0;
			}
	
	
	}	
		
	//
	// Do validation Campaign Name
	//
	function doCheckCampaignName($strTitleCampaign){
		  $query = "SELECT * FROM campaign WHERE strTitle='".$strTitleCampaign."' ";
	 	
				
		if($this->query_id = $this->db->query($query)){
						
				return 1;
			}
			else{
				return 0;
			}
	
	
	}	
 	
	//
	// Do validation Brand Name
	//
	function doCheckBrandName($strBrandName){
		  $query = "SELECT * FROM  brand WHERE strBrandName='".$strBrandName."' ";
	 	
				
		if($this->query_id = $this->db->query($query)){
						
				return 1;
			}
			else{
				return 0;
			}
	
	
	}
	
	
	
	
	
	
	//
	// Do validation Product Name
	//
	function doCheckProductName1($strProductName){
		  $query = "SELECT * FROM  product WHERE strProductName='".$strProductName."' ";
	 	
				
		if($this->query_id = $this->db->query($query)){
						
				return 1;
			}
			else{
				return 0;
			}
	
	
	}
	
	
	
	//
	// Do validation Network Title
	//
	function doCheckNetworkPhotoName($strTitle){
		  $query = "SELECT * FROM  network_photo WHERE strTitle='".$strTitle."' ";
	 	
				
		if($this->query_id = $this->db->query($query)){
						
				return 1;
			}
			else{
				return 0;
			}
	
	
	}
	
	
	//
	// Get All Answer for Q
	//
	function doGetAnswerSelect($intQuestionId,$intCampaignId){
	  $query = "SELECT * FROM campaign_answer_selection WHERE intQuestionId=".$intQuestionId." AND intCampaignId=".$intCampaignId." Order By intOrder ASC";
	 	
				
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return 1;
			}
			else{
				return 0;
			}	
	
	
	}
	
	//
	// Get All Message link
	//
	function getAllMessage(){
	  
	   $query="SELECT * FROM message_link Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return 1;
			}
			else{
				return 0;
			}	
	
	}
	
	//
	// Get All Message Photo
	//
	function getAllMessagePhoto(){
	  
	   $query="SELECT * FROM message_photo Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	//
	// Get All Message Photo : Solution
	//
	function getAllMessagePhotoSolution(){
	  
	   $query="SELECT * FROM message_photo_solution Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	//
	// Get All Network Photo
	//
	function getAllNetworkPhoto(){
	  
	   $query="SELECT * FROM network_photo Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	
	//
	// Get All Manage Product
	//
	function getAllManageProduct(){
	  
	   $query="SELECT * FROM  product WHERE intStatus!=3 Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	
	//
	// Get All Campaign
	//
	function getAllCampaign(){
	  
	   $query="SELECT * FROM  campaign Order By UpdateDate DESC" ;
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	//
	// Get All Question
	//
	function getAllQuestionByCampaignId($intCampaignId){
	  
	   $query="SELECT * FROM  campaign_question WHERE intCampaignId =".$intCampaignId." Order By UpdateDate DESC" ;
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	//
	// Get All Answer Selected
	//
	function getAllAnswerSelected($intQuestionId,$intCampaignId){
	  
	   $query="SELECT * FROM  campaign_answer_selection WHERE intQuestionId=".$intQuestionId." AND intCampaignId=".$intCampaignId." Order By UpdateDate DESC" ;
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	
	//
	// Get All Manage Information
	//
	function getAllManageInform(){
	  
	   $query="SELECT * FROM  contactus Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	//
	// Get All Brand
	//
	function getAllBrand(){
	  
	   $query="SELECT * FROM  brand Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	//
	// Get All Manage Category
	//
	function getAllManageCategory(){
	  
	   $query="SELECT * FROM   category Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	//
	// Get AllSub
	//
	function getAllSubCate(){
	  $query="SELECT * FROM   subcategory Order By UpdateDate DESC";
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return 1;
			}
			else{
				return 0;
			}	
	}
	
	//
	// Get All Sub Category
	//
	function getAllSubcategory($intId){
	   $query="SELECT * FROM   subcategory WHERE intCategoryId=".$intId." Order By UpdateDate DESC";
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	//
	// Get All Feedback
	//
	function getAllFeedback(){
	  
	   $query="SELECT * FROM  feedback Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	
	//
	// Get All Customer
	//
	function getAllCustomerReport(){
	  
	   $query="SELECT * FROM  campaign_customer Order By UpdateDate DESC";
	
			
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
	}
	
	
 	//
	// Add message link
	//
 	function doAddMessageLink($args){
		global $CONF;
	 	 	
	 	$query = "INSERT INTO message_link(intMsgId,strMessage,strLink,intStatus,strAddedBy,CreateDate,UpdateDate)
	 	VALUES(0,'".$args['strMessage']."','".$args['strLink']."',".$args['intStatus'].",'".$args['strAddedBy']."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 	
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Do Update Message Link
	//
	function doUpdateMessageLink($args){
		global $CONF;
		
  $query ='UPDATE message_link                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strMessage       ="'.$args['strMessage'].	'",                                 
		 	 strLink           ="'.$args['strLink'].	'" ,                                  
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intMsgId='.$args['id']; 
	 
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	
	
	//
	// Add feedback
	//
 	function doAddFeedback($args){
		global $CONF;
	 	 	
	 	$query = "INSERT INTO feedback(intFeedbackId,strName,strCompanyName,strEmail,strTel,strFax,strWebsite,strMessage,intStatus,strAddedBy,CreateDate,UpdateDate)
	 	VALUES(0,'".$args['strName']."','".$args['strCompanyName']."','".$args['strEmail']."','".$args['strTel']."','".$args['strFax']."','".$args['strWebsite']."','".$args['strMessage']."',".$args['intStatus'].",'".$args['strAddedBy']."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 	
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Do Update Feedback
	//
	function doUpdateFeedback($args){
		global $CONF;
		
  $query ='UPDATE feedback                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strName       ="'.$args['strName'].	'",
			 strCompanyName       ="'.$args['strCompanyName'].	'",
			 strEmail       ="'.$args['strEmail'].	'",
			 strTel       ="'.$args['strTel'].	'",
			 strFax       ="'.$args['strFax'].	'",
			 strWebsite       ="'.$args['strWebsite'].	'",
			 strMessage       ="'.$args['strMessage'].	'",                            
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intFeedbackId='.$args['id']; 
	 
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	
	//
	// Add Customer
	//
 	function doAddCustomerReport($args){
		global $CONF;
	 	 	
	 	$query = "INSERT INTO campaign_customer(intCustomerId,strName,strCompanyName,strEmail,strTel,CreateDate,UpdateDate)
	 	VALUES(0,'".$args['strName']."','".$args['strCompanyName']."','".$args['strEmail']."','".$args['strTel']."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 	
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Do Update Customer
	//
	function doUpdateCustomerReport($args){
		global $CONF;
		
  $query ='UPDATE campaign_customer                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strName       ="'.$args['strName'].	'",
			 strCompanyName       ="'.$args['strCompanyName'].	'",
			 strEmail       ="'.$args['strEmail'].	'",
			 strTel       ="'.$args['strTel'].	'"                        
                                    
       WHERE 
      		intCustomerId='.$args['id']; 
	 
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	
	//
	// Add Main Banner
	//
 	function doAddmainbanner($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO banner(intBannerId,intTypeBanner,strBanner,strLink,strCaption,intOrder,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,".$args['intBannerType'].",'".$photo."','".$args['strBannerLink']."','".$args['strBannerCaption']."','".$args['intBannerOrder']."','".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	//
	// Updata Banner Photo
	//
	function doInsertUpdateBanner($id,$arrfilename){
	 global $CONF;
	
	$query = 'UPDATE  banner SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strBanner="'.$arrfilename.'" 
	 
	 WHERE intBannerId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	//
	// Add Message Photo doAddMesagePhoto
	//
 	function doAddMessagePhoto($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO message_photo(intMshCeoId,intSection,strMessage,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,".$args['intSection'].",'".$args['strDescription']."','".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	}
	//
	// Do Update Message Photo
	//
	function doUpdateMessagePhoto($args){
		global $CONF;
		
  $query ='UPDATE message_photo                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 intSection       ='.$args['intSection'].	' ,                                 
		 	 strMessage       ="'.$args['strMessage'].	'" ,                                  
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intMshCeoId='.$args['id']; 
	 
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	
	
	//
	// Add Message Photo doAddMesagePhoto Solutions
	//
 	function doAddMessagePhotoSolutions($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO message_photo_solution(intSolutionId,intSection,strMessage,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,".$args['intSection'].",'".$args['strDescription']."','".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	}
	//
	// Do Update Message Photo Solutions
	//
	function doUpdateMessagePhotoSolutions($args){
		global $CONF;
		
  $query ='UPDATE message_photo_solution                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 intSection       ='.$args['intSection'].	' ,                                 
		 	 strMessage       ="'.$args['strMessage'].	'" ,                                  
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intSolutionId='.$args['id']; 
	 
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	
	
	
	
	//
	// Add manage product
	//
 	function doAddManageProduct($args){
		global $CONF;
		
	 	$photo="-"; 
		
		if(trim($args['strSmallDescription'])==""){
			$args['strSmallDescription']= '-';
		}
		
		
		
		
			
	 	$query = "INSERT INTO product(intProductId,strProductName,intBrandId,intCategoryId,intSubcategoryId,strPhoto,strSmallDescription,strDescription,strURLStatus,strURL,intStatus,strAddedBy,CreateDate,UpdateDate)
	 	VALUES(0,'".$args['strProductName']."',".$args['intBrandId'].",".$args['intCategoryId'].",".$args['intSubcategoryId'].",'".$photo."','".$args['strSmallDescription']."','".$args['strDescription']."','".$args['strURLStatus']."','".$args['strURL']."',".$args['intStatus'].",'".$args['strAddedBy']."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
	  
	 
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Updata Manage Product
	//
	function doUpdateManageProduct($args){
	 global $CONF;

 $a = explode("::",$args['strURLStatus']);

	$query ='UPDATE product                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strProductName       ="'.$args['strProductName'].	'",                                 
		 	 
			 strSmallDescription           ="'.$args['strSmallDescription'].'" ,
			 strDescription           ="'.$args['strDescription'].	'" , 
			 intBrandId         ='.$args['intBrandId'].',
			 intCategoryId         ='.$args['intCategoryId'].',
			 intSubcategoryId         ='.$args['intSubcategoryId'].' ,                                
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,   
 		  	 strURLStatus        ="'.$a[0].	'" ,  
 		  	 strURL        ="'.$args['strURL'].	'" ,                                 
		     intStatus         ='.$args['intStatus'].'                                      
       WHERE 
      		intProductId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Updata Product Photo
	//
	function doInsertUpdateManageProduct($id,$arrfilename){
	 global $CONF;
	
	$query = 'UPDATE  product SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strPhoto="'.$arrfilename.'" 
	 
	 WHERE intProductId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Add network photo
	//
 	function doAddNetworkPhoto($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO network_photo(intNetworkId,strTitle,strMessage,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,'".$args['strTitle']."','".$photo."','".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	//
	// Updata Network Photo
	//
	function doInsertUpdateNetworkPhoto($args){
	 global $CONF;

	$query ='UPDATE network_photo                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strTitle       ="'.$args['strTitle'].	'",                                                                  
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].'                                      
       WHERE 
      		intNetworkId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Update Image
	//
	function doInsertUpdateNetworkImage($id,$arrfilename){
	
	$query ='UPDATE network_photo                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strMessage       ="'.$arrfilename.	'"                                                               
 		  	                              
       WHERE 
      		intNetworkId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	
	
	//
	// Add manage information
	//
 	function doAddManageInformation($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO contactus(intContactUsId,strTitle,strMapPhoto,strAdd,strTel,strFax,strEmail,strGoogleMap,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,'".$args['strTitleinform']."','".$photo."','".$args['strDescription']."','".$args['strTelefon']."','".$args['strFax']."','".$args['strEmail']."','".$args['strGoogleLink']."','".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Updata Manage Information
	//
	function doUpdateManageInformation($args){
	 global $CONF;

	$query ='UPDATE contactus                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strTitle       ="'.$args['strTitleinform'].	'",                                 
		 	 
			 strAdd           ="'.$args['strDescription'].	'" ,
			 strTel           ="'.$args['strTelefon'].	'" , 
			 strFax           ="'.$args['strFax'].	'" ,
			 strEmail           ="'.$args['strEmail'].	'" ,    
			  strGoogleLink           ="'.$args['strGoogleLink'].	'" ,                              
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intContactUsId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Updata Map Photo
	//
	function doInsertUpdateManageInformation($id,$arrfilename){
	 global $CONF;
	
	$query = 'UPDATE  contactus SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strMapPhoto="'.$arrfilename.'" 
	 
	 WHERE intContactUsId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Add Campaign
	//
 	function doAddCampaign($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO campaign(intCampaignId,strTitle,strPic,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,'".$args['strTitleCampaign']."','".$photo."','".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Updata Campaign
	//
	function doUpdateCampaign($args){
	 global $CONF;

	$query ='UPDATE campaign                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strTitle          ="'.$args['strTitleCampaign'].	'",                                                               
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intCampaignId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Updata Campaign Photo
	//
	function doInsertUpdateCampaign($id,$arrfilename){
	 global $CONF;
	
	$query = 'UPDATE  campaign SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strPic="'.$arrfilename.'" 
	 
	 WHERE intCampaignId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	
	
	//
	// Add Question
	//
 	function doAddQuestion($args){
		global $CONF;
	 	 	
			$photo="-";
			$intAnswer = "-";
	 	$query = "INSERT INTO campaign_question(intQuestionId,intCampaignId,strQuestion,strPic,intAnswer,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,".$args['intCampaignId'].",'".$args['strQuestion']."','".$photo."','".$intAnswer."','".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 

	 
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Updata Question
	//
	function doUpdateQuestion($args){
	 global $CONF;

	$query ='UPDATE campaign_question                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strQuestion          ="'.$args['strQuestion'].	'",                                                               
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,
			 intAnswer         ='.$args['intAnswer'].',                                  
		     intStatus         ='.$args['intStatus'].'                                      
       WHERE 
      		intQuestionId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Updata Question Photo
	//
	function doInsertUpdateQuestion($id,$arrfilename){
	 global $CONF;
	
	$query = 'UPDATE  campaign_question SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strPic="'.$arrfilename.'" 
	 
	 WHERE intQuestionId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	
	
	
	//
	// Add Answer Selected
	//
 	function doAddAnswerSelected($args){
		global $CONF;	 	 	
			
	 	$query = "INSERT INTO campaign_answer_selection(intAnswerSelectionId,intQuestionId,intCampaignId,strAnswer,intOrder,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,".$args['intQuestionId'].",
		".$args['intCampaignId'].",
		'".$args['strAnswer']."',
		".$args['intOrder'].",
		'".$args['strAddedBy']."',
		".$args['intStatus'].",
		'".timestr(time(),$CONF['const_gmt_offset'])."',
		'".timestr(time(),$CONF['const_gmt_offset'])."')";

	 
		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}	
	}
	
	//
	// Updata Answer Selected
	//
	function doUpdateAnswerSelected($args){
	 global $CONF;

	$query ='UPDATE campaign_answer_selection                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",
			 intOrder         ='.$args['intOrder'].',          
		 	 strAnswer          ="'.$args['strAnswer'].'",                                                               
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].'                                      
       WHERE 
      		intAnswerSelectionId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	//
	// Add Brand
	//
 	function doAddBrand($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO brand(intBrandId,intOrder,strBrandName,strPhoto,strDescription,strLongDesc,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,".$args['intBrandOrder'].",'".$args['strBrandName']."','".$photo."','".$args['strDescription']."','".$args['LongDescription']."','".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	

	}
	
	//
	// Updata Brand
	//
	function doUpdateBrand($args){
	 global $CONF;

	$query ='UPDATE brand                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'", 
			 intOrder         ='.$args['intOrder'].',
		 	 strBrandName       ="'.$args['strBrandName'].	'",                                  	 
			 strDescription           ="'.$args['strDescription'].	'" ,     
			 strLongDesc           ="'.$args['LongDesc'].	'" ,   
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,                                  
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intBrandId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	
	}
	//
	// Updata Brand Photo
	//
	function doInsertUpdateBrand($id,$arrfilename){
	 global $CONF;
	
	$query = 'UPDATE brand SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strPhoto="'.$arrfilename.'" 
	 
	 WHERE intBrandId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Add Category
	//
 	function doAddCategory($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO category(intCategoryId,strCategoryName,strPhoto,strDescription,intOrder,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,'".$args['strCategoryName']."','".$photo."','".$args['strDescription']."',".$args['intOrder'].",'".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Updata Category
	//
	function doUpdateManageCategory($args){
	 global $CONF;

	$query ='UPDATE category                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strCategoryName       ="'.$args['strCategoryName'].	'",                                  	 
			 strDescription           ="'.$args['strDescription'].	'" ,                              
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,      
			   intOrder         ='.$args['intOrder'].	' ,
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intCategoryId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Updata Category Photo
	//
	function doInsertUpdateCategory($id,$arrfilename){
	 global $CONF;
	
	$query = 'UPDATE  category SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strPhoto="'.$arrfilename.'" 
	 
	 WHERE intCategoryId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
	//
	// Add Subcategory
	//
 	function doAddSubcategory($args){
		global $CONF;
	 	 	
			$photo="-";
	 	$query = "INSERT INTO subcategory(intSubCategoryId,intCategoryId,strSubCategoryName,strPic,strDescription,strAddedBy,intOrder,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,".$args['intCateId'].",'".$args['strSubCategoryName']."','".$photo."','".$args['strDescription']."','".$args['strAddedBy']."',".$args['intOrder'].",".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	
		if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}	
	
		
	
	}
	
	//
	// Updata Subcategory
	//
	function doUpdateSubcategory($args){
	 global $CONF;

	$query ='UPDATE subcategory                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		 	 strSubCategoryName       ="'.$args['strSubCategoryName'].	'",                                  	 
			 strDescription           ="'.$args['strDescription'].	'" ,                              
 		  	 strAddedBy        ="'.$args['strAddedBy'].	'" ,  
			 intOrder         ='.$args['intOrder'].	',
		     intStatus         ='.$args['intStatus'].	'                                      
       WHERE 
      		intSubCategoryId='.$args['id'];

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Updata Subcategory Photo
	//
	function doInsertUpdateSubcategory($id,$arrfilename){
	 global $CONF;
	
	$query = 'UPDATE  subcategory SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strPic="'.$arrfilename.'" 
	 
	 WHERE intSubCategoryId='.$id;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	
		 
	 //
	 // Do Add Banners
	 //
	 function doAddBanner($args){
	 	global $CONF;
	 	
	 	$strBanner = "-";
	 	
	 	$query = "INSERT INTO banner(intBannerId,strBanner,strLink,strCaption,intTypeBanner,intOrder,strAddedBy,intStatus,CreateDate,UpdateDate)
	 	VALUES(0,'".$strBanner."','".$args['strBannerLink']."','".$args['strBannerCaption']."',".$args['intBannerType'].",".$args['intBannerOrder'].",'".$args['strAddedBy']."',".$args['intStatus'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
	 	
	
		
		
		
	 			if($this->query_id = $this->db->query($query)){
				 $id = mysql_insert_id();
				
				return $id;
			}
			else{
				return 0;
			}
	 	}
	 //
	 // Do Update Banner
	 //	
	 function doUpdateBanner($args){
	 	global $CONF;
		
	 	$query = 'UPDATE banner SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	
	 strLink         = "'.$args['strBannerLink'].'" ,
	 strCaption      = "'.$args['strBannerCaption'].'",
	 intTypeBanner   = '.$args['intBannerType'].',
	 intOrder        = '.$args['intBannerOrder'].',
	  intStatus      = '.$args['intStatus'].',
		strAddedBy     = "'.$args['strAddedBy'].'"
	 WHERE intBannerId='.$args['intBannerId'];
	

			if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	
	 	
	 	}
	 	
	 	
	 	
	 
	 //
	 // Get All Banners
	 //
	 function getAllBanner(){
	 	$query = "SELECT * FROM banner ORDER BY CreateDate DESC";
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	
	 }
	 //
	 // Get Banner By Id
	 //
	 function doGetBannerById($intId){
	 		$query = "SELECT * FROM banner WHERE intBannerId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	
	 	
	 	}
	 
	 //
	 // Get Message Link By ID
	 //
	 function doGetMessageLinkById($intId){
	   $query = "SELECT * FROM message_link WHERE intMsgId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	 //
	 // Get Brand By ID
	 //
	 function doGetBrandById($intId){
	   $query = "SELECT * FROM brand WHERE intBrandId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	  //
	 // Get Network Photo By ID
	 //
	 function doGetNetworkPhotoById($intId){
	   $query = "SELECT * FROM network_photo WHERE intNetworkId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	   //
	 // Get Manage Inform By ID
	 //
	 function doGetManageInformById($intId){
	   $query = "SELECT * FROM contactus WHERE intContactUsId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	  //
	 // Get Campaign By ID
	 //
	 function doGetCampaignById($intId){
	   $query = "SELECT * FROM campaign WHERE intCampaignId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	 
	  //
	 // Get Answer Selected By ID
	 //
	 function doGetAnswerSelectedById($intId){
	   $query = "SELECT * FROM campaign_answer_selection WHERE intAnswerSelectionId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	 
	 //
	 // Get Question By ID
	 //
	 function doGetQuestionById($intId){
	   $query = "SELECT * FROM campaign_question WHERE intQuestionId=".$intId;
	 		
	 	
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	  //
	 // Get Mc Category By ID
	 //
	 function doGetMcCategoryById($intId){
	   $query = "SELECT * FROM category WHERE intCategoryId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	  //
	 // Get Message Photo By ID
	 //
	 function doGetMessagePhotoById($intId){
	   $query = "SELECT * FROM message_photo WHERE intMshCeoId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	   //
	 // Get Message Photo By ID Solution
	 //
	 function doGetMessagePhotoSolutionById($intId){
	   $query = "SELECT * FROM message_photo_solution WHERE intSolutionId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	 //
	 // Get Sub Category By ID
	 //
	 function doGetSubcategoryById($intSubCategoryId){
	   $query = "SELECT * FROM subcategory WHERE intSubCategoryId=".$intSubCategoryId;
	 	
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	  //
	 // Get Manage Product By ID
	 //
	 function doGetManageProductById($intId){
	   $query = "SELECT * FROM product WHERE intProductId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	 
	 //
	 // Get Feedback By ID
	 //
	 function doGetFeedbackById($intId){
	   $query = "SELECT * FROM feedback WHERE intFeedbackId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	 
	 //
	 // Get Customer By ID
	 //
	 function doGetCustomerReportById($intId){
	   $query = "SELECT * FROM campaign_customer WHERE intCustomerId=".$intId;
	 		
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
/*******************************************************************************************************************************************************************************/	 
	 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 	
	//
	// Do Add Quantity
	//
	function doAddQuantity($intItemId,$strAddedBy,$strQuantity,$strSPrice){
		global $CONF;
		$strStatus = "A";
		
		$query = 'INSERT INTO closetmino_item_store
		 (intItemStoreId,intItemId,strSPrice,intQuantity,strAddedBy,strStatus,CreateDate,UpdateDate) 
						VALUES(0,'.$intItemId.',"'.$strSPrice.'","'.$strQuantity.'","'.$strAddedBy.'","'.$strStatus.'", "' . timestr(time(),	$CONF['const_gmt_offset']) . '","' . timestr(time(),	$CONF['const_gmt_offset']) . '")';

  
			if ($this->query_id = $this->db->query($query)){
				
				return 1;
			}
			else{
				return 0;
			}	
		
		
		
		}
 
 
 //
 // Do Update Quantity Return
 //
 function doUpdateReturnQuantity($args){
 		global $CONF;
		
		$query = 'UPDATE  closetmino_inventory_return SET 
		strPoId = "'.$args['strPoId'].'",  
		intItemId = '.$args['intItemId'].',  
		intTypeId = '.$args['intTypeId'].',  
		intAttId  = '.$args['intAttId'].',   
		intValueId = '.$args['intValueId'].', 
		strItemCode = "'.$args['strProductCode'].'",
		strItemName = "'.$args['strProductName'].'",
		intQtyReturn = '.$args['intQtyReturn'].',
		strRemarks = "'.$args['strRemarks'].'",
		UpdateDate="'.timestr(time(),	$CONF['const_gmt_offset']).'", 
		strAddedBy ="'.$args['strAddedBy'].'"
  	 WHERE intInvRId='.$args['intId'];
	
			if($this->query_id = $this->db->query($query)){
				return 1;
      }
			else{
				return 0;
        }
 	}
 
 
 //
 // Get the Return Quantity Form
 //
 function doGetReturnById($id){
 
 		$query = "SELECT * FROM closetmino_inventory_return WHERE intInvRId=".$id;	
		
	
			if($this->query_id = $this->db->query($query)){
				return 1;
                    }
			else{
				return 0;
                  }
 	
 }
 
 
 //
 // Do Delete Note
 //
 function doDeleteNote($arrReturnId,$strAddedBy){
 	 global $CONF;
 	 $strStatus ="D";
 	
 		foreach($arrReturnId as $key => $ReturnId){
				
				$query = 'UPDATE  closetmino_inventory_return SET strStatus="'.$strStatus.'" ,UpdateDate="'.timestr(time(),	$CONF['const_gmt_offset']).'", strAddedBy ="'.$strAddedBy.'" WHERE intInvRId='.$ReturnId;
  				
  				if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			}
		 	return 1;
 	
 	
 	}	
	
	
	//
	// Do Add Quantity Return Form
	//
	function doAddReturnQuantity($args){
		global $CONF;
		
			$query = "INSERT INTO closetmino_inventory_return
			(intInvRId,strPoId,intItemId,intTypeId,intAttId,intValueId,strItemCode,strItemName,intQtyReturn,strRemarks,strAddedBy,CreateDate,UpdateDate)
			VALUES(0,'".$args['strPoId']."' ,".$args['intItemId'].",".$args['intTypeId'].",".$args['intAttId'].",".$args['intValueId'].",'".$args['strProductCode']."','".$args['strProductName']."',".$args['intQtyReturn'].",'".$args['strRemarks']."','".$args['strAddedBy']."','".timestr(time(),	$CONF['const_gmt_offset'])."','".timestr(time(),	$CONF['const_gmt_offset'])."')";
  		
  		if($this->query_id = $this->db->query($query)){
				return 1;
                    }
			else{
				return 0;
                  }
 
	}
	
	
	//
	// Get the closetmino_qc_total
	//
	function doGetValue($id){
		
		$query = "SELECT * FROM closetmino_qc_total WHERE intItemId=".$id;	
			if($this->query_id = $this->db->query($query)){
				return 1;
                    }
			else{
				return 0;
                  }
	}
	
	
	
	//
	// Get All Product Code
	//
	function getAllProductCode(){
		
			$query = 'SELECT intItemId,strItemCode FROM  closetmino_item order by strItemCode ASC';
				if($this->query_id = $this->db->query($query)){
				return 1;
                    }
			else{
				return 0;
                  }
		
	}
	
	
	//
	// Get All Return Quantity
	// 
	function getAllReturnStore($intPage=0){
			global $CONF;
		
		$strStatus = "Y";
		$query = "SELECT * FROM  closetmino_inventory_return WHERE strStatus='".$strStatus."' ORDER BY CreateDate DESC";

			if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
 	//
 	// Get Account Details
 	//
 	function getAccDetails($intAccID){
			
			$strEmailpaters = 'NULL';
			$strStatus  = 'Y';
		$query = 'SELECT intAccId, strEmail,strUser FROM closetmino_account_closetmino WHERE strEmail!="'.$strEmailpaters.'" AND strStatus="'.$strStatus.'" AND intAccId IN '.$intAccID;
			if($this->query_id = $this->db->query($query)){
				return 1;
                    }
			else{
				return 0;
                  }
		}
 
 //
 // Get All Active Users
 //
		function getActiveUser(){
			$strEmailpaters = 'NULL';
			$strStatus  = 'Y';
			$query = 'SELECT * FROM  closetmino_account_closetmino WHERE strEmail!="'.$strEmailpaters.'" AND strStatus="'.$strStatus.'"';
				if($this->query_id = $this->db->query($query)){
				return 1;
                    }
			else{
				return 0;
                  }
		}
 
 
 
  //
  // Do Insert Tag
  //
  function doInsertTag($intItemId,$arrTag,$strAddedBy){
  	global $CONF;	
  	
  		foreach($arrTag as $key => $TagCateid){
				
				$query = "INSERT INTO closetmino_tagging(intTagingId,intCateSubCateId,intItemId,strAddedBy,CreateDate,UpdateDate)VALUES(0,".$TagCateid.",".$intItemId.",'".$strAddedBy."','".timestr(time(),	$CONF['const_gmt_offset'])."','".timestr(time(),	$CONF['const_gmt_offset'])."')";
  				//echo $query;
  				//echo "<br>";
  				if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			}
		 	return 1;
  	
  }
 //
 // Get the Tag Recording
 //
 function doGetTagExisting($id){
 	
 	$query = "SELECT * FROM closetmino_tagging WHERE intItemId=".$id;
 	
 	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
 	
 	}
 	//
 	// Do delete Tag
 	//
 	function doDeleteTag($intItemId){
 
 		$query = "DELETE FROM closetmino_tagging WHERE intItemId=".$intItemId;
 		
 		if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
 	}
 	//
 	// Do Edit Product
 	//
 	function doEditProduct($args){
 		global $CONF;
 	 		
 		$query = 'UPDATE closetmino_item SET 
 		
 		intTypeId='.$args['intTypeid'].',
 		intCollectionId = '.$args['intCollectionid'].',
 		intCategoryId   = '.$args['intCate'].', 
 		intSubCateId    = '.$args['intSubCate'].',
 		strItemName     = "'.$args['strProductName'].'",
 		strItemCode     = "'.$args['strProductCode'].'",
 		strSKU          = "'.$args['strSKU'].'",
 		strWeight       = "'.$args['strWeight'].'",
 		strNormalItemPrice  = "'.$args['strNPrice'].'",
 		strSPrice        = "'.$args['strSPrice'].'",
 		strVPrice       = "'.$args['strVIPPrice'].'",
 		strDescription  = "'.$args['strDesc'].'",
 		intStatus       = '.$args['intStatus'].',
 		strAddedBy     ="'.$args['strAddedBy'].'",
 		strStartDate   = "'.$args['strStartDate'].'",
 		strEndDate     = "'.$args['strStartDateE'].'",
 		intExpiryDateMk = '.$args['intMkDayEnd'].',
 		strRedemption    = "'.$args['strPointRedemption'].'",
 		strRedemptionPoint = '.$args['intPoint'].',
 		
 		
 		
 		UpdateDate="'.timestr(time(),	$CONF['const_gmt_offset']).'"
 		WHERE intItemId='.$args['id'];
		
 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
  		
 	}
 	
 	
 	
 	//
 	// Do Get Pro By Id
 	//
  function doGetProductById($id){
  	global $CONF;
  	
  	 $query = 'SELECT * FROM  closetmino_item  WHERE  intItemId='.$id;
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
  	
  	
  }
 
 	//
 	// Get All Product
 	//
 	function getAllProduct($intPage=0){
 		global $CONF;
		
		$query = "SELECT * FROM closetmino_item  ORDER BY CreateDate DESC";

			if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
 	}
 
 	//
	// Get Used Quantity
	//
	function getitemdetailtran($intItemId){
		global $CONF;
		
	 $query = 'SELECT strQuantityBuy FROM closetmino_shopping_deductquantity  WHERE  intItemId='.$intItemId;
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
 	//
	// Do Get Quantity
	//
	function doGetQuantity($intItemId ,$storeid){
	 global $CONF;

	$query ='SELECT * FROM   closetmino_item_store WHERE intItemStoreId='.$storeid.' AND intItemId='.$intItemId;
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	
	//
	// Get the quantity in blufroge_item
	//	
	function doGetitemQ($intItemId){
		
			$query ='SELECT strQuantity FROM  closetmino_item WHERE intItemId='.$intItemId;
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Do Get all order quantity
	//
	function doGetQuantityOrder($intItemId){
		
	 $query ='SELECT strQty  FROM closetmino_shoppingitem_trans WHERE intitemId ='.$intItemId;
	
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	
	//
	// Do Insert Delete Quantity
	//
	function doInsertDeleteAgent($intItemId ,$intStoreQuantity,$intProductType,$strProductDetails,$strAddedBy){
		global $CONF;
		
			$query = 'INSERT INTO  closetmino_qty_delete
		 (intQuantityId,intItemId,intQuantity,intProductType,strProductDetail,strAddedBy,CreateDate,UpdateDate) 
						VALUES(0,'.$intItemId.','.$intStoreQuantity.','.$intProductType.',"'.$strProductDetails.'","'.$strAddedBy.'","' . timestr(time(),	$CONF['const_gmt_offset']) . '","' . timestr(time(),	$CONF['const_gmt_offset']) . '")';


/*echo "<br> Insert Delete here : <br>";
echo $query;
echo "------------------------<br>";
*/
	if ($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}	
			
      
	}
	//
	// Do Get QCS
	//
	function doGetQCSQuantity($id , $intStoreId ){
		
		$query = "SELECT * FROM closetmino_item_qc WHERE intItemStoreId=".$intStoreId." AND intItemId=".$id;	
		
		if ($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}	
	}

	//
	// Do Update blufroge item
	//
	function doUpdateQuantity($strAddedBy ,$intItemId,$intFinalTotal){
			global $CONF;
		
		$query = 'UPDATE closetmino_item SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	strQuantity='.$intFinalTotal.' WHERE intItemId='.$intItemId.' AND strAddedBy="'.$strAddedBy.'" LIMIT 1';
		
			
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	}
	//
	// Delete item_qc
	//
	function doDeleteitemqc($storeid,$intItemId){
		
		$query = 'DELETE FROM closetmino_item_qc WHERE intItemStoreId='.$storeid.' AND intItemId='.$intItemId;
		
		//echo $query."<br>";
		//exit;
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
	//
	// Do Delete qc_total
	//
	function doDeleteQCTotal($intItemId,$arrQSC){
			global $CONF;
		
		foreach($arrQSC as $intItemId => $k){
			foreach($k as $intColor => $v){
				foreach($v as $intSize => $qty){
					//echo "<br>";
					$query = 'DELETE FROM closetmino_qc_total WHERE intAtt1='.$intColor.' AND intAtt2='.$intSize.' AND intQuantity='.$qty.' AND intItemId='.$intItemId;
					
					//echo $query;
					
					//echo "<br>";
					
					 if(!$this->query_id = $this->db->query($query)){
					return 0;
			}
					
					}
				
				}
			
			
			}
		return 1;
		
	}
	
		//
	// Get qc_total
	//
	function GetTheQctotal($intItemId){
		$query = 'SELECT intQuantity FROM closetmino_qc_total WHERE intItemId='.$intItemId;
		
		if ($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}	
	}
	
	
		//
	// Do Update sms_company_item
	//
	function doUpdatetheQuantityItem($intItemId,$intTotalUp,$strAddedBy){
		global $CONF;

		
		$query = 'UPDATE closetmino_item SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	strQuantity='.$intTotalUp.' WHERE intItemId='.$intItemId.' AND strAddedBy="'.$strAddedBy.'" LIMIT 1';
		
	//	echo $query;
		
		
			
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
	
		//
	// Insert qc_total
	//
	function doInsertitemqc($arrMashup,$intItemId){
		global $CONF;
		
		foreach($arrMashup as $intItemId => $k){
			foreach($k as $intColor => $v){
				foreach($v as $intSize => $qty){
					//echo "<br>";
					$query = 'INSERT INTO closetmino_qc_total(intTotalColorId,intItemId,intAtt1,intAtt2,intQuantity,CreateDate,UpdateDate)	VALUES(0,'.$intItemId.','.$intColor.','.$intSize.','.$qty.',"'.timestr(time(),$CONF['const_gmt_offset']).'","'.timestr(time(),$CONF['const_gmt_offset']).'")';
					
				
					 if(!$this->query_id = $this->db->query($query)){
					return 0;
			}
					}
				
				}
			
			
			}
		return 1;
		
	}
	//
	// Do remove quantity
	// 
	function doDeleteQuantityStore($storeid,$intItemId){
		
		$query = 'DELETE FROM closetmino_item_store WHERE intItemStoreId='.$storeid.' AND intItemId='.$intItemId.'  LIMIT 1';
		
		//echo $query;
		//echo "<br>";
		//exit;
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
		 
	//
	// Get the closetmino_qc
	// 
	function doGetQuantityQC($intItemId ,$storeid){
		
			$query ='SELECT * FROM  closetmino_item_qc WHERE intItemStoreId='.$storeid.' AND intItemId='.$intItemId;
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
		
	}
 	//
	// Do Update Quantity
	//
	function doUpdateItemQ($finalQuantity,$intItemId){
		
			global $CONF;
		
		$query = 'UPDATE  closetmino_item SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	strQuantity='.$finalQuantity.'  WHERE intItemId='.$intItemId;
	 	
	 	
	 	
	 
	 	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
 	//
	// Get More Photo
	//
	function getMorePhoto($intItemId){
		global $CONF;	
		
		$query = 'SELECT * FROM closetmino_company_item_photo_store WHERE intItemId='.$intItemId.'  ORDER BY CreateDate DESC';
		
			if ($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	}
 	
 	
 	
 	
 	//
	// Get All Quantity Store
	//
	function getAllItemStore($intItemId,$intPage=0){
		global $CONF;
		
		$query = 'SELECT * FROM closetmino_item_store  WHERE  intItemId='.$intItemId .'  ORDER BY CreateDate DESC';
	 
			
			if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
				
	}
 	//
	// Get Details of Item
	//
	function getitemdetail($intItemId){
		global $CONF;
		
		$query = 'SELECT * FROM closetmino_item_store  WHERE  intItemId='.$intItemId;
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
  //
	// Get Details of Item
	//
	function getitemdetailnormail($intItemId){
			global $CONF;
		
		$query = 'SELECT * FROM closetmino_item  WHERE  intItemId='.$intItemId;
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
 
 
 
  //
	// Update sms_company_item
	//
	function doUpdateComitem($intAllOver,$intItemId,$strAddedBy){
		global $CONF;
		
		$query = 'UPDATE  closetmino_item SET strAddedBy="'.$strAddedBy.'",UpdateDate="'.timestr(time(),	$CONF['const_gmt_offset']).'",strQuantity='.$intAllOver.' WHERE intItemId='.$intItemId;
		
		//echo $query."<br>";
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
 
 
 
  //
	// Get Total item
	//
	function doGetitemStoreItem($intItemId){
		
			$query = 'SELECT intQuantity FROM  closetmino_item_store WHERE intItemId='.$intItemId;
		
			if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;	
		
		
	}
  
  //
	// Get Update qc
	//
	function doUpdateitemStore($intTotal,$intStoreId,$strAddedBy){
		global $CONF;
		
		$query = 'UPDATE  closetmino_item_store SET strAddedBy="'.$strAddedBy.'" ,UpdateDate="'.timestr(time(),	$CONF['const_gmt_offset']).'", intQuantity='.$intTotal.' WHERE intItemStoreId='.$intStoreId;
		
		//echo $query."<br>---------------------------------<br>";
			if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
  
  //
  // Do Add QC Total
  //
  function doAddQCSQtyTotal($arrMashup,$intOption,$intItemId,$intAttId,$strAddedBy){
  	global $CONF;
  	
  	$intTypeId = 2;
  	  $intAtt2   = 0;
  			foreach($arrMashup as $intAtt1 => $id){
				foreach($id as $r => $qty){
				$query = "INSERT INTO closetmino_qc_total(intTotalColorId,intItemId,intAttId,intAtt1,intAtt2,intQuantity,strAddedBy,CreateDate,UpdateDate)
					VALUES(0,".$intItemId.",".$intAttId.",".$r.",".$intAtt2.",".$qty[0].",'".$strAddedBy."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";		
  		//echo $query."<br>";
  				if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			}
		}
  	return 1;
  	
  	}
  
  //
  // Do CQS Semi
  //
  function doAddQCSQty($arrFinal,$intOption,$intItemId,$intStoreId,$strAddedBy ){
  		global $CONF;
  	  
  	  $intTypeId = 2;
  	  $intAtt2   = 0;
  	  
  		foreach($arrFinal as $intAtt1 => $id){
				foreach($id as $r => $qty){
				$query = "INSERT INTO closetmino_item_qc(intQcId,intTypeId,intItemId,intItemStoreId,intAttId,intAtt1,intAtt2,intQuantity,strAddedBy,CreateDate,UpdateDate)
					VALUES(0,".$intTypeId.",".$intItemId.",".$intStoreId.",".$intOption.",".$intAtt1.",".$intAtt2.",".$qty.",'".$strAddedBy."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";		
  		//echo $query."<br>";
  				if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			}
		}
		return 1;
  	
  }
  
  
  //
  // Do CQS Semi
  //
  function doAddQCSQtyEdit($arrFinal,$intOption,$intItemId,$intStoreId,$strAddedBy ){
  		global $CONF;
  	  
  	  $intTypeId = 2;
  	  $intAtt2   = 0;
  	  
  		foreach($arrFinal[$intStoreId] as $intAtt1 => $id){
				foreach($id as $r => $qty){
				$query = "INSERT INTO closetmino_item_qc(intQcId,intTypeId,intItemId,intItemStoreId,intAttId,intAtt1,intAtt2,intQuantity,strAddedBy,CreateDate,UpdateDate)
					VALUES(0,".$intTypeId.",".$intItemId.",".$intStoreId.",".$intOption.",".$intAtt1.",".$intAtt2.",".$qty.",'".$strAddedBy."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";		
  		//echo $query."<br>";
  				if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			}
		}
		//return 1;
  	
  }
  
  
  
  
  
  	//
	// Get Used color 
	//
	function doGetUsedQC($intItemId, $arrColor){
			$FilterColorId = implode(',',$arrColor);
			
			$query = 'SELECT * FROM closetmino_used_color_size WHERE intColor  IN ('.$FilterColorId.') AND intItemId='.$intItemId;
  echo $query;
  
  exit;
  	if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
		
	}
  
  //
	// Get the qc total
	//
	function doGetTotalQC($intItemId, $arrColor){
		
			$FilterColorId = implode(',',$arrColor);
			
			$query = 'SELECT * FROM closetmino_qc_total WHERE intAtt1  IN ('.$FilterColorId.') AND intItemId='.$intItemId;
  
  	if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
  function doAddTotalQCS($intItemId,$intOption ,$arrNewFinalMashup,$strAddedBy){
		global $CONF;
		
		
				foreach($arrNewFinalMashup as $intcolor => $q){
					foreach($q as $size => $quantity){
			$query = 'INSERT INTO closetmino_qc_total(intTotalColorId,intItemId,intAttId,intAtt1,intAtt2,intQuantity,strAddedBy,CreateDate,UpdateDate)
				VALUES( 0,' .
  					'' . $intItemId	.', ' .
		  			''.$intOption .','.
		  			'' .  $intcolor. ','. 
		  			'' .  $size. ','. 
		  			'' .  $quantity. ','.
		  			'"'.$strAddedBy.'",' .
						'"' . timestr(time(),	$CONF['const_gmt_offset']) . '", ' .	
						'"' . timestr(time(),	$CONF['const_gmt_offset']) . '")';
	//echo $query;
	//echo "<br>";
				if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			
				
			}
				
			}
			return 1;
		
		
	}
	//
	// Delete QSC
	//
	function doDeleteQCSAdd($intItemId){
		global $CONF;
		
			$query = 'DELETE FROM  closetmino_qc_total  WHERE intItemId='.$intItemId;
			
			//echo $query;
			//echo "<br>--------------------------<br>";	
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
  
  //
	// Get total
	//
	function doGetTotal($intStoreId){
		global $CONF;
		
		$query = " SELECT intQuantity FROM closetmino_item_qc WHERE intItemStoreId=".$intStoreId;
		
			if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
  
   
  
  
  //
	// Get the Attribute
	//
	function doGetExisting($intItemId){
		global $CONF;
		
		$query = 'SELECT * FROM closetmino_item_qc WHERE intItemId='.$intItemId;
		
	//echo $query;
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}

	function getAllCategoriesTagging($intPage=0){
		$query = "SELECT * FROM closetmino_category WHERE intTypeCate =2 Order By intCategoryId DESC";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}


 //
 // Get the Store Id
 //
 function doGetStoreId($intID){
 		$query = "SELECT intItemStoreId FROM closetmino_item_store WHERE intItemId=".$intID;

 	if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
 	
 	
 	}
 //
 // Get All value inside value by id
 //
 function doShowallvalueByid($intOption){
 		$query = "SELECT * FROM closetmino_value WHERE intAttId=".$intOption;

 	if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
 	}
  
  //
	// Get all color , size and quantity
	//
	function getAllDetailsSizeColor($intItemId,$intStoreId){
		
	 $query = 'SELECT * FROM closetmino_item_qc WHERE intItemId='.$intItemId.' AND intItemStoreId='.$intStoreId;

		if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
  //
	// Do Delete color , size and quantity
	//

	function doDeleteCSQ($intItemId,$intStoreId){
		
		$query = 'DELETE FROM closetmino_item_qc WHERE intItemId='.$intItemId.' AND intItemStoreId='.$intStoreId;

//echo $query;
//echo "<br>-----------------------------------------------------<br>";
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	
	
	 }
  
  	//
	// Do add color , size and quantity
	//
	function doInsertColorSizeQuantity($arrFinal,$intItemId,$intStoreId){
		global $CONF;
		foreach($arrFinal as $color => $x){
			foreach($x as $size => $quantity){
				$query = 'INSERT INTO closetmino_item_qc(intQcId,intItemId,intItemStoreId,intColor,intSize,intQuantity,CreateDate,Updatedate)
				VALUE(0,'.$intItemId.','.$intStoreId.','.$color.','.$size.','.$quantity.',"'.timestr(time(),$CONF['const_gmt_offset']).'","'.timestr(time(),$CONF['const_gmt_offset']).'")' ;
  			if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			//echo "<br> Insert :<br>";
  		//echo $query;
  		//echo"<br>";
  		
  		}
		}
		return 1;
	}
  
  
  
  
  //
	// Select all QSC
	//
	function getAllQCSSIZE($intItemId){
		$query = 'SELECT * FROM closetmino_item_qc WHERE intItemId='.$intItemId;
		
			if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
 
 //
 // Get All value inside value
 //
 function doShowallvalue(){
 		$query = "SELECT * FROM closetmino_value ";
 	
 	if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
 	}

 //
 // Get All value
 //
 function doGetAllArr(){
 	$strStatus = "Y";
 	$query = "SELECT * FROM  closetmino_attribute WHERE strStatus='".$strStatus."'";
 	
 	if($this->query_id = $this->db->query($query)){
				$id = mysql_insert_id();
				return $id;
     }
		else{
				return 0;
      }	
 	}
 
 
 
 //
 // Update Quantity Store
 //
 function doAddProductStore($args){
 	global $CONF;
 	
 	$query = "INSERT INTO closetmino_item_store(intItemStoreId,intItemId,strSPrice,intQuantity,strAddedBy,CreateDate,UpdateDate)
 	VALUES(0,".$args['intItemId'].",'".$args['strItemPrice']."',".$args['intQuantity'].",'".$args['strAddedBy']."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";		
 	
 	
 	
 	if($this->query_id = $this->db->query($query)){
			$i = mysql_insert_id();
			
				return $i;
     }
		else{
				return 0;
      }	
 }
 //
 // Update Pic
 //
 function doInsertImageAdPic($id,$arrfilename){
 		global $CONF;
 	
 		$query = "UPDATE closetmino_item SET UpdateDate='".timestr(time(),$CONF['const_gmt_offset'])."', strImage='".$arrfilename."' WHERE intItemId=".$id;
 			
 			if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
 		
 	
 	}
  
 //
 // Do update Value
 //
 function doUpdateValue($valueid,$strValue ,	$strStatus , $strAddedBy){
 	$query = 'UPDATE closetmino_value SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", strStatus= "'.$strStatus.'" , variable="'.$strValue.'", strAddedBy="'.$strAddedBy.'" WHERE intValueId='.$valueid;
		
		if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
 	
 	
 }
 
 //
 // Get the Value
 //
 function doGetValueVariable($intAttributeId,$intValueId){
 	$query = "SELECT * FROM closetmino_value WHERE intValueId=".$intValueId." AND intAttId=".$intAttributeId;
 	
 	if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
 	
 }
 
 //
 // Do insert att Value
 //
 function doInsertValueAtt($strValue,$intAtt,$strStatus,$strAddedBy){
 	global $CONF;
 		$query = "INSERT INTO closetmino_value(intValueId,intAttId,variable,strStatus,strAddedBy,CreateDate,UpdateDate)VALUES(0,".$intAtt.",'".$strValue."' , '".$strStatus."','".$strAddedBy."','".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";		
	

		if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
 	
 	}
 
 
  //
  // Check the Value
  //
  function doCheckValue($strValue){
  		$query = "SELECT * FROM closetmino_value WHERE variable='".$strValue."'";		
		
		
		
		if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
  }
	//
	// Get the head values
	//
	function getValueHead($intAttributeId){
			$query = "SELECT strValue FROM closetmino_attribute WHERE intAttId=".$intAttributeId;		
		if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
	}
	
	//
	// Get  Values By AttId
	//
	function getAllValuesByattId($intPage=0,$intAttributeId){
		$query = "SELECT * FROM closetmino_value WHERE intAttId=".$intAttributeId." Order By UpdateDate DESC";		
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
	//
	// Get All Attribute
	//
	function doGetAttribute(){
		$query = "SELECT * FROM  closetmino_attribute Order By UpdateDate DESC";
			if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }
	}
	//
	// Update Customer Profile
	//
	function doChangesStatusCustomer($strStatus,$intCustomerType,$strAddedBy,$intAccId){
		$query = 'UPDATE closetmino_account_closetmino SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", strStatus= "'.$strStatus.'" , intAccType='.$intCustomerType.', strAddedBy="'.$strAddedBy.'" WHERE intAccId='.$intAccId;
		
		if($this->query_id = $this->db->query($query)){
				return 1;
     }
		else{
				return 0;
      }	
	}
	
	//
	// Get Customer Details by Id
	//
	function doGetCustomerById($intId){
		$query="SELECT * FROM  closetmino_account_closetmino WHERE intAccId=".$intId;

			if($this->query_id = $this->db->query($query)){
				return 1;
       }
			else{
				return 0;
      }
	}
	
	
	//
	// Do Add Newsletter
	//
	function doAddNewsletter($strSubject,$postedValue){
		global $CONF;
		
			$query = 'INSERT INTO
						closetmino_newsletter(intNewsId,strTitle,strContent,CreateDate,UpdateDate)      
							VALUES(0, ' .
							'"' . $strSubject. '", ' .
							'"' . $postedValue. '", ' .
							
							'"' . timestr(time(),	$CONF['const_gmt_offset']) . '", ' .	
							'"' . timestr(time(),	$CONF['const_gmt_offset']) . '")';			
					
							
		
		if($this->query_id = $this->db->query($query)){
			
				return 1;
                    }
			else{
				return 0;
                  }
		
		
	}
	//
	// Get All Newsletter
	//
	function getAllnewsletter($intPage){
		$query = 'SELECT * FROM closetmino_newsletter Order by UpdateDate DESC';
		
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
		
		
	}
	//
	// Get Newsletter Edit By Id
	//
	function getNewsletter($intNewsLetterId){
		
		$query = 'SELECT * FROM closetmino_newsletter WHERE intNewsId='.$intNewsLetterId;
		
			if($this->query_id = $this->db->query($query)){
			
				return 1;
                    }
			else{
				return 0;
                  }
		
	}
	//
	// Do update Newsletter
	//
	function doEditNewsletter($newsId,$strSubject,$postedValue){
		global $CONF;
		
		$query = 'UPDATE closetmino_newsletter SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", strTitle= "'.$strSubject.'" , strContent="'.$postedValue.'" WHERE intNewsId='.$newsId;
		
		if($this->query_id = $this->db->query($query)){
				return 1;
                    }
			else{
				return 0;
                  }
	}
	//
	// Do delete Newsletter
	//
	function doDeleteNewsletter($newId){
		foreach($newId as $key => $id){
				
				$query = 'DELETE FROM closetmino_newsletter WHERE intNewsId='.$id ;
  				
  				if(!$this->query_id = $this->db->query($query)){
					return 0;
				}
			}
		 	return 1;
	
	}	


		//
		// Do validation account
		//
		function doValidateAccount($intUserId,$intExpire){
			$strA = 'A';
			$strY = 'Y';
		  $query = 'UPDATE sms_account_closetmino SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'",intIsEmail="'.$strY.'", strVerifyPhone="'.$strA.'",strExpireMobile='.$intExpire.' WHERE intAccId='.$intUserId;
	  
	  if($this->query_id = $this->db->query($query)){
				return 1;
			}
		return 0;
  	}
	 //
	 // Get All Categories
	 //
	 function getAllCategory(){
	 	global $CONF;
	 	
	 	$strStatus = 'Y';
	 	$query = 'SELECT * FROM closetmino_category WHERE strStatus ="'.$strStatus.'" AND intTypeCate=1';
     
	 	if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	 	}
	 
	  //
	 // Get All Categories
	 //
	 function getAllCategoryBoth(){
	 	global $CONF;
	 	
	 	$query = "SELECT * FROM closetmino_category ";
     
	 	if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	 	}
	   //
	 // Get All Categories
	 //
	 function getAllSubCategoryBoth(){
	 	global $CONF;
	 	
	 	$query = "SELECT * FROM closetmino_subcategory ";
     
	 	if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	 	}
	    
	 //
	 // Get All Tag Categories
	 //
	 function getAllCategoryTag(){
	 	global $CONF;
	 	
	 	$strStatus = 'Y';
	 	$query = 'SELECT * FROM closetmino_category WHERE strStatus ="'.$strStatus.'" AND intTypeCate=2';
     
	 	if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	 	}
	
 	 //
	 // Get All Categories
	 //
	 function getCategory($intPage=0){
	 	 global $CONF;
     
     $query = 'SELECT * FROM closetmino_category WHERE intTypeCate=1 order by CreateDate DESC';
    
      
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	 	
	 	}
	 //
	 // Get All Sub Categories
	 //	
	 function getSubCategory($intPage=0,$intCategoryId){
	 	global $CONF;
     
     $query = 'SELECT * FROM closetmino_subcategory  WHERE intCategoryId ='.$intCategoryId.' order by CreateDate DESC';
     
     
    
      
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	 	
	 	
	 	}
	 //
	 // Get All
	 //
	 function doGetCollectionAll(){
	 	$strStatus = 'Y';
	 		$query = 'SELECT * FROM closetmino_collection WHERE strStatus="'.$strStatus.'"';
		 if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	
	 	}
	 //
	 // Get VIP
	 //	
	 function doGetVIP(){
	 	$query = "SELECT * FROM  closetmino_vip ";
		 if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	
	 	}
	 //
	 // Update VIP
	 //
	 function doUpdateVIP($strCurrency1,$strAddedBy){
	 	global $CONF;
	 	
	 	$intId = 1;
	 		 	
	 	$query = 'UPDATE closetmino_vip SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	strValue	="'.$strCurrency1.'",
	 strAddedBy = "'.$strAddedBy.'"
	 WHERE 	intVIPId='.$intId;
	 	if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	}
	 //
	 // Get Currency
	 //
	 function doGetCurrency(){
	 	$query = "SELECT * FROM  closetmino_currency ";
		 if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	
	 	
	 	}
	 //
	 // Update Currency
	 //
	 function doUpdateCurrency($strCurrency1,$strAddedBy){
	 	global $CONF;
	 	
	 	$intId = 1;
	 		 	
	 	$query = 'UPDATE closetmino_currency SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	strMoney1	="'.$strCurrency1.'",
	 strAddedBy = "'.$strAddedBy.'"
	 WHERE 	intCurrencyId='.$intId;
	 	if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	}
	 
	 //
	 // Do Edit Collection
	 //
	 function doGetCollectionEdit($intId,$strCollectionName,$strStatus,$strAdded){
	 	global $CONF;
	 	
	 	$query = 'UPDATE closetmino_collection SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	strCollectionName	="'.$strCollectionName.'",
	 strStatus="'.$strStatus.'" ,
	 strAddedBy = "'.$strAdded.'"
	 WHERE 	intCollectionId='.$intId;
	 	
	 	 if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 }
	//
	// Get the Collection By Id
	//
	function doGetCollectionById($intId){
			global $CONF;
     
     $query = 'SELECT * FROM closetmino_collection WHERE intCollectionId='.$intId;
		 if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
    
		
	}
	//
	// Get All Collection
	//
	function getCollection($intPage=0){
			global $CONF;
     
     $query = 'SELECT * FROM closetmino_collection order by CreateDate DESC';
     
     if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
    
      
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
		
		
	}
	
	//
	// Get All Collection without Pagination
	//
	function getAllColl(){
			global $CONF;
     
     $query = 'SELECT * FROM closetmino_collection order by CreateDate DESC';	
		
		
   	if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	}
	
	
	//
	// Do Insert Collection
	//
	function doInsertCollection($strCollectionName,$strStatus , $strAddedBy){
		global $CONF;	
		 	
	 	$query = 'INSERT INTO closetmino_collection(intCollectionId,strCollectionName,strStatus,strAddedBy,CreateDate,UpdateDate)VALUES(0,"'.$strCollectionName.'","'.$strStatus.'","'.$strAddedBy.'","'.timestr(time(),$CONF['const_gmt_offset']).'","'.timestr(time(),$CONF['const_gmt_offset']).'")';
 
   	if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
		
	}	
	
	//
	// Do Insert Shipping
	//
	function doInsertShipping($intShippingAgent,$intTo ,$strAmount,$strRate,$strAmount2,$strRate2,$strAddedBy){
		global $CONF;	
		 	
		 	$strStatus = "Y";
	 	$query = 'INSERT INTO closetmino_shipping(intShippingId,intTo,intShippingAgent,strAmount,strAmount2,strRate,strRate2,strStatus,strAddedBy,CreateDate,UpdateDate)
	 	VALUES(0,'.$intShippingAgent.','.$intTo .',"'.$strAmount.'","'.$strAmount2.'","'.$strRate.'","'.$strRate2.'","'.$strStatus.'","'.$strAddedBy.'","'.timestr(time(),$CONF['const_gmt_offset']).'","'.timestr(time(),$CONF['const_gmt_offset']).'")';
 
   	if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
		
	}
	//
	// Show all Shipping
	//
	function doGetAllShipping(){
	
	$query = "SELECT * FROM closetmino_shipping ORDER BY UpdateDate DESC";	
			if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
		
	}
	//
	//Show Edit Shipping
	//
	function doGetShippingById($intId){
			
	$query = "SELECT * FROM closetmino_shipping WHERE intShippingId=".$intId;	
			if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
		
		
	}
	//
	// Do Edit Shipping
	// 
	function doEditShipping($intShippingAgent,$intTo ,$strAmount,$strAmount2,$strRate,$strRate2,$strAddedBy,$intShippingId ){
		global $CONF;
		
	 	$query = 'UPDATE closetmino_shipping SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	strAddedBy    = "'.$strAddedBy.'",
	 intShippingAgent='.$intShippingAgent.' ,
	 intTo         ="'.$intTo.'",
	 strAmount = "'.$strAmount.'",
	  	strRate = "'.$strRate.'",
	  strAmount2 = "'.$strAmount2.'",
		strRate2 = "'.$strRate2.'"
	 WHERE intShippingId='.$intShippingId;
		

			if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
		
	}

	 
	 
	 
	 
	 
	 
	 //
	 // Check Collection
	 //
	 function doCheckCollection($strCollectionName){
	 	 global $CONF;
     
     $query = 'SELECT intCollectionId FROM closetmino_collection WHERE strCategoryName = "'.$strCollectionName.'"';
	 
	 		$data = array();
			if ($this->query_id = $this->db->query($query)){
				$this->GetNextRecord();
				if($this->Get('intCollectionId')){
					$data['intCollectionId']   = $this->Get('intCollectionId'); 
					return $data;
				}
				else{
					return 0;		
				}
		}
	 }
	 	
	 	
	 //
	 // Check Category
	 //
	 function doCheckCategory($strCategory){
	 	 global $CONF;
     
     $query = 'SELECT intCategoryId FROM closetmino_category WHERE strCategoryName = "'.$strCategory.'"';
	
	 		$data = array();
			if ($this->query_id = $this->db->query($query)){
				$this->GetNextRecord();
				if($this->Get('intCategoryId')){
					$data['intCategoryId']   = $this->Get('intCategoryId'); 
					return $data;
				}
				else{
					return 0;		
				}
		}
	 }
	 
	 //
	 // Check Sub Category
	 //
	 function doCheckSubCategory($strSubCategory,$intCategory){
	 	global $CONF;
     
     $query = 'SELECT intSubCategoryId FROM closetmino_subcategory WHERE strSubCategoryName = "'.$strSubCategory.'" AND intCategoryId='.$intCategory;
	 
	 		$data = array();
			if ($this->query_id = $this->db->query($query)){
				$this->GetNextRecord();
				if($this->Get('intSubCategoryId')){
					$data['intSubCategoryId']   = $this->Get('intSubCategoryId'); 
					return $data;
				}
				else{
					return 0;		
				}
		}
	 	
	 	
	 	}
	 
	 
	 //
	 // Insert category
	 //
	 function addCategory($strCategory,$strStatus,$strAddedBy,$intType){
	 	global $CONF;
	 	
	 	if($intType!=""){
	 		$intType = 2;
	 		}
	 	else{
	 		
	 		$intType=1;
	 		}
	 	
	 	$query = 'INSERT INTO closetmino_category(intCategoryId,strCategoryName,strStatus,intTypeCate,strAddedBy,CreateDate,UpdateDate)VALUES(0,"'.$strCategory.'","'.$strStatus.'",'.$intType.',"'.$strAddedBy.'","'.timestr(time(),$CONF['const_gmt_offset']).'","'.timestr(time(),$CONF['const_gmt_offset']).'")';
 
   	if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	
	 }
	 //
	 // Insert sub category
	 //
	 function addsubCategory($intCategory,$strSubCategoryName,$strStatus,$arrVolume,$strAlc,$strAddedBy){
	  global $CONF;
	  $query = 'INSERT INTO closetmino_subcategory(intSubCategoryId,intCategoryId,strSubCategoryName,strStatus,strAddedBy,CreateDate,UpdateDate)VALUES(0,'.$intCategory.',"'.$strSubCategoryName.'","'.$strStatus.'","'.$strAddedBy.'","'.timestr(time(),$CONF['const_gmt_offset']).'","'.timestr(time(),$CONF['const_gmt_offset']).'")';
   
   	if($this->query_id = $this->db->query($query)){
	$id = mysql_insert_id();		
            return $id;
			}
			else{
				return 0;
			}
	 	
	 	
	 	}
	
	 //
	 // Get Category By Id
	 //
	 function getCategoryById($intCategoryId){
	 	
	 	$query = 'SELECT * FROM closetmino_category WHERE intCategoryId = "'.$intCategoryId.'"';
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 }
	 //
	 // Get Sub Category By Id
	 //
	 function getSubCategoryById($intSubCategoryId){
	 	
	 	$query = 'SELECT * FROM closetmino_subcategory WHERE intSubcategoryId = '.$intSubCategoryId;
	 		
	 		if($this->query_id = $this->db->query($query)){
				return 1;
			}
			else{
				return 0;
			}
	 	
	 	}
	 	 
	 
	 
	 //
	 // Update Category
	 //
	 function doUpdateCategory($strCategory,$strStatus,$intCategoryId,$strAddedBy){
	 	global $CONF;
	 	
	 	$query = 'UPDATE closetmino_category SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 	strAddedBy    = "'.$strAddedBy.'",
	 strCategoryName="'.$strCategory.'" ,
	 strStatus = "'.$strStatus.'"
	 WHERE intCategoryId='.$intCategoryId;

	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
 	}
	 	
	 //
	 // Update Sub Category
	 //
	 function doUpdateMSubCategory($strSubCategory,$strStatus,$intSubCategoryId,$strVcl,$strAlc,$strAddedBy){
	 	global $CONF;
	 	
	 	$query = 'UPDATE closetmino_subcategory SET UpdateDate="'.timestr(time(),$CONF['const_gmt_offset']).'", 
	 strSubCategoryName="'.$strSubCategory.'" ,
	 strAddedBy="'.$strAddedBy.'" ,
	 strStatus = "'.$strStatus.'"
	 
	 WHERE intMSubCategoryId='.$intSubCategoryId;
	
	 if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	 	
	 	
	 	
	 	}
	
 	  //
 	  // Function get Email
 	  //
 	  function doGetCompanyEmailById($arrCompanyEmail){
 	  		$FilterDate = "'".implode('\',\'',$arrCompanyEmail)."'";
		$query = 'SELECT * FROM sms_company WHERE  intCompanyId IN('.$FilterDate.') ORDER BY CreateDate DESC';
		
	
		
	if($this->query_id = $this->db->query($query)){
				return 1;
			}
		return 0;
 	  	
 	  	
 	  }
 	
 	//
 	// Get Address
 	//
 	function getShippingAddress($id){
 		
 		$query = 'SELECT * FROM sms_shopping_shipping  WHERE  strPoId ="'.$id.'"';
		
		
	if($this->query_id = $this->db->query($query)){
				return 1;
			}
		return 0;
 	  	
 		
 		
 	}
 	
 	 
}

?>