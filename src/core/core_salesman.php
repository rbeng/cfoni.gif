<?php

class core_salesman extends Record{
	function __construct(){
		$this->db = new DB();
		}

	//
	// Check Login 
	//
	function getLoginMember($user_id){
		$query = "SELECT strSalesmanCode,strUser,intSalesId,strPass,intType FROM salesman WHERE intStatus=1 AND strUser='".$user_id."'";
	

		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;	
	}
	//
	// Add Session
	//
	function addRecord($UsrName,$session){
		global $CONF;
		
		$query = "INSERT INTO salesman_session(strUser,strSession,CreateDate,UpdateDate) VALUES('".$UsrName."','".$session."', now(),now())";
			
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Delete Session
	//
	function deleteRecord($UsrName){
		$query = "DELETE FROM salesman_session  WHERE strUser='".$UsrName."'";
	
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Checking Session for log in user
	//
	function retrieveSession($module,$UsrName,$Session){
		$query = "SELECT * FROM ".$module." where strUser='".$UsrName."' AND strSession='".$Session."'";
		
		
		$data = array();
		if ($this->query_id = $this->db->query($query)){
			$this->GetNextRecord();
			if($this->Get('strUser')){
				$data['strUser']   = $this->Get('strUser'); 
				return $data;
			}
			else{
				return 0;		
				}
			}
	}
	//
	// Update the Last Login
	//
	function doUpdateLastlogin($intSalesId,$strNowLogin){
		global $CONF;
		
		$query ="UPDATE salesman                                                         
			SET strNowlogin    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			strLastlogin       = '".$strNowLogin."'       
			   WHERE 
      		intSalesId=".$intSalesId; 
		
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Update the First Time Last Login
	//
	function doUpdateBothLogin($intSalesId){
		global $CONF;
		
		$query ="UPDATE salesman                                                         
			SET strLastlogin    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			 strNowlogin        = '".timestr(time(),$CONF['const_gmt_offset'])."'        
			   WHERE 
      		intSalesId=".$intSalesId; 
		
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	
	
	//
	// Get All Customer By Salesman Id
	//
	function doGetAllCustomerBySalesmanId($intPage=0,$intSalesCode){
		$query = "SELECT * FROM customer WHERE strSalesmanCode='".$intSalesCode."' AND intStatus!=5  Order By UpdateDate DESC ";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
	//
	// Get the Profile By Id
	//
	function doGetProfile($id){
		$query = "SELECT * FROM salesman  WHERE intSalesId=".$id;
	 
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Get Lastlogin Value
	//
	function getLastlogin($intAccManagerId){
		$query = "SELECT strLastlogin,strNowLogin FROM salesman  WHERE intSalesId=".$intAccManagerId;
	 
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Do Check Email Address
	//
	function doCheckEmailAdd($strEmail){
		$query = "SELECT intSalesId FROM salesman WHERE  strEmail='".$strEmail."'";
	
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Do Update Profile
	//
	function doUpdateAccManagerProfile($args){
		global $CONF;
		
		if($args['strPassword1']!=""){
				$query ="UPDATE salesman                                                         
			SET UpdateDate    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			 strFullName      = '".$args['strFullName']."',
			 strContact       = '".$args['strContact']."', 
			 strPass       = '".$args['strPassword1']."', 
			 strEmail         = '".$args['strEmail']."'       
			   WHERE 
      		intAccountManagerId=".$args['id'];
		}
		else{
		
		$query ="UPDATE accountmanager                                                         
			SET UpdateDate    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			 strFullName      = '".$args['strFullName']."',
			 strContact       = '".$args['strContact']."', 
			 strEmail         = '".$args['strEmail']."'       
			   WHERE 
      		intAccountManagerId=".$args['id']; 
		}
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Do Add Salesman
	//
	function doAddSalesman($args){
		global $CONF;
		
		$strCode = "-";
		$query = "INSERT INTO  salesman(intSalesId,strSalesmanCode,strCommisionRate,intType,strName,strEmail,strContact,strUser,strPass,intAccountManagerId,CreateDate,UpdateDate) 
		VALUES(0,'".$strCode."',".$args['strCommisionRate'].",".$args['intType'].",'".$args['strFullName']."','".$args['strEmail']."','".$args['strContact']."','".$args['strUsername']."','".$args['strPassword']."',".$args['intAccountManagerId'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
		
	
	  if ($this->query_id = $this->db->query($query)){
	    $id = mysql_insert_id();
		return $id;
	  }
	  else{
		return 0;
	  }
		
	}
	//
	// Check Salesman Name
	//
	function doCheckUsername($strUsername){
		$query = "SELECT * FROM salesman WHERE  strUser='".$strUsername."'";
	
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	
	//
	// Check Salesman Emai;
	//
	function doCheckEmail($strEmail){
		$query = "SELECT * FROM salesman WHERE  strEmail='".$strEmail."'";
	
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Do Update Salesman Code
	//
	function doUpdateSalesman($strCode,$id){
		global $CONF;
		
		$query ="UPDATE salesman                                                         
			SET UpdateDate    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			 strSalesmanCode      = '".$strCode."'
			   WHERE 
      		intSalesId=".$id; 
		
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Do Show Salesman
	//
	function doGetSalesmanDetailsById($id,$intAccManagerId){
		$query = "SELECT * FROM salesman WHERE intSalesId=".$id." AND intAccountManagerId=".$intAccManagerId;
		
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Do Delete Salesman
	//
	function doDeleteSalesmanById($id,$strAddedBy){
		global $CONF;
		
		$intStatus = 5;
		 $query ='UPDATE salesman                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 intAccountManagerId       ='.$strAddedBy.	' ,      
		     intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intSalesId='.$id; 
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Do Show reseller
	//
	function doGetAllResellerBySalesmanId($intPage=0,$intSalesCode){
		$query = "SELECT * FROM reseller WHERE strSalesmanCode='".$intSalesCode."' AND intStatus!=5";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
		
	}
	//
	// Update Profile Salesman
	//
	function doUpdateProfileSalesman($args){
		global $CONF;
		
		if(trim($args['strPass'])==""){
		 $query ='UPDATE salesman                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
				strName       = "'.$args['strFullName'].'",          
				strIc         = "'.$args['strIc'].'",   
				strEmail      = "'.$args['strEmail'].'",   
				strContact    = "'.$args['strContact'].'",   
				strAdd        = "'.$args['strAdd'].'",   
				strCity       = "'.$args['strCity'].'",   
				strState      = "'.$args['strState'].'",   
				intCountry    = '.$args['intCountry'].', 
				strZip        = "'.$args['strZip'].'"                        
    	   WHERE 
      		intSalesId='.$args['intSalesId']; 
		}
		else{
			 $query ='UPDATE salesman                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
				strName       = "'.$args['strFullName'].'",          
				strIc         = "'.$args['strIc'].'",   
				strEmail      = "'.$args['strEmail'].'",   
				strContact    = "'.$args['strContact'].'",   
				strAdd        = "'.$args['strAdd'].'",   
				strCity       = "'.$args['strCity'].'",   
				strState      = "'.$args['strState'].'",   
				intCountry    = '.$args['intCountry'].', 
				strPass       = "'.md5($args['strPass']).'",  
				strZip        = "'.$args['strZip'].'"                        
    	   WHERE 
      		intSalesId='.$args['intSalesId']; 
			
		}
		if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Get All Customer By SalesCode
	//
	function getAllCustomerBySalesman($strSalesmanCode,$intPage=0){
		$query ="SELECT * FROM customer WHERE strSalesmanCode='".$strSalesmanCode."' AND intReseller=1 AND intStatus=1 Order By CreateDate DESC";
	
	if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
	//
	// Get the Customer Details
	//
	function doGetDetailsCustomer($intCustomerId){
		
		$query ="SELECT strUser, strPass FROM customer WHERE intCustomerId=".$intCustomerId;	
		
		if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
	
	
	
	
	
	//
	// Add : Point Reseller
	//
	function doAddNewReseller($intResellerType,$intCustomerId,$strCommisionRate,$strSalesmanCode,$strUsername,$strPassword,$strEmail,$strContact ){
		global $CONF;
		
		$strCode = "-";
		$query = "INSERT INTO  reseller(intResellerId,intResellerType,strResellerCode,intCommisionRate,strSalesmanCode,intCustomerId,strEmail,strContact,strUser,strPass,intStatus,CreateDate,UpdateDate) 
		VALUES(0,".$intResellerType.",'".$strCode."',".$strCommisionRate.",'".$strSalesmanCode."',".$intCustomerId.",'".$strEmail."','".$strContact."','".$strUsername."','".$strPassword."',1,'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
		
	  if ($this->query_id = $this->db->query($query)){
	    $id = mysql_insert_id();
		return $id;
	  }
	  else{
		return 0;
	  }
		
	}
	//
	// Add New Reseller
	//
	function doAddNewResellerNew($intResellerType,$strCommisionRate,$strSalesmanCode,$strUsername,$strPassword,$strEmail,$strContact ){
		global $CONF;
		
		$strCode = "-";
		$query = "INSERT INTO  reseller(intResellerId,intResellerType,strResellerCode,intCommisionRate,strSalesmanCode,intCustomerId,strEmail,strContact,strUser,strPass,intStatus,CreateDate,UpdateDate) 
		VALUES(0,".$intResellerType.",'".$strCode."',".$strCommisionRate.",'".$strSalesmanCode."',0,'".$strEmail."','".$strContact."','".$strUsername."','".md5($strPassword)."',1,'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
		
	  if ($this->query_id = $this->db->query($query)){
	    $id = mysql_insert_id();
		return $id;
	  }
	  else{
		return 0;
	  }
		
		
	}
	
	//
	// Do Get All Active Reseller Code
	//
	function doGetAllActiveResellerlists($intSalesCode){
		
		$query ="SELECT strResellerCode,strUser FROM reseller WHERE strSalesmanCode='".$intSalesCode."'";
		
		if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	} 
	

	
	
	
	
	//
	// Update Reseller Code
	//
	function doUpdateResellerCode($strResellerCode,$id,$strSalesmanCode){
		global $CONF;

		 $query ='UPDATE reseller                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 strResellerCode       ="'.$strResellerCode.	'"       
       WHERE 
      		intResellerId='.$id.'
		AND 
		     strSalesmanCode  = "'.$strSalesmanCode.'" '; 
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Do Update Customer Record
	//
	function doUpdateCustomer($intCustomerId){
		global $CONF;
		
		 $query ='UPDATE customer                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 	intReseller       =2       
	   WHERE 
      		intCustomerId='.$intCustomerId;
		
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
	
	//
	// Get All Customer By Salesman Id
	//
	function getAllCustomerDetailsBySalesmanCode($intSalesCode){
		$query = "SELECT * FROM customer WHERE strSalesmanCode='".$intSalesCode."'";
		
		if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
	
	//
	// Do Change Status Reseller
	//
	function doResellerStatusById($id,$intStatus){
		global $CONF;
		$query ='UPDATE reseller                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	   intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intResellerId='.$id; 

	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
	//
	// Check Reseller Username
	//	
	function doCheckUsernameReseller($strUsername){
		$query ="SELECT strUser FROM reseller WHERE  strUser='".$strUsername."'";
	
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
	}
	//
	// Do Update Point Reseller to Customer
	//
	function doUpdateResellertoCustomer($strResellerCode,$intCustomerId,$strSalesmanCode){
		global $CONF;
		$query ='UPDATE customer                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	   strIntroduceResellerCode        ="'.$strResellerCode.	'"                                      
       WHERE 
      		intCustomerId='.$intCustomerId.' AND strSalesmanCode="'.$strSalesmanCode.'"' ;
   
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
		
	}
	
	
	
	
	
}
?>