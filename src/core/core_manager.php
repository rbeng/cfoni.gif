<?php

class core_manager extends Record{
	function __construct(){
		$this->db = new DB();
		}

	//
	// Check Login 
	//
	function getLoginMember($user_id){
		$query = "SELECT strUser,intAccountManagerId,strPass FROM accountmanager WHERE intStatus=1 AND strUser='".$user_id."'";
	
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;	
	}
	//
	// Add Session
	//
	function addRecord($UsrName,$session){
		global $CONF;
		
		$query = "INSERT INTO accountmanager_session(strUser,strSession,CreateDate,UpdateDate) VALUES('".$UsrName."','".$session."', now(),now())";
			
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Delete Session
	//
	function deleteRecord($UsrName){
		$query = "DELETE FROM accountmanager_session  WHERE strUser='".$UsrName."'";
	
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Checking Session for log in user
	//
	function retrieveSession($module,$UsrName,$Session){
		$query = "SELECT * FROM ".$module." where strUser='".$UsrName."' AND strSession='".$Session."'";
		
		
		$data = array();
		if ($this->query_id = $this->db->query($query)){
			$this->GetNextRecord();
			if($this->Get('strUser')){
				$data['strUser']   = $this->Get('strUser'); 
				return $data;
			}
			else{
				return 0;		
				}
			}
	}
	//
	// Update the Last Login
	//
	function doUpdateLastlogin($intAccManagerId,$strNowLogin){
		global $CONF;
		
		$query ="UPDATE accountmanager                                                         
			SET strNowlogin    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			strLastlogin       = '".$strNowLogin."'       
			   WHERE 
      		intAccountManagerId=".$intAccManagerId; 
		
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Update the First Time Last Login
	//
	function doUpdateBothLogin($intAccManagerId){
		global $CONF;
		
		$query ="UPDATE accountmanager                                                         
			SET strLastlogin    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			 strNowlogin        = '".timestr(time(),$CONF['const_gmt_offset'])."'        
			   WHERE 
      		intAccountManagerId=".$intAccManagerId; 
		
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	
	
	//
	// Get All Salesman By Account Manager Id
	//
	function doGetAllSalesmanByAccManagerId($intPage=0,$intAccManagerId){
		$query = "SELECT * FROM salesman WHERE intAccountManagerId=".$intAccManagerId." AND intStatus!=5";
		
		if($intPage!=0) {				
			$this->GeneratePage($intPage, $query);	
			$this->Set('num_row',$this->page->total_list);
			} else {	
			if($this->query_id = $this->db->query($query)){
				$this->Set('num_row',$this->db->num_rows());
				return 1;
			}
		}
		return 0;
	}
	//
	// Get the Profile By Id
	//
	function doGetProfile($id){
		$query = "SELECT * FROM accountmanager  WHERE intAccountManagerId=".$id;
	 
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Get Lastlogin Value
	//
	function getLastlogin($intAccManagerId){
		$query = "SELECT strLastlogin,strNowLogin FROM accountmanager  WHERE intAccountManagerId=".$intAccManagerId;
	 
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Do Check Email Address
	//
	function doCheckEmailAdd($strEmail){
		$query = "SELECT intAccountManagerId FROM accountmanager WHERE  strEmail='".$strEmail."'";
	
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Do Update Profile
	//
	function doUpdateAccManagerProfile($args){
		global $CONF;
		
		if($args['strPassword1']!=""){
				$query ="UPDATE accountmanager                                                         
			SET UpdateDate    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			 strFullName      = '".$args['strFullName']."',
			 strContact       = '".$args['strContact']."', 
			 strPass       = '".$args['strPassword1']."', 
			 strEmail         = '".$args['strEmail']."'       
			   WHERE 
      		intAccountManagerId=".$args['id'];
		}
		else{
		
		$query ="UPDATE accountmanager                                                         
			SET UpdateDate    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			 strFullName      = '".$args['strFullName']."',
			 strContact       = '".$args['strContact']."', 
			 strEmail         = '".$args['strEmail']."'       
			   WHERE 
      		intAccountManagerId=".$args['id']; 
		}
		if($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
	}
	//
	// Do Add Salesman
	//
	function doAddSalesman($args){
		global $CONF;
		
		$strCode = "-";
		$query = "INSERT INTO  salesman(intSalesId,strSalesmanCode,strCommisionRate,intType,strName,strEmail,strContact,strUser,strPass,intAccountManagerId,CreateDate,UpdateDate) 
		VALUES(0,'".$strCode."',".$args['strCommisionRate'].",".$args['intType'].",'".$args['strFullName']."','".$args['strEmail']."','".$args['strContact']."','".$args['strUsername']."','".md5($args['strPassword'])."',".$args['intAccountManagerId'].",'".timestr(time(),$CONF['const_gmt_offset'])."','".timestr(time(),$CONF['const_gmt_offset'])."')";
		
	
	  if ($this->query_id = $this->db->query($query)){
	    $id = mysql_insert_id();
		return $id;
	  }
	  else{
		return 0;
	  }
		
	}
	//
	// Check Salesman Name
	//
	function doCheckUsername($strUsername){
		$query = "SELECT * FROM salesman WHERE  strUser='".$strUsername."'";
	
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	
	//
	// Check Salesman Emai;
	//
	function doCheckEmail($strEmail){
		$query = "SELECT * FROM salesman WHERE  strEmail='".$strEmail."'";
	
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Do Update Salesman Code
	//
	function doUpdateSalesman($strCode,$id){
		global $CONF;
		
		$query ="UPDATE salesman                                                         
			SET UpdateDate    = '".timestr(time(),$CONF['const_gmt_offset'])."' ,
			 strSalesmanCode      = '".$strCode."'
			   WHERE 
      		intSalesId=".$id; 
		
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Do Show Salesman
	//
	function doGetSalesmanDetailsById($id,$intAccManagerId){
		$query = "SELECT * FROM salesman WHERE intSalesId=".$id." AND intAccountManagerId=".$intAccManagerId;
		
		if ($this->query_id = $this->db->query($query))
			return 1;
		else
			return 0;
		
	}
	//
	// Do Delete Salesman
	//
	function doDeleteSalesmanById($id,$strAddedBy){
		global $CONF;
		
		$intStatus = 5;
		 $query ='UPDATE salesman                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 intAccountManagerId       ='.$strAddedBy.	' ,      
		     intStatus        ='.$intStatus.	'                                      
       WHERE 
      		intSalesId='.$id; 
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	//
	// Update More Salesman Record
	//
	function doUpdateSalesmanRecord($args){
		global $CONF;
		
		if($args['strPassword']!=""){
		 $query ='UPDATE salesman                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 intAccountManagerId       ='.$args['intAccountManagerId'].	' ,      
		     intStatus        ='.$args['intStatus'].	'   ,
			 strName    = "'.$args['strFullName'].'",        
			 strEmail    = "'.$args['strEmail'].'", 
			 intType        ='.$args['intType'].	' ,
			 strCommisionRate = "'.$args['strCommisionRate'].'",  
			 strPass          = "'.md5($args['strPassword']).'"                                       
       WHERE 
      		intSalesId='.$args['id']; 
			
		}
		else{
			 $query ='UPDATE salesman                                                         
			SET UpdateDate    = "'.timestr(time(),$CONF['const_gmt_offset']).'",          
		  	 intAccountManagerId       ='.$args['intAccountManagerId'].	' ,      
		     intStatus        ='.$args['intStatus'].	'   ,
			 strName    = "'.$args['strFullName'].'",        
			 strEmail    = "'.$args['strEmail'].'", 
			 intType        ='.$args['intType'].	' ,
			 strCommisionRate = "'.$args['strCommisionRate'].'"  
			                       
       WHERE 
      		intSalesId='.$args['id']; 
			
		}
			
	
			
	if ($this->query_id = $this->db->query($query))
		return 1;
	else
		return 0;
		
	}
	
	
}
?>