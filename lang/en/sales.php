<?php
global $LANG,$Q,$CONF;

	$lang = array(	
	
	'err_product_name'				=> 'Item name cannot be empty.',
	'err_price_name'				=> 'Item price cannot be empty.',
	'err_code_name'				=> 'Item code cannot be empty.',
	//
	// <!--URL m=main&c=show_main -->
	//
	'no_data'				=> 'No data.',
	'contact_no'			=> 'Contact:',
	'get_code'				=> 'Ad Code:',
	'get_date'				=> 'Date:',
	
	//
	// <!--URL m=main&c=show_error_page -->
	//
	'Title_Reminder'  => 'Reminder - Please Register',
	'error_pages'			=> 'To use our services, you first need to register with '.SITENAME.'. So fret not, it\'s just an easy one-time registration process and you can start posting ads right away!
								<br /><br />
								If you do not wish to register, you can still place ads via SMS by texting your message to '.short_code.' (currently applies to Maxis users only) and have details displayed to our network of Digital Community Boards (terms & conditions apply), plus 30 days online automatically free.
								<br /><br />
								If you have already posted an ad via SMS search for your ad by entering your Ad Code in our search box on our landing page.',
	
	//
	// <!--URL m=main&c=show_about -->
	//
	'show_about_sms2board'	=> 'Your local community board now takes a digital leap with '.SITENAME.'.
								<br /><br />
								Have an item for sale, a place to let, or a service to offer? Perhaps you\'d simply like to say "I love you"? '.SITENAME.' lets you do this in a new, novel way via SMS and the Web. In a nutshell, it\'s classified advertising made simple.
								<br /><br />
								All it takes is a quick text message to "'.short_code.'" and your classified ad will make its way to dozens of digital community bulletin boards strategically placed at the premises of our community partners throughout the Klang Valley. Your messages and pictures (which can be placed via the Web) won\'t just be viewed by an online audience, but by the general public.
								<br /><br />
								Just text your message from your phone or access '.SITENAME.' online for more features. It\'s as quick, easy and simple as that!
								<br /><br />
								'.SITENAME.'... Your world for sale, at your fingertips.
								<br /><br />',
	
	//
	// <!--URL m=main&c=show_contact -->
	//
	'Title_contact'             => 'Contact Us',
	'show_contact'			=> 'Do you have burning questions that need answers? Or feedback to help us serve you better? Feel free to get in touch with us and we\'ll get back to you shortly. Your comments are appreciated.',
	'fields_required'		=> 'All fields are required.',
	'alternative_contact'	=> 'Alternatively, contact us at 1300-88-0027.',
	'form_name'				=> 'Name :',
	'form_contact'			=> 'Contact :',
	'form_email'			=> 'Email :',
	'form_comment'			=> 'Your comments / enquiries :',
	'form_display_code'		=> 'Please enter the displayed security code:',
	'form_security_code'	=> 'Security Code :',
	
	//
	// <!--URL m=main&c=c=show_success -->
	//
	'show_success_contact_us'=> 'Thank you for contacting us and we will get back to you shortly. Your feedback is important to help us serve you better.
								<br /><br />
								Warm regards,<br />
								The '.SITENAME.' Team',
	
	//
	// <!--URL m=main&c=show_how_to_sms -->
	//
	'title_function'		=> 'FUNCTIONS',
	'description_function'	=> 'To interact with '.SITENAME.', send any of the following SMS codes to <strong>'.short_code.'</strong>.',

	'table_code_code'		=> 'Code',
	'table_code_do'			=> 'What does it do?',
	'table_code_example'	=> 'Example',
	/* EXAMPLE */												
	'EXAMPLE_1'				=> 'CS &lt;NETWORK&gt; &lt;DAY CODE&gt; &lt; MESSAGE&gt;',								
	'EXAMPLE_1.1'			=> 'Post an ad to a network for a specified number of days. An SMS placed will find its way to our Digital Community Boards.',							
	'EXAMPLE_1.1.1'			=> 'CS BA D6 TV for sale 20 inch, brand new, call Kevin 016 888 6666',					
	'EXAMPLE_2'				=> 'CS  WEB &lt;PAYMENT CODE&gt;',					
	'EXAMPLE_2.1'			=> 'Make a payment for premium ads posted via the Web using a randomly generated payment code.',						
	'EXAMPLE_2.1.1'			=> 'CS  WEB 64s98',							
	'EXAMPLE_3'				=> 'CS  GET &lt;AD CODE&gt;',				
	'EXAMPLE_3.1'			=> 'Download an ad to your mobile phone using the Ad Code.',						
	'EXAMPLE_3.1.1'			=> 'CS GET 74s798',	

								
	'title_network'			=> 'NETWORK',
	'description_network'	=> 'Click <a href="'.$CONF['url_app'].'?m=network&amp;c=show_main_network" target="_self" class="a_link_blue">here</a> to view our list of available networks and their respective codes e.g. Network \'Bangsar\' has the code of \'BA\'.',
								
	'title_day_code'		=> 'DAY CODE',
	'description_day_code'	=> 'Choose to advertise from one (1) to six (20) days.',
	'note_day_code'			=> '*Note: You can omit the Day Code for a one (1) day Ad by entering shortcut values i.e. CS &lt;NETWORK&gt; &lt;MESSAGE&gt;',
		
	'table_day_no'			=> 'Number of Days',
	'table_day_code'		=> 'Day Code',
	/* DAYCODE */												
	'DAYCODE_1'				=> '1',								
	'DAYCODE_1.1'			=> 'D1',					
	'DAYCODE_2'				=> '2',					
	'DAYCODE_2.1'			=> 'D2',						
	'DAYCODE_3'				=> '3',				
	'DAYCODE_3.1'			=> 'D3',						
	'DAYCODE_4'				=> '...',						
	'DAYCODE_4.1'			=> '...',						
	'DAYCODE_5'				=> '20',			
	'DAYCODE_5.1'			=> 'D20',
								
	'title_rates'			=> 'RATES',
	'description_rates'		=> 'The following are the SMS charges which will be reflected on your monthly phone bills.',
	'note_rates'			=> 'Note: Standard telco charges apply.',
	
	'table_rates_function'	=> 'SMS Functions',
	'table_rates_price'		=> 'Price',
	/* DAYCODE */												
	'RATES_1'				=> 'Post an ad via SMS',								
	'RATES_1.1'				=> 'RM0.50 for each day',					
	'RATES_2'				=> 'Pay for an ad posted via Web',					
	'RATES_2.1'				=> 'RM0.50 for each day',						
	'RATES_3'				=> 'Download an ad to your mobile',				
	'RATES_3.1'				=> 'RM0.50 per download',						
	'RATES_4'				=> 'Validate your mobile',						
	'RATES_4.1'				=> 'FREE',
	
	'description_help'		=> '<b> Need more help ? Click <a href="'.$CONF['url_app'].'?m=main&c=show_faq">FAQ</a></b>',
	
	//
	// <!--URL m=main&c=show_terms -->
	//
								
	/* TnC */		
	'Title'					=> 'Terms of Service',					
	'TNC_1'					=> 'Acceptance of Terms and Conditions',
	'TNC_1.1'				=> ''.$CONF['company_name'].', a company incorporated in Malaysia (Company Registration Number: 793604-D) ("'.$CONF['company_name2'].'") 
								manages the internet portal '.$CONF['website'].' ("<strong>Website</strong>") and the Services offered on the Website, subject to the 
								following Terms and Conditions ("<strong>Terms</strong>"). The "<strong>Services</strong>" means the provision of the advertisement 
								broadcasting services on the Network by MBoard, as further described under Clause 3 below.',
	'TNC_1.2'				=> 'The Terms apply to your access and use of the Website and/or the Services. By accessing any page of the Website and/or using the 
								Services, you agree to be bound by the Terms immediately without limitation or qualification. Without limiting the foregoing, exercise 
								of either of the options under Clauses 3.2(i), 3.2(ii) and/or 3.8 below shall constitute "use" of the Services. If you do not accept 
								the Terms, please discontinue your access to the Website and/or use of the Services.',
	'TNC_1.3'				=> 'If you are registering and/or using the Website and/or the Services on behalf of a business entity, you represent and warrant that you 
								have full legal authority to bind the entity to the Terms. If you do not have the legal authority to bind such entity to the Terms, 
								please discontinue your access to the Website and/or use of the Services.',
	'TNC_2'					=> 'Updates to Terms and Conditions',
	'TNC_2.1'				=> ''.$CONF['company_name2'].' reserves the right to update the Terms at any time by giving you notice by posting the updated Terms on the 
								Website. You expressly acknowledge and agree that your continued use of the Website and/or the Services after such update constitutes 
								an acceptance of and agreement to be bound by the updated Terms. Once posted on the Website, the updated Terms will apply to all users 
								from the date of posting. You shall be responsible for reviewing the current version of the Terms each time you visit the Website 
								and/or use the Services.',
	'TNC_3'					=> 'The Services',
	'TNC_3.1'				=> ''.$CONF['company_name2'].' operates an extensive advertising network consisting of:-
								<br />
								<ol style="list-style-type:lower-roman">
								<li>computer programmable electronic display boards at various locations ("<strong>Location</strong>") ("<strong>Media Boards</strong>"); and</li>
								<li>the Website.</li>
								</ol>
								The Media Boards and the Website shall together form the "<strong>Network</strong>".<br><br><em>Paid Advertisements</em>',
	'TNC_3.2'				=> 'You can post on advertisement on the Network by exercising any of the following options:
								<br />
								<ol style="list-style-type:lower-roman">
								<li><u>Via SMS</u>. Send a SMS to '.short_code.' with the SMS code CS&lt;space&gt;NETWORK&lt;space&gt;D(No. of days)&lt;space&gt;
								MESSAGE; AND OR</li>
								<li><u>Via the Website</u>. In order to post an advertisement via the Website, you are required to register by filling up the 
								<a href="'.$CONF['url_app'].'?m=main&c=show_register&KeepThis=true&TB_iframe=true&height=500&width=650" 
								class="thickbox a_link_blue" style="font-size:12px"><u>Registration Form</u></a>. Registration is free of charge. As soon as your 
								registration is verified, you may submit your advertisement by following the procedure as set out 
								<a href="'.$CONF['url_app'].'?m=main&c=show_how_to_sms" style="font-size:12px"><u>here</u></a>.</li>
								</ol>',
	'TNC_3.2_pop'			=> 'You can post on advertisement on the Network by exercising any of the following options:
								<br />
								<ol style="list-style-type:lower-roman">
								<li><u>Via SMS</u>. Send a SMS to '.short_code.' with the SMS code 
								CS&lt;space&gt;NETWORK&lt;space&gt;D(No. of days)&lt;space&gt;MESSAGE; AND OR</li>
								<li><u>Via the Website</u>. In order to post an advertisement via the Website, you are required to register by filling up the 
								<u>Registration Form</u>. Registration is free of charge. As soon as your registration is verified, you may submit your advertisement 
								by following the procedure as set out <u>here</u>.
								</li>
								</ol>',
	'TNC_3.3'				=> 'Once you have complied with the procedure as set out in Clause 3.2(i) and/or 3.2(ii) above and remitted payment for your 
								advertisement , you will receive a confirmation code ("<strong>Confirmation Code</strong>") via SMS. You shall retain the Confirmation 
								Code as proof of submission of your advertisement.',
	'TNC_3.4'				=> 'Placement of advertisements on the Network in either manner as described in Clause 3.2(i) and/or 3.2(ii) ("<strong>Paid Advertisements
								</strong>") shall be subject to the charges as set forth <u>here</u>.',
	'TNC_3.5'				=> 'Each Paid Advertisement shall be displayed on the Website for a minimum duration of ninety (90) Days and displayed on the Media Boards 
								for the number of days as per your request pursuant to the procedure under Clause 3.2 above ("<strong>Display Duration</strong>"). In 
								relation to display cycle on the Media Board, the Paid Advertisements shall be displayed on the Media Boards for thirty (30) seconds 
								every hour for the duration of the opening hours of the Locations in which the Media Boards  are located ("<strong>Display Cycle
								</strong>") . MBoard reserves the right to vary or amend the Display Duration and Display Cycle at its sole discretion without 
								providing you prior notice.',
	'TNC_3.6'				=> 'For the avoidance of doubt, Paid Advertisements submitted before 5pm will appear on the Media Boards the next day. Paid Advertisements 
								submitted after 5pm, will appear on the Media Boards the day after next.<br /><br /><em>Free Advertisements</em>',
	'TNC_3.7'				=> 'You may also post advertisements on the Website free of charge by following the registration procedure as set forth in Clause 3.2(ii) 
								above. As soon as your registration is verified, you may submit your advertisement by following the procedure as set out 
								<a href="'.$CONF['url_app'].'?m=ads&c=show_main_ads" style="font-size:12px"><u>here</u></a> ("<strong>Free Advertisements</strong>").',
	'TNC_3.7_pop'			=> 'You may also post advertisements on the Website free of charge by following the registration procedure as set forth in Clause 3.2(ii) 
								above. As soon as your registration is verified, you may submit your advertisement by following the procedure as set out <u>here</u> ("
								<strong>Free Advertisements</strong>").',
	'TNC_3.8'				=> 'Free Advertisements shall only be displayed on the Website and not on the Media Boards.',
	'TNC_3.9'				=> 'Each Free Advertisement shall be displayed on the Website for a minimum duration of ninety (90) days. '.$CONF['company_name2'].' 
								reserves the right to vary or amend the duration of the display of the Free Advertisement on the Website at its sole discretion without 
								providing you prior notice.',
	'TNC_4'					=> 'Use of the Services',
	'TNC_4.1'				=> '<u><em>Membership and Registration</em></u>',
	'TNC_4.1.1'				=> 'In order to exercise the option under Clause 3.2(ii), you are required to register by filling up the 
								<a href="'.$CONF['url_app'].'?m=main&c=show_register&KeepThis=true&TB_iframe=true&height=500&width=650" 
								class="thickbox a_link_blue" style="font-size:12px"><u>Registration Form</u></a>. You hereby agree and warrant that:<br />
								<ol style="list-style-type:lower-roman">
								<li>that all information provided by you in the Registration Form ("<strong>Information</strong>") is true, accurate, current 
								and complete; and</li>
								<li>that you will maintain and promptly update the Information if any of the Information becomes untrue, inaccurate, current and 
								incomplete.</li>
								</ol>',
	'TNC_4.1.1_pop'			=> 'In order to exercise the option under Clause 3.2(ii), you are required to register by filling up the <u>Registration Form</u>. 
								You hereby agree and warrant that:<br />
								<ol style="list-style-type:lower-roman">
								<li>that all information provided by you in the Registration Form ("<strong>Information</strong>") is true, accurate, current 
								and complete; and</li>
								<li>that you will maintain and promptly update the Information if any of the Information becomes untrue, inaccurate, current and 
								incomplete.</li>
								</ol>',
	'TNC_4.1.2'				=> ''.$CONF['company_name2'].' has the right to suspend or terminate your registration and refuse any and all current or future use of the 
								Website and/or the Services (or any part thereof) if '.$CONF['company_name2'].' has reasonable grounds to believe that any Information 
								provided by you is untrue, inaccurate, not current or incomplete.',
	'TNC_4.2'				=> '<u><em>User ID and Password</em></u>',
	'TNC_4.2.1'				=> 'You will be required to select a User ID and a password upon completion of the Registration Form. You are responsible for all actions 
								and activities taken under that User ID and password and it is your responsibility to keep your User ID safe and to maintain the 
								confidentiality of the User ID and password. You have no right to disclose or transfer your account or User ID to any other person.',
	'TNC_4.2.2'				=> 'You shall ensure that you logout at the end of each session and you shall immediately notify '.$CONF['company_name2'].' of any 
								unauthorized use of your User ID and/or password or any other breach of security. '.$CONF['company_name2'].' will not be held liable 
								for any loss or damage arising out of your failure to comply with the provisions under this Clause 4 or for any unauthorized use of 
								your User ID and/or password.',
	'TNC_4.3'				=> '<u><em>Submitted Advertisements</em></u>',
	'TNC_4.3.1'				=> 'Paid Advertisements and Free Advertisements shall be collectively referred to as "<strong>Submitted Advertisements</strong>". Except 
								where otherwise indicated, where there is reference to "Submitted Advertisements" in any of the clauses herein, such clauses shall 
								apply to both Paid Advertisements and Free Advertisements.',
	'TNC_4.3.2'				=> 'You agree to use the Services for lawful purposes and in a way that does not infringe the rights of, restrict or inhibit anyone else\'s 
								use and enjoyment of the Services. Prohibited behaviour includes transmitting advertisements or content which are obscene, offensive, 
								fraudulent, immoral or in contravention of any laws, regulations and/or codes in Malaysia.',
	'TNC_4.3.3'				=> 'All Submitted Advertisements will go through a screening and moderation process in order to assess the Submitted Advertisement(s) 
								suitability for display on the Network. '.$CONF['company_name2'].' reserves the right to refuse to broadcast or remove any Submitted 
								Advertisement on the Network, which '.$CONF['company_name2'].', at its sole discretion considers to be obscene, offensive, fraudulent, 
								immoral or in contravention of any laws, regulations and/or codes in Malaysia.',
	'TNC_4.3.4'				=> 'In the event any Paid Advertisement fails to be broadcasted on the Network, '.$CONF['company_name2'].' shall refund in full the 
								fees paid for the said  Paid Advertisement. In order to claim the refund, the party claiming the refund shall be required to provide 
								details of the Confirmation Code at '.$CONF['company_name2'].'\'s office at Room 16.01, 16th Floor, Campbell Complex, 98 Jalan Dang 
								Wangi, 50100 Kuala Lumpur.',
	'TNC_4.3.5'				=> 'Notwithstanding the refund provision in Clause 4.3.4, '.$CONF['company_name2'].' shall not provide a refund for any Submitted 
								Advertisement (if applicable) which is not broadcasted on the Network in the following circumstances:-
								<br />
								<ol style="list-style-type:lower-roman">
								<li>where such Submitted Advertisement considered by '.$CONF['company_name2'].' to be obscene, offensive, fraudulent, immoral or in 
								contravention of any laws, regulations and/or codes in Malaysia;</li>
								<li>where such Submitted Advertisement is not in compliance with the Terms;</li>
								<li>where such failure to broadcast the Submitted Advertisement is not due to the sole fault of '.$CONF['company_name2'].';</li>
								<li>where such failure to broadcast the Submitted Advertisement is due to an event beyond the control of '.$CONF['company_name2'].', 
								including power shortages, network interruptions, network failure or interruption of the internet connection etc.</li>
								</ol>',
	'TNC_4.4'				=> '<u><em>Suspension of the Services</em></u>',
	'TNC_4.4.1'				=> ''.$CONF['company_name2'].' reserves the right at any time to modify or discontinue, temporarily or permanently, the Services or any 
								part thereof with or without notice. You agree that '.$CONF['company_name2'].' shall not be liable to you or to any third party for any 
								modification, suspension or discontinuance of the Services.',
	'TNC_4.4.2'				=> 'The Services are not available to, and may not be used by individuals who cannot form legally binding contracts under applicable 
								law. Without limiting the foregoing, the Services are not available to persons, to temporarily or indefinitely suspended users of 
								the Website and/or the Services or persons barred from receiving the Services under the laws of Malaysia or other 
								applicable jurisdiction.',
	'TNC_5'					=> 'Reliance on the Submitted Advertisements displayed on the Network',
	'TNC_5.1'				=> 'Your use and reliance on the Submitted Advertisements displayed on the Network ("<strong>Displayed Advertisements</strong>") or the 
								information contained therein is entirely at your own risk. The Displayed Advertisements are provided on the Network for your 
								convenience, but they are beyond the control of '.$CONF['company_name2'].' and '.$CONF['company_name2'].' is not responsible or liable 
								for the content or the accuracy of the information contained in the Displayed Advertisement. No endorsement of the Displayed 
								Advertisements or the content therein is implied. '.$CONF['company_name2'].' has not verified the content of the Displayed 
								Advertisements. It is recommended that you verify the content of the Displayed Advertisements before use of or reliance on the same.',
	'TNC_5.2'				=> ''.$CONF['company_name2'].' is not responsible for the accuracy or the continued availability of the Displayed Advertisements. The 
								Displayed Advertisements are for knowledge and convenience only and '.$CONF['company_name2'].' is not responsible or liable for any 
								damages, losses, difficulties or consequences suffered as a result of your use of and reliance on the Displayed Advertisements.',
	'TNC_6'					=> 'Use of the Website and/or Servies',
	'TNC_6.1'				=> 'You agree to use the Website and/or Services for lawful purposes and in a way that does not infringe the rights of, restrict or inhibit 
								anyone else\'s use and enjoyment of the Website and/or the Services. Prohibited behaviour includes harassing or causing distress or 
								inconvenience to any person, transmitting obscene or offensive content or disrupting the Website and/or Services.',
	'TNC_7'					=> 'Intellectual Property',
	'TNC_7.1'				=> 'All copyright, trade marks, design rights, patents and other intellectual property rights (registered and unregistered) in and on the 
								Website and all content (including all applications) located on the Website shall remain vested in '.$CONF['company_name2'].' or its 
								licensors. You may not copy, reproduce, republish, disassemble, decompile, reverse engineer, download, post, broadcast, transmit make 
								available to the public or otherwise use the content of the Website and/or the Services in any way except for personal and 
								non-commercial use. ',
	'TNC_7.2'				=> 'The names, images and logos identifying MBoard and/or third parties and their products and services are subject to copyright, design 
								rights and trade marks of '.$CONF['company_name2'].' and/or such third parties. Nothing contained in this Terms or on the Website shall 
								be construed as granting any licence or right to use any trade mark, design right or copyright of '.$CONF['company_name2'].' and/or any 
								third party.',
	'TNC_8'					=> 'External Links',
	'TNC_8.1'				=> 'External links may be provided on the Website for your convenience, but they are beyond the control of '.$CONF['company_name2'].' 
								and '.$CONF['company_name2'].' is not responsible for the content of any other websites or pages linked to or from the Website. No 
								endorsement of third party websites or content is implied. '.$CONF['company_name2'].' has not verified the content of any such websites 
								or pages. Use or reliance on any external links and any content contained on such external links is at your own risk. It is recommended 
								that you view the linked website\'s terms or privacy policy pages to understand how use of that website may affect you.',
	'TNC_8.2'				=> ''.$CONF['company_name2'].' is not responsible for the accuracy, timeliness or the continued availability or the existence of content, 
								hyperlinks or third party websites or pages linked to the Website. Links to downloadable software sites are for convenience only and '.
								$CONF['company_name2'].' is not responsible or liable for any difficulties or consequences associated with downloading the software. 
								Use of any downloaded software is governed by the terms of the licence agreement, if any, which accompanies or is provided with 
								the software.',
	'TNC_8.3'				=> 'No hypertext links shall be created from any website to this website without prior written permission of '.$CONF['company_name2'].'. 
								Please contact us if you wish to link to the Website or would like to request a link to your website.',
	'TNC_9'					=> 'No Warranties',
	'TNC_9.1'				=> 'To the greatest extent permitted by applicable law and your local laws, '.$CONF['company_name2'].' does not warrant or represent that 
								the information contained on the Network is accurate, complete, correctly sequenced, reliable or timely.',
	'TNC_9.2'				=> 'Further '.$CONF['company_name2'].' does not warrant or represent that the display of the Submitted Advertisements on the Network will 
								be uninterrupted, continuous, correctly sequenced, reliable or timely. In the event of defect, failure or interruption occurring in 
								four (4) or more Locations, '.$CONF['company_name2'].' shall use all reasonable efforts to ensure that such defect, failure or 
								interruption in each Location is rectified within a reasonable timeframe upon notification of such defect, failure or interruption 
								to '.$CONF['company_name2'].'.',
	'TNC_9.3'				=> 'To the greatest extent permitted by applicable law and your local laws, MBoard does not warrant or represent that the information 
								contained on the Website is accurate, complete, correctly sequenced, reliable or timely or that the Website will be uninterrupted 
								or free of errors and/or viruses. Use of the Website is at your sole risk. You have sole responsibility for adequate protection and 
								back up of data and/or equipment and for undertaking reasonable and appropriate precautions to scan for computer viruses or 
								other destructive properties.',
	'TNC_9.4'				=> 'The information and materials contained in the Website and on the Network, including the Displayed Advertisements, text, graphics, 
								links or other items are provided "as is" and "as available". To the greatest extent permitted by applicable law and your local 
								laws, '.$CONF['company_name2'].' makes no warranties, representations, statements or guarantees, whether express or implied regarding 
								the Website, Displayed Advertisements, the Services and/or the Network, the information contained on the Website and/or the Network, 
								the security of you or your company\'s personal information, material, information and/or submissions transmitted through the Website.',
	'TNC_9.5'				=> 'To the greatest extent permitted by applicable law and your local laws, '.$CONF['company_name2'].' makes no warranty or representation 
								that the information provided on the Website and/or the Services and/or the Displayed Advertisements is appropriate for use in any 
								jurisdiction. By accessing the Website and/or using the Services and/or the Displayed Advertisements, you warrant and represent that 
								you are legally entitled to do so and to make use of the information contained on the Website and/or the Services.',
	'TNC_10'				=> 'Disclaimer of Liability',
	'TNC_10.1'				=> 'To the greatest extent permitted by applicable law and your local laws, '.$CONF['company_name2'].' shall not be responsible for 
								and disclaims all liability for any loss, liability, damage (whether direct, indirect, special, incidental or consequential), 
								personal injury or expense of any nature whatsoever which may be suffered by you or any third party (including your company), as 
								a result of or which may be attributable, directly or indirectly, to your access and use of the Website, the Services, the Network 
								and/or the Displayed Advertisements, any information contained on the Website, the Services, the Network and/or the Displayed 
								Advertisements, you or your company\'s personal information, material, information, submissions or Submitted Advertisement(s) 
								transmitted through the Website and/or through SMS, even if you advise '.$CONF['company_name2'].' of the possibility of such damages, 
								losses or expenses.',
	'TNC_10.2'				=> 'To the greatest extent permitted by applicable law and your local laws, '.$CONF['company_name2'].' shall not be responsible for and 
								disclaims all liability for any loss, liability, damage (whether direct, indirect, special, incidental or consequential), personal 
								injury or expense of any nature whatsoever, whether caused by the negligence of '.$CONF['company_name2'].', its employees or otherwise 
								arising in connection with the Website, the Services, the Network and/or the Displayed Advertisements, even if you advise 
								'.$CONF['company_name2'].' of the possibility of such damages, losses or expenses.',
	'TNC_11'				=> 'Termination',
	'TNC_11.1'				=> 'You agree that '.$CONF['company_name2'].' may, in its sole discretion, without cause and without prior notice, immediately terminate 
								and/or suspend your account, and refuse any and all current or future use of the Website and/or the Services (or any part thereof) 
								without any liability whatsoever. Cause for such termination and/or suspension shall include, without limitation:
								<br />
								<ol style="list-style-type:lower-alpha">
								<li>breaches or violations or purported breaches or violations of the Terms or other incorporated agreements or guidelines,</li>
								<li>your use of the Website and/or Services is deemed by '.$CONF['company_name2'].' to be disruptive, inappropriate and infringing the 
								rights of, restrict or inhibiting anyone else\'s use and enjoyment of the Website and/or the Services. Such disruptive and 
								inappropriate use of the Website and/or Services include without limitation, spamming and submission of advertisements considered by 
								'.$CONF['company_name2'].' to be obscene, offensive, fraudulent, immoral or in contravention of any laws, regulations and/or codes 
								in Malaysia,</li>
								<li>requests by law enforcement or other government agencies,</li>
								<li>a request by you (self-initiated account deletions),</li>
								<li>discontinuance or material modification to the Website, the Services and/or the Network (or any part thereof),</li>
								<li>unexpected technical or security issues or problems,</li>
								<li>extended periods of inactivity, and/or</li>
								<li>engagement by you in fraudulent or illegal activities.</li>
								</ol>',
	'TNC_11.2'				=> 'Termination of your account includes:
								<br />
								<ol style="list-style-type:lower-alpha">
								<li>removal of access to the Website and/or the Services;</li>
								<li>deletion of your password and all related information, files and content associated with or inside your account (or any part 
								thereof), and</li>
								<li>barring of further use of the Website and/or the Services.</li>
								</ol>',
	'TNC_12'				=> 'General',
	'TNC_12.1'				=> '<strong>Entire Agreement.</strong> These Terms constitute the agreement between you and '.$CONF['company_name2'].' in relation to your 
								access and use of the Website and/or the Services. Unless otherwise specifically stated these Terms shall take precedence over all 
								prior commitments, undertakings or representations, whether written or oral between you and '.$CONF['company_name2'].' with respect 
								to your use of the Website and/or the Services.',
	'TNC_12.2'				=> '<strong>Applicable Law.</strong> The Terms shall be governed by and construed in accordance with the laws of Malaysia without giving 
								effect to any principles of conflict of law. You hereby consent to submit to the exclusive jurisdiction of the Courts of Malaysia in 
								respect of any disputes arising in connection with the Terms, the Website and/or the Services or any matter related to or in 
								connection therewith.',
	'TNC_12.3'				=> '<strong>Electronic Form.</strong> A printed version of the Terms and of any notice given in electronic form shall be admissible in 
								judicial or administrative proceedings based upon or relating to these terms and conditions to the same extent and subject to the same 
								conditions as other business documents and records originally generated and maintained in printed form.',
	'TNC_12.4'				=> '<strong>Severability of Terms.</strong> If any of these terms are determined to be illegal, invalid or otherwise unenforceable by 
								reason of the laws of any state or country in which these terms are intended to be effective, the parties nevertheless agree that the 
								court should endeavour to give effect to the parties\' intentions as reflected in the provisions and to construe such provision to 
								the maximum extent permitted by law so as to render that provision valid and enforceable, and the other provisions of the Terms to 
								remain in fill force and effect. ',
	'TNC_12.5'				=> '<strong>Waiver.</strong> The failure or delay of '.$CONF['company_name2'].' to exercise or enforce any right in the Terms does not 
								waive '.$CONF['company_name2'].'\'s right to enforce that right. ',
	'TNC_12.6'				=> 'Additional Terms. You agree that your use of the Website and/or the Services is also subject to the Privacy Policy which can be viewed 
								at <a href="'.$CONF['url_app'].'?m=main&amp;c=show_privacy" style="font-size:12px">Privacy Policy</a>',
	'TNC_12.6_pop'			=> 'Additional Terms. You agree that your use of the Website and/or the Services is also subject to the Privacy Policy which can be viewed 
								at Privacy Policy',
	
	/* END TnC*/					
	
	//
	// <!--URL m=main&c=show_privacy -->
	//					
	/* PRIVACY */	
	'Title_Privacy'   => 'Privacy Policy',											
	'PRIVACY_1'				=> '<p class="pdbottom10 dark_grey">This Privacy Policy explains MBoard\'s policy relating to MBoard\'s use of any information collected or received by MBoard from your use of the Website and/or the Services. By using the Website and/or the Services, registering your user account information for the Services or using services provided on the Website, you agree that MBoard may use such user account information or any information MBoard receives from you in any way.</p>								
	
								<p class="pdbottom10 dark_grey">MBoard reserves the right to update this Privacy Policy at any time by giving you notice by posting the updated Privacy Policy on the Website. You expressly acknowledge and agree that your continued use of the Website and/or the Services and/or use of your user account after such update constitutes an acceptance of and agreement to be bound by the updated Privacy Policy. Once posted on the Website, the updated Privacy Policy will apply to all users from the date of posting. You shall be responsible for reviewing the current version of the Privacy Policy each time you visit the Website and/or use the Services.</p>',
	
	'PRIVACY_2'				=> '<h1>User Account Information</h1>',
	'PRIVACY_2.1'			=> '<p class="pdbottom10 dark_grey">When you register with the Website for the Services, you agree to provide MBoard with information to you including but not limited to name, address, email address, mobile number, birth date, gender, occupation and industry ("Information").</p>
								
								<p class="pdbottom10 dark_grey">You agree and consent to MBoard sharing and transferring your Information to third parties including to third parties outside Malaysia. Such third parties include but are not limited to content providers, advertisers, technology partners, applications providers and other suppliers of products or services that MBoard may contract with.</p>
								
								<p class="pdbottom10 dark_grey">You may edit your Information by logging into your account on the Website using your username and password.</p>
								
								<p class="pdbottom10 dark_grey">Upon termination of your user account with MBoard, your Information will not be held in storage for longer than necessary by MBoard.</p>',
	
	'PRIVACY_3'				=> '<h1>Storage</h1>',
	'PRIVACY_3.1'			=> '<p class="pdbottom10 dark_grey">To safeguard your personal data, all electronic storage and transmission of user account information are stored in a secure infrastructure and adheres to a standard security guideline.</p>',
	
	
	'PRIVACY_4'				=> '<h1>Cookies</h1>',
	'PRIVACY_4.1'			=> '<p class="pdbottom10 dark_grey">You hereby agree and acknowledge that MBoard or any third parties providing services on the Website may use cookies which may record data relating to you.</p>
								
								<p class="pdbottom10 dark_grey">These cookies may include \'per session\' cookies which are cookies that remain in the cookie file of your computer until you close your internet browser.</p>
								
								<p class="pdbottom10 dark_grey">These cookies may also include \'persistent\' cookies which permanently remain on the cookie file of your computer.</p>
								
								<p class="pdbottom10 dark_grey">Such cookies may be used to record details of pages relating to services you have visited on the Website. You hereby agree and grant permission to MBoard to use the information collected by the use of such cookies in any way MBoard wishes</p>.',
	
	'PRIVACY_5'				=> '<h1>Feedback</h1>',
	'PRIVACY_5.1'			=> '<p>If you have any feedback, suggestions or inquiries, please email MBoard at feedback@blufroge.com or telephone MBoard\'s hotline at 1300-88-0027.</p>',
	
	
	
	
	
	
	
	//
	// <!--URL m=main&c=show_comunity -->
	//					
	/* PRIVACY */	
	'Title_Community'   => '<h2 class="pink pdbottom20">Community Guidelines</h2>',											
	'Community_1'		=> '<h1>Community</h1><p>blufroge is a multiplatform network based on the values of communities. By leveraging on the mobile phone, the internet and digital signage, we aim to create vibrant communities filled with creativity, discussions, collaborations, trades, events, happenings, learning, sharing and more. Presently in its infancy, we are all ears, happy listening to feedbacks and suggestions as to how best to fully utilize the technology at hand and to maximize its potential. blufroge hopes to be shaped by you; it is the people and communities of users who will determine the evolution of this platform, bit by bit, function by function, into a truly dynamic and productive platform.</p>  ',
	
	'Community_2'		=> '<h1>Guidelines</strong>',
	'Community_2.1'		=> 'blufroge provides you the means to broadcast your message and reach out to a wide audience base in both public and private venues, as well as over the internet. To keep the community a fun and safe place for our users, everyone using blufroge has agreed to follow our Terms of Service, understand our Privacy Policy and adhere to our guidelines, which are:',
	
	'Community_3'		=> '<p class="zfont_TB font15 pdleft20">Responsibility</p>',
	'Community_3.1'		=> '<ul class="square"><li>You are responsible for every post and comment that you make on blufroge and this includes links, photos and videos.  So, be careful with the information that you share with others because it will be broadcasted to the online community as well as on the ground.</li></ul>',
	
	'Community_4'		=> '<p class="zfont_TB font15 pdleft20">Be good </p>',
	'Community_4.1'		=> '<ul class="square">
							<li>You agree to use the services for lawful purposes and in a way that does not infringe the rights of, restrict or inhibit anyone else\'s use and enjoyment of the services. Prohibited behaviour includes transmitting advertisements or content which are obscene, offensive, fraudulent, immoral or in contravention of any laws, regulations and/or codes in Malaysia.</li>
							<li>Do not bully, harass or target other members of the blufroge community with hate speech or threat. A community that allows that type of behavior will quickly deteriorate; therefore we insist that while you\'re on blufroge, you respect each member that you encounter and their contents.</li>
							</ul>',
	
	
	'Community_5'		=> '<p class="zfont_TB font15 pdleft20">I am</p>',
	'Community_5.1'		=> '<ul class="square"><li>Impersonating another person is so 2000 and late nor allowed on blufroge. If found to be impersonating someone else, we will disable your account permanently.</li></ul>',
	
	'Community_6'		=> '<p class="zfont_TB font15 pdleft20">Nags</strong>',
	'Community_6.1'		=> '<ul class="square"><li>Never post any content on blufroge that is obscene, pornographic, threatening, harassing, libelous, hate-oriented, racist, provocative, illegal, or contains nudity. If you had second thoughts about posting your content or message, that\'s a sign that you might be breaking the rules. Don\'t do it. </li></ul>',
	
	'Community_7'		=> '<p class="zfont_TB font15 pdleft20">Report</strong>',	
	'Community_7.1'		=> '<ul class="square"><li>If you have been continuously targeted or received repeated messages or comments that violate these guidelines or find content in violation of blufroge, please report the violation to us here, Our support staff will investigate the matter and take the necessary actions to resolve the issue.</li></ul>',	
	'Community_7.2'		=> '<p class="pdbottom10">If we find that you engage in conduct that breaks our Terms of Service, or the rules above, your account may be disabled permanently. blufroge reserves the right to refuse broadcast or remove any Submitted Advertisement on the Network, which blufroge, at its sole discretion considers to be obscene, offensive, fraudulent, immoral or in contravention of any laws, regulations and/or codes in Malaysia.</p>',	
	
	
	'Community_8'		=> '<p class="zfont_TB font15">Language</p>',
	
	'Community_8.1'     => '<p>The mobile phone has expressively changed the way we communicate and we can hardly imagine a world without texting. The SMS language is popular among users due to the necessary brevity of mobile text messaging, but it has also gained widespread criticism as the SMS language does not always obey or follow standard grammar. In view of this, blufroge would like to bring back the good old days and encourage the use of proper spelling and grammar throughout its services from SMS messages on the notice board to comments on our website.</p>
                            <p>Thank you for reading.</p>',

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//
	// <!--URL m=main&c=show_faq -->
	//								
	/* TnC */		
										
	'FAQ_1'				=> 'You Should Know',
	
	'FAQ_1.1'			=> 'The Story',			
	'FAQ_1.1A'			=> 'When the traditional notice board heard about the internet, he wanted a piece of the action. Without hesitating, he hooked himself up to his neighbour\'s slow wife and got lost (on purpose) in the vast universe of the rubba dub dub dub. When he finally came to his senses, he decided to learn and gather valuable knowledge & rare treasures from afar. Somehow, this story is related to the birth of blufroge and it is..... . . . . In a nutshell, the traditional notice board fell into a washing machine called the internet and out popped blufroge.',
	
	'FAQ_1.2'			=> 'Where are these boards located?',					
	'FAQ_1.2A'    	    => 'blufroge is currently located at the top colleges & universities around Klang Valley, not to mention the best FnBs in town! Go <a style="color:#0071BC" href="'.$CONF['url_app'].'?m=network&c=show_main_network">Here & There</a> to view our current locations.', 
								    
	'FAQ_1.3'			=> 'The difference between a premium and free listing?',
	'FAQ_1.3A'			=>'	Description                      Premium	Free
						Appears on Notice Board            Yes           No
						No. of Pictures Online		    5	         1
						Size of Notice Online 		  Large         Small
						Days Listed Online		    90           30
						Appears in Live Feed		   Yes	         No
						Video Link			   Yes		 No
						24 Random Notices		   Yes		 No
						Cheap				   Yes           Yes.',
        
		
		
	'FAQ_1.3.1'			=> 'The character limits for the title and body',
							
	'FAQ_1.3.1A'	=>'	The title is limited to 150 characters and the body is limited to 500 characters.',
 	'FAQ_1.4'			=> 'That the notices are filtered or moderated before it is displayed',
        
        'FAQ_1.4A'                      =>'     Uncle Ben once said, with great power comes great responsibility. With the ability to reach thousands with just one SMS, you are responsible for every word that you say. Read our Community Guidelines to better understand the type of notices you should not post. That being said, we moderate and filter out any offensive content.',
        'FAQ_1.5'			=> 'The duration each premium notice is displayed on the notice board',
	
	'FAQ_1.5A'			=>'	Your notice\'s display duration depends on the no. of days purchased (RM0.50 per day) and the operating hour of each location.',
	'FAQ_1.6'			=> 'Why didn\'t my notice appear instantly on the notice board?',

	'FAQ_1.6A'			=>'Every notice is moderated to filter out offensive or unsuitable content before it is displayed to the public. Our team will endeavor to do this in the shortest possible time . Right now it takes just under an hour during office hours , and a little longer after. We are always looking at ways to get your messages up as quickly as possible. 	',
                                             
    
	'FAQ_1.6.1A.Title'        => '<b>Scenario 1:</b>',
	
	'FAQ_1.6.1A'			=>' Megan posted a notice at 10.08 am, 25 Dec 2010. Her notice will appear after 12.01 pm on the same day.',
	 
	 'FAQ_1.6.2A.Title'        => '<b>Scenario 2:</b>',    
   'FAQ_1.6.2A'	   =>  ' Brad posted a notice at 3.06 pm, 25 Dec 2010. Her notice will appear after 12.01 pm on the 26 Dec 2010.<br><br>',
	
	
                         
	
	
	
	
	
	
	
	
	'FAQ_1.7'			=> 'Do you have a burning question, comment, suggestion or would like to report a user?',
	'FAQ_1.7A'			=>'	Click  <a href="'.$CONF['url_app'].'?m=network&c=show_contact" style="color:#0071BC">here</a> to Contact Us.',
	







	'FAQ_2'				=> 'Registration',
	'FAQ_2.1'			=> 'Where do I register?',
	
	'FAQ_2.1A'		        =>'	You can  <a href="'.$CONF['url_app'].'?m=main&c=show_register" style="color:#0071BC">Sign Up</a> here.',
	'FAQ_2.2'			=> 'Why do I have to validate my email?',
	
	'FAQ_2.2A'			=> 'Validating your email is a security measure to ensure that:
 					    i) You are a genuine user
					   ii) You are able to retrieve your password in case you forgot
					   and iii) Receive notification on comments made on your notice.',
	
	'FAQ_2.3'			=> 'Why do I have to secure my mobile no. to my account?',
						
        'FAQ_2.3A' 			=> 'Securing your mobile no. to your account automatically links your details to your post. This means notices posted via SMS will automatically include your username &  <a href="'.$CONF['url_app'].'?m=main&c=show_faq#2" style="color:#0071BC">profile picture.</a>',
	'FAQ_2.4'			=> 'Will you disclose my details to a 3rd party or sell it to the highest bidder?',
						
  	'FAQ_2.4A'			=> 'No, you have control over how your information is disclosed on blufroge. We will not disclose or sell your personal information to anyone.',
	


	'FAQ_3'				=> 'Post a Notice',

      'FAQ_3E1'=>                  ' Posting Online', 
	'FAQ_3E2'=>'			    Click <a href="'.$CONF['url_app'].'?m=ads&c=show_premium_ads" style="color:#0071BC">Post a Notice</a> and complete the form as follows:<br>
					    Note: Only the title & image are displayed on the notice board, the additional details appear online',
	
	'FAQ_3.1'			=> '1 - Choose a category',
  	'FAQ_3.1A'     			=> 'if you do not have a picture, each category has a default icon.',
	
	'FAQ_3.2'			=> '2 - Choose an image',
   	'FAQ_3.2A'     			=> 'An image can be the most powerful factor in determining whether you get noticed or not. A good image should be eye-catching and relevant to your text. Select the default picture, profile picture or a new picture to appear alongside your text on the notice board. Optimal image: Dimensios 226 pixel wide x 128 pixel tall, Aspect ratio 16:9 (preferred) or 4:3. Max size per file 640 kb.',
	
	'FAQ_3.3'			=> '3 - Construct a title',
   	'FAQ_3.3A'			=>'A good headline will capture the audience immediately. The longest headline may not be the best one, we suggest a short and sweet text. More importantly, your title & image should complement each other (Character limit 150).',
	
	'FAQ_3.4'			=> '4 - Choose a payment type',
	'FAQ_3.4A'			=> 'Notices paid via SMS or Web Credit appears on both the notice board & www.blufroge.com
                                          i) SMS - Pay via SMS 
                                         ii) Web Credit - Pay via Web Credit 
				        iii) Free Posting - Free Listings (Online Only)',
	'FAQ_3.5'			=> '5 - Choose a video',

	'FAQ_3.5A'			=> 'A video that complements your notice will give a "kick" and enhances the overall message.',    
	'FAQ_3.6'			=> '6 - Choose the location(s):',
	
	'FAQ_3.6A'			=>'Depending on your targeted audience, choose the location(s) you want your notice to appear. Go here & there to view a list of our current locations.',
     'FAQ_3.7'			=> '7 - Choose the no. of day(s)"',
	
	'FAQ_3.7A'			=>'Choose the number of day(s) you want your notice to appear on the notice board
   					   Note: Paid notices are by default listed for 90 days online
					   Free postings are by default listed for 30 days online..',
        'FAQ_3.8'			=> '8 - Choose a start date',
	
	'FAQ_3.8A'			=> 'Choose the date you want your notice to start appearing.',
        'FAQ_3.9'			=> '9 - Add more details:',				
	
	'FAQ_3.9A'			=> 'To complete your posting, add links, fanpage, myspace or contact details.',
        'FAQ_3.10'			=> '10 - Voila! Click Post!',
	
        'FAQ_3.11'			=> 'Posting via SMS',
	'FAQ_3.11A'			=> 'Check out <a href="'.$CONF['url_app'].'?m=main&c=show_how_to_sms" style="color:#0071BC">How to SMS</a>.',
        
	'FAQ_3.12'			=> 'Can I post an MMS to blufroge?',

	'FAQ_3.12A'			=> 'No. You can upload photos for your notices via the website.',
        'FAQ_3.13'			=> 'Why can\'t I upload my picture?',

	'FAQ_3.13A'			=> 'The maximum size allowed per image is 640kb, kindly check or resize your image before uploading again.
					    To resize images, use Microsoft Office Picture Manager
					    On the top action bar, Click --> Edit Pictures
					    On the right edit menu, Click --> Resize
					    Choose any --> Predefined width x height
					    Click --> OK
					    Save the file, this should do the trick.',		 			 				
		      
        'FAQ_3.14'			=> 'Payment',

        'FAQ_3.15'			=> 'Can I submit payment with a different mobile number?',

	'FAQ_3.15A'			=> 'No, the payment must be made via the mobile no. registered and validated to the specific username. If you changed your mobile no. you have to update and re-validate your mobile no. in My Account -> Profile before posting again.',
  'FAQ_3.16'			=> 'What happens if I posted my notice before 12.00 pm but only paid for the notice after 12.01 pm?',

	'FAQ_3.16A'			=> 'Your notice will not appear after 12.01 pm and the payment code would have expired. To ensure notice posted before 12.00   pm appear on the same day, please make payment before 12.00 pm', 	
        'FAQ_3.17'			=> 'If I posted a notice at 10.35 am and only paid for the notice at 12.30 pm, will the notice appear today?',

	'FAQ_3.17A'			=> 'Your notice will not appear after 12.01 pm today(day posted) as intended because the payment was not received before 11.59pm. You will not be charge for the notice as the payment code would have expired.', 	



	'FAQ_4'			=> 'Website',
	'FAQ_4.1'		=> 'Why can\'t I make a comment?',
	'FAQ_4.1A'  	        => 'Only Registered users can make comments on blufroge. Don\'t fret, registration is very quick and easy. Click on <a href="'.$CONF['url_app'].'?m=main&c=show_register" style="color:#0071BC"> Sign Up </a> and soon you\'ll have more to say than the auntie selling fish at the morning market',
	
	'FAQ_4.2'		=> 'I have registered, why can\'t I make a comment?',
	'FAQ_4.2A' 		=>' Your registration is incomplete until you have validated both your email and mobile no. Click <a href="'.$CONF['url_app'].'?m=ads&c=show_premium_ads" style="color:#0071BC">Post a Notice </a> and follow the instructions.',	
			
	'FAQ_4.3'		=> 'Why do some notices have location icons while others don\'t?',
	'FAQ_4.3A'		=> 'Location icons indicate the places where these notices will be displayed. Therefore, premium notices have icons while free notices don\'t.',	
	
	'FAQ_4.4'		=> 'Where can I view more details of each notice?',
	'FAQ_4.4A'		=> 'Simply click on the Code, Picture or Comment to view more details. You can also share any content you may find interesting on Facebook or Twitter via the icons.',	


	
	'FAQ_5'			=> 'My Account',
	'FAQ_5.1'		=> 'What\'s in My Account?',
	'FAQ_5.1A'		=> 'View notices posted under Listings or update your personal details under Profile',							

	'FAQ_5.2'		=> 'Why can\'t I change my username?',
	'FAQ_5.2A'		=> 'Our security feature uses your unique username and tags it to your mobile number. As your mobile number and email may change from time to time, your username has to stay the same',	
	
	'FAQ_5.3'		=> 'What does Active, Pending Payment, Edit, Plus+ etc mean?',
	'FAQ_5.3A'		=> 'Status		What it does
				    Active		Notice approved and is ready to be displayed/is being displayed
				    Pending Payment	Payment for notice has not been received
				    Pending Review	Payment received, waiting for approval
				    Expired		Notice has passed display period

				    Action		What it does
				    Edit		Edit unpaid notices
				    Plus+		Edit & Add photos to paid notices
				    New Post		Quick Link to post a new notice',



	'FAQ_6'			=> 'Security',
	'FAQ_6.1'		=> 'what if I forgot my password?',
	'FAQ_6.1A'		=> 'Simply key in your username or email address <a href="'.$CONF['url_app'].'?m=main&c=show_forgot_pass " style="color:#0071BC">here</a> and follow the instructions to retrieve your password',							
					
	'FAQ_6.2'		=> 'How do I change my password?',
	'FAQ_6.2A'		=> 'Login to your account, go to My Account --> Profile --> Change Password
				    Make sure your password contains both letters and numbers.
				    Do not under any circumstances reveal your password to someone else.',							

	'FAQ_6.3'		=> 'Why can\'t I change my username?',
	'FAQ_6.3A'		=> 'See here.',							

	

	'FAQ_7'			=> 'Cool Stuff',
	'FAQ_7.1'		=> 'Profile Picture',
	'FAQ_7.1A'		=> 'Upload your avatar, baby pictures or favourite celebrity as your profile picture and voila!
				    Each time you text to blufroge, your profile picture appears alongside your messages.
				    Your friends will instantly recognize your messages when they look at the board. 
				    Of course, you can change the picture anytime or choose a different picture when posting online.
				    Don\'t be shy macho guy, we all know you have a poster of Bieber on your room wall',							

	'FAQ_7.2'		=> 'Multiple Location',
	'FAQ_7.2A'		=> 'blufroge offers the convenience of posting to multiple screens at multiple locations at the push of a button. This saves you (time & money) from printing copies on paper and running around putting up notices manually.',

	'FAQ_7.3'		=> '24 random notices',
	'FAQ_7.3A'		=> '24 notices are chosen randomly each day from the pool of premium notices (last 30 days) and displayed at all our locations. This gives the notices more exposure and a higher chance of getting noticed for free.',

	'FAQ_7.4'		=> 'Paperless',
	'FAQ_7.4A'		=> 'blufroge is a paperless notice board; save the trees!',							



	//
	// <!--URL m=main&c=do_seach_results_inline -->
	//
	'search_result'			=> 'Search results for:',	
	'search_result2'		=> 'We did not find results for:',	
	'search_result3'		=> '. Try again.',	
	
	//
	// <!--URL m=main&c=show_forgot_pass -->
	//
	'show_forgot_pass'		=> 'blufroge will send password reset instructions to the email address associated with your account.<br><br>Please type your email address below.',
  	//
  	//URL m=main&c=show_forgot_pass's error message
  	//
  	'err_valid_email'  		=> 'Please enter a valid email address.',
  	'err_email_not_registered'=> 'The email address is not registered.',
  	'err_sms_account'      	=> 'Error: sms_account' , 
	
	//
	// <!--URL m=main&c=do_change_password -->
	//
	'show_do_change_password'=> 'A message was sent to your email address with instructions on creating a new password.', 		
	
	//
	// <!--URL m=main&c=show_tell_friend -->
	//
	'show_tell_friend'		=> 'Get the word out on this to your friend!',
	'form_your_name'		=> 'Your Name:',
	'form_your_email'		=> 'Your Email:',
	'form_friend_name'		=> 'Your Friend\'s Name:',
	'form_friend_email'		=> 'Your Friend\'s Email: ',
	'form_guess_what'		=> 'Guess what? Here\'s something I just discovered that may interest you!',
	'form_for_more'			=> 'Look for more on www.blufroge.com!',	
  	//
  	//URL m=main&c=show_tell_friend's error message
  	//
  	'err_fields_empty'  	=> 'Do not leave any fields empty. Please fill in your friend\'s particulars.',
  	'err_valid_email2'		=> 'You have not entered a valid email address.',
  	'err_valid_email3'      => 'Your friend\'s email address is invalid.' , 	
								
	//
	// <!--URL m=main&c=show_thanks_tell_friend -->
	//
	'show_thanks_tell_friend'=> 'Thank you. Your message has been sent.',
	
	//
	// <!--URL m=main&c=show_register -->
	//
	'why_join'				=> 'Why Join '.SITENAME.'?',
	'why_join_list'			=> '<ul>
								<li>Allows you to post ads<br/>on our growing network of<br/>Digital Community Boards. Find us in Damansara, Bangsar, TTDI and many more urban centres!
								</li>
								<li>No credit cards required - just use your mobile phone to pay for ads posted via SMS</li>
								<li>Post your ads online for FREE</li>
								<li>Get alerts on Events and Promotions by our community partners</li>
								</ul> ',
	'register_insruction'	=> 'Just<strong> fill in your personal particulars</strong> here and submit to receive a <strong>verification email</strong>. To confirm your registration, login to our site with the given password to begin your '.SITENAME.' experience!',
	'field_compulsory'		=> '* Compulsory field.',
	'form_first_name'		=> 'First Name :',
	'form_last_name'		=> 'Last Name :',
	'form_username2'		=> 'Username :',
	'form_profile_image'	=> 'Profile Picture:<br />(optional)',
	'form_mobile_no'		=> 'Mobile Number :',
	'form_email2'			=> 'Email : ',
	'form_email_verify'		=> 'Re-type Email:',
	'form_password'			=> 'Password:',
	'form_Rpassword'		=> 'Retype Password:',
	'form_oldpassword'		=> 'Old Password:',
	'form_newpassword'		=> 'New Password:',
	'form_Rnewpassword'		=> 'Retype<br/>New Password',
	'form_email_eg'			=> '(jsmith@example.com)',
	'form_mobile_code'		=> '+6',
  	//
  	//URL m=main&c=show_register's error message
  	//
  	'err_fields_empty2'  	=> 'Do not leave any fields empty. Please fill in your particulars.',
  	'err_number_invalid'	=> 'Your mobile number is invalid.',
  	'err_valid_email4'		=> 'You have not entered a valid email address.',
  	'err_valid_email5'		=> 'Your email address doesn\'t match.',
	'err_valid_reg_pwd'		=> 'Your password doesn\'t match.',
	'err_valid_pwd'			=> 'Your new password doesn\'t match.',
  	'form_email_registered'	=> 'This email address has been registered.',
	'form_old_password_error'	=> 'Invalid old password.',
  	'form_username_not_available'=> 'This username is not available.',
								
	//
	// <!--URL m=main&c=show_thanks_tell_friend -->
	//
	'show_thanks_register'	=> 'Welcome to blufroge! Please login to your registered email account to retrieve the password. With the password, you may login to start posting ads.
								<br /><br /><a href="'.$CONF['url_app'].'?m=main" class="highlight" target="_parent">Login here</a>.
								<br /><br /><br />
								Warm regards,
								<br />
								The '.SITENAME.' Team
								<br /><br />
								<span class="red_text">*Please check your spam box as the email may have inadvertently been sent there.</span>',
	
	
	
	//
	//<!-- URL m=main&c=show_complete_account -->
	//
	'show_instruction' => 'Fill in the fields below and ensure the details are accurate to complete your registration. Please check your email account as we will be sending you a confirmation email to activate your account.',
	'show_note_register'        => 'Note: <br>Please change your password after you have successfully updated your account.',
	'show_thanks_register_sendacc' => 'You\'re almost there! Check your email to complete your registration.',
	'show_note'        => 'Your account is not activated yet. <br><br>To resend the email, click ',
	'show_note2'       =>'To change your email, click ',
	'show_email_instruction' => 'Please enter a valid email address. We\'ll send you an email to complete your registration.',
	
	
	//
	//<!-- URl m=main&c=show_webmsg
	//
	
	//'web_thanks' => 'You received 100cr in your account. Thank you.',
	'web_thanks' => 'Web Credits has been added into your account. Thank you.',
	'redeem_thanks' => 'Woo Hoo!
<br><br>You have just added 100 web credits to your account, go nuts!',
	'web_err1' => 'Oops, you have already redeemed your Web Credits. You may only redeem once.',
	'web_err2' => 'Sorry, the credit offer has expired.',
	'web_err3' => 'Invalid Account',
	'web_err4' => 'You have enterend an invalid or expired redemption code.<br> ',
	'web_err41' => 'Kindly re-check your redemption code as your account will be barred after 3 wrong entries.',
	
	'err_code_qty' => 'Quantity must be number.',
);
?>