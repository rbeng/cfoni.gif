<?php
global $LANG,$Q,$CONF;

	$lang = array(
 	'title_welcome' 		=> 'Welcome',
	'logout'                => 'Sign Out',
	'dashboard'             => 'Dashboard',
	'customer'              => 'Customer Lists',
 	
	
	
	
	
	
	'err_login'     		=> 'Incorrect login or password',
 	
 	'err_fields_empty'  	=> 'Do not leave any fields empty. Please fill in your friend\'s particulars.',
 	'err_valid_email'		=> 'You have not entered a valid email address.',
 	'err_number_invalid'	=> 'Your mobile number is invalid.',
 	'err_number_username' 	=> 'Username must be at least 6 characters.',
 	'err_username_used'   	=> 'User login name is used by someone,please choose another user login name.',
 	'err_premium'         	=> 'This premium code already used by someone,please use another 1.',
 	'err_premium_code'    	=> 'Please enter premium code 12 charactres.',
 	'err_email_registered'	=> 'This email is used by someone,please choose another email.',
 	'err_premium_exists'  	=> 'This is invalid coupon number, please enter valid coupon number.',
	
	
 	'err_startdate'  		=> 'Start date cannot be empty.',
 	'err_startdate_less'  	=> 'End date cannot less than start date.',
	
	
	  //
 		// Error Message
 		//
 		'err_category'              => 'Category name cannot be empty',
 		'err_category_unavailiable' => 'Category name has been used before ',
 		'err_cannotinsert_category' => 'Cannot Insert Category Table.[pasivi_category]',
 		'err_cannotinsert_zone'     => 'Cannot Insert Zone Table.[pasivi_zone]',
 		
 		'err_subcategory'           => 'Subcategory cannot be empty',
 		'err_subcategory_unavailiable' => 'SubCategory name has been used before ',
 		
 		'err_collection'            => 'Collection name cannot be empty',
 		'err_collection_unavailiable' => 'Collection name has been used before',
 		
	
	
	
);
?>