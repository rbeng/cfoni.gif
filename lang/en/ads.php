<?php
global $LANG,$Q,$CONF;
	$lang = array(
	//
	//URL m=ads&c=show_main_ads 
	//
  'premium_ads_notice'    				 =>  ' 123 -Welliam Simply post an ad here or via SMS, and see your ad appearing in your neigbourhood\'s stores! Placed around town, our Digital Community Boards will loop your ads at selected locations.
																	    <ul>
																	    <li>Premium ads are displayed on crisp <br/>High-Definition LCD TVs and you can even upload a picture of your product!
																	    <li>Maximise your ad\'s reach to a wider target audience</li>
																	    <li>Paying for an ad is made easy and convenient with our SMS payment gateway system
																	    <li>Each premium ad receives an additional free 30 days online exposure</li>
																	    </ul>',
  'free_ads_notice'       				 => 'Enjoy our 30 days comprehensive online classifieds. Place your ads here at absolutely no cost. Upgrade later to premium to enjoy greater benefits.',
  
  //
  //URL m=ads&c=do_update_phone's error message
  //
  'err_empty_mobile'          		  => 'Your Mobile Number field should not be empty.',
  'err_invalid_format_mobile' 		  => 'Your Mobile Number is invalid format.', 

  //
  //URL m=ads&c=do_free_ads's error message
  //
  'err_empty_description'  				  => 'Please write a description of your ad.',
  'err_exceed_description' 				  => 'Description must not exceed 150 characters.',
  'err_uncheck_agree'      				  => 'Please read and agree to these <a href='.$CONF['url_app'].'?m=main&c=show_terms target=_blank>Terms & Conditions</a> in order to use blufroge.' , 
  'err_bad_words'          				  => 'Bad words found.', 
  'err_photo_format'       				  => 'Supports .jpg format only',
  'err_photo_size'         				  => 'The file you attempted to upload is too large {{filesize}} kb',   
  
  //
  //URL m=ads&c=do_premium_ads
  //
  'err_premium_empty_description'   => 'Please write a title of your ad.',
  'err_premium_uncheck_agree'       => 'Please tick the checkbox if you have agreed to the <a href='.$CONF['url_app'].'?m=main&c=show_terms>Terms & Conditions</a>.',
  'err_premium_invalid_date'        => 'The date you have selected has passed, kindly select a new start date.',
	'err_premium_exceed_description'  => 'Description must not exceed 150 characters.',
	'err_premium_full_booked'         => 'The selected date is not available',
	'err_premium_photo_format'        => 'Supports .jpg format only',
	'err_premium_photo_size'          => 'The file you attempted to upload is too large {{filesize}} kb', 
	'err_expiry'                      => 'Sorry, your web credit validity has expired. Please select "SMS Credit" to continue.',
	'err_credit'                      => 'Sorry,you have insufficient web credit',
	
	'err_premium_networkid'           => 'Please tick at least 1 Location.',
	
	//
	//URL m=ads&c=do_repost_ads
	//
	'err_repost_empty_description'  	=> 'Please write a description of your ad.',
  'err_repost_exceed_description' 	=> 'Description must not exceed 150 characters.',
	
	
  	'err_backDate' 					=> 'You are not allowed to select dates prior to today.',
	
	
		
	//
	// DB Error Filling up by developer
	//  
  'err_db_insert'        				 	  => 'Database Error SMS_ADS_SCHEDULE - Free Ads : doInsertFree',
  'err_db_update_ad'     				 	  => 'Database Error SMS_ADS_SCHEDULE - Free Ads : doUpdateAdsCode',
  'err_db_update'        				 	  => 'Database Error SMS_ADS_SCHEDULE - Free Ads Image : doUpdateImage', 
  'err_db_insert_taxnomy'				 	  => 'Database Error SMS_ADS_SCHEDULE - Free Ads Taxnomy : doAddTaxnomy',
  'err_db_insert_phone'  				 	  => 'Database Error SMS_ADS_SCHEDULE - Free Ads Taxnomy : doUpdateStorePhoneCodeAds',
  'err_premium_insert_db'				 	  => 'Database Error SMS_ADS_SCHEDULE - Premium Ads : doInsertSMSads',
  'err_premium_update_db'				 	  => 'Database Error SMS_ADS_SCHEDULE - Premium Ads : doUpdateAdsCode',
  'err_premium_update_db_image'  	  => 'Database Error SMS_ADS_SCHEDULE - Premium Ads : doUpdateImage',
  'err_premium_insert_db_booked'    => 'Database Error SMS_ADS_SCHEDULE - Premium Ads : doInsertSmsBooked',
  'err_premium_insert_db_count'     => 'Database Error SMS_ADS_SCHEDULE - Premium Ads : doInsertNewCount',
  'err_premium_insert_db_countday'  => 'Database Error SMS_ADS_SCHEDULE - Premium Ads : doManageNewCount',
  'err_premium_update_db_countday'  => 'Database Error SMS_ADS_SCHEDULE - Premium Ads : doManageUpdateCount',
  'err_premium_insert_db_taxnomy'   => 'Database Error SMS_ADS_SCHEDULE - Premium Ads Taxnomy : doAddTaxnomy',
  
  
  
       
);
?>