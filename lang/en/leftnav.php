<?php
global $LANG,$Q,$CONF;
	$lang = array(
	// -- Login --
	'username'				=> 'User Name:',
	'password'				=> 'Password:',
	'remember'				=> 'Remember me',
	'forgot_pwd'			=> '<a href="'.$CONF['url_app'].'?m=main&c=show_forgot_pass" class="a_login">Forgot Password </a>',  
	'sign_up'				=> '<a href="'.$CONF['url_app'].'?m=main&c=show_register&KeepThis=true&TB_iframe=true&height=600&width=650" class="thickbox" title="">&nbsp;Sign up? </a>',  
	
	// -- Account --
	'current_login'			=> 'You are currently login as',
	'logout'				=> '<a href="'.$CONF['url_app'].'?m=main&c=do_logout" class="a_link">Logout</a>', 
	'my_listing'			=> '<a href="'.$CONF['url_app'].'" class="a_login2">Home</a>', 
	'update_acc'			=> '<a href="'.$CONF['url_app'].'?m=account&c=show_edit_account" class="a_login2">Update My Account</a>', 
	'change_pwd'			=> '<a href="'.$CONF['url_app'].'?m=account&c=show_edit_password" class="a_login2">Change password</a>', 
	'not_validate'			=> 'Mobile Not Validated',
	'click_validate'		=> '<a href="'.$CONF['url_app'].'?m=ads&c=show_premium_ads" class="a_login">Click Here to Validate</a>',
	'validate'				=> 'Mobile Is Validated', 
	
	// -- Search --
	'search_eg'			=> 'Search',
	
	// -- Get Free Access --
	'free_access_register'	=> 'Register for '.SITENAME.'',
	'free_access_list'		=> '<ul><li>Display your ads via our network of Digital Community Boards</li>
									<li>Use our SMS payment gateway - no credit cards required</li>
									<li>Yes! Free Online Classifieds</li>
									<li>Events and Promotions</li>
								</ul> ',
	'free_access_sign_up'	=> 'It only takes a minute to sign up!',	
	
	// -- Just SMS --
	'just_sms_top'		=> 'Get your message out through our Digital Community Boards! See "<a href="'.$CONF['url_app'].'?m=network&c=show_main_network">Networks</a>" for locations.',
	'just_sms_bottom'		=> 'Currently available only to <img src="'.$CONF['vir_lib'].'img/common/maxis.png" /> users. Coming soon to other service providers.',


 // -- Credit --
 'show_topup' => 'Web credits expired.',

);
?>