<?php
global $LANG,$Q,$CONF;

	$lang = array(	
	
	'symbol'				=> '*',
	//
	// <!--URL m=main&c=show_main -->
	//
	'no_data'				=> 'No data.',
	'contact_no'			=> 'Contact:',
	'get_code'				=> 'Ad Code:',
	'get_date'				=> 'Date:',
	
	//
	// <!--URL m=main&c=show_error_page -->
	//
	'error_pages'			=> 'To use our services, you first need to register with '.SITENAME.'. So fret not, it\'s just an easy one-time registration process and you can start posting ads right away!
								<br /><br />
								If you do not wish to register, you can still place ads via SMS by texting your message to '.short_code.' (currently applies to Maxis users only) and have details displayed to our network of Digital Community Boards (terms & conditions apply), plus 30 days online automatically free.
								<br /><br />
								If you have already posted an ad via SMS search for your ad by entering your Ad Code in our search box on our landing page.',
	
	//
	// <!--URL m=main&c=show_about -->
	//
	'show_about_sms2board'	=> 'Your local community board now takes a digital leap with '.SITENAME.'.
								<br /><br />
								Have an item for sale, a place to let, or a service to offer? Perhaps you\'d simply like to say "I love you"? '.SITENAME.' lets you do this in a new, novel way via SMS and the Web. In a nutshell, it\'s classified advertising made simple.
								<br /><br />
								All it takes is a quick text message to "'.short_code.'" and your classified ad will make its way to dozens of digital community bulletin boards strategically placed at the premises of our community partners throughout the Klang Valley. Your messages and pictures (which can be placed via the Web) won\'t just be viewed by an online audience, but by the general public.
								<br /><br />
								Just text your message from your phone or access '.SITENAME.' online for more features. It\'s as quick, easy and simple as that!
								<br /><br />
								'.SITENAME.'... Your world for sale, at your fingertips.
								<br /><br />',
	
	//
	// <!--URL m=main&c=show_contact -->
	//
	'show_contact'			=> 'Do you have burning questions that need answers? Or feedback to help us serve you better? Feel free to get in touch with us and we\'ll get back to you shortly. Your comments are appreciated.',
	'fields_required'		=> 'All fields are required.',
	'alternative_contact'	=> 'Alternatively, contact us at 1300-88-0027.',
	'form_name'				=> 'Name :',
	'form_contact'			=> 'Contact :',
	'form_email'			=> 'Email :',
	'form_comment'			=> 'Your comments / enquiries :',
	'form_display_code'		=> 'Please enter the displayed security code:',
	'form_security_code'	=> 'Security Code :',
	
	//
	// <!--URL m=main&c=c=show_success -->
	//
	'show_success_contact_us'=> 'Thank you for contacting us and we will get back to you shortly. Your feedback is important to help us serve you better.
								<br /><br />
								Warm regards,<br />
								The '.SITENAME.' Team',
	
	//
	// <!--URL m=main&c=show_how_to_sms -->
	//
	'title_function'		=> 'FUNCTIONS',
	'description_function'	=> 'To interact with '.SITENAME.', send any of the following SMS codes to <strong>'.short_code.'</strong>.',

	'table_code_code'		=> 'Code',
	'table_code_do'			=> 'What does it do?',
	'table_code_example'	=> 'Example',
	/* EXAMPLE */												
	'EXAMPLE_1'				=> 'CS &lt;NETWORK&gt; &lt;DAY CODE&gt; &lt; MESSAGE&gt;',								
	'EXAMPLE_1.1'			=> 'Post an ad to a network for a specified number of days. An SMS placed will find its way to our Digital Community Boards.',							
	'EXAMPLE_1.1.1'			=> 'CS BA D6 TV for sale 20 inch, brand new, call Kevin 016 888 6666',					
	'EXAMPLE_2'				=> 'CS  WEB &lt;PAYMENT CODE&gt;',					
	'EXAMPLE_2.1'			=> 'Make a payment for premium ads posted via the Web using a randomly generated payment code.',						
	'EXAMPLE_2.1.1'			=> 'CS  WEB 64s98',							
	'EXAMPLE_3'				=> 'CS  GET &lt;AD CODE&gt;',				
	'EXAMPLE_3.1'			=> 'Download an ad to your mobile phone using the Ad Code.',						
	'EXAMPLE_3.1.1'			=> 'CS GET 74s798',	

								
	'title_network'			=> 'NETWORK',
	'description_network'	=> 'Click <a href="'.$CONF['url_app'].'?m=network&amp;c=show_main_network" target="_self" class="a_link_blue">here</a> to view our list of available networks and their respective codes e.g. Network \'Bangsar\' has the code of \'BA\'.',
								
	'title_day_code'		=> 'DAY CODE',
	'description_day_code'	=> 'Choose to advertise from one (1) to six (20) days.',
	'note_day_code'			=> '*Note: You can omit the Day Code for a one (1) day Ad by entering shortcut values i.e. CS &lt;NETWORK&gt; &lt;MESSAGE&gt;',
		
	'table_day_no'			=> 'Number of Days',
	'table_day_code'		=> 'Day Code',
	/* DAYCODE */												
	'DAYCODE_1'				=> '1',								
	'DAYCODE_1.1'			=> 'D1',					
	'DAYCODE_2'				=> '2',					
	'DAYCODE_2.1'			=> 'D2',						
	'DAYCODE_3'				=> '3',				
	'DAYCODE_3.1'			=> 'D3',						
	'DAYCODE_4'				=> '...',						
	'DAYCODE_4.1'			=> '...',						
	'DAYCODE_5'				=> '20',			
	'DAYCODE_5.1'			=> 'D20',
								
	'title_rates'			=> 'RATES',
	'description_rates'		=> 'The following are the SMS charges which will be reflected on your monthly phone bills.',
	'note_rates'			=> 'Note: Standard telco charges apply.',
	
	'table_rates_function'	=> 'SMS Functions',
	'table_rates_price'		=> 'Price',
	/* DAYCODE */												
	'RATES_1'				=> 'Post an ad via SMS',								
	'RATES_1.1'				=> 'RM0.50 for each day',					
	'RATES_2'				=> 'Pay for an ad posted via Web',					
	'RATES_2.1'				=> 'RM0.50 for each day',						
	'RATES_3'				=> 'Download an ad to your mobile',				
	'RATES_3.1'				=> 'RM0.50 per download',						
	'RATES_4'				=> 'Validate your mobile',						
	'RATES_4.1'				=> 'FREE',
	
	'description_help'		=> '<b> Need more help ? Click <a href="'.$CONF['url_app'].'?m=main&c=show_faq">FAQ</a></b>',
	
	//
	// <!--URL m=main&c=show_terms -->
	//
								
	/* TnC */		
	'Title'					=> 'Terms of Service  ',					
	'TNC_1'					=> 'Acceptance of Terms and Conditions',
	'TNC_1.1'				=> ''.$CONF['company_name'].', a company incorporated in Malaysia (Company Registration Number: 793604-D) ("'.$CONF['company_name2'].'") 
								manages the internet portal '.$CONF['website'].' ("<strong>Website</strong>") and the Services offered on the Website, subject to the 
								following Terms and Conditions ("<strong>Terms</strong>"). The "<strong>Services</strong>" means the provision of the advertisement 
								broadcasting services on the Network by MBoard, as further described under Clause 3 below.',
	'TNC_1.2'				=> 'The Terms apply to your access and use of the Website and/or the Services. By accessing any page of the Website and/or using the 
								Services, you agree to be bound by the Terms immediately without limitation or qualification. Without limiting the foregoing, exercise 
								of either of the options under Clauses 3.2(i), 3.2(ii) and/or 3.8 below shall constitute "use" of the Services. If you do not accept 
								the Terms, please discontinue your access to the Website and/or use of the Services.',
	'TNC_1.3'				=> 'If you are registering and/or using the Website and/or the Services on behalf of a business entity, you represent and warrant that you 
								have full legal authority to bind the entity to the Terms. If you do not have the legal authority to bind such entity to the Terms, 
								please discontinue your access to the Website and/or use of the Services.',
	'TNC_2'					=> 'Updates to Terms and Conditions',
	'TNC_2.1'				=> ''.$CONF['company_name2'].' reserves the right to update the Terms at any time by giving you notice by posting the updated Terms on the 
								Website. You expressly acknowledge and agree that your continued use of the Website and/or the Services after such update constitutes 
								an acceptance of and agreement to be bound by the updated Terms. Once posted on the Website, the updated Terms will apply to all users 
								from the date of posting. You shall be responsible for reviewing the current version of the Terms each time you visit the Website 
								and/or use the Services.',
	'TNC_3'					=> 'The Services',
	'TNC_3.1'				=> ''.$CONF['company_name2'].' operates an extensive advertising network consisting of:-
								<br />
								<ol style="list-style-type:lower-roman">
								<li>computer programmable electronic display boards at various locations ("<strong>Location</strong>") ("<strong>Media Boards</strong>"); and</li>
								<li>the Website.</li>
								</ol>
								The Media Boards and the Website shall together form the "<strong>Network</strong>".<br><br><em>Paid Advertisements</em>',
	'TNC_3.2'				=> 'You can post on advertisement on the Network by exercising any of the following options:
								<br />
								<ol style="list-style-type:lower-roman">
								<li><u>Via SMS</u>. Send a SMS to '.short_code.' with the SMS code CS&lt;space&gt;NETWORK&lt;space&gt;D(No. of days)&lt;space&gt;
								MESSAGE; AND OR</li>
								<li><u>Via the Website</u>. In order to post an advertisement via the Website, you are required to register by filling up the 
								<a href="'.$CONF['url_app'].'?m=main&c=show_register&KeepThis=true&TB_iframe=true&height=500&width=650" 
								class="thickbox a_link_blue" style="font-size:12px"><u>Registration Form</u></a>. Registration is free of charge. As soon as your 
								registration is verified, you may submit your advertisement by following the procedure as set out 
								<a href="'.$CONF['url_app'].'?m=main&c=show_how_to_sms" style="font-size:12px"><u>here</u></a>.</li>
								</ol>',
	'TNC_3.2_pop'			=> 'You can post on advertisement on the Network by exercising any of the following options:
								<br />
								<ol style="list-style-type:lower-roman">
								<li><u>Via SMS</u>. Send a SMS to '.short_code.' with the SMS code 
								CS&lt;space&gt;NETWORK&lt;space&gt;D(No. of days)&lt;space&gt;MESSAGE; AND OR</li>
								<li><u>Via the Website</u>. In order to post an advertisement via the Website, you are required to register by filling up the 
								<u>Registration Form</u>. Registration is free of charge. As soon as your registration is verified, you may submit your advertisement 
								by following the procedure as set out <u>here</u>.
								</li>
								</ol>',
	'TNC_3.3'				=> 'Once you have complied with the procedure as set out in Clause 3.2(i) and/or 3.2(ii) above and remitted payment for your 
								advertisement , you will receive a confirmation code ("<strong>Confirmation Code</strong>") via SMS. You shall retain the Confirmation 
								Code as proof of submission of your advertisement.',
	'TNC_3.4'				=> 'Placement of advertisements on the Network in either manner as described in Clause 3.2(i) and/or 3.2(ii) ("<strong>Paid Advertisements
								</strong>") shall be subject to the charges as set forth <u>here</u>.',
	'TNC_3.5'				=> 'Each Paid Advertisement shall be displayed on the Website for a minimum duration of ninety (90) Days and displayed on the Media Boards 
								for the number of days as per your request pursuant to the procedure under Clause 3.2 above ("<strong>Display Duration</strong>"). In 
								relation to display cycle on the Media Board, the Paid Advertisements shall be displayed on the Media Boards for thirty (30) seconds 
								every hour for the duration of the opening hours of the Locations in which the Media Boards  are located ("<strong>Display Cycle
								</strong>") . MBoard reserves the right to vary or amend the Display Duration and Display Cycle at its sole discretion without 
								providing you prior notice.',
	'TNC_3.6'				=> 'For the avoidance of doubt, Paid Advertisements submitted before 5pm will appear on the Media Boards the next day. Paid Advertisements 
								submitted after 5pm, will appear on the Media Boards the day after next.<br /><br /><em>Free Advertisements</em>',
	'TNC_3.7'				=> 'You may also post advertisements on the Website free of charge by following the registration procedure as set forth in Clause 3.2(ii) 
								above. As soon as your registration is verified, you may submit your advertisement by following the procedure as set out 
								<a href="'.$CONF['url_app'].'?m=ads&c=show_main_ads" style="font-size:12px"><u>here</u></a> ("<strong>Free Advertisements</strong>").',
	'TNC_3.7_pop'			=> 'You may also post advertisements on the Website free of charge by following the registration procedure as set forth in Clause 3.2(ii) 
								above. As soon as your registration is verified, you may submit your advertisement by following the procedure as set out <u>here</u> ("
								<strong>Free Advertisements</strong>").',
	'TNC_3.8'				=> 'Free Advertisements shall only be displayed on the Website and not on the Media Boards.',
	'TNC_3.9'				=> 'Each Free Advertisement shall be displayed on the Website for a minimum duration of ninety (90) days. '.$CONF['company_name2'].' 
								reserves the right to vary or amend the duration of the display of the Free Advertisement on the Website at its sole discretion without 
								providing you prior notice.',
	'TNC_4'					=> 'Use of the Services',
	'TNC_4.1'				=> '<u><em>Membership and Registration</em></u>',
	'TNC_4.1.1'				=> 'In order to exercise the option under Clause 3.2(ii), you are required to register by filling up the 
								<a href="'.$CONF['url_app'].'?m=main&c=show_register&KeepThis=true&TB_iframe=true&height=500&width=650" 
								class="thickbox a_link_blue" style="font-size:12px"><u>Registration Form</u></a>. You hereby agree and warrant that:<br />
								<ol style="list-style-type:lower-roman">
								<li>that all information provided by you in the Registration Form ("<strong>Information</strong>") is true, accurate, current 
								and complete; and</li>
								<li>that you will maintain and promptly update the Information if any of the Information becomes untrue, inaccurate, current and 
								incomplete.</li>
								</ol>',
	'TNC_4.1.1_pop'			=> 'In order to exercise the option under Clause 3.2(ii), you are required to register by filling up the <u>Registration Form</u>. 
								You hereby agree and warrant that:<br />
								<ol style="list-style-type:lower-roman">
								<li>that all information provided by you in the Registration Form ("<strong>Information</strong>") is true, accurate, current 
								and complete; and</li>
								<li>that you will maintain and promptly update the Information if any of the Information becomes untrue, inaccurate, current and 
								incomplete.</li>
								</ol>',
	'TNC_4.1.2'				=> ''.$CONF['company_name2'].' has the right to suspend or terminate your registration and refuse any and all current or future use of the 
								Website and/or the Services (or any part thereof) if '.$CONF['company_name2'].' has reasonable grounds to believe that any Information 
								provided by you is untrue, inaccurate, not current or incomplete.',
	'TNC_4.2'				=> '<u><em>User ID and Password</em></u>',
	'TNC_4.2.1'				=> 'You will be required to select a User ID and a password upon completion of the Registration Form. You are responsible for all actions 
								and activities taken under that User ID and password and it is your responsibility to keep your User ID safe and to maintain the 
								confidentiality of the User ID and password. You have no right to disclose or transfer your account or User ID to any other person.',
	'TNC_4.2.2'				=> 'You shall ensure that you logout at the end of each session and you shall immediately notify '.$CONF['company_name2'].' of any 
								unauthorized use of your User ID and/or password or any other breach of security. '.$CONF['company_name2'].' will not be held liable 
								for any loss or damage arising out of your failure to comply with the provisions under this Clause 4 or for any unauthorized use of 
								your User ID and/or password.',
	'TNC_4.3'				=> '<u><em>Submitted Advertisements</em></u>',
	'TNC_4.3.1'				=> 'Paid Advertisements and Free Advertisements shall be collectively referred to as "<strong>Submitted Advertisements</strong>". Except 
								where otherwise indicated, where there is reference to "Submitted Advertisements" in any of the clauses herein, such clauses shall 
								apply to both Paid Advertisements and Free Advertisements.',
	'TNC_4.3.2'				=> 'You agree to use the Services for lawful purposes and in a way that does not infringe the rights of, restrict or inhibit anyone else\'s 
								use and enjoyment of the Services. Prohibited behaviour includes transmitting advertisements or content which are obscene, offensive, 
								fraudulent, immoral or in contravention of any laws, regulations and/or codes in Malaysia.',
	'TNC_4.3.3'				=> 'All Submitted Advertisements will go through a screening and moderation process in order to assess the Submitted Advertisement(s) 
								suitability for display on the Network. '.$CONF['company_name2'].' reserves the right to refuse to broadcast or remove any Submitted 
								Advertisement on the Network, which '.$CONF['company_name2'].', at its sole discretion considers to be obscene, offensive, fraudulent, 
								immoral or in contravention of any laws, regulations and/or codes in Malaysia.',
	'TNC_4.3.4'				=> 'In the event any Paid Advertisement fails to be broadcasted on the Network, '.$CONF['company_name2'].' shall refund in full the 
								fees paid for the said  Paid Advertisement. In order to claim the refund, the party claiming the refund shall be required to provide 
								details of the Confirmation Code at '.$CONF['company_name2'].'\'s office at Room 16.01, 16th Floor, Campbell Complex, 98 Jalan Dang 
								Wangi, 50100 Kuala Lumpur.',
	'TNC_4.3.5'				=> 'Notwithstanding the refund provision in Clause 4.3.4, '.$CONF['company_name2'].' shall not provide a refund for any Submitted 
								Advertisement (if applicable) which is not broadcasted on the Network in the following circumstances:-
								<br />
								<ol style="list-style-type:lower-roman">
								<li>where such Submitted Advertisement considered by '.$CONF['company_name2'].' to be obscene, offensive, fraudulent, immoral or in 
								contravention of any laws, regulations and/or codes in Malaysia;</li>
								<li>where such Submitted Advertisement is not in compliance with the Terms;</li>
								<li>where such failure to broadcast the Submitted Advertisement is not due to the sole fault of '.$CONF['company_name2'].';</li>
								<li>where such failure to broadcast the Submitted Advertisement is due to an event beyond the control of '.$CONF['company_name2'].', 
								including power shortages, network interruptions, network failure or interruption of the internet connection etc.</li>
								</ol>',
	'TNC_4.4'				=> '<u><em>Suspension of the Services</em></u>',
	'TNC_4.4.1'				=> ''.$CONF['company_name2'].' reserves the right at any time to modify or discontinue, temporarily or permanently, the Services or any 
								part thereof with or without notice. You agree that '.$CONF['company_name2'].' shall not be liable to you or to any third party for any 
								modification, suspension or discontinuance of the Services.',
	'TNC_4.4.2'				=> 'The Services are not available to, and may not be used by individuals who cannot form legally binding contracts under applicable 
								law. Without limiting the foregoing, the Services are not available to persons, to temporarily or indefinitely suspended users of 
								the Website and/or the Services or persons barred from receiving the Services under the laws of Malaysia or other 
								applicable jurisdiction.',
	'TNC_5'					=> 'Reliance on the Submitted Advertisements displayed on the Network',
	'TNC_5.1'				=> 'Your use and reliance on the Submitted Advertisements displayed on the Network ("<strong>Displayed Advertisements</strong>") or the 
								information contained therein is entirely at your own risk. The Displayed Advertisements are provided on the Network for your 
								convenience, but they are beyond the control of '.$CONF['company_name2'].' and '.$CONF['company_name2'].' is not responsible or liable 
								for the content or the accuracy of the information contained in the Displayed Advertisement. No endorsement of the Displayed 
								Advertisements or the content therein is implied. '.$CONF['company_name2'].' has not verified the content of the Displayed 
								Advertisements. It is recommended that you verify the content of the Displayed Advertisements before use of or reliance on the same.',
	'TNC_5.2'				=> ''.$CONF['company_name2'].' is not responsible for the accuracy or the continued availability of the Displayed Advertisements. The 
								Displayed Advertisements are for knowledge and convenience only and '.$CONF['company_name2'].' is not responsible or liable for any 
								damages, losses, difficulties or consequences suffered as a result of your use of and reliance on the Displayed Advertisements.',
	'TNC_6'					=> 'Use of the Website and/or Servies',
	'TNC_6.1'				=> 'You agree to use the Website and/or Services for lawful purposes and in a way that does not infringe the rights of, restrict or inhibit 
								anyone else\'s use and enjoyment of the Website and/or the Services. Prohibited behaviour includes harassing or causing distress or 
								inconvenience to any person, transmitting obscene or offensive content or disrupting the Website and/or Services.',
	'TNC_7'					=> 'Intellectual Property',
	'TNC_7.1'				=> 'All copyright, trade marks, design rights, patents and other intellectual property rights (registered and unregistered) in and on the 
								Website and all content (including all applications) located on the Website shall remain vested in '.$CONF['company_name2'].' or its 
								licensors. You may not copy, reproduce, republish, disassemble, decompile, reverse engineer, download, post, broadcast, transmit make 
								available to the public or otherwise use the content of the Website and/or the Services in any way except for personal and 
								non-commercial use. ',
	'TNC_7.2'				=> 'The names, images and logos identifying MBoard and/or third parties and their products and services are subject to copyright, design 
								rights and trade marks of '.$CONF['company_name2'].' and/or such third parties. Nothing contained in this Terms or on the Website shall 
								be construed as granting any licence or right to use any trade mark, design right or copyright of '.$CONF['company_name2'].' and/or any 
								third party.',
	'TNC_8'					=> 'External Links',
	'TNC_8.1'				=> 'External links may be provided on the Website for your convenience, but they are beyond the control of '.$CONF['company_name2'].' 
								and '.$CONF['company_name2'].' is not responsible for the content of any other websites or pages linked to or from the Website. No 
								endorsement of third party websites or content is implied. '.$CONF['company_name2'].' has not verified the content of any such websites 
								or pages. Use or reliance on any external links and any content contained on such external links is at your own risk. It is recommended 
								that you view the linked website\'s terms or privacy policy pages to understand how use of that website may affect you.',
	'TNC_8.2'				=> ''.$CONF['company_name2'].' is not responsible for the accuracy, timeliness or the continued availability or the existence of content, 
								hyperlinks or third party websites or pages linked to the Website. Links to downloadable software sites are for convenience only and '.
								$CONF['company_name2'].' is not responsible or liable for any difficulties or consequences associated with downloading the software. 
								Use of any downloaded software is governed by the terms of the licence agreement, if any, which accompanies or is provided with 
								the software.',
	'TNC_8.3'				=> 'No hypertext links shall be created from any website to this website without prior written permission of '.$CONF['company_name2'].'. 
								Please contact us if you wish to link to the Website or would like to request a link to your website.',
	'TNC_9'					=> 'No Warranties',
	'TNC_9.1'				=> 'To the greatest extent permitted by applicable law and your local laws, '.$CONF['company_name2'].' does not warrant or represent that 
								the information contained on the Network is accurate, complete, correctly sequenced, reliable or timely.',
	'TNC_9.2'				=> 'Further '.$CONF['company_name2'].' does not warrant or represent that the display of the Submitted Advertisements on the Network will 
								be uninterrupted, continuous, correctly sequenced, reliable or timely. In the event of defect, failure or interruption occurring in 
								four (4) or more Locations, '.$CONF['company_name2'].' shall use all reasonable efforts to ensure that such defect, failure or 
								interruption in each Location is rectified within a reasonable timeframe upon notification of such defect, failure or interruption 
								to '.$CONF['company_name2'].'.',
	'TNC_9.3'				=> 'To the greatest extent permitted by applicable law and your local laws, MBoard does not warrant or represent that the information 
								contained on the Website is accurate, complete, correctly sequenced, reliable or timely or that the Website will be uninterrupted 
								or free of errors and/or viruses. Use of the Website is at your sole risk. You have sole responsibility for adequate protection and 
								back up of data and/or equipment and for undertaking reasonable and appropriate precautions to scan for computer viruses or 
								other destructive properties.',
	'TNC_9.4'				=> 'The information and materials contained in the Website and on the Network, including the Displayed Advertisements, text, graphics, 
								links or other items are provided "as is" and "as available". To the greatest extent permitted by applicable law and your local 
								laws, '.$CONF['company_name2'].' makes no warranties, representations, statements or guarantees, whether express or implied regarding 
								the Website, Displayed Advertisements, the Services and/or the Network, the information contained on the Website and/or the Network, 
								the security of you or your company\'s personal information, material, information and/or submissions transmitted through the Website.',
	'TNC_9.5'				=> 'To the greatest extent permitted by applicable law and your local laws, '.$CONF['company_name2'].' makes no warranty or representation 
								that the information provided on the Website and/or the Services and/or the Displayed Advertisements is appropriate for use in any 
								jurisdiction. By accessing the Website and/or using the Services and/or the Displayed Advertisements, you warrant and represent that 
								you are legally entitled to do so and to make use of the information contained on the Website and/or the Services.',
	'TNC_10'				=> 'Disclaimer of Liability',
	'TNC_10.1'				=> 'To the greatest extent permitted by applicable law and your local laws, '.$CONF['company_name2'].' shall not be responsible for 
								and disclaims all liability for any loss, liability, damage (whether direct, indirect, special, incidental or consequential), 
								personal injury or expense of any nature whatsoever which may be suffered by you or any third party (including your company), as 
								a result of or which may be attributable, directly or indirectly, to your access and use of the Website, the Services, the Network 
								and/or the Displayed Advertisements, any information contained on the Website, the Services, the Network and/or the Displayed 
								Advertisements, you or your company\'s personal information, material, information, submissions or Submitted Advertisement(s) 
								transmitted through the Website and/or through SMS, even if you advise '.$CONF['company_name2'].' of the possibility of such damages, 
								losses or expenses.',
	'TNC_10.2'				=> 'To the greatest extent permitted by applicable law and your local laws, '.$CONF['company_name2'].' shall not be responsible for and 
								disclaims all liability for any loss, liability, damage (whether direct, indirect, special, incidental or consequential), personal 
								injury or expense of any nature whatsoever, whether caused by the negligence of '.$CONF['company_name2'].', its employees or otherwise 
								arising in connection with the Website, the Services, the Network and/or the Displayed Advertisements, even if you advise 
								'.$CONF['company_name2'].' of the possibility of such damages, losses or expenses.',
	'TNC_11'				=> 'Termination',
	'TNC_11.1'				=> 'You agree that '.$CONF['company_name2'].' may, in its sole discretion, without cause and without prior notice, immediately terminate 
								and/or suspend your account, and refuse any and all current or future use of the Website and/or the Services (or any part thereof) 
								without any liability whatsoever. Cause for such termination and/or suspension shall include, without limitation:
								<br />
								<ol style="list-style-type:lower-alpha">
								<li>breaches or violations or purported breaches or violations of the Terms or other incorporated agreements or guidelines,</li>
								<li>your use of the Website and/or Services is deemed by '.$CONF['company_name2'].' to be disruptive, inappropriate and infringing the 
								rights of, restrict or inhibiting anyone else\'s use and enjoyment of the Website and/or the Services. Such disruptive and 
								inappropriate use of the Website and/or Services include without limitation, spamming and submission of advertisements considered by 
								'.$CONF['company_name2'].' to be obscene, offensive, fraudulent, immoral or in contravention of any laws, regulations and/or codes 
								in Malaysia,</li>
								<li>requests by law enforcement or other government agencies,</li>
								<li>a request by you (self-initiated account deletions),</li>
								<li>discontinuance or material modification to the Website, the Services and/or the Network (or any part thereof),</li>
								<li>unexpected technical or security issues or problems,</li>
								<li>extended periods of inactivity, and/or</li>
								<li>engagement by you in fraudulent or illegal activities.</li>
								</ol>',
	'TNC_11.2'				=> 'Termination of your account includes:
								<br />
								<ol style="list-style-type:lower-alpha">
								<li>removal of access to the Website and/or the Services;</li>
								<li>deletion of your password and all related information, files and content associated with or inside your account (or any part 
								thereof), and</li>
								<li>barring of further use of the Website and/or the Services.</li>
								</ol>',
	'TNC_12'				=> 'General',
	'TNC_12.1'				=> '<strong>Entire Agreement.</strong> These Terms constitute the agreement between you and '.$CONF['company_name2'].' in relation to your 
								access and use of the Website and/or the Services. Unless otherwise specifically stated these Terms shall take precedence over all 
								prior commitments, undertakings or representations, whether written or oral between you and '.$CONF['company_name2'].' with respect 
								to your use of the Website and/or the Services.',
	'TNC_12.2'				=> '<strong>Applicable Law.</strong> The Terms shall be governed by and construed in accordance with the laws of Malaysia without giving 
								effect to any principles of conflict of law. You hereby consent to submit to the exclusive jurisdiction of the Courts of Malaysia in 
								respect of any disputes arising in connection with the Terms, the Website and/or the Services or any matter related to or in 
								connection therewith.',
	'TNC_12.3'				=> '<strong>Electronic Form.</strong> A printed version of the Terms and of any notice given in electronic form shall be admissible in 
								judicial or administrative proceedings based upon or relating to these terms and conditions to the same extent and subject to the same 
								conditions as other business documents and records originally generated and maintained in printed form.',
	'TNC_12.4'				=> '<strong>Severability of Terms.</strong> If any of these terms are determined to be illegal, invalid or otherwise unenforceable by 
								reason of the laws of any state or country in which these terms are intended to be effective, the parties nevertheless agree that the 
								court should endeavour to give effect to the parties\' intentions as reflected in the provisions and to construe such provision to 
								the maximum extent permitted by law so as to render that provision valid and enforceable, and the other provisions of the Terms to 
								remain in fill force and effect. ',
	'TNC_12.5'				=> '<strong>Waiver.</strong> The failure or delay of '.$CONF['company_name2'].' to exercise or enforce any right in the Terms does not 
								waive '.$CONF['company_name2'].'\'s right to enforce that right. ',
	'TNC_12.6'				=> 'Additional Terms. You agree that your use of the Website and/or the Services is also subject to the Privacy Policy which can be viewed 
								at <a href="'.$CONF['url_app'].'?m=main&amp;c=show_privacy" style="font-size:12px">Privacy Policy</a>',
	'TNC_12.6_pop'			=> 'Additional Terms. You agree that your use of the Website and/or the Services is also subject to the Privacy Policy which can be viewed 
								at Privacy Policy',
	
	/* END TnC*/					
	
	//
	// <!--URL m=main&c=show_privacy -->
	//					
	/* PRIVACY */												
	'PRIVACY_1'				=> 'This Privacy Policy explains MBoard\'s policy relating to MBoard\'s use of any information collected or received by MBoard from your use of the Website and/or the Services. By using the Website and/or the Services, registering your user account information for the Services or using services provided on the Website, you agree that MBoard may use such user account information or any information MBoard receives from you in any way.
								<br /><br />
								MBoard reserves the right to update this Privacy Policy at any time by giving you notice by posting the updated Privacy Policy on the Website. You expressly acknowledge and agree that your continued use of the Website and/or the Services and/or use of your user account after such update constitutes an acceptance of and agreement to be bound by the updated Privacy Policy. Once posted on the Website, the updated Privacy Policy will apply to all users from the date of posting. You shall be responsible for reviewing the current version of the Privacy Policy each time you visit the Website and/or use the Services.',
	'PRIVACY_2'				=> '<strong>User Account Information</strong>',
	'PRIVACY_2.1'			=> 'When you register with the Website for the Services, you agree to provide MBoard with information to you including but not limited to name, address, email address, mobile number, birth date, gender, occupation and industry ("Information").
								<br /><br />
								You agree and consent to MBoard sharing and transferring your Information to third parties including to third parties outside Malaysia. Such third parties include but are not limited to content providers, advertisers, technology partners, applications providers and other suppliers of products or services that MBoard may contract with.
								<br /><br />
								You may edit your Information by logging into your account on the Website using your username and password.
								<br /><br />
								Upon termination of your user account with MBoard, your Information will not be held in storage for longer than necessary by MBoard.',
	'PRIVACY_3'				=> '<strong>Storage</strong>',
	'PRIVACY_3.1'			=> 'To safeguard your personal data, all electronic storage and transmission of user account information are stored in a secure infrastructure and adheres to a standard security guideline.',
	'PRIVACY_4'				=> '<strong>Cookies</strong>',
	'PRIVACY_4.1'			=> 'You hereby agree and acknowledge that MBoard or any third parties providing services on the Website may use cookies which may record data relating to you.
								<br /><br />
								These cookies may include \'per session\' cookies which are cookies that remain in the cookie file of your computer until you close your internet browser.
								<br /><br />
								These cookies may also include \'persistent\' cookies which permanently remain on the cookie file of your computer.
								<br /><br />
								Such cookies may be used to record details of pages relating to services you have visited on the Website. You hereby agree and grant permission to MBoard to use the information collected by the use of such cookies in any way MBoard wishes.',
	'PRIVACY_5'				=> '<strong>Feedback</strong>',
	'PRIVACY_5.1'			=> 'If you have any feedback, suggestions or inquiries, please email MBoard at feedback@sms2board.com or telephone MBoard\'s hotline at 1300-88-0027.',
	
	
	
	//
	// <!--URL m=main&c=show_faq -->
	//								
	/* TnC */												
	'FAQ_1'					=> 'Registration',
	'FAQ_1.1'				=> 'How do I view my account?',			
	'FAQ_1.1A'      => 'To access your account, you must first login with the registered username.',
	'FAQ_1.2'				=> 'Where do I register ?',
							
	'FAQ_1.2A'      =>'	You can register <a href="'.$CONF['url_app'].'?m=main&c=show_register&KeepThis=true&TB_iframe=true&height=500&width=650" 
								    class="thickbox a_link_blue">here</a>.',
	'FAQ_1.3'				=> 'Will there be any verification email sent out during registration?',
							
	'FAQ_1.3A'			=>'	Yes, you will receive an email containing your username and a randomly generated password to login to your account.',
	'FAQ_2'					=> 'My Account',
	'FAQ_2.1'				=> 'How do I view my account?',
	
	'FAQ_2.1A'      => 'To access your account, you must first login with the registered username.',
	
	
	
	'FAQ_2.2'				=> 'What can I do with my account?',
	
	'FAQ_2.2A'				=> 'You can view your ads history, edit your ads and update personal details.',
	
	'FAQ_2.3'				=> 'What if I forgot my password?',
						
  'FAQ_2.3A' => '  Just click <a href="'.$CONF['url_app'].'?m=main&c=show_forgot_pass" class="a_link_blue" style="font-size:12px">here</a> and enter the registered email address. A temporary password will be sent to that address. For security reasons, we advise you to immediately change your password.',
	'FAQ_2.4'				=> 'How do I change my password?',
						
  'FAQ_2.4A' => 'You can do so by login in to your account, navigate to &quot;Change Password&quot; link and update accordingly.',
	'FAQ_2.5'				=> 'What does my ad status mean?',
								
	'FAQ_2.5A'				=>	'	Ad status determines your ad\'s publishing state. <b>Active</b> indicates that your ad has been approved and is ready to be published the next day, or it is already running on our Digital Community Boards. <strong>Inactive</strong> shows your ad is still pending payment. <strong>Waiting for Approval</strong> means your ad has been paid, but pending approval to publish.',
	'FAQ_3'					=> 'Locations',
	'FAQ_3.1'				=> 'What is a Digital Community Board?',
  'FAQ_3.1A'      => 'This term is used to describe our remodelled traditional &quot;Notice Boards&quot; where crisp High-Definition LCD TVs are utilised to display your ads.',
	
	
	'FAQ_3.2'				=> 'What are networks?<br />',
   'FAQ_3.2A'     =>'           Networks define the locations of the Digital Community Boards. <a href="'.$CONF['url_app'].'?m=network&c=show_main_network" class="a_link_blue">See</a> if we are in your neighbourhood.',
	'FAQ_3.3'				=> 'What are these networks for?',
   'FAQ_3.3A' =>'To display your premium ads, choose the network that best fits your target buyers.',
	'FAQ_3.4'				=> 'What are the available networks?',
	'FAQ_3.4A'	=>'		Click <a href="'.$CONF['url_app'].'?m=network&c=show_main_network">here</a> to see list of commercial premises with Digital Community Boards.',
	
	
	
	
	'FAQ_4'					=> 'Placing an ad',
	'FAQ_4.1'				=> 'Are ads filtered before it\'s displayed?',
	'FAQ_4.1A'	    =>'	Yes, all ads will be filtered to remove any postings that we deemed offensive or just isn\'t fit for airing!',
	
	'FAQ_4.2'				=> '<strong>Web</strong>',
	'FAQ_4.2.1'				=> 'Can I place ads without registration?',
  'FAQ_4.2.1A' =>' For free or premium ads posted via '.SITENAME.' website, you will require a login.',	
	
	
	
	'FAQ_4.2.2'				=> 'Can I post premium ads via the '.SITENAME.' website?',
	'FAQ_4.2.2A'			=> 'Yes, you can!',	
	
	
	
	'FAQ_4.2.3'		=> 'Will I be charged to post ads?',
	'FAQ_4.2.3A'  => 'The premium ads are displayed on our growing networks of Digital Community Boards and '.SITENAME.' website, and you will be charged 
								to post. However, you can still post free ads which are displayed only on the '.SITENAME.' website.',	
	
	'FAQ_4.2.4'		 => 'How long is my ad displayed on the '.SITENAME.' website?',
	'FAQ_4.2.4A'	 => 'You ad will be displayed on the '.SITENAME.' website for 30 days.',
	
		
	'FAQ_4.2.5'		=> 'How long is my premium ad displayed on the Digital Community Boards?',
	'FAQ_4.2.5A'	=> 'The length of the run depends on the days purchased. Packages range from one (1) to six(6) days.',
	
	
	
	
	
	
	'FAQ_4.2.6'		=> 'Tell me how to post a premium ad.',
	'FAQ_4.2.6A'	=>	'You can either post with your mobile phone via SMS or by logging in to your '.SITENAME.' account.',
	
	
	
	
	'FAQ_4.2.7'		=> 'How do I pay a premium ad via the '.SITENAME.' website?',
	'FAQ_4.2.7A'	=> '	Upon posting a premium ad via web, you will be given an payment code. Just SMS the code to '.short_code.' and you will be charged 
								accordingly. No credit cards required!',
	
	'FAQ_4.2.8'				=> 'Can I submit payment with a different mobile number?',
	'FAQ_4.2.8A'     => 'No, the payment must be made with the registered and validated mobile number. If you have updated your mobile number in your account 
								details, you must revalidate the new mobile number before resuming posting ads.',
	
	'FAQ_4.2.9'				=> 'I accidentally closed my browser before I could pay. How do I retrieve my ad code?',
	'FAQ_4.2.9A'		=>' You can search for your ad under "My Accounts" with the payment status of "Pending". SMS "CS WEB &lt;PAYMENT CODE&gt;" and you should 
								receive a reply indicating successful payment.',
	'FAQ_4.2.10'			=> 'Can I repost my previous ads?',
	'FAQ_4.2.10A'			=>'	Yes, you can! We offer an easy function to repost your expired ads. 
								Just go to "<a href="'.$CONF['url_app'].'?m=account&c=show_my_listings" style="font-size:12px">My Account</a>", select the ad to repost 
								and click the Repost button. You are only allowed to change your network and date to repost.',
	
	
	'FAQ_4.2.11'			=> 'What if an ad is posted for the following day but is paid for after 5pm?',
	'FAQ_4.2.11A'			=> 'You will receive a reply SMS stating the payment period has expired. You may opt to repost your ad by selecting another date.',
	
	
	
	
	'FAQ_4.3'				=> '<strong>SMS</strong>',
	'FAQ_4.3.1'				=> 'How do I place an ad via SMS?',
	'FAQ_4.3.1A'				=> 'Just use the SMS feature in your mobile phone to type in CS &lt;NETWORK&gt; &lt;DAY CODE&gt; &lt;MESSAGE&gt; before sending 
								to '.short_code.' e.g. CS BA D2 TV for sale 20 inch, brand new, call Kevin 016 888 6666',	
	'FAQ_4.3.2'				=> 'How much will I be charged?',		
	'FAQ_4.3.2A'				=> 'You will be charged the rates as stated in "<a href="'.$CONF['url_app'].'?m=main&c=show_how_to_sms">How to SMS</a>" page. Standard	telco rates apply.',	
	
	
	
	'FAQ_4.3.3'				=> 'Why do I receive a message stating no slots available for my ad?',
	'FAQ_4.3.3A'				=> 'Our networks of Digital Community Boards are able to only display a limited number of ads in a day. If the selected date is 
								unavailable, you will not be charged for your ad post.',	
	
	
	
	'FAQ_4.3.4'				=> 'Can I submit an MMS?',
	'FAQ_4.3.4A'			=> 'No, but you can upload photos on the website after logging in.',
	
	'FAQ_4.3.5'				=> 'Can I select the number of days to display my ads on the Digital Community Boards?',
	'FAQ_4.3.5A'		 => 'Yes, you can! Just follow the Day Codes as set out under 
								"<a href="'.$CONF['url_app'].'?m=main&c=show_how_to_sms">How to SMS</a>" page.',									
	
	
	
	
	
	
	'FAQ_5'					=> 'Contact Us',
	'FAQ_5.1'				=> 'Who do I contact should I have enquiries?',
	'FAQ_5.1A'			=> 'Click <a href="'.$CONF['url_app'].'?m=main&amp;c=show_contact">here</a> to contact us online.',							
	
	//
	// <!--URL m=main&c=do_seach_results_inline -->
	//
	'search_result'			=> 'Search results for:',	
	'search_result2'		=> 'We did not find results for:',	
	'search_result3'		=> '. Try again.',	
	
	//
	// <!--URL m=main&c=show_forgot_pass -->
	//
	'show_forgot_pass'		=> 'Please enter the email address associated with your member account and click the Send button. We will then send you an email that will contain your password.',
  	//
  	//URL m=main&c=show_forgot_pass's error message
  	//
  	'err_valid_email'  		=> 'Please enter a valid email address.',
  	'err_email_not_registered'=> 'The email address is not registered.',
  	'err_sms_account'      	=> 'Error: sms_account' , 
	
	//
	// <!--URL m=main&c=do_change_password -->
	//
	'show_do_change_password'=> 'Your password has been reset. Please check your email for your new password.
								<br /><br />
								Warm regards,<br />
								The '.SITENAME.' Team', 		
	
	//
	// <!--URL m=main&c=show_tell_friend -->
	//
	'show_tell_friend'		=> 'Get the word out on this to your friend!',
	'form_your_name'		=> 'Your Name:',
	'form_your_email'		=> 'Your Email:',
	'form_friend_name'		=> 'Your Friend\'s Name:',
	'form_friend_email'		=> 'Your Friend\'s Email: ',
	'form_guess_what'		=> 'Guess what? Here\'s something I just discovered that may interest you!',
	'form_for_more'			=> 'Look for more on www.sms2board.com!',	
  	//
  	//URL m=main&c=show_tell_friend's error message
  	//
  	'err_fields_empty'  	=> 'Do not leave any fields empty. Please fill in your friend\'s particulars.',
  	'err_valid_email2'		=> 'You have not entered a valid email address.',
  	'err_valid_email3'      => 'Your friend\'s email address is invalid.' , 	
								
	//
	// <!--URL m=main&c=show_thanks_tell_friend -->
	//
	'show_thanks_tell_friend'=> 'Thank you. Your message has been sent.',
	
	//
	// <!--URL m=main&c=show_register -->
	//
	'why_join'				=> 'Why Join '.SITENAME.'?',
	'why_join_list'			=> '<ul>
								<li>Allows you to post ads<br/>on our growing network of<br/>Digital Community Boards. Find us in Damansara, Bangsar, TTDI and many more urban centres!
								</li>
								<li>No credit cards required - just use your mobile phone to pay for ads posted via SMS</li>
								<li>Post your ads online for FREE</li>
								<li>Get alerts on Events and Promotions by our community partners</li>
								</ul> ',
	'register_insruction'	=> 'Just<strong> fill in your personal particulars</strong> here and submit to receive a <strong>verification email</strong>. To confirm your registration, login to our site with the given password to begin your '.SITENAME.' experience!',
	'field_compulsory'		=> '* Compulsory field.',
	'form_first_name'		=> 'First Name :',
	'form_last_name'		=> 'Last Name :',
	'form_username2'		=> 'Username :',
	'form_profile_image'	=> 'Profile Picture:<br />(optional)',
	'form_mobile_no'		=> 'Mobile Number :',
	'form_email2'			=> 'Email : ',
	'form_email_verify'		=> 'Re-type Email:',
	'form_password'			=> 'Password:',
	'form_Rpassword'		=> 'Retype Password:',
	'form_oldpassword'		=> 'Old Password:',
	'form_newpassword'		=> 'New Password:',
	'form_Rnewpassword'		=> 'Retype<br/>New Password',
	'form_email_eg'			=> '(jsmith@example.com)',
	'form_mobile_code'		=> '+6',
  	//
  	//URL m=main&c=show_register's error message
  	//
  	'err_fields_empty2'  	=> 'Do not leave any fields empty. Please fill in your particulars.',
  	'err_number_invalid'	=> 'Your mobile number is invalid.',
  	'err_valid_email4'		=> 'You have not entered a valid email address.',
  	'err_valid_email5'		=> 'Your email address doesn\'t match.',
	'err_valid_reg_pwd'		=> 'Your password doesn\'t match.',
	'err_valid_pwd'			=> 'Your new password doesn\'t match.',
  	'form_email_registered'	=> 'This email address has been registered.',
	'form_old_password_error'	=> 'Invalid old password.',
  	'form_username_not_available'=> 'This username is not available.',
								
	//
	// <!--URL m=main&c=show_thanks_tell_friend -->
	//
	'show_thanks_register'	=> 'Welcome to SMS2Board! Please login to your registered email account to retrieve the password. With the password, you may login to start posting ads.
								<br /><br /><a href="'.$CONF['url_app'].'?m=main" class="highlight" target="_parent">Login here</a>.
								<br /><br /><br />
								Warm regards,
								<br />
								The '.SITENAME.' Team
								<br /><br />
								<span class="red_text">*Please check your spam box as the email may have inadvertently been sent there.</span>',
	
	
	
	//
	//<!-- URL m=main&c=show_complete_account -->
	//
	'show_instruction' => 'Fill in the fields below and ensure the details are accurate to complete your registration. Please check your email account as we will be sending you a confirmation email to activate your account.',
	'show_note_register'        => 'Note: <br>Please change your password after you have successfully updated your account.',
	'show_thanks_register_sendacc' => 'You\'re almost there! Check your email to complete your registration.',
	'show_note'        => 'Your account is not activated yet. <br><br>To resend the email, click ',
	'show_note2'       =>'To change your email, click ',
	'show_email_instruction' => 'Please enter a valid email address. We\'ll send you an email to complete your registration.',
	
	
	//
	//<!-- URl m=main&c=show_webmsg
	//
	
	//'web_thanks' => 'You received 100cr in your account. Thank you.',
	'web_thanks' => 'Web Credits has been added into your account. Thank you.',
	'redeem_thanks' => '100 Web Credits has been added into your account. Thank you.',
	'web_err1' => 'Oops, you have already redeemed your Web Credits. You may only redeem once.',
	'web_err2' => 'Sorry, the credit offer has expired.',
	'web_err3' => 'Invalid Account',
	'web_err4' => 'You have entered an invalid or expired redemption code. Please try again.',
);
?>