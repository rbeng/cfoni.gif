<?php
//
// Root path
//
//echo $_COOKIE['account:strRemember'];
//exit;
define('ROOT_PATH', dirname(__FILE__).'/');
define('PICTURE_PATH', dirname(__FILE__).'/');
define("TEMPLATE_PATH", dirname(__FILE__) . "/design/tpl/");
date_default_timezone_set('Asia/Kuala_Lumpur');


//-----------------------------------------------
// NO USER EDITABLE SECTIONS BELOW
//-----------------------------------------------
global $CONF, $Q, $OUT, $DB, $DBSH, $CODE, $LANG, $LANGLEFT,$MailClass,$MailClassSecond,$MailClassThird,$MailClassfour,$MailClassfive,$PDF;

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_ALL);
//set_magic_quotes_runtime(0);
set_error_handler('error_handler');
ini_set("display_errors", 1);
//require_once('c:/wamp/www/facebook/facebook.php');
require_once(ROOT_PATH . 'src/config.php');
require_once(ROOT_PATH . 'src/code.php');
require_once(ROOT_PATH . 'src/lib/CGI.php');
require_once(ROOT_PATH . 'src/lib/Lang.php');
require_once(ROOT_PATH . 'src/lib/CGIOut.php');
require_once ROOT_PATH . 'src/lib/Thumbnail.php';
require_once(ROOT_PATH . 'src/function/functions.php');
define('COPYRIGHT','Copyright reserved by cFoni');

define('DIR_CLASSES',ROOT_PATH.'src/classes');
define('USE_CACHE', true);
//require_once DIR_CLASSES . '/Cache.php';
require_once DIR_CLASSES . '/pagination.class.php';
require_once DIR_CLASSES . '/paginator.class.php';
require_once DIR_CLASSES . '/record.class.php';
require_once DIR_CLASSES . '/db.class.php';
require_once DIR_CLASSES . '/PHPExcel.php';	
require_once DIR_CLASSES .'/class_banner.php';
require_once DIR_CLASSES . '/class.phpmailer.php';
require_once DIR_CLASSES . '/rss_php.php';


$Q    = new CGI();
$OUT  = new CGIOut();
$LANG = new Lang();
$LANGLEFT = new Lang();


$MailClass = new PHPMailer();
$MailClass->SMTPAuth	 = true;	
//$MailClass->Mailer 	   = "tester";
$MailClass->Host		   = _EMAIL_SMTP_HOST;
$MailClass->Helo		   = _EMAIL_SMTP_HELO;
$MailClass->Username   = _EMAIL_SMTP_USER;
$MailClass->Password   = _EMAIL_SMTP_PASS; 
$MailClass->FromName	 = _EMAIL_FROM_NAME;
$MailClass->From		   = _EMAIL_FROM_EMAIL;
$MailClass->Sender 	   = _EMAIL_FROM_EMAIL;


	
		
$MailClassSecond = new PHPMailer();
$MailClassSecond->SMTPAuth	 = true;	

$MailClassSecond->Host		   = _EMAIL_SMTP_HOST;
$MailClassSecond->Helo		   = _EMAIL_SMTP_HELO;
$MailClassSecond->Username   = _EMAIL_SMTP_USER;
$MailClassSecond->Password   = _EMAIL_SMTP_PASS; 
$MailClassSecond->FromName	 = _EMAIL_FROM_NAME;
$MailClassSecond->From		   = _EMAIL_FROM_EMAIL;
$MailClassSecond->Sender 	   = _EMAIL_FROM_EMAIL;


$MailClassThird = new PHPMailer();
$MailClassThird->SMTPAuth	 = true;	
//$MailClass->Mailer 	   = "tester";
$MailClassThird->Host		   = _EMAIL_SMTP_HOST;
$MailClassThird->Helo		   = _EMAIL_SMTP_HELO;
$MailClassThird->Username   = _EMAIL_SMTP_USER;
$MailClassThird->Password   = _EMAIL_SMTP_PASS; 
$MailClassThird->FromName	 = _EMAIL_FROM_NAME;
$MailClassThird->From		   = _EMAIL_FROM_EMAIL;
$MailClassThird->Sender 	   = _EMAIL_FROM_EMAIL;




$MailClassfour = new PHPMailer();
$MailClassfour->SMTPAuth	 = true;	
//$MailClass->Mailer 	   = "tester";
$MailClassfour->Host		   = _EMAIL_SMTP_HOST;
$MailClassfour->Helo		   = _EMAIL_SMTP_HELO;
$MailClassfour->Username   = _EMAIL_SMTP_USER;
$MailClassfour->Password   = _EMAIL_SMTP_PASS; 
$MailClassfour->FromName	 = _EMAIL_FROM_NAME;
$MailClassfour->From		   = _EMAIL_FROM_EMAIL;
$MailClassfour->Sender 	   = _EMAIL_FROM_EMAIL;





$MailClassfive = new PHPMailer();
$MailClassfive->SMTPAuth	 = true;	
//$MailClass->Mailer 	   = "tester";
$MailClassfive->Host		   = _EMAIL_SMTP_HOST;
$MailClassfive->Helo		   = _EMAIL_SMTP_HELO;
$MailClassfive->Username   = _EMAIL_SMTP_USER;
$MailClassfive->Password   = _EMAIL_SMTP_PASS; 
$MailClassfive->FromName	 = _EMAIL_FROM_NAME;
$MailClassfive->From		   = _EMAIL_FROM_EMAIL;
$MailClassfive->Sender 	   = _EMAIL_FROM_EMAIL;



$fileblock = $CONF['dir_data']."blocklist_iponly.txt";
$redirect_block = $CONF['url_redirect_blocked'];
if(file_exists($fileblock)){
$current_ip = $_SERVER['REMOTE_ADDR'];
data_load($fileblock, $data);
$arrFlip = array_flip($data);

if(array_key_exists($current_ip, $arrFlip)){

//header('Location:'..'');
$OUT->redirect($redirect_block);
exit;
 }
}




if($Q->req['lang']=="ch"){
	//$OUT->addCookie('user:Return', '-', time()-3600); 
	$langfir = $Q->req['lang'];
	//$OUT->addCookie('user:lang', $langfir);
	//$langId = '2';
	define('LANG_ID','1');
	define('LANG','ch');
//print_R($_COOKIE);
	}
else{
	//$OUT->addCookie('user:Return', '-', time()-3600); 
	$langfir = 'en';
	//$langId  = '2';
	//$OUT->addCookie('user:lang', $langfir);
	define('LANG_ID','2');
	define('LANG','en');
	}

define('DIR_TEMP',ROOT_PATH.'/design/'.$CONF['tpl_name'].'tpl/fo/tmp.php');
define('DIR_TEMP_PATH_FO',ROOT_PATH.'/design/'.$CONF['tpl_name'].'/tpl/fo');
define('DIR_TEMP_PATH_BO',ROOT_PATH.'tpl/bo');

define('DIR_TEMP_PATH_MANAGER',ROOT_PATH.'/tpl/manager');
define('DIR_TEMP_PATH_RESELLER',ROOT_PATH.'/tpl/reseller');
define('DIR_TEMP_PATH_SALESMAN',ROOT_PATH.'/tpl/salesman');



// Admin language file
if(isset($Q->cookies['admin:user_id'])){
// related to Lang lib file
define('DIR_LANG',ROOT_PATH.'lang/en/');
}
else{
// related to Lang lib file
define('DIR_LANG',ROOT_PATH.'lang/en/');
	}

$CONF['main_title']  = 'Welcome To cfoni !';
$CONF['meta_title']  = 'Welcome To cfoni !';

$CONF['meta_description']  = 'cfoni is the advanced PBX in Malaysia.';

$CONF['meta_pic'] =  HTTP_SERVER.'/design/'.$CONF['tpl_name'].'/images/logo.png';
	
// if empty mode
if(! array_key_exists('m', $Q->req)){
	$Q->req['m'] = 'main';
}
//$LANG->load('common');
//
// Setting up MODE
//
$mode = array(
	'main'      	=> array('main'    		,  'main'    ),
	'admin'     	=> array('admin'  		,  'admin'   ),
	'account'		=> array('account'  	,  'account'   ),
	'manager'		=> array('manager'  	,  'manager'   ),
	'reseller'		=> array('reseller'  	,  'reseller'   ),
	'salesman'		=> array('salesman'  	,  'salesman'   ),

);


session_start();

if (!$_SESSION['jcartToken']) {
  $_SESSION['jcartToken'] = md5(session_id() . time() . $_SERVER['HTTP_USER_AGENT']);
}




if(isset($_COOKIE['account:strRemember'])){
	if(!isset($_COOKIE['account:strUser']) || !isset($_COOKIE['account:strPass'])){
		//$OUT->addCookie('account:strRemember', '',time()-3600);
		setcookie ('account:strRemember', '', time() - 3600);
	}else{
		if(!isset($_SESSION['cookie_expire'])){
			$_SESSION['cookie_expire']=1;
			header('Location:'.$CONF['url_app'].'?m=main&c=do_login');
		}
	}
}
				  
/*
if(isset($_COOKIE['account:strRemember']) && !isset($_SESSION['cookie_expire'])){
	$_SESSION['cookie_expire']=1;
	header('Location:'.$CONF['url_app'].'?m=main&c=do_login');
}
*/

// Require the class and execute
if(file_exists(ROOT_PATH . "src/cgi/mode/". $mode[$Q->req['m']][0]. ".php")){
	require_once(ROOT_PATH . "src/cgi/mode/". $mode[$Q->req['m']][0]. ".php");
	$obj = new $mode[$Q->req['m']][1];
	$obj->_run();
}else{
	die('Mode[' . $Q->req['m'] . '] not defined.');
}


/*-------------------------------------------------------------------------*/
// Custom error handler
/*-------------------------------------------------------------------------*/

function error_handler($errno, $errstr, $errfile, $errline)
{
	// Did we turn off errors with @?
	if(! error_reporting()){
		return;
	}
	$errfile = str_replace(@getcwd(), '', $errfile);

	switch($errno){
		case E_ERROR:
			echo "<b>App Error</b> [$errno] $errstr (Line: $errline of $errfile)<br>\n";
			exit(1);
   		break;
		case E_WARNING:
			echo "<b>App Warning</b> [$errno] $errstr (Line: $errline of $errfile)<br>\n";
			break;
 		default:
			//Do nothing
			break;
	}
}

?>
