<?php
global $CONF,$Q,$CODE,$LANG;

$arrStatus = array('Y'=>'Active','N'=>'Inactive');
$arrType  = array('1'=>'Item','2'=>'Weight');

?>


<SCRIPT language="javascript">

function editsize(intProductId,x,y){
 window.open("?m=admin&c=show_category_form",""," width=400,height=300,scrollbars=1");//$data['weekly']

}

function editstatus(intCategory,x,y){
 window.open("?m=admin&c=show_edit_category&intCategory="+ intCategory,""," width=600,height=400,scrollbars=1");//$data['weekly']
}

 function Reload() {  
    window.location.reload();  
 }  

</SCRIPT>

<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/admin/shipping.js"></script>


<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/custom/gallery.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#example').dataTable( {
		"sPaginationType": "full_numbers"
	});
	
});
</script>
<div class="breadcrumbs">
    	<a href="<?php echo $CONF['url_app'];?>?m=admin">Dashboard</a>
        <span>Manage Customer Report</span>
    </div><!-- breadcrumbs -->
	
    <div class="left">
   	
<h1 class="pageTitle">Manage Customer Report</h1>
<?php
	$nErorr = count($error);
	if($nErorr > 0){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	<?php
		foreach($error as $k => $errMsg ){
		?>
			<p><?php echo $errMsg;?></p>
		
			<?php
		}
	
	?>
 	 <a class="close"></a>
 </div>
<?php
}
?>
<?php

	//$nErorr = count($error);
	if($Q->req['msg']==1){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('New Customer Report has been sucessfully added.');
location.href = "?m=admin&c=show_customereport";
</script>
 <div class="notification msgsuccess" id="messageBox">
	New Manage Customer Report has been sucessfully added.
 	 <a class="close"></a> </div>
<?php
}
	if($Q->req['msg']==2){
		?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('Your Customer Report has been sucessfully edited.');
location.href = "?m=admin&c=show_customereport";
</script>
 <div class="notification msgsuccess" id="messageBox">
	 Shipping setting has been sucessfully edited.
 	 <a class="close"></a>
 </div>
<?php
		
		
		}
if($Q->req['errMsg']==1){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	Manage Customer Report cannot be empty.
 	 <a class="close"></a> </div>
	
	<?php
	}
	if($Q->req['errMsg']==2){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
Duplicate	Collection name .
 	 <a class="close"></a>
 </div>
	
	<?php
	}

?>   


        
      
        <br />
     <div class="sTableWrapper">
    
    	
             <table cellpadding="0" cellspacing="0" border="0" class="dyntable" id="example">
             
            <thead>
            	
          
            	
                <tr>
                    <th class="head0">No.</th>
                     <th class="head1">Name</th>
                  <th class="head0">Company Name</th>
                  <th class="head1">Job Title</th>
                  <th class="head0">Email</th>
 
                  <th class="head1">Telefon</th>

                  
                     <th class="head0">Create Date</th>
                   
              </tr>
            </thead>
            <colgroup>
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
            	<col class="con0" />
            	<col class="con1" />
                <col class="con0" />
               
                
            </colgroup>
            <tbody>
             <?php
			 
			 
			
             $cData = count($data['campaign_customer']);
             $index = 1;
             if($cData > 0){
             	foreach($data['campaign_customer'] as $key => $value){
             ?>
             
                <tr>
                    <td class="con0"><?php echo $index;?></td>
                     <td class="con1"><?php echo $value['strName'];?></td>
                    <td class="con0"><?php echo $value['strCompanyName'];?></td>
                    <td class="con1"><?php echo $value['strJobTitle'];?></td>
                      
                     <td align="center"><span class="con0"><?php echo $value['strEmail'];?></span></td>

                  <td class="con1"><?php echo $value['strTel'];?></td>

                    <td class="center con0"><?php echo $value['CreateDate'];?></td>
                     
                </tr>
             <?php
             $index++;
             }
            }
            else{
            ?><tr><td colspan="9"></td></tr><?php	
            	
            }
             
             ?>  
            </tbody>
            <tfoot>
                <tr>
                    <th class="head0">No.</th>
                     <th class="head1">Name</th>
                  <th class="head0">Company Name</th>
                  <th class="head1">Job Title</th>
                  <th class="head0">Email</th>
 
                  <th class="head1">Telefon</th>

                  
                     <th class="head0">Create Date</th>
                   
              </tr>
            </tfoot>
        </table>
            
            
      </div><!--sTableWrapper-->
</div>

</div>
 </div>
    
    <br clear="all" />
