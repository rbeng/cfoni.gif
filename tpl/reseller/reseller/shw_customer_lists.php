<?php
global $CONF,$CODE,$Q;

$cReseller = count($data['reseller']);
$url_add_salesman = $CONF['url_app'].'?m=manager&c=show_salesman_form';

if($Q->req['msg']==3){
	?>
	<script>
	alert('You have sucessfully Pointed the Customer to Reseller');
	location.href='?m=salesman&c=show_customer_reseller';
	</script>
	
	<?php
}


?>
<script>
function doDelete(id){
	if (confirm("Are you sure want to delete this customer?")) {
        // your deletion code
		location.href='?m=salesman&c=do_delete_customer&id=' + id
    }
    return false;
}

function doPointResellertoCustomer(strResellerCode , intCustomerId){
	if (confirm("Are you sure want to point this customer to this Reseller?")) {
        // your deletion code
		location.href='?m=salesman&c=do_point_reseller&strResellerCode=' + strResellerCode + '&intCustomerId=' + intCustomerId ;
	}
    else{
	   location.href='?m=salesman&c=show_customer_reseller';	
		
	}
	
}


</script>






					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app']?>?m=reseller&c=show_main">Home</a>
							</li>
							<li class="active"> Customers Listings</li>
						</ul><!-- .breadcrumb -->

						<!--<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
                                    By 
                                    <select name="intSearchType">
                                    <option value="1">Full Name / Login Name</option>
                                    <option value="2">Email</option>
                                    
                                    </select>
								</span>
							</form>
						</div>-->
								<div>
									<div class="col-xs-12">
										<h2>Customers Listings</h2>
                                    
                                       
                                        <div class="hr hr-18 dotted hr-double"></div>
                                     	<div class="table-header">
											 Customers Listings
										</div>
                                      
										<div class="table-responsive">
											<table  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
                                                         No.
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
														</th>
														<th>Customer Username [ Login ]</th>
                                                      
														<th>Email </th>
														<th class="hidden-480">Contact No.</th>

														<th>
															<i class="icon-time bigger-110 hidden-480"></i>
															Date Joined
														</th>
														<th class="hidden-480">Status</th>

														
													</tr>
												</thead>

												<tbody>
													<?php
													$cArr = count($data['customer']);
													
                                                    if($Q->req['page']=="" || $Q->req['page']==1){
														$index = 1;
													}
													else{
														$index =  ($Q->req['page']*30) - 29;
														
													}
													
													 if($cArr > 0){
													  foreach($data['customer'] as $k => $value){
													
													?>
                                                    
                                                    <tr>
														<td class="center">
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
                                                            <?php
                                                            echo $index;
															?>
														</td>

														<td>
														 <?php echo $value['strUser'];?>
														</td>
                                                        
														<td><a href="mailto:<?php echo $value['strEmail'];?>" target="_self"><?php echo $value['strEmail'];?></a></td>
														<td ><?php echo $value['strMobile'];?> </td>
														<td><?php echo $value['CreateDate'];?></td>

														<td class="hidden-480">
											                <?php
                                                            if($value['intStatus']==1){
																?>
																 <span class="label label-sm label-success">Active</span>
                                                            
																<?php
															}
															elseif($value['intStatus']==2){
																?>
																<span class="label label-sm label-inverse arrowed-in">Pending</span>
																<?php
																
															}
															elseif($value['intStatus']==3){
																?>
																<span class="label label-sm label-info arrowed arrowed-righ">Suspend</span>
																
																<?php
																
															}
															elseif($value['intStatus']==4){
																?>
																<span class="label label-sm label-warning">
                                                                Inactive
                                                               </span>
																<?php
															}
															?>
                                                    	</td>

														   
                                                          </tr>
                                                       <?php
													     $index++;
														 } 
                                                       }
													   else{
														 ?>
														<tr>
                                                        <td colspan="8" align="center"> No data at the moment...</td>
                                                        </tr> 
														 <?php  
													  }
													   
													   ?>
													</tbody>
												</table>
											</div>

											<div>
                                            
                                            
                                            
                                            <ul class="pagination pull-left no-margin">
													

													
														<?php echo $this->pages->display_pages(); ?>
													

												

													
												</ul>
                                            
                                            
                                            
                                            
                                            
                                            </div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			
	      
