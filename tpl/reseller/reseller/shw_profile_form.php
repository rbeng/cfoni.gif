<?php
global $CONF,$Q,$CODE;
?>
<script type="text/javascript" src="<?php echo HTTP_SERVER?>design/<?php echo $CONF['tpl_name'];?>/js/valid.js"></script>

<?php
if($Q->req['msg']==1){
	?>
	<script>
	alert('You have sucessfully edited your profile');
	location.href="?m=salesman&c=show_profile";
	</script>
	<?php
	
}


?>

		<div>
									<div class="col-xs-12">
										<h2>Your Profile</h2> [ <b> Last Login : <?php echo $data['strLastLogin'];?> </b> ]
                                        
                                        <div class="hr hr-18 dotted hr-double"></div>
                                        			
									<div id="user-profile-3" >
										<div class="col-sm-offset-1 col-sm-10">
											<?php
                                         $cError = count($error);
										 if($cError > 0){
											 ?>
											   <b><font color="#FF0000">Error(s) Messages : <br /> ----------------------------------------------<br />
											 
											 <?php
											foreach($error as $k => $v){
												 echo $v."<br>";
											}
											?>
                                             ----------------------------------------------
                                               </font></b>
											<?php
										 }
										?>

											<form class="form-horizontal" method="post" action="<?php echo $CONF['url_app'];?>?m=salesman&c=do_update_salesman" name="frmRegistration">
												<div class="tabbable">
													<ul class="nav nav-tabs padding-16">
														<li class="active">
															<a data-toggle="tab" href="#edit-basic">
																<i class="green icon-edit bigger-125"></i>
																Basic Info
															</a>
														</li>
                                              	</ul>

													<div class="tab-content profile-edit-tab-content">
														<div id="edit-basic" class="tab-pane in active">
															<h4 class="header blue bolder smaller">General</h4>

															<div class="row">
															

																
                                                            
                                                         
																
														
															
                                                            
                                                            
                                                            
															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-email">Email Address</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		<input type="text" name="strEmail" id="form-field-email" value="<?php echo $data['strEmail'];?>" />
																		<i class="icon-envelope"></i>
																	</span>
																</div>
															</div>

															
															<div class="space-4"></div>

															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-phone">Contact No.</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		<input class="input-medium input-mask-phone" name="strContact" type="text" id="form-field-phone" value="<?php echo $data['strContact'];?>" />
																		<i class="icon-phone icon-flip-horizontal"></i>
																	</span>
																</div>
															</div>

														
															

														
															<div class="space-10"></div>
                                                            
                                                            
                                                
                                                            
                                                            
                                                            
                                                         
                                                            
                                                            
                                                            
															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-pass1">Username</label>

																<div class="col-sm-9">
																	
                                                                    <input class="input-medium input-mask-phone" type="text" id="form-field-phone" readonly value="<?php echo $Q->cookies['salesman:strUser'];?>" />
																</div>
															</div>
                                                            
                                                            <div class="space-4"></div>
                                                            
															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-pass1">Reset Password</label>

																<div class="col-sm-9">
																	<input type="password" id="form-field-pass1" name="strPass1" />
																</div>
															</div>

															
														</div>
													</div>
												</div>

												<div class="clearfix form-actions">
													<div class="col-md-offset-3 col-md-9">
														<button class="btn btn-info" type="submit">
															<i class="icon-ok bigger-110"></i>
															Save
														</button>

														&nbsp; &nbsp;
														<button class="btn" type="reset">
															<i class="icon-undo bigger-110"></i>
															Reset
														</button>
													</div>
												</div>
											</form>
										</div><!-- /span -->
									</div><!-- /user-profile -->
								</div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				<!-- /.main-content -->
