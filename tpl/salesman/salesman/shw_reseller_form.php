<?php
global $CONF;

$url_reseller_listing = $CONF['url_app'].'?m=salesman&c=show_reseller';

if($Q->req['msg']==1){
	?>
	<script>
	alert('You have sucessfully added 1 Reseller');
	location.href='?m=salesman&c=shw_reseller_form';
	</script>
	
	<?php
}

?>

					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app'];?>?m=admin">Home</a>
							</li>

							
						     <li>
								<a href="<?php echo $url_reseller_listing;?>">Reseller Listing</a>
							</li>
							<li class="active">Add New Reseller</li>
						</ul><!-- .breadcrumb -->
                      

						<!-- #nav-search -->
					</div>

<div class="page-content">
						<div class="page-header">
							<h1>
								Add / Point New Reseller
								
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
									<div class="row-fluid">
									<div class="span12">
										<div class="widget-box">
											<div class="widget-header widget-header-blue widget-header-flat">
												<h3 class="lighter block green">Enter the following information </h3>
						                  </div>

											<div class="widget-body">
												<div class="widget-main">
												
										<div class="row-fluid position-relative" >
										
                                        <?php
                                         $cError = count($error);
										 if($cError > 0){
											 ?>
											   <b><font color="#FF0000">Error(s) Messages : <br /> ----------------------------------------------<br />
											 
											 <?php
											foreach($error as $k => $v){
												 echo $v."<br>";
												 
												
											}
											?>
                                          
                                             ----------------------------------------------
                                               </font></b>
											<?php
											
											
										 }
										 
										?>
                                        
                                        
                                 <form class="form-horizontal" role="form" method="post" action="?m=salesman&c=shw_reseller_form" name="formType">   
                                
                                  <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Reseller Type</label>

										<div class="col-sm-9">
										  <select name="intType" onchange="javascript:formType.submit();" >
                                            <?php
                                            foreach($CODE['reseller_type'] as $k_value => $vType_name){
												?>
												<option value="<?php echo $k_value;?>"  <?php if($Q->req['intType']==$k_value){ echo "selected";}?>    ><?php echo $vType_name;?></option>
												
												<?php
												
											}
											?>
                                            </select>
										</div>
									</div>
                                    </form>
                                
									<form class="form-horizontal" role="form" action="<?php echo $CONF['url_app']?>?m=salesman&c=do_add_reseller" method="post" name="form">
								
                                
                                    <input type="hidden" name="intResellerType" value="<?php if($Q->req['intType']!=""){ echo $Q->req['intType'];} else{ echo "1";}?>" />
                                    
                                    <?php
                                    if($Q->req['intType']==2){
										?>
										 <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Customer Id </label>
										
										<div class="col-sm-9">
											<input type="text" readonly name='parent_name' value="" id="form-field-1"  class="col-xs-10 col-sm-5" >
											<input type="hidden" name='parent_id' value="">
                                           <button class="btn btn-info" type="button" onClick="window.open('?m=salesman&c=show_member_id','newWindow','width=1000, height=350,left=200,top=200,titlebar=no,toolbar=no,resizable=false,location=no')">
												     <i class="icon-find bigger-110"></i>
												   Search Customer Id
											     </button>          
                                            </div>
									     </div>
										<?php
										}
									
									?>
                                     <p><b id="add">
                                     
                                     
                                      <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email</label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1"  name="parent_email" value="" class="col-xs-10 col-sm-5" /> 
										</div>
									</div>
                                     
                                     
                                     
                                     
                                      <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact</label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1"  name="parent_contact" value="" class="col-xs-10 col-sm-5" /> 
										</div>
									</div>
                                    
                                    
                                     <?php
                                    if($Q->req['intType']!=2){
										?>
                                    
                                      <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Username</label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1"  name="parent_username" value="" class="col-xs-10 col-sm-5" /> 
										</div>
									</div>
                                    
                                    
                                    
                                      <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password</label>

										<div class="col-sm-9">
										  <input type="password" id="form-field-1"  name="parent_password" value="" class="col-xs-10 col-sm-5" /> 
										</div>
									</div>
                                     
                                     <?php
									}
									if($Q->req['intType']==2){
										?>
										 <div class="form-group">
                                         
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1">  <font color="#000099">* Username and Password </font> </label>
                                         	<div class="col-sm-9">
										  <input type="text" readonly id="form-field-1"   value="Same as Customer's Login Details" class="col-xs-10 col-sm-5" /> 
										</div>
                                         </div>
										
										<?php
									}
									 ?>
                                     
                                      <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Commision Rate ( % )</label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1"  name="strCommisionRate" value="<?php if($Q->req['strCommisionRate']==""){ echo 4;}else{ echo $Q->req['strCommisionRate'];};?>" class="col-xs-10 col-sm-5" /> 
										</div>
									</div>
                                    
                                   
                                    
                                  

									<div class="space-4"></div>

										<div class="form-group">
													<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Send Confirmation Email  ? </label>
										            <label class="middle">
													&nbsp;&nbsp;<input class="ace" type="checkbox" value="1" name="intAutoEmail" id="id-disable-check" />
													<span class="lbl"></span>
												</label>
														
                                        
                                        </div>					
										  
                                      </b>
 <script src="<?php echo HTTP_SERVER;?>ajaxColor/chainProduct.js"></script>
                       
                       
      </p> 
                                    				
													<hr />
													<div class="clearfix form-actions">
										           <div class="col-md-offset-3 col-md-9">
											      <button class="btn btn-info" type="submit">
												     <i class="icon-ok bigger-110"></i>
												    Submit
											     </button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												Reset
											</button>
										</div>
                                         </div>
                                        </form>
                                       
									</div>
												</div><!-- /widget-main -->
											</div><!-- /widget-body -->
										</div>
									</div>
								</div>

								
						  </div>
  </div>
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			