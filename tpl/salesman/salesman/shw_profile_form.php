<?php
global $CONF,$Q,$CODE;
?>
<script type="text/javascript" src="<?php echo HTTP_SERVER?>design/<?php echo $CONF['tpl_name'];?>/js/valid.js"></script>

<?php
if($Q->req['msg']==1){
	?>
	<script>
	alert('You have sucessfully edited your profile');
	location.href="?m=salesman&c=show_profile";
	</script>
	<?php
	
}


?>

		<div>
									<div class="col-xs-12">
										<h2>Your Profile</h2> [ <b> Last Login : <?php echo $data['strLastLogin'];?> </b> ]
                                        
                                        <div class="hr hr-18 dotted hr-double"></div>
                                        			
									<div id="user-profile-3" >
										<div class="col-sm-offset-1 col-sm-10">
											<?php
                                         $cError = count($error);
										 if($cError > 0){
											 ?>
											   <b><font color="#FF0000">Error(s) Messages : <br /> ----------------------------------------------<br />
											 
											 <?php
											foreach($error as $k => $v){
												 echo $v."<br>";
											}
											?>
                                             ----------------------------------------------
                                               </font></b>
											<?php
										 }
										?>

											<form class="form-horizontal" method="post" action="<?php echo $CONF['url_app'];?>?m=salesman&c=do_update_salesman" name="frmRegistration">
												<div class="tabbable">
													<ul class="nav nav-tabs padding-16">
														<li class="active">
															<a data-toggle="tab" href="#edit-basic">
																<i class="green icon-edit bigger-125"></i>
																Basic Info
															</a>
														</li>
                                              	</ul>

													<div class="tab-content profile-edit-tab-content">
														<div id="edit-basic" class="tab-pane in active">
															<h4 class="header blue bolder smaller">General</h4>

															<div class="row">
															

																<div class="col-xs-12 col-sm-8">
																	<div class="form-group">
																		<label class="col-sm-4 control-label no-padding-right" for="form-field-username">Full Name </label> 

																		<div class="col-sm-8">
																			<input class="col-xs-12 col-sm-10" type="text" name="strFullName" id="form-field-username"  value="<?php echo $data['strFullName'];?>" />
																		</div>
																	</div>

																	<div class="space-4"></div>

																	
																</div>
															</div>
															
                                                            
                                                         
																
														
															<div class="space-4"></div>

															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-phone">IC / Passport No.</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		<input class="input-medium input-mask-phone" name="strIc" type="text" id="form-field-phone" value="<?php echo $data['strIC'];?>" />
																		
																	</span>
																</div>
															</div>
                                                            
                                                            
                                                            
															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-email">Email Address</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		<input type="text" name="strEmail" id="form-field-email" value="<?php echo $data['strEmail'];?>" />
																		<i class="icon-envelope"></i>
																	</span>
																</div>
															</div>

															
															<div class="space-4"></div>

															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-phone">Contact No.</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		<input class="input-medium input-mask-phone" name="strContact" type="text" id="form-field-phone" value="<?php echo $data['strContact'];?>" />
																		<i class="icon-phone icon-flip-horizontal"></i>
																	</span>
																</div>
															</div>

														
															<div class="space-10"></div>
                                                            
                                                           
                                                            	<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-phone">Address</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		
																		<textarea name="strAdd" rows="10" cols="50"><?php echo $data['strAdd'];?></textarea>
																	</span>
																</div>
															</div>

														
															<div class="space-10"></div>
                                                            
                                                            
                                                         
                                             	<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-phone">Country</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		  <SELECT name="country" style="width:152px" onChange="valid_country();" tyle="width:305px">
					   
					<?php
						foreach($CODE['country'] as $countrykey => $counntry){
							?><OPTION value="<?php echo $countrykey; ?>"  <?php if($countrykey==$data['intCountry']){ echo "selected" ;}elseif($countrykey==1){echo "selected";} ?>    > <?php echo $counntry;
						}
						?>
					</SELECT>
																	</span>
																</div>
															</div>

														
															<div class="space-10"></div>
                                                                           
                                                            	<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-phone">State</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		<?php
						
				
						 if($Q->req['country']!=1 && $Q->req['country']!=""){
						?>
						
						
						<div id="div_nonmalaysia" style="display:block">
						
							<input type="text" name="non_malaysia_city" size="30" class="textfield"  maxlength="100" value="<?php echo $data['strState'];?>" onBlur="valid_state()">
						</div>
						
						<div id="div_malaysia" style="display:none">
						<select name="malaysia_city" onBlur="valid_state()" style="width:305px">
							<option value="">-- Select One --</option>
						<?php
						foreach($CODE['state_real'] as $keyState => $state){
							?><option value="<?php echo $keyState; ?>" <?php if($data['strState'] ==$keyState){ echo "selected"; }?>><?php echo $state; ?></option><?php
							
							}
						?>
					</select>
					</div>
					 <?php
					 
					}
					 ?>
					 			<?php
						 if($Q->req['country']==1 || $Q->req['country']==""){
						 	
						?>
						
						<div id="div_nonmalaysia" style="display:none">
						
							<input type="text" name="non_malaysia_city" size="30" class="textfield"  maxlength="100"  value="<?php echo $data['strState'];?>" onBlur="valid_state()">
						</div>
						
						
						<div id="div_malaysia" style="display:block">
						<select name="malaysia_city" onBlur="valid_state()" style="width:305px">
							<option value="">-- Select One --</option>
						<?php
						foreach($CODE['state_real'] as $keyState => $state){
							?><option value="<?php echo $keyState; ?>" <?php if($data['strState']==$keyState){ echo "selected"; }?>><?php echo $state; ?></option><?php
							
							}
						?>
					</select>
					</div>
					
				<?php
				}
				?>	        		
																	</span>
																</div>
															</div>

														
															<div class="space-10"></div>
                                                            
                                                               	<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-phone">City.</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		<input name="strCity" type="text" id="form-field-phone" value="<?php echo $data['strCity'];?>" />
																		
																	</span>
																</div>
															</div>

														
															<div class="space-10"></div>
                                                            
                                                            
                                                            
                                                            	<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-phone">Zip / Postal Code</label>

																<div class="col-sm-9">
																	<span class="input-icon input-icon-right">
																		<input  name="strZip" type="text" id="form-field-phone" value="<?php echo $data['strZip'];?>" />
																
																	</span>
																</div>
															</div>

														
															<div class="space-10"></div>
                                                            
                                                         
                                                            
                                                            
                                                            
															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-pass1">Username</label>

																<div class="col-sm-9">
																	
                                                                    <input class="input-medium input-mask-phone" type="text" id="form-field-phone" readonly value="<?php echo $Q->cookies['salesman:strUser'];?>" />
																</div>
															</div>
                                                            
                                                            <div class="space-4"></div>
                                                            
															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-pass1">New Password</label>

																<div class="col-sm-9">
																	<input type="password" id="form-field-pass1" name="strPass1" />
																</div>
															</div>

															<div class="space-4"></div>

															<div class="form-group">
																<label class="col-sm-3 control-label no-padding-right" for="form-field-pass2">Confirm Password</label>

																<div class="col-sm-9">
																	<input type="password" id="form-field-pass2" name="strPass2"/>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="clearfix form-actions">
													<div class="col-md-offset-3 col-md-9">
														<button class="btn btn-info" type="submit">
															<i class="icon-ok bigger-110"></i>
															Save
														</button>

														&nbsp; &nbsp;
														<button class="btn" type="reset">
															<i class="icon-undo bigger-110"></i>
															Reset
														</button>
													</div>
												</div>
											</form>
										</div><!-- /span -->
									</div><!-- /user-profile -->
								</div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				<!-- /.main-content -->
