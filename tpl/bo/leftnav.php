<?php
global $CONF,$Q,$LANG;
$extraName = substr($Q->cookies['admin:user_id'],0,2);


$arrName = array('tl'=>'Taylors Lakeside','th'=>'Taylors Hartamas','tc'=>'Taylors College','kdu'=>'KDU','sun'=>'Sunway University','rc'=>'Rock Cafe','ac'=>'Asia Cafe');
$dashboard='';

// All Users
$customer         = '';
$account_manager  = '';
$salesman         = '';
$reseller         = '';
$user_groupactive = '';

// All Setting
$product         = '';



if($Q->req['c']=="show_main"){
	$dashboard = 'active';
}
if($Q->req['c']=="show_customer"){
	$customer = 'active';
	$user_groupactive = 'active open';
}

// All Users
if($Q->req['c']=="show_account_manager" || $Q->req['c']=="show_account_manager_form" || $Q->req['c']=="do_add_account_manager"){
	$account_manager = 'active';
	$user_groupactive = 'active open';
}

if($Q->req['c']=="show_salesman"){
	$salesman = 'active';
	$user_groupactive = 'active open';
}

if($Q->req['c']=="show_reseller"){
	$reseller = 'active';
	$user_groupactive = 'active open';
}
// Transaction / Sales
if($Q->req['c']=="show_pending_order"){
	$pending_order = 'active';
	$transaction_groupactive = 'active open';
}
if($Q->req['c']=="show_order"){
	$order = 'active';
	$transaction_groupactive = 'active open';
}



// Setting
if($Q->req['c']=="show_product" || $Q->req['c']=="show_product_form" || $Q->req['c']=="do_add_product"){
	$product = 'active';
	$setting_groupactive = 'active open';
}


if($Q->req['c']=="show_package" || $Q->req['c']=="show_package_form" || $Q->req['c']=="do_add_package" ){
	$package = 'active';
	$setting_groupactive = 'active open';
}


if($Q->req['c']=="show_phone" || $Q->req['c']=="show_phone_form" || $Q->req['c']=="do_add_phone"){
	$phone = 'active';
	$setting_groupactive = 'active open';
}



if($Q->req['c']=="show_faq" || $Q->req['c']=="show_faq_form" || $Q->req['c']=="do_add_faq"){
	$faq = 'active';
	$setting_groupactive = 'active open';
}
?>	

	<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					</script>

					<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<!--<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="icon-signal"></i>
							</button>

							<button class="btn btn-info">
								<i class="icon-pencil"></i>
							</button>

							<button class="btn btn-warning">
								<i class="icon-group"></i>
							</button>

							<button class="btn btn-danger">
								<i class="icon-cogs"></i>
							</button>
						</div>

						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
						</div>-->
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
						<li class="<?php echo $dashboard;?>">
							<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_main">
								<i class="icon-dashboard"></i>
								<span class="menu-text"> <?php echo $LANG->lookup['admin']['dashboard'];?></span>
							</a>
						</li>

					
						
                         <li class="<?php echo $user_groupactive;?>">
							<a href="#" class="dropdown-toggle">
								<i class="icon-group icon-2x "></i>
								<span class="menu-text"> All Users </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								
                                <li class="<?php echo $account_manager;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_account_manager" class="<?php echo $account_manager;?>">
										<i class="icon-double-angle-right"></i>
										Account Manager Listings
									</a>
								</li>
                                <li class="<?php echo $salesman;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_salesman" class="<?php echo $salesman;?>">
										<i class="icon-double-angle-right"></i>
										Salesman Listings
									</a>
								</li>

								<li class="<?php echo $reseller;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_reseller" class="<?php echo $reseller;?>">
										<i class="icon-double-angle-right"></i>
										Reseller Listings
									</a>
								</li>
                                
                                <li class="<?php echo $customer;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_customer" class="<?php echo $customer;?>">
										<i class="icon-double-angle-right"></i>
										Customer Listings
									</a>
								</li>
                               </ul>
                           </li>
                        
                        
                        <li class="<?php echo $transaction_groupactive;?>">
							<a href="#" class="dropdown-toggle">
								<i class="icon-edit"></i>
								<span class="menu-text"> Transaction / Sales </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li class="<?php echo $pending_order;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_pending_order">
										<i class="icon-double-angle-right"></i>
										Pending Order
									</a>
								</li>

								<li class="<?php echo $order;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_order">
										<i class="icon-double-angle-right"></i>
										Sales Order
									</a>
								</li>

								
							</ul>
						</li>
                     
                        
                        
                         <li class="<?php echo $setting_groupactive;?>">
							<a href="#" class="dropdown-toggle">
								<i class="icon-cogs"></i>
								<span class="menu-text">System Setting </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li class="<?php echo $product;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_product" class="<?php echo $product;?>">
										<i class="icon-double-angle-right"></i>
										Product Listings
									</a>
								</li>
                                <li class="<?php echo $package;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_package">
										<i class="icon-double-angle-right"></i>
										Package Listings
									</a>
								</li>
                                <li class="<?php echo $phone;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_phone">
										<i class="icon-double-angle-right"></i>
										Phone Listings
									</a>
								</li>
								
								 <li class="<?php echo $faq;?>">
									<a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_faq">
										<i class="icon-double-angle-right"></i>
										FAQ Listings
									</a>
								</li>
                               </ul>
                           </li>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
						<!--
                        <li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-desktop"></i>
								<span class="menu-text"> UI 组件 </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="elements.html">
										<i class="icon-double-angle-right"></i>
										组件
									</a>
								</li>

								<li>
									<a href="buttons.html">
										<i class="icon-double-angle-right"></i>
										按钮 &amp; 图表
									</a>
								</li>

								<li>
									<a href="treeview.html">
										<i class="icon-double-angle-right"></i>
										树菜单
									</a>
								</li>

								<li>
									<a href="jquery-ui.html">
										<i class="icon-double-angle-right"></i>
										jQuery UI
									</a>
								</li>

								<li>
									<a href="nestable-list.html">
										<i class="icon-double-angle-right"></i>
										可拖拽列表
									</a>
								</li>

								<li>
									<a href="#" class="dropdown-toggle">
										<i class="icon-double-angle-right"></i>

										三级菜单
										<b class="arrow icon-angle-down"></b>
									</a>

									<ul class="submenu">
										<li>
											<a href="#">
												<i class="icon-leaf"></i>
												第一级
											</a>
										</li>

										<li>
											<a href="#" class="dropdown-toggle">
												<i class="icon-pencil"></i>

												第四级
												<b class="arrow icon-angle-down"></b>
											</a>

											<ul class="submenu">
												<li>
													<a href="#">
														<i class="icon-plus"></i>
														添加产品
													</a>
												</li>

												<li>
													<a href="#">
														<i class="icon-eye-open"></i>
														查看商品
													</a>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
						</li>
						
						<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-list"></i>
								<span class="menu-text"> 表格 </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="tables.html">
										<i class="icon-double-angle-right"></i>
										简单 &amp; 动态
									</a>
								</li>

								<li>
									<a href="jqgrid.html">
										<i class="icon-double-angle-right"></i>
										jqGrid plugin
									</a>
								</li>
							</ul>
						</li>

						<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-edit"></i>
								<span class="menu-text"> 表单 </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="form-elements.html">
										<i class="icon-double-angle-right"></i>
										表单组件
									</a>
								</li>

								<li>
									<a href="form-wizard.html">
										<i class="icon-double-angle-right"></i>
										向导提示 &amp; 验证
									</a>
								</li>

								<li>
									<a href="wysiwyg.html">
										<i class="icon-double-angle-right"></i>
										编辑器
									</a>
								</li>

								<li>
									<a href="dropzone.html">
										<i class="icon-double-angle-right"></i>
										文件上传
									</a>
								</li>
							</ul>
						</li>

						<li>
							<a href="widgets.html">
								<i class="icon-list-alt"></i>
								<span class="menu-text"> 插件 </span>
							</a>
						</li>

						<li>
							<a href="calendar.html">
								<i class="icon-calendar"></i>

								<span class="menu-text">
									日历
									<span class="badge badge-transparent tooltip-error" title="2&nbsp;Important&nbsp;Events">
										<i class="icon-warning-sign red bigger-130"></i>
									</span>
								</span>
							</a>
						</li>

						<li>
							<a href="gallery.html">
								<i class="icon-picture"></i>
								<span class="menu-text"> 相册 </span>
							</a>
						</li>

						<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-tag"></i>
								<span class="menu-text"> 更多页面 </span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="profile.html">
										<i class="icon-double-angle-right"></i>
										用户信息
									</a>
								</li>

								<li>
									<a href="inbox.html">
										<i class="icon-double-angle-right"></i>
										收件箱
									</a>
								</li>

								<li>
									<a href="pricing.html">
										<i class="icon-double-angle-right"></i>
										售价单
									</a>
								</li>

								<li>
									<a href="invoice.html">
										<i class="icon-double-angle-right"></i>
										购物车
									</a>
								</li>

								<li>
									<a href="timeline.html">
										<i class="icon-double-angle-right"></i>
										时间轴
									</a>
								</li>

								<li>
									<a href="login.html">
										<i class="icon-double-angle-right"></i>
										登录 &amp; 注册
									</a>
								</li>
							</ul>
						</li>

						<li>
							<a href="#" class="dropdown-toggle">
								<i class="icon-file-alt"></i>

								<span class="menu-text">
									其他页面
									<span class="badge badge-primary ">5</span>
								</span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
								<li>
									<a href="faq.html">
										<i class="icon-double-angle-right"></i>
										帮助
									</a>
								</li>

								<li>
									<a href="error-404.html">
										<i class="icon-double-angle-right"></i>
										404错误页面
									</a>
								</li>

								<li>
									<a href="error-500.html">
										<i class="icon-double-angle-right"></i>
										500错误页面
									</a>
								</li>

								<li>
									<a href="grid.html">
										<i class="icon-double-angle-right"></i>
										网格
									</a>
								</li>

								<li>
									<a href="blank.html">
										<i class="icon-double-angle-right"></i>
										空白页面
									</a>
								</li>
							</ul>
						</li>-->
					</ul> 

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
					</div>
					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
				</div>
