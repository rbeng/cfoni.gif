<?php
global $CONF,$Q,$OUT,$CODE;

if($Q->req['msg']==3){
?>
<script>
alert('You have sucessfully to change the status.');
location.href = "?m=admin&c=show_customer";
</script>
<?php	
}

if($Q->req['msg']==4){
?>
<script>
alert('You have sucessfully to point this Internal Salesman to handle this customer , Internal Salesman need to point the Reseller as well.');
location.href = "?m=admin&c=show_customer";
</script>
<?php	
}

if($Q->req['msg']==5){
?>
<script>
alert('You have sucessfully to point this salesman to handle this customer');
location.href = "?m=admin&c=show_customer";
</script>
<?php	
}
if($Q->req['msg']==6){
?>
<script>
alert('You have sucessfully to point this customer to empty Salesman');
location.href = "?m=admin&c=show_customer";
</script>
<?php	
}


?>
<script>
function doChangeStatus(intStatus , id){
	
	
	location.href="?m=admin&c=do_change_status_customer&id=" + id + "&intStatus="+intStatus;
	
	
	
}

function doPointSalesman(strSalesmanCode,intCustomerId){
	if (confirm("Are you sure want to point this Customer to this Salesman?")) {
        // your deletion code
		location.href='?m=admin&c=do_point_salesman&strSalesmanCode=' + strSalesmanCode + '&intCustomerId=' + intCustomerId 
    }
    else{
	   location.href='?m=admin&c=show_customer';	
		
	}
	
}



</script>


<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app'];?>?m=admin"><?php echo "Home";?></a>
							</li>
							<li class="active"><?php echo "Customer Management";?></li>
						</ul><!-- .breadcrumb -->

					</div>

									<div class="col-xs-12">
										<h2>Customer Listings</h2>
                                        
                                        <div class="hr hr-18 dotted hr-double"></div>
                                     
                                     	<div class="table-header">
											Customer Listings
										</div>
                                      
										<div class="table-responsive">
											<table  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
                                                         No.
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
														</th>
														<th>Username</th>
														
														
                                                        <th class="hidden-480">Email</th>
                                                         <th class="hidden-480">Contact No.</th>
                                                         <th class="hidden-480">Reseller </th>
                                                         <th class="hidden-480">Salesman </th>
														<th>
															<i class="icon-time bigger-110 hidden-480"></i>
															Date Created
														</th>
														<th class="hidden-480">Status</th>

														
													</tr>
												</thead>

												<tbody>
													<?php
													$cArr = count($data['customer']);
													
                                                    if($Q->req['page']=="" || $Q->req['page']==1){
														$index = 1;
													}
													else{
														$index =  ($Q->req['page']*30) - 29;
														
													}
													
													 if($cArr > 0){
													  foreach($data['customer'] as $k => $value){
													
													?>
                                                    
                                                    <tr>
														<td class="center">
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
                                                          <?php
                                                            echo $index;
															?>
                                                          
														</td>

														<td>
															  <?php echo $value['strUser'];?>
														</td>
														
														
                                                        <td class="hidden-480"><a href="mailto:<?php echo $value['strEmail'];?>"><?php echo $value['strEmail'];?></a></td>
                                                        <td class="hidden-480"><?php echo $value['strContact'];?></td>
                                                        <td class="hidden-480"><?php echo $value['strResellerCode'];?></td>
                                                        <td class="hidden-480">
														<?php
                                                        //echo "<pre>";
														//print_R($data['salesman_selection']);
														?>
                                                        <select name="strSalesmanCode" onchange="javascript:doPointSalesman(this.value,'<?php echo $k;?>')">
                                                         <option valur="0">------ Point to Salesman ---------</option>
                                                        <?php
                                                        $cSalesman = count($data['salesman_selection']);
														 if($cSalesman > 0){
															 foreach($data['salesman_selection'] as $v_salesman => $d_salesman){
																
																 ?>
																 <option value="<?php echo $d_salesman['type'].":".$v_salesman;?>" <?php if($value['strSalesmanCode']==$v_salesman){ echo "selected";}?>><?php echo $d_salesman['details'];?> </option>
																 <?php
																 
																
															 }
															 
															}
														?>
                                                        
                                                        
                                                        
                                                        </select>	
													
                                                        
                                                        
                                                        
                                                        </td>
                                                       <!-- <td class="hidden-480"><?php if($value['strSalesmanCode']!=""){ echo $value['strSalesmanCode'];}else{ echo $value['strResellerCode'];};?></td>-->
														<td><?php echo $value['CreateDate'];?></td>

														<td class="hidden-480">
											            
                                                        
                                                            <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                               							   <select name="intStatus" onchange="javascript:doChangeStatus(this.value,'<?php echo $value['intCustomerId'];?>')">
                                                           <?php
                                                           foreach($CODE['status'] as $k => $v_status){
															   ?>
															   <option value="<?php echo $k;?>"  <?php if($value['intStatus']==$k){ echo "selected";}?>     ><?php echo $v_status?></option>
															   <?php
															   
															  }
														   
														   ?>
                                                        
                                                           </select>
                                                        
                                                           
                                                        		
                                                                </div>
														</td>
	
                                                          </tr>
                                                       <?php
													     $index++;
														 } 
                                                       }
													   else{
														 ?>
														<tr>
                                                        <td colspan="9" align="center"> No data at the moment...</td>
                                                        </tr> 
														 <?php  
													  }
													   
													   ?>
													</tbody>
												</table>
											</div>

											<div>
                                            
                                            
                                            
                                            <ul class="pagination pull-left no-margin">
														<?php //echo $this->pages->display_pages(); ?>
												</ul>
                                            </div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			
	      