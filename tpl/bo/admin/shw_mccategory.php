<?php
global $CONF,$Q,$CODE,$LANG;

$arrStatus = array('Y'=>'Active','N'=>'Inactive');
$arrType  = array('1'=>'Item','2'=>'Weight');

?>


<SCRIPT language="javascript">

function editsize(intProductId,x,y){
 window.open("?m=admin&c=show_category_form",""," width=400,height=300,scrollbars=1");//$data['weekly']

}

function editstatus(intCategory,x,y){
 window.open("?m=admin&c=show_edit_category&intCategory="+ intCategory,""," width=600,height=400,scrollbars=1");//$data['weekly']
}

 function Reload() {  
    window.location.reload();  
 }  

</SCRIPT>

<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/admin/shipping.js"></script>


<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/custom/gallery.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#example').dataTable( {
		"sPaginationType": "full_numbers"
	});
	
});
</script>
<div class="breadcrumbs">
    	<a href="<?php echo $CONF['url_app'];?>?m=admin">Dashboard</a>
        <span>Manage Categories</span>
    </div><!-- breadcrumbs -->
	
    <div class="left">
   	
<h1 class="pageTitle">Manage Categories</h1>
<?php
	$nErorr = count($error);
	if($nErorr > 0){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	<?php
		foreach($error as $k => $errMsg ){
		?>
			<p><?php echo $errMsg;?></p>
		
			<?php
		}
	
	?>
 	 <a class="close"></a>
 </div>
<?php
}
?>
<?php
	//$nErorr = count($error);
	if($Q->req['msg']==1){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('New Manage Category has been sucessfully added.');
location.href = "?m=admin&c=show_mcategory";
</script>
 <div class="notification msgsuccess" id="messageBox">
	New Manage Categories has been sucessfully added.
 	 <a class="close"></a> </div>
<?php
}
	if($Q->req['msg']==2){
		?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('Your Manage Category has been sucessfully edited.');
location.href = "?m=admin&c=show_mcategory";
</script>
 <div class="notification msgsuccess" id="messageBox">
	 Shipping setting has been sucessfully edited.
 	 <a class="close"></a>
 </div>
<?php
		
		
		}
if($Q->req['errMsg']==1){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	Manage Categories cannot be empty.
 	 <a class="close"></a> </div>
	
	<?php
	}
	if($Q->req['errMsg']==2){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
Duplicate Category name .
 	 <a class="close"></a>
 </div>
	
	<?php
	}

?>   

       <?php
       if($Q->req['type']==1){
       	?>
         <form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_update_category" id="formshipping" enctype="multipart/form-data">
         <input type="hidden" value="<?php echo $data['categoryedit']['intCategoryId'];?>" name="id" />
      
        	
        	<div class="form_default">
        
        		<fieldset>
                 		<h1 class="pageTitle">Edit Category</h1>   
        <p>Category Name&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="text" name="strCategoryName"  value="<?php echo $data['categoryedit']['strCategoryName'];?>" class="sf" /></p>
      
                   
                     
                <p>Category Photo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="file" name="strCategoryPhoto"  class="sf" /></p>
                <p>Description</p>
                <textarea cols="40" rows="5" name="strDescription"> <?php echo $data['categoryedit']['strDescription'];?>
                      </textarea>
                 <p>Order&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="text" name="intOrder"  value="<?php echo $data['categoryedit']['intOrder'];?>" class="sf" /></p>
      
                  
			<p>Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              <select name="intStatus"  class="sf" />
                       		<?php
                       		foreach($CODE['type_banner_status'] as $k => $valueTo){
                       			?><option value="<?php echo $k;?>" <?php if($k==$data['categoryedit']['intStatus']){ echo "selected";}?> ><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                       		</select></p>
    <br>
                       	
                       		<button>Save</button>
                       	</p>
              
                    
                      	</p>
                  
                    	
        		</fieldset>
        		
        	</div>
        </form>
       	
       	
       	
       	<?php
       	
       	
       	}
       
       else{
       ?>
        <form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_add_category" id="formshipping" enctype="multipart/form-data">
      
        	
        	<div class="form_default">
        
        		<fieldset>
                 		<h1 class="pageTitle">New Category</h1>   
        <p>Category Name&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="text" name="strCategoryName"  value="<?php echo $Q->req['strCategoryName'];?>" class="sf" />
        </p>
      
                   
                     
                <p>Category Photo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="file" name="strCategoryPhoto"  class="sf" /></p>
                <p>Description</p>
                <textarea cols="40" rows="5" name="strDescription"> <?php echo $Q->req['strDescription'];?>
                      </textarea>
                
             <p>Order&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="text" name="intOrder"  value="<?php echo $Q->req['intOrder'];?>" class="sf" /></p>
            
			<p>Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              <select name="intStatus" value="<?php echo $Q->req['intStatus'];?>" class="sf" />
                       		<?php
                       		foreach($CODE['type_banner_status'] as $k => $valueTo){
                       			?><option value="<?php echo $k;?>"><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                       		</select></p>
    <br>
                       	
                       		<button>Add</button>
                       	</p>
              
                    
                      	</p>
                  
                    	
        		</fieldset>
        		
        	</div>
        </form>
       <?php
      }
       ?> 
        
        
        
      
        <br />
     <div class="sTableWrapper">
    
    	
             <table cellpadding="0" cellspacing="0" border="0" class="dyntable" id="example">
             
            <thead>
            	
          
            	
                <tr>
                    <th class="head0">No.</th>
                     <th class="head1">Category Name</th>
                  <th class="head0">Category Photo</th>
                    <th class="head1">Order</th>
                  <th class="head0">Description</th>
                
                    <th class="head1">Status</th>
                     <th class="head0">Create Date</th>
                    <th class="head1">Action</th>
                </tr>
            </thead>
            <colgroup>
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
            	<col class="con0" />
            	<col class="con1" />
            	<col class="con0" />
                <col class="con1" />
            </colgroup>
            <tbody>
             <?php
             $cData = count($data['category']);
             $index = 1;
             if($cData > 0){
             	foreach($data['category'] as $key => $value){
             ?>
             
                <tr>
                    <td class="con0"><?php echo $index;?></td>
                     <td class="con1"><?php echo $value['strCategoryName'];?></td>
      <td class="con0"><span class="listview">
                      <?php
                     	$folder_id = (int)($value['intCategoryId']/ $CONF['const_photo_per_dir'    ]);  
                     $dirpoto = $CONF['dir_photo'].'/categoryphoto/'.$folder_id.'/'.$value['intCategoryId'].'/'.$value['strCategoryPhoto']; 
                     	if(file_exists($dirpoto)){
                     		$image 		= $CONF['http_photo'].'/categoryphoto/'.$folder_id.'/'.$value['intCategoryId'].'/'.$value['strCategoryPhoto']; 
                     		$imageBig = $CONF['http_photo'].'/categoryphoto/'.$folder_id.'/'.$value['intCategoryId'].'/'.$value['strCategoryPhoto']; 
                     		}
                   
                     	?>
                    </span><span class="con1"><a href="<?php echo $imageBig;?>" class="view"><img src="<?php echo $image;?>" alt="<?php echo $value['strCategoryPhoto'];?>" width=46 /></a></span></td>
                        <td class="con0"><?php echo $value['intOrder'];?></td>
                     <td align="center">
                   
                     	<?php echo $value['strDescription'];?>                     	</td>
                     	<td class="center con0"><?php echo $CODE['type_banner_status'][$value['intStatus']];?></td>
            <td class="con1"><span class="center con1"><?php echo $value['CreateDate'];?></span></td>
                    
                     <td class="center con0"><a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_mcategory&type=1&id=<?php echo $key;?>">Edit</a> | <a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_subcategory&id=<?php echo $key;?>">Manage Subcategory</a></td>
                </tr>
             <?php
             $index++;
             }
            }
            else{
            ?><tr><td colspan="9"></td></tr><?php	
            	
            }
             
             ?>  
            </tbody>
            <tfoot>
             <tr>
                     <th class="head0">No.</th>
                     <th class="head1">Category Name</th>
                  <th class="head0">Category Photo</th>
                    <th class="head1">Order</th>
                  <th class="head0">Description</th>
                
                    <th class="head1">Status</th>
                     <th class="head0">Create Date</th>
                    <th class="head1">Action</th>
                </tr>
            </tfoot>
        </table>
            
      </div><!--sTableWrapper-->
   </div>

</div>
 </div>
    
    <br clear="all" />