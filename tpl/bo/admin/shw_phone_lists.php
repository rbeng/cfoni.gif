<?php
global $CONF,$CODE,$Q;

$url_add_account_manager = $CONF['url_app'].'?m=admin&c=show_phone_form';
$url_bulk_upload         = $CONF['url_app'].'?m=admin&c=show_phone_bulk_form';

if($Q->req['msg']==2){
	?>
	<script>
	alert('You have sucessfully deleted 1 Phone Record');
	location.href='?m=admin&c=show_phone';
	</script>
	
	<?php
}


?>
<script>
function doDelete(id){
	if (confirm("Are you sure want to delete this phone number?")) {
        // your deletion code
		location.href='?m=admin&c=do_delete_phone&id=' + id
    }
    return false;
}
</script>






					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app']?>?m=admin&c=show_main">Home</a>
							</li>
							<li class="active">Phone Listings</li>
						</ul><!-- .breadcrumb -->

						<!--<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
                                    By 
                                    <select name="intSearchType">
                                    <option value="1">Full Name / Login Name</option>
                                    <option value="2">Email</option>
                                    
                                    </select>
								</span>
							</form>
						</div>-->
								<div>
									<div class="col-xs-12">
										<h2>Phone Listings</h2>
                                        
                                        <div class="hr hr-18 dotted hr-double"></div>
                                      
                                        
                                        <!-- <a href="<?php echo $url_add_account_manager;?>" class="btn btn-app btn-light btn-xs">
												<i class="icon-edit bigger-160"></i>
												NEW
											</a>-->
                                       
                                       <a href="<?php echo $url_bulk_upload;?>"  class="fancybox fancybox.iframe btn btn-app btn-purple btn-xs fancybox">
												<i class="icon-cloud-upload bigger-160"></i>
												Upload
											</a>
                                       
                                       
                                       
                                        <div class="hr hr-18 dotted hr-double"></div>
                                     	<div class="table-header">
											Product Listings
										</div>
                                      
										<div class="table-responsive">
											<table  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
                                                         No.
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
														</th>
                                                        <th>Batch Id</th>
														<th>Phone No.</th>
														
														<th class="hidden-480">Type</th>
                                                        <th class="hidden-480">Price ( RM )</th>

														<th>
															<i class="icon-time bigger-110 hidden-480"></i>
															Date Created
														</th>
														<th class="hidden-480">Status</th>

														<th>Action(s)</th>
													</tr>
												</thead>

												<tbody>
													<?php
													$cArr = count($data['phone']);
													
                                                    if($Q->req['page']=="" || $Q->req['page']==1){
														$index = 1;
													}
													else{
														$index =  ($Q->req['page']*30) - 29;
														
													}
													
													 if($cArr > 0){
													  foreach($data['phone'] as $k => $value){
													
													?>
                                                    
                                                    <tr>
														<td class="center">
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
                                                          <?php
                                                            echo $index;
															?>
                                                          
														</td>
														<td>
															 <?php echo $value['strBatch'];?> 
														</td>
														<td>
															 <?php echo $value['strNumber'];?> 
														</td>
														
														<td class="hidden-480"><?php echo $value['intType'];?></td>
                                                        <td class="hidden-480"><?php echo $value['strPrice'];?></td>
														<td><?php echo $value['CreateDate'];?></td>

														<td class="hidden-480">
											                <?php
                                                            if($value['intStatus']==1){
																?>
																 <span class="label label-sm label-success">New</span>
                                                            
																<?php
															}
															elseif($value['intStatus']==2){
																?>
																<span class="label label-sm label-inverse arrowed-in">In-used</span>
																<?php
																
															}
															elseif($value['intStatus']==3){
																?>
																<span class="label label-sm label-info arrowed arrowed-righ">Pending</span>
																
																<?php
																
															}
															elseif($value['intStatus']==4){
																?>
																<span class="label label-sm label-warning">
                                                                Suspend
                                                               </span>
																<?php
																
															}
															?>
                                                            
                               
														</td>

														<td>
															<div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
																
                                            					<a class="red" href="#" onClick="javascript:doDelete('<?php echo encrypt($value['intDidFaxId'],'cfoni.8888');?>')">
																	<i class="icon-trash bigger-130"></i>
																</a>
															</div>

															</td>	
                                                          </tr>
                                                       <?php
													     $index++;
														 } 
                                                       }
													   else{
														 ?>
														<tr>
                                                        <td colspan="7" align="center"> No data at the moment...</td>
                                                        </tr> 
														 <?php  
													  }
													   
													   ?>
													</tbody>
												</table>
											</div>

											<div>
                                            
                                            
                                            
                                            <ul class="pagination pull-left no-margin">
														<?php echo $this->pages->display_pages(); ?>
												</ul>
                                            </div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			
	      
