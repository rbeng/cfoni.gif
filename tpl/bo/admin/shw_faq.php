<?php
global $CONF,$CODE;

$url_add_faq = $CONF['url_app'].'?m=admin&c=show_faq_form';


if($Q->req['msg']==2){
	?>
	<script>
	alert('You have sucessfully deleted 1 FAQ Record');
	location.href='?m=admin&c=show_faq';
	</script>
	
	<?php
}
?>

<script>
function doDelete(id){
	if (confirm("Are you sure want to delete this FAQ?")) {
        // your deletion code
		location.href='?m=admin&c=do_delete_FAQ&id=' + id
    }
    return false;
}
</script>

<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app'];?>?m=admin"><?php echo "Home";?></a>
							</li>
							<li class="active"><?php echo "FAQ Management";?></li>
						</ul><!-- .breadcrumb -->

					</div>

									<div class="col-xs-12">
										<h2>FAQ Listings</h2>
                                        
                                        
                                         <a href="<?php echo $url_add_faq;?>" class="btn btn-app btn-light btn-xs">
												<i class="icon-edit bigger-160"></i>
												NEW
											</a>
                                        <div class="hr hr-18 dotted hr-double"></div>
                                     
                                     	 
                                      
										<div class="table-responsive">
											<table  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
                                                         No.
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
														</th>
														<th>Added By</th>
														
														
                                                        <th class="hidden-480">FAQ Type</th>
                                                         <th class="hidden-480">Question</th>
                                                         <th class="hidden-480">Answer</th>
                                                         <th class="hidden-480">Order </th>
														<th>
															<i class="icon-time bigger-110 hidden-480"></i>
															Date Created
														</th>
														<th class="hidden-480">Status</th>

														
													</tr>
												</thead>

												<tbody>
													<?php
													$cArr = count($data['faq']);
													
                                                    if($Q->req['page']=="" || $Q->req['page']==1){
														$index = 1;
													}
													else{
														$index =  ($Q->req['page']*30) - 29;
														
													}
													
													 if($cArr > 0){
													  foreach($data['faq'] as $k => $value){
													
													?>
                                                    
                                                    <tr>
														<td class="center">
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
                                                          <?php
                                                            echo $index;
															?>
                                                          
														</td>

														<td>
															  <?php echo $value['strAddedBy'];?>
														</td>
														
														
                                                        <td class="hidden-480"><?php echo $value['intType'];?></td>
                                                        <td class="hidden-480"><?php echo html_entity_decode($value['strQ']);?></td>
                                                        <td class="hidden-480"><?php echo html_entity_decode($value['strA']);?></td>
                                                        <td class="hidden-480">
														<?php echo $value['intOrder'];?>
                                                        
                                                        
                                                        </td>
                                                      
														<td><?php echo $value['CreateDate'];?></td>

													
											            
                                                        <td>
                                                         <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
																<a class="fancybox fancybox.iframe" href="?m=admin&c=show_faq_details&id=<?php echo $value['intFaqId'];?>">
																	<i class="icon-pencil bigger-130"></i>
																</a>
                                            					<a class="red" href="#" onClick="javascript:doDelete('<?php echo encrypt($value['intFaqId'],'cfoni.8888');?>')">
																	<i class="icon-trash bigger-130"></i>
																</a>
															</div>
                                                           
                                                        		
                                                              
														</td>
	
                                                          </tr>
                                                       <?php
													     $index++;
														 } 
                                                       }
													   else{
														 ?>
														<tr>
                                                        <td colspan="9" align="center"> No data at the moment...</td>
                                                        </tr> 
														 <?php  
													  }
													   
													   ?>
													</tbody>
												</table>
											</div>

											<div>
                                            
                                            
                                            
                                            <ul class="pagination pull-left no-margin">
														<?php //echo $this->pages->display_pages(); ?>
												</ul>
                                            </div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			
	      