<?php
global $CONF,$Q,$CODE,$LANG;

$arrStatus = array('Y'=>'Active','N'=>'Inactive');
$arrType  = array('1'=>'Item','2'=>'Weight');

?>


<SCRIPT language="javascript">

function editsize(intProductId,x,y){
 window.open("?m=admin&c=show_category_form",""," width=400,height=300,scrollbars=1");//$data['weekly']

}

function editstatus(intCategory,x,y){
 window.open("?m=admin&c=show_edit_category&intCategory="+ intCategory,""," width=600,height=400,scrollbars=1");//$data['weekly']
}

 function Reload() {  
    window.location.reload();  
 }  

</SCRIPT>

<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/admin/shipping.js"></script>


<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/custom/gallery.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#example').dataTable( {
		"sPaginationType": "full_numbers"
	});
	
});
</script>
<div class="breadcrumbs">
    	<a href="<?php echo $CONF['url_app'];?>?m=admin">Dashboard</a>
        <span>Banner Gallery</span>
    </div><!-- breadcrumbs -->
	
    <div class="left">
   	
<h1 class="pageTitle">Manage Banner Gallery</h1>
<?php
	$nErorr = count($error);
	if($nErorr > 0){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	<?php
		foreach($error as $k => $errMsg ){
		?>
			<p><?php echo $errMsg;?></p>
		
			<?php
		}
	
	?>
 	 <a class="close"></a>
 </div>
<?php
}
?>
<?php
	//$nErorr = count($error);
	if($Q->req['msg']==1){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('New Banner has been sucessfully added.');
location.href = "?m=admin&c=show_banner_lists";
</script>
 <div class="notification msgsuccess" id="messageBox">
	New Banner has been sucessfully added.
 	 <a class="close"></a>
 </div>
<?php
}
	if($Q->req['msg']==2){
		?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('Your Banner has been sucessfully edited.');
location.href = "?m=admin&c=show_banner_lists";
</script>
 <div class="notification msgsuccess" id="messageBox">
	 Banner setting has been sucessfully edited.
 	 <a class="close"></a>
 </div>
<?php
		
		
		}
if($Q->req['errMsg']==1){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	Banner cannot be empty.
 	 <a class="close"></a>
 </div>
	
	<?php
	}
	if($Q->req['errMsg']==2){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
Duplicate	Collection name .
 	 <a class="close"></a>
 </div>
	
	<?php
	}

?>   

       <?php
       if($Q->req['type']==1){
       	?>
        <form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_update_banner" id="formshipping" enctype="multipart/form-data">
      
        	<input type="hidden" name="id" value="<?php echo $data['banneredit']['intBannerId'];?>" />
        	<div class="form_default">
        
        		<fieldset>
                 		<h1 class="pageTitle">Edit Banner</h1>   
                       <p>Banner Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                       	<select name="intBannerType" class="sf" />
                       		<?php
                       		foreach($CODE['type_banner'] as $k => $valueTo){
                       			?><option value="<?php echo $k;?>"  <?php if($k==$data['banneredit']['intTypeBanner']){ echo "selected";}      ?>  ><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                       		</select></p>
                    
                     
                     
                      	
                      <p>Banner Hyperlink &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="strBannerlink"  value="<?php echo $data['banneredit']['strLink'];?>" class="sf" /> 
                      eg.http://xxxx.xxx.xxx
                      	
                      <p>Banner Caption &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="strBannerCaption"  value="<?php echo $data['banneredit']['strCaption'];?>" class="sf" /></p>
      
                   
                     
                        <p>Banner Order &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="intBannerOrder"  value="<?php echo $data['banneredit']['intOrder'];?>" class="sf" /> 
                      	
                      <p>Banner Image &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      <input type="file" name="strBannerImage"  class="sf" /></p>
      <p> Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<select name="intStatus" class="sf" />
                       		<?php
                       		foreach($CODE['type_banner_status'] as $k => $valueTo){
                       			?><option value="<?php echo $k;?>" <?php if($k==$data['banneredit']['intStatus']){ echo "selected";}      ?>><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                       		</select></p>
    <br>
                       	
                       		<button>Save</button>
                       	</p>
              
                    
                      	</p>
                  
                    	
        		</fieldset>
        		
        	</div>
        </form>
       <?php
      }
       
       
       else{
       ?>
        <form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_add_banner" id="formshipping" enctype="multipart/form-data">
      
        	
        	<div class="form_default">
        
        		<fieldset>
                 		<h1 class="pageTitle">New Banner</h1>   
                       <p>Banner Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                       	<select name="intBannerType" value="<?php echo $Q->req['intBannerType'];?>" class="sf" />
                       		<?php
                       		foreach($CODE['type_banner'] as $k => $valueTo){
                       			?><option value="<?php echo $k;?>"><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                       		</select></p>
                    
                     
                     
                      	
                      <p>Banner Hyperlink &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="strBannerlink"  value="http://<?php echo $Q->req['strBannerlink'];?>" class="sf" /> 
                      eg.http://xxxx.xxx.xxx
                      	
                      <p>Banner Caption &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="strBannerCaption"  value="<?php echo $Q->req['strBannerCaption'];?>" class="sf" /></p>
      
                   
                     
                        <p>Banner Order &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="intBannerOrder"  value="<?php echo $Q->req['intBannerOrder'];?>" class="sf" /> 
                      	
                      <p>Banner Image &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="strBannerImage"  class="sf" /></p>
      <p> Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<select name="intStatus" value="<?php echo $Q->req['intStatus'];?>" class="sf" />
                       		<?php
                       		foreach($CODE['type_banner_status'] as $k => $valueTo){
                       			?><option value="<?php echo $k;?>"><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                       		</select></p>
    <br>
                       	
                       		<button>Add</button>
                       	</p>
              
                    
                      	</p>
                  
                    	
        		</fieldset>
        		
        	</div>
        </form>
       <?php
      }
       ?> 
        
        
        
      
        <br />
     <div class="sTableWrapper">
    
    	
             <table cellpadding="0" cellspacing="0" border="0" class="dyntable" id="example">
             
            <thead>
            	
          
            	
                <tr>
                    <th class="head0">No.</th>
                     <th class="head1">Banner Type</th>
                    <th class="head0">Banner Hyperlink</th>
                    <th class="head1">Banner Caption</th>
                    <th class="head0">Banner Order</th>
                    <th class="head1">Banner Image</th>
                    <th class="head0">Status</th>
                     <th class="head1">Create Date</th>
                    <th class="head0">Action</th>
                </tr>
            </thead>
            <colgroup>
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
            	<col class="con0" />
            	<col class="con1" />
            	<col class="con0" />
            	<col class="con1" />
            </colgroup>
            <tbody>
             <?php
             $cData = count($data['banner']);
             $index = 1;
             if($cData > 0){
             	foreach($data['banner'] as $key => $value){
             ?>
             
                <tr>
                    <td class="con0"><?php echo $index;?></td>
                     <td class="con1"><?php echo $CODE['type_banner'][$value['intTypeBanner']];?></td>
                    <td class="con0"><?php echo $value['strLink'];?></td>
                      
                     <td align="center">
                   
                     	<?php echo $value['strCaption'];?>
                     	
                    
                     	</td>
                     	<td class="center con0"><?php echo $value['intOrder'];?></td>
                    <td class="con1"> 
                 <div id="listview" class="listview">
                     	<?php
                     	$folder_id = (int)($value['intBannerId']/ $CONF['const_photo_per_dir'    ]);  
                     $dirpoto = $CONF['dir_photo'].'/mainbanner/'.$folder_id.'/'.$value['intBannerId'].'/'.$value['strBanner']; 
                     	if(file_exists($dirpoto)){
                     		$image 		= $CONF['http_photo'].'/mainbanner/'.$folder_id.'/'.$value['intBannerId'].'/'.$value['strBanner']; 
                     		$imageBig = $CONF['http_photo'].'/mainbanner/'.$folder_id.'/'.$value['intBannerId'].'/'.$value['strBanner']; 
                     		}
                   
                     	?>
                    	
                    	
                    		<a href="<?php echo $imageBig;?>" class="view"><img src="<?php echo $image;?>" alt="<?php echo $value['strCaption'];?>" width=46> </a></div></td>
                    
                    <td class="center con0"><?php echo $CODE['type_banner_status'][$value['intStatus']];?></td>
                    <td class="center con1"><?php echo $value['CreateDate'];?></td>
                     <td class="center con0"><a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_banner_lists&type=1&id=<?php echo $key;?>">Edit</a> </td>
                </tr>
             <?php
             $index++;
             }
            }
            else{
            ?><tr><td colspan="9"></td></tr><?php	
            	
            }
             
             ?>  
            </tbody>
            <tfoot>
             <tr>
                    <th class="head0">No.</th>
                     <th class="head1">Banner Type</th>
                    <th class="head0">Banner Hyperlink</th>
                    <th class="head1">Banner Caption</th>
                    <th class="head0">Banner Order</th>
                    <th class="head1">Banner Image</th>
                    <th class="head0">Status</th>
                     <th class="head1">Create Date</th>
                    <th class="head0">Action</th>
                </tr>
            </tfoot>
        </table>
            
	     </div><!--sTableWrapper-->
   </div>

 </div>
 </div>
    
    <br clear="all" />