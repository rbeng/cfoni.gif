<?php
global $CONF,$Q,$CODE,$LANG;

$arrStatus = array('Y'=>'Active','N'=>'Inactive');
$arrType  = array('1'=>'Item','2'=>'Weight');
$URL = HTTP_SERVER.'ckeditor/';
?>

<script type="text/javascript" src="<?php  echo $URL;?>/ckeditor.js"></script>
<SCRIPT language="javascript">

function editsize(intProductId,x,y){
 window.open("?m=admin&c=show_category_form",""," width=400,height=300,scrollbars=1");//$data['weekly']

}

function editstatus(intCategory,x,y){
 window.open("?m=admin&c=show_edit_category&intCategory="+ intCategory,""," width=600,height=400,scrollbars=1");//$data['weekly']
}

 function Reload() {  
    window.location.reload();  
 }  

</SCRIPT>

<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/admin/shipping.js"></script>


<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/custom/gallery.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#example').dataTable( {
		"sPaginationType": "full_numbers"
	});
	
});
</script>
<div class="breadcrumbs">
    	<a href="<?php echo $CONF['url_app'];?>?m=admin">Dashboard</a> Manage Brand</div><!-- breadcrumbs -->
	
    <div class="left">
   	
<h1 class="pageTitle">Manage Brand</h1>
<?php
	$nErorr = count($error);
	if($nErorr > 0){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	<?php
		foreach($error as $k => $errMsg ){
		?>
			<p><?php echo $errMsg;?></p>
		
			<?php
		}
	
	?>
 	 <a class="close"></a>
 </div>
<?php
}
?>
<?php
	//$nErorr = count($error);
	if($Q->req['msg']==1){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('New Brand has been sucessfully added.');
location.href = "?m=admin&c=show_brand";
</script>
 <div class="notification msgsuccess" id="messageBox">
	New <span class="breadcrumbs">Manage Brand</span> has been sucessfully added.
 	 <a class="close"></a> </div>
<?php
}
	if($Q->req['msg']==2){
		?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('Your Brand has been sucessfully edited.');
location.href = "?m=admin&c=show_brand";
</script>
 <div class="notification msgsuccess" id="messageBox">
	 Shipping setting has been sucessfully edited.
 	 <a class="close"></a>
 </div>
<?php
		
		
		}
if($Q->req['errMsg']==1){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	Manage Brand cannot be empty.
 	 <a class="close"></a> </div>
	
	<?php
	}
	if($Q->req['errMsg']==2){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
Duplicate Brand name .
 	 <a class="close"></a>
 </div>
	
	<?php
	}

?>   

       <?php
       if($Q->req['type']==1){
       	?>
         <form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_update_brand" id="formshipping" enctype="multipart/form-data">
         <input type="hidden" value="<?php echo $data['brandedit']['intBrandId'];?>" name="id" />
      
        	
        	<div class="form_default">
        
        		<fieldset>
                 		<h1 class="pageTitle">Edit Brand</h1>   
          <p>Brand Name &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="strBrandName"  value="<?php echo $data['brandedit']['strBrandName'];?>" class="sf" /></p>
      
                     <p>Brand Order &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="intBrandOrder"  value="<?php echo $data['brandedit']['intOrder'];?>" class="sf" /></p>
        
                     
                <p>Brand Image &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                <input type="file" name="strBrandImage"  class="sf" /></p>
                
                
                
                
                
                
                <p>Description</p>
                <textarea cols="40" rows="5" name="strDescription"  ;?><?php echo $data['brandedit']['strDescription'];?></textarea>
               
               
               <p>Long Description</p>
                
                 <textarea cols="15" id="editor1" name="editor1" rows="5"><?php echo $data['brandedit']['strLongDescription'];?></textarea>
			<script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'editor1',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
        filebrowserBrowseUrl : '/techdata/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			</script>
               
               
               
               
               
               
               
               
               
                <p> Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<select name="intStatus"  class="sf" />
               	        
                       		<?php
                       		foreach($CODE['type_banner_status'] as $k => $valueTo){
                       			?>
                       		<option value="<?php echo $k;?>" <?php if($k==$data['brandedit']['intStatus']){ echo "selected";}?> ><?php echo $valueTo; ?></option>
                       		<?php
                       			}
                       		
                       		?>
                       		
                       		</select>
                </p>
<br>
                       	
                       		<button>Save</button>
                       	</p>
              
                    
                      	</p>
                  
                    	
        		</fieldset>
        		
        	</div>
        </form>
       	
       	
       	
       	<?php
       	
       	
       	}
       
       else{
       ?>
        <form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_add_brand" id="formshipping" enctype="multipart/form-data">
      
        	
        	<div class="form_default">
        
        		<fieldset>
                 		<h1 class="pageTitle">New Brand</h1>   
          <p>Brand Name &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="strBrandName"  value="<?php echo $Q->req['strBrandName'];?>" class="sf" /></p>
      
                  <p>Brand Order &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="intBrandOrder"  value="<?php echo $Q->req['intBrandOrder'];?>" class="sf" /></p>
        
                     
                <p>Brand Image &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                <input type="file" name="strBrandImage"  class="sf" /></p>
                <p>Description</p>
                <textarea cols="40" rows="5" name="strDescription"> <?php echo $Q->req['strDescription'];?></textarea>
                
                
                
                
                
                
                
                  <p>Long Description</p>
                
                 <textarea cols="15" id="editor1" name="editor1" rows="5"></textarea>
			<script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'editor1',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
        filebrowserBrowseUrl : '/techdata/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			</script>
                
                
                
                <p> Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<select name="intStatus" value="<?php echo $Q->req['intStatus'];?>" class="sf" />
               	        
                       		<?php
                       		foreach($CODE['type_banner_status'] as $k => $valueTo){
                       			?>
                       		<option value="<?php echo $k;?>"><?php echo $valueTo; ?></option>
                       		<?php
                       			}
                       		
                       		?>
                       		
                       		</select>
                </p>
<br>
                       	
                       		<button>Add</button>
                       	</p>
              
                    
                      	</p>
                  
                    	
        		</fieldset>
        		
        	</div>
        </form>
       <?php
      }
       ?> 
        
        
        
      
        <br />
     <div class="sTableWrapper">
    
    	
             <table cellpadding="0" cellspacing="0" border="0" class="dyntable" id="example">
             
            <thead>
            	
          
            	
                <tr>
                    <th width="24" class="head0">No.</th>
                     <th width="100" class="head1">Brand Name</th>
                     <th width="36" class="head0">Order</th>
                  <th width="146" class="head1">Brand Image</th>
                  <th width="110" class="head0">Description</th>
                  <th width="48" class="head1">Status</th>
                     <th width="88" class="head0">Create Date</th>
                    <th width="48" class="head1">Action</th>
                </tr>
            </thead>
            <colgroup>
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
            	<col class="con0" />
            	<col class="con1" />
            	<col class="con0" />
            </colgroup>
            <tbody>
             <?php
             $cData = count($data['brand']);
             $index = 1;
             if($cData > 0){
             	foreach($data['brand'] as $key => $value){
             ?>
             
                <tr>
                    <td class="con0"><?php echo $index;?></td>
                     <td class="con1"><?php echo $value['strBrandName'];?></td>
                      <td class="con0"><?php echo $value['intOrder'];?></td>
                    <td class="con1"><span class="listview">
               <?php
                     	$folder_id = (int)($value['intBrandId']/ $CONF['const_photo_per_dir'    ]);  
                     $dirpoto = $CONF['dir_photo'].'/brandphoto/'.$folder_id.'/'.$value['intBrandId'].'/'.$value['strBrandImage']; 
                     	if(file_exists($dirpoto)){
                     		$image 		= $CONF['http_photo'].'/brandphoto/'.$folder_id.'/'.$value['intBrandId'].'/'.$value['strBrandImage']; 
                     		$imageBig = $CONF['http_photo'].'/brandphoto/'.$folder_id.'/'.$value['intBrandId'].'/'.$value['strBrandImage']; 
                     		}
                   
                     	?>
             </span><span class="listview"><a href="<?php echo $imageBig;?>" class="view"><img src="<?php echo $image;?>" alt="<?php echo $value['strBrandImage'];?>" width=46 /></a></span></td>
                      
                     <td align="center">
                   
                     	<?php echo $value['strDescription'];?>               	  </td>
                     	<td class="center con0"><?php echo $CODE['type_banner_status'][$value['intStatus']];?></td>
            <td class="con1"><span class="center con1"><?php echo $value['CreateDate'];?></span></td>
                    
                    
                     <td class="center con0"><a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_brand&type=1&id=<?php echo $key;?>">Edit</a> </td>
                </tr>
             <?php
             $index++;
             }
            }
            else{
            ?><tr><td colspan="9"></td></tr><?php	
            	
            }
             
             ?>  
            </tbody>
            <tfoot>
              <tr>
                    <th width="24" class="head0">No.</th>
                     <th width="100" class="head1">Brand Name</th>
                     <th width="36" class="head0">Order</th>
                  <th width="146" class="head1">Brand Image</th>
                  <th width="110" class="head0">Description</th>
                  <th width="48" class="head1">Status</th>
                     <th width="88" class="head0">Create Date</th>
                    <th width="48" class="head1">Action</th>
                </tr>
            </tfoot>
        </table>
            
      </div><!--sTableWrapper-->
   </div>

</div>
 </div>
    
    <br clear="all" />