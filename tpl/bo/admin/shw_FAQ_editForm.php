<?php
global $CONF,$CODE,$error,$Q;
$URL = HTTP_SERVER.'ckeditor/';

$error = json_decode($Q->req['error']);



?>
<script type="text/javascript" src="<?php  echo $URL;?>/ckeditor.js"></script>
<script>
function fn_close_refresh(){
  parent.jQuery.fancybox.close()
  parent.location.reload(true);
                                 
}
</script>

<?php
if($Q->req['success']==1){
	?>
	<script>
    alert('You have successfully to update.');
    </script>
	
	<?php
	}

?>

	<!-- basic styles -->
		<link href="<?php echo HTTP_SERVER;?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo HTTP_SERVER;?>assets/css/font-awesome.min.css" />
        <link rel="shortcut icon" href="<?php echo HTTP_SERVER;?>/design/<?php echo $CONF['tpl_name'] ;?>/images/cfoni.ico" type="image/x-icon" >

 <?php
 if($Q->req['view']==1){
	 ?>
	 <div style="padding-left:20px;"> <h1>View Description</h1>
	 <hr />
	  <?php echo html_entity_decode($data['strDescription']);?>
	 </div>
    <br />
     <div style="padding-left:20px;"> <h1>View Features</h1>
	 <hr />
	  <?php echo html_entity_decode($data['strFeature']);?>
	 </div>
	 <?php
 }
 else{
	 ?>
												<div class="table-header" style="padding-left:20px;"><h1>
												
													Edit FAQ Record </h1>
												</div>
											</div>
                                          
                                          <div id="simple-msg" style="padding-left:20px;">
                                          
                                          
                                          
                                          
                                          
                                            <?php
                                         $cError = count($error);
										 if($cError > 0){
											 ?>
											   <b><font color="#FF0000">Error(s) Messages : <br /> ----------------------------------------------<br />
											 
											 <?php
											foreach($error as $k => $v){
												 echo $v."<br>";
												 
												
											}
											?>
                                          
                                             ----------------------------------------------
                                               </font></b>
											<?php
											
											
										 }
										 
										?>
                                          
                                          </div>
                                         
                                           <form name="ajaxform" id="ajaxform" action="<?php echo $CONF['url_app'];?>?m=admin&c=do_edit_FAQ" method="post" enctype="multipart/form-data">
                                           <input type="hidden" name="id" value="<?php echo $data['intFaqId'];?>" />
											<div class="modal-body no-padding">
												<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
													<tbody>
                                                   
                                                       <tr>
                                                        <td>FAQ Type </td>
                                                        <td> 
                                                        
                                                        <select name="intType">
                                            <?php
                                            foreach($CODE['faq_type'] as $k => $v){
												
												?><option value="<?php echo $k;?>" <?php if($data['intType']==$k){ echo "selected";}?>><?php echo $v;?></option><?php
												
											}
											
											?>
                                            </select>
                                                        </td>
												       </tr>
                                                       <tr>
                                                        <td>Order</td>
                                                        <td><input type="text" value="<?php echo $data['intOrder'] ?>" name="intOrder" /></td>
												       </tr>
                                                      
                                                       <tr>
                                                        <td>Status</td>
                                                        <td>
                                                        <select name="intStatus">
                                                         <?php
                                                         foreach($CODE['status'] as $kstatus => $vstatus){
															?>
															<option value="<?php echo$kstatus ?>" <?php if($kstatus==$data['intStatus']){ echo "selected";} ?> ><?php echo $vstatus; ?></option>
															
															<?php 
														  }
														 
														 ?>
                                                        </select>
                                                        </td>
												       </tr>
                                                      
                                                       <tr>
                                                        <td colspan="2"><b>Question</b></td>
                                                        </tr>
                                                        <tr>
                                                        <td colspan="2">
                                                        
                                                        <textarea cols="15" id="editor1" name="editor1" rows="5"><?php echo html_entity_decode($data['strQ']);?></textarea>
			<script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'editor1',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
        filebrowserBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			</script>
                                                        
                                                        
                                                        </td>
                                                       </tr>
                                                       
                                                        <tr>
                                                        <td colspan="2"><b>Answer</b></td>
                                                        </tr>
                                                        <tr>
                                                        <td colspan="2">
                                                        
                                                        <textarea cols="15" id="editor2" name="editor2" rows="5"><?php echo html_entity_decode($data['strA']);?></textarea>
			<script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'editor2',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
        filebrowserBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			</script>
                                                        
                                                        
                                                        </td>
                                                       </tr>
                                                       
													</tbody>
												</table>
											</div>
										  
											<div class="modal-footer no-margin-top">
												<button class="btn btn-sm btn-danger pull-left" id="simple-post" type="submit">
												
													Update
												</button>
                                               <button class="btn btn-sm btn-danger pull-left" onClick="javascript:fn_close_refresh();">												
													Close & Refresh
												</button>
 											</div>
                                            
                                             </form>
 <?php
  }
 ?>
<script>
$(document).ready(function()
{
	
$("#simple-post").click(function()
{
	$("#ajaxform").submit(function(e)
	{
		$("#simple-msg").html("<img src='loading.gif'/>");
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
		$.ajax(
		{
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data, textStatus, jqXHR) 
			{
				$("#simple-msg").html('<pre><code class="prettyprint">'+data+'</code></pre>');

			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				$("#simple-msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus='+textStatus+', errorThrown='+errorThrown+'</code></pre>');
			}
		});
	    e.preventDefault();	//STOP default action
	    e.unbind();
	});
		
	$("#ajaxform").submit(); //SUBMIT FORM
});

});
</script>
								
