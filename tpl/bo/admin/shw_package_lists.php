<?php
global $CONF,$CODE,$Q;

$url_add_account_manager = $CONF['url_app'].'?m=admin&c=show_package_form';

if($Q->req['msg']==2){
	?>
	<script>
	alert('You have sucessfully deleted 1 Package Record');
	location.href='?m=admin&c=show_package';
	</script>
	
	<?php
}


?>
<script>
function doDelete(id){
	if (confirm("Are you sure want to delete this package?")) {
        // your deletion code
		location.href='?m=admin&c=do_delete_package&id=' + id
    }
    return false;
}
</script>
			<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app']?>?m=admin&c=show_main">Home</a>
							</li>
							<li class="active">Package Listings</li>
						</ul><!-- .breadcrumb -->

						<!--<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
                                    By 
                                    <select name="intSearchType">
                                    <option value="1">Full Name / Login Name</option>
                                    <option value="2">Email</option>
                                    
                                    </select>
								</span>
							</form>
						</div>-->
								<div>
									<div class="col-xs-12">
										<h2>Package Listings</h2>
                                        
                                        <div class="hr hr-18 dotted hr-double"></div>
                                      
                                        
                                         <a href="<?php echo $url_add_account_manager;?>" class="btn btn-app btn-light btn-xs">
												<i class="icon-edit bigger-160"></i>
												NEW
											</a>
                                       
                                        <div class="hr hr-18 dotted hr-double"></div>
                                     	<div class="table-header">
											Package Listings
										</div>
                                      
										<div class="table-responsive">
											<table  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
                                                         No.
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
														</th>
														<th>Package Name </th>
                                                        <th class="hidden-480">Image</th>
														<th class="hidden-480">Price</th>
														<th class="hidden-480">Order</th>

														<th>
															<i class="icon-time bigger-110 hidden-480"></i>
															Date Created
														</th>
														<th class="hidden-480">Status</th>

														<th>Action(s)</th>
													</tr>
												</thead>

												<tbody>
													<?php
													$cArr = count($data['package']);
													
                                                    if($Q->req['page']=="" || $Q->req['page']==1){
														$index = 1;
													}
													else{
														$index =  ($Q->req['page']*30) - 29;
														
													}
													
													 if($cArr > 0){
													  foreach($data['package'] as $k => $value){
													
													?>
                                                    
                                                    <tr>
														<td class="center">
															<!--<label>
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>-->
                                                          <?php
                                                            echo $index;
															?>
                                                          
														</td>

														<td>
															 <a class="fancybox fancybox.iframe" href="?m=admin&c=show_package_details&id=<?php echo $value['intPackageId'];?>&view=1"> <?php echo $value['strPackageName'];?> </a>
                                                             <br />
                                                             
														</td>
														
                                                        <td class="hidden-480">
														<?php 
														$folder_id = (int)($value['intPackageId']/ $CONF['const_photo_per_dir'    ]);  
                    									$dirpoto = $CONF['dir_photo'].'/package/'.$folder_id.'/'.$value['intPackageId'].'/'.$value['strImage']; 
                     									if(file_exists($dirpoto)){
                     										$image 		= $CONF['http_photo'].'/package/'.$folder_id.'/'.$value['intPackageId'].'/'.$value['strImage']; 
                     										?>
															<a  class="fancybox" href="<?php echo $image?>"><img src="<?php echo $image?>"  width="30"/></a>
															
															<?php
                     									}
														else{
															echo " No Image";
															}
														
														?>
                                                        
                                                        </td>
                                                       	<td class="hidden-480"> RM <?php echo $value['strPrice']?></td>
                                                        
                                                        
														<td class="hidden-480"><?php echo $value['intOrder'];?></td>
														<td><?php echo $value['CreateDate'];?></td>

														<td class="hidden-480">
											                <?php
                                                            if($value['intStatus']==1){
																?>
																 <span class="label label-sm label-success">Active</span>
                                                            
																<?php
															}
															elseif($value['intStatus']==2){
																?>
																<span class="label label-sm label-inverse arrowed-in">Pending</span>
																<?php
																
															}
															elseif($value['intStatus']==3){
																?>
																<span class="label label-sm label-info arrowed arrowed-righ">Suspend</span>
																
																<?php
																
															}
															elseif($value['intStatus']==4){
																?>
																<span class="label label-sm label-warning">
                                                                Inactive
                                                               </span>
																<?php
																
															}
															?>
                                                            
                               
														</td>

														<td>
															<div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
																<a class="fancybox fancybox.iframe" href="?m=admin&c=show_package_details&id=<?php echo $value['intPackageId'];?>">
																	<i class="icon-pencil bigger-130"></i>
																</a>
                                            					<a class="red" href="#" onClick="javascript:doDelete('<?php echo encrypt($value['intPackageId'],'cfoni.8888');?>')">
																	<i class="icon-trash bigger-130"></i>
																</a>
															</div>

															</td>	
                                                          </tr>
                                                       <?php
													     $index++;
														 } 
                                                       }
													   else{
														 ?>
														<tr>
                                                        <td colspan="7" align="center"> No data at the moment...</td>
                                                        </tr> 
														 <?php  
													  }
													   
													   ?>
													</tbody>
												</table>
											</div>

											<div>
                                            
                                            
                                            
                                            <ul class="pagination pull-left no-margin">
														<?php echo $this->pages->display_pages(); ?>
												</ul>
                                            </div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			
	      
