<?php
global $CONF,$CODE;

$url_product_listing = $CONF['url_app'].'?m=admin&c=show_package';
$URL = HTTP_SERVER.'ckeditor/';


?>
<script type="text/javascript" src="<?php  echo $URL;?>/ckeditor.js"></script>
<?php
if($Q->req['msg']==1){
	?>
	<script>
	alert('You have sucessfully added 1 Package');
	location.href='?m=admin&c=show_package_form';
	</script>
	
	<?php
}

?>

					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app'];?>?m=admin">Home</a>
							</li>

							
						     <li>
								<a href="<?php echo $url_product_listing;?>">Package Listings</a>
							</li>
							<li class="active">Add New Package</li>
						</ul><!-- .breadcrumb -->
                      

						<!-- #nav-search -->
					</div>

<div class="page-content">
						<div class="page-header">
							<h1>
								Add New Package
								
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
									<div class="row-fluid">
									<div class="span12">
										<div class="widget-box">
											<div class="widget-header widget-header-blue widget-header-flat">
												<h3 class="lighter block green">Enter the package information </h3>
						                  </div>

											<div class="widget-body">
												<div class="widget-main">
												
										<div class="row-fluid position-relative" >
										
                                        <?php
                                         $cError = count($error);
										 if($cError > 0){
											 ?>
											   <b><font color="#FF0000">Error(s) Messages : <br /> ----------------------------------------------<br />
											 
											 <?php
											foreach($error as $k => $v){
												 echo $v."<br>";
												 
												
											}
											?>
                                          
                                             ----------------------------------------------
                                               </font></b>
											<?php
											
											
										 }
										 
										?>
                                      <form class="form-horizontal" role="form" action="<?php echo $CONF['url_app']?>?m=admin&c=do_add_package" method="post" enctype="multipart/form-data">
								
                                       <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Package Name </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="Package Name" value="<?php echo $Q->req['strPackageName'];?>" name="strPackageName" class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                    
                                    
                                       <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1" > Default Image </label>

										<div class="col-sm-9">
											<input type="file" id="form-field-1" placeholder="" name="strImage"  class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                     <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Price : 	RM  </label>

										<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="0.00" value="<?php echo $Q->req['strPrice'];?>" name="strPrice" class="col-xs-10 col-sm-5" />
										</div>
									</div>       
                              <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Description </label>

										<div class="col-sm-9">
									   
                                     <textarea cols="15" id="strDesc" name="strDesc" rows="5"><?php echo $Q->req['strDesc'];?></textarea>
			<script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'strDesc',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
        filebrowserBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			</script>
                                     
                     </div>
									</div>  
                                    
                                    
                                            
                                     
                            <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Features </label>

										<div class="col-sm-9">
									   
                                     <textarea cols="15" id="strFeatures" name="strFeatures" rows="5"><?php echo $Q->req['strFeatures'];?></textarea>
			<script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'strFeatures',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
        filebrowserBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/<?php echo FOLDER;?>ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/<?php echo FOLDER;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			</script>
                                     
                     </div>
									</div>                
                                     
                                     
                                     
                                     
                                    
                                    <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Order </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="" name="intOrder" value="<?php echo $Q->req['intOrder'];?>" class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Status </label>

										<div class="col-sm-9">
										 <select name="intStatus">
                                         <?php
                                         foreach($CODE['status_add'] as $k_status => $v_status){
											 ?>
											 <option value="<?php echo $k_status?>"><?php echo $v_status;?></option>
											 
											 <?php
											 }
										 ?>
                                         </select>
										</div>
									</div>


													<div class="clearfix form-actions">
										           <div class="col-md-offset-3 col-md-9">
											      <button class="btn btn-info" type="submit">
												     <i class="icon-ok bigger-110"></i>
												    Submit
											     </button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												Reset
											</button>
										</div>
                                         </div>
                                        </form>
                                       
									</div>
												</div><!-- /widget-main -->
											</div><!-- /widget-body -->
										</div>
									</div>
								</div>

								
						  </div>
  </div>
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			