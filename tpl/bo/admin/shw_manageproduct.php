<?php
global $CONF,$Q,$CODE,$LANG;
$URL = HTTP_SERVER.'ckeditor/';
$arrStatus = array('Y'=>'Active','N'=>'Inactive');
$arrType  = array('1'=>'Item','2'=>'Weight');

?>


<SCRIPT language="javascript">

function editsize(intProductId,x,y){
 window.open("?m=admin&c=show_category_form",""," width=400,height=300,scrollbars=1");//$data['weekly']

}

function editstatus(intCategory,x,y){
 window.open("?m=admin&c=show_edit_category&intCategory="+ intCategory,""," width=600,height=400,scrollbars=1");//$data['weekly']
}

 function Reload() {  
    window.location.reload();  
 }  

</SCRIPT><head>
	<title>Full Page Editing with Document Properties Plugin &mdash; CKEditor Sample</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
	<script type="text/javascript" src="<?php  echo $URL;?>/ckeditor.js"></script>
	<script src="<?php  echo $URL;?>sample.js" type="text/javascript"></script>
	<link href="<?php  echo $URL;?>sample.css" rel="stylesheet" type="text/css" />
</head>

<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/admin/shipping.js"></script>


<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/custom/gallery.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#example').dataTable( {
		"sPaginationType": "full_numbers"
	});
	
});
</script>

<div class="breadcrumbs">
    	<a href="<?php echo $CONF['url_app'];?>?m=admin">Dashboard</a>
        <span>Manage Product</span>
    </div><!-- breadcrumbs -->
	
    <div class="left">
   	
<h1 class="pageTitle">Manage Product</h1>
<?php
	$nErorr = count($error);
	if($nErorr > 0){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	<?php
		foreach($error as $k => $errMsg ){
		?>
			<p><?php echo $errMsg;?></p>
		
			<?php
		}
	
	?>
 	 <a class="close"></a>
 </div>
<?php
}
?>
<?php
	//$nErorr = count($error);
	if($Q->req['msg']==1){
?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('New Manage Product has been sucessfully added.');
location.href = "?m=admin&c=show_manageproduct";
</script>
 <div class="notification msgsuccess" id="messageBox">
	New Manage Product has been sucessfully added.
 	 <a class="close"></a> </div>
<?php
}
	if($Q->req['msg']==2){
		?>
<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
alert('Your Manage Product has been sucessfully edited.');
location.href = "?m=admin&c=show_manageproduct";
</script>
 <div class="notification msgsuccess" id="messageBox">
	 Shipping setting has been sucessfully edited.
 	 <a class="close"></a>
 </div>
<?php
		
		
		}
if($Q->req['errMsg']==1){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
	Product Name cannot be empty.
 	 <a class="close"></a> </div>
	
	<?php
	}
	if($Q->req['errMsg']==2){
	?>
	<script src="<?php echo HTTP_SERVER;?>js/admin/jquery_ms.min.js" type="text/javascript"></script> 
<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgerror" id="messageBox">
Duplicate Product name .
 	 <a class="close"></a>
 </div>
	
	<?php
	}

?>   

       <?php
       if($Q->req['type']==1){
       	?>
         <form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_update_manageproduct" id="formshipping" enctype="multipart/form-data">
          <input type="hidden" value="<?php echo $data['manageproductedit']['intProductId'];?>" name="id" />
      
        	<script src="jquery1.min.js" type="text/javascript"></script>   
        	
        	<div class="form_default">
        
        		<fieldset>
                 		<h1 class="pageTitle">Edit  Product</h1>
                 		<p>Product Name&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="text" name="strProductName"  value="<?php echo $data['manageproductedit']['strProductName'];?>" class="sf" />
               		  </p>
                 		<p>Brand &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <select name="intBrandId"  class="sf" />
                       		<?php
                       		foreach( $data['brand'] as $kB => $valueTo){
                       			?><option value="<?php echo $kB;?>" <?php if($kB==$data['manageproductedit']['intBrandId']){ echo "selected";}?> ><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                   		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                 		<p>Category &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <select name="intCategoryId"  id="category1"  class="sf" />
                       		<?php
                       		foreach($data['category'] as $kC => $valueTo){
                       			?><option value="<?php echo $kC;?>" <?php if($kC==$data['manageproductedit']['intCategoryId']){ echo "selected";}?> ><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                   		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                 		<p>Subcategory &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 		  <select name="intSubcategoryId"  id="subcategory1" class="sf" />
                       		<?php
                       		
							
				 
				 foreach($data['subcategory'] as $category => $v){
				 	foreach($v as $vules => $subCategory){
				 	?><option class="<?php echo $category ;?>" <?php if($data['manageproductedit']['intSubcategoryId']==$vules){ echo "selected";}?> value="<?php echo $vules?>"><?php echo $subCategory;?></option><?php
				 	}
				 }
			
						
                       		 
                       		?>
               
                       		
                       		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</select>
                 		</p>
                 
                 
                <p>URL As Information ?   &nbsp;&nbsp;&nbsp;&nbsp;   <select id="first" name="strURLStatus">

<?php
 foreach($CODE['url_status'] as $key => $optionname){
 	?>
 	<option value="<?php echo $key."::".$data['manageproductedit']['strURL']; ?>" <?php if($data['manageproductedit']['strURLStatus']==$key){ echo "selected";}?>><?php echo ucwords($optionname);?></option>
 	<?php
 	}
?>




</select>
   
   </p>
   <p><?php
   	if($data['manageproductedit']['strURLStatus']=="Y"){
   		?>
   		<p><b id="add">
   		 URL (http://www.aa.com)
                            <input type="text" id="first2" name="strURL"  value="<?php echo $data['manageproductedit']['strURL'];?>" class="sf" />
               		 
   		</b>
   	</p>
   		
   		<?php
   }
	else{
		?>
		<b id="add"></b>
		
		<?php
		
	}
   	?>    
 <script src="<?php echo HTTP_SERVER;?>ajaxColor/chainEditProduct.js"></script>
                       
                       
      </p>   
                 
                 
                 
                 
                 		<p>Product Photo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 		  <input type="file" name="strPhoto"  class="sf" />
               		  </p>
                 		<p style="float:left;">Small Description</p>
   		        <p style="float:left;"><textarea cols="40" rows="5" name="strSmallDescription"> <?php echo $data['manageproductedit']['strSmallDescription'];?>
           		        </textarea>
                </p>

<p style="float:left; "></p>
<p style="float:left;">Description                        
<textarea cols="40" id="editor1" name="editor1" rows="5"><?php echo html_entity_decode($data['manageproductedit']['strDescription']);?></textarea>
                        <script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'editor1',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
       filebrowserBrowseUrl : '/techdata/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			            </script>
                      </p>
                <p style="float:left;">  Status  
                  <select name="intStatus"  class="sf" />
                       		<?php
                       		foreach($CODE['type_banner_status'] as $k => $valueTo){
                       			?><option value="<?php echo $k;?>" <?php if($k==$data['manageproductedit']['intStatus']){ echo "selected";}?> ><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                       		</select></p>
<br>
                       	<p style="float:left;">
                   		  <button>Save</button> </p>
</p>
              
                    
                      	</p>
                  
                    	
        		</fieldset>
        		
        	</div>
        <script src="jquery.chained.mini.js" type="text/javascript" charset="utf-8"></script>
           	<script type="text/javascript" charset="utf-8">
          $(function(){
            $("#subcategory1").chained("#category1"); 
         
          });
          </script>  	
        	
        	
        </form>
       	
       	
       	
       	<?php
       	
       	
       	}
       
       else{
       ?>
       
       
  
       
        <form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_add_manageproduct" id="formshipping" enctype="multipart/form-data">
    <script src="jquery1.min.js" type="text/javascript"></script>   
        	
        	<div class="form_default">
        
        		<fieldset>
                 		<h1 class="pageTitle">New  Product</h1>
                 		<p>Product Name&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="text" name="strProductName"  value="<?php echo $Q->req['strProductName'];?>" class="sf" />
               		  </p>
                 		<p>Brand &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <select name="intBrandId"  >
                       		<?php foreach( $data['brand'] as $k => $sectionname){?><option value="<?php echo $k;?>"><?php echo $sectionname;?></option><?php }?>
                       		
                   		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                 		<p>Category &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <select name="intCategoryId"  id="category"   class="span2"  />
                       		<?php foreach($data['category'] as $k => $sectionname){?><option value="<?php echo $k;?>"><?php echo $sectionname;?></option><?php }?>
                       		
                   		</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                 		<p>Subcategory &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 		  <select name="intSubcategoryId"  id="subcategory"  class="span2" />
                       		
                            
                            <?php
				 
				 foreach($data['subcategory'] as $category => $v){
				 	foreach($v as $vules => $subCategory){
				 	?><option class="<?php echo $category ;?>" <?php if($Q->req['intSubcategoryId']==$vules){ echo "selected";}?> value="<?php echo $vules?>"><?php echo $subCategory;?></option><?php
				 	}
				 }
				?>
                            
                        
                            
                       		</select>
                 		</p>
                        
                        
      
                        
           	<script src="jquery.chained.mini.js" type="text/javascript" charset="utf-8"></script>
           	<script type="text/javascript" charset="utf-8">
          $(function(){
            $("#subcategory").chained("#category"); 
         
          });
          </script>                
                        
   <p>URL As Information ?   &nbsp;&nbsp;&nbsp;&nbsp;   <select id="first" name="strURLStatus">

<?php
 foreach($CODE['url_status'] as $key => $optionname){
 	?>
 	<option value="<?php echo $key ?>"><?php echo ucwords($optionname);?></option>
 	<?php
 	}
?>




</select>
   
   </p>
   <p><b id="add">   </b>
 <script src="<?php echo HTTP_SERVER;?>ajaxColor/chainProduct.js"></script>
                       
                       
      </p>   
      <p>Product Photo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 		  <input type="file" name="strPhoto"  class="sf" />
               		  </p>
                 		<p style="float:left;">Small Description</p>
   		        <p style="float:left;"><textarea cols="40" rows="5" name="strSmallDescription"> <?php echo $Q->req['strSmallDescription'];?>
           		        </textarea>
                </p>


<p style="float:left;">Description                        
<textarea cols="40" id="editor1" name="editor1" rows="5"></textarea>
                        <script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'editor1',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
       filebrowserBrowseUrl : '/techdata/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			            </script>
                      </p>
                <p style="float:left;">  Status  
                  <select name="intStatus" value="<?php echo $Q->req['intStatus'];?>" class="sf" />
                       		<?php
                       		foreach($CODE['type_banner_status_nodelete'] as $k => $valueTo){
                       			?><option value="<?php echo $k;?>"><?php echo $valueTo; ?></option><?php
                       			}
                       		
                       		?>
                       		
                       		</select></p>
		        
                 		
<br>
                       	<p style="float:left;">
                   		  <button>Add</button> </p>
</p>
              
                    
                      	</p>
                  
                    	
        		</fieldset>
        		
        	</div>
        </form>
       <?php
      }
       ?> 
        
        
        
      
        <br />
     
        
        
     <div class="sTableWrapper">
    
    	
              <table cellpadding="0" cellspacing="0" border="0" class="dyntable" id="example">
             
            <thead>
            	
          
            	
                <tr>
                    <th class="head0">No.</th>
                     <th class="head1">Product Name</th>
                     <th class="head0">Brand</th>
                     <th class="head1">Category</th>
                     <th class="head0">Subcategory</th>
                    <th class="head1">Product Photo</th>
                    <th class="head0">Small Description</th>
                    <th class="head1">Description</th>
                    <th class="head0">Status</th>
                     <th class="head1">Create Date</th>
                    <th class="head0">Action</th>
                </tr>
            </thead>
            <colgroup>
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
            	<col class="con0" />
            	<col class="con1" />
            	<col class="con0" />
            	<col class="con1" />
                <col class="con0" />
            	<col class="con1" />
                <col class="con0" />
            	
            </colgroup>
            <tbody>
             <?php
             $cData = count($data['product']);
             $index = 1;
             if($cData > 0){
             	foreach($data['product'] as $key => $value){
             ?>
             
                <tr>
                    <td class="con0"><a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_manageproduct&type=1&id=<?php echo $key;?>"><?php echo $index;?></a></td>
                     <td class="con1"><?php echo $value['strProductName'];?></td>
                     <td class="con1"><?php echo $data['brand'][$value['intBrandId']];?></td>
                     <td class="con1"><?php echo $data['category'][$value['intCategoryId']];?></td>
                     <td class="con1"><?php echo  $data['subcategory'][$value['intCategoryId']][$value['intSubcategoryId']];?></td>
                    
                        
             
             <td class="con0"><span class="con1">
                      <div id="listview" class="listview">
                    
                    
                      <?php
                     	$folder_id = (int)($value['intProductId']/ $CONF['const_photo_per_dir'    ]);  
                     $dirpoto = $CONF['dir_photo'].'/manageproductphoto/'.$folder_id.'/'.$value['intProductId'].'/'.$value['strPhoto']; 
                     	if(file_exists($dirpoto)){
                     		$image 		= $CONF['http_photo'].'/manageproductphoto/'.$folder_id.'/'.$value['intProductId'].'/'.$value['strPhoto']; 
                     		$imageBig = $CONF['http_photo'].'/manageproductphoto/'.$folder_id.'/'.$value['intProductId'].'/'.$value['strPhoto']; 
                     	?>  
                        
                    
                        
                         <a href="<?php echo $imageBig;?>" class="view"><img src="<?php echo $image;?>" alt="<?php echo $value['strPhoto'];?>" width=46 /></a></div></span><?php
                     	
                     	
                     		}
                     		else{
                     			echo " - ";
                     			
                     			}
                   
                     	?>
               
         </td>
                      
              <td align="center"><?php echo $value['strSmallDescription'];?> </td>
                     	<td class="con0" valign="top" width="25%"><?php echo html_entity_decode($value['strDescription']);?></td>
                    <td class="con1"> 
                   	    <div id="listview" class="listview"><span class="con0"><span class="center con0"><?php echo $CODE['type_banner_status'][$value['intStatus']];?></span></span></td>
                    
                  <td class="center con0"><span class="center con1"><?php echo $value['CreateDate'];?></span></td>
                    
                     <td class="center con1"><a href="<?php echo $CONF['url_app'];?>?m=admin&c=show_manageproduct&type=1&id=<?php echo $key;?>">Edit</a> </td>
                </tr>
             <?php
             $index++;
             }
            }
            else{
            ?><tr><td colspan="9"></td></tr><?php	
            	
            }
             
             ?>  
            </tbody>
            <tfoot>
             <tr>
                    <th class="head0">No.</th>
                     <th class="head1">Product Name</th>
                    <th class="head0">Brand</th>
                     <th class="head1">Category</th>
                     <th class="head0">Subcategory</th>
                    <th class="head1">Product Photo</th>
                    <th class="head0">Small Description</th>
                    <th class="head1">Description</th>
                    <th class="head0">Status</th>
                     <th class="head1">Create Date</th>
                    <th class="head0">Action</th>
                </tr>
            </tfoot>
        </table>
            
      </div><!--sTableWrapper-->
   </div>

 </div>
 </div>
    
    <br clear="all" />