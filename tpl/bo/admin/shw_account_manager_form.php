<?php
global $CONF;

$url_account_manager_listing = $CONF['url_app'].'?m=admin&c=show_account_manager';

if($Q->req['msg']==1){
	?>
	<script>
	alert('You have sucessfully added 1 Account Manager');
	location.href='?m=admin&c=show_account_manager_form';
	</script>
	
	<?php
}

?>









					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app'];?>?m=admin">Home</a>
							</li>

							
						     <li>
								<a href="<?php echo $url_account_manager_listing;?>">Account Manager Listing</a>
							</li>
							<li class="active">Add New Account Manager</li>
						</ul><!-- .breadcrumb -->
                      

						<!-- #nav-search -->
					</div>

<div class="page-content">
						<div class="page-header">
							<h1>
								Add New Account Manager
								
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
									<div class="row-fluid">
									<div class="span12">
										<div class="widget-box">
											<div class="widget-header widget-header-blue widget-header-flat">
												<h3 class="lighter block green">Enter the following information </h3>
						                  </div>

											<div class="widget-body">
												<div class="widget-main">
												
										<div class="row-fluid position-relative" >
										
                                        <?php
                                         $cError = count($error);
										 if($cError > 0){
											 ?>
											   <b><font color="#FF0000">Error(s) Messages : <br /> ----------------------------------------------<br />
											 
											 <?php
											foreach($error as $k => $v){
												 echo $v."<br>";
												 
												
											}
											?>
                                          
                                             ----------------------------------------------
                                               </font></b>
											<?php
											
											
										 }
										 
										?>
                                        
                                        
                                        				
										<form class="form-horizontal" role="form" action="<?php echo $CONF['url_app']?>?m=admin&c=do_add_account_manager" method="post">
								
                                       <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Full Name ( as in IC / Passport No.) </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="Full Name" value="<?php echo $Q->req['strFullName'];?>" name="strFullName" class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                    
                                      <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email Address </label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1" placeholder="xxx@cfoni.com" name="strEmail" value="<?php echo $Q->req['strEmail'];?>" class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact No. </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="" name="strContact" value="<?php echo $Q->req['strContact'];?>" class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Username </label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1" placeholder="Username" value="<?php echo $Q->req['strUsername'];?>" name="strUsername" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Password </label>

										<div class="col-sm-9">
										  <input type="password" id="form-field-2" placeholder="Password" name="strPassword" class="col-xs-10 col-sm-5" />
											
										</div>
									</div>

									<div class="space-4"></div>

										<div class="form-group">
													<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Auto Send Login Details ? </label>
										            <label class="middle">
													&nbsp;&nbsp;<input class="ace" type="checkbox" value="1" name="intAutoEmail" id="id-disable-check" />
													<span class="lbl"></span>
												</label>
														
                                        
                                        </div>					
														
													<hr />
													<div class="clearfix form-actions">
										           <div class="col-md-offset-3 col-md-9">
											      <button class="btn btn-info" type="submit">
												     <i class="icon-ok bigger-110"></i>
												    Submit
											     </button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												Reset
											</button>
										</div>
                                         </div>
                                        </form>
                                       
									</div>
												</div><!-- /widget-main -->
											</div><!-- /widget-body -->
										</div>
									</div>
								</div>

								
						  </div>
  </div>
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			