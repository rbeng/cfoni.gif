<?php
global $CONF,$Q,$CODE;
include $CONF['dir_fck']."/fckeditor/fckeditor.php";
$date = date('d') + 1;
$endDate = date('m').'/'.$date.'/'.date('Y');
$URL = HTTP_SERVER.'ckeditor/';
$arrType = array('1'=>'Single','2'=>'Semi');
$arrStatus = array('1'=>'Active','2'=>'Inactive','3'=>'Sold Out');

?>
<script type="text/javascript" src="<?php  echo $URL;?>/ckeditor.js"></script>
<script src="<?php  echo $URL;?>sample.js" type="text/javascript"></script>
<link href="<?php  echo $URL;?>sample.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/plugins/jquery.validate.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo HTTP_SERVER;?>css/admin/tabcontent.css" />
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>js/tabcontent.js"></script>


<script>
	$(function() {
		$( "#datepicker" ).datepicker();
		$( "#datepicker2" ).datepicker();
	});
	</script>

<script type="text/javascript">
jQuery(document).ready(function(){
	
	//////////// FORM VALIDATION /////////////////
	jQuery("#form").validate({
		rules: {
			name: "required",
			email: {
				required: true,
				email: true,
			},
			itemtype: "required"
		},
		messages: {
			name: "Please enter your name",
			email: "Please enter a valid email address",
			itemtype: "Please select your item type"
		}
	});

});	
</script>


<?php
	if($Q->req['msgSingle']==1){
?>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 2500);
});
</script>
 <div class="notification msgsuccess" id="messageBox">
	1 product has been sucessfully added in.
 	 <a class="close"></a>
 </div>
<?php
}
?>
<div id="content">

<br>
 
 <div class="main">
 	
 	
 		<form method="post" action="<?php echo $CONF['url_app']?>?m=admin&c=do_add_product" enctype="multipart/form-data">
    	
   
  <h1 style="padding-left:30px">Add Product Details </h1>
    	<div class="form_default">
         <table>
         	<tr>
         		<td>
         			   <p>
                    	<label for="gender">Product Type</label>
                       <select name="intTypeid" class="sf" >
				<?php
				 foreach($CODE['Pro_Type']  as $typek => $typev){
				 
				 	?><option value="<?php echo $typek ?>"><?php echo $typev;?></option><?php
				 	
				 }
				?>
				
				
		</select>
                    </p>
                      
                    
                   <p>
                    	<label for="gender">Collection</label>
                       <select name="intCollectionid" class="sf" >
				<?php
				 foreach($data['collection'] as $collectionk => $collectionn){
				 
				 	?><option value="<?php echo $collectionk?>"><?php echo $collectionn['strCollectionName'];?></option><?php
				 	
				 }
				?>
				
				
		</select>
                    </p>
                    
                    <p>
                    	<label for="email">Category</label>
                      	<select name="intCate" id="category" class="span2 sf" > 
				<?php
				 foreach($data['arrCate'] as $k1 => $v1){
				 	?><option value="<?php echo $k1?>"><?php echo $v1;?></option><?php
				 	}
				
				?>
				
				
		</select>
                    </p>
                    
                    <p>
                    	<label for="location">Sub Categories</label>
                       <select name="intSubCate" id="subcategory" class="span2 sf">
				<?php
				 foreach($data['arrSubCate'] as $category => $v){
				 	foreach($v as $vules => $subCategory){
				 	?><option class="<?php echo $category ;?>" value="<?php echo $vules;?>"><?php echo $subCategory;?></option><?php
				 	}
				 }
				?>
				
				
		</select>
                    </p>
                    
                
                 
                   
                    <p>
                    	<label for="location">Product Name</label>
                        <input type="text" name="strProductName"  value="<?php echo $Q->req['strProductName'];?>" class="sf" />
                    </p>
                    
                    <p>
                    	<label for="location">Product Code</label>
                        <input type="text" name="strProductCode"  value="<?php echo $Q->req['strProductCode'];?>" class="sf" />
                    </p>
                      <p>
                    	<label for="location">SKU Code</label>
                        <input type="text" name="strSKU"  value="<?php echo $Q->req['strSKU'];?>" class="sf" />
                    </p>
                     <p>
                    	<label for="location">Weight</label>
                        <input type="text" name="strWeight"  value="<?php echo $Q->req['strWeight'];?>" class="sf" />
                    </p>  
                      <p >
         			
         		    	<label for="location">Normal Price</label>
                        <input type="text" name="strNPrice"  value="<?php echo $Q->req['strNPrice'];?>" class="sf" />
                    </p>
                      <p>
                    	<label for="location">Selling Price</label>
                        <input type="text" name="strSPrice"  value="<?php echo $Q->req['strSPrice'];?>" class="sf" />
                    </p>
                        <p>
         		   
         		   	<label for="location">VIP Price</label>
                        <input type="text" name="strVIPPrice"  value="<?php echo $Q->req['strVIPPrice'];?>" class="sf" />
                    </p>
                    
                     <p>
                    	<label for="location">Quantity</label>
                        <input type="text" name="intQuantity"  value="<?php echo $Q->req['intQuantity'];?>" class="sf" />
                    </p>  
                    
                    
                     <p>
                    	<label for="location">Redemption Product ?</label>
                       
                       
                       <div class="color">
     <div class="highlight">
<select id="first" name="intRedemption">

<?php
 foreach($CODE['type_promotion'] as $key => $optionname){
 	?>
 	<option value="<?php echo $key ?>"><?php echo ucwords($optionname);?></option>
 	<?php
 	}
?>




</select>
         <b id="add"></b>
     </div>



<script src="<?php echo HTTP_SERVER;?>ajaxColor/chainProduct.js"></script>
                       
                       
                       
                    </p> 
                    
                    
                    
                    
                    
                    
                      
                    <p class="padding15">
                    	<label>Date Start(M / D / YYYY)</label>
                        <input type="text" name="strStartDate" id="datepicker" value="<?php echo date('m/d/Y')?>">
                    </p>
                      <p>
                    	<label for="location">Date End (M / D / YYYY)</label>
                         
                        &nbsp;<input type="text" name="strEndDate" id="datepicker2" value="<?php  echo $endDate; ?>">
                    </p> 
                    
                    
                      <p>
                    	<label for="location">Default Image [Single People [.jpg]]</label>
                        <input type="file" name="strPic"   class="sf" />
                    </p> 
                    
                    
                    
                    
                      <p>
                    	<label for="location">Status</label>
                        <select name="intStatus"  class="span2 sf" > 
				<?php
				 foreach($CODE['Status_Pro'] as $kStatus => $vStatus){
				 	?><option value="<?php echo $kStatus;?>"><?php echo $vStatus;?></option><?php
				 	}
				
				?>
				
				
		</select>
                    </p>  
                  <label for="location">Product Description</label>
              
                    	
                        <textarea cols="15" id="editor1" name="editor1" rows="5"></textarea>
			<script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'editor1',
					{
						fullPage : true,
						extraPlugins : 'docprops',
					
        filebrowserBrowseUrl : '/techdata/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/techdata/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/techdata/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
 
					});

			//]]>
			</script>
                  
         			
         		</td>
         		
         
         	</tr>
        </table>
         
        <script src="<?php echo HTTP_SERVER;?>jquery.chained.mini.js" type="text/javascript" charset="utf-8">
        	</script>
        	<script type="text/javascript" charset="utf-8">
          $(function(){
              $("#subcategory").chained("#category"); 
            });
          </script> 
             
                   
        <br>     
      <button>Submit</button>        
     </div>
    
   <!--country1-->
      
  </div>                
				
			
          	

	
	</form>	
  </div>
   
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
<script type="text/javascript">

var countries=new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init()

</script>