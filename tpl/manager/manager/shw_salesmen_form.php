<?php
global $CONF,$CODE;

$url_account_manager_listing = $CONF['url_app'].'?m=manager&c=show_salesman';

if($Q->req['msg']==1){
	?>
	<script>
	alert('You have sucessfully added 1 Salesman');
	location.href='?m=admin&c=show_salesmen_form';
	</script>
	
	<?php
}

?>


					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo $CONF['url_app'];?>?m=admin">Home</a>
							</li>

							
						     <li>
								<a href="<?php echo $url_account_manager_listing;?>">Salesman Listing</a>
							</li>
							<li class="active">Add New Salesman</li>
						</ul><!-- .breadcrumb -->
                      

						<!-- #nav-search -->
					</div>

                   <div class="page-content">
						<div class="page-header">
							<h1>
								Add New Salesman
								
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
									<div class="row-fluid">
									<div class="span12">
										<div class="widget-box">
											<div class="widget-header widget-header-blue widget-header-flat">
												<h3 class="lighter block green">Enter the salesman information </h3>
						                  </div>

											<div class="widget-body">
												<div class="widget-main">
												
										<div class="row-fluid position-relative" >
										
                                        <?php
                                         $cError = count($error);
										 if($cError > 0){
											 ?>
											   <b><font color="#FF0000">Error(s) Messages : <br /> ----------------------------------------------<br />
											 
											 <?php
											foreach($error as $k => $v){
												 echo $v."<br>";
												 
												
											}
											?>
                                          
                                             ----------------------------------------------
                                               </font></b>
											<?php
											
											
										 }
										 
										?>
                                        
                                        
                                        				
									<form class="form-horizontal" role="form" action="<?php echo $CONF['url_app']?>?m=manager&c=do_add_salesman" method="post">
								
                                	 <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Type of Salesman </label>

										<div class="col-sm-9">
											<select name="intType">
                                            <?php
                                            foreach($CODE['salesman_type'] as $k_value => $vType_name){
												?>
												<option value="<?php echo $k_value;?>"><?php echo $vType_name;?></option>
												
												<?php
												
											}
											?>
                                            </select>
										</div>
									</div>
                                    
                                
                                       <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Full Name ( as in IC / Passport No.) </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="Full Name" value="<?php echo $Q->req['strFullName'];?>" name="strFullName" class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                     <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Commission Rate </label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1"  name="strCommisionRate" value="<?php echo $Q->req['strCommisionRate'];?>" class="col-xs-10 col-sm-5" /> %
										</div>
									</div>
                                      <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email Address </label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1" placeholder="xxx@cfoni.com" name="strEmail" value="<?php echo $Q->req['strEmail'];?>" class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact No. </label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" placeholder="" name="strContact" value="<?php echo $Q->req['strContact'];?>" class="col-xs-10 col-sm-5" />
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Username </label>

										<div class="col-sm-9">
										  <input type="text" id="form-field-1" placeholder="Username" value="<?php echo $Q->req['strUsername'];?>" name="strUsername" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Password </label>

										<div class="col-sm-9">
										  <input type="password" id="form-field-2" placeholder="Password" name="strPassword" class="col-xs-10 col-sm-5" />
											
										</div>
									</div>

									<div class="space-4"></div>

										<div class="form-group">
													<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Auto Send Login Details ? </label>
										            <label class="middle">
													&nbsp;&nbsp;<input class="ace" type="checkbox" value="1" name="intAutoEmail" id="id-disable-check" />
													<span class="lbl"></span>
												</label>
														
                                        
                                        </div>					
														
													<hr />
													<div class="clearfix form-actions">
										           <div class="col-md-offset-3 col-md-9">
											      <button class="btn btn-info" type="submit">
												     <i class="icon-ok bigger-110"></i>
												    Submit
											     </button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												Reset
											</button>
										</div>
                                         </div>
                                        </form>
                                       
									</div>
												</div><!-- /widget-main -->
											</div><!-- /widget-body -->
										</div>
									</div>
								</div>

								
						  </div>
  </div>
								</div><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
			