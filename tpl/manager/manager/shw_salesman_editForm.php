<?php
global $CONF,$CODE;

?>

<script>
function fn_close_refresh(){
	parent.jQuery.fancybox.close()
   parent.location.reload(true);
                                 
}
</script>



	<!-- basic styles -->
		<link href="<?php echo HTTP_SERVER;?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo HTTP_SERVER;?>assets/css/font-awesome.min.css" />
        <link rel="shortcut icon" href="<?php echo HTTP_SERVER;?>/design/<?php echo $CONF['tpl_name'] ;?>/images/cfoni.ico" type="image/x-icon" >
     
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
												<div class="table-header" style="padding-left:20px;"><h1>
												
													Edit Salesman Record </h1>
												</div>
											</div>
                                          
                                          <div id="simple-msg">
                                         
                                          
                                          </div>
                                           <form name="ajaxform" id="ajaxform" action="<?php echo $CONF['url_app'];?>?m=manager&c=do_update_salesmen" method="post">
                                           <input type="hidden" name="id" value="<?php echo $data['intSalesId'];?>" />
                                          	<div class="modal-body no-padding">
												<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
													<tbody>
                                                   <tr>
                                                    <td>Salesman Type</td>
                                                    <td>
                                                    <select name="intType">
                                           				 <?php
                                           					 foreach($CODE['salesman_type'] as $k_value => $vType_name){
															?>
															<option value="<?php echo $k_value;?>"  <?php if($data['intType']==$k_value){ echo "selected";}?>  ><?php echo $vType_name;?></option>
															<?php
															}
														?>
                                            		</select>
                                                    
                                                    </td>
                                                    </tr>
                                                  
                                                    <tr>
                                                        <td>Username</td>
                                                        <td><?php echo $data['strUsername']; ?></td>
												       </tr>
                                                      
														<tr>
                                                        <td>Full Name</td>
                                                        <td><input type="text" value="<?php echo $data['strFullName']; ?>" name="strFullName" /></td>
												       </tr>
                                                       
                                                        <tr>
                                                        <td>Comission Rate</td>
                                                        <td><input type="text" value="<?php echo $data['strCommisionRate']; ?>" name="strCommisionRate" /></td>
												       </tr>
                                                       <tr>
                                                        <td>Email</td>
                                                        <td><input type="text" value="<?php echo $data['strEmail'] ?>" name="strEmail" /></td>
												       </tr>
                                                       <tr>
                                                        <td>Contact No.</td>
                                                        <td><input type="text" value="<?php echo $data['strContact'] ?>" name="strContact"/></td>
												       </tr>
                                                       <tr>
                                                        <td>Status</td>
                                                        <td>
                                                        <select name="intStatus">
                                                         <?php
                                                         foreach($CODE['status'] as $kstatus => $vstatus){
															?>
															<option value="<?php echo$kstatus ?>" <?php if($kstatus==$data['intStatus']){ echo "selected";} ?> ><?php echo $vstatus; ?></option>
															
															<?php 
														  }
														 
														 ?>
                                                        </select>
                                                        </td>
												       </tr>
                                                      
                                                       <tr>
                                                        <td><b>Reset Password</b></td>
                                                        
                                                        <td><input type="text" name="strPassword" value=""  /></td>
                                                       </tr>
                                                       
                                                       
                                                       
													</tbody>
												</table>
										
										  
											<div class="modal-footer no-margin-top">
												<button class="btn btn-sm btn-danger pull-left" id="simple-post" type="submit">
												
													Update
												</button>
                                               <button class="btn btn-sm btn-danger pull-left" onClick="javascript:fn_close_refresh();">												
													Close & Refresh
												</button>
 											</div>
                                            
                                             </form>
<script>
$(document).ready(function()
{
	
$("#simple-post").click(function()
{
	$("#ajaxform").submit(function(e)
	{
		$("#simple-msg").html("<img src='loading.gif'/>");
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
		$.ajax(
		{
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data, textStatus, jqXHR) 
			{
				$("#simple-msg").html('<pre><code class="prettyprint">'+data+'</code></pre>');

			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				$("#simple-msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus='+textStatus+', errorThrown='+errorThrown+'</code></pre>');
			}
		});
	    e.preventDefault();	//STOP default action
	    e.unbind();
	});
		
	$("#ajaxform").submit(); //SUBMIT FORM
});

});
</script>
								
