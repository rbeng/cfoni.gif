-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 05, 2012 at 04:43 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `cfoni`
--


--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `intCustomerId` int(12) unsigned NOT NULL auto_increment,
  `intAccCate` int(3) NOT NULL default '1' COMMENT '1=Personel ,2=Company',
  `strFirstName` varchar(220) NOT NULL,
  `strLastName` varchar(220) NOT NULL,
  `strMobile` varchar(220) default NULL,
  `strCompanyName` varchar(220) default NULL,
  `strCompanyAdd` text,
  `strCompanyImage` text,
  `strCompanyAttachment` text,
  `strCompanyCity` varchar(100) default NULL,
  `strCompanyState` varchar(150) default NULL,
  `strCompanyCountry` int(5) default NULL,
  `strCompanyZip` varchar(25) default NULL,
  `strUser` varchar(220) default NULL,
  `strPass`  varchar(220) default NULL,
  `strIP` varchar(100) NOT NULL,
  `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
  `intResellerId` int(12) NOT NULL,
  `intSalesId` int(12) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intCustomerId`),
  KEY `CreateDate` (`CreateDate`)
)
--
-- Table structure for table `customer_session`
--
CREATE TABLE `customer_session` (
 `intCustomerSessionId` int(12) unsigned NOT NULL auto_increment,
 `strEmail` varchar(220) default NULL,
 `strSession` varchar(220) default NULL,
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intCustomerSessionId`),
  KEY `CreateDate` (`CreateDate`)
)
--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `intFeedbackId` int(12) unsigned NOT NULL auto_increment,
  `strName` varchar(200) character set utf8 NOT NULL,
  `strCompanyName` varchar(200) character set utf8 NOT NULL,
  `strEmail` varchar(200) character set utf8 NOT NULL,
  `strTel`   varchar(150) character set utf8 NOT NULL,
  `intType`  int(12) unsigned NOT NULL default '1' COMMENT '1=General ,2=Technical,3=PBX',
  `strMessage` text,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intFeedbackId`),
  KEY `CreateDate` (`CreateDate`)
) ;

--
-- Table structure for table `accountmanager` : Create Salesman
--
CREATE TABLE `accountmanager` (
  `intAccountManagerId` int(12) unsigned NOT NULL auto_increment,
  `strName` varchar(200) character set utf8 NOT NULL,
  `strContact`   varchar(150) character set utf8 NOT NULL,
  `strEmail` varchar(200) character set utf8 NOT NULL,
  `strUser` varchar(220) default NULL,
  `strPass`  varchar(220) default NULL,
  `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
  `strAddedBy` varchar(220) default NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intResellerId`),
  KEY `CreateDate` (`CreateDate`)

)

--
-- Table structure for table `accountmanager_session`
--
CREATE TABLE `accountmanager_session` (
 `intAccountManagerSessionId` int(12) unsigned NOT NULL auto_increment,
 `strEmail` varchar(220) default NULL,
 `strSession` varchar(220) default NULL,
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intAccountManagerSessionId`),
  KEY `CreateDate` (`CreateDate`)
)

--
-- Table structure for table `sales` : Consider Indoor / Out Door
--
CREATE TABLE `salesman` (
  `intSalesId` int(12) unsigned NOT NULL auto_increment,
  `strSalesCode` text,
  `strType` NOT NULL default '1' COMMENT '1=InDoor ,2=OutDoor',
  `intCommisionRate` varchar(10) NOT NULL, 
  `strName` varchar(220) default NULL,
  `strIC` varchar(220) default NULL,
  `strEmail` varchar(220) default NULL,  
  `strAdd` text,
  `strCity` varchar(100) default NULL,
  `strState` varchar(150) default NULL,
  `strCountry` int(5) default NULL,
  `strZip` varchar(25) default NULL,
  `strUser` varchar(220) default NULL,
  `strPass`  varchar(220) default NULL,
  `strEmail` varchar(220) default NULL,
  `intAccountManagerId` int(12) NOT NULL,
  `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intSalesId`),
  KEY `CreateDate` (`CreateDate`)

)
--
-- Table structure for table `salesman_session`
--
CREATE TABLE `salesman_session` (
 `intSalesSessionId` int(12) unsigned NOT NULL auto_increment,
 `strEmail` varchar(220) default NULL,
 `strSession` varchar(220) default NULL,
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intSalesSessionId`),
  KEY `CreateDate` (`CreateDate`)
)


--
-- Table structure for table `reseller`
--
CREATE TABLE `reseller` (
  `intResellerId` int(12) unsigned NOT NULL auto_increment,
  `strResellerCode` text,
  `intCommisionRate`  int(12) NOT NULL, 
  `strCompanyName` varchar(220) default NULL,
  `strCompanyAdd` text,
  `strCompanyCity` varchar(100) default NULL,
  `strCompanyState` varchar(150) default NULL,
  `strCompanyCountry` int(5) default NULL,
  `strCompanyZip` varchar(25) default NULL,
  `strEmail` varchar(220) default NULL,
  `strPass`  varchar(220) default NULL,
  `intSalesId` int(12)  NULL,
  `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intSalesId`),
  KEY `CreateDate` (`CreateDate`)

)

--
-- Table structure for table `reseller_session`
--
CREATE TABLE `reseller_session` (
 `intResellerId` int(12) unsigned NOT NULL auto_increment,
 `strEmail` varchar(220) default NULL,
 `strSession` varchar(220) default NULL,
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intResellerId`),
  KEY `CreateDate` (`CreateDate`)
)


--
-- Table structure for table `product` # Unique :Domain Name , Hosting Plan , CRM, CT
--
CREATE TABLE `product` (
 `intProductId` int(12) unsigned NOT NULL auto_increment,
 `strProductName` varchar(220) default NULL,
 `strDescription` text,
 `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intProductId`),
  KEY `CreateDate` (`CreateDate`)
)

--
-- Table structure for table `package` # Unique
--
CREATE TABLE `package` (
 `intPackageId` int(12) unsigned NOT NULL auto_increment,
 `strProductName` varchar(220) default NULL,
 `strImage` text,
 `strDescription` text,
  `strPrice` text,
  `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Inactive',
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intPackageId`),
  KEY `CreateDate` (`CreateDate`)
)


--
-- Table structure for table `package_details` # Unique
--
CREATE TABLE `package_details` (
 `intPackageDetailsId` int(12) unsigned NOT NULL auto_increment,
 `intProductId` int(12) NOT NULL ,
 `strDescription` text,
  `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Inactive',
  `intPackageId` int(12) NOT NULL,
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intPackageDetailsId`),
  KEY `CreateDate` (`CreateDate`)
)


--
-- Table structure for table `number_did_fax` # Unique
--
CREATE TABLE `number_did_fax` (
 `intDidFaxId` int(12) unsigned NOT NULL auto_increment,
 `strType` NOT NULL default '1' COMMENT '1=DID ,2=FAX',
  `strNumber` varchar(30) default NULL,
  `intStatus` int(3) NOT NULL default '1' COMMENT '1=InUsed ,2=Unuse , 3= Pending ',
  `intPackageId` int(12) NOT NULL,
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intDidFaxId`),
  KEY `CreateDate` (`CreateDate`)
)

--
-- Table structure for table `faq` # Unique
--
CREATE TABLE `faq` (
 `intFaqId` int(12) unsigned NOT NULL auto_increment,
 `intType` NOT NULL default '1' COMMENT '1=General ,2=Account , 3=Payment',
 `strQ` text,
 `strA` text,
 `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
 `strAddedBy` text,
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intFaqId`),
  KEY `CreateDate` (`CreateDate`)
)


--
-- Table structure for table `Order` # Unique
--

CREATE TABLE `order` (
 `intOrderId` int(12) unsigned NOT NULL auto_increment,
 `strOrderNo` varchar(200) ,
 `intPackageId` int(12),
 `intCustomerId` int(12),
 `strDate` varchar(30) ,
 `strMonth` varchar(30),
 `strYear` varchar(30) ,
 `strApproveDate` varchar(30) ,
 `strApproveMonth` varchar(30) ,
 `strApproveYear` varchar(30) ,
 `strStartDateMK` varchar(150) ,
 `strEndDateMK`   varchar(150) ,
 `strDomainAddOnQty` varchar(150) ,
 `strDomainAddOnCost` varchar(150) ,
 `strEmailMailBoxCostAddOnQty` varchar(150) ,
 `strEmailMailBoxCostAddOnCost` varchar(150) ,
 `strExtensionAddOnQty` varchar(150),
 `strExtensionAddOnCost` varchar(150) ,
 `strTotalCost` varchar(150) ,
 `strTotalMailBox` varchar(150) ,
 `strCRMName` varchar(150) ,
 `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intOrderId`),
  KEY `CreateDate` (`CreateDate`)
)


--
-- Table structure for table `Order_phone` # Unique
--

CREATE TABLE `order_phone` (
 `intOrderPhoneDetailsId` int(12) unsigned NOT NULL auto_increment,
 `intOrderId` int(12),
 `intCustomerId` int(12),
 `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
 `intTypePhone` int(3) NOT NULL default '1' COMMENT '1=DID ,2=Fax,3=Extension',
 `strNumber` varchar(30) default NULL,
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intOrderPhoneDetailsId`),
  KEY `CreateDate` (`CreateDate`)
)

--
-- Table structure for table `order_domian` # Unique
--
CREATE TABLE `order_domain` (
 `intOrderDomainDetailsId` int(12) unsigned NOT NULL auto_increment,
 `intOrderId` int(12),
 `intCustomerId` int(12),
 `intTypeDomain` int(3) NOT NULL default '1' COMMENT '1=New ,2=Transfer',
 `strAuthCode` text ,
 `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
 `strDomainName` varchar(30) ,
 `strDate` varchar(30) ,
 `strMonth` varchar(30) ,
 `strYear` varchar(30) ,
 `strApproveDate` varchar(30) ,
 `strApproveMonth` varchar(30) ,
 `strApproveYear` varchar(30) ,
 `strStartDateMK` varchar(150) ,
 `strEndDateMK`   varchar(150) ,
 `intHowmanyyearId` int(3) ,
 `strHowmanyyearCost` varchar(30) ,
 `CreateDate` datetime ,
 `UpdateDate` datetime ,
  PRIMARY KEY  (`intOrderDomainDetailsId`),
  KEY `CreateDate` (`CreateDate`)
)

--
-- Table structure for table `order_crm` # Unique
--

CREATE TABLE `order_crm` (
 `intCRMId` int(12) unsigned NOT NULL auto_increment,
 `intOrderId` int(12),
 `intCustomerId` int(12),
 `strCRMEmail` text ,
 `strCRMName` text ,
 `intStatus` int(3) NOT NULL default '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
 `CreateDate` datetime NOT NULL,
 `UpdateDate` datetime NOT NULL,
  PRIMARY KEY  (`intCRMId`),
  KEY `CreateDate` (`CreateDate`)
)




