-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 12, 2014 at 01:12 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cfoni_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountmanager`
--

CREATE TABLE IF NOT EXISTS `accountmanager` (
  `intAccountManagerId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strFullName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `strContact` varchar(150) CHARACTER SET utf8 NOT NULL,
  `strEmail` varchar(200) CHARACTER SET utf8 NOT NULL,
  `strUser` varchar(220) DEFAULT NULL,
  `strPass` varchar(220) DEFAULT NULL,
  `intStatus` int(3) NOT NULL DEFAULT '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive,5=delete',
  `intAutoEmailInfor` int(2) NOT NULL DEFAULT '0',
  `strAddedBy` varchar(50) NOT NULL,
  `strNowLogin` varchar(50) NOT NULL,
  `strLastlogin` varchar(50) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intAccountManagerId`),
  UNIQUE KEY `strEmail` (`strEmail`,`strUser`),
  UNIQUE KEY `strEmail_2` (`strEmail`,`strUser`),
  UNIQUE KEY `strUser` (`strUser`),
  UNIQUE KEY `strUser_2` (`strUser`),
  KEY `CreateDate` (`CreateDate`),
  KEY `intAccountManagerId` (`intAccountManagerId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `accountmanager`
--

INSERT INTO `accountmanager` (`intAccountManagerId`, `strFullName`, `strContact`, `strEmail`, `strUser`, `strPass`, `intStatus`, `intAutoEmailInfor`, `strAddedBy`, `strNowLogin`, `strLastlogin`, `CreateDate`, `UpdateDate`) VALUES
(1, 'Tan1 Kim Meng', '1988888888', 'kim@cfoni.com', 'kim', '1bbd886460827015e5d605ed44252251', 5, 1, 'admin', '', '', '2014-02-05 10:30:07', '2014-02-06 09:43:04'),
(2, 'Perk Ngok Beng1', '-9080081', 'perk@cfoni.com', 'acc_perk', 'e3ceb5881a0a1fdaad01296d7554868d', 1, 0, 'admin', '2014-02-11 13:50:30', '2014-02-11 13:50:09', '2014-02-05 11:28:57', '2014-02-11 13:50:20'),
(3, 'Chong Tien Fong', '01988888888', 'ts@hotmail.com', 'acc_ts', '96e79218965eb72c92a549dd5a330112', 1, 1, 'admin', '', '', '2014-02-05 11:35:52', '2014-02-05 17:21:24'),
(4, 'Chong Tien Fqong', '1988888889', 'tsq@hotmail.com', 'acc_tsb', '96e79218965eb72c92a549dd5a330112', 2, 1, 'admin', '', '', '2014-02-05 11:36:37', '2014-02-06 11:33:06'),
(5, 'TW', '198886266', 'ggg11@hotmaail.com', 'acc_111111', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', 1, 1, 'admin', '', '', '2014-02-05 11:38:41', '2014-02-05 17:05:02'),
(6, 'TW', '198886266', 'ggg@hotmaail.com', 'acc_111119', '670b14728ad9902aecba32e22fa4f6bd', 3, 0, 'admin', '', '', '2014-02-05 11:41:22', '2014-02-09 19:29:12'),
(7, 'Lam', '988299', 'lam@hotmail.com', 'acc_888', '96e79218965eb72c92a549dd5a330112', 5, 1, 'admin', '', '', '2014-02-05 11:42:12', '2014-02-06 16:04:22'),
(8, 'Lam dd', '0988299232', 'lamdd@hotmail.com', 'acc_888wwe', '2c2ac91c2cc709a9b5cffff4b3f0f905', 5, 1, 'admin', '', '', '2014-02-05 11:45:03', '2014-02-05 17:59:27'),
(9, 'fvr', '43r34', 'rgrtg@hotn.com', '3434', 'c788b2c99cca6eae332cf9f7a297ea0b', 5, 0, 'admin', '', '', '2014-02-05 12:30:47', '2014-02-06 10:53:46'),
(10, 'ewf3f34', '87897', '3f34f34@cfoni.con', 'hkjhkh', '72877eaa860314902d1d73e273b66202', 5, 0, 'admin', '', '', '2014-02-05 12:31:00', '2014-02-06 10:53:09'),
(11, 'Tien Long', '8y878', 'erfrfre@hotmailcon.com', 'khjkhk', '10d3aeb6639675594477989a6098627d', 5, 0, 'admin', '', '', '2014-02-05 12:31:22', '2014-02-06 15:08:33');

-- --------------------------------------------------------

--
-- Table structure for table `accountmanager_session`
--

CREATE TABLE IF NOT EXISTS `accountmanager_session` (
  `intAccountManagerSessionId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strUser` varchar(220) DEFAULT NULL,
  `strSession` varchar(220) DEFAULT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intAccountManagerSessionId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `accountmanager_session`
--

INSERT INTO `accountmanager_session` (`intAccountManagerSessionId`, `strUser`, `strSession`, `CreateDate`, `UpdateDate`) VALUES
(29, 'acc_perk', '9Ygksble9Tq4', '2014-02-11 13:50:30', '2014-02-11 13:50:30');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `intCustomerId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `intAccCate` int(3) NOT NULL DEFAULT '1' COMMENT '1=Personel ,2=Company',
  `strFirstName` varchar(220) NOT NULL,
  `strLastName` varchar(220) NOT NULL,
  `strMobile` varchar(220) DEFAULT NULL,
  `strEmail` varchar(200) NOT NULL,
  `strCompanyName` varchar(220) DEFAULT NULL,
  `strCompanyAdd` text,
  `strCompanyImage` text,
  `strCompanyAttachment` text,
  `strCompanyCity` varchar(100) DEFAULT NULL,
  `strCompanyState` varchar(150) DEFAULT NULL,
  `strCompanyCountry` int(5) DEFAULT NULL,
  `strCompanyZip` varchar(25) DEFAULT NULL,
  `strUser` varchar(220) DEFAULT NULL,
  `strPass` varchar(220) DEFAULT NULL,
  `strIP` varchar(100) NOT NULL,
  `intStatus` int(3) NOT NULL DEFAULT '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
  `strResellerCode` text NOT NULL,
  `strSalesmanCode` text NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intCustomerId`),
  KEY `CreateDate` (`CreateDate`),
  KEY `intCustomerId` (`intCustomerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_session`
--

CREATE TABLE IF NOT EXISTS `customer_session` (
  `intCustomerSessionId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strEmail` varchar(220) DEFAULT NULL,
  `strSession` varchar(220) DEFAULT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intCustomerSessionId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `intFeedbackId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `strCompanyName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `strEmail` varchar(200) CHARACTER SET utf8 NOT NULL,
  `strTel` varchar(150) CHARACTER SET utf8 NOT NULL,
  `intType` int(12) unsigned NOT NULL DEFAULT '1' COMMENT '1=General ,2=Technical,3=PBX',
  `strMessage` text,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intFeedbackId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `number_did_fax`
--

CREATE TABLE IF NOT EXISTS `number_did_fax` (
  `intDidFaxId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strBatch` varchar(100) NOT NULL,
  `strNumber` varchar(30) DEFAULT NULL,
  `intType` int(20) NOT NULL COMMENT '1= Normal, 2= Special',
  `strPrice` varchar(30) NOT NULL DEFAULT '0.00',
  `intStatus` int(3) NOT NULL DEFAULT '1' COMMENT '1=New ,2=In-Used , 3= Pending , 4=Suspend,5=Deleted',
  `strAddedBy` varchar(100) NOT NULL,
  `intOrderId` int(12) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intDidFaxId`),
  UNIQUE KEY `strNumber` (`strNumber`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=166 ;

--
-- Dumping data for table `number_did_fax`
--

INSERT INTO `number_did_fax` (`intDidFaxId`, `strBatch`, `strNumber`, `intType`, `strPrice`, `intStatus`, `strAddedBy`, `intOrderId`, `CreateDate`, `UpdateDate`) VALUES
(1, 'S120214009', '60322026201', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(2, 'S120214009', '60322026204', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(3, 'S120214009', '60322026205', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(4, 'S120214009', '60322026207', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(5, 'S120214009', '60322026208', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(6, 'S120214009', '60322026209', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(7, 'S120214009', '60322026210', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(8, 'S120214009', '60322026212', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(9, 'S120214009', '60322026213', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(10, 'S120214009', '60322026214', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(11, 'S120214009', '60322026215', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(12, 'S120214009', '60322026216', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(13, 'S120214009', '60322026217', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(14, 'S120214009', '60322026218', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(15, 'S120214009', '60322026219', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(16, 'S120214009', '60322026227', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(17, 'S120214009', '60322026228', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(18, 'S120214009', '60322026229', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(19, 'S120214009', '60322026231', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(20, 'S120214009', '60322026234', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(21, 'S120214009', '60322026235', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(22, 'S120214009', '60322026237', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(23, 'S120214009', '60322026238', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(24, 'S120214009', '60322026239', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(25, 'S120214009', '60322026240', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(26, 'S120214009', '60322026241', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(27, 'S120214009', '60322026243', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(28, 'S120214009', '60322026245', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(29, 'S120214009', '60322026246', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(30, 'S120214009', '60322026247', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(31, 'S120214009', '60322026248', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(32, 'S120214009', '60322026249', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(33, 'S120214009', '60322026251', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(34, 'S120214009', '60322026253', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(35, 'S120214009', '60322026254', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(36, 'S120214009', '60322026256', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(37, 'S120214009', '60322026257', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(38, 'S120214009', '60322026258', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(39, 'S120214009', '60322026259', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(40, 'S120214009', '60322026261', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(41, 'S120214009', '60322026264', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:22', '2014-02-12 15:20:22'),
(42, 'S120214009', '60322026265', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(43, 'S120214009', '60322026267', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(44, 'S120214009', '60322026269', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(45, 'S120214009', '60322026271', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(46, 'S120214009', '60322026273', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(47, 'S120214009', '60322026274', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(48, 'S120214009', '60322026275', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(49, 'S120214009', '60322026276', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(50, 'S120214009', '60322026278', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(51, 'S120214009', '60322026279', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(52, 'S120214009', '60322026281', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(53, 'S120214009', '60322026283', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(54, 'S120214009', '60322026284', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(55, 'S120214009', '60322026285', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(56, 'S120214009', '60322026286', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(57, 'S120214009', '60322026287', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(58, 'S120214009', '60322026289', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(59, 'S120214009', '60322026290', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(60, 'S120214009', '60322026291', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(61, 'S120214009', '60322026293', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(62, 'S120214009', '60322026294', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(63, 'S120214009', '60322026295', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(64, 'S120214009', '60322026296', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(65, 'S120214009', '60322026297', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(66, 'S120214009', '60322026298', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(67, 'S120214009', '60322026301', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(68, 'S120214009', '60322026302', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(69, 'S120214009', '60322026304', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(70, 'S120214009', '60322026305', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(71, 'S120214009', '60322026307', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(72, 'S120214009', '60322026308', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(73, 'S120214009', '60322026309', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(74, 'S120214009', '60322026310', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(75, 'S120214009', '60322026312', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(76, 'S120214009', '60322026313', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(77, 'S120214009', '60322026314', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(78, 'S120214009', '60322026315', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(79, 'S120214009', '60322026316', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(80, 'S120214009', '60322026317', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(81, 'S120214009', '60322026318', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(82, 'S120214009', '60322026319', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(83, 'S120214009', '60322026321', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(84, 'S120214009', '60322026324', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(85, 'S120214009', '60322026325', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(86, 'S120214009', '60322026327', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(87, 'S120214009', '60322026328', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(88, 'S120214009', '60322026329', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(89, 'S120214009', '60322026331', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(90, 'S120214009', '60322026332', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(91, 'S120214009', '60322026334', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(92, 'S120214009', '60322026335', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(93, 'S120214009', '60322026337', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(94, 'S120214009', '60322026338', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(95, 'S120214009', '60322026340', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(96, 'S120214009', '60322026341', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(97, 'S120214009', '60322026342', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(98, 'S120214009', '60322026343', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(99, 'S120214009', '60322026345', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(100, 'S120214009', '60322026346', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(101, 'S120214009', '60322026347', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(102, 'S120214009', '60322026348', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(103, 'S120214009', '60322026349', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(104, 'S120214009', '60322026351', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(105, 'S120214009', '60322026352', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(106, 'S120214009', '60322026354', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(107, 'S120214009', '60322026356', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(108, 'S120214009', '60322026357', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(109, 'S120214009', '60322026358', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(110, 'S120214009', '60322026359', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(111, 'S120214009', '60322026360', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(112, 'S120214009', '60322026361', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(113, 'S120214009', '60322026362', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(114, 'S120214009', '60322026364', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(115, 'S120214009', '60322026365', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(116, 'S120214009', '60322026367', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(117, 'S120214009', '60322026368', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(118, 'S120214009', '60322026369', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(119, 'S120214009', '60322026371', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(120, 'S120214009', '60322026372', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(121, 'S120214009', '60322026373', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(122, 'S120214009', '60322026374', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(123, 'S120214009', '60322026375', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(124, 'S120214009', '60322026378', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(125, 'S120214009', '60322026379', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:23', '2014-02-12 15:20:23'),
(126, 'S120214009', '60322026381', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(127, 'S120214009', '60322026382', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(128, 'S120214009', '60322026384', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(129, 'S120214009', '60322026385', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(130, 'S120214009', '60322026387', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(131, 'S120214009', '60322026389', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(132, 'S120214009', '60322026390', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(133, 'S120214009', '60322026391', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(134, 'S120214009', '60322026392', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(135, 'S120214009', '60322026394', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(136, 'S120214009', '60322026395', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(137, 'S120214009', '60322026396', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(138, 'S120214009', '60322026397', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(139, 'S120214009', '60322026398', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(140, 'S120214009', '60322026102', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(141, 'S120214009', '60322026103', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(142, 'S120214009', '60322026104', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(143, 'S120214009', '60322026105', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(144, 'S120214009', '60322026107', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(145, 'S120214009', '60322026108', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(146, 'S120214009', '60322026109', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(147, 'S120214009', '60322026113', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(148, 'S120214009', '60322026114', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(149, 'S120214009', '60322026115', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(150, 'S120214009', '60322026116', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(151, 'S120214009', '60322026117', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(152, 'S120214009', '60322026118', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(153, 'S120214009', '60322026123', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(154, 'S120214009', '60322026124', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(155, 'S120214009', '60322026125', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(156, 'S120214009', '60322026127', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(157, 'S120214009', '60322026128', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(158, 'S120214009', '60322026129', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(159, 'S120214009', '60322026091', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(160, 'S120214009', '60322026092', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(161, 'S120214009', '60322026093', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(162, 'S120214009', '60322026094', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(163, 'S120214009', '60322026095', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(164, 'S120214009', '60322026097', 1, '0', 1, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 15:20:24'),
(165, 'S120214009', '60322026098', 1, '0', 5, 'admin', 0, '2014-02-12 15:20:24', '2014-02-12 16:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE IF NOT EXISTS `package` (
  `intPackageId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strPackageName` varchar(220) DEFAULT NULL,
  `strImage` text,
  `strFeature` text NOT NULL,
  `strDescription` text,
  `strPrice` text,
  `intOrder` int(5) NOT NULL,
  `intStatus` int(3) NOT NULL DEFAULT '1' COMMENT '1=Active ,2=Inactive',
  `strAddedBy` varchar(200) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intPackageId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`intPackageId`, `strPackageName`, `strImage`, `strFeature`, `strDescription`, `strPrice`, `intOrder`, `intStatus`, `strAddedBy`, `CreateDate`, `UpdateDate`) VALUES
(1, 'Micro-Enterprise', 'micro.jpg', '<html>\r\n	<head>\r\n		<title></title>\r\n	</head>\r\n	<body>\r\n		<p>\r\n			hello</p>\r\n	</body>\r\n</html>\r\n', '&lt;html&gt;\r\n	&lt;head&gt;\r\n		&lt;title&gt;&lt;/title&gt;\r\n	&lt;/head&gt;\r\n	&lt;body&gt;\r\n		&lt;p&gt;\r\n			abc =P&lt;/p&gt;\r\n	&lt;/body&gt;\r\n&lt;/html&gt;\r\n', '1100.00', 1, 1, 'admin', '2014-02-09 17:46:01', '2014-02-10 10:45:09');

-- --------------------------------------------------------

--
-- Table structure for table `package_details`
--

CREATE TABLE IF NOT EXISTS `package_details` (
  `intPackageDetailsId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `intProductId` int(12) NOT NULL,
  `strDescription` text,
  `intStatus` int(3) NOT NULL DEFAULT '1' COMMENT '1=Active ,2=Inactive',
  `intPackageId` int(12) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intPackageDetailsId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `intProductId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strProductName` varchar(220) DEFAULT NULL,
  `strDescription` text,
  `intOrder` int(5) NOT NULL,
  `strAddedBy` varchar(100) NOT NULL,
  `intStatus` int(3) NOT NULL DEFAULT '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intProductId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`intProductId`, `strProductName`, `strDescription`, `intOrder`, `strAddedBy`, `intStatus`, `CreateDate`, `UpdateDate`) VALUES
(1, 'PBX Cloud', '&lt;html&gt;\r\n	&lt;head&gt;\r\n		&lt;title&gt;&lt;/title&gt;\r\n	&lt;/head&gt;\r\n	&lt;body&gt;\r\n		&lt;table align=&quot;center&quot; border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot; style=&quot;width: 800px;&quot;&gt;\r\n			&lt;tbody&gt;\r\n				&lt;tr&gt;\r\n					&lt;td valign=&quot;top&quot;&gt;\r\n						&lt;img alt=&quot;&quot; src=&quot;/ckfinder/userfiles/images/cloud.png&quot; style=&quot;width: 400px; height: 117px;&quot; /&gt;&lt;/td&gt;\r\n					&lt;td valign=&quot;top&quot;&gt;\r\n						&lt;span style=&quot;color:#0000ff;&quot;&gt;CLOUD PBX&lt;/span&gt;&lt;/td&gt;\r\n				&lt;/tr&gt;\r\n			&lt;/tbody&gt;\r\n		&lt;/table&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&amp;nbsp;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;img alt=&quot;&quot; src=&quot;/ckfinder/userfiles/images/cloud.png&quot; style=&quot;width: 954px; height: 278px;&quot; /&gt;&lt;/p&gt;\r\n	&lt;/body&gt;\r\n&lt;/html&gt;\r\n', 1, 'admin', 1, '2014-02-07 12:31:39', '2014-02-09 18:51:47');

-- --------------------------------------------------------

--
-- Table structure for table `reseller`
--

CREATE TABLE IF NOT EXISTS `reseller` (
  `intResellerId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strResellerCode` text,
  `intCommisionRate` int(12) NOT NULL,
  `strCompanyName` varchar(220) DEFAULT NULL,
  `strCompanyAdd` text,
  `strCompanyCity` varchar(100) DEFAULT NULL,
  `strCompanyState` varchar(150) DEFAULT NULL,
  `strCompanyCountry` int(5) DEFAULT NULL,
  `strCompanyZip` varchar(25) DEFAULT NULL,
  `strEmail` varchar(220) DEFAULT NULL,
  `strPass` varchar(220) DEFAULT NULL,
  `intSalesId` int(12) DEFAULT NULL,
  `intStatus` int(3) NOT NULL DEFAULT '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intResellerId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reseller_session`
--

CREATE TABLE IF NOT EXISTS `reseller_session` (
  `intResellerId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strUser` varchar(220) DEFAULT NULL,
  `strSession` varchar(220) DEFAULT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intResellerId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `salesman`
--

CREATE TABLE IF NOT EXISTS `salesman` (
  `intSalesId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strSalesmanCode` text,
  `strCommisionRate` varchar(10) NOT NULL,
  `intType` int(10) NOT NULL DEFAULT '1' COMMENT '1=Indoor , 2 = OutDoor',
  `strName` varchar(220) DEFAULT NULL,
  `strIC` varchar(220) NOT NULL,
  `strEmail` varchar(220) DEFAULT NULL,
  `strContact` varchar(50) NOT NULL,
  `strAdd` text NOT NULL,
  `strCity` varchar(100) NOT NULL,
  `strState` varchar(150) NOT NULL,
  `strCountry` int(5) NOT NULL,
  `strZip` varchar(25) NOT NULL,
  `strUser` varchar(220) DEFAULT NULL,
  `strPass` varchar(220) DEFAULT NULL,
  `intAccountManagerId` int(12) NOT NULL,
  `intStatus` int(3) DEFAULT '1' COMMENT '1=Active ,2=Pending,3=Suspend,4=Inactive',
  `strNowLogin` varchar(50) NOT NULL,
  `strLastlogin` varchar(50) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intSalesId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `salesman`
--

INSERT INTO `salesman` (`intSalesId`, `strSalesmanCode`, `strCommisionRate`, `intType`, `strName`, `strIC`, `strEmail`, `strContact`, `strAdd`, `strCity`, `strState`, `strCountry`, `strZip`, `strUser`, `strPass`, `intAccountManagerId`, `intStatus`, `strNowLogin`, `strLastlogin`, `CreateDate`, `UpdateDate`) VALUES
(1, '1102148801', '4', 2, 'Perk', '', 'perk@cfoni.com', '017-3019855', '', '', '', 0, '', 'ex_perk', '111111', 2, 1, '', '', '2014-02-11 16:43:56', '2014-02-12 17:28:26'),
(2, '1102148802', '4', 2, 'Perk Cheong', '', 'ex_perk@cfoni.com', '019-99900990', '', '', '', 0, '', 's_perk', '96e79218965eb72c92a549dd5a330112', 2, 1, '2014-02-12 20:11:27', '2014-02-12 19:49:10', '2014-02-11 16:52:01', '2014-02-12 17:08:49'),
(3, '1102148803', '4', 1, 'Perk Cheong', '', 'ex_perk@cfoni.com', '019-99900990', '', '', '', 0, '', 'ex1q_perk', '111111', 2, 5, '', '', '2014-02-11 16:56:08', '2014-02-11 22:55:21');

-- --------------------------------------------------------

--
-- Table structure for table `salesman_session`
--

CREATE TABLE IF NOT EXISTS `salesman_session` (
  `intSalesSessionId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `strUser` varchar(220) DEFAULT NULL,
  `strSession` varchar(220) DEFAULT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`intSalesSessionId`),
  KEY `CreateDate` (`CreateDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
