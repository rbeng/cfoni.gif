<?php
global $CONF;

$http_dir = HTTP_SERVER.'design/'.$CONF['tpl_name'].'/';


?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>CFONI  - Cloud Communication Solutions</title>
		<meta name="keywords" content="Cloud, PBX, Telephony, DID, numbers, Malaysia, Singapore, Phone, CRM" />
		<meta name="description" content="Cloud Based PBX, CRM and Email Solutions for the Enterprise of tomorrow">
		<meta name="author" content="CFONI.COM">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="google-translate-customization" content="664860bbe3583a05-f27da74215a7257a-g520050b37cb9d19b-9"></meta>

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Libs CSS -->
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/bootstrap.css">
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/fonts/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="<?php echo $http_dir; ?>vendor/flexslider/flexslider.css" media="screen" />
		<link rel="stylesheet" href="<?php echo $http_dir; ?>vendor/magnific-popup/magnific-popup.css" media="screen" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/theme.css">
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/theme-animate.css">

		<!-- Current Page Styles -->
		<link rel="stylesheet" href="<?php echo $http_dir; ?>vendor/revolution-slider/css/settings.css" media="screen" />
		<link rel="stylesheet" href="<?php echo $http_dir; ?>vendor/revolution-slider/css/captions.css" media="screen" />
		<link rel="stylesheet" href="<?php echo $http_dir; ?>vendor/circle-flip-slideshow/css/component.css" media="screen" />

		<!-- Custom CSS -->
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/custom.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/skins/blue.css">

		<!-- Responsive CSS -->
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/bootstrap-responsive.css" />
		<link rel="stylesheet" href="<?php echo $http_dir; ?>css/theme-responsive.css" />

		<!-- Favicons -->
		<link rel="shortcut icon" href="<?php echo HTTP_SERVER;?>/design/<?php echo $CONF['tpl_name'] ;?>/images/cfoni.ico" type="image/x-icon" >
		<link rel="apple-touch-icon" href="<?php echo $http_dir; ?>img/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $http_dir; ?>img/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $http_dir; ?>img/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $http_dir; ?>img/apple-touch-icon-144x144.png">

		<!-- Head Libs -->
		<script src="<?php echo $http_dir; ?>vendor/modernizr.js"></script>

		<!--[if IE]>
			<link rel="stylesheet" href="<?php echo $http_dir; ?>css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="<?php echo $http_dir; ?>vendor/respond.js"></script>
		<![endif]-->

 
	</head>
	<body>

		<div class="body">
      <header>

			<?php   include( "header.php" );
		
			 ?>
            
      	</header>     
          
            <!-- Content -->
			<div role="main" class="main">
				<?php
  //
  // Content
  //
  if(isset($include_file['main'])) {
  // main only
  $file_str = $include_file['main'];
  if (substr($file_str, (strlen($file_str) - 3), 3) == 'php') {
  include($file_str);
  } 
  else {
  }
  } 
  else {
  trigger_error("Attempt to load template with non PHP files",E_USER_ERROR);
  }
  ?>
            
             </div>
            <!-- End Content -->
			<?php include('footer.php'); ?>
		   </div>
       

		<!-- Libs -->
		<script type="text/javascript" src="<?php echo $http_dir; ?>js/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo $http_dir; ?>vendor/jquery.js"><\/script>')</script>
		<script src="<?php echo $http_dir; ?>vendor/jquery.easing.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/jquery.appear.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/jquery.cookie.js"></script>
		<!-- <script src="master/style-switcher/style.switcher.js"></script> Style Switcher -->
		<script src="<?php echo $http_dir; ?>vendor/bootstrap.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/selectnav.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/twitterjs/twitter.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/revolution-slider/js/jquery.themepunch.plugins.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/revolution-slider/js/jquery.themepunch.revolution.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/flexslider/jquery.flexslider.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script src=<?php echo $http_dir; ?>"vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo $http_dir; ?>vendor/jquery.validate.js"></script>

		<script src="<?php echo $http_dir; ?>js/plugins.js"></script>

		<!-- Current Page Scripts -->
		<script src="<?php echo $http_dir; ?>js/views/view.home.js"></script>

		<!-- Theme Initializer -->
		<script src="<?php echo $http_dir; ?>js/theme.js"></script>

		<!-- Custom JS -->
		<script src="<?php echo $http_dir; ?>js/custom.js"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
		<!--
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		-->

	</body>
</html>




















