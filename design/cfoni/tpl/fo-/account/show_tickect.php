<?php

global $CONF;

?>



<html xmlns="http://www.w3.org/1999/xhtml" >

<head>

    <title>Print Ticket</title>

  <style type="text/css">

    body {font-family: Helvetica, Verdana, Arial, Serif; font-size: 12px;}

    table.list,table.list td,table.list th{border: 1px solid #333;border-collapse:collapse; text-align:center; padding:2px 5px; font-size: 12px;}

    table.list th { background-color: #cccccc;}

  </style>

    <script type="text/javascript" language="javascript">

      function printpage() {

        window.print();

      }

    </script>

</head>

<body onload="printpage();">

<fieldset>
<table border=0 width="100%">
	<tr>
		<td><img src="images/logo.png" width="250"></td>
		<td align="right"><h1><b>Share4More Ticket Number:  <?php echo $id;?></b></h1></td>
	</tr>
	
</table>	

<hr>
<h1><b>Grand Prize</b> <br>
<b>3D2N Phuket Escapade for 2</b></h1>
	
	
			
<h1><b>5x consolation prize</b> <br>
<b>GSC Gold Class ticket for 2</b>	
	</h1>	
	
<table border=0 width="100%">
<tr>
	<td>
		<b>Username :</b>
<?php echo $username;?><br>
<p><b>Mobile Number : </b><?php echo $mobile; ?></p>


<p><b>Date Received :</b>
<?php echo $dateReceived;?></p>
<b>Annoucement Date :</b>
6 June 2011
		
	</td>
	<td valign="top"><b>Redemption Location :</b><br>
		<p><b>MBoard Media SDN BHD (793604-D)</b>
		<br>
		UNIT I-1-06,1st Floor Block I Taipan 2,<br>JLN 1A/1 Ara Damansara,<br>47301 PJ, Selangor.
	</p>
		</td>
</tr>

</table>
<br>


<b>Fine Print</b>
<br>
<ul>
 <li>All existing and new individual SMS2board members are eligible to join the Share4More competition, except for management, employees and directors of SMS2board and their immediate family members.</li>

 <li>One entry per Sms2board account only.</li>
 <li>Additional ticket can be obtained by referring friends. The more friends you refer, the greater the chances for you to win the prize. See terms and conditions for details.</li>
 <li>Announcement of the winning Draw Ticket number will be made on 6 June 2011 in this Share4More page and via the SMS2board newsletter.</li>
 <li>The prize may not be exchanged for cash and is not transferable.</li>
 <li>Winner will be based on a random draw and judges'decision is final.</li>
 <li>The winner must contact SMS2board office on weekdays, between 10am to 6pm.</li>
 <li>Bring the winning ticket and personal identification (IC or passport) when claiming the prize.</li>
 </ul>
</fieldset>
<br>

<b>How2Claim:</b>
<br>

<p>If your Ticket number is selected on the announcement date, you can claim your prize with these simple steps:</p>
<ol>
<li>
Contact us at 012 2797 688 ( Keong Lee ) from Monday to Friday, 10am to 6pm.(closed on public holidays)</li>
<li>
Print out and bring the winning ticket to our office above.</li>
<li>
Present the ticket together with your personal identification(I.C or passport) and claim your prize!</li>

</ol>

<br>
<p>Email Us : <a href="mailto:feedback@sms2board.com">feedback@sms2board.com</a></p>
<p><b>Legal stuff YOU need to know;</b></p>
<p>This Share4More is governed by terms and conditions stated on : <a href="<?php echo $CONF['url_app'];?>?m=main&c=show_referral" target="_blank">http://www.sms2board.com/index.php?m=main&c=show_referral</a></p>
<p>SMS2board.com reserves the right to amend these terms and conditions, and to suspend, terminate or vary each Share4More with or without notice to members.</p>
</body>
</html>
