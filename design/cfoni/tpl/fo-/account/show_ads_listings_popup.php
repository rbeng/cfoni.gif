<?php
global $CONF;

$directoryImages = $CONF['vir_lib'].'common/';
$gmt_offset = date('Z');
$parts = getdate(time()-$gmt_offset+$CONF['const_gmt_offset']);

$today = mktime(0, 0, 0, sprintf('%02d', $parts['mon']) ,sprintf('%02d', $parts['mday']),sprintf('%04d', $parts['year']));
$intCount = count($data['ads']);
$arrStatus = array('A'=>'Active','I'=>'Inactive','P'=>'Inactive');
$timeCheck =  timeCheck(time(),	$CONF['const_gmt_offset']) ;
$five = $CONF['cut_off_time'];

//echo sprintf('%04d', $parts['year']).":".sprintf('%02d', $parts['mon'])." : ".sprintf('%02d', $parts['mday']);

$intToday = mktime(0, 0, 0, sprintf('%02d', $parts['mon']) ,sprintf('%02d', $parts['mday'])+ 1,sprintf('%04d', $parts['year']));
$timeCheck 			= timeCheck(time(),	$CONF['const_gmt_offset']) ;
$five 				  = $CONF['cut_off_time'];
if($timeCheck >= $five){
$intToday = mktime(0, 0, 0, sprintf('%02d', $parts['mon']) ,sprintf('%02d', $parts['mday'])+ 2,sprintf('%02d', $parts['mday']));
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo $CONF['vir_lib'];?>css/style_noback.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $CONF['vir_lib'];?>/tw/css/fonts.css" charset="utf-8">

<body> 
  
<style>
	
.p-shadow {
  width: 50%;
  float:left;
  background: url(shadowAlpha.png) no-repeat bottom right !important;
  background: url(shadow.gif) no-repeat bottom right;
  margin: 10px 0 0 10px !important;
  margin: 10px 0 0 5px;
  }

.p-shadow div {
  background: none !important;
  background: url(shadow2.gif) no-repeat left top;
  padding: 0 !important;
  padding: 0 6px 6px 0;
  }

.p-shadow p {
  color: #777;
  background-color: #fff;
  font: italic 1em georgia, serif;
  border: 1px solid #a9a9a9;
  padding: 4px;
  margin: -6px 6px 6px -6px !important;
  margin: 0;
  }	
	
	
.img-shadow {
  float:left;
  background: url(shadowAlpha.png) no-repeat bottom right !important;
  background: url(shadow.gif) no-repeat bottom right;
  margin: 10px 0 0 10px !important;
  margin: 10px 0 0 5px;
  }

.img-shadow img {
  display: block;
  position: relative;
  background-color: #fff;
  border: 1px solid #a9a9a9;
  margin: -6px 6px 6px -6px;
  padding: 4px;
  }


</style>
 
<div style="width:670px;">   
     
<table width="660" border="0" cellspacing="0" cellpadding="0">
   <tr>
     <td><img src="images/white_box_small_light/pop_left_top.png" width="8" height="7" /></td>
     <td width="100%" style="background:url(images/white_box_small_light/top_mid.jpg) repeat-x"></td>
     <td><img src="images/white_box_small_light/pop_right_top.png" width="8" height="7" /></td>
   </tr>
   <tr>
     <td style="background:url(images/white_box_small_light/left_mid.jpg) repeat-y">&nbsp;</td>
     <td style="background:#FFFFFF; padding:2px">
     
     <?php
	
	
	if($intCount > 0){
	foreach($data['ads'] as $key => $value){
	$folder_id = (int)($value['adsId']/$CONF['const_photo_per_dir']); 
    $file = PICTURE_PATH.'pictures/sms_ads/'.$folder_id.'/'.$value['adsId'].'/'.$value['strImage'];
    $directoryImage = $CONF['http_photo'  ].'/sms_ads/';
	
	
	
	?>
    
    
     <table width="100%" cellpadding="0" cellspacing="0" border="0" >
      
      <tr>
        <td rowspan="2">&nbsp;</td>        
       	<td colspan="4"><h1 class="pink pdbottom5">Details</h1></td>
      </tr>
      
      <tr>
       <td class="dash" colspan="4">&nbsp;</td>
      </tr>
       
      <tr>
       <td style="width:20px" rowspan="12">&nbsp;</td>       
       <?php
	   if(file_exists($file)&&$value['strImage']!=""){
	   ?>
         
       	<td style="width:230px;" rowspan="3" valign="top"><div class="img-shadow"><img src="<?php echo $directoryImage.'/'.$folder_id.'/'.$value['adsId'].'/'.$value['strImage'];?>" border=0 alt="<?php echo $value['strDescription'];?>" width="200" height="150"></div></td>
		<?php
		}else{	
		?>
        <td style="width:230px;" rowspan="3" valign="top" >
        
        <?php
        $intImage = HTTP_SERVER.'/ads_pic/'.$value['intCategory'].'/1.jpg';
		?>
         <div class="img-shadow"><img src="<?php echo $intImage?>" alt="Category Image"></div>        </td>
        
		<?php }?>       
         
        <td style="width:90px" class="font_TB font11 pink pdbottom5">Code</td>
        <td style="width:300px" class="font_TB font12 pdbottom5">: <?php echo $value['strAdsCode'];?></td>
       </tr>
       
       <tr>
         <td valign="top" class="font_TB font11 pink pdbottom5">Type </td>
         <td valign="top" class="font_TB font12 pdbottom5">: <?php if($value['strPackage']=="Premium" || $value['strPackage']=="premium"){echo "Premium";}?></td>
       </tr>
       
       <tr>
         <td valign="top" class="font_TB font11 pink pdbottom5">Origin</td>
         <td valign="top" class="font_TB font12 pdbottom5">: <?php echo $value['strMedium'];?></td>
       </tr>
       
       <tr>
        <td colspan="3" class="spacer_10">&nbsp;</td>
       </tr>
       
       
       <?php if($value['strPackage']=="premium"){?>
      
       <tr>
         <td valign="top" colspan="3" class="pdbottom10"><span class="font_TB font11 pink" >Location </span>:<span class="font_TB font12"><?php echo $value['NetworkName'];?></span></td>
       </tr>
	   <?php } ?>       
       
       <tr>
         <td class="dash" colspan="3">&nbsp;</td>
       </tr>
       
       <tr >
         <td valign="top" class="pdbottom5"><span class="font_TB font11 pink" >Start Date : </span><span class="font_TB font12" ><?php echo $value['strStartDate'];?></span></td>
         <td valign="top" colspan="2" class="pdbottom5"><span class="font_TB font11 pink" >End Date :</span> <span class="font_TB font12 " ><?php echo $value['strExpiryDate'];?></span></td>
       </tr>
       
       
      <?php if($value['strPackage']=="premium"){?>
     
      <tr>
        <td valign="top" class="pdbottom5"><span class="font_TB font11 pink">Days :</span><span class="font_TB font12 "><?php echo $value['intTotalday'];?></span></td>
        <td valign="top" colspan="2" class="pdbottom5">
		<?php if($value['strPackage']=="premium"){?>
       
        <span class="font_TB font11 pink pdbottom5" >Payment Status : </span><span class="font_TB font12"><?php if($value['strPayment']!="Paid"){ echo "Payment pending";}else{ echo "Paid";}?></span>
              
		<?php } ?>
        </td>
      </tr>	
     <?php } ?>
     
               
     <tr>
       <td valign="top" colspan="3" class="pdbottom5" style="height:100px"><span class="font_TB font11 pink" >Description : </span><span class="font_TB font12" ><?php echo $value['strDescription'];?></span></td>      
     </tr>     
     
     <tr>
      <td colspan="3" class="smaller grey">*all notices online expire after 90 days </td>
     </tr>
     
     
      <?php }}?>
     </table>
   
     
     
     </td>
     <td style="background:url(images/white_box_small_light/right_mid.jpg) repeat-y">&nbsp;</td>
   </tr>
   <tr>
     <td><img src="images/white_box_small_light/pop_left_bottom.png" width="8" height="7" /></td>
     <td style="background:url(images/white_box_small_light/bottom_mid.jpg) repeat-x"></td>
     <td><img src="images/white_box_small_light/pop_right_bottom.png" width="8" height="7" /></td>
   </tr>
  </table>

  
  </div>
  
  
  </body>