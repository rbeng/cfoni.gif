<?php
global $CONF,$Q,$CODE;
$directoryComman = $CONF['vir_lib'].'/common/';

$url_cloudpbx = $CONF['url_app'].'?m=main&c=shw_cloudpbx';
$url_aboutus = $CONF['url_app'].'?m=main&c=shw_about';


?>
<div class="social-icons">
						<ul class="social-icons">
                        
							<li class="facebook"><a href="<?php echo $CONF['url_facebook'];?>" target="_blank" title="Facebook">Facebook</a></li>
							<li class="twitter"><a href="https://twitter.com/cfoni" target="_blank" title="Twitter">Twitter</a></li>

						</ul>
					</div>
					<nav>
						<ul class="nav nav-pills nav-main" id="mainMenu">
						
							<li class="dropdown">
								<a class="dropdown-toggle" href="#">
									Services
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="feature-pricing-tables.php">CloudPABX (Features and Pricing)</a></li>
                                    <li><a href="email_zimbra.php">Email - Zimbra (Pricing and Features)</a></li>
                                    <li><a href="hosted_crm.php">Hosted CRM</a></li>
                                    <li><a href="did_no.php">DID numbers</a></li>
									<li><a href="fax_to_email.php">FAX-to-Email</a></li>
                                    <li><a href="business_sms.php">Business SMS</a></li>
                                    <li><a href="complete_office_communication_package.php">CompleteOffice Communications Packages</a></li>
                                    <li><a href="PowerProfessional_Packages.php">PowerProfessional Packages</a></li>
                                    <li><a href="Customized_Solutions.php">Customized Solutions</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" href="#">
									About us
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo $url_aboutus;?>">About Cfoni</a></li>
                                    <li><a href="privacy_policy.php">Privacy Policy</a></li>
                                    <li><a href="terms_and_service.php">Terms of Service</a></li>
                                    <li><a href="Service_Level_Agreement.php">Service Level Agreement</a></li>
								</ul>
							</li>
                            <?php    if(!session_is_registered(myusername)){ ?>
							<li class="dropdown">
								<a class="dropdown-toggle" href="#">
									Login
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="login-page.php">Login (Management - Customer Portal)</a></li>
								</ul>
							</li>
                            <?php  } ?>
							<li class="dropdown">
								<a class="dropdown-toggle" href="contact-us.php">
									Support
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
                                    <li><a href="page-faq.php">FAQ</a></li>
									<li><a href="how_to.php">How-tos</a></li>
                                    <li><a href="download.php">Downloads</a></li>
                                    <li><a href="osticket/index.php" target="_new">Get Technical Support</a></li>
                         
                                    
                                    
                                    
									<!--<li><a href="contact-us-2.php">Version 2</a></li>-->
								</ul>
							</li>
                            
                            <li class="dropdown">
								<a class="dropdown-toggle" href="contact-us.php">
									Contact
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
                                    <li><a href="contact-us.php">Contact us</a></li>
									
								</ul>
							</li>
						</ul>
