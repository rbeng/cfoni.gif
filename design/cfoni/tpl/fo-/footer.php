<?php 
global $CONF,$Q,$CODE;

$url_contactus = $CONF['url_app'].'?m=main&c=shw_contactus';


?>

<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="footer-ribon">
							<span>Get in Touch</span>
						</div>
						<div class="span3">
							<h4>Newsletter</h4>
							<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>

							<div class="alert alert-success hidden" id="newsletterSuccess">
								<strong>Success!</strong> You've been added to our email list.
							</div>

							<div class="alert alert-error hidden" id="newsletterError"></div>

							<form class="form-inline" id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
								<div class="control-group">
									<div class="input-append">
										<input class="span2" placeholder="Email Address" name="email" id="email" type="text">
										<button class="btn" type="submit">Go!</button>
									</div>
								</div>
							</form>
						</div>
						<div class="span3">
							<h4>Latest Tweet</h4>
							<div id="tweet" class="twitter" data-account-id="crivosthemes">
								<p>Please wait...</p>
							</div>
						</div>
						<div class="span4">
							<div class="contact-details">
								<h4>Contact Us</h4>
								<ul class="contact">
									<li><p><i class="icon-map-marker"></i> <strong>Address:</strong> A-3-1, Northpoint Offices, Medan Syed Putra Utama, No 1 Jalan Syed Putra, Mid Valley City 59200 Kuala Lumpur</p></li>
									<li><p><i class="icon-phone"></i> <strong>Phone:</strong> 603-228406807</p></li>
									<li><p><i class="icon-envelope"></i> <strong>Email:</strong> <a href="mailto:helpdesk@cfoni.com">helpdesk@cfoni.com</a></p></li>
								</ul>
							</div>
						</div>
						<div class="span2">
							<h4>Follow Us</h4>
							<div class="social-icons">
								<ul class="social-icons">
									<li class="facebook"><a href="<?php echo $CONF['url_facebook'];?>" target="_blank" data-placement="bottom" rel="tooltip" title="Facebook">Facebook</a></li>
									<li class="twitter"><a href="https://twitter.com/cfoni" target="_blank" data-placement="bottom" rel="tooltip" title="Twitter">Twitter</a></li>
									<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" data-placement="bottom" rel="tooltip" title="Linkedin">Linkedin</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="span1">
								<a href="<?php echo $CONF['url_app'];?>" class="logo">
									
								</a>
							</div>
							<div class="span7">
								<p>&copy; Copyright <?php echo date('Y'); ?> by CFONI. All Rights Reserved.</p>
							</div>
							<div class="span4">
								<nav id="sub-menu">
									<ul>
										<li><a href="page-faq.php">FAQ's</a></li>
					
										<li><a href="<?php echo $url_contactus;?>">Contact</a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</footer>
            