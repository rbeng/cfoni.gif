<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>techdata product page</title>

<link rel="stylesheet" type="text/css" href="http://127.0.0.1/techdata/design/techdata/scripts/styles.css" />

<script type="text/javascript" src="http://127.0.0.1/techdata/design/techdata/scripts/jquery.min.js"></script>
<script type="text/javascript" src="http://127.0.0.1/techdata/design/techdata/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://127.0.0.1/techdata/design/techdata/scripts/jquery.flip.min.js"></script>

<script type="text/javascript" src="http://127.0.0.1/techdata/design/techdata/scripts/script.js"></script>

</head>

<body>


<?php

// Each sponsor is an element of the $sponsors array:

$sponsors = array(
	array('logo_APC','The biggest social network in the world.','http://www.facebook.com/'),
	array('logo_EasyGuard','The leading software developer targeted at web designers and developers.','http://www.adobe.com/'),
	array('logo_FireEye','One of the top software companies of the world.','http://www.microsoft.com/'),
	array('logo_hp','A global multibillion electronics and entertainment company ','http://www.sony.com/'),
	array('logo_netgear','One of the biggest computer developers and assemblers.','http://www.dell.com/'),
	array('logo_ruckus','The biggest online auction and shopping websites.','http://www.ebay.com/'),
	array('logo_watchGuard','One of the most popular web 2.0 social networks.','http://www.digg.com/')
);


// Randomizing the order of sponsors:

shuffle($sponsors);

?>



<div id="main">
 <div class="sponsorListHolder">		
 <?php			
 // Looping through the array:			
 foreach($sponsors as $company)
 {
 echo'
				
 <div class="sponsor" title="Click to flip" >
  <div class="sponsorFlip">
   <img src="http://127.0.0.1/techdata/design/techdata/images/banner/'.$company[0].'.png" alt="More about '.$company[0].'" width="180" />
  </div>
					
  <div class="sponsorData">
   <div class="sponsorDescription">'.$company[1].'</div>
   <div class="sponsorURL"><a href="'.$company[2].'"><img src="http://127.0.0.1/techdata/design/techdata/images/icon_more_detail.png" alt="'.$company[0].'" border="0" /></a></div>
  </div>					
 </div>			
				
 ';
 }
		
 ?>     
    	
 </div> 

</div>


</body>
</html>
