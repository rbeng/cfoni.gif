<?php
global $CONF,$Q;
?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.html">Home</a> <span class="divider">/</span></li>
									<li class="active">About Us</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>About Us</h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h2>About <strong>Cfoni</strong></h2>

					<div class="row">
						<div class="span10">
							<p class="lead">
							Cfoni Technologies Sdn Bhd has been established to provide a collaboration of Telephony Service, Email, and Customer Relationship Management Services, Software As A Service (SaaS) platform that enable business to extend their applications and information to any wireless device. The firm's vision is to assist organizations in reforming the ways businesses are conducted in the increasingly complex and competitive global K-Economy environment and to help organizations realize their business goals in maximizing productivity and efficiency. Cfoni focuses strategically in Telephony Service, Email and Customer Relationship Management services in providing a range of services. The firm not only extend a range of products and value-added services to the private organizations but also plans in developing a total Cloud Mobile Applications Solution to suit the Local and Government entities, bank and financial institutions which will be our key focus in providing the service and to tackle the operational challenges ahead. Cfoni strives to become the leader in product and services innovation to transfer the technology towards the society, by creating world-class intellectual added-values in the solutions design and applications, in line with our vision and mission statements.</p>
						</div>
						<div class="span2">
							<a href="#" class="btn btn-large btn-primary pull-top">Join Our Team!</a>
						</div>
					</div>

					<hr class="tall">

					<div class="row">
						<div class="span8">
							<h3><strong>Who</strong> We Are</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Nullam convallis, arcu vel pellentesque sodales, nisi est varius diam, ac ultrices sem ante quis sem. Proin ultricies volutpat sapien, nec scelerisque ligula mollis lobortis.</p>
							<p>Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing <span class="alternative-font">metus</span> sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula.</p>
						</div>
						<div class="span4">
							<div class="featured-box featured-box-secundary">
								<div class="box-content">
									<h4>Behind the scenes</h4>
									<ul class="flickr-feed" data-plugin-options='{"qstrings": { "id": "93691989@N03" }}'></ul>
								</div>
							</div>
						</div>
					</div>

					<hr class="tall">

					<div class="row">
						<div class="span12">
							<h3 class="pull-top">Our <strong>History</strong></h3>
						</div>
					</div>

					<div class="row">
						<div class="span12">

							<ul class="timeline">
								<li data-appear-animation="fadeInUp">
									<div class="thumb" data-appear-animation="fadeInRight">
										<img src="img/office-4.jpg" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<h4><strong>2012</strong></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>
										</div>
									</div>
								</li>
								<li data-appear-animation="fadeInUp">
									<div class="thumb" data-appear-animation="fadeInRight">
										<img src="img/office-3.jpg" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<h4><strong>2010</strong></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia.</p>
										</div>
									</div>
								</li>
								<li data-appear-animation="fadeInUp">
									<div class="thumb" data-appear-animation="fadeInRight">
										<img src="img/office-2.jpg" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<h4><strong>2005</strong></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>
										</div>
									</div>
								</li>
								<li data-appear-animation="fadeInUp">
									<div class="thumb" data-appear-animation="fadeInRight">
										<img src="img/office-1.jpg" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<h4><strong>2000</strong></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>
										</div>
									</div>
								</li>
							</ul>

						</div>
					</div>

				</div>
