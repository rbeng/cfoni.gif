<?php
global $CONF,$Q;
$http_dir = HTTP_SERVER.'design/'.$CONF['tpl_name'].'/';
?>
<div id="content" class="content full">

					<div class="slider-container">
						<div class="slider" id="revolutionSlider">
							<ul>
								<li data-transition="pop" data-slotamount="10" data-masterspeed="300">
									<img src="<?php echo $http_dir; ?>img/slides/slide_1.jpg" data-fullwidthcentering="on" alt="">

										<div class="caption sft stb visible-desktop"
											 data-x="42"
											 data-y="180"
											 data-speed="300"
											 data-start="1000"
											 data-easing="easeOutExpo"></div>

										<div class="caption top-label lfl stl"
											 data-x="92"
											 data-y="180"
											 data-speed="300"
											 data-start="500"
											 data-easing="easeOutExpo"></div>

										<div class="caption sft stb visible-desktop"
											 data-x="342"
											 data-y="180"
											 data-speed="300"
											 data-start="1000"
											 data-easing="easeOutExpo"></div>

										<div class="caption main-label sft stb"
											 data-x="0"
											 data-y="230"
											 data-speed="300"
											 data-start="1500"
											 data-easing="easeOutExpo"></div>

										<div class="caption bottom-label sft stb"
											 data-x="50"
											 data-y="280"
											 data-speed="500"
											 data-start="2000"
											 data-easing="easeOutExpo"></div>

										<div class="caption randomrotate"
											 data-x="800"
											 data-y="250"
											 data-speed="500"
											 data-start="2500"
											 data-easing="easeOutBack"></div>

										<div class="caption sfb"
											 data-x="850"
											 data-y="200"
											 data-speed="400"
											 data-start="3000"
											 data-easing="easeOutBack"></div>

										<div class="caption sfb"
											 data-x="820"
											 data-y="170"
											 data-speed="700"
											 data-start="3150"
											 data-easing="easeOutBack"></div>

										<div class="caption sfb"
											 data-x="770"
											 data-y="130"
											 data-speed="1000"
											 data-start="3250"
											 data-easing="easeOutBack"></div>

										<div class="caption sfb"
											 data-x="500"
											 data-y="80"
											 data-speed="600"
											 data-start="3450"
											 data-easing="easeOutExpo"></div>

										<div class="caption blackboard-text lfb "
											 data-x="530"
											 data-y="300"
											 data-speed="500"
											 data-start="3450"
											 data-easing="easeOutExpo" style="font-size: 37px;"></div>

										<div class="caption blackboard-text lfb "
											 data-x="555"
											 data-y="350"
											 data-speed="500"
											 data-start="3650"
											 data-easing="easeOutExpo" style="font-size: 47px;"></div>

										<div class="caption blackboard-text lfb "
											 data-x="580"
											 data-y="400"
											 data-speed="500"
											 data-start="3850"
											 data-easing="easeOutExpo" style="font-size: 32px;"></div>
								</li>
								<li data-transition="pop" data-slotamount="10" data-masterspeed="300">
									<img src="<?php echo $http_dir; ?>img/slides/slide_2.jpg" data-fullwidthcentering="on" alt="">

										<div class="caption fade"
											 data-x="50"
											 data-y="100"
											 data-speed="2500"
											 data-start="200"
											 data-easing="easeOutExpo"></div>

										<div class="caption blackboard-text fade "
											 data-x="180"
											 data-y="180"
											 data-speed="1500"
											 data-start="1000"
											 data-easing="easeOutExpo" style="font-size: 30px;"></div>

										<div class="caption blackboard-text fade "
											 data-x="180"
											 data-y="220"
											 data-speed="1500"
											 data-start="1200"
											 data-easing="easeOutExpo" style="font-size: 40px;"></div>

										<div class="caption main-label sft stb"
											 data-x="580"
											 data-y="200"
											 data-speed="300"
											 data-start="1500"
											 data-easing="easeOutExpo"></div>

										<div class="caption bottom-label sft stb"
											 data-x="580"
											 data-y="250"
											 data-speed="500"
											 data-start="2000"
											 data-easing="easeOutExpo"></div>

								</li>
							</ul>
						</div>
					</div>

					<div class="home-intro">
						<div class="container">

							<div class="row">
								<div class="span8">
									<p>
										The fastest way to grow your business with the leader in <em>Technology.</em>
										<span>Check out our options and features included.</span>
									</p>
								</div>
								<div class="span4">
									<div class="get-started">
										<a href="#" class="btn btn-large btn-primary">Get Started Now!</a>
										<div class="learn-more">or <a href="index.php">learn more.</a></div>
									</div>
								</div>
							</div>

						</div>
					</div>

					<div class="container">

						<div class="row center">
							<div class="span12">
								<h2 class="short"><strong class="inverted">incredibly</strong> Cfoni Technologies Sdn Bhd.</h2>
								<p class="featured lead">
									Company Overview

To its core business, the company intends to launch cloud-based IP-PABX (Private Automated Branch Exchange) Service, bundled with Corporate Email service and Customer Relationship Management Application services. The services will allow SMEs and SOHO customers to utilize a one-stop solution center for Telephony Service, Email Collaboration, and Customer Relationship Management services under one umbrella with support for integration of the different solutions. The customers would also be able to get additional Value Added Services like Inbound DID numbers instantly without waiting for a telephone company to setup physical lines, or utilize FAX services without purchasing a FAX machine.</div>

					</div>

					<div class="home-concept">
						<div class="container">

							<div class="row center">
								<span class="sun"></span>
								<span class="cloud"></span>
								<div class="span2 offset1">
									<div class="process-image" data-appear-animation="bounceIn">
										<img src="<?php echo $http_dir; ?>img/strategy.jpg" alt="" />
										<strong>Strategy</strong>
									</div>
								</div>
								<div class="span2">
									<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="200">
										<img src="<?php echo $http_dir; ?>img/planning.jpg" alt="" />
										<strong>Planning</strong>
									</div>
								</div>
								<div class="span2">
									<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="400">
										<img src="<?php echo $http_dir; ?>img/build.jpg" alt="" />
										<strong>Build</strong>
									</div>
								</div>
								<div class="span4 offset1">
									<div class="project-image">
										<div id="fcSlideshow" class="fc-slideshow">
											<ul class="fc-slides">
												<li><a href="portfolio-single-project.php"><img src="<?php echo $http_dir; ?>img/work.jpg" /></a></li>
												<li><a href="portfolio-single-project.php"><img src="<?php echo $http_dir; ?>img/work.jpg" /></a></li>
												<li><a href="portfolio-single-project.php"><img src="<?php echo $http_dir; ?>img/work.jpg" /></a></li>
											</ul>
										</div>
										<strong class="our-work">Collaborate</strong>
									</div>
								</div>
							</div>

							<hr class="tall" />

						</div>
					</div>

					<div class="container">

						<div class="row">
							<div class="span8">
								<h2>Our <strong>Product</strong></h2>
								<div class="row">
									<div class="span4">
										<div class="feature-box">
											<div class="feature-box-icon">
												<i class="icon-group"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="shorter">PBXWare</h4>
												<p class="tall">Enterprise VOIP solution.</p>
											</div>
										</div>
										<div class="feature-box">
											<div class="feature-box-icon">
												<i class="icon-file"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="shorter">HRM</h4>
												<p class="tall">Syncronize your calendar activities,daily tasks and many more</p>
											</div>
										</div>
										<div class="feature-box">
											<div class="feature-box-icon">
												<i class="icon-google-plus"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="shorter">ENTERPRISE WEBMAIL Solution</h4>
												<p class="tall">Using enterprise webmail with domain to boost your business strategy</p>
											</div>
										</div>
										<div class="feature-box">
											<div class="feature-box-icon">
												<i class="icon-adjust"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="shorter">DIDs</h4>
												<p class="tall">Using your own office phone number .</p>
											</div>
										</div>
									</div>
									<div class="span4">
										<div class="feature-box">
											<div class="feature-box-icon">
												<i class="icon-film"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="shorter">Voicemail</h4>
												<p class="tall">record your customer order,complaint 24 hours per day .</p>
											</div>
										</div>
										<div class="feature-box">
											<div class="feature-box-icon">
												<i class="image-icon small user"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="shorter">Channel</h4>
												<p class="tall">Multi extension services.</p>
											</div>
										</div>
										<div class="feature-box">
											<div class="feature-box-icon">
												<i class="icon-reorder"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="shorter">Call Forwarding</h4>
												<p class="tall">Forward your office phone number to your mobile.</p>
											</div>
										</div>
										<div class="feature-box">
											<div class="feature-box-icon">
												<i class="icon-desktop"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="shorter">Customer Support</h4>
												<p class="tall">Lorem sit amet, consectetur adip.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="span4">
								<h2>and more...</h2>
								<div class="accordion" id="accordion">
									<div class="accordion-group">
										<div class="accordion-heading">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="icon-lightbulb"></i>Package Basic</a>
										</div>
										<div id="collapseOne" class="accordion-body collapse in">
											<div class="accordion-inner">
												Containing 1 DID ,1 voicemail, 1 call forwarding,1 enterprise email account,1 HRM account
											</div>
										</div>
									</div>
									<div class="accordion-group">
										<div class="accordion-heading">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><i class="icon-bell-alt"></i> Package Medium</a>
										</div>
										<div id="collapseTwo" class="accordion-body collapse">
											<div class="accordion-inner">
												Containing 5 DID ,15 voicemail,5 call forwarding,5 enterprise email account,5 HRM account
											</div>
										</div>
									</div>
									<div class="accordion-group">
										<div class="accordion-heading">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><i class="icon-laptop"></i> Package Enterprise</a>
										</div>
										<div id="collapseThree" class="accordion-body collapse">
											<div class="accordion-inner">
												Containing 10 DID ,10 voicemail, 10 call forwarding,10 enterprise email account,10 HRM account
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<hr class="tall" />
</li>
													
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			
           