<?php
global $CONF,$Q,$CODE;
$directoryComman = $CONF['vir_lib'].'/common/';

$url_cloudpbx = $CONF['url_app'].'?m=main&c=shw_cloudpbx';
$url_emailzimbra = $CONF['url_app'].'?m=main&c=shw_emailzimbra';
$url_hosted_crm = $CONF['url_app'].'?m=main&c=shw_hosted_crm';
$url_didnumbers = $CONF['url_app'].'?m=main&c=shw_didnumbers';
$url_faxemail = $CONF['url_app'].'?m=main&c=shw_faxemail';
$url_customizedsolutions = $CONF['url_app'].'?m=main&c=shw_customizedsolutions';
$url_conference = $CONF['url_app'].'?m=main&c=shw_conference';
$url_callforwarding = $CONF['url_app'].'?m=main&c=shw_callforwarding';
$url_pagefaq = $CONF['url_app'].'?m=main&c=shw_pagefaq';
$url_howto = $CONF['url_app'].'?m=main&c=shw_howto';
$url_download = $CONF['url_app'].'?m=main&c=shw_download';
$url_aboutus = $CONF['url_app'].'?m=main&c=shw_about';
$url_privacypolicy = $CONF['url_app'].'?m=main&c=shw_privacypolicy';
$url_terms_and_services = $CONF['url_app'].'?m=main&c=shw_terms_and_services';
$url_service_level_agreement = $CONF['url_app'].'?m=main&c=shw_service_level_agreement';

$url_contactus= $CONF['url_app'].'?m=main&c=shw_contactus';


//
// Call the Package
//
$this->objpackage = new core_main();
$this->objpackage->doGetPackageActive();
while($this->objpackage->GetNextRecord()){
 $arrPackage[$this->objpackage->Get('intPackageId')] = $this->objpackage->Get('strPackageName'); 	
}


//
// Call the Product
//
$this->objroduct = new core_main();
$this->objroduct->doGetProductActive();
while($this->objroduct->GetNextRecord()){
 $arrProduct[$this->objroduct->Get('intProductId')] = $this->objroduct->Get('strProductName'); 	
}


?>
<div class="social-icons">
						<ul class="social-icons">
                        
							<li class="facebook"><a href="<?php echo $CONF['url_facebook'];?>" target="_blank" title="Facebook">Facebook</a></li>
							<li class="twitter"><a href="https://twitter.com/cfoni" target="_blank" title="Twitter">Twitter</a></li>

						</ul>
					</div>
					<nav>
						<ul class="nav nav-pills nav-main" id="mainMenu">
						<?php
							$cProduct = count($arrProduct);
							if($cProduct > 0){
								foreach($arrProduct as $kP => $productname){
							?>
							<li class="dropdown">
								<a class="dropdown-toggle" href="#">
								Products
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
								<li><a href="<?php echo $CONF['url_app'];?>?m=main&c=shw_product_byid&id=<?php echo encrypt($kP,'cfoni.282828');?>"><?php echo $productname;?> </a></li>
                                
                              <!--  <li><a href="<?php echo $url_cloudpbx;?>">PBX Cloud </a></li>  
                                 <li><a href="<?php echo $url_emailzimbra;?>">emailzimbra </a></li>   
                                    <li><a href="<?php echo $url_hosted_crm;?>">Customer Relationship Management</a></li>
                                    <li><a href="<?php echo $url_didnumbers;?>">DID numbers</a></li>
									<li><a href="<?php echo $url_faxemail;?>">FAX-to-Email</a></li>
								 <li><a href="<?php echo $url_customizedsolutions;?>">Customized Solutions</a></li>
								 <li><a href="<?php echo $url_conference;?>">Conferencing </a></li>
								 <li><a href="<?php echo $url_callforwarding;?>">call forwarding </a></li>-->
								 								</ul>
							</li>
                            <?php
								}
							}
							?>
                            <?php
							$cPackage = count($arrPackage);
							if($cPackage > 0){
								foreach($arrPackage as $k => $packagename){
							?>
                            
                            <li class="dropdown">
								<a class="dropdown-toggle" href="#">
								PLANS
									<i class="icon-angle-down"></i>
								</a>
                                
                                
                                <ul class="dropdown-menu">
                                   <li><a href="<?php echo $CONF['url_app'];?>?m=main&c=shw_package&id=<?php echo encrypt($k,'cfoni.282828');?>"><?php echo $packagename;?></a></li>
									<!--<li><a href="micro_enterprise.php">Micro Enterprise</a></li>
                                    <li><a href="small_enterprise.php">Small Enterprise</a></li>
                                    <li><a href="medium_enterprise.php">Medium Enterprise</a></li>
                                    <li><a href="large_enterprise.php">Large Enterprise</a></li>-->
								</ul>
                                
                                </li>
                                
                                <?php
                                 }
							}
								
								?>
                                
                                <?php    
								if($Q->cookies['account:strUser']==""){ ?>
							<li class="dropdown">
								<a class="dropdown-toggle" href="#">
									Login 
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo $CONF['url_app']?>?m=main&c=shw_login">Login (Management - Customer Portal)</a></li>
								</ul>
							</li>
                            <?php  } ?>
                                
							
							<li class="dropdown">
								<a class="dropdown-toggle" href="#">
									Support
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
                                    <li><a href="<?php echo $url_pagefaq;?>">FAQ</a></li>
									 <li><a href="<?php echo $url_howto;?>">How-tos</a></li>
                                    <li><a href="<?php echo $url_download;?>">Downloads</a></li>
                                    <li><a href="osticket/index.php" target="_new">Get Technical Support</a></li>
                         
                                    
                                    
                                    
									<!--<li><a href="contact-us-2.php">Version 2</a></li>-->
								</ul>
							</li>
                            <li class="dropdown">
								<a class="dropdown-toggle" href="#">
									About us
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo $url_aboutus;?>">About Cfoni</a></li>
									<li><a href="<?php echo $url_privacypolicy;?>">Privacy Policy</a></li>
                                   <li><a href="<?php echo $url_terms_and_services;?>">Terms of Service</a></li>
                                   <li><a href="<?php echo $url_service_level_agreement;?>">Service Level Agreement</a></li>
								</ul>
							</li>
                            
                            
                            <li class="dropdown">
								<a class="dropdown-toggle" href="contact-us.php">
									Contact
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
                                    <li><a href="<?php echo $url_contactus;?>">Contact us</a></li>
									<!--<li><a href="chat/index.php">Chat (LiveHelperChat)</a></li>-->
                                  
                                    
                                    
									<!--<li><a href="contact-us-2.php">Version 2</a></li>-->
								</ul>
							</li>
						</ul>
					</nav>