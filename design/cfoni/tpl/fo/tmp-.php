<?php
global $CONF;

$http_dir = HTTP_SERVER.'design/'.$CONF['tpl_name'].'/';






?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to Techdata</title>
<link rel="stylesheet" href="<?php echo $http_dir; ?>css/style.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo $http_dir; ?>css/font.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo $http_dir; ?>css/form.css" type="text/css" media="all">
<link rel="shortcut icon" href="<?php echo HTTP_SERVER;?>/images/logoicon.ico" type="image/x-icon" >
<!--slide menu-->
<link rel="stylesheet" href="<?php echo $http_dir; ?>css/menu.css" type="text/css" media="all">

<script type="text/javascript" src="<?php echo $http_dir; ?>js/jquery.min.js"></script>
<script type='text/javascript' src='<?php echo $http_dir; ?>js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='<?php echo $http_dir; ?>js/jquery.dcmegamenu.1.2.js'></script>
<script type="text/javascript">
$(document).ready(function($){
	$('#mega-menu').dcMegaMenu({
		rowItems: '5',
		speed: 'fast',
		effect: 'slide'
	});
});
</script>
<!--end slide menu-->


<!--slide banner-->
<link href="<?php echo $http_dir; ?>css/slides.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $http_dir; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $http_dir; ?>js/scripts.js"></script>
<!--end slide banner-->

<!--jco-->

<!--[if lt IE 9]>
<script src="/js/html5shiv.js"></script>
<![endif]-->	
<script type="text/javascript" src="<?php echo $http_dir; ?>scripts/jquery.min.js"></script>
<!--<script type='text/javascript' src='<?php echo $http_dir; ?>jco/jquery.min.js'></script>-->
<script type='text/javascript' src="<?php echo $http_dir; ?>jco/jquery.bxslider.js"></script>
<script type='text/javascript' src="<?php echo $http_dir; ?>jco/rainbow.min.js"></script>
<script type='text/javascript' src="<?php echo $http_dir; ?>jco/bxsliderscripts.js"></script>
<!--end jco-->

<script type="text/javascript" src="<?php echo $http_dir; ?>js/crawler.js">
/* Text and/or Image Crawler Script v1.5 (c)2009-2011 John Davenport Scheuer
   as first seen in http://www.dynamicdrive.com/forums/
   username: jscheuer1 - This Notice Must Remain for Legal Use
   updated: 4/2011 for random order option, more (see below)
*/

</script> 
<script type="text/javascript" src="<?php echo $http_dir; ?>js/marquee.min.js"></script>

<script type="text/javascript">
<!--
window.onload = function()
{
	var marquee1 = new Marquee({id:"marquee1"}).init();
};
// -->
</script>

<script>
$(document).ready(function(){

	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

});
</script>

<style>


#back-top {
	position: fixed;
	bottom: 30px;
	margin-left: 990px;
}
#back-top a {
	width: 108px;
	display: block;
	text-align: center;
	font: 11px/100% Arial, Helvetica, sans-serif;
	text-transform: uppercase;
	text-decoration: none;
	color: #bbb;
	/* background color transition */
	-webkit-transition: 1s;
	-moz-transition: 1s;
	transition: 1s;
}
#back-top a:hover {
	color: #000;
}
/* arrow icon (span tag) */
#back-top span {
	width: 108px;
	height: 108px;
	display: block;
	margin-bottom: 7px;
	background: #ddd url(<?php echo $http_dir;?>/images/up-arrow.png) no-repeat center center;
	/* rounded corners */
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	/* background color transition */
	-webkit-transition: 1s;
	-moz-transition: 1s;
	transition: 1s;
}
#back-top a:hover span {
	background-color: #777;
}

</style>
<script>
  (function() {
    var cx = '001691877294436549166:ljmseun13xy';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>

</head>

<body>


<div id="wrapper">
 
 <div style="position:fixed; top:0;right:0; margin-top:150px"><img src="<?php echo $http_dir;?>images/content_icon.png" width="120" height="326" /></div> 
 <div id="page">  
  <div id="header-top">   
   <div id="logo"><img src="<?php echo $http_dir; ?>images/logo.png" width="200" height="65" /></div>     
   <div id="nav"><?php include ('navigation.php'); ?></div>  
  </div><!--header-top--> 
 

   
  <div id="message-line">  

  <div id="marquee1" style="white-space:nowrap">
   <h3>
<?php  
//
// Get Msg Link
//
$this->objmsg = new core_main();
$this->objmsg->doGetMsg();

  while($this->objmsg->GetNextRecord()){
 ?> 
 <a href="<?php echo $this->objmsg->Get('strLink');?>" target="_blank"><?php echo $this->objmsg->Get('strMessage');?></a> 
  <?php
  }
  ?>
  </h3> 
  
  
  
  </div>
  
  
  </div>

<gcse:search></gcse:search>








  <?php
  //
  // Content
  //
  if(isset($include_file['main'])) {
  // main only
  $file_str = $include_file['main'];
  if (substr($file_str, (strlen($file_str) - 3), 3) == 'php') {
  include($file_str);
  } 
  else {
  }
  } 
  else {
  trigger_error("Attempt to load template with non PHP files",E_USER_ERROR);
  }
  ?>
 
<?php
if($Q->req['c']!="" && $Q->req['c']!="shw_main" && $Q->req['c']!="show_main"){
?>

  <p id="back-top">
		<a href="#top" class="back-top"><span></span>Back to Top</a>
	</p>
  </div><!--page-->	
<?php
}
?>
		
 <?php include('footer.php');?>
 
</div><!--wrapper-->

</body>
</html>