<?php
global $CONF,$Q,$CODE;

?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">Privacy and policy </li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>Privacy and policy </h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h3><strong>Privacy and policy</strong></h3>

					<div class="row">
						<div class="span12">
					
					<p> Cfoni.com respects the privacy of its customers and takes its obligations to protect the privacy of its customers seriously. This policy briefly describes the personally identifiable information that Cfoni.com collects from its customers and prospective customers and the steps that it takes to safeguard such information.</p>		
                  <h5>Ordering</h5>
                   <p> When taking orders online, Cfoni.com will request certain information from a customer to complete an order. The customer's information will include name, address, telephone number, e-mail address, payment information and other information as may be requested by Cfoni.com in the order form. This information is used for billing purposes, fulfillment of orders, ongoing account maintenance and upgrades. </p>
                  <h5>Security </h5>
                  <p>When customers are asked to enter certain sensitive information (such as credit card number and/or social security number), that information is encrypted and is protected through a Secure Socket Layer (SSL). SSL negotiates and employs the essential functions of mutual authentication, data encryption and data integrity for secure transactions.  </p>
                  <p>All customer/visitor information, not just the sensitive information mentioned above, is restricted to Cfoni.com developers, network operations personnel and other qualified employees (such as billing clerks or customer care representatives). Finally, the servers on which Cfoni.com stores personally identifiable information are kept in a secure location. 
</p>
<h5>Notification of Changes</h5>

<p>If we make any changes to the Cfoni.com Privacy Policy, we will post those changes on Cfoni.com web sites so that customers/visitors are informed. We will always use information in accordance with the Cfoni.com Privacy Policy.
</p>
<h5>Questions</h5>
<p> Any questions or comments regarding privacy should be sent to helpdesk@cfoni.com.</p>


</div>
					
					

					

							
							</div>

						</div>

					</div>

				</div>
