<?php
global $CONF,$Q,$CODE;

?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">How It Works</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>How It Works</h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<!--[if lt IE 10]>
					<div class="alert">
						<strong>Warning!</strong> Animations are not compatible with old Internet Explorer versions.
					</div>
					<![endif]-->

					<h2>Receiving Calls</h2>

					<div class="row">
						<div class="span12">

							<img class="responsive appear-animation pull-left"<img src="<?php echo $CONF['url_pic_direct']; ?>img/device.png" data-appear-animation="fadeIn">
							<h4>1.Callers dial your company's phone Number</h4>
							<p> You can keep your existing number or get a new one from
Virtual PBX, a top VoIP provider. Local, toll-free, and international numbers are
all available. You can have multiple numbers, and use a different
greeting on each one.</p><br><br><br><br><br><br>
                         	<hr>

							<img class="responsive appear-animation pull-left" <img src="<?php echo $CONF['url_pic_direct']; ?>img/device.png" data-appear-animation="fadeInDown">
							<h4>2.Callers are greeted by a custom auto-attendant</h4>
							<p>The greeting usually includes menu options for dialing departments and/or individuals. You can also have one of your employees answer as a live operator. Greetings and system operation can be scheduled to match your business hours.
</p>
                            <br><br><br><br><br><br>
                            	<hr>

							<img class="responsive appear-animation pull-left"<img src="<?php echo $CONF['url_pic_direct']; ?>img/device.png" data-appear-animation="fadeInLeft">
							<h4>3.Virtual PBX manages call routing through your company</h4>
							<p>Callers select the person or department they want to reacha and Virtual PBX will route the call to the selected person or group. Sophisticated queuing options reduces holding times and improve efficiency.
</p>
							<br><br><br><br><br><br>
                            <hr>

							

							<img class="responsive pull-left" <img src="<?php echo $CONF['url_pic_direct']; ?>img/device.png" data-appear-animation="wiggle">
							<h4>4.Employees can work anywhere</h4>
							<p>Calls can go to any kind of phone, in any location in the world. Use a VoIP PBX to stop paying for expensive land lines and get more features Voice and fax messages are available online or as email attachments to your inbox.</p>
					 <br><br><br><br><br><br>
                            <hr>
                      </div>
				  </div>
                  
                  <h2>Making Calls</h2>

					<div class="row">
						<div class="span12">
                      
                      
                      <img class="responsive appear-animation pull-left" <img src="<?php echo $CONF['url_pic_direct']; ?>img/device.png" data-appear-animation="fadeInLeft">
							<h4>Using a Virtual PBX VoIP phone</h4>
							<p>Just pick up the VoIP phone and dial.
No long-distance charges.
Direct dial extension-to-extension.</p>
                           <hr>

                      </div>
				  </div>    

			  </div>

			</div>