<?php
global $CONF,$Q;

$text = generateCode();

if($Q->req['msg']==1){
	?>
	<script>
	alert('Thanks for joining our campaign.');
	location.href="?m=main&c=shw_campaign";
	</script>
	
	<?php
	}

?>
<link rel="stylesheet" href="<?php echo $http_dir; ?>jco/jquery.produtslider.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $http_dir; ?>jco/github.css" type="text/css" />	
<link href="<?php echo $http_dir; ?>css/site.css" rel="stylesheet">
<script src="<?php echo $http_dir; ?>js-1/site.js"></script> 
<script type="text/javascript" src="<?php echo $http_dir; ?>js/valid.js"></script>
 
 
<div id="banner-full">
 <?php include(DIR_TEMP_PATH_FO.'/brandlogo.php');?>
</div>
 
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>design/<?php echo $CONF['tpl_name'];?>/ajaxsubmit/jquery.form.js"></script>
<script type="text/javascript">
function validForm(){
$("#submitBtn").hide();
$("#flashShow").show();
$("#flashShow").html('<img src="images/loading.gif" class="marL114"  border="0" />');
var options ={success: function(data)
{
$("#flashShow").hide();
$("#submitBtn").show();
if(data=='done'){
//$("#formClass").hide();  // If you dont wana hide the form than only reset using jquery
// $("#responseMessage").html('Thank you for submitting your contact information<br/>');
					
location.href="index.php?m=main&c=shw_campaign&msg=1";					
}else{
$("#responseMessage").html(data);
}
return false;
}};
$("#frmRegistration").ajaxForm(options).submit();
		
}
</script>
 
 

 
 <div class="product-content" style="background:url('design/<?php echo $CONF['tpl_name'];?>/images/line.png') left top repeat-y;">  
  
  <?php
  if($data['campaign']['strTitle']==""){
  ?>
  <div id="banner-about" class="right" >
   <div id="main-content" style="padding-right:400px;">
    <h1> No campaign at the moment... </h1>
   </div>
  </div>
 </div>
  <?php  
  }
  else{
  ?>
      
  <div id="banner-about" class="right" >
    
   <div class="campaignTitle"><?php if($data['campaign']['strTitle']!="") { echo $data['campaign']['strTitle'] ; }?></div>
   
   <div class="dotline">&nbsp;</div>
   
   <div id="banner-campaign">
   
    <?php
    if($data['campaign']['strPic']!=""){
	 $folder_id = (int)($data['campaign']['intCampaignId']/5000);
	 $image =  $CONF['http_photo'].'/campaignphoto/'.$folder_id.'/'.$data['campaign']['intCampaignId'].'/'.$data['campaign']['strPic']; 
	 ?>
     <img src="<?php echo $image;?>" alt="<?php echo $data['campaign']['strTitle']  ;?>"  width=950/><?php
	 }
	 ?>
    </div>    
    
    
    </div>
   
    <br class="clear"   />
    <form method="post" action="<?php echo $CONF['url_app'];?>?m=main&c=do_addCampaign" name="frmRegistration" id="frmRegistration">
    <input type="hidden" value="" name="strSecureBotA"/>
    <input  type="hidden" name="intCampaignId" value="<?php echo $data['campaign']['intCampaignId']; ?>" />
          
   
    <div id="main-content" >
   
    <br />
    
    <div id="formClass">
    <?php
    $cQ = count($data['campaign_q']);
    ?>
    
    <input type="hidden" name="intTotalq" value="<?php echo $cQ;?>"><?php
	$index = 1;
	if($cQ > 0){
	foreach($data['campaign_q'] as $intQId => $Q){
	?>
    
     <div id="question"><?php echo $index." . ". $Q;?></div>
     
     
     <div id="answer">
      <?php
       foreach($data['answer_select'][$intQId] as $key => $answer){
	    ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="answer[<?php echo $intQId ?>]" value="<?php echo $key  ?>"/><?php echo $answer;?> &nbsp;&nbsp;&nbsp; <?php
		
	   }
	  ?>
     
     </div>
     <br />
    <?php
	 $index++;
      }
	 }
	
	?> 
     
     <div class="dotline clear">&nbsp;</div>
     <h1>Customer Details</h1>
    
     <br />
   
       <input type="hidden" value="" name="strSecureBot"/>
         <span id="responseMessage"></span>
     
       <ul class="info-equiry">          
        <li>
         <label class="bookLabel">Name <span class="red">*</span></label>
         <br />  <div id="firmname_msg"><font style="color:#0F4F7C">Max 255 characters (A-Z, a-z, 0-9).</font></div>
         <input type="text" name="firmname" maxlength="255" class="booktextfield" size="55" value="<?php echo $Q->req['firmname'];?>" onBlur="valid_firmname()">         
        </li>
  
        <li>
         <label class="bookLabel">Company Name</label> 
         <br />  <div id="firmname_msg"><font style="color:#0F4F7C">Max 255 characters (A-Z, a-z, 0-9).</font></div>     
          <input type="text" name="strCompany" maxlength="255" class="booktextfield" size="55" value="<?php echo $Q->req['firmname'];?>" onBlur="valid_firmname()">         
        </li> 
        
        <li>
         <label class="bookLabel">Job Title</label> 
            
          <input type="text" name="strJob" maxlength="255" class="booktextfield" size="55" value="<?php echo $Q->req['strJob'];?>">         
        </li>
               
        <li>
         <label class="bookLabel">Email<span class="red">*</span></label>
         <br /><div id="mail_msg"><font style="color:#0F4F7C">Max 50 Characters.<br> Please enter valid Email Address eg. abc@xxxx.xxx</font></div>
          <input type="text" name="mail" maxlength="100" class="booktextfield" size="55" value="<?php echo $Q->req['mail'];?>" onBlur="valid_mail()">          
        </li>
         
        <li>
         <label class="bookLabel">Phone No</label>  <br />
         
         <input type="text" name="phone" maxlength="100" class="booktextfield" size="55" value="">   
        </li>    
        
      <li>
     <label class="bookLabel">Security Code </label>  <br />
     <div id="security_msg"><font style="color:#0F4F7C">Enter the number shown on the left.</font></div>
       
    <input type="text" name="security" class="booktextfield"  maxlength="30" size="30" onblur="valid_security()">    
	<input type="hidden" name="securityhidden" value="<?php echo $text;?>" onblur="valid_security()">	
     <img src="<?php echo $CONF['url_app_root'];?>CaptchaSecurityImages.php?width=100&height=30&characters=5&codehidden=<?php echo $text;?>" align="absbottom">
          
    </li> 
        <div>
      
        <p>
                           <span id="submitBtn">
                               <button type="button" name="submitForm" value="Send" class="btn marL114" onclick="return validForm()">Send</button>
                            
                           </span>
                           <span id="flashShow"></span>
	              
        
        </p>
      </ul> 
  
      </form>    
     
     </div>
    </div><!--main-content-->  
 </div><!--product-content-->
 <?php
 }
 
 ?>

 </div>