<?php
global $CONF,$Q,$CODE;

?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">Features</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>Customized solutions</h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<!--[if lt IE 10]>
					<div class="alert">
						<strong>Warning!</strong> Animations are not compatible with old Internet Explorer versions.
					</div>
					<![endif]-->

					<h2>Customized solutions</h2>

<div class="row">
						<div class="span6">

							<div class="" >
<img src="<?php echo $CONF['url_pic_direct']; ?>img/projects/cuz.jpg" height="365" width="550">
									</li>
								
							</div><br><br>
							

							<!-- AddThis Button BEGIN -->
							<div class="addthis_toolbox addthis_default_style ">
								<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
								<a class="addthis_button_tweet"></a>
								<a class="addthis_button_pinterest_pinit"></a>
								<a class="addthis_counter addthis_pill_style"></a>
							</div>
							<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
							<!-- AddThis Button END -->

						</div>

						<div class="span6">

							<h3>Features </h3>
							<h4>Hosted PBX</h4>
							<p>Hosted PBX is a service where the call platform and PBX features are hosted at the service provider's location. The business end users connect via IP to the provider for voice services.

</p>

<h4>Business Hosted PBX Providers</h4>
<p>A business hosted PBX phone system can reduce your monthly phone bill tremendously compared to a traditional business phone system. As a characteristic of a hosted PBX system, the PBX is hosted offsite, ie: at the provider's facilities. Below is a list of hosted PBX for business providers
</p>
<h4>Residential Hosted PBX Providers</h4>	
<p>A residential hosted PBX phone system can reduce your monthly phone bill tremendously as well when compared to plain old telephone service. As a characteristic of a hosted PBX system, the PBX is hosted offsite at the provider's facilities.                                                                                        
</p>
<p>Hosted PBX is a service where the call platform and PBX features are hosted at the service provider's location. The business end users connect via IP to the provider for voice service.

</p>

<h4>Benefits of Hosted PBX</h4>
<p>There are many benefits of using Hosted PBX rather than a traditional phone system, or an on-premise PBX. The main benefit is cost-saving. A Hosted PBX system costs much lesser to set-up than an on-premise PBX. In many cases, there are no set-up fees for a hosted PBX system. Purchasing and setting up an on-premise PBX can cost tens of thousands of dollars. Hosted PBX phone systems fall under operational expenditure rather than capital expenditure, which also makes hosted PBX service attractive to businesses. With hosted PBX service, you merely pay a monthly fee, and the hosted PBX service provider takes care of the rest.

</p>
<p>Another benefit of a hosted PBX system over an on-premise PBX is that hosted PBX service providers will take care of all the set-ups and installations, meaning, you do not need to be a telecom or VoIP expert in order to get a hosted PBX system. A downside to a hosted PBX is that you may have a little less ability to customize your solution to your business, but many hosted PBX service providers nowadays can achieve a deep level of customization.
</p>
				
						</div>
					</div>

				</div>

			</div>