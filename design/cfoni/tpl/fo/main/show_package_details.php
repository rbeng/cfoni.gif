<?php
global $CONF;

$url_price_table =  $CONF['url_app'].'?m=main&c=shw_table_price';
?>

<div role="main" class="main">

				<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">Package Description</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2><?php echo $data['strPackageName'];?> </h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h2>Everything about <?php echo $data['strPackageName'];?> </h2>

					<div class="row">
						<div class="span6">

							<div>
						       <?php 
														$folder_id = (int)($data['intPackageId']/ $CONF['const_photo_per_dir'    ]);  
                    									$dirpoto = $CONF['dir_photo'].'/package/'.$folder_id.'/'.$data['intPackageId'].'/'.$data['strImage']; 
                     									if(file_exists($dirpoto)){
                     										$image 		= $CONF['http_photo'].'/package/'.$folder_id.'/'.$data['intPackageId'].'/'.$data['strImage']; 
                     										?>
															<a  class="fancybox" href="<?php echo $image?>"><img src="<?php echo $image?>"  height="300" width="400"/></a>
															
															<?php
                     									}
														
														
														?>
                                
                                		
                                        
                                        
                                        
                                        
                                        
							</li>
								
							</div><br><br>
							

							<!-- AddThis Button BEGIN -->
							<div class="addthis_toolbox addthis_default_style ">
								<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
								<a class="addthis_button_tweet"></a>
								<a class="addthis_button_pinterest_pinit"></a>
								<a class="addthis_counter addthis_pill_style"></a>
							</div>
							<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
							<!-- AddThis Button END -->

						</div>

						<div class="span6">

							<h4><?php echo $data['strPackageName'];?>  <strong>Description</strong></h4>
							<p>
                            <?php
                            
							echo $data['strDescription'];
							
							?>
                            </p>
 </p>
							<a href="<?php echo $url_price_table;?>" class="btn btn-primary">Go to Order</a> <span class="arrow hlb hidden-phone" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>

							<h4 class="pull-top"><?php echo $data['strPackageName'];?> <strong>Features</strong></h4>

							
                            
                            <?php
                            
							echo   $data['strFeature']
							?>
                            
                            
                            

						</div>
					</div>

					<hr class="tall" />					
							

					</div>

				</div>

			</div>


