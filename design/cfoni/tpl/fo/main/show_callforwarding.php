<?php
global $CONF,$Q,$CODE;

?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">Call Forwarding</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>Call Forwarding</h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h2>CALL FORWARDING</h2>

					<div class="row">
						<div class="span6">

							<div class="" >
<img src="<?php echo $CONF['url_pic_direct']; ?>img/projects/forward.png" height="365" width="548">
									</li>
								
							</div><br><br>
							

							<!-- AddThis Button BEGIN -->
							<div class="addthis_toolbox addthis_default_style ">
								<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
								<a class="addthis_button_tweet"></a>
								<a class="addthis_button_pinterest_pinit"></a>
								<a class="addthis_counter addthis_pill_style"></a>
							</div>
							<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
							<!-- AddThis Button END -->

						</div>

						<div class="span6">

							<h4>Call Forward  <strong>Description</strong></h4>
							<p>Forward calls with ease and flexibility—never miss calls while you travel. Callers to your main number (and extension) will hear your professional business greeting and then the Auto-Receptionist will ask them to hold while locating you. You’ll hear the name by phone or, if you’re online, through your computer speakers. If enabled, the Auto-Receptionist will ask the caller to state their name.
</p>
							
							<h4 class="pull-top">Call Forward <strong>Features</strong></h4>

							<ul class="list icons unstyled">
								<li><i class="icon-ok"></i> Forward calls automatically to multiple phone numbers, such as an office, home, or mobile phone</li>
								<li><i class="icon-ok"></i> Configure your system to ring your selected numbers in simultaneous or sequential order</li>
								<li><i class="icon-ok"></i> You can forward calls to any international number using our low international rates</li>
								<li><i class="icon-ok"></i> You can have all phones ring at once or in a fixed order. For instance, if you forward calls to a sales team and one person doesn’t pick up, the call is automatically forwarded to the next person or department</li>
                                <li><i class="icon-ok"></i> Define the number of times a phone rings before calls forward to another device</li>
                                
							</ul>

						</div>
					</div>

					<hr class="tall" />					
							

					</div>

				</div>

			</div>
