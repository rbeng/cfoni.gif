<?php
global $CONF,$Q;
?>

<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
if (limitField.value.length > limitNum) {
	limitField.value = limitField.value.substring(0, limitNum);
	} else {
	limitCount.value = limitNum - limitField.value.length;
	}
}
</script>


<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#messageBox").addClass("messagebox");setTimeout(function(){
$("#messageBox").fadeOut("slow")}, 6000);
});
</script>
<?php
if($Q->req['msg']==1){
	?>
	<script>
	alert('Thanks for sending us mail, we will get back to you in shortly.');
	location.href="?m=main&c=shw_contactus";
	</script>
	
	<?php
	}

?>

<!--<script type="text/javascript" src="design/js/valid.js"></script>-->

<style>
.smallalert{ font-size:smaller; color:#990000;};
</style>
<script type="text/javascript" src="<?php echo HTTP_SERVER;?>design/<?php echo $CONF['tpl_name'];?>/ajaxsubmit/jquery.form.js"></script>
<script type="text/javascript">
function validForm(){
	
	
         $("#submitBtn").hide();
         $("#flashShow").show();
                 var options ={success: function(data)
	 {
               $("#flashShow").hide();
               $("#submitBtn").show();
               if(data=='done'){
                     //$("#formClass").hide();  // If you dont wana hide the form than only reset using jquery
                    // $("#responseMessage").html('Thank you for submitting your contact information<br/>');
					
					location.href="index.php?m=main&c=shw_contactus&msg=1";
					
               }else{
                     $("#responseMessage").html(data);
               }
               return false;
         }};
         $("#frmRegistration").submit();
		
}
</script>
<div role="main" class="main">

				<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.html">Home</a> <span class="divider">/</span></li>
									<li class="active">Contact Us</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>Contact Us</h2>
							</div>
						</div>
					</div>
				</section>

				

				<div class="container">

					<div class="row">
						<div class="span6">
                        <?php
	 					$cError = count($error);
						 if($cError > 0){
 	 					?>     
     					
    					 <?php
	 						foreach($error as $fc => $eMsgx){
							 echo  "<font color='#FF0000'>".$eMsgx."</font>";
							 echo "<br>";
							}
						 }
						 ?>
                    
            				<h2 class="short"><strong>Contact</strong> Us</h2>
                             <span id="responseMessage"></span>
							<form method="post" action="<?php echo $CONF['url_app'];?>?m=main&c=do_addContactUs"  name="frmRegistration" id="frmRegistration"  enctype="multipart/form-data">
                            <input type="hidden" name="csf" value="">
                            
								<div class="row controls">
									<div class="span3 control-group">
										<label>Your name *</label>
										<input type="text"  name="name" value="<?php echo $Q->req['name']; ?>"  maxlength="100" >
									  </div>
									<div class="span3 control-group">
										<label>Your email address *</label>
										<input type="text" value="<?php echo $Q->req['email']; ?>"  maxlength="100" class="span3" name="email" >
									</div>
								</div>
								<div class="row controls">
									<div class="span6 control-group">
										<label>Subject</label>
										<input type="text" value="<?php echo $Q->req['subject']; ?>"  maxlength="100" class="span6" name="subject" >
									</div>
								</div>
                                	
								<div class="row controls">
									<div class="span6 control-group">
										<label>Message *</label>
								      <textarea id="message" name="message" class="span6" cols="50" rows="5" onKeyDown="limitText(this.form.message,this.form.countdown,220);"onKeyUp="limitText(this.form.message,this.form.countdown,220);"></textarea> <br />
                                     <font  size="1" style="color:#0F4F7C">(Maximum characters: 220) You have <input readonly type="text" lass="booktextarea" name="countdown" size="3" value="220"> characters left.</font>
     
                                    
                                    </div>
								</div>
								<div class="btn-toolbar">
									<!--<input type="submit" value="Send Message Here" class="btn btn-primary btn-large">-->
                                    
                                    
                                    
                                      <span id="submitBtn">
                               <button type="button" name="submitForm"  class="btn btn-primary btn-large" onclick="return validForm()">Send Message Here</button>
                            
                           </span>
                           <span id="flashShow"></span>
                                    
                                    
                                    
                                   
                         </td>
						    
                                     <span class="arrow hlb hidden-phone" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>

                                    
                                  
								</div>
                                
                               
</td>
						    
							</form>
                            
                           
						</div>
                     
						<div class="span6">

							<h4 class="pull-top">Get in <strong>touch</strong></h4>
							<p>We are a new startup company that provide VOIP solutions for B 2 B, as well as handle outsource server management and database management for companies. We focus on spects of IT Technology & Websystem, infrastructure and administration including installation, configuration,monitoring and troubleshooting. </p>

							<hr />

							<h4>The <strong>Office</strong></h4>
							<ul class="unstyled">
								<li><i class="icon-map-marker"></i> <strong>Address:</strong>A-3-1, Northpoint Offices, Medan Syed Putra Utama, No 1 Jalan Syed Putra, Mid Valley City 59200 Kuala Lumpur</li>
								<li><i class="icon-phone"></i> <strong>Phone:</strong>03-228406807</li>
								<li><i class="icon-envelope"></i> <strong>Email:</strong> <a href="mailto:helpdesk@cfoni.com">helpdesk@cfoni.com</a></li>
							</ul>

							<hr />

							<h4>Business <strong>Hours</strong></h4>
							<ul class="unstyled">
								<li><i class="icon-time"></i> Monday - Friday 9am to 5pm</li>
								<li><i class="icon-time"></i> Saturday - 9am to 2pm</li>
								<li><i class="icon-time"></i> Sunday - Closed</li>
							</ul>

						</div>
                      

					</div>

				</div>

			</div>

			 
		</div>

	
