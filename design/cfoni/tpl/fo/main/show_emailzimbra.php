<?php
global $CONF,$Q,$CODE;

?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">Cmail.my and MS Exchange</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>Cmail.my and MS Exchange</h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h2>Cmail.my and MS Exchange for the Post-PC era</h2>

					<div class="row">
						<div class="span6">

							<div class="" >
										<img src="<?php echo $CONF['url_pic_direct']; ?>img/projects/email.jpg" height="365" width="550">
								
															</div>

							
						</div>

						<div class="span6">

							<h4>Cmail.my email <strong>Description</strong></h4>
							<p>Bring together Email, Calendaring and Enterprise Applications.
Cmail.my is an enterprise-class email, calendar and collaboration solution: built for the cloud, both public and private. With a redesigned browser-based interface, Cmail.my offers the most innovative messaging experience available today, connecting end users to the information and activity in their personal clouds.</p>

							<a href="feature-pricing-tables.php" class="btn btn-primary">Go to Order</a> <span class="arrow hlb hidden-phone" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>

							<h4 class="pull-top">Cmail.my <strong> Features</strong></h4>

							<ul class="list icons unstyled">
								<li><i class="icon-ok"></i> A smarter mailbox reduces time spent finding important emails</li>
								<li><i class="icon-ok"></i> Email Compose tabs eliminate the need for new windows with every message</li>
								<li><i class="icon-ok"></i> Integration of email, contacts and calendar makes toggling between them unnecessary
</li>
								<li><i class="icon-ok"></i> Recover gracefully with Undo Send, Restore Email and Scheduled Delivery</li>
                                <li><i class="icon-ok"></i> Simple sharing with delegated access to inbox or folders</li>
                                
							</ul>

						</div>
						<div class="container">

					

					<div class="row">
						<div class="span6">

							<div class="" >
										<img src="<?php echo $CONF['url_pic_direct']; ?>img/projects/exc.jpg" height="365" width="550">
									</li>
								</div><br><br>
									<!-- AddThis Button BEGIN -->
							<div class="addthis_toolbox addthis_default_style ">
								<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
								<a class="addthis_button_tweet"></a>
								<a class="addthis_button_pinterest_pinit"></a>
								<a class="addthis_counter addthis_pill_style"></a>
							</div>
							<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
							<!-- AddThis Button END -->
								
							

							
						</div>

						<div class="span6">

							<h4>Ms Exchange <strong>Description</strong></h4>
							<p>The best advantage of using Microsoft Exchange server is the high-level of security features of the software package. The server neutralizes security threats and thus, the users are protected against viruses, spam and hackers. The security feature allows users to use the system to its maximum potential. They can communicate irrespective of whether they are in an office or on the go. Microsoft exchange server is compatible with additional features such as voice mail storage, calendar, and contact organizing application and scheduling. Microsoft Exchange uses the Microsoft Office Outlook as its email platform. </p>
							<a href="feature-pricing-tables.php" class="btn btn-primary">Go to Order</a> <span class="arrow hlb hidden-phone" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>

							<h4 class="pull-top">Ms Exchange<strong> Features</strong></h4>

							<ul class="list icons unstyled">
								<li><i class="icon-ok"></i>  Supports IMAP, POP and web email clients including Microsoft Outlook</li>
								<li><i class="icon-ok"></i> The exchange lets users share information using either Outlook Web Access or Outlook</li>
								<li><i class="icon-ok"></i> Allows users to schedule messages by fixing the start and endpoints. Separate messages for recipient inside and outside the organization can be created</li>
								<li><i class="icon-ok"></i> The search lets you find information from any corner of the inbox using keywords</li>
                                <li><i class="icon-ok"></i>  RSS feed supporter, email scheduler, better preview pane and an option to view attachment</li>
                                
							</ul>

						</div>	
						
							

						</div>
					</div>
				</div>
			</div>

					<hr class="tall" />

										
							

					</div>

				</div>

			</div>
