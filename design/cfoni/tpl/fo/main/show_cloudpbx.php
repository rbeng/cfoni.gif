<?php
global $CONF,$Q,$CODE;

?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active"> Cloud PBX</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2> Cloud PBX</h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h2>CLOUD PBX</h2>

					<div class="row">
						<div class="span6">

							<div class="" >
										<img src="<?php echo $CONF['url_pic_direct']; ?>img/projects/cloud.png" height="365" width="550">
									</li>
								
							</div><br><br>
							

							<!-- AddThis Button BEGIN -->
							<div class="addthis_toolbox addthis_default_style ">
								<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
								<a class="addthis_button_tweet"></a>
								<a class="addthis_button_pinterest_pinit"></a>
								<a class="addthis_counter addthis_pill_style"></a>
							</div>
							<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
							<!-- AddThis Button END -->

						</div>

						<div class="span6">

							<h3>Cloud PBX </h3>
							<h4>Traditional hard wired PBX system has evolved into the technology that makes advanced solutions.
 </h4>
							<p>PBX (Private Branch Exchange) is a telephone exchange that serves a particular business or office building, as opposed to one that a common carrier or telephone company operates for many businesses or for the general public. PBXs make connections among the internal telephones of a business or private organization and connect them to the public switched telephone network (PSTN)
. 
</p><p>Cloud PBX systems, also known as a 	VoIP	 PBX, IP-PBX, hosted PBX, virtual PBX or IPBX, use Internet
Protocol to carry calls. Most modern PBXs support VoIP, which simply means Voice Over Internet Protocol. A  Cloud PBX system consists of one or more VoIP enabled handsets of softphones, and an IP PBX.
The phones have to be pre-registered with the IP PBX server, in order for the IP PBX to establish the connection when user make a call.
</p>

<h4>Why should Companies choose a Cloud PBX?
</h4>
<p>Cloud based technologies have become an ubiquitous part of modern life.
    Many of us do our shopping,  banking as well as taxes in the cloud these days. We also use cloud technologies such as social media networks to keep in touch with our family, friends and colleagues. These tools are popular because they are user-friendly, accessible from anywhere and available at very low (or sometimes no) cost.
  These are very similar to the reasons why more and more companies are choosing a cloud PBX.
</p>
<p>The second reason is that accessibility from anywhere is important for disaster planning and recovery. In the event that your office location becomes inaccessible due to natural or manmade disaster, having a cloud PBX means that calls can be re-routed to other locations, mobile or home phones. It also means that you can make changes to auto-attendants or other announcements to inform your callers know what is happening, without having the need to enter the office.
</p>
<h5>This is the best solution where it provides expert installation assistance, project management and end user support.
</h5>	
<p>Choosing a cloud-based PBX
solution is a great way to free up IT resources to work on projects that are more strategic to the business.  It is also a good choice for small businesses with few technical resources.  The best
solutions provide 	expert installation assistance	, project management and end user support.
</p>
<h5>Cloud PBX solutions eliminate multiple vendor confusion </h5>
<p>With a traditional PBX, companies
may have a PBX vendor, a telecommunications carrier and a local service provider, each with a different invoice, different point of contact and fingers to point at each other. With a cloud-based PBX, companies only have one vendor to manage and one point of responsibility.
</p>
<h5>A Cloud PBX is accessible from anywhere</h5>
<p>This is important for two reasons. First, having a phone system that is not tied to a physical location makes unifying multiple offices and remote workers easier.  Everyone is accessing the same system from wherever they happen to be. This means that you can transfer calls by dialling 4-digits number and then engage in ad hoc conferencing with business partners in the another state as easy as with colleagues in the next cubicle.
 
</p>
<h5>Cloud-based PBX solutions are easily scalable</h5>
<p>The main advantage of a hosted PBX solution is that it can grow along with your company. This means that you pay for only what you need now and capacity can be added easily as and when needed. Often the features are scalable too. ShoreTel offers several standard phone features and add-on services to our cloud PBX that companies can order when they sign up, or add later when the need for each becomes apparent.
</p>
<p>These are just some of the reasons that 	companies benefit	 from cloud PBX solutions.  We are happy to
help you learn more about our 	hosted PBX offering	 and discuss the specific ways it might be a good fit for
your business
</p>
							<a href="feature-pricing-tables.php" class="btn btn-primary">Go to Order</a> <span class="arrow hlb hidden-phone" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>

							<h4 class="pull-top">Cloud PBX <strong>Features</strong></h4>

							<ul class="list icons unstyled">
								<li><i class="icon-ok"></i> Establishing connections between the telephone sets of two users.</i> </li>
								<li><i class="icon-ok"></i> Maintaining the connections as long as needed.</i> </li>
								<li><i class="icon-ok"></i> Disconnecting the connections when the users hang up.</i> </li>
								<li><i class="icon-ok"> </i> Providing information for accounting purposes.</i> </li>
                                                                
							</ul>

						</div>
					</div>

					<hr class="tall" />					
							

					</div>

				</div>

			</div>