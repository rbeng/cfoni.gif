<?php
global $CONF,$Q,$CODE;

?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">Carrer </li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>Carrer </h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h3><strong>Carrer</strong></h3>
					
					<div class="row">
						<div class="span12">
					
					<p>we recognize that our people are our greatest assets and are vital to our continued growth and innovative spirit. Currently two positions available at the moment for Sales & Marketing Executive and PHP Programmer. </p>		
                  <h4>Sales & Marketing Executive</h4>
                  <h5>Responsibilites</h5>
                  <ul>
            <li>   Visit potential customers to prospect for new business, identifying new markets and business opportunities. </li>
            <li>  Support customer in providing technical information, quotations, delivery and after sales services. </li>
            <li> Solve customer issues on all matters pertaining payment issues, discrepancy in goods, credibility etc. </li>
            <li> Monitor and expedite overdue accounts in planned time frame. </li>
            <li> Review own sales & collection performance, aim to meet or exceed  targets for higher commission. </li>
            <li>  Report to the Sales & Marketing Manager on a regular basis in writing. </li>
            <li>   Identify all customers$B!G(B complaints and satisfaction analysis by generating regular reports, for improvements to be done.</li>
        </ul>
                                  
                  <h5>Requirements </h5>
                  <ul>
                  	<li> A combination of Sales and Marketing, Strategic Planning, Market Development, or Business Development experience is advantageous.</li>
                  	<li> Self-motivated and adaptable to be able to work with minimal supervision.</li>
                  	<li>  IT knowledge required as selling IT Solutions.</li>
                  </ul>
                 

	<h5> Benefits</h5>
	<ul>
	<li> 
    Attractive remuneration/benefit package- EPF, SOSCO, Medical, Performance Bonus, Staff Welfare.</li>
	<li> Basic + Attractive Commission Scheme + Monthly Transport & Outstation Allowances.</li>
	</ul>


 <h4>PHP Programmer</h4>

                  <h5>Responsibilites</h5>
                  
                  <ul >
                  <li> Develop database driven PHP web applications.</li>
                  <li> Understand business requirements, and plan work flow accordingly.</li>
                  <li> Well Organized and Able to handle multiple tasks simultaneously.</li>
                  <li> Write clean, presentable and efficient code.</li>
                  <li> Manage Linux-based Application Environment and write automation scripts to implement cron jobs.</li>
                  <li> Self starter, should have own initiative to improvements of applications.</li>
                  <li> Test and Debug Web Applications.</li>
								                   
				</ul>
                  <h5>Requirements </h5>
                  <ul>
                  	<li> Candidate must possess at least a Diploma, Advanced/Higher/Graduate Diploma, Bachelor's Degree, Post Graduate Diploma, Professional Degree, Information Technology or equivalent.</li>
                  	<li>  Work experience of at least 3 years is required.</li>
                  	<li> Proficient in LAMP based development.</li>
                  	<li> At least 3+ years experience with PHP.</li>
                  	<li> Experience in XML will be an advantage.</li>
                    <li> Experience with JavaScript/jQuery, CSS, AJAX and HTML5 web standards.</li>
                    <li> Experience of using Linux, and familiarity with basic Linux CLI environment.</li>
                    <li> Shell scripting capability will be a plus.</li>
                    <li> Prior experience of Asterisk or other VoIP Systems is preferred.</li> 
                    <li> Prior experience of using third-party API is preferred.</li>
                    <li>   Prior experience of using Payment systems is preferred.</li>
                    <li>  Able to work independently with minimum supervision.</li> 
                    <li>  Good communication skills (Written and verbal).</li>               		                  
                  	</ul>
                 

<h5>Company Overview</h5>
<p> We are a company that provide VOIP solutions for B2B and B2C, as well as handle out source server management and database management for companies. We Focus on spects of IT Technology & Websystem,infrastructure and administration including installation,configuration,monitoring and troubleshooting.</p>
<h5>Why Join Us?</h5>
<p> In Cfoni Techonologies Sdn Bhd,we are welcoming the potential and suitable candidates to join our company as we provide good working environment and it is suitable for candidates that are hunger for carrer growth in Computer/information Technology (Software) industry.</p>



</div>
					
					

					

							
							</div>

						</div>

					</div>

				</div>

			</div>