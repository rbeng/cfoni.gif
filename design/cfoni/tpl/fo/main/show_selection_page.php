<?php
global $CONF;
$http_dir = HTTP_SERVER.'design/'.$CONF['tpl_name'].'/';
$URL_ADDOrder =  $CONF['url_app'].'?m=main&c=do_addOrder';
?>
<script type="text/javascript" src="<?php echo $http_dir; ?>/js/jquery.js"></script>

<script>
$(document).ready(function() {

		//the min chars for username
		var min_chars = 1;

		//result texts
		var characters_error = '<span class="is_not_available"><b>Email Address cannot be empty.</span>';
		var checking_html = '<img src="images/loading.gif">Checking...';

		//when button is clicked
		$('#check_username_availability').click(function(){
			//run the character number check
			if($('#username').val().length < min_chars){
				//if it's bellow the minimum show characters_error text '
				$('#username_availability_result').html(characters_error);
			}else{
				//else show the cheking_text and run the function to check
				$('#username_availability_result').html(checking_html);
				check_availability();
			}
		});

  });

//function to check username availability
function check_availability(){

		//get the username
		var username = $('#username').val();

		//use ajax to run the check
		$.post("?m=main&c=do_check_email", { username: username },
			function(result){
				//if the result is 1
				if(result == 1){
					//show that the username is available
					$('#username_availability_result').html('<span class="is_available"><b>' +username + '@cmail.my </b> is Available</span>');
				}else{
					//show that the username is NOT available
					$('#username_availability_result').html('<span class="is_not_available"><b>' +username + '@cmail.my </b> is not Available</span>');
				}
		});

}






</script>



<style type='text/css'>
#check_username_availability{
	background: #225384;
	border:1px solid black;
	color:white;
}

.is_available{
	color:green;
}
.is_not_available{
	color:red;
}
</style>

<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">ORDER STEPS : FREE TRIAL ( 1 MONTH )</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>ORDER STEPS : FREE TRIAL ( 1 MONTH )</h2>
							</div>
						</div>
					</div>
				</section>
   <div style="padding-left:50px;">             
 <form method="post" action="<?php echo $URL_ADDOrder?>" name="Fromfreepackage">
 
    <table width="50%" cellpadding="0" cellspacing="0" border="0">
    <tr>
    <td width="18%">Email Address</td>
    <td>
    <input type="text" name="username" id="username" >@cmail.my
    <input type='button' id='check_username_availability' value='Check Availability' class="btn btn-primary" >  
     <div id='username_availability_result'></div>  
     
     
     
     
     
    </td>
    </tr>
    <tr>
    <td width="18%">DID Number</td>
    <td>
    <select name="intDID">
    <?php
     foreach($data['number'] as $kId => $intNumber){
		 ?>
		 <option value="<?php echo $kId;?>"><?php echo $intNumber;?></option>
		 
		 <?php
		 
		}
	?>
    </select>
    
    </td>
    </tr>
     <tr>
    <td width="18%">Fax Number</td>
    <td>
    <select name="intFax">
    <?php
     foreach($data['number'] as $kId => $intNumber){
		 ?>
		 <option value="<?php echo $kId;?>"><?php echo $intNumber;?></option>
		 
		 <?php
		 
		}
	?>
    </select>
    
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <input type="submit" class="btn btn-primary"  value="Order Now"/>
  
    
    </td></tr>
    
    
    </table>
 
 
 </form>
 </div>