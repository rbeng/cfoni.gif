<?php
global $CONF,$Q,$CODE;

?>
<section class="page-top">
					<div class="container">
						<div class="row">
							<div class="span12">
								<ul class="breadcrumb">
									<li><a href="index.php">Home</a> <span class="divider">/</span></li>
									<li class="active">FAQ</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<h2>FAQ</h2>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h2>Frequently Asked <strong>Questions</strong></h2>

					<!--<div class="row">
						<h5> What is Cfoni Technologies ? </h5>
						<div> <p> Cfoni Technologies is a cloud-based business phone and fax system provider. It combines a hosted phone system with advanced call and fax management, reliable phone and fax service, and full-featured IP phones that arrive pre-configured and ready for use.
</p>
							<p> With Cfoni Technologies, you can easily connect your office, remote and mobile employees under one phone system, regardless of their locations.
 </p>
							<p> Unlike expensive, conventional phone systems, Cfoni Technologies is purchased, activated, setup and managed online, which means we manage it and you use it. By eliminating the need for complex hardware, lengthy installation and technical expertise, you get the functionality and flexibility to easily connect with customers without high costs or hardware hassles.
 </p>
 <h5> What is VoIP ?</h5>
							<p>VoIP (Voice over Internet Protocol) is a general term used to  describe voice traffic being transmitted via the Internet networks rather then the traditional circuit-switched or public switched telephone network.</p>
						<h5> Why should I choose a VoIP cloud PBX  ?</h5>
						<p>The  cloud PBX model eliminates your software and hardware  overhead since your provider is maintaining the equipment and software for you and supporting your users with qualified and  experienced support/telecom engineers. The only equipment you  need are your VoIP phones.</p>
						</div>
							<h5> What are the benefits of Cfoni Technologies ? </h5>
							<p> Setup your phone system to work the way you do</p>
							<ul class="list icons unstyled">
								<li><i class="icon-ok"></i> Connect your office, remote employees and mobile users under one phone system</li>
								<li><i class="icon-ok"></i> Easily support multiple locations, move from one location to another or instantly add new users</li>
								<li><i class="icon-ok"></i> Change answering and forwarding rules online, anytime, anywhere</li>
								<li><i class="icon-ok"></i> Integration with existing phones or mobile devices</li>
                                  <li><i class="icon-ok"></i> Never miss a call or fax</li>
								<li><i class="icon-ok"></i> Be connected to your phone system in and out of the office</li>
								<li><i class="icon-ok"></i> Customers can reach you or your employees, wherever you are</li>
								<li><i class="icon-ok"></i> Get your voice and fax messages anytime, from anywhere</li>
								<li><i class="icon-ok"></i> One provider for the business phone system, voice and fax</li>
								<li><i class="icon-ok"></i> 24/7 Customer support*</li>
								                                                                
							</ul>
                            <h4>ABOUT CFONI TELECOM</h4>
                            <h5> How do you handle billing ?</h5>
                            <p>We accept pay via paypal.com and ipay88.com or we can invoice you.</p>
                            <h5>Can I be invoiced for your services ?</h5>
                            <p>Yes.</p>
                            <h5>What can cfoni do for me and my business ?</h5>
                            <p>cfoni Telecom offers a feature-rich suite of telephony services  that run over your existing high-speed internet connection.We provide a cloud PBX product that is scalable and versatile providing you with a wide range of features many of which do not exist on a standard local telephone service.</p>
						<h5>How long has cfoni Telecom been in business ?</h5>
						<p>cfoni Telecom was started in 2013.</p>
						<h5> What is required to use a cfoni cloud PBX phone system ?</h5>
                         <p>A broadband internet connection includes DSL, T-1, fiber and cable connectivity. (When in doubt, please contact cfoni Telecom and we'll work with you to determine your type of connectivity you have and if there is enough available bandwidth to run VoIP.</p>
                        
                         <p>A VoIP phone, smartphone app or computer based soft phone.There are many software-based telephones available (many which are freely available), however, business owners generally prefer a professional VoIP phone. There are a lot of features that come with VoIP phones that software phones do not have.</p>
                         <p> When purchasing VoIP phones from cfoni Telecom we will send you your VoIP phone pre-provisioned and so all you have to do is plug it into your broadband internet connection.  If you purchase your phone somewhere else, our engineers can walk you through the provisioning of you phone.</p>
													</div>-->
                <div role="main" class="main">
		
           <?php
          $cFaq =  count($data['faq']);
		   if( $cFaq > 0){
		   ?>    
                
<div class="tabs"><!--  start tab  -->
                                    <ul class="nav nav-tabs">
									
                                    <?php
									
									
                                    foreach($CODE['faq_type'] as $Tabname => $v){
									?>
                                    
                                    	<li class="<?php if($Tabname==1){ echo "active";}?>"><a href="#<?php echo $Tabname;?>" data-toggle="tab"> <?php echo $CODE['faq_type'][$Tabname];?></a></li>
										
									<?php
                                     }
									
									
									?>
                                    
                                    
                                    </ul>
									
<div class="tab-content"><!--  start tab content  -->
  <?php
  foreach($data['faq'] as $Tabname1 => $v){
  ?>
  
    <div class="tab-pane <?php if($Tabname1==1){ echo "active";}?>" id="<?php echo $Tabname1; ?>">
    
       <?php
       foreach($v as $g => $v){
		   ?>
		   <p><?php echo html_entity_decode($v['strQ']);?></p>
            <p><?php echo html_entity_decode($v['strA']);?></p>
		   
		   <?php
		   
		   }
	   
	   ?>
    
    
    
    </div>
  
   <?php
  }
   ?>

    </div>
    </div>                 
                                                    
                                                    
      <?php
		   }
		   else{
			   
			  echo "No FAQ at the moment..."; 
			  }
	  
	  ?>                                              
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    

						</div>
