function valid_g(){
	var value;	
	value = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"width=250><tr bgcolor=\"#F8F7F7\"><td width=\"3\" bgcolor=\"#A2F504\"></td><td style=\"padding:5px\"><font style=\"color:#338A4C\"><b>OK</b> !</font></td><td width=\"3\" bgcolor=\"#A2F504\"></tr></table>";
	return value;
}

function valid_r(x){
	var value;
	value = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=250><tr bgcolor=\"#F8F7F7\"><td width=\"3\" bgcolor=\"#FF0000\"></td><td style=\"padding:5px\"><font style=\"color:#FF0000\">"+x+"</font></td><td width=\"3\" bgcolor=\"#FF0000\"></tr></table>";
	return value;
}

function valid_firmname(){

	if (frmRegistration.firmname.value.length<=0)	{
		document.getElementById('firmname_msg').innerHTML = valid_r('Please, specify Company Name.');
	} else {
		document.getElementById('firmname_msg').innerHTML = valid_g();
	}
}

function valid_reg_no(){

	if (frmRegistration.reg_no.value.length<=100)	{
		document.getElementById('reg_no_msg').innerHTML = valid_g();
	} else {
		document.getElementById('reg_no_msg').innerHTML = valid_r('Max 100 characters (A-Z, a-z, 0-9).')
	}
}

function valid_address(){

	if (frmRegistration.address.value.length<=0)	{
		document.getElementById('address_msg').innerHTML = valid_r('Please, specify Address.');
	} else {
		document.getElementById('address_msg').innerHTML = valid_g();
	}
}
function valid_mobile(){

	if (frmRegistration.mobile.value.length >= 0){
		document.getElementById('mobile_msg').innerHTML = valid_g();
	}
} 

function valid_zip(){

	if (frmRegistration.zip.value.length<=0)	{
		document.getElementById('zip_msg').innerHTML = valid_r('Please, specify ZIP / Postal Code.');
	} else {
		if (isNaN(frmRegistration.zip.value)){
			document.getElementById('zip_msg').innerHTML = valid_r('Invalid ZIP / Postal Code. Number format only.');
		} else {
			document.getElementById('zip_msg').innerHTML = valid_g();
		}
	}
}

function valid_state(){

	if (document.getElementById('div_malaysia').style.display == "block"){
		if (frmRegistration.malaysia_city.value.length<=0)	{
			document.getElementById('state_msg').innerHTML = valid_r('Please, specify State.');
		} else {
			document.getElementById('state_msg').innerHTML = valid_g();
		}
	}	

	if (document.getElementById('div_nonmalaysia').style.display == "block"){
		if (frmRegistration.non_malaysia_city.value.length<=0)	{
			document.getElementById('state_msg').innerHTML = valid_r('Please, specify  State.');
		} else {
			document.getElementById('state_msg').innerHTML = valid_g();
		}
	}
}





function valid_city(){

	if (frmRegistration.strCity.value.length<=0)	{
		document.getElementById('city_msg').innerHTML = valid_r('Please, specify City.');
	} else {
		document.getElementById('city_msg').innerHTML = valid_g();
	}
}



function valid_surname(){

	if (frmRegistration.surname.value.length<=0)	{
		document.getElementById('surname_msg').innerHTML = valid_r('Please, specify Surname.');
	} else {
		document.getElementById('surname_msg').innerHTML = valid_g();
	}
}


function valid_lastname(){

	if (frmRegistration.lastname.value.length<=0)	{
		document.getElementById('lastname_msg').innerHTML = valid_r('Please, specify Last Name.');
	} else {
		document.getElementById('lastname_msg').innerHTML = valid_g();
	}
}

function valid_jobtitle(){

	if (frmRegistration.jobtitle.value.length<=0)	{
		document.getElementById('jobtitle_msg').innerHTML = valid_r('Please, specify Job Title.');
	} else {
		document.getElementById('jobtitle_msg').innerHTML = valid_g();
	}
}






function valid_mail(){

	if (frmRegistration.mail.value.length<=0)	{
		document.getElementById('mail_msg').innerHTML = valid_r('Please, specify Email.');
	} else {
		if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(frmRegistration.mail.value))){
			document.getElementById('mail_msg').innerHTML = valid_r('Invalid Email.');
		} else {
			document.getElementById('mail_msg').innerHTML = valid_g();
		}
	}
}

function valid_phone(){

	if (frmRegistration.phone.value.length > 0){
		if (frmRegistration.phone.value == 'Phone Number'){
			document.getElementById('phone_msg').innerHTML = valid_r('Please, specify Phone Number.');
		} else {
			document.getElementById('phone_msg').innerHTML = valid_g();
		}
	} else if (frmRegistration.phone.value.length <= 0){
		document.getElementById('phone_msg').innerHTML = valid_r('Please, specify Phone Number.');
	}
}

function valid_fax(){

	if (frmRegistration.fax.value.length >= 0){
		document.getElementById('fax_msg').innerHTML = valid_g();
	}
}



function valid_country(){

	if (document.frmRegistration.country.value == '1'){
		document.getElementById('div_malaysia').style.display = "block";
		document.getElementById('div_nonmalaysia').style.display = "none";
	} else {
		document.getElementById('div_malaysia').style.display = "none";
		document.getElementById('div_nonmalaysia').style.display = "block";
	}
}

function valid_search(){

	if (frmRegistration.search.value == "NA" || frmRegistration.search.value == ""){
		if (document.frmRegistration.search.value == ""){
			document.getElementById('search_msg').innerHTML = valid_r('Please make a selection.');
			document.getElementById('div_search').style.display = "none";
		} else if (document.frmRegistration.search.value == "NA"){
			document.getElementById('div_search').style.display = "block";
			if (document.frmRegistration.search_value.value.length <= 0){
				document.getElementById('search_msg').innerHTML = valid_r('If other, please specify.');
			} else {
				document.getElementById('search_msg').innerHTML = valid_g();
			}
		} else {
			document.getElementById('div_search').style.display = "none";
		}

	} else {
		document.getElementById('div_search').style.display = "none";
		document.getElementById('search_msg').innerHTML = valid_g();
	}
}

function valid_businesscat(){

	var fieldobj = document.getElementsByName('businesscat[]');
	var x=0;

	for(i=0;i<fieldobj.length;i++){
		if (fieldobj[i].checked) {
			x++;
		}
	}

	if(x == 0){
		document.getElementById('business_msg').innerHTML = valid_r('Please specify, tick the terms and Conditions.');
	} else if (x > 3){
		document.getElementById('business_msg').innerHTML = valid_r('No more than 3 selection is allowed.');
	} else {
		document.getElementById('business_msg').innerHTML = valid_g();
	} 
}





function valid_terms(){

	var fieldobj = document.getElementsByName('notice[]');
	var x=0;

	for(i=0;i<fieldobj.length;i++){
		if (fieldobj[i].checked) {
			x++;
		}
	}

	if(x == 0){
		document.getElementById('terms_msg').innerHTML = valid_r('Please specify, tick the terms and Conditions.');
	}  else {
		document.getElementById('terms_msg').innerHTML = valid_g();
	} 
}








function valid_business(){
	if (frmRegistration.business.value.length<=0)	{
		document.getElementById('business_msg').innerHTML = valid_r('Please, specify Company Profile.');
	} else {
		document.getElementById('business_msg').innerHTML = valid_g();
	}
}

function valid_pro_desc(){
	if (frmRegistration.pro_desc.value.length<=0)	{
		document.getElementById('pro_desc_msg').innerHTML = valid_r('Please, specify Products / Services.');
	} else {
		document.getElementById('pro_desc_msg').innerHTML = valid_g();
	}
}

function valid_procat(){

	var fieldobj = document.getElementsByName('procat[]');
	var x=0;

	for(i=0;i<fieldobj.length;i++){
		if (fieldobj[i].checked) {
			x++;
		}
	}

	if(x == 0){
		document.getElementById('procat_msg').innerHTML = valid_r('Please specify, Business Category .');
	} else if (x > 3){
		document.getElementById('procat_msg').innerHTML = valid_r('No more than 3 selection is allowed.');
	} else {
		document.getElementById('procat_msg').innerHTML = valid_g();
	} 
}

function valid_pass(){

	if (frmRegistration.pass.value.length <= 0)	{
		document.getElementById('pass_msg').innerHTML = valid_r('Please, specify Password.');
	} else if (frmRegistration.pass.value.length > 0 && frmRegistration.pass.value.length <= 3){
		document.getElementById('pass_msg').innerHTML = valid_r('4 to 20 characters (A-Z, a-z, 0-9, no spaces).');
	} else {
		var valid = passCharsValid(frmRegistration.pass.value);
		if (!valid){
			document.getElementById('pass_msg').innerHTML = valid_r('4 to 20 characters (A-Z, a-z, 0-9, no spaces).');
		} else {
			document.getElementById('pass_msg').innerHTML = valid_g();
		}
	}
}

function valid_pass2(){

	if (frmRegistration.pass2.value.length <= 0)	{
		document.getElementById('pass2_msg').innerHTML = valid_r('Please, specify Password.');
	} else if (frmRegistration.pass2.value.length > 0 && frmRegistration.pass2.value.length <= 3){
		document.getElementById('pass2_msg').innerHTML = valid_r('4 to 20 characters (A-Z, a-z, 0-9, no spaces).');
	} else {
		if (frmRegistration.pass.value != frmRegistration.pass2.value){
			document.getElementById('pass_msg').innerHTML = valid_r('Your password entries did not match !');
			document.getElementById('pass2_msg').innerHTML = valid_r('Your password entries did not match !');
		} else {
			var valid = passCharsValid(frmRegistration.pass2.value);
			if (!valid){
				document.getElementById('pass2_msg').innerHTML = valid_r('4 to 20 characters (A-Z, a-z, 0-9, no spaces).');
			} else {
				document.getElementById('pass2_msg').innerHTML = valid_g();
			}
		}
	}
}

function valid_security(){
	if (frmRegistration.security.value.length <= 0)	{
		document.getElementById('security_msg').innerHTML = valid_r('Please, specify Security Code.');
	}
	else if (frmRegistration.securityhidden.value != frmRegistration.security.value ){
			document.getElementById('security_msg').innerHTML = valid_r('Your Security Code entries match security !');
			
		}
	
	 else if(frmRegistration.securityhidden.value == frmRegistration.security.value) {
		document.getElementById('security_msg').innerHTML = valid_g();
	}
}

function valid_all_step1(){

	valid_firmname();
	valid_reg_no();
	valid_address();
	valid_zip();
	valid_country();
	valid_city();

	valid_surname();
	valid_mail();
	valid_phone();
	valid_fax();
	
	valid_state();
	valid_lastname();
	valid_jobtitle();
	valid_pass();
	valid_pass2();
	valid_security();
	valid_terms();
	
}





function valid_step1(form){	

	valid_all_step1()

	if (form.firmname.value.length <= 0){		
		return (false);
	}

	if (form.address.value.length <= 0){
		return (false);
	}

	if (document.getElementById('div_malaysia').style.display == "block"){
		if (form.malaysia_city.value.length <= 0){
			return (false);
		}
	}	

	if (document.getElementById('div_nonmalaysia').style.display == "block"){
		if (form.non_malaysia_city.value.length <= 0){	
			return (false);
		}
	}

	if (form.zip.value.length <= 0){		
		return (false);
	} else {
		if (isNaN(form.zip.value)){
			return (false);
		}
	}
	if (form.surname.value.length <= 0){		
		return (false);
	}

	if (form.mail.value.length <= 0){
		return (false);
	} else {
		if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form.mail.value))){
			return (false);
		}
	}

	if (form.phone.value.length > 0){
		if (form.phone.value == 'Phone Number'){			
			return (false);
		}
	} else if (form.phone.value.length <= 0){
		return (false);
	}

	if (form.domain.value.length > 0 && form.domain.value.length < 3){
		return (false);
	} else	if (form.domain.value.length >= 3){
		var valid = domainnameCharsValid(form.domain.value);
		if (!valid){
			return (false);
		}
	}

	

	var fieldobj = document.getElementsByName('businesscat[]');
	var x=0;

	for(i=0;i<fieldobj.length;i++){
		if (fieldobj[i].checked) {
			x++;
		}
	}

	if(x == 0){
		return (false);
	} else if (x > 3){
		return (false);
	}
}

function valid_step2(form){

	valid_all_step2();

	if (form.business.value.length <= 0){
		return (false);
	}

	if (form.pro_desc.value.length <= 0){
		return (false);
	}

	var fieldobj = document.getElementsByName('procat[]');
	var x=0;

	for(i=0;i<fieldobj.length;i++){
		if (fieldobj[i].checked) {
			x++;
		}
	}

	if(x == 0){
		return (false);
	} else if (x > 3){
		return (false);
	}
}

function valid_step3(form){

	valid_all_step3();

	if (form.login.value.length <= 3){
		return (false);
	} else {

		var valid = usernameCharsValid(form.login.value);

		if (!valid){
			return (false);
		}
	}

	if (form.pass.value.length <= 3)	{
		return (false);
	} else {
		var valid = passCharsValid(form.pass.value);
		if (!valid){
			return (false);
		}
	}

	if (form.pass2.value.length <= 3){
		return (false);
	} else {
		var valid = passCharsValid(form.pass2.value);
		if (!valid){
			return (false);
		}
	}

	if (form.pass.value != form.pass2.value){
		return (false);
	}

	if (form.security.value.length <= 0){
		return (false);
	}

	if (form.chkagree.checked == false)	{

		alert('You need to agree Trademal\'s TERM OF USE AGREEMENT in order to proceed next step.');
		return (false);
	}
}

function domainnameCharValid(c) {
  return (((c >= 'a') && (c <= 'z')) ||
           ((c >= 'A') && (c <= 'Z')) ||
           ((c >= '0') && (c <= '9')) ||
           (c == '-') || (c == '_'));
}

function domainnameCharsValid(s) {
  for (var i = 0; i < s.length; i++) {
    if (!domainnameCharValid(s.charAt(i)))
      return false;
  }
  return true;
}

function usernameCharValid(c) {

  return (((c >= 'a') && (c <= 'z')) ||
           ((c >= 'A') && (c <= 'Z')) ||
           ((c >= '0') && (c <= '9')) ||
           (c == '-') || (c == '_') ||
		   (c == '@') || (c == '.') );
}

function usernameCharsValid(s) {
  for (var i = 0; i < s.length; i++) {
    if (!usernameCharValid(s.charAt(i)))
      return false;
  }
  return true;
}

function passCharValid(c) {

  return (((c >= 'a') && (c <= 'z')) ||
           ((c >= 'A') && (c <= 'Z')) ||
           ((c >= '0') && (c <= '9')));
}

function passCharsValid(s) {
  for (var i = 0; i < s.length; i++) {
    if (!usernameCharValid(s.charAt(i)))
      return false;
  }
  return true;
}

function domainchecking(baseurl, x){
  //alert(x);
	var msg = "";
	
	if (x.length > 0 && x.length < 3){

		msg = "err";

	} else if (x.length >= 3){

		var valid = domainnameCharsValid(x);

		if (!valid){
			msg = "err";
		}
	}

	var checkingurl = baseurl+'/subdomain.php?';
	var pars = 'x='+x+'&msg='+msg;
	var target = 'domain_result';

	var myAjax = new Ajax.Request(
		checkingurl, 
	{
		method: 'post', 
		parameters: pars, 
		onComplete: function(request) {
			$(target).innerHTML = request.responseText;
			document.getElementById('domain_msg').style.display = "none";
			document.getElementById('domain_result').style.display = "block";
		},
		onFailure: reportError
	});	
}


function usernamechecking(baseurl, x){

	var msg = "";
	
	if (x.length > 0 && x.length < 3){

		msg = "err";

	} else if (x.length >= 3){

		var valid = usernameCharsValid(x);

		if (!valid){
			msg = "err";
		}
	}

	var checkingurl = baseurl+'/valid.username2.php?';
	var pars = 'x='+x+'&msg='+msg;
	var target = 'username_result';

	var myAjax = new Ajax.Request(
		checkingurl, 
	{
		method: 'post', 
		parameters: pars, 
		onComplete: function(request) {
			$(target).innerHTML = request.responseText;
			document.getElementById('username_msg').style.display = "none";
			document.getElementById('username_result').style.display = "block";
		},
		onFailure: reportError
	});	
}

function reportError(){
	alert("Reporting Time Out !");
}


function valid_sendmail(form){

	if (form.from_company.value.length <= 0){
		alert("Plaese specify, Your Company Name.");
		form.from_company.focus();
		return (false);
	}

	if (form.from_name.value.length <= 0){
		alert("Plaese specify, Your Name.");
		form.from_name.focus();
		return (false);
	}

	if (form.from_email.value.length <= 0){
		alert("Plaese specify, Your Email.");
		form.from_email.focus();
		return (false);

	} else {
		if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form.from_email.value))){
			alert("Invalid Email.");
			form.from_email.focus();
			return (false);
		}
	}

	if (form.company1.value.length > 0){

		if (form.name1.value.length <= 0){
			alert("Plaese specify, Name.");
			form.name1.focus();
			return (false);
		}

		if (form.email1.value.length <= 0){
			alert("Plaese specify, Email.");
			form.email1.focus();
			return (false);

		} else {
			if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form.email1.value))){
				alert("Invalid Email.");
				form.email1.focus();
				return (false);
			}
		}
	}

	if (form.company2.value.length > 0){

		if (form.name2.value.length <= 0){
			alert("Plaese specify, Name.");
			form.name2.focus();
			return (false);
		}

		if (form.email2.value.length <= 0){
			alert("Plaese specify, Email.");
			form.email2.focus();
			return (false);

		} else {
			if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form.email2.value))){
				alert("Invalid Email.");
				form.email2.focus();
				return (false);
			}
		}
	}

	if (form.company3.value.length > 0){

		if (form.name3.value.length <= 0){
			alert("Plaese specify, Name.");
			form.name3.focus();
			return (false);
		}

		if (form.email3.value.length <= 0){
			alert("Plaese specify, Email.");
			form.email3.focus();
			return (false);

		} else {
			if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form.email3.value))){
				alert("Invalid Email.");
				form.email3.focus();
				return (false);
			}
		}
	}
}
